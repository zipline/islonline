<?php
//
//Descriptors
$success = array();
$error = array();

//
//Paths
$real_path = '/var/www/dev.islonline.org';
$back_path = '/home/backup';

//
//Files
$new_file = 'nightly_dev_backup_' . date('Y_m_d_H');
//
//2018-04-04 DJT Changed formatting and order of month and day.
//$old_file = 'nightly_dev_backup_' . date('Y_m_d_H', mktime(date('H'), 0, 0, date('n')-14, date('m'), date('Y')));
$old_file = 'nightly_dev_backup_' . date('Y_m_d_H', mktime(date('H'), 0, 0, date('n'), date('j')-10, date('Y')));

//
//Check to see if $back_path exists, otherwise create it
if(!file_exists($back_path))
{
	if(!mkdir($back_path, '0700', true))
	{
		$error[] = 'Cannot create ' . $back_path;
	}
	else
	{
		$success[] = 'Directory Created: ' . $back_path;		
	}
}
else
{
	$success[] = 'Directory Exists: ' . $back_path;	
}


//
//Check to see if $new_file exits, if so then error
if(file_exists($back_path . "/" . $new_file . '.tar.gz'))
{
	$error[] = 'New file ' . $new_file . ' already exists';
}

//
//Perform the backup...
if(count($error)==0)
{
	$command = 'tar -c -z -P -f "' . $back_path . '/' . $new_file . '.tar.gz" -C / ' . substr($real_path,1) . '/';
	$results = shell_exec($command . ' 2>&1');
}

//
//Check new file
if(file_exists($back_path . '/' . $new_file . '.tar.gz'))
{
	$success[] = 'File Created: ' . $back_path . '/' . $new_file . '.tar.gz';
}
else
{
	$error[] = 'Tar failed';	
}

//
//Remove old file
if(file_exists($back_path . '/' . $old_file . '.tar.gz'))
{
	if(unlink($back_path . '/' . $old_file . '.tar.gz'))
	{
		$success[] = 'File Removal: ' . $back_path . '/' . $old_file . '.tar.gz' . ' removed';
	}
	else
	{
		$error[] = 'File Removal: ' . $back_path . '/' . $old_file . '.tar.gz' . ' failed to delete';
	}
}
else
{
	$success[] = 'File Removal: ' . $back_path . '/' . $old_file . '.tar.gz' . ' does not exist';
}

//
//2018-07-08 DJT New removal code to kill old database backup files
$db_file_count = 0;
$db_missed_count = 0;
$db_path = $back_path . '/db';
$db_file_date = date('Y_m_d', mktime(0, 0, 0, date('n'), date('j')-11, date('Y')));

for($db_hour = 0; $db_hour<=23; $db_hour++)
{
	$db_filename = $db_file_date . '_' . sprintf('%02d', $db_hour) . '.tar.gz';

	if(file_exists($db_path . '/' . $db_filename))
	{
		if(unlink($db_path . '/' . $db_filename))
		{
			$success[] = 'File Removal: ' . $db_path . '/' . $db_filename . ' removed';
			$db_file_count++;
		}
		else
		{
			$error[] = 'File Removal: ' . $db_path . '/' . $db_filename . ' failed to delete';
			$db_missed_count++;
		}
	}
	else
	{
		$success[] = 'Database files do not exist for ' . $db_filename;
	}
}

//
//Mail results
$body = '';
$body .= 'New: ' . $new_file . PHP_EOL;
$body .= 'Old: ' . $old_file . PHP_EOL;
$body .= 'Com: ' . (!empty($command) ? $command : 'Command Missing') . PHP_EOL;
$body .= PHP_EOL;
$body .= 'Res: ' . implode(PHP_EOL . '     ', $success) . PHP_EOL;
$body .= 'Err: ' . implode(PHP_EOL . '     ', $error) . PHP_EOL;
$body .= 'Db: ' . $db_file_count . PHP_EOL;

echo $body;

mail('david@illumatech.net', 'Nightly Backup ' . date("Y-m-d"), $body);

?>