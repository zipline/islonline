<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ISL
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<?php 
	 $page_slug = '';
    if (is_page()) { $page_slug = 'page-'.$post->post_name; }

        $menu1Options = array(
        'menu'            => 'Primary Left',
        'theme_location'  => 'primary-left',
        'container'       => '',
        'menu_class'      => 'menu',
        'menu_id'         => 'primary-left',
        'link_before'     => '<span>',
        'link_after'      => '</span>'
    );

        $menu1OptionsOffCanvas = array(
        'menu'            => 'Primary Left',
        'theme_location'  => 'primary-left',
        'container'       => '',
        'menu_class'      => 'menu off-canvas-list',
        'menu_id'         => 'primary-left',
        'link_before'     => '<span>',
        'link_after'      => '</span>',
    );

        $menu2Options = array(
        'menu'            => 'Primary Right',
        'theme_location'  => 'primary-right',
        'container'       => '',
        'menu_class'      => 'menu',
        'menu_id'         => 'primary-right',
        'link_before'     => '<span>',
        'link_after'      => '</span>'
    );
        $menu2OptionsOffCanvas = array(
        'menu'            => 'Primary Right',
        'theme_location'  => 'primary-right',
        'container'       => '',
        'menu_class'      => 'menu off-canvas-list',
        'menu_id'         => 'primary-right',
        'link_before'     => '<span>',
        'link_after'      => '</span>'
    );
?>
<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
			<nav class="main-navigation nav-left">
				<ul class="menu"><li><a><span><i class="fa fa-bars"></i></span></a></li></ul>
				<?php wp_nav_menu($menu1Options); ?>
			</nav>
			<nav class="main-navigation nav-right">
				<?php wp_nav_menu($menu2Options); ?>
			</nav>
			<div id="logo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" /></a></div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
