<?php
/*
Template Name: Checkout Page
*/
?>

<?php get_header(); ?>


	
    	<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>        

<?php do_shortcode('[show_cart]');  ?>



<?php get_footer(); ?>

