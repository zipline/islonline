<?php
/*
Template Name: Checkout CC Pay
*/
?>

<?php get_header(); ?>


	
    	<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>        


<?php do_shortcode('[show_cart display_only="true"]');  ?>
<?php do_shortcode('[credit_card_payment_code price="2499"]');  ?>
<?php do_shortcode('[display_credit_form]');  ?>
<?php do_shortcode('[credit_card_payment_thankyou_page]'); ?>


<?php get_footer(); ?>

