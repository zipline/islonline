<?php
/*
Template Name: Program
*/
?>

<?php get_header(); ?>

	
    	<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>        
    
    	<img src="<?php the_field('program_image'); ?>">
        
    	<?php the_field('program_content'); ?>


<table>
	<TR>
    	<TD><?php echo do_shortcode("[list_program_links]"); ?></TD>
        <TD><?php echo do_shortcode("[list_countries_links]"); ?></TD>
    </TR>
</table>    

<?php get_footer(); ?>

