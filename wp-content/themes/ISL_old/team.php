<?php
/*
Template Name: Team
*/
?>

<?php
function enqueue_ajax_cart_features()
{
	wp_enqueue_script('jquery');

	wp_enqueue_script('cmd_cart_loading', plugins_url() . '/command/js/cart.load.js');
	wp_localize_script('cmd_cart_loading', 'ajax_path', array('ajax_url' => admin_url('admin-ajax.php')));

	wp_enqueue_script('cmd_cart_count', plugins_url() . '/command/js/cart.count.js');
	wp_localize_script('cmd_cart_count', 'ajax_path', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action ('wp_enqueue_scripts', 'enqueue_ajax_cart_features');


//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require $plugin_path . "team.class.php";
require $plugin_path . "country.class.php";

//
//Gather Team Details
//
$team = new TEAM();
$team->set_team($team->find_team_by_name(substr($pagename, 5)));

?>

<div name="cart" align="right" style="background-color:yellow">
    <a href="/checkout/">You have <span name="cart_items" id="cart_items"></span> items in your cart.</a>
</div>

<?php get_header(); ?>


	
        
    	<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>        
    
    	<div style="background-color:grey">
        	<table style="width:100%">
				<tr>
                	<td style="width:33%">Country: <?php _e($country_list)?></td>
                	<td style="width:33%"></td>
                    <td style="width:33%" rowspan=0>
                    	<button type="button" name="addtocart" id="addtocart">
                                Join this Team!
                        </button>
                    </td>
                </tr>
                <tr>
                	<td>ARRIVAL: <?php _e($team->arrival_date())?></td>
                    <td>ARRIVAL CITY: <?php _e($team->value('arrival_city'))?></td>
                    <td></td>
                </tr>
                <TR>
                	<TD>DEPARTURE: <?php _e($team->departure_date())?></TD>
                    <td>Departure City: <?php _e($team->value('departure_city'))?></td>
                    <td></td>
                </TR>
                <TR>
                	<TD>Duration: <?php _e($team->trip_duration())?></TD>
                    <TD>Cost: <?php _e($team->trip_cost())?></TD>
                    <TD></TD>
                </TR>
                <TR><TD colspan=2>Just x more volunteers to make this trip a go!</TD><td></td><td></td></TR>            
            </table>
        </div>
        
        <p>And then we go here</p>
<br>
<br>
<br>

	<form name="team_add" id="team_add">
    	<input type="hidden" name="team_id" id="team_id" value="<?php _e($team->get_team())?>">
        <input type="hidden" name="team_quan" id="team_quan" value="1">
    </form>
        


<?php _e(get_footer())?>
