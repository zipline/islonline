<?php
/*
Template Name: Country
*/
?>

<?php get_header(); ?>

<?php
$src = get_field('country_image'); ?>

<div class="banner" style="background-image: url(<?php echo $src ?>);">
	
    	<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>        
</div>
    	<!--<img src="<?php the_field('country_image'); ?>">-->

<div class="row columns">
    <div class="medium-8 columns">
        
    	<?php the_field('country_details'); ?>
    </div>

    <div class="medium-4 columns">
        Trip feed goes here.
    </div>


<table>
	<TR>
    	<TD><?php echo do_shortcode("[list_program_links]"); ?></TD>
        <TD><?php echo do_shortcode("[list_countries_links]"); ?></TD>
    </TR>
</table>    

<?php get_footer(); ?>
