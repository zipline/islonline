<?php
/*
Template Name: Personal Fundraiser
*/

$rsvp_id = !empty($_REQUEST['rsvp']) ? $_REQUEST['rsvp'] : (int) basename($_SERVER['REQUEST_URI']);
	//
//Check RSVP number
if(!empty($rsvp_id))
{
    //
    //Setup classes
    $plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
    require_once ($plugin_path . "rsvp.class.php");
    require_once ($plugin_path . "user.class.php");
    require_once ($plugin_path . "team.class.php");
    require_once ($plugin_path . "finance.class.php");

    $rsvp = new RSVP();
    $rsvp->set_rsvp_id($rsvp_id);
    $reservation = $rsvp->fetch_rsvp();

	//
	//If there is an actual reservation, then gather the rest of the data
	if(!empty($reservation['post_id']))
	{
		$team = new TEAM();
		$user = new USER();
		$finance = new FINANCE();
		
		//
		//User Data
		$verbiage = $rsvp->fund_language();
	
		//
		//Check verbiage
		if(empty($verbiage['amount']))
		{
			$verbiage['amount'] = 0;
		}
		

		//
		//Team Data
		$team->set_team($reservation['team_id']);
		
		$date_array = date_parse_from_format("m-d-Y", $team->arrival_date());
		$timestamp = mktime(0, 0, 0, $date_array['month'], $date_array['day'], $date_array['year']);
		$diff = round(($timestamp - time()) / (60 *60 *24), 0, PHP_ROUND_HALF_UP);
		
		if($diff > 0)
		{
			$message = "Days Left: " . $diff;
		}
		else
		{
			$message = "Thank you!";
		}

		//
		//Gather raised amount
		$raised = 0;
		
		$donations = $finance->Fetch_OrderItems($reservation['order_id'], 'All', 'All', true, true);
//		echo '<pre>';
//		print_r($donations);
//		echo var_dump($donations);
//        echo $reservation['order_item_id'];die;
//		$donations = $finance->payments(array($reservation['order_item_id']), false);

//OLD VERSION
//        foreach($donations as $ind => $data)
//        {
//            if(isset($data['payment']))
//            {
//                $raised += $data['payment'];
//
//            }
//        }
//END OF OLD VERSION
        //NEW VERSION
        $item = $finance->fetch_item($reservation['order_item_id']);
        $add_ons = $finance->add_ons($reservation['order_item_id']);

        $all_items[] = $reservation['order_item_id'];
        foreach($add_ons as $index => $itm)
        {
            $all_items[] = $itm['id'];
        }

        $raised = 0;

        foreach($finance->payments($all_items) as $payments)
        {
            if($payments['amount'] != 0)
            {
                $raised += $payments['amount'];
            }
        }
        //END OOF NEW VERSION
//        $donations = $finance->payments(array($reservation['order_item_id']), false);
//        foreach($donations as $ind => $data)
//		{
//            if(isset($data['amount']))
//            {
//                $raised += $data['amount'];
//
//            }
//        }

	}

	if(isset($verbiage))
	{

		function override_og_url(){
			$rsvp_id = !empty($_REQUEST['rsvp']) ? $_REQUEST['rsvp'] : (int) basename($_SERVER['REQUEST_URI']);
			$url = "sendme/" . $rsvp_id;
			return site_url($url);
		}
		add_filter('wpseo_canonical', 'override_og_url');
		add_filter('wpseo_opengraph_url', 'override_og_url');


		global $sendme_og_title;
		$sendme_og_title = $verbiage['title'];
		function override_og_title(){
			global $sendme_og_title;
			return $sendme_og_title;
		}
		add_filter('wpseo_title', 'override_og_title');


		global $sendme_og_desc;
		$sendme_og_desc = $verbiage['blurb'];
		function override_og_description(){
			global $sendme_og_desc;
			return $sendme_og_desc;
		}
		add_filter('wpseo_metadesc', 'override_og_description');


		global $sendme_og_image;
		$sendme_og_image = $user->banner($reservation['user_id']);
		function insert_og_image() {
			global $sendme_og_image;
			echo '<meta property="og:image" content="'. $sendme_og_image .'" />';
		}
		add_action('wp_head','insert_og_image');

	}
}
?>
<?php 
get_header();
?>
<?php
if(!empty($reservation['post_id']))
{
	?>	

    <div id="fundraiser">
        <div class="banner" style="background-image: url('<?php echo $user->banner($reservation['user_id']); ?>'); background-size: cover; background-position: center center;">
            <?php if(!empty($verbiage['title'])) : ?>
            <header class="entry-header">
                <h1><?php echo $verbiage['title']; ?></h1>
            </header>     
            <?php endif; ?>   
        </div>
    
			<?php
					//echo var_dump($_SERVER['REQUEST_URI']);
					//echo var_dump($_SERVER['QUERY_STRING']);
			?>
        <div id="content">
            <div id="inner-content" class="row">
                <main id="main" class="large-10 medium-10 medium-centered columns" role="main">
    
                    <div class="row">
                    <div id="left-navigation" class="large-3 columns">
                        <div class="avatar" style="background-image: url(<?php echo $user->avatar($reservation['user_id']); ?>); background-size: cover; background-position: center center;"></div>
                        <h3><?php echo $user->user_name($reservation['user_id']);?></h3>
                    </div>
    
                    <div id="right" class="large-9 columns">
    
                    <div id="canvas">
                    	<h2><?php echo $message; ?></h2>
                    	
                        <meter value="<?php echo $raised;?>" min="0" max="<?php echo $finance->fetch_Cost(false,$reservation['order_id'])?>"></meter>

                        <div class="raised">
                            <h4>Raised: <br />
                            <span>$<?php echo number_format($raised,2);?></span></h4>
                        </div>
                        <div class="goal">
                            <h4>Goal: <br />
                            <span>$<?php echo number_format($finance->fetch_Cost(false,$reservation['order_id']) ,2);?></span></h4>
                        </div>
                        
                        <p><?php echo $verbiage['blurb']?></p>
                        <?php
						if($message != "Thank you!")
						{
							?>
                        <a class='large button' href='/payment/?team_id=<?php echo $team->get_team();?>&member_id=<?php echo $reservation['user_id'];?>&rsvp=<?php echo $rsvp_id;?>&donate=true&max=<?php echo $verbiage['amount']?>'>Donate</a>
                        	<?php
						}
						?>
                        
                        <h3>Share:
                            <a class='twitter' href='http://twitter.com/home?status=Join this @islonline program and make a difference! <?= site_url("sendme/" . $rsvp_id); ?> #islonline' title='Share this post on Twitter!' target='_blank'><i class='fa fa-twitter'></i></a>
                            <a class="facebook" id="fb_share" href='http://www.facebook.com/sharer.php?u=<?= the_permalink(); ?>' title='Share this post on Facebook!' target='_blank'><i class='fa fa-facebook'></i></a>
                        </h3>
                        
                        <div id="donations">
                        	<?php
							$donations = $finance->payments(array($reservation['order_item_id']), false);
							foreach($donations as $ind => $data)
							{
								if(isset($data['special']) && $data['special'] == 'donate' || $data['special'] == 'donate anyonymous')
								{
									echo '<div class="item">';
									echo '<div class="donater"><h3 class="name"><span>From:</span><br /> ' . (($data['created_by'] > 0 && $data['special'] != 'donate anyonymous') ? $user->user_name($data['created_by']) : "Anonymous") . '</h3>';
									echo '<div class="note"><h3 class="name"><span>Note:</span></h3><p>' . stripslashes($data['note']) . '</p></div>';
                                    echo '</div>';
									echo '<div class="donation"><h3 class="amount"><span>Donated:</span><br />$' . number_format($data['amount']) . '</h3>';
									echo '<p>' . $data['created_when'] . '</p>';
									echo '</div>';
									echo '</div>';
								}
							}
							?>
                        </div>
                         
                    </div>
                
                </main> <!-- end #main -->
            </div> <!-- end #inner-content -->
        </div> <!-- end #content --> 
    </div><!-- end #user -->
	
	<?php
}
else
{
	?>

    <div id="fundraiser">
        <div class="banner" style="background-image: url(''); background-size: cover; background-position: center center;">
            <header class="entry-header">
                <h1>This is not a valid Fundraiser page</h1>
            </header>     
        </div>
    
        <div id="content">
            <div id="inner-content" class="row">
				<main id="main" class="large-10 medium-10 large-centered columns" role="main">
					We are sorry, but the fundraiser you are looking for does not exist.    
				</main>
			</div>
		</div>                    
	</div>
 	<?php
}
?>
<script>
    window.onload = function() {
        fb_share.href ='http://www.facebook.com/share.php?u=' + encodeURIComponent(location.href); 
    }  
</script>

<?php get_footer(); ?>