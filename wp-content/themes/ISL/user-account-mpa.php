<?php
/*
Template Name: MPA Contact
*/
?>
<?php
//
//Setup classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "user.class.php");
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "team.class.php");

$user = new USER();
$rsvp = new RSVP();
$team = new TEAM();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(isset($_GET['rsvp']) && $_GET['rsvp'] > 0 && !$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;

$rsvp->set_rsvp_id($_GET{'rsvp'});
$rsvp->fetch_rsvp();
$reservation = $rsvp->get_rsvp();

$team->set_team($reservation['team_id']);

$departure = DateTime::createFromFormat('m/d/Y', $team->departure_date());
$today = DateTime::createFromFormat('m/d/Y', date('m/d/Y'));


?>

<?php acf_form_head(); ?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>

<h1>Proof of Participation</h1>

<?php
if($departure >= $today)
{
	echo "Your POP letter will be available here as a downloadable PDF after your trip. This helpful document serves as proof of your participation in a medical learning environment and will document your hours of service.";
	echo "<BR>";
	echo "<BR>";
}
else
{
	echo "This is your Proof of Participation letter. This helpful document serves as proof of your participation in a medical learning environment and will document your hours of service.";
	echo "<BR>";
	echo '<a href="/pop-content/?rsvp=' . $_GET['rsvp'] . '&userid=' . $_GET['user_id'] . '&team_id=' . $_GET['team_id'] . '" target="_blank">POP Letter</a>';
	echo "<BR>";
	echo "<BR>";
}
?>

<?php wp_head(); ?>

<div class="iframe-inner">
<h1>MPR Contact Form</h1>
	
<?php the_content(); ?>
</div>
<?php wp_footer(); ?>

