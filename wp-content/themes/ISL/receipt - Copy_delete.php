<?php
/*
Template Name: Receipt Old
*/
?>

<?php
//
//Test Data
// Account Number: 4032039649918960
// Card Type: Visa
// Expiration: 01/2021
// CVV2: Any

//
//Make sure we should be here, otherwise get out of here
if(empty($_POST['MethodAmount']) || ($_POST['MethodAmount'] != 'balance' && $_POST['MethodAmount'] != 'deposit'))
{
 	wp_safe_redirect( wp_get_referer() );	
}

if(empty($_POST['paythrough']))
{
	wp_safe_redirect( wp_get_referer() );
}
else
{
	$page = $_POST['paythrough'];
}


//
// Include required library files.
require_once CC_CLASSES_DIR . "paypal/config.php";
require_once CC_CLASSES_DIR . "paypal/autoload.php";
require_once CC_CLASSES_DIR . "paypal/PayPal/PayPal.php";
require_once CC_CLASSES_DIR . "paypal/PayPal/PayFlow.php";
require_once CC_CLASSES_DIR . "cart.class.php";
require_once CC_CLASSES_DIR . "finance.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "user.class.php";
require_once CC_CLASSES_DIR . "team.class.php";

//
// Create PayPal object.
/*
$PayPalConfig = array(
					'Sandbox' => $sandbox,
					'APIUsername' => $api_username,
					'APIPassword' => $api_password,
					'APISignature' => $api_signature, 
					'PrintHeaders' => $print_headers, 
					'LogResults' => $log_results,
					'LogPath' => $log_path,
					);
*/
$PayPalConfig = array(
					'Sandbox' => $sandbox,
					'APIUsername' => $payflow_username, 
					'APIPassword' => $payflow_password, 
					'APIVendor' => $payflow_vendor, 
					'APIPartner' => $payflow_partner, 
					'PrintHeaders' => $print_headers, 
					'LogResults' => $log_results,
					'LogPath' => $log_path,
					);

//
//Instantiate PayPal object
$PayPal = new angelleye\PayPal\PayFlow($PayPalConfig);

if($page == "checkout")
{
	//
	//Fetch the cart and validate it
	
	$cart = new CART();
	$cart->validate();
	$team = new TEAM();
	
	//
	//Amount to process...
	if($_POST['MethodAmount'] == 'balance')
	{
		$process_amount = $cart->total($cart->type('total'));	
	}
	if($_POST['MethodAmount'] == 'deposit')
	{
		$process_amount = $cart->total($cart->type('deposit')) + $cart->total($cart->type('fee'));	
	}
}
else
{

	$rsvp = new RSVP();
	$finance = new FINANCE();
	
	$rsvp->set_id_by_team_user($_REQUEST['team_id'], $_REQUEST['member_id']);
	$rsvp_data = $rsvp->fetch_rsvp();

	$item = $finance->fetch_item($rsvp_data['order_item_id']);
	$add_ons = $finance->add_ons($rsvp_data['order_item_id']);
	$all_items[] = $rsvp_data['order_item_id'];

	$cost = $item['price'];
	foreach($add_ons as $index => $itm)
	{
		$all_items[] = $itm['id'];
		$cost += $itm['price'];
	}		

	$pays = 0;
	foreach($finance->payments($all_items) as $payments)
	{
		$paid_amount[$payments['order_item_id']] = $payments['amount'];
		$pays += $payments['amount'];	
	}
	

	//
	//Amount to process...
	if($_POST['MethodAmount'] == 'balance')
	{
		$process_amount = ($cost - $pays);
	}
	if($_POST['MethodAmount'] == 'deposit')
	{
		$process_amount = $_POST['alt_pay'];	
	}
}



//
// Prepare request arrays
$DPFields = array(
    'paymentaction' => 'Sale', 
    'ipaddress' => $_SERVER['REMOTE_ADDR'], 
    'returnfmfdetails' => '0'
);

//
//Set basic credit card details				
/*
$CCDetails = array(
					'creditcardtype' => $_POST['card'], 											// Required
					'acct' => $_POST['acctnum'], 													// Required
					'expdate' => date('m', strtotime($_POST['exp_month'])) . $_POST['exp_year'],	// Required
					'cvv2' => $_POST['cvv2']														// Required
				);
// Required.  Indicates the type of transaction to perform.  Values are:  A = Authorization, B = Balance Inquiry, C = Credit, D = Delayed Capture, F = Voice Authorization, I = Inquiry, L = Data Upload, N = Duplicate Transaction, S = Sale, V = Void
		
		*/
$CCDetails = array(
					'tender' => 'C',					 											// Required
					'trxtype' => 'S',					 											// Required
					'acct' => $_POST['acctnum'], 													// Required
					'expdate' => date('m', strtotime($_POST['exp_month'])) . $_POST['exp_year'],	// Required
					'cvv2' => $_POST['cvv2']														// Required
				);

//
//Set payer information				
$PayerInfo = array(
					'email' => $_POST['email'],						// Required
					'firstname' => $_POST['firstname'], 			// Required
					'lastname' => $_POST['lastname']				// Required
				);

//
//Set Payer Billing Address information				
$BillingAddress = array(
						'street' => $_POST['addr1'],				// Required. Street address		
						'street2' => $_POST['addr2'], 				// Second street address.
						'city' => $_POST['city'], 					// Required.  Name of City.
						'state' => $_POST['state'], 				// Required. Name of State or Province.
						'countrycode' => 'US', 						// Required.  Country code.
						'zip' => $_POST['zip'], 					// Required.  Postal code of payer.
						'phonenum' => $_POST['phone'] 				// Phone Number of payer.  20 char max.
					);
					
//
//Set Payment financial information					
$PaymentDetails = array(
						'amt' => $process_amount,					// Required
						'currencycode' => 'USD',					// Required
						'itemamt' => $process_amount,				// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
						'desc' => '', 								// Description of the order the customer is purchasing.  127 char max.
						'custom' => '', 							// Free-form field for your own use.  256 char max.
						'invnum' => '', 							// Your own invoice or tracking number
						'notifyurl' => '', 							// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
						'recurring' => 'N'							// Required
					);

if($page=="checkout")
{
	//
	//Line Items
	$OrderItems = array();		
		
	foreach($cart->list_items() as $types => $full_items)
	{
		foreach($full_items as $item_id => $item_data)
		{
			
			$mod_amount = 0;
			$l_name = '';
			switch($_POST['MethodAmount'])
			{
				case 'deposit':
					switch($types)
					{
						case 1:
							$mod_amount = $cart->deposit_amount($item_id);
							break;
						default:
							$mod_amount = $item_data[1];
							break;	
					}
					break;
				case 'balance':
					$mod_amount = $item_data[1];
					break;
			}
	
			switch ($types)
			{
				case 0:
					$team->set_team($item_id);
					$l_name = $team->value('hash') . ' Application Fee';
					break;				
				case 1:
					$team->set_team($item_id);
					$l_name = $team->value('hash');
					if($_POST['MethodAmount'] == 'deposit')
					{
						$l_name .= " Deposit";	
					}
					break;
				default:
					$l_name = 'Unregistered product';				
			}
	
			$Item	 = array(
									'l_name' => $l_name, 						// Item Name.  127 char max.
									'l_desc' => $item_id, 					// Item description.  127 char max.
									'l_amt' => $mod_amount, 				// Cost of individual item.
									'l_number' => '', 						// Item Number.  127 char max.
									'l_qty' => $item_data[2], 				// Item quantity.  Must be any positive integer.  
									'l_taxamt' => '0', 						// Item's sales tax amount.
									'l_ebayitemnumber' => '', 				// eBay auction number of item.
									'l_ebayitemauctiontxnid' => '', 		// eBay transaction ID of purchased item.
									'l_ebayitemorderid' => '' 				// eBay order ID for the item.
							);
	
			array_push($OrderItems, $Item);
		}
	}
}
else
{
	$OrderItems = array();
}

//
//Assemble the PayPal Request
/*
$PayPalRequestData = array(
						   'DPFields' => $DPFields, 
						   'CCDetails' => $CCDetails, 
						   'PayerInfo' => $PayerInfo, 
						   'BillingAddress' => $BillingAddress, 
						   'ShippingAddress' => array(), 
						   'PaymentDetails' => $PaymentDetails, 
						   'OrderItems' => $OrderItems
						   );
*/

$PayPalRequestData = array_merge($CCDetails, 
						   		 $PaymentDetails
								);

//
//Check to see if we have a logged in user, if not, then we need to create one
if(!is_user_logged_in())
{
	$user =- new USER();
	
	//
	//Create a new user
	$user_info['user_login'] = $user->temp_user_name();
	$user_info['user_pass'] = $_POST['lastname'];
	$user_info['first_name'] = $_POST['firstname'];
	$user_info['last_name'] = $_POST['lastname'];
	$user_info['user_email'] = $_POST['email'];
		
	$new_user = $user->create_user($user_info);
	
	//
	//Log in the user
	if($new_user)
	{
		wp_set_current_user($new_user, $user_info['user_login']);
		wp_set_auth_cookie($new_user);
		do_action('wp_login', $user_info['user_login']);		
	}
	else
	{
		//
		//Error, user cannot be created for some reason
	}
}


?>

<?php get_header(); ?>

<p><BR>
  <BR>
  <BR>
  <BR>
</p>

	
    	<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>        


<?php //show_cart(false);  ?>

<?php

//echo var_dump($_POST);

/*
Run card for processing

If successful
	Write RSVP record for all 0 items
	Write payment record for all items
	Send Receipt
	Remove Cart Items
	Open MyISL Page
Else
	Error


*/

//
// Pass data into class for processing with PayPal and load the response array into $PayPalResult
$PayPalResult = $PayPal->DoDirectPayment($PayPalRequestData);
//$PayPalResult = $PayPal->ProcessTransaction($PayPalRequestData);


//
//Write Transaction Record
$finance = new FINANCE();
echo var_dump($PayPalResult);
$trans_data = 	array('amount' => $PayPalResult['REQUESTDATA']['AMT'],
					  'form' => !empty($PayPalResult['REQUESTDATA']['CREDITCARDTYPE']) ? $PayPalResult['REQUESTDATA']['CREDITCARDTYPE'] : '',
					  'response' => (empty($PayPalResult['ACK']) ? 'Failure' : $PayPalResult['ACK']),
					  'transaction_id' => !empty($PayPalResult['TRANSACTIONID']) ? $PayPalResult['TRANSACTIONID'] : '',
					  'raw_request' => $PayPalResult['RAWREQUEST'],
					  'raw_response' => $PayPalResult['RAWRESPONSE']
					  );
$finance->new_transaction($trans_data);	

//
//If our request was approved.... [ACK] == Success
if(!empty($PayPalResult['ACK']) && $PayPalResult['ACK'] === 'Success')
{
	if($page=="checkout")
	{
		//
		//Start a new Order and new RSVP
		$finance->new_order();
		$rsvp = new RSVP();
		
		//
		//Migrate Cart Items to Order Items and RSVP Items
		foreach($cart->list_items() as $types => $full_items)
		{
			foreach($full_items as $item_id => $itm_data)
			{
				for($cnt = 1; $cnt <= $itm_data[2]; $cnt++)
				{
					$item_data = 	array('type' => $types,
										  'item_id' => $item_id,
										  'note' => '',
										  'add_on' => $itm_data[0],
										  'quantity' => 1,
										  'price' => $itm_data[1]
										  );
					$item_ins_id = $finance->new_item($item_data);
	
					//
					//RSVP
					if($item_data['type'] == 1)
					{
						$rsvp_for = ($cnt == 1 ? get_current_user_id() : 0);
						$rsvp->new_rsvp($item_id, $rsvp_for, $item_ins_id);
					}
	
					//
					//Mark payments
					switch($_POST['MethodAmount'])
					{
						case 'deposit':
							switch($item_data['type'])
							{
								case 1:
									$mod_amount = $cart->deposit_amount($item_id);
									break;
								default:
									$mod_amount = $item_data['price'];
									break;	
							}
							break;
						case 'balance':
							$mod_amount = $item_data['price'];
							break;
					}
					$finance->payment($item_ins_id, $mod_amount);
				}
			}
		}
	}
	else
	{
		//
		//Figure out how to divide payment...
		$items = array();
		foreach($finance->payments($all_items) as $index=>$data)
		{
			if(empty($items[$data['order_item_id']]['payments'][0]))
			{
				$items[$data['order_item_id']]['payments'][0] = 0;
			}
		
			$items[$data['order_item_id']]['payments'][0] += $data['amount'];
			$items[$data['order_item_id']]['payments'][$data['created_when']] = $data['amount'];
		}
		
		foreach($finance->items_cost($all_items) as $index=>$data)
		{
			if(empty($items[$data['id']]['cost'][0]))
			{
				$items[$data['id']]['cost'][0] = 0;
			}
		
			$items[$data['id']]['cost'][0] += $data['price'];
			$items[$data['id']]['cost'][$data['created_when']] = $data['price'];
			$items[$data['id']]['add_on'] = $data['add_on'];
			$items[$data['id']]['item_type'] = $data['item_type'];
		}
		
		foreach($all_items as $item_id)
		{
			$items[$item_id]['balance'] = $items[$item_id]['cost'][0] - $items[$item_id]['payments'][0]; 	
		}

		//
		//Apply payment to Application Fees, then major items, then add-ons
		$finance->new_order();
		$checks = array_keys($items);
		$passes = 4;
		$funds = $process_amount;
		while ($passes > 0 && count($checks) > 0)
		{
			foreach($items as $item_id => $data)
			{
				if($passes == 3 && $data['item_type'] == 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff);	
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
				}
				
				if($passes == 2 && $data['item_type'] == 1 && $data['add_on'] == 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff);	
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
					
				}

				if($passes == 1 && $data['item_type'] > 1 && $data['add_on'] == 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff);	
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
					
				}

				if($passes == 2 && $data['item_type'] > 1 && $data['add_on'] > 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff);	
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
					
				}

				$passes --;				
			}
		}
					
	}

	//
	//Clear Cart
	$cart->clear_cart();

// Write the contents of the response array to the screen for demo purposes.
//echo '<pre />';
//print_r($PayPalResult);
	
	echo "Receipt goes here";
	
}
else
{
	//	error goes here...
	foreach($PayPalResult['ERRORS'] as $index=>$error)
	{
		echo "Error " . $error['L_ERRORCODE'] . ": " . $error['L_LONGMESSAGE'] . "<br>";
	}
	//echo $PayPalResult['RAWRESPONSE'];	
}

?>

<?php get_footer(); ?>

