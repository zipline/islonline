<?php
/*
Template Name: Custom Team
*/
?>

<?php
/*
2018-02-13 DJT Removed scripting that checked login status and required login prior to submission.
               Add Event field to ACF Custom Team group.
			   Add Rewrite to theme functions.php
*/
?>

<?php
//
//DJT Removed 2018-02-13
//wp_enqueue_script("cmd_check_login", CC_JS_PATH . 'custom.team.logged.in.js', NULL);

//
//DJT Removed 2018-02-13
//if (is_user_logged_in() )
//{
//	$app_button = '';
//	$register_button = ' style="display: none;"';
//}
//else
//{
//	$app_button = ' style="display: none;"';
//	$register_button = '';
//}


//
//Get user data
$current_user = wp_get_current_user();

acf_form_head();

//echo var_dump($_SERVER['REQUEST_URI']);
//echo var_dump($_SERVER);

?>

<?php get_header(); ?>
<div class="no-banner"></div>
<div id="content">
	<div id="inner-content" class="row">
    	<main id="main" class="large-8 medium-8 small-centered columns" role="main">
        	<!--DJT Removed 2018-02-13 
        	<div <?php //echo $register_button; ?> name="intern_not_logged_in" id="intern_not_logged_in">
        		<h2>Create a Team</h2>
				<p>You need to be logged in before requesting a Custom Team so we can make sure we associate all of your request information with a proper account.</p>
				<P>After you login, you can close the login window to come back here and create this Custom Team request</P>
				<a class="button" href="/login" target="_blank">Click here to Login/Register</a>
			</div>
			-->
		<?php
			if(!empty($_REQUEST['updated']))
			{
				echo "<h2 style='text-align: center;'>Thank you for your Custom Program request.</h2><BR /><p style='text-align: center;'>We will follow-up with you soon.</p><p style='text-align: center;'>In the meantime, if you have any questions, feel free to <a href='/contact-us'>Contact Us</a>.</p><BR><BR>	";
			}
		?>
        
      	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	    	<?php get_template_part( 'parts/loop', 'page' ); ?>
	    <?php endwhile; endif; ?>
            <?php echo do_shortcode('[gravityform id="30"]'); ?>
			<?php
//				acf_form(array(
//					'post_id'		=> 'new_post',
//					'new_post'		=> array(
//						'post_type'		=> 'custom_team',
//						'post_status'	=> 'draft'
//					),
//					'submit_value'		=> 'Request Custom Team',
//					'updated_message' => 'Thank you for your request. We will be in contact with you soon.'
//				));
			?>

        </main> <!-- end #main -->
 
    </div> <!-- end #inner-content -->
</div> <!-- end #content -->
<!-- -->
<!-- DJT Added 2018-02-13 Required to hide event field and populate -->
<script>
	jQuery(document).ready(function($)
	{
		var online = navigator.onLine;
		
		if(online)
		{
			var event_field = jQuery("#custom-team-event");
			if (event_field.length > 0){
                event_field.css('display', 'none');
                event_field.value(<?php echo ((isset($_GET['event'])) ? $_GET['event'] : '')?>);
            }
		}
	})
	
</script>
	
<?php get_footer(); ?>