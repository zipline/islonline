<?php
/*
Template Name: Ambassador Registration
*/
?>

<?php acf_form_head(); ?>

<?php

wp_register_script('jquery-ui-core', '/wp-includes/js/jquery/ui/jquery.ui.core.min.js', array('jquery'));
wp_register_script('jquery-ui-widget', '/wp-includes/js/jquery/ui/jquery.ui.widget.min.js', array('jquery'));
wp_register_script('jquery-ui-position', '/wp-includes/js/jquery/ui/jquery.ui.position.min.js', array('jquery'));
wp_register_script('jquery-ui-autocomplete', '/wp-includes/js/jquery/ui/jquery.ui.autocomplete.min.js', array('jquery'));

wp_enqueue_script('cmd_school_selector', plugins_url() . '/command/js/school.selector.js', array('jquery', 'jquery-ui-autocomplete'), NULL, true );
wp_localize_script('cmd_school_selector', 'svars', array('ajax_url' => admin_url('admin-ajax.php')));

wp_enqueue_script('cmd_ambassador_js', plugins_url() . '/command/js/ambassador.js', array('jquery', 'jquery-ui-autocomplete'), NULL, true );

//
//Get user data
$user = wp_get_current_user();
if(!empty($user))
{
	$post_id = get_user_meta($user->ID, '_cmd_comm_ambassador_post', true);
}

if (is_user_logged_in() )
{
	$app_button = '';
	$register_button = ' style="display: none;"';
}
else
{
	$app_button = ' style="display: none;"';
	$register_button = '';
}
?>

<?php get_header(); ?>

<style>
	.ui-menu 
	{
		background-color: black;
		text-align: left;
	}
	.ui-menu-item
	{
		background-color: black;
		color: white;
	}
	.ui-menu-item:hover
	{
		background-color: blue;
		color: white;
	}

</style>

<div class="no-banner"></div>
<div id="content">
	<div id="inner-content" class="row">
		
    	<main id="main" class="large-8 medium-8 small-centered columns" role="main">
			<h2>ISL Ambassador Application</h2>
				<div <?php echo $register_button; ?> name="intern_not_logged_in" id="intern_not_logged_in">
				<p>You need to be logged in before requesting an Ambassadorship so we can make sure we associate all of your request information with a proper account.</p>
				<P>After you Login, you can close the login window to come back here and apply for this Ambassadorship</P>
				<a class="button" href="/login" target="_blank">Click here to Login/Register</a>

			</div>
        
			<?php 
			$base_path = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;

			if(!empty($post_id))
			{
				$args = array('post_id'		=> $post_id,
							  'return' => $base_path,
							  'field_groups'	=> array(35181),
							  'submit_value'		=> 'Update Application',
							 );
			}
			else
			{
				$post_id = 'new_post';
				$args = array('post_id'		=> $post_id,
							  'field_groups'	=> array(35181),
							  'new_post'	=> array(
									'post_type'		=> 'isl_ambassador',
									'post_status'		=> 'draft'
													),
							  //'return' => $base_path,
							  'submit_value'		=> 'Request Ambassadorship'
							 );
			}

			if($post_id != "new_post")
			{
				echo "<div class='callout success' data-closable>Thank you for your request. We will be in contact with you soon.</div>";
			}

			acf_form($args);
			?>

		</main> <!-- end #main -->
 
    </div> <!-- end #inner-content -->
</div> <!-- end #content -->

<?php get_footer(); ?>