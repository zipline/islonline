<?php get_header(); ?>


<?php 

$src = get_field('featured_image')['sizes']['xlarge']; 

?>

<div class="banner" style="background-image: url(<?php echo $src ?>);">
	
    	<header class="entry-header">
			<h1 class="entry-title" style="color: <?php echo $color; ?>;"><?php the_title(); ?></h1>
		</header>  
<!--     <div class="overlay" style="background-color: <?php echo $overlay; ?>;"></div>    -->   
</div>

<div id="content">
	
	<div id="inner-content" class="row">

		<main id="main" class="large-10 medium-10 small-centered columns" role="main">

		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		    	<?php get_template_part( 'parts/loop', 'single-team_add_ons' ); ?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->
</div> <!-- end #content -->

<?php get_footer(); ?>