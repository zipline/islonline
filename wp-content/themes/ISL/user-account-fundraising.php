<?php
/*
Template Name: Fundraiser
*/
?>
<?php
//
//Kick the user to the login page if they aren't logged in
//
if(!is_user_logged_in() == 1)
{
	header ('Location: ' . site_url() . '/login/');	
}
?>
<?php
//
//Get user data
// wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );
wp_head();

//
//Setup classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "tabs.class.php");
require_once ($plugin_path . "finance.class.php");
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "acf.link.class.php");


$rsvp = new RSVP();
$page = new TAB_MANAGE();
$finance = new FINANCE();
$team = new TEAM();
$user = new USER();
$adv_custom_flds = new ACF_LINK();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;

if(!user_can($current_user->ID, 'upload_files'))
{
	$usr_upload = new WP_USER($current_user->ID);
	$usr_upload->add_cap('upload_files');
}

$rsvp->set_rsvp_id($_REQUEST['rsvp']);
$reservation = $rsvp->fetch_rsvp();

if(isset($_REQUEST['saving']) && $_REQUEST['saving'] == "Save Now")
{
	$fld_list = $_POST;
	foreach($_POST as $index=>$value)
	{
		//
		//ACF Fields
		if($index == "acf")
		{
			foreach($value as $acf_index=>$acf_value)
			{
				//
				//ACF field so handle differently
				if(substr($acf_index,0,6) == "field_" && !empty($fld_list['acf'][$acf_index]))
				{
					$adv_custom_flds->save_field_group($acf_index, $fld_list['acf'], $_GET['user_id'], NULL, true);
				}
			}
		}

	}

	$rsvp->fund_language($_REQUEST['title'], 0, $_REQUEST['blurb']);	

}

$fields = $rsvp->fund_language();

//
//
$team->set_team($reservation['team_id']);
if($team->value('no_finances') == 1)
{
	?>
	<script>
		window.location = '/my-isl/';
	</script>
	<?php
}

$donated = $finance->payments(array($reservation['order_item_id']), false, 'donate');

$min_donate = max(0, empty($donated[0]['amount']) ? 0 : $donated[0]['amount']);
$max_donate = $team->value('cost');

$share_url = site_url("sendme/" . $_REQUEST['rsvp']);

//
//Setup classes
wp_enqueue_script('cmd_settings_pics', plugins_url() . '/command/js/settings.pics.js', array('jquery'), '4.7.1');	
wp_localize_script('cmd_settings_pics', 'svars', array( 'banner' => 1,
														'user' => 0,
														'ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form')
														));

?>

<div id="myisl-content" class="sub-page fundraising">
    <p>
        This is the content for your public-facing Fundraising Page.<br>
        <strong>Share this page freely with your friends and family to help make your trip a reality!</strong>
    </p>

    <p>
        <a href="<?= $share_url; ?>"
            target="_blank" 
            class="button view-page-link">
            View Your Page
            <i class="fa fa-external-link" aria-hidden="true"></i>
        </a>
        <span style="font-size: 150%">
            <a class='twitter' href='http://twitter.com/home?status=Join this @islonline program and make a difference! <?= $share_url; ?> #islonline' title='Share this post on Twitter!' target='_blank'><i class='fa fa-twitter'></i></a>
            <a href='http://www.facebook.com/sharer.php?u=<?= $share_url; ?>' title='Share this post on Facebook!' target='_blank'><i class='fa fa-facebook'></i></a>
        </span>
    </p>

    <p class="share-url">
        <span>Here's your unique link:</span>
        <span class="url"><?= $share_url; ?></span>
    </p>

    <form name="fundraiser" id="fundraiser" method="POST" enctype = "multipart/form-data" action="<?php echo $page->GET_path(array('save'));?>">
        <table>
			 <?php
				$content = $adv_custom_flds->get_fields(4761);

				foreach($content as $fld_id => $fld_data)
				{
					echo "<TR>";
					echo "<th><label for='acf-" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
					echo "<td>" . $adv_custom_flds->build_field($fld_id, $_GET['user_id']);
					echo "</td>";
					echo "</TR>";

				}
			?>
           <TR>
                <TD>Title</td>
                <td><input type="text" name="title" id="title" size="60" value="<?php _e(!empty($fields['title']) ? $fields['title'] : '');?>"></TD>
            </TR>
            <TR>
                <TD style="vertical-align: top;">Blurb</TD>
                <td><textarea id="blurb" name="blurb" cols="60" rows="20"><?php _e(!empty($fields['blurb']) ? $fields['blurb'] : '');?></textarea></TD>
            </TR>
                
        </table>
        <input type="hidden" name="rsvp" value="<?php _e($_REQUEST['rsvp']) ?>">

        <div class="button-container">
            <button type="submit" id="saving" name="saving" class="button save-button float-right" value="Save Now">Save</button>
            <a href="<?php echo site_url('/sendme/?rsvp=' . $_REQUEST['rsvp']);?>" 
                target="_blank" 
                class="button secondary view-page-link float-left">
                View Your Page
                <i class="fa fa-external-link" aria-hidden="true"></i>
            </a>
        </div>
    </form>

</div>

<script>
	jQuery(document).ready(function() {
		var gender = jQuery('[name="acf[field_56fc9743543d0]"]');
		var birthdate = jQuery('#acf-field_57249f7a0c63c');

		//
		//Hide the actual ACF fields
		gender.closest('tr').hide();
		birthdate.hide();

		//jQuery('label[for=' + gender.attr('id') + ']').hide();
		jQuery('label[for=' + birthdate.attr('id') + ']').hide();
								 
								 
	})
</script>

<?php wp_footer() ?>