<?php
/*
Template Name: Cart Page
*/

//
//Required classes
require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "country.class.php";
require_once CC_CLASSES_DIR . "program.class.php";
require_once CC_CLASSES_DIR . "cart.class.php";
require_once CC_CLASSES_DIR . "itinerary.class.php";
require_once CC_CLASSES_DIR . "user.class.php";

$cart = new CART();
$cart->Validate();
$due_block = '';

$temp = 0;

//
//Check if we are clearing first
if(!empty($_REQUEST['method']) && $_REQUEST['method'] == 'clear')
{
	$cart->Clear();
	?>
		<script>
			 window.location = get_permalink(4411);
		</script>
	<?php
}

//
//Check Rep_code
if(!empty($_REQUEST['repcode']))
{
	$user = new USER();
	
	$rep_id = $user->user_by_rep_code($_REQUEST['repcode']);

	//
	//Apply CAP Rebate to each reservation
	if($rep_id)
	{
		$cart->add_CAP($rep_id);
	}
}

//
//Check Coupon
if(!empty($_REQUEST['coupon']))
{
	//
	//Apply Coupons to each item
	$cart->add_Coupon($_REQUEST['coupon']);
}
else
{
	$cart->clear_Coupons();
}

$cart->Validate();


$NO_FINANCE = false;
$cfl_remove = 0;

get_header();

?>
    
<div class="no-banner"></div>

<div id="content">
	<div id="inner-content" class="row">
		<div class="page-title small-centered columns">
			<?php
			//
			//Link to return to refering page.
			if(isset($_POST['cart_refer']) && $_POST['cart_refer'] != (site_url() . '/cart'))
			{
				echo "<p><a href='" . $_POST['cart_refer'] . "'><i class='fa fa-angle-left' aria-hidden='true'></i> Take me Back</a>";
			}
			else
			{
				echo "<p><a href='", get_permalink(4411), "'><i class='fa fa-angle-left' aria-hidden='true'></i> Find another Program</a>";
			}
			?>
			<h2>Your Selected Programs</h2>
			<hr />   
		</div>
		
		<main id="cart" class="small-centered columns" role="main"> 
			
            <div class="team-cart row">
                
				<?php
           		if( $cart->Count( 'Main' ) )
				{

					$add_ons_due_now = 0;
					$team_ids = array();
					foreach( $cart->Processed() as $main_item_id => $item_types )
					{

						echo "<div class='item large-12 medium-12 columns'>";
						
						foreach( $item_types as $item_type => $item )
						{

							foreach( $item as $item_id => $item_data )
							{

								switch( $item_type )
								{
									case 'reservation':
										//
										//Gather Team, Country, Program Details
										//
										$team = new TEAM();
										$country = new COUNTRY();
										$program = new PROGRAM();
										$itinerary = new ITINERARY();
										$rsvp = new RSVP();

										$trip_id = $item_id;
										$team->set_team($item_id);
										$team_ids[] = $item_id;
										$country_list = $country->country_list($team->schedule_countries());

										if($team->value('no_finances') == 1)
										{
											$NO_FINANCE = true;
										}
										
										//
										//Build program listings with line breaks between each.
										$programs = '';
										foreach($team->get_schedule() as $id=>$schedule_data)
										{
											$program->set_program($schedule_data['program']);
											$programs .= $program->get_name() . "<BR>";		
										}
										if(strlen($programs) > 3)
										{
											$programs =substr($programs,0,-4);
										}

										//
										//Double check quantity is ok
										$maximum = max(0, ($team->value('max') - $rsvp->count_rsvp($item_id)));
										if($maximum < $item_data['quantity'] || $maximum == 0)
										{
											$cart->set_RSVP($item_id, $item_data['cost'], $maximum);	
											$display_items = $maximum;

											//
											//Remove add-ons if the main item is gone
											if($maximum == 0)
											{
												$cart->set_item(0, $item_id, 0);
											}
										}
										else
										{
											$display_items = $item_data['quantity'];
										}

										//$line_sub_total += ($item_data['cost'] * $display_items);

										?>
										<h5>Program: <?php _e($team->value('hash'))?></h5>
										<div class="details medium-6 columns">
											<b>Program Details:</b> <?php _e($programs)?> in <?php _e($country_list)?><br />
											<input type="hidden" name="item_id" id="item_id_<?php _e($item_id)?>" value="<?php _e($team->get_team())?>">
											<input type="hidden" name="item_quan" id="item_quan_<?php _e($item_id)?>" value="1">
											<input type="hidden" name="add_on" id="add_on_<?php _e($item_id)?>" value="<?php _e($item_id)?>">
											<input type="hidden" name="item_type" id="item_type_<?php _e($item_id)?>" value="rsvp">
											<input type="hidden" name="item_cost" id="item_cost_<?php _e($item_id)?>" value="<?php _e($team->trip_cost())?>">
										</div>

										<div class="quantity medium-3 columns">
											 <b>Quantity:</b><br /> 

												   <input type="number" 
													   class="set_join"
													   name="quantity_<?php _e($item_id)?>" 
													   id="quantity_<?php _e($item_id)?>" 
													   maxlength="4"
													   size="4"
													   min="<?php _e($maximum==0 ? 0 : 1)?>"
													   max="<?php _e(max(0, $team->value('max') - $rsvp->count_rsvp($item_id)))?>"
													   step='1'
													   value="<?php _e($display_items)?>"
													   style="width: 5em; display: inline-block;"
													   required>

											<?php
											//$total_items += $display_items;
											if($maximum == 0)
											{
												echo "<BR /><span class='small'>Sold Out!</span>";	
											}
											?>

										</div>

										<div class="team-totals medium-3 columns">
											<b>Trip Cost:</b> <?php _e($NO_FINANCE ? 'PAID' : $cart->Money($item_data['cost']))?><br />
										</div>

										<?php

										if($NO_FINANCE)
										{
											$cfl_remove += $item_data['cost'] * $item_data['quantity'];
										}

										break;

									case 'fee':
										//$count_fees += $item_data['quantity'];
										//$line_sub_total += ($item_data['cost'] * $item_data['quantity']);
										?>
										<div class="details medium-6 columns">
											<span class="small">+ Non-refundable Application Fee <div class="tooltip-wrapper"><i class="fa fa-info-circle" aria-hidden="true"></i><div class="tooltip-text">This one-time application fee does not apply to your program fee and is nonrefundable. The applicable deposits and any future payments made will apply directly to your program fee and may be refundable. Please refer to our refund policy.</div></div></span>
										</div>

										<div class="team-totals medium-6 columns">
											<span class="small"><strong>App Fee</strong></span>: <?php _e($NO_FINANCE ? 'PAID' : $cart->Money($item_data['cost']))?><br />
										</div>
										<?php

										if($NO_FINANCE)
										{
											$cfl_remove += $item_data['cost'] * $item_data['quantity'];
										}
											
										break;

									case 'add-on':
										$a_team = new TEAM();
										$a_finance = new FINANCE();

										$order_item = $a_finance->fetch_item($main_item_id);
										$a_team->set_team($order_item['item_id']);

										$add_on = get_post($item_id, ARRAY_A);
										$add_on_data = get_fields($item_id);

										if($add_on_data['pay_type'] == "POS" || $add_on_data['delivery'] == "Direct Ship")
										{
											$add_ons_due_now += $item_data['cost'];
										}

										$trip_id = $item_id;
										?>
										<div class="details large-12 medium-12 columns">
											<h6><B>Program:</B> <?php _e($a_team->value('hash'))?><BR></h6>
										</div>
										<div class="details medium-6 columns">
											
												
												<?php echo $add_on['post_title']?>
											
										</div>
										<div class="quantity medium-3 columns">
											 <b>Quantity:</b><br /> 

												   <input type="number" 
													   class="set_join"
													   name="quantity_<?php _e($item_id)?>" 
													   id="quantity_<?php _e($item_id)?>" 
													   maxlength="4"
													   size="4"
													   min="1"
													   max="100"
													   step='1'
													   value="<?php _e($item_data['quantity'])?>"
													   style="width: 5em; display: inline-block;"
													   required>

										</div>
										<div class="team-totals medium-3 columns">
											$ <?php _e($cart->Money($item_data['cost']))?><br />
										</div>

										<input type="hidden" name="item_id" id="item_id_<?php _e($item_id)?>" value="<?php _e($main_item_id)?>">
										<input type="hidden" name="item_quan" id="item_quan_<?php _e($item_id)?>" value="1">
										<input type="hidden" name="add_on" id="add_on_<?php _e($item_id)?>" value="<?php _e($item_id)?>">
										<input type="hidden" name="item_type" id="item_type_<?php _e($item_id)?>" value="add-on">
										<input type="hidden" name="item_cost" id="item_cost_<?php _e($item_id)?>" value="<?php _e($item_data['cost'])?>">
										<?php
											
										break;
										
									case 'cap':
										?>

										<div class="team-totals medium-3 medium-offset-9 columns">
											<B>CAP Discount: </B><?php _e($cart->Money($item_data['cost']))?><br />
										</div>
										<?php

										//
										//Find CAP Code
										$cap_code = get_user_meta($item_id, '_cmd_comm_ambassador_cap_code', true);

										break;

									case 'coupon':
										?>
<!-- 										<div class="details medium-6 columns">
											&nbsp;
										</div>
										<div class="quantity medium-3 columns">
											&nbsp;
										</div> -->
										<div class="team-totals medium-3 medium-offset-9 columns">
											<B>Coupon: </B><?php _e($cart->Money($item_data['cost']))?><br />
										</div>
										<?php

										//
										//Find Coupon Code
										$coupon_info = get_fields($item_id);
										$coupon = $coupon_info['code'];
								
										break;

									default:
										?>
										<div class="details medium-6 columns">
											<span class="small">+ Shouldn't see this yet</span>
										</div>
										<div class="quantity medium-3 columns">
											&nbsp;
										</div>
										<div class="team-totals medium-3 columns">
											$ <?php _e($item_data['cost'])?><br />
										</div>
										<?php
										break;
										
								}	// <!-- end switch -->

							}	// <!-- end item -->

						}	// <!-- end type -->

							?>					
							<div class="subtotal row collapse">
								<div class="details medium-9 columns">
									<a class="empty" name="empty_<?php _e($trip_id)?>" id="empty2_<?php _e($trip_id)?>">&times; <span class="small delete_rows">Delete</span></a>
								</div>

								<div class="team-totals medium-3 columns">
									<!-- <span><b>Subtotal:</b> $<?php //_e(number_format($line_sub_total,2) )?></span> -->
								</div>
							</div>
						
						</div>
						<?php    
								
					}	//End of Cart
					
					?>
					<!--</div> <!-- End team cart -->
					</div> <!-- End large-12 class -->
					<div class="row medium-collapse">
						<div class="large-9 columns">
							<?php
							if( $cart->Count() && !$NO_FINANCE)
							{
								?>
								<div class="rep-code">
									<p>Do you have a Coupon?</p> 
									<form name="data_cpn" method="POST" enctype="multipart/form-data" action="/cart">
										<input type="hidden" name="method_cpn" id="method_cpn" value="Return">
										<input type="hidden" name="fee_count" id="fee_count" value="<?php echo $cart->Count('Fees') ?>">
										<!--2018-03-08 DJT Added hidden coupon since 2nd form -->
										<!--2018-03-08 DJT Changed coupon id to coupon_primer since 2nd form -->
										<input type="hidden" name="repcode" id="repcode" <?php _e(!empty($cap_code) ? 'value="' . $cap_code . '"' : '')?>>
										<input type="text" name="coupon" id="coupon_prime" <?php _e( !empty( $coupon ) ? 'value="' . $coupon . '"' : '')?>>
										<input type="submit" name="rep_check" id="cpn_submit" value="Apply Coupon">

										<?php
										if(!empty( $coupon ) )
										{
											echo "<INPUT type='submit' value='X' onclick='javascript:clear_coupon()'>";
										}
										if( !empty( $_REQUEST['member_id']) )
										{
											echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . $_REQUEST['member_id'] . "\">";
										}
										if( is_user_logged_in() && empty( $_REQUEST['member_id'] ) )
										{
											echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . get_current_user_id() . "\">";
										}
										?>
									</form>
								</div>
								<?php
							}
							?>

							<?php
							if( $cart->Count('reservation')  && !$NO_FINANCE)
							{
								?>
								<div class="rep-code">
									<p>Do you have a CAP code?</p> 
									<form name="data_cap" method="POST" enctype="multipart/form-data" action="/cart">
										<input type="hidden" name="method_cap" id="method_capn" value="Return">
										<input type="hidden" name="fee_count" id="fee_count" value="<?php echo $cart->Count('Fees') ?>">
										<!--2018-03-08 DJT Changed repcode id to repcode_prime since 2nd form -->
										<input type="text" name="repcode" id="repcode_prime" <?php _e(!empty($cap_code) ? 'value="' . $cap_code . '"' : '')?>>
										<!--2018-03-08 DJT Added hidden coupon since 2nd form -->
										<input type="hidden" name="coupon" id="coupon" <?php _e( !empty( $coupon ) ? 'value="' . $coupon . '"' : '')?>>
										<input type="submit" name="rep_check" value="Apply CAP Code">

										<?php
										if( !empty( $_REQUEST['member_id'] ) )
										{
											echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . $_REQUEST['member_id'] . "\">";
										}
										if( is_user_logged_in() && empty( $_REQUEST['member_id'] ) )
										{
											echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . get_current_user_id() . "\">";
										}

										?>
									</form>
								</div>
							<?php
							}
					
							if($NO_FINANCE)
							{
                                ?>
                                <div class="rep-code">
<!--                                    <form name="data_cap" method="POST" enctype="multipart/form-data" action="/checkout">-->
<!--                                        <input type="submit" name="rep_check" value="Continue">-->
<!--                                        --><?php
//                                        if( !empty( $_REQUEST['member_id'] ) )
//                                        {
//                                            echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . $_REQUEST['member_id'] . "\">";
//                                        }
//                                        if( is_user_logged_in() && empty( $_REQUEST['member_id'] ) )
//                                        {
//                                            echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . get_current_user_id() . "\">";
//                                        }
//
//                                        ?>
<!--                                    </form>-->
                                </div>
                                <?php
							}

							?>
						</div>
						<div class="large-3 columns">
						<div class="totals row">
							<?php
								$sub_total = $cart->Total() - $cart->Total('CAP')  - $cart->Total('coupon') - $cfl_remove;
							?>

							<b>Subtotal:</b> $<?php _e( number_format($sub_total, 2))?><br />
							<?php
							if(!empty($cap_code) ||  !empty($coupon))
							{
								?>
								<b>Discount: </b><?php _e($cart->Money($cart->Total('CAP') + $cart->Total('Coupon')))?>
								<?php
							}
							?>
							<hr />
							<h4><b>Total:</b> <?php _e($cart->Money($cart->Total() - $cfl_remove))?></h4>

						</div>
					</div>
						<form name="data" method="POST" enctype="multipart/form-data" action="/cart">
							<input type="hidden" name="method" id="method" value="Return">
							<input type="submit" name="rep_check" value="submit" style="display:none">
							<?php
							if(!empty($_REQUEST['member_id']))
							{
								echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . $_REQUEST['member_id'] . "\">";
							}
							if(is_user_logged_in() && empty($_REQUEST['member_id']))
							{
								echo "<input type=\"hidden\" name=\"member_id\" id=\"member_id\" value=\"" . get_current_user_id() . "\">";
							}
							?>
						</form>
					</div>
					<?php
					if($cart->Count() > 0)
					{
						?>
						<div id="payment" class="row" data-equalizer data-equalize-on="large">
						<?php

						if(	($cart->Count('Reservation') > 0 && $cart->Total() > 0)
						   ||
						    ($cart->Count('Add-on') > 0 && $cart->Total('Add-on') > $add_ons_due_now))
						{
						?>
							<div id="deposit" class="panel" data-equalizer-watch>
							<?php
							if(	$cart->Count('Reservation') > 0 && ($cart->Total('Deposit') + $cart->Total('Fee') + $add_ons_due_now - $cfl_remove > 0))
							{
								?>
								<div>
									<h3>Pay Only <?php _e($cart->Money($cart->Total('Deposit') + $cart->Total('Fee') + $add_ons_due_now))?> Today</h3>
									<span>This payment includes a non-refundable application fee and applicable trip deposits. The remaining balance is due 45 days before departure.</span>
									<p><b><?php echo $due_block?></b></p>	
								</div>
								<a class="button" name="deposit" onclick="javascript:paynow('deposit')">Pay Deposit & Fees Only</a>
								<?php
							}
							elseif ($add_ons_due_now > 0)
							{
								?>
								<div>
									<h3><?php echo $cart->Money($add_ons_due_now);?> Due Today</h3>
									<p><b><?php echo $due_block?></b></p>	
								</div>
								<a class="button" name="deposit" onclick="javascript:paynow('deposit')">Finish Checkout</a>
								<?php
							}
							else
							{
								?>
								<div>
									<h3>$0.00 Due Today</h3>
									<p><b><?php echo $due_block?></b></p>	
								</div>
								<a class="button" name="deposit" onclick="javascript:paynow('deposit')">Finish Checkout</a>
								<?php
							}
							?>
							</div>
						<?php
						}

						if(	!$NO_FINANCE &&
                            ( $cart->count('Reservation') > 0
                                ||
						     ($cart->Count('Add-on') > 0 && $cart->Total('Add-on') > $add_ons_due_now)))
						{
						?>
							<div class="divider" data-equalizer-watch>
								<h4>OR</h4>
							</div>
						<?php
						}

						if($cart->count() > 0 && !$NO_FINANCE)
						{
							?>
							<div id="balance" class="panel" data-equalizer-watch>
								<?php
								if($cart->count() > 0)
								{
								?>
									<div>
										<h3>Pay <?php _e($cart->Money($cart->total()))?> Today<br /></h3>
										<p><span><?php echo ($cart->count('Reservation') ? "Applicable fees + trip total" : "") ?></span></p>
									</div>

									<?php
									if($cart->total() > 0)
									{
										?>
										<a class="button" name="total" onclick="javascript:paynow('balance')">Pay Balance in Full Now</a>
										<?php
									}
									else
									{
										?>
										<a class="button" name="total" onclick="javascript:paynow('balance')">Finish Checkout</a>
										<?php
									}
								}
								else
								{
								?>
									<div>
										<h3>Your items are sold out.</h3>
									</div>
									<a class="button" name="clear" onclick="javascript:clear_cart()">Click here to clear your cart and start over</a>
								<?php
								}

								?>
							</div>


						</div>
						<?php
											
						}
                        if ($NO_FINANCE){
                            ?>
                            <a style="float: right" class="button" name="total" id="total_checkout" onclick="javascript:paynow('balance')">Finish Checkout</a>
                            <?php
                        }

					}
				}
				else
				{
					echo "<h5>You have nothing in your cart.</h5><a class='button' href='", get_permalink(4411), "'> Click here and find a Program to join now!</a>";

				}
				?>

			</div> 	<!-- end Team cart row -->  

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content --> 


<script>
	function paynow(method)
	{
		var methamt = document.getElementById('method');
		methamt.value = method;
		
		document.data.action = "/checkout";
		document.data.submit();			
	}

	function clear_cart()
	{
		var methamt = document.getElementById('method');
		methamt.value = 'clear';
		
		document.data.action = "/cart";
		document.data.submit();			
	}
	
	function clear_coupon()
	{
		var btn = document.getElementById('cpn_button');
		var fld = document.getElementById('coupon');
		fld.value = '';
		btn.submit();
	}

    var total_variables = document.getElementsByClassName("totals");
	var total_rows = document.getElementsByClassName("delete_rows");
    if (total_variables.length){
        var total_variable = total_variables[0];
        for (let i = 0; i < total_variable.children.length; i++) {
            if (total_variable.children[i].tagName == 'H4'){
                var content_total = total_variable.children[i].innerHTML;
                var pos = content_total.indexOf('$0.00');
                if((pos+1) !== 0) {
                    if (total_rows.length > 1){
                        alert("You have selected more then one item. Please delete all of them from the cart and add one item again.")
                        document.getElementById('deposit').style.display = 'none';
                        document.getElementById('total_checkout').style.display = 'none';
                    }
                    else{
                        var val = document.getElementsByClassName('set_join')[0].value;
                        if (val > 1){
                            var field = document.getElementsByClassName('set_join')[0];
                            var index = jQuery(field).attr('id').substr(jQuery(field).attr('id').lastIndexOf("_")+1);

                            var item_id = jQuery('#item_id_' + index).val();
                            var add_on = jQuery('#add_on_' + index).val();
                            var item_type = jQuery('#item_type_' + index).val();
                            var item_cost = jQuery('#item_cost_' + index).val();

                            var data =
                                {
                                    'action': 'cmd_cart_set',
                                    'item_id': item_id,
                                    'item_quan': '1',
                                    'add_on': add_on,
                                    'item_type': item_type,
                                    'item_cost': item_cost,
                                };

                            jQuery.ajax(
                                {
                                    type: 'POST',
                                    url: ajax_path.ajax_url,
                                    data: data,
                                    success:function(results)
                                    {
                                        var url = '/cart';
                                        var form = $('<form action="' + url + '" method="post">' +
                                            '<input type="text" name="cart_refer" value="' + window.location + '" />' +
                                            '</form>');
                                        $('body').append(form);
                                        form.submit();
                                        //window.location = "/cart?refer=" + window.location;
                                    },
                                    error:function(xml, status, error)
                                    {
                                        alert('Error: ' + error);
                                    }
                                });
                        }
                        else{
                            document.getElementsByName("deposit")[0].click();
                        }
                        // document.getElementsByClassName('set_join')[0].setAttribute("value", "1");
                        //
                    }
                }
            }
        }
    }

    // console.log(total_variable);
</script>

<?php get_footer(); ?>