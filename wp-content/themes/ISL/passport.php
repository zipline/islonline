<?php
/*
Template Name: Passport
*/
?>
<?php wp_head(); ?>
<style type="text/css">
body {
	background: #ffffff;
}
</style>
<?php
//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "rsvp.class.php");

$team = new TEAM();
$rsvp = new RSVP();

$team->set_team($rsvp->team($_GET['rsvp']));

?>

   	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>        

    <div id="content">

		<?php
        ?>

    </div> <!-- end #content --> 