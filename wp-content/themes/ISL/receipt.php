<?php
/*
Template Name: Receipt
*/
?>
<?php
//
// action = checkout (initial rsvp puschase), donate (donation to an rsvp), payment (payment)
// method = balance (cost and app fees), deposit (deposits and app fees), defined (defined amount)
//
//
// Test Data
// Account Number: 4032039649918960
// Card Type: Visa
// Expiration: 01/2021
// CVV2: Any

//
//Core variables
$actions = array( 'checkout', 'donate', 'payment' );
$methods = array( 'balance', 'deposit', 'defined' );
$create_user = false;

//
//Make sure we should be here, otherwise get out of here
if(empty($_POST['action']) || !in_array($_POST['action'], $actions) || empty($_POST['method']) || !in_array($_POST['method'], $methods))
{   
 	wp_safe_redirect( wp_get_referer() );
}
else
{
	$method = $_POST['method'];
	$action = $_POST['action'];
}


//
// Include required library files.
require_once CC_CLASSES_DIR . "paypal/config.php";
require_once CC_CLASSES_DIR . "paypal/autoload.php";
require_once CC_CLASSES_DIR . "paypal/PayPal/PayPal.php";
require_once CC_CLASSES_DIR . "paypal/PayPal/PayFlow.php";
require_once CC_CLASSES_DIR . "cart.class.php";
require_once CC_CLASSES_DIR . "finance.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "user.class.php";
require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "program.class.php";
require_once CC_CLASSES_DIR . "itinerary.class.php";
require_once CC_CLASSES_DIR . "email.class.php";
require_once CC_CLASSES_DIR . "docs.class.php";
require_once "./wp-admin/includes/user.php";


//
//2018-02-22 Moved from below to authenticate user prior to authorization
if(!is_user_logged_in())
{
	$user = new USER();

	//
	//Create a new user
	$user_info['user_login'] = $user->temp_user_name();
	$user_info['user_pass'] = $_POST['lastname'];
	$user_info['first_name'] = $_POST['firstname'];
	$user_info['last_name'] = $_POST['lastname'];
	$user_info['user_email'] = $_POST['email'];

	//
	//See if that emai8l address is in use...
	$try_user = get_user_by( 'email', $_POST['email']);
	
	if($action == "donate" && $try_user)
	{
		//
		//Set the user without logging them in
		wp_set_current_user($try_user->ID);
		$current_user_id = $try_user->ID;	
	}
	elseif ($try_user)
	{
		//
		//Error, user is not logged in and has an account
		$current_user_id = 0;
	}
	else
	{
		//
		//2018-02-22 DJT Moved do_action to after transaction authorized
		$new_user = $user->create_user($user_info);
		//do_action('cmd_custom_register', $action, $user_info['user_login'], $user_info['first_name'], $user_info['last_name'], $user_info['user_email']);
	}
	
	if(!empty($new_user))
	{

		if($action=="checkout")
		{
			add_user_meta( $new_user, '_birthdate', 'field_57249f7a0c63c' );
			add_user_meta( $new_user, 'birthdate', $_POST['birthdate']);
			add_user_meta( $new_user, '_mailing_address_line_1', 'field_5721d2078b820' );
			add_user_meta( $new_user, 'mailing_address_line_1', $_POST['addr1']);
			add_user_meta( $new_user, '_mailing_address_line_2', 'field_5721d2188b821' );
			add_user_meta( $new_user, 'mailing_address_line_2', $_POST['addr2']);
			add_user_meta( $new_user, '_city', 'field_5721d2338b822' );
			add_user_meta( $new_user, 'city', $_POST['city']);
			add_user_meta( $new_user, '_state', 'field_5721d2418b823' );
			add_user_meta( $new_user, 'state', $_POST['state']);
			add_user_meta( $new_user, '_zip_code', 'field_5721d3488b824' );
			add_user_meta( $new_user, 'zip_code', $_POST['zip'] );

			if(isset($_POST['phone']))
			{
				add_user_meta( $new_user, '_primary_phone', 'field_5721d1c98b81e');
				add_user_meta( $new_user, 'primary_phone', preg_replace("/[^0-9]/", '', $_POST['phone']));
			}
		}

		add_user_meta( $new_user, '_banner', 'field_56fc1b73dff8c' );
		add_user_meta( $new_user, 'banner', site_url('/wp-content/default-pics/banner.jpg'));
		add_user_meta( $new_user, '_local_avatar', 'field_56f882d3ccb2a' );
		add_user_meta( $new_user, 'local_avatar', site_url('/wp-content/default-pics/avatar.png'));

		//
		//Log int he new user
		wp_set_current_user($new_user, $user_info['user_login']);
		wp_set_auth_cookie($new_user);
		do_action('wp_login', $user_info['user_login']);		
		$current_user_id = $new_user;
	}

	if(empty($current_user_id))
	{
		//
		//Error, user cannot be created for some reason
		?>
		<?php get_header(); ?>

		<div class='no-banner'></div>
		<div id='content'>
			<div id='inner-content' class='row'>
				<main id='cart' class='large-10 medium-12 small-centered columns' role='main'> 

					<h2>Processing Error</h2>
					<p>It appears your email address is already in use for an account.</p>
					<p>Please <a href='<?php echo wp_login_url()?>'>log in</a> and try again</p>
					<br>
					<p>The items have not been removed from your cart and should follow you to your account after login.</p>

					<?php
					if(isset($rsvp) && !empty($rsvp->get_rsvp_id()) && $action == "donate")
					{
						?>
						<a href='/sendme/<?php echo $rsvp->get_rsvp_id()?>'><i class='fa fa-long-arrow-left aria-hidden="true"'></i>&nbsp;Return to Donation</a>
						<?php
					}
					else
					{
						?>
						<a href='/checkout'><i class='fa fa-long-arrow-left aria-hidden="true"'></i>&nbsp;Return to Checkout</a>
						<?php
					}
					?>
					
				</main><!-- End Main -->
			</div> <!-- End inner-content -->
		</div><!-- End Content -->
		
		<?php get_footer(); ?>

		<?php
		exit;
	}
}
else
{
	$current_user_id = $_REQUEST['member_id'];
}


//
//Determine amount to process based upon action
if( $action == "checkout" )
{
	//
	//Fetch the cart and validate it
	$cart = new CART();
	$cart->validate();


	//
	//Amount to process...
	switch($method)
	{
		case 'balance':
			$process_amount = $cart->Total();	
			break;
		case 'deposit':
			$cfl_remove = 0;
			
			foreach( $cart->Processed() as $main_item_id => $item_types )
			{
				foreach( $item_types as $item_type => $item )
				{
					foreach( $item as $item_id => $item_data )
					{
						switch( $item_type )
						{
							case 'reservation':
								$team = new TEAM();
								$team->set_team($item_id);

								if($team->value('no_finances') == 1)
								{
									$cfl_remove += $item_data['cost'] * $item_data['quantity'];
								}
								break;

							case 'fee':
								$team = new TEAM();
								$team->set_team($item_id);

								if($team->value('no_finances') == 1)
								{
									$cfl_remove += $item_data['cost'] * $item_data['quantity'];
								}
								break;
						}
					}
				}
			}

			$process_amount = $cart->Total( 'deposit' ) + $cart->Total( 'fee' ) - $cfl_remove;	
			break;
		case 'defined':
			$process_amount = 0;
			break;
		default:
			$process_amount = 0;
	}
	
	//
	//Minor?
	if( isset($_POST['minoragree']) && $_POST['minoragree']=='on' )
	{
		$docs = new DOCS();
		$minor_docs = $docs->minor_travel_form();
	}
}
else
{

	$rsvp = new RSVP();
	$finance = new FINANCE();
	
	$rsvp->set_id_by_team_user( $_REQUEST['team_id'], $_REQUEST['member_id'] );
	$rsvp_data = $rsvp->fetch_rsvp();

	$item = $finance->fetch_item( $rsvp_data['order_item_id'] );
	$order_id = $item['order_id'];
	
	$add_ons = $finance->add_ons( $rsvp_data['order_item_id'] );
	$all_items[] = $rsvp_data['order_item_id'];

	$cost = $item['price'];
	foreach( $add_ons as $index => $itm )
	{
		$all_items[] = $itm['id'];
		$cost += $itm['price'];
	}		

	$pays = 0;
	foreach( $finance->payments( $all_items ) as $payments )
	{
		$paid_amount[$payments['order_item_id']] = $payments['amount'];
		$pays += $payments['amount'];	
	}
	

	//
	//Amount to process...
	switch($method)
	{
		case 'balance':
			$process_amount = floatval( preg_replace('/[0-9.]*/', '', ($cost - $pays)) );	
			break;
		case 'deposit':
			$process_amount = 0;
			break;
		case 'defined':
			$process_amount = floatval( preg_replace('/0-9.]*/', '', $_POST['alt_pay'] ));
			break;
		default:
			$process_amount = 0;
	}
}


//
// Make credit transaction or set variables if amoutn to process is 0
if($process_amount > 0)
{
	//
	// Create PayPal object.
	$PayPalConfig = array(
						'Sandbox' => $sandbox,
						'APIUsername' => $payflow_username, 
						'APIPassword' => $payflow_password, 
						'APIVendor' => $payflow_vendor, 
						'APIPartner' => $payflow_partner, 
						'PrintHeaders' => $print_headers, 
						'LogResults' => $log_results,
						'LogPath' => $log_path,
						);

	//
	//Instantiate PayPal object
	$PayPal = new angelleye\PayPal\PayFlow($PayPalConfig);

	//
	//Set basic credit card details				
	$CCDetails = array(
						'BILLTOFIRSTNAME' => !empty($_POST['firstname']) ? $_POST['firstname'] : '',
						'BILLTOLASTNAME' => !empty($_POST['lastname']) ? $_POST['lastname'] : '',
						'TENDER' => 'C',					 											// Required
						'TRXTYPE' => 'S',					 											// Required
						'ACCT' => $_POST['acctnum'], 													// Required
						'EXPDATE' => date('m', strtotime($_POST['exp_month'])) . substr($_POST['exp_year'],2,2),  // Required
						'CVV2' => $_POST['cvv2'],
						'AMT' => number_format($process_amount,2)										// Required
					);
	
	$PayPalResult = $PayPal->ProcessTransaction($CCDetails);

	$trans_data = 	array('amount' => empty($PayPalResult['AMT']) ? -1 : $PayPalResult['AMT'],
						  'form' => $_POST['card'],
						  'response' => $PayPalResult['RESPMSG'],
						  'transaction_id' => empty($PayPalResult['PNREF']) ? "No Ref" : $PayPalResult['PNREF'],
						  'raw_request' => $PayPalResult['RAWREQUEST'],
						  'raw_response' => $PayPalResult['RAWRESPONSE'],
						  'ip_address' => $_SERVER['REMOTE_ADDR'],
						  'user_agent' => $_SERVER['HTTP_USER_AGENT'],
						  'pp_trans_id' => empty($PayPalResult['PPREF']) ? "No Ref" : $PayPalResult['PPREF']
						  );
}
else
{
	$trans_data = array('amount' => 0,
						'form' => 'No Payment',
						'response' => 'Approved',
						'transaction_id' => date('Ymdhis'),
						'raw_request' => 'No Request Data,  No Payment Due',
						'raw_response' => 'Approved with 0 down',
  					    'ip_address' => $_SERVER['REMOTE_ADDR'],
						'user_agent' => $_SERVER['HTTP_USER_AGENT'],
						'pp_trans_id' => date('Ymdhis')
						);
						
	$PayPalResult['RESPMSG'] = "Approved";
}


//
//2018-02-22 DJT Moved to before transaction to make sure we authenticate a user first.
//
//Check to see if we have a logged in user, if not, then we need to create one
/*
if(!is_user_logged_in())
{
	$user = new USER();

	//
	//Create a new user
	$user_info['user_login'] = $user->temp_user_name();
	$user_info['user_pass'] = $_POST['lastname'];
	$user_info['first_name'] = $_POST['firstname'];
	$user_info['last_name'] = $_POST['lastname'];
	$user_info['user_email'] = $_POST['email'];

	//
	//See if that emai8l address is in use...
	$try_user = get_user_by( 'email', $_POST['email']);
	
	if($action == "donate" && $try_user)
	{
		//
		//Set the user without logging them in
		wp_set_current_user($try_user->ID);
		$current_user_id = $try_user->ID;	
	}
	elseif ($try_user)
	{
		//
		//Error, user is not logged in and has an account
		$current_user_id = 0;
	}
	else
	{
		$new_user = $user->create_user($user_info);
		do_action('cmd_custom_register', $action, $user_info['user_login'], $user_info['first_name'], $user_info['last_name'], $user_info['user_email']);
	}
	
	if(!empty($new_user))
	{

		if($action=="checkout")
		{
			add_user_meta( $new_user, '_birthdate', 'field_57249f7a0c63c' );
			add_user_meta( $new_user, 'birthdate', $_POST['birthdate']);
			add_user_meta( $new_user, '_mailing_address_line_1', 'field_5721d2078b820' );
			add_user_meta( $new_user, 'mailing_address_line_1', $_POST['addr1']);
			add_user_meta( $new_user, '_mailing_address_line_2', 'field_5721d2188b821' );
			add_user_meta( $new_user, 'mailing_address_line_2', $_POST['addr2']);
			add_user_meta( $new_user, '_city', 'field_5721d2338b822' );
			add_user_meta( $new_user, 'city', $_POST['city']);
			add_user_meta( $new_user, '_state', 'field_5721d2418b823' );
			add_user_meta( $new_user, 'state', $_POST['state']);
			add_user_meta( $new_user, '_zip_code', 'field_5721d3488b824' );
			add_user_meta( $new_user, 'zip_code', $_POST['zip'] );

			if(isset($_POST['phone']))
			{
				add_user_meta( $new_user, '_primary_phone', 'field_5721d1c98b81e');
				add_user_meta( $new_user, 'primary_phone', preg_replace("/[^0-9]/", '', $_POST['phone']));
			}
		}

		add_user_meta( $new_user, '_banner', 'field_56fc1b73dff8c' );
		add_user_meta( $new_user, 'banner', site_url('/wp-content/default-pics/banner.jpg'));
		add_user_meta( $new_user, '_local_avatar', 'field_56f882d3ccb2a' );
		add_user_meta( $new_user, 'local_avatar', site_url('/wp-content/default-pics/avatar.png'));

		//
		//Log int he new user
		wp_set_current_user($new_user, $user_info['user_login']);
		wp_set_auth_cookie($new_user);
		do_action('wp_login', $user_info['user_login']);		
		$current_user_id = $new_user;
	}

	if(empty($current_user_id))
	{
		//
		//Error, user cannot be created for some reason
		?>
		<?php get_header(); ?>

		<div class='no-banner'></div>
		<div id='content'>
			<div id='inner-content' class='row'>
				<main id='cart' class='large-10 medium-12 small-centered columns' role='main'> 

					<h2>Processing Error</h2>
					<p>It appears your email address is already in use for an account. Please log in and try again</p>
					<p>The items have not been removed from your cart.</p>

					<?php
					if(isset($rsvp) && !empty($rsvp->get_rsvp_id()) && $action == "donate")
					{
						?>
						<a href='/sendme/<?php echo $rsvp->get_rsvp_id()?>'><i class='fa fa-long-arrow-left aria-hidden="true"'></i>&nbsp;Return to Donation</a>
						<?php
					}
					else
					{
						?>
						<a href='/checkout'><i class='fa fa-long-arrow-left aria-hidden="true"'></i>&nbsp;Return to Checkout</a>
						<?php
					}
					?>
					
				</main><!-- End Main -->
			</div> <!-- End inner-content -->
		</div><!-- End Content -->
		
		<?php get_footer(); ?>

		<?php
		exit;
	}
}
else
{
	$current_user_id = $_REQUEST['member_id'];
}
*/
	
//
//Hold on just a moment
sleep(1);

//
//Write Transaction Record
$finance = new FINANCE();
$finance->new_transaction($trans_data, $method, $action);	

//
//If our request was approved.... [ACK] == Success
if(!empty($PayPalResult['RESPMSG']) && $PayPalResult['RESPMSG'] === 'Approved')
{

	//
	//2018-02-22 DJT Moved create_user to here, fires if user was created and transaction processed.
	//2018-03-07 DJT Changed to $new user and removed $create_user from above.
	if(!empty($new_user))
	{
		do_action('cmd_custom_register', $action, $user_info['user_login'], $user_info['first_name'], $user_info['last_name'], $user_info['user_email']);
	}

	$message = '';

	//
	//Transition items from cart to the order
	if($action == "checkout")
	{
		$finance->move_CartToOrder( $cart );
		$items = 'All';
		$cart->Clear();	

		//
		//Write in Chatterbox information
		if(!empty($_REQUEST['team_ids']))
		{
			$team = new TEAM();
			foreach($_REQUEST['team_ids'] as $tid)
			{
				do_action('cmd_checkout', $tid, $current_user_id);

				if(!empty($minor_docs))
				{
					$team->set_team($tid);

					$docs->post_id($team->value('post'));
					$docs->set_user($current_user_id);
					$docs->set_user_list( array($minor_docs) );
				}
			}
			unset($team);
		}
	}
	else
	{
		$finance->OrderId($order_id);
		$finance->Items($all_items);
		$items = 'List';
		
		if($action == 'donate')
		{
			$message = !empty($_POST['note']) ? $_POST['note'] : '';	
		}
	}

	//
	//Note payments
	if(isset($_POST['anonymous']) && $_POST['anonymous'] == "yes")
	{
		$action = "anonymous";
	}
	$finance->apply_payments($method, $action, $trans_data['amount'], $items, $message);

	//
	//Build receipt and email
	$receipt = $finance->show_receipt($finance->get_trans_id(), $action, $method);

	$email = new EMAIL();
	$email->to($_POST['firstname'] . ' ' . $_POST['lastname'], $_POST['email']);
	$email->from('ISL Receipt', 'donotreply@islonline.org');
	$email->subject('ISL Receipt');
	$email->message($receipt);
	$email->send();
	?>	

	<?php get_header(); ?>

	<div class='no-banner'></div>
	<div id='content'>
		<div id='inner-content' class='row'>
			<main id='cart' class='large-10 medium-12 small-centered columns' role='main'> 
				<div class='team-cart row'>
					<?php echo $receipt;?>

					<hr />

					<?php
					if($action == "checkout")
					{
						echo "<br />";
						echo "<input class='button primary' type='button' value='Go to your MyISL page & complete your MyISL Volunteer Profile' onclick='window.location.href = \"/my-isl/\"'>";
					}
					if($action == "donate")
					{
						echo "<br />";
						echo "<input class='button primary' type='button' value='Go back to the Donation Page' onclick='window.location.href = \"/sendme/" . $rsvp->get_rsvp_id() . "\"'>";
					}
					?>
		
				</div> <!-- End team-cart row-->   
			</main><!-- End Main -->
		</div> <!-- End inner-content -->
	</div><!-- End Content -->
	
	<!-- 2018-02-27 DJT Added cookie clearing here since class isn't working -->
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	<script>
		jQuery(document).ready(function()
		{
			Cookies.remove('islonline_org_cartcode', { path: '/', domain: 'islonline.org'});	
			Cookies.remove('islonline_org_cartitems', { path: '/', domain: 'islonline.org'});	
		})
	</script>

	<?php
}
else
{ 
	//
	//2018-02-22 DJT Delete user account if user was created and transaction failed.
	if(!empty($new_user))
	{
		wp_logout();
		wp_delete_user ( $current_user_id );
	}

	//
	//Error handler
	switch($PayPalResult['HOSTCODE'])
	{
		case '10001':
			$error = "The supplied credit card number, expiration date, or cvv2 code is not valid.<BR>";
			break;
        case '10510':
            $error = "The credit card type is not supported. Try another card type.<BR>";
            break;
        case '10535':
            $error = "This transaction cannot be processed. Please enter a valid credit card number and type.<BR>";
            break;
        case '10606':
            $error = "Transaction cannot be processed. Please try another card.<BR>";
            break;
        case '10764':
            $error = "This transaction cannot be processed at this time. Try again later.<BR>";
            break;
		case '11519':
			$error = "The amount to process is invalid: $" . $process_amount;
			break;
		case '15004':
            $error = "This transaction cannot be processed. Please enter a valid Credit Card Verification Number.<BR>";
			break;
		case '15005':
            $error = "Payment has been declined by the processing bank. Please contact the bank that issued the card for more details.<BR>";
			break;
		case '15006':
            $error = "Payment has been declined by the processing bank. Please contact the bank that issued the card for more details.<BR>";
			break;
		case '15007':
            $error = "Payment has been declined by the processing bank. Please contact the bank that issued the card for more details.<BR>";
			break;
		default:
			$error = "A processing error has occurred.<BR>";
//            print_r($PayPalResult);
//            echo '<br>';
//			$error = "Payment has been declined by the processing bank. Please contact the bank that issued the card for more details.<BR>";
	}
	?>
	
	<?php get_header(); ?>

	<div class='no-banner'></div>
	<div id='content'>
		<div id='inner-content' class='row'>
			<main id='cart' class='large-10 medium-12 small-centered columns' role='main'> 

				<h2>Processing Error</h2>
				<p>There were problems processing your credit card. The following errors were found:</p>
				<p><strong>Error(s):</strong><br>
				<em><?php echo $error; ?></em>
				<?php //echo var_dump($PayPalResult['RESPMSG']); ?>
<!--				<p>The items have not been removed from your cart. Please try again shortly.</p>-->

				<?php
				if(isset($rsvp) && !empty($rsvp->get_rsvp_id()) && $action == "donate")
				{
					?>
					<a href='/sendme/<?php echo $rsvp->get_rsvp_id()?>'><i class='fa fa-long-arrow-left aria-hidden="true"'></i>&nbsp;Return to Donation</a>
					<?php
				}
				else
				{
					?>
					<a href='/checkout'><i class='fa fa-long-arrow-left aria-hidden="true"'></i>&nbsp;Return to Checkout</a>
					<?php
				}
				?>

	<?php
	$email = new EMAIL();

	//
	//2018-02-22 DJT Broke out this complex line into separate features.
	//$admin_addresses = explode(",", get_option('cmd_center')['payment_failure_email']);
	$cmd_options = get_option('cmd_center');
	$to_list = $cmd_options['payment_failure_email'];
	$admin_addresses = explode(',', $to_list);

	foreach($admin_addresses as $send_to)
	{
		//
		//2018-02-22 DJT added send_to for name as well as email
		$email->to(trim($send_to), trim($send_to));
	}
	$email->from('Payment Failure', 'do_not_reply@islonline.org');
	$email->subject('Payment Failure');
	$email->group(true);

	$text = '<HTML>';
	$text .= "<P>Paypal Response: " . $PayPalResult['RESPMSG'] . "</P>" . PHP_EOL;
	$text .= "<P>Client Error: " . $error . "</P>";
	$text .= "<BR />";

	$text .= "<P>";
	$text .= "<B>Form Data</B><BR />";

	foreach($_POST as $part=>$data)
	{
		if(is_array($data))
		{
			foreach($data as $subpart => $subdata)
			{
				if($subpart == 'acctnum')
				{
					
					$text .= "&nbsp;&nbsp;&nbsp;&nbsp;" . $subpart . ": " . str_repeat('*', strlen($subdata) - 4) . substr($subdata, -4) . "<BR>" . PHP_EOL;
				}
				else
				{
					$text .= "&nbsp;&nbsp;&nbsp;&nbsp;" . $subpart . ": " . $subdata . "<BR>" . PHP_EOL;
				}
			}
		}
		else
		{
			$text .= $part . ": " . $data . "<BR />" . PHP_EOL;
		}
	}
	$text .= "</P>";

	$text .= "<P>";
	$text .= "<B>PayPal Error Data</B><BR />";

	if(!empty($PayPalResult['ERRORS']))
	{
		foreach($PayPalResult['ERRORS'] as $index=>$error)
		{
			$text .= "Error " . $error['L_ERRORCODE'] . ": " . $error['L_LONGMESSAGE'] . "<BR />" . PHP_EOL;
		}
	}

	$text .= "<P>";
	$text .= "<B>PayPal RAW Data</B><BR />";
	$text .= $PayPalResult['RAWRESPONSE'];	
	$text .= "</P>";

	//
	//2018-02-22 DJT Added normal email options
	$options = array("MIME-Version: 1.0\r\n", "Content-Type: text/html; charset=ISO-8859-1\r\n");
	$email->options($options);

	$email->message($text);
	$email->send();
}
?>
		</main><!-- End Main -->
	</div> <!-- End inner-content -->
</div><!-- End Content -->


<?php get_footer(); ?>