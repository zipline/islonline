<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800');
#external_landing_pg p, #external_landing_pg a, #external_landing_pg strong {
    font-family: 'Montserrat', sans-serif;
    font-weight: 500;
}
	
#external_landing_pg strong {
    font-size: 20px;
    font-weight: 700 !important;
    color: #F15A29;
}
	
#external_landing_pg h2 {
    font-family: 'Montserrat', sans-serif;
}
	
.custom_banner {
    background: url(/wp-content/uploads/2018/10/ISL_External_v1.jpg);
    height: 460px;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
    background-position: center center;
	display: flex;
    align-items: center;
}
	
.banner_content {
    position: relative;
}
	
.banner_content h2, .banner_content p, .banner_content a {
    color: #fff;
}

.sustain, .join {
	color: #fff;
}
	
.mission {
	padding: 50px 0;
}
	
.sustain a {
	color: #fff;
}

.join strong {
	color: #fff !important;
}
	
.get-started {
    padding: 70px !important;
    float: left !important;
}
	
.orange_btn {
    background: #F15A29;
    padding: 13px 35px;
    text-transform: capitalize;
    display: inline-block;
	transition: 0.3s all;
}
	.orange_btn:hover {
		background: #FF7F5C;
		color: #fff;
	}
	
.get-started h2 {
    color: #F15A29 !important;
}
	
.form_tabs {
    padding: 20px 0;
}
	.form_tabs button {
		border: 2px solid #F15A29;
		padding: 11px 32px;
		color: #F15A29;
		font-family: 'Montserrat', sans-serif;
		font-weight: 500;
		font-size: 16px;
		border-radius: 0;
		margin-right: 13px;
		transition: 0.3s all;
	}
		.form_active {
			background: #F15A29;
			color: #fff !important;
		}
	@media screen and (min-width: 1460px) {
		.custom_banner {
    background: url(/wp-content/uploads/2018/10/ISL_External_v1.jpg);
    height: 520px;
    background-repeat: no-repeat;
    background-size: contain;
    position: relative;
    background-position: center center;
    display: flex;
    align-items: center;
}
	}
@media screen and (min-width: 768px) {
	li#field_28_6 {
		width: 66%;
		display: inline-block;
	}
	
	.banner_content {
    padding: 50px 0;
}
	
	li#field_28_7, li#field_28_8 {
    	width: 66%;
	}
	
	.name_field, .phone_field, .email_field, .contact_field, .schedule_field, .dates_field, .size_field, .group_field {
    	width: 33%;
		display: inline-block;
		float: left;
	}

	
	.gform_wrapper.gform_validation_error .gform_body ul li.gfield.gfield_error:not(.gf_left_half):not(.gf_right_half) {
		max-width: calc(66% - 16px)!important;
	}
	
	
	.join {
		display: flex;
		align-items: center;
	}
	
	.sustain {
		display: flex;
		align-items: center;
	}
	
	.mission {
		display: flex;
		align-items: center;
	}
	
	.contact_field li, .schedule_field li {
		width: 100px;
		display: inline-block;
		margin-right: 10px !important;
	}
	
	li.gchoice_28_4_1, li.gchoice_28_5_1 {
		width: auto;
		display: inline-block;
	}
	
	.ISLCheckbox {
		width: 66%;
		display: inline-block;
	}	
		
	li#field_28_5, li#field_28_4 {
		width: 33%;
		display: inline-block;
		float: left;
	}
	
	.ISLCheckbox {
    	border-left: 1px solid;
		padding-left: 23px;
	}
}

@media screen and (max-width: 767px) {
		.form_tabs button {
			margin-bottom: 10px;
			width: 100%;
		}
		.get-started {
			padding: 40px 0 !important;
		}
}
	
.size_field select {
    height: 50px;
    border: 2px solid #888;
    border-radius: 0;
    padding: 0px 10px !important;
	width: 100% !important;
}
	
.highlight {
	background: #eee;
	transition: 0.3s all;
}

.gfield label {
	display: inline-block !important;
}
	
.need_help input[type=text] {
    height: 50px;
    border-radius: 0;
    border: 2px solid #888;
	width: 100% !important;
}
	
.need_help .ginput_container span {
    width: 100% !important;
}
	
.need_help .ginput_container .gfield_radio label {
	position: relative;
	top: -5px;
	font-size: 16px;
	color: #444;
	padding-left: 3px;
	font-family: 'Montserrat', sans-serif;
	font-weight: 500;
}

.ISLCheckbox ul input[type=checkbox]:checked + label {
    background: #888;
    color: #fff;
}
	
.ISLCheckbox ul li {
    display: inline;
}
.ISLCheckbox ul li label {
	font-size: 16px !important;
	color: #333;
	font-family: 'Montserrat';
	margin-bottom: 7px !important;
	font-weight: 500;
	border: 2px solid #888 !important;
	padding: 10px 20px !important;
	display: inline-block !important;
	width: auto !important;
}
	
.ISLCheckbox input {
    display: none !important;
}
	
.need_help label.gfield_label.gfield_label_before_complex {
    display: none !important;
}
	
.ginput_container.ginput_container_checkbox {
    margin-top: 0 !important;
}
	
.need_help label {
    font-size: 16px !important;
    letter-spacing: 0 !important;
    color: #888;
    font-weight: 500 !important;
    font-family: 'Montserrat';
}
	
span.gfield_required {
    display: none;
}
	
body .gform_wrapper ul li.gfield {
	margin-top: 0 !important;
}
	
label.gfield_label {
    padding-top: 10px;
}
	
span#input_28_1_6_container, span#input_29_1_6_container, span#input_30_1_6_container {
    margin-top: 10px;
    margin-bottom: 10px;
}
	
.gfield {
    padding: 10px;
}
	
.gform_footer.top_label input[type=submit] {
    float: right;
    border-radius: 0;
    font-family: 'Montserrat';
    font-weight: 500;
    background: #43B1E2;
}
.ISLCheckbox {
	padding-left: 20px;
}
.blue {
    color: #43B1E2;
}
	
.dark_blue {
	color: #265592;
}
</style>


<?php
	/*
	Template Name: Start Your Journey
	*/

	get_header();
?>
<div id="external_landing_pg">

<div class="custom_banner" style="background-image: url('<?php the_field('custom_banner'); ?>') !important;">
	<div class="medium-11 medium-centered columns">
		<div class="banner_content">
			<div class="large-6 columns">

				<h2><?php the_field('banner_header'); ?></h2>
				<p><?php the_field('banner_text'); ?></p>
				
				<a class="orange_btn" href="#start">Start Today</a>

			</div>
		</div>	
	</div>
</div>
		<div class="medium-11 medium-centered columns mission">
			<div class="large-6 columns">

				<h2>
					<?php the_field('white_section_header'); ?>
				</h2>
				<p>
					<?php the_field('white_section_text'); ?>				
				</p>
				<strong>Call Us Today 1-877-779-8075</strong>
			</div>
			<div class="large-6 columns">
				<img src="<?php the_field('white_section_image'); ?>">
			</div>
		</div>
		
		<div class="medium-12 medium-centered sustain" style="float: left; background: #265592;">
			<div class="large-5" style="display: inline-block;">
				<img src="<?php the_field('blue_section_image'); ?>">
			</div>
			<div class="large-7" style="display: inline-block; float: right; padding: 50px;">
				<h2>
					<?php the_field('blue_section_header'); ?>
				</h2>
				<p>
					<?php the_field('blue_section_text'); ?>
				</p>
				<a class="orange_btn" href="#start">Start Today</a>
			</div>
			<div style="clear: both;"></div>
		</div>
		
		<div class="medium-12 medium-centered join" style="float: left; background: #43B1E2;">
			<div class="large-7" style="display: inline-block; float: left; padding: 50px;">
				<h2>
					<?php the_field( 'light_blue_section_header' ); ?>
				</h2>
				<p>
					<?php the_field('light_blue_section_text'); ?>				
				</p>
				<strong>Call Us Today 1-877-779-8075</strong>			
			</div>
			<div class="large-5" style="display: inline-block;">
				<img src="<?php the_field( 'light_blue_section_image' ); ?>">
			</div>
			<div style="clear: both;"></div>
		</div>
	
		<a name="start"></a>
<div class="medium-12 medium-centered columns get-started">
	<div class="large-12 columns">
		<h2>
			GET STARTED
		</h2>
		<p>
			Fill out the form below, and one of our Placement Specialists will reach out to you as soon as possible!
		</p>

		<div class="form_tabs">
			<button class="form_btn tablink form_active" onclick="openForm(event,'learn')">Learn More Information</button>
			<button class="form_btn tablink" onclick="openForm(event,'select')">I Need Help Selecting a Program</button>
			<button class="form_btn tablink" onclick="window.location.replace('https://islonline.org/custom-programs/');">I would like to create a Custom Program</button>
		</div>

		<div id="learn" class="form">
			<?php gravity_form( 51, false, false, false, '', false ); ?>
		</div>

		<div id="select" class="form" style="display:none">
			<?php gravity_form( 52, false, false, false, '', false ); ?>
		</div>

		<div id="create" class="form" style="display:none">
			<?php gravity_form( 53, false, false, false, '', false ); ?>
		</div>
	</div>
</div>
		
</div>
    <?php edit_post_link('edit'); ?>

<?php get_footer(); ?>

<script>
function openForm(evt, formName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("form");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" form_active", "");
  }
  document.getElementById(formName).style.display = "block";
  evt.currentTarget.className += " form_active";
}
	
$('input').focus(
    function(){
        $(this).closest('.gfield').addClass('highlight');
    });

$('input').blur(
    function(){
        $(this).closest('.gfield').removeClass('highlight');
    });
</script>