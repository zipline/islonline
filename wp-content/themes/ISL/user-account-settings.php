<?php
/*
Template Name: User Settings
*/
?>

<?php
//
//Setup classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "user.class.php");
require_once ($plugin_path . "rsvp.class.php");

$user = new USER();
$rsvp = new RSVP();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(isset($_GET['rsvp']) && $_GET['rsvp'] > 0 && !$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;



?>

<?php acf_form_head(); ?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>
<div id="settings" class="iframe-inner">
<?php 
    $user_id = get_current_user_id();
	
	$phone = get_user_meta($user_id, 'primary_phone', true);
	
    $fields = array(21681);
    acf_form(array(
            'post_id' => 'user_' . $user_id,
            'field_groups' => $fields,
            'updated_message' => "<div class='callout success' data-closable>Your contact preferences have been updated.<button class='close-button' aria-label='Dismiss alert' type='button' data-close><span aria-hidden='true'>&times;</span></button></div>",
        )
    );
?>
</div>

<?php
if(strlen($phone)>=7)
{
	?>
	
	<script>
		jQuery(document).ready(function()
		{
			jQuery('#acf-field_57ff5711447f7-SMS').parent().append(' to ' + <?php echo $phone?>);
		});
	</script>
	
	<?php
	
} else {
	?>
	
	<script>
		jQuery(document).ready(function()
		{
			jQuery('#acf-field_57ff5711447f7-SMS').attr('disabled', true);
		});
	</script>
	
	<?php
	
}
?>

<?php wp_footer(); ?>
