<?php
/*
Template Name: List Info Docs
*/
?>
<?php
/*
2018-02-16 DJT added Scrollbar to HTML open to compensate for IE, Removed HREF from same to compensate for IT
*/
?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>

<?php
//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "docs.class.php");
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "rsvp.class.php");

wp_enqueue_script('cmd_document_signature', plugins_url() . '/command/js/doc.sig.js', array('jquery'));	
wp_localize_script('cmd_document_signature', 'svars', array('ajax' => admin_url('admin-ajax.php')));

$docs = new DOCS();
$team = new TEAM();
$rsvp = new RSVP();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;


$team->set_team($rsvp->team($_GET['rsvp']));
$docs->post_id($team->value('post'));
$docs->fetch(true, false);
$docs->fetch_read();
$docs->set_custom(true);

if ($docs->documents_complete()) : ?>
<script type="text/javascript">
	parent.complete_item("list-info-docs_link");
</script>
<?php endif; ?>

<div id="myisl-content">
	<?php the_field('instructions'); ?> 
	<?php
    foreach($docs->team_docs() as $doc_id => $doc_title)
    {
        $doc_type = get_post_meta($doc_id, 'resource_type', true);
		$signature = get_post_meta($doc_id, 'signature_required', true);
		$classname = '';
		if(empty($signature) || $signature == false || $doc_type=='PDF Document')
		{
			$classname = "class='no_req_check' name='" . $doc_id . "'";
		}
		
        echo "<div class='document' id='" . $doc_id . "'>";
		echo "<input type='checkbox' disabled='true' name='doc_check_" . $doc_id . "' id='doc_check_" . $doc_id . "' " . ($docs->is_read($doc_id)  ? 'checked' : '') . ">&nbsp;&nbsp;";
		
		switch($doc_type)
		{
			case 'HTML Page':
				//
				//2018-02-16 DJT added Scrollbar to HTML open to compensate for IE, Removed HREF from same to compensate for IT
				//
				echo "<a onclick=\"window.open('read-info-docs?doc_id=" . $doc_id . "&rsvp=" . $_GET['rsvp'] . "&post_id=" . $team->value('post') . "', 'newwindow', 'width=800, height=800, menubar=yes, scrollbars=1')\" " . $classname . ">" . $doc_title . "</a>";
				break;
			case 'URL':
				$url = get_post_meta($doc_id, 'url', true);
				echo "<a href='" . $url . "' " . $classname . " id='doc_check_" . $doc_id . "' target='_blank'>" . $doc_title . "</a>";
				break;
			case 'PDF Document':
				$pid = get_post_meta($doc_id, 'pdf_document', true);
				echo "<a href='" . get_the_guid($pid) . "' " . $classname . " id='doc_check_" . $doc_id . "' target='_blank'>" . $doc_title . "</a>";
				break;
			default:
				echo "<a href='" . get_the_guid($doc_id) . "'  " . $classname . " id='doc_check_" . $doc_id . "' target='_blank'>" . $doc_title . "</a>";
				break;
		}
		
        echo "</div>\r\n";

    }
	echo "<input type='hidden' name='post_id' id='post_id' value='" . $team->value('post') . "'>";

    ?>

</div> <!-- end #content --> 
 
 <?php wp_footer() ?>   
