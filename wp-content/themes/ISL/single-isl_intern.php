<?php

wp_enqueue_script("cmd_check_login", CC_JS_PATH . 'intern.logged.in.js', NULL);

if (is_user_logged_in() )
{
	$app_button = '';
	$register_button = ' style="display: none;"';
}
else
{
	$app_button = ' style="display: none;"';
	$register_button = '';
}
?>


<?php get_header(); ?>

<?php get_template_part( 'banner' ); ?>

    <div id="content">
    
        <div class="row collapse">
    
            <main id="main" class="large-8 medium-12 small-centered columns" role="main">

                <h2><?php the_title(); ?> in <?php the_field('country') ?> </h2>
                <h4>Placement Type: <?php the_field('placement_type'); ?></h4>
                <b>Placement Focus:</b> <?php the_field('placement_focus'); ?><br />
                <b>Placement Location:</b> <?php the_field('placement_location'); ?><br />
                <b>Required Language Skills:</b> <?php the_field('required_language_skills'); ?><br />
                <b>Minimum Placement Length:</b> <?php the_field('min_length'); ?><br />
                <b>Maximum Placement Length:</b> <?php the_field('max_length'); ?>
                <hr />
                <h4>About the Organization</h4>
                <?php the_field('about'); ?>
                <h4>Assignment</h4>
                <?php the_field('details'); ?>
                <h4>Internship Requirements</h4>
                <?php the_field('requirements'); ?>
                <?php if(the_field('notes')) : ?>
                <h4>Additional Notes</h4>
                <?php the_field('notes'); ?>
                <?php endif; ?>
                <h4>Starting Cost:</h4>
                <?php the_field('starting_cost'); ?><br />
                <br />
                
                <a <?php echo $app_button;?> id="register" class="button" href="/internship-application?id=<?php echo the_ID();?>&country=<?php the_field('country')?>">Apply Now</a>

                <div <?php echo $register_button; ?> name="intern_not_logged_in" id="intern_not_logged_in">
					<p>You need to be logged in before reqeusting an Internship so we can make sure we associate all of your request information with a proper account.</p>
					<P>After you Login, you can close the login window to come back here and apply for this internship</P>
	                <a class="button" href="/login" target="_blank">Click here to Login/Register</a>
	                
				</div>
           					
            </main> <!-- end #main -->

        </div> <!-- end #inner-content -->
    <?php edit_post_link('edit'); ?>
    </div> <!-- end #content --> 

<?php get_footer(); ?>


