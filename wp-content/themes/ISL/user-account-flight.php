<?php
/*
Template Name: User Flight
*/

//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "team.support.class.php");
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "tabs.class.php");

$team = new TEAM();
$support = new TEAM_SUPPORT();
$rsvp = new RSVP();
$page = new TAB_MANAGE();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;

$rsvp->set_rsvp_id($_REQUEST['rsvp']);
$rsvp->fetch_rsvp();

$team->set_team($_REQUEST['team_id']);
$prep_ready = $support->status_value($team->value('status'), 'prep');
$traveled = $support->status_value($team->value('status'), 'trav');

$reservation = $rsvp->get_rsvp();

$script_path = "/wp-content/plugins/command/js/";

require_once(get_template_directory().'/my-isl-subpage-head.php');
wp_head();

wp_enqueue_script('jquery-ui-datepicker');
//wp_enqueue_script('command-date', $script_path . 'datepickers-generic.js', 'jquery-ui-datepicker', '4.7.1', true);
wp_enqueue_script('command-flight', $script_path . 'flight.build.js', 'jquery-ui-datepicker', '4.8.0', true);
wp_localize_script('command-flight', 'vars', array('to' => $team->value('arrival_city'), 'from' => $team->value('departure_city')));

wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');



//
//Save data if we have it
if(!empty($_POST['saving']) && $_POST['saving']=="Save")
{
	$flight_info = array();

	//
	//Sequence into Travel to (direction 0) and Trevel from (direction 1)
	for($i = 0; $i <= $_POST['count_fld']; $i++)
	{
		if(isset($_POST['direction_' . $i]) && $_POST['direction_' . $i] == 0)
		{
			if(!empty($_POST['airline_' . $i]) && 
			   !empty($_POST['flight_' . $i]) &&
			   !empty($_POST['depart_' . $i]) &&
			   !empty($_POST['ddate_' . $i]) &&
			   !empty($_POST['arrive_' . $i]) &&
			   !empty($_POST['adate_' . $i])
			)
			{
				$flight_info[] = array('direction' => 0,
									   'airline' => $_POST['airline_' . $i], 'flight' => $_POST['flight_' . $i], 'seat' => 0,
									   'depart' => $_POST['depart_' . $i], 'ddate' => $_POST['ddate_' . $i], 'dtime' => $_POST['dtime_' . $i],
									   'arrive' => $_POST['arrive_' . $i], 'adate' => $_POST['adate_' . $i], 'atime' => $_POST['atime_' . $i]); 		
			}
		}
	}
	for($i = 0; $i <= $_POST['count_fld']; $i++)
	{
		if(isset($_POST['direction_' . $i]) && $_POST['direction_' . $i] == 1)
		{
			if(!empty($_POST['airline_' . $i]) && 
			   !empty($_POST['flight_' . $i]) &&
			   !empty($_POST['depart_' . $i]) &&
			   !empty($_POST['ddate_' . $i]) &&
			   !empty($_POST['arrive_' . $i]) &&
			   !empty($_POST['adate_' . $i])
			)
			{
				$flight_info[] = array('direction' => 1,
									   'airline' => $_POST['airline_' . $i], 'flight' => $_POST['flight_' . $i], 'seat' => 0,
									   'depart' => $_POST['depart_' . $i], 'ddate' => $_POST['ddate_' . $i], 'dtime' => $_POST['dtime_' . $i],
									   'arrive' => $_POST['arrive_' . $i], 'adate' => $_POST['adate_' . $i], 'atime' => $_POST['atime_' . $i]); 		
			}
		}
	}
	$rsvp->set_flight_info($flight_info);

	$flight_fees = array('arrive' => !empty($_POST['off_arrival']) ? "checked" : '',
				 		 'depart' => !empty($_POST['off_depart']) ? "checked" : '');
	$rsvp->set_flight_fees($flight_fees);
}

//
//Fetch data for the Flight Itinerary
$flight_data = $rsvp->get_flight_info();
$flight_fees = $rsvp->get_flight_fees();


if($prep_ready || $traveled)
{
	if($rsvp->has_travel($_REQUEST['rsvp']))
	{
	?>
	<script type="text/javascript">
		parent.complete_item("flight_link");
	</script>
	<?php
	}
	?>

<div name="flight-info" id="flight-info" class="iframe-inner"> 
	<p>Please check the 'Late or Early Arrival' or 'Late or Early Departure' boxes if your arrival in country or departure from country is outside of the time detailed in your Trips Travel Document.</p>
	<form name="flight_data" id="flight_data" method="POST" enctype="multipart/form-data" action="">
		<table name='flight_information' id='flight_information'>
			<tbody>
				<TR><TD>Late or Early Arrival</TD><TD><input type="checkbox" name="off_arrival" id="off_arrival" <?php echo $flight_fees['arrive']?>></TD></TR>
				<TR><TD>Late or Early Departure</TD><TD><input type="checkbox" name="off_depart" id="off_depart" <?php echo $flight_fees['depart']?>></TD></TR>
				
			<?php
			foreach($flight_data as $index => $data)
			{
			?>	
				<tr class="flight_block">
					<td>
						<table class="first" name="t_<?php _e(count($flight_data) - $index)?>" id="t_<?php _e(count($flight_data) - $index)?>"> 
							<tr>
								<th><label>Direction:</label></th>
								<td>
									<select name="direction_<?php _e(count($flight_data) - $index)?>" id="direction_<?php _e(count($flight_data) - $index)?>" required>
										<option value="0" <?php echo isset($data['direction']) && $data['direction'] == 0 ? " selected" : ""?>>Travel To <?php echo $team->value('arrival_city')?></option>
										<option value="1" <?php echo isset($data['direction']) && $data['direction'] == 1 ? " selected" : ""?>>Travel From <?php echo $team->value('departure_city')?></option>
									</select>
								</td>
							</tr>
							<tr>
								<th><label>Airline:</label></th>
								<td>
									<input placeholder="Your airline" type="text" name="airline_<?php _e(count($flight_data) - $index)?>" id="airline_<?php _e(count($flight_data) - $index)?>"  value="<?php _e($data['airline'])?>" required>
								</td>
							</tr>
							<tr>
								<th><label>Flight:</label></th>
								<td>
									<input placeholder="Your flight number" type="text" name="flight_<?php _e(count($flight_data) - $index)?>" id="flight_<?php _e(count($flight_data) - $index)?>"  value="<?php _e($data['flight'])?>" required>
								</td>
							</tr>
							<tr>    
								<th><label>Departing:</label></th>
								<td>
									<input placeholder="Departure City" type="text" name="depart_<?php _e(count($flight_data) - $index)?>" id="depart_<?php _e(count($flight_data) - $index)?>"  placeholder="Which City / State / Country" value="<?php _e($data['depart'])?>" required>
								</td>
							</tr>
							<tr>
								<th><label>&nbsp;</label></th>
								<td>
									<div class="input-group">
										<label for="ddate_<?php _e(count($flight_data) - $index)?>">Date:</label>
										<input placeholder="Departure date" type="text" class="datepicker" name="ddate_<?php _e(count($flight_data) - $index)?>" id="ddate_<?php _e(count($flight_data) - $index)?>"  value="<?php _e($data['ddate'])?>" required>
									</div>
									<div class="input-group">
										<label for="dtime_<?php _e(count($flight_data) - $index)?>">Time:</label>
										<input placeholder="Departure time" type="text" name="dtime_<?php _e(count($flight_data) - $index)?>" id="dtime_<?php _e(count($flight_data) - $index)?>"  value="<?php _e($data['dtime'])?>" required>
									</div>
								</td>
							</tr>
							<tr>
								<th><label>Arriving:</label></th>
								<td>
									<input type="text" name="arrive_<?php _e(count($flight_data) - $index)?>" id="arrive_<?php _e(count($flight_data) - $index)?>"  placeholder="Which City / State / Country" value="<?php _e($data['arrive'])?>" required>
								</td>
							</tr>
							<tr>
								<th><label>&nbsp;</label></th>
								<td>
									<div class="input-group">
										<label for="adate_<?php _e(count($flight_data) - $index)?>">Date:</label>
										<input placeholder="Arrival date" type="text" class="datepicker" name="adate_<?php _e(count($flight_data) - $index)?>" id="adate_<?php _e(count($flight_data) - $index)?>"  value="<?php _e($data['adate'])?>" required>
									</div>
									<div class="input-group">
										<label for="atime_<?php _e(count($flight_data) - $index)?>">Time:</label>
										<input placeholder="Arrival time" type="text" name="atime_<?php _e(count($flight_data) - $index)?>" id="atime_<?php _e(count($flight_data) - $index)?>"  value="<?php _e($data['atime'])?>" required> 
									</div>
								</td>
							</tr>
							<tr>
								<th><label>&nbsp;</label></th>
								<td>
									<input type="hidden" name="seat_<?php _e(count($flight_data) - $index)?>" id="seat_<?php _e(count($flight_data) - $index)?>"  value="0">
									<?php
									if(!$traveled)
									{
										?>
										<input type="submit" class="button small delete-segment" value="Delete this trip segment" name="delete_<?php _e(count($flight_data) - $index)?>" id="delete_<?php _e(count($flight_data) - $index)?>">
										<input type="button" class="button small add-segment" value="Add another trip segment" />
										<?php
									}
									?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<?php
			}
			?>
			</tbody>

			<tfoot>
				<tr>
				<?php
				if(!$traveled)
				{
					?>
					<td colspan="5">
						<input type="submit" class="button float-center" name="saving" id="saving" value="Save" />
					</td>
					<?php
				}
				?>
				</tr>
			</tfoot>
		</table>
		<input type="hidden" name="count_fld" id="count_fld" value="<?php _e(count($flight_data))?>" />
		<input type="hidden" name="rsvp" value="<?php _e($_REQUEST['rsvp']) ?>" />

	</form>
</div>			

<?php
}
else
{
	echo "<p>Once your Program is ready, you'll receive a message letting you know everything is good on our side. At that point in time, you should purchase your flights and setup travel for your trip. </p>";
	echo "<p><b>DO NOT</b> make any purchases until you hear from us that it's time. You should be hearing from us soon!</p>";	
}
?>
<script language="JavaScript">
	// var iFrameInner = (contentWindow || contentDocument);
	// var iFrameInnerHeight = document.body.scrollHeight;
	// var iFrameInnerHeight = $(iFrameInner).find('body').height();
	// console.log('MY Height', iFrameInnerHeight);
</script>
<script>
	jQuery(document).ready(function(){
		jQuery('.hasDatepicker').each(function(){

			jQuery(this).datepicker('destroy');

			jQuery(this).datepicker({
				changeMonth : true,
				changeYear : true,
				dateFormat : 'mm/dd/yy',
				beforeShow: function(event, ui) {
               jQuery('#ui-datepicker-div').addClass('open');
            },
            onClose: function(event, ui) {
                jQuery('#ui-datepicker-div').removeClass('open');
            }
			});
		});
	});
</script>
<?php wp_footer() ?>