<?php
/*
Template Name: Find an Internship
*/
?>

<?php
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style',
				 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "rsvp.class.php";


//
//Gather Team, Country, Program Details
//
$country = new COUNTRY();
$program = new PROGRAM();
$team = new TEAM();
$rsvp = new RSVP();

$search_team = isset($_POST['search_form']) ? $_POST['search_form'] : '';
$sel_prog = isset($_POST['program']) ? $_POST['program'] : 0;
$sel_cntry = isset($_POST['country']) ? $_POST['country'] : 0;
$trip_length = isset($_POST['trip_length']) ? $_POST['trip_length'] : 0;
$order_by = isset($_POST['order_by']) ? $_POST['order_by'] : 'date_early';
$view_by = isset($_POST['view_by']) ? $_POST['view_by'] : 'card';

if(!empty($search_team))
{
	$team_id = $team->find_team_by_name($search_team);
	if(!empty($team_id))
	{
		$team->set_team($team_id);
		$post = $team->value('post');
		$url = get_permalink($post);
		
		header ('Location: ' . $url);
		exit;		
		$coming = array();		
	}
	else
	{
		$coming = array();		
	}
}
else
{
	$coming = $team->upcoming($sel_prog, $sel_cntry, $trip_length, '', '', $order_by, array('internship'));
}

?>

<?php get_header(); ?>
	
 <?php 
    $src = get_field('image')['sizes']['large'];     
    $vposition = get_field('image_position'); 
    $hposition = get_field('image_position_hz');
    $color = get_field('title_color');
    $overlay = get_field('banner_image_overlay');
?>

<div class="short-banner find-a-team" style="background-image: url(<?php echo $src ?>); background-position: <?php echo $vposition; ?> <?php echo $hposition; ?>;">
    <header class="entry-header">
        <h1 class="entry-title" style="color: <?php echo $color; ?>;"><?php the_title(); ?></h1>
    </header>
</div>
    
<div class="search-bar row">
	<form name="opts" id="opts" method="POST" enctype="multipart/form-data" action="<?php _e($_SERVER['REQUEST_URI'])?>">

		<div class="country search-selector">

			<label for="country"><strong>WHERE</strong> do you want to go?:</label>
			<div class="select-wrap <?php echo ($sel_cntry ? 'selected' : ''); ?>">
				<SELECT name="country" id="country" onchange="javascipt:document.getElementById('opts').submit()"><?php _e($country->selector($sel_cntry, true, true, $sel_prog, true, array('internship')))?></SELECT>
				<i class="fa fa-chevron-down"></i>
			</div>
		</div>

		<div class="program search-selector">
			<label for="program"><strong>WHAT</strong> do you want to do?:</label> 
			<div class="select-wrap <?php echo ($sel_prog ? 'selected' : ''); ?>">
				<SELECT name="program" id="program" onchange="javascipt:document.getElementById('opts').submit()"><?php _e($program->selector($sel_prog, true, false, true, $sel_cntry, true, array('internship')))?></SELECT>
				<i class="fa fa-chevron-down"></i>
			</div>
		</div>

		<div class="trip-length search-selector">
			<label for="trip-length"><strong>HOW LONG</strong> do you want to stay?</label>
			<div class="select-wrap <?php echo ($trip_length ? 'selected' : ''); ?>">
				<SELECT name="trip_length" id="trip_length" onchange="javascipt:document.getElementById('opts').submit()">
					<OPTION value=0  <?php selected($trip_length,0)?>> ALL</OPTION>
					<OPTION value=10 <?php selected($trip_length,10)?>> 30 Days or less</OPTION>
					<OPTION value=11 <?php selected($trip_length,11)?>> Between 31 and 45 days</OPTION>
					<OPTION value=12 <?php selected($trip_length,12)?>> Between 46 and 60 days</OPTION>
					<OPTION value=13 <?php selected($trip_length,13)?>> More than 60 Days</OPTION>
				</SELECT>

				<i class="fa fa-chevron-down"></i>
			</div>
		</div>


		<a class="show-all-trips clearfix" href="javascript:clean_reload();">Show all available internships</a>


		<input type="hidden" name="order_by" id="order_by" value="<?php echo $order_by;?>">
		<input type="hidden" name="view_by" id="view_by" value="<?php echo $view_by;?>">
	</form>	
</div>

<div id="content" class="search-results">
	<div id="inner-content" class="row">
        <main id="search-main" role="main">

        	<h2>Internships</h2>

        	<div class="sort">
        		<div class="view-buttons">
					<a class="card-view <?php _e($view_by == 'card' ? 'active' : '')?>" onclick="javascript:change_view('card')"><i class="fa fa-th-large" aria-hidden="true"></i></a>
					<a class="list-view <?php _e($view_by == 'list' ? 'active' : '')?>" onclick="javascript:change_view('list')"><i class="fa fa-th-list" aria-hidden="true"></i></a>
				</div>
				<select name="order_field" id="order_field" onchange="javascipt:order_field()">
					<option value="price_low" <?php selected($order_by, 'price_low')?>>Price: Low to High</option>
					<option value="price_high" <?php selected($order_by, 'price_high')?>>Price: High to Low</option>
				</select>
			</div> 

        	<div class="team-results"><!-- Search Results Wrapper -->
        		<div id="grid" class="row small-up-1 medium-up-2 large-up-4 <?php _e($view_by == 'card' ? 'active' : '')?>" data-equalizer data-equalize-on="medium" data-equalize-by-row="true">
					<?php

					if(count($coming)>0)
					{
						foreach($coming as $data)
						{
							$programs = array();
							$countries = array();
							foreach(explode(",",$data['program_id']) as $program_id)
							{
								$program->set_program($program_id);
								$programs[$program_id] = $program->get_name();
							}
							foreach(explode(",",$data['country_id']) as $country_id)
							{
								$country->set_country($country_id);
								$countries[$country_id] = $country->get_name();
							}
							
							$stime = strtotime($data['start_date']);
							$etime = strtotime($data['end_date']);
							$intern_length = (($etime-$stime)  / 60 / 60 / 24) + 1;
							
							if(has_post_thumbnail($data['post_id']))
							{
								$source = wp_get_attachment_image_src(get_post_thumbnail_id($data['post_id']), 'single-post-thumbnail');	
								$image = $source[0];
							}
							else
							{
								$pid = $program->post_id();
								$source = get_field("image", $pid);
								
								if($source)
								{
									$image = $source['url'];
								}
								else 
								{
									$cid = $country->post_id();
									$source = get_field("image", $cid);
									$image = $source['url'];
								}
							}
					?>
					
						<div class="column">
							<div class="team" data-equalizer-watch>
								<div class="team-banner" style="background-image: url('<?php echo $image; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center center;"></div>

								<h5><?php _e($program->display_program_list($programs))?> <span>in</span> <?php _e($country->display_country_list($countries))?></h5>

								<div class="details">
									<div class="dates">
										<div class="intern-length">
											<span class="day"><?php _e($intern_length)?></span><br />
											<span class="year">Days</span><br />
										</div>
									</div>

									<div class="intern-description"><?php _e($data['teaser'])?></div>
								</div>
								<div class="bottom">
									<h5 class="cost"><?php _e('$' . $data['cost'])?></h5>
									<a href="<?php _e($data['url'])?>" class="button">Details</a>
								</div>
							</div>
						</div>

					<?php
						}
					}
					else
					{
						echo "<div style='padding-left: 1.25rem;'>";
						echo "There are no matching teams. Please try different criteria.";
						echo "</div>";
						
					}
					?>
				</div>

				<!-- List View -->
				<div id="list" class="<?php _e($view_by == 'list' ? 'active' : '')?>">
					<?php
					//$coming = $team->upcoming($prg_filter, $cty_filter);

					if(count($coming)>0)
					{
						foreach($coming as $data)
						{
							$programs = array();
							$countries = array();
							foreach(explode(",",$data['program_id']) as $program_id)
							{
								$program->set_program($program_id);
								$programs[$program_id] = $program->get_name();
							}
							foreach(explode(",",$data['country_id']) as $country_id)
							{
								$country->set_country($country_id);
								$countries[$country_id] = $country->get_name();
							}
							
							$stime = strtotime($data['start_date']);
							$etime = strtotime($data['end_date']);
							$intern_length = $etime-$stime;

							if(has_post_thumbnail($data['post_id']))
							{
								$source = wp_get_attachment_image_src(get_post_thumbnail_id($data['post_id']), 'single-post-thumbnail');	
								$image = $source[0];
							}
							else
							{
								$pid = $program->post_id();
								$source = get_field("image", $pid);
								
								if($source)
								{
									$image = $source['url'];
								}
								else 
								{
									$cid = $country->post_id();
									$source = get_field("image", $cid);
									$image = $source['url'];
								}
							}
								?>
                        		<div class="team row">
                        			<div class="large-3 medium-4 columns">
	                        			<h6><?php _e($program->display_program_list($programs))?> <span>in</span> <?php _e($country->display_country_list($countries))?></h6>
	                        			<div class="date">
			                                <?php _e(date("M", $stime))?> <?php _e(date("d", $stime))?>, <?php _e(date("Y", $stime))?> to
			                                <?php _e(date("M", $etime))?> <?php _e(date("d", $etime))?>, <?php _e(date("Y", $etime))?>
			                            </div>
                        			</div>
                        			<div class="large-6 medium-2 columns">
			                            <div class="description"><?php _e($data['teaser'])?></div>
                        			</div>

                                	<div class="large-1 medium-1 columns">
                                		<div class="price">
                                   		<?php _e('$' . $data['cost'])?>
                                   		</div>
                                   	</div>
                                   	<div class="large-2 medium-2 columns">
                                    	<a href="<?php _e($data['url'])?>" class="button">Internship Details</a>
                                	</div>
                                </div>
                        
			            <?php
								}
							}
							else
							{
								echo "<p>";
								echo "There are no matching teams. Please try different criteria.";
								echo "</p>";
							}
						?>
				</div><!-- End List View -->
			</div><!-- End Search Wrapper -->
        </main> <!-- end #main -->
    </div> <!-- end #inner-content -->
</div>
   
<script>
	function clean_reload()
	{
		document.getElementById('country').value = 0;
		document.getElementById('program').value = 0;
		document.getElementById('begin_date').value = '';
		document.getElementById('end_date').value = '';
		document.getElementById('trip_length').value = 0;
		document.getElementById('opts').submit();
	}

	function order_field()
	{
		document.getElementById('order_by').value = document.getElementById('order_field').value;
		document.getElementById('opts').submit()	
	}

	function change_view(view)
	{
		document.getElementById('view_by').value = view;	
	}


</script>
<?php get_footer(); ?>