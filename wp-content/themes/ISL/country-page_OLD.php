<?php
/*
Template Name: Country_OLD
*/
?>

<?php get_header(); ?>

<?php
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "rsvp.class.php";
require_once $plugin_path . "user.class.php";

$team = new TEAM();
$country = new COUNTRY();
$program = new PROGRAM();
$rsvp = new RSVP();

$country->set_country($country->by_post(get_the_ID()));
$coming = $team->upcoming(0, $country->get_country());


//
//Find staff
$user_meta_key = 'cmd_' . str_replace(' ', '_', strtolower($country->get_name())) . '_staff_pos';
$query = "SELECT user.ID, meta.meta_value
		  FROM " . $wpdb->prefix . "users user
		       INNER JOIN " . $wpdb->prefix . "usermeta meta on user.id=meta.user_id
		  WHERE
		       meta_key ='" . $user_meta_key . "'";

$results = $wpdb->get_results($query, ARRAY_A);

foreach($results as $id=>$data)
{
	$users[$data['ID']]['ID'] = $data['ID'] 	;
	$users[$data['ID']]['Title'] = $data['meta_value'] 	;
	$users[$data['ID']]['first_name'] = get_user_meta($data['ID'], 'first_name', true);
	$users[$data['ID']]['last_name'] = get_user_meta($data['ID'], 'last_name', true);
}
?>

<?php get_template_part( 'banner' ); ?>
    
    <div id="content">
    <a class="button tiny mobile-anchor" href="#team-sidebar">View Teams</a>
        <div class="row collapse">
    
            <main id="main" class="large-8 medium-8 columns country" role="main">
        
               <?php the_flex_content(); ?>
            
            </main> <!-- end #main -->

			<div id="team-sidebar" class="large-4 medium-4 columns">
				<h3>Upcoming Teams</h3>
				<?php
				if(count($coming))
					{ ?>
				<div class="team-feed">
					<?php foreach($coming as $data) :

						$countries = array();
						foreach(explode(",",$data['country_id']) as $country_id) {
							$country->set_country($country_id);

							$reg_label = '';
							foreach(explode(',', $data['region_id']) as $region_ids)
							{
								$country->set_region($region_ids);
								$this_region = $country->region_details();
								
								if($this_region['country_id'] == $country_id && $this_region['title'] != 'ALL')
								{
									$reg_label .= $this_region['title'] . ' & ';
								}
								
							}	
							
							if(strlen($reg_label) > 2 && substr($reg_label,-3) == ' & ')
							{
								$reg_label = ': ' . substr($reg_label, 0, -3);	
							}
							
							$countries[$country_id] = $country->get_name() . $reg_label;
						}
					
						$programs = array();
						foreach(explode(",",$data['program_id']) as $program_id)
						{
							$program->set_program($program_id);
							$programs[$program_id] = $program->get_name();
						}
						
						$stime = strtotime($data['start_date']);
						$etime = strtotime($data['end_date']);
						$open = $data['minimum_volunteers'] - $rsvp->count_rsvp($data['id']);
						$avail = max(0, $data['maximum_volunteers'] - $rsvp->count_rsvp($data['id']));
						$sold_out = !(bool)$avail;
					?>

						<div class="team">
						<h5><?php _e($program->display_program_list($programs))?> in <?php _e($country->display_country_list($countries))?>
							<?php if($open >0 || $open <= 0 && !$sold_out ) { ?>
                                <span><?php _e('$' . $data['cost'])?></span>
                            <?php } if($sold_out) { ?>
                                <span class="sold-out"><del><em><?php _e('$' . $data['cost'])?></del></em></span>
                            <?php } ?>  
						</h5>
						<div class="description"><?php _e($data['teaser'])?></div>
						<div class="dates">
							<div class="date"><!-- Start Date -->
								<span class="month"><?php _e(date("M", $stime))?></span><br />
								<span class="day"><?php _e(date("d", $stime))?></span><br />
								<span class="year"><?php _e(date("Y", $stime))?></span><br />
							</div>
							<div><b>to</b></div>
							<div class="date"><!-- End Date -->
								<span class="month"><?php _e(date("M", $etime))?></span><br />
								<span class="day"><?php _e(date("d", $etime))?></span><br />
								<span class="year"><?php _e(date("Y", $etime))?></span><br />
							</div>
						</div>
						<div class="details">
							<?php if($open >0 || $open <= 0 && !$sold_out ) { ?>
                                <a href="<?php _e($data['url'])?>" class="button tiny">Team Details</a>
                                <?php } if($sold_out) { ?>
                                <span href="<?php _e($data['url'])?>" class="button secondary disabled">Team Details</span>
                                <h5 class="sold-out"><em>SOLD OUT!</em></h5>
                                <?php } ?>   
                           
							
								<?php
								if($open >0 )
								{
                                    echo '<div class="count">Just ';
                                    echo '<span>';
                                    echo ''.$open.' more volunteer'.($open != 1 ? 's' : '').' ';
                                    echo "</span>to make this trip a 'go'!</div>";
								}
								if($sold_out)
								{
									?>
									<div class="count"><span color="red"></span></div>
									<?php
								}
								if($open <= 0 && !$sold_out)
								{
									echo "<div class='count'>Join this team before it's too late. Only ";
	                                echo '<span>';
	                                echo ''.$avail.' open spot'.($avail != 1 ? 's' : '').'';
	                                echo "</span>";
	                                echo "!</div>";
								}
								?>
						</div>  
						<div class="share">Share this team: <a class="twitter" href="http://twitter.com/home?status=Join this @islonline team and make a difference! <?php _e($data['url'])?>" title="Share this post on Twitter!" target="_blank"><i class="fa fa-twitter"></i></a><a href="http://www.facebook.com/sharer.php?u=<?php _e($data['url'])?>" title="Share this post on Facebook!" target="_blank"><i class="fa fa-facebook"></i></a></div>
						</div>
					<?php
					endforeach;
					?>
					</div> <!-- End Team feed -->
					<?php } else
					{ ?>
						<div class="no-teams">
							<p>No teams right now, but check back soon. New teams are added all the time! Maybe check out some of our other available teams.</p>
							<a href='/find-a-team' class='button align-center'>Find a Team</a>
							<p>Have a location and program in mind, but don't see what you're looking for?</p>
							<a href='/custom-team' class='button align-center'>Create a Team</a>
						</div>
					<?php } ?>		
			</div> <!-- End sidebar -->

        </div> <!-- end #inner-content -->
        <div id="staff" class="row">
        <h2><?php the_title(); ?>'s Staff</h2>
        <p>We love our staff and we are positive that you will too! Each staff member is professional, courteous, and has the same passion as you do: to serve others.</p>
			<?php
			if(isset($users) && count($users)>0){
				$cnt = 1;
				foreach($users as $user_id=>$user_data)
				{
					$user = new USER();
					$image = $user->avatar($user_id);

					echo "<div class='large-3 medium-4 columns'>";
                    echo "<div class='staff-profile'>";
                	echo "<div class='avatar' style='background: url(" . $image . "); 
								background-size: cover; background-position: center center;'></div>";
					echo "<h5>" . $user_data['first_name'] . " " . $user_data['last_name'] . "</h5>";
                    echo "<em>" . $user_data['Title'] . "</em>";
					echo "</div>";
                    echo "</div>";
				}
			}
			?>
        </div>
    <?php edit_post_link('edit'); ?>
    </div> <!-- end #content --> 

<?php get_footer()?>
