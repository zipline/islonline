<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800');
.internal h2, .internal p {
	font-family: 'Montserrat', sans-serif;
	font-weight: 500;
}
.get-started h2 {
    color: #F15A29 !important;
	font-weight: 700;
}
	
.form_tabs {
    padding: 20px 0;
}
	.form_tabs button {
		border: 2px solid #F15A29;
		padding: 18px 32px;
		color: #F15A29;
		font-family: 'Montserrat', sans-serif;
		font-weight: 500;
		font-size: 16px;
		float: left;
		display: block;
		width: 100%;
		margin-bottom: 10px;
		border-radius: 0;
		margin-right: 13px;
		transition: 0.3s all;
	}
		.form_active {
			background: #F15A29;
			color: #fff !important;
		}

@media screen and (min-width: 768px) {

	
	.join {
		display: flex;
		align-items: center;
	}
	
	.sustain {
		display: flex;
		align-items: center;
	}
	
	.mission {
		display: flex;
		align-items: center;
	}
	
	.contact_field li, .schedule_field li {
		width: 100px;
		display: inline-block;
		margin-right: 10px !important;
	}
	
	li.gchoice_28_4_1, li.gchoice_28_5_1 {
		width: auto;
		display: inline-block;
	}
}

@media screen and (max-width: 767px) {
		.form_tabs button {
			margin-bottom: 10px;
			width: 100%;
		}
		.get-started {
			padding: 40px 0 !important;
		}
}
	
.size_field select {
    height: 50px;
    border: 2px solid #888;
    border-radius: 0;
    padding: 0px 10px !important;
	width: 100% !important;
}
	
.highlight {
	background: #eee;
	transition: 0.3s all;
}

.gfield label {
	display: inline-block !important;
}
	
.need_help input[type=text] {
    height: 50px;
    border-radius: 0;
    border: 2px solid #888;
	width: 100% !important;
}
	
.need_help .ginput_container span {
    width: 100% !important;
}
	
.need_help .ginput_container .gfield_radio label {
	position: relative;
	top: -5px;
	font-size: 16px;
	color: #444;
	padding-left: 3px;
	font-family: 'Montserrat', sans-serif;
	font-weight: 500;
}

.ISLCheckbox ul input[type=checkbox]:checked + label {
    background: #888;
    color: #fff;
}
	
.ISLCheckbox ul li {
    display: inline;
}
.ISLCheckbox ul li label {
	font-size: 16px !important;
	color: #333;
	font-family: 'Montserrat';
	margin-bottom: 7px !important;
	font-weight: 500;
	border: 2px solid #888 !important;
	padding: 10px 20px !important;
	display: inline-block !important;
	width: auto !important;
}
	
.ISLCheckbox input {
    display: none !important;
}
	
.need_help label.gfield_label.gfield_label_before_complex {
    display: none !important;
}
	
.ginput_container.ginput_container_checkbox {
    margin-top: 0 !important;
}
	
.need_help label {
    font-size: 16px !important;
    letter-spacing: 0 !important;
    color: #888;
    font-weight: 500 !important;
    font-family: 'Montserrat';
}
	
span.gfield_required {
    display: none;
}
	
body .gform_wrapper ul li.gfield {
	margin-top: 0 !important;
}
	
label.gfield_label {
    padding-top: 10px;
}
	
span#input_28_1_6_container, span#input_29_1_6_container, span#input_30_1_6_container {
    margin-top: 10px;
    margin-bottom: 10px;
}
	
.gfield {
    padding: 10px;
}
	
.internal .gform_footer.top_label input[type=submit] {
    float: left;
    border-radius: 0;
    font-family: 'Montserrat';
    font-weight: 500;
    background: #43B1E2;
}
	
.banner {
    height: 290px !important;
    background-repeat: no-repeat;
    background-size: cover;
    /* position: relative; */
    /* display: flex; */
    /* align-items: center; */
	font-family: 'Montserrat', sans-serif;
}
	
h2.blue {
    font-weight: 700;
    color: #43B1E2;
	margin-top: 70px;
}
	
h2.dark_blue {
	color: #265592;
	font-weight: 700;
}
	
li.gfield {
    padding: 0 !important;
}
	
@media screen and (max-width: 767px) {
	.dark_blue {
		margin-top: 50px;
	}
}
</style>

<?php
	/*
	Template Name: Start Your Journey (Internal)
	*/

	get_header();
?>
<div class="internal">

<?php get_template_part( 'banner' ); ?>

    <div id="content">
    
        <div id="inner-content" class="row">
    				<div class="large-7 columns">
					<h2 class="dark_blue">
						<?php the_field('first_section_header'); ?>
					</h2>
					<p>
						<?php the_field('first_section_text'); ?>					
					</p>
					<img src="<?php the_field('first_section_image'); ?>">
						<h2 class="blue">
							<?php the_field('second_section_header'); ?>
						</h2>
						<p>
							<?php the_field('second_section_text'); ?>
						</p>
						<img src="<?php the_field('second_section_image'); ?>">
				</div>
				<div class="large-5 columns get-started">
					<h2>
						GET STARTED
					</h2>
					<p>
						Fill out the form below, and one of our Placement Specialists will reach out to you as soon as possible!
					</p>

					<div class="form_tabs">
						<button class="form_btn tablink form_active" onclick="openForm(event,'learn')">Learn More Information</button>
						<button class="form_btn tablink" onclick="openForm(event,'select')">I Need Help Selecting a Program</button>
						<button class="form_btn tablink" onclick="window.location.replace('https://islonline.org/custom-programs/');">I would like to create a Custom Program</button>
					</div>

					<div id="learn" class="form">
						<?php gravity_form( 51, false, false, false, '', false ); ?>
					</div>

					<div id="select" class="form" style="display:none">
						<?php gravity_form( 52, false, false, false, '', false ); ?>
					</div>

					<div id="create" class="form" style="display:none">
						<?php gravity_form( 53, false, false, false, '', false ); ?>
					</div>
				</div>
 
        </div> <!-- end #inner-content -->
    <?php edit_post_link('edit'); ?>
    </div> <!-- end #content --> 
	
</div>
<?php get_footer(); ?>

<script>
function openForm(evt, formName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("form");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" form_active", "");
  }
  document.getElementById(formName).style.display = "block";
  evt.currentTarget.className += " form_active";
}
	
$('input').focus(
    function(){
        $(this).closest('.gfield').addClass('highlight');
    });

$('input').blur(
    function(){
        $(this).closest('.gfield').removeClass('highlight');
    });
</script>