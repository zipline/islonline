<?php
/*
Template Name: User Dashboard
*/
?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>
<?php
//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "rsvp.class.php";
require_once $plugin_path . "finance.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "message.class.php";

$user = new USER();
$team = new TEAM();
$rsvp = new RSVP();
$finance = new FINANCE();
$country = new COUNTRY();
$message = new MESSAGES();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(isset($_GET['rsvp']) && $_GET['rsvp'] > 0 && !$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;


$team->set_team($_GET['team_id']);
$countries = array();
foreach($team->get_schedule() as $data)
{
	$country->set_country($data['country']);
	$country->set_region($data['region']);
	$this_region = $country->region_details();

	if(!isset($countries[$data['country']]))
	{
		$countries[$data['country']] = $country->get_name();
	}

	if($this_region['title'] != 'ALL')
	{
		if(substr($countries[$data['country']], -3) != ' & ')
		{
			$countries[$data['country']] .= ": ";
		}
		$countries[$data['country']] .= $this_region['title'] . ' & ';
	}
}

foreach($countries as $index => $cntry_data)
{
	if(strlen($cntry_data) > 2 && substr($cntry_data,-3) == ' & ')
	{
		$countries[$index] = substr($countries[$index], 0, -3);	
	}
	if(strlen($cntry_data) > 2 && substr($cntry_data,-1) == ':')
	{
		$countries[$index] = substr($countries[$index], 0, -1);	
	}
}

//$country_list = $country->display_country_list($country->country_array($team->schedule_countries()));
$country_list = implode(' & ', $countries);

if(!empty($_GET['rsvp']))
{
	$rsvp->set_rsvp_id($_GET['rsvp']);
	$rsvp->fetch_rsvp();
	$reservation = $rsvp->get_rsvp();
	$item = $finance->fetch_item($reservation['order_item_id']);
	$add_ons = $finance->add_ons($reservation['order_item_id']);
	
	$all_items[] = $reservation['order_item_id'];
	
	$start = strtotime(str_replace("-","/",$team->arrival_date()));
	$now = time();
	if($start - $now < 0)
	{
		$history = 1;
	}
	else
	{
		$history = 0;
	}
	
	$meta_team_staff_key = 'cmd_team' . $_GET['team_id'] . '_staff_pos';	
	
	//
	//Gather Defined Data here
	$current_users = array();
	$query = "SELECT um.user_id, um.meta_value, u.display_name
				FROM " . $wpdb->prefix . "usermeta um INNER JOIN
					 " . $wpdb->prefix . "users u on u.ID = um.user_id 
			  WHERE meta_key = '" . $meta_team_staff_key . "' and um.meta_value='VEC'
			  ORDER BY display_name";
	$possibles = $wpdb->get_results($query, ARRAY_A);

	$vec_name = "";
	foreach($possibles as $data)
	{
		$vec_name = $data['display_name'];
		$vec_url = get_user_meta($data['user_id'],'local_avatar',true); 
	}
	if(empty($vec_url))
	{
		$vec_url = "/wp-content/uploads/2016/03/avatar.png";
	}
	
	$cancel_text = get_post_meta($reservation['post_id'], '_cmd_cancel_request', true);

	if( empty( $cancel_text ))
	{
		$cancel_text = 'Request Cancellation';
	}

}
else
{
	?>
<div id="inner-content" class="row">
	<style type="text/css" scoped>
    div.msg_body { display: none; }
    div#inner-content {height: 0; }
    </style>
	<BR><BR>
 </div>

<div id="featured-trips">
    <h3>Teams you may be interested in</h3>

		<?php
			$program = new PROGRAM();
			$country = new COUNTRY();
			$team = new TEAM();
			
			$featured = explode(",",$team->featured_teams());

			foreach($featured as $team_id)
			{
				$team->set_team($team_id);
				$post_id = $team->value('post');
				$trip_date = date("M Y", strtotime($team->arrival_date()));
				$program->set_program(@array_shift($team->schedule_programs()));
				$prgm = $program->get_name();
				$country->set_country(@array_shift($team->schedule_countries()));
				$ctry = $country->get_name();
				
				
				if(has_post_thumbnail($post_id))
				{
					$source = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'gallery-thumb');	
					$image = $source[0];
				}
				else
				{
					$pid = $program->post_id();
					$source = get_field("image", $pid)['sizes']['gallery-thumb'];
					
					if($source)
					{
						$image = $source;
					}
					else 
					{
						$cid = $country->post_id();
						$source = get_field("image", $cid)['sizes']['gallery-thumb'];
						$image = $source;
					}
				}
		?>

			<div id="trip-feed" class="row collapse">
				<a href="<?php _e(the_permalink($post_id))?>">
        		<div class="team-thumb medium-2 columns" style="background-image: url(<?php echo $image?>); background-size: cover; background-position: center center;">
				</a>
				</div>
				<div class="medium-7 columns">
					<div class="team">
						<h5><?php _e($team->value('hash'))?></h5>
						<!--Program--><h6><strong><?php _e($prgm)?></strong> in <strong><?php _e($ctry)?></strong></h6>
						<p class="description"><?php echo urldecode($team->get_blurb('teaser'));?></p>                       
					</div>
				</div>
				<div class="medium-2 columns">
					<h6><strong><?php _e($trip_date)?></strong></h6>
					<h6>$<?php echo $team->value('cost')?></h6>
					<a class="small button" href="<?php _e(the_permalink($post_id))?>" target="_parent">Details</a>
				</div>
			</div>
			<hr />
		<?php
			}
		?>
	 <!-- End Wrapper-->
</div>    
<?php
	exit;
}

/** process read/hide */
if(isset($_REQUEST['read_message']))
{
	$message->read_msg($_REQUEST['message_id']);
	return;
}
if(isset($_REQUEST['hide_message']))
{
	$message->hide_msg($_REQUEST['message_id']);
	return;
}

/** get messages for user & team */
if(isset($_REQUEST['show_all_msgs']))
{
	$messages = $message->get_recipient_messages($current_user->ID,$_GET['team_id'],false);
}
else if(isset($_REQUEST['hide_read_msgs']) || !isset($REQUEST['hide_read_msgs']))
{
	$messages = $message->get_recipient_messages($current_user->ID,$_GET['team_id'],false,true);
}

/** Get team message to be used on the page*/
$team_msg = get_post_meta($team->value('post'),'_public_team_note',true);

// Team Image
$team_image_id = get_post_meta($team->value('post'), '_thumbnail_id', true);
$team_image_url = get_post_field ('guid', $team_image_id, 'large');

?>
<div id="inner-content">
	<main id="main" role="main">
		<div id="dashboard">
			<div class="header-info row collapse">
<!-- 				<?php if ($team_image_id) : ?>
				<div class="team-image-container small-12 columns">
					<div class="banner" style="background-image: url('<?=$team_image_url; ?>');"></div>
				</div>
				<?php endif; ?> -->
				
				<div class="team-details-container medium-5 columns">
					<h4 class="section-label">Program details</h4>
					
					<h3><?php _e($team->value('hash'))?></h3>

					<div class="team-details">
						<dl>
							<dt>Country:</dt>
								<dd><?php _e($country_list)?></dd>
							<dt>Arrival Date:</dt>
								<dd><?php _e($team->arrival_date())?></dd>
							<dt>Departure Date:</dt>
								<dd><?php _e($team->departure_date())?></dd>
							<?php
							if($team->value('no_finances') == 0)
							{
								?>
							<dt>Payments Due:</dt>
								<dd><span id="balance_due"></span> by <?php echo date_format(date_sub(date_create($team->arrival_date()), date_interval_create_from_date_string("45 day")), "m/d/Y");?></dd>
								<?php
							}
							?>
						</dl>

						<dl>
							<dt>Duration:</dt>
								<dd><?php _e($team->trip_duration())?> days</dd>
							<dt>Arrival City:</dt>
								<dd><?php _e($team->value('arrival_city'))?></dd>
							<dt>Departure City:</dt>
								<dd><?php _e($team->value('departure_city'))?></dd>
						</dl>
					</div>
				</div>
				<?php
				if(!empty($vec_name)) { ?>
				<div class="vec-info-container small-6 medium-3 columns">
					<div class="vec-info">
						<div class="avatar" style="background-image: url(<?php echo $vec_url; ?>);"></div>
						<h5 class="vec-name"><?php echo $vec_name?></h5>
						<p class="vec-desc">is your Volunteer Experience Coordinator</p>

					</div>
				</div>

				<div class="vec-message-container small-6 medium-4 columns">
					<p class="vec-message"><?php echo nl2br($team_msg); ?></p>
				</div>
				<?php } ?>
			</div>

			<div class="messages-container">
				<h4 class="section-label">
					<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
						<?php if(isset($_REQUEST['show_all_msgs'])) : ?>
						<button id="unread-hide-toggle" class="float-right" type="submit" name="hide_read_msgs">Hide read messages</button>
						<?php elseif (isset($_REQUEST['hide_read_msgs']) || !isset($_REQUEST['show_all_msgs'])) : ?>
						<button id="unread-hide-toggle" class="float-right" type="submit" name="show_all_msgs">Show all messages</button>
						<?php endif; ?>
					</form>
					Team Message Center
				</h4>

				<ol class="team-messages-list">
					<?php 
					foreach($messages as $message) :
						$read = ($message->read_at == NULL ? 'unread' : 'read');

						$messageSubject = $message->subject;
						if(strlen($messageSubject) > 80) {
							$messageSubject = substr($messageSubject,0,77) ."...";
						}
					?>
					<li class="message <?php echo $read; ?> closed" data-message-id="<?php echo $message->id; ?>" >
						<h5 class="message-subject"><?= $messageSubject; ?></h5>
						<div class="message-body-wrap">
							<div class="message-body">
								<?php echo stripslashes(stripslashes(html_entity_decode($message->msg_text))); ?>

								<div class="message-footer">
									<?php echo date("n/j/o g:ia",strtotime($message->created_at)); ?>
								</div>
							</div>
						</div>

						<button class="message-toggle-trigger tiny button"></button>
					</li>
					<?php endforeach; ?>
				</ol>
			</div>

			<script>
				$(document).ready(function() {
					$('.message-toggle-trigger').on('click', function(event) {
						var $message = $(this).parent('.message'),
							$messageBody = $message.find('.message-body'),
							$messageSubject = $message.find('.message-subject'),
							$messageBodyWrap = $message.find('.message-body-wrap');

						// Open...
						if ($message.hasClass('closed')) {
							$message.removeClass('closed').addClass('open');
							$messageBodyWrap.css('height', $messageBody.outerHeight());

							$('.message').addClass('dim');
							$message.removeClass('dim');
						} else 
						// Close and mark as read
						if ($message.hasClass('open')) {
							var updateDB = ($message.hasClass('unread'));
							$message.removeClass('open').addClass('closed');
							$message.removeClass('unread').addClass('read');
							$messageBodyWrap.css('height', 0);
							$('.message').removeClass('dim');

							// Tell the db that this message has been 'read'...
							if (updateDB) {
								$.ajax({
									url : "<?php echo $_SERVER['REQUEST_URI']; ?>",
									type: 'post',
									data : {
										read_message: 'read_message',
										message_id: $message.data('message-id')
									}
								});
							}
						}

						// Tell the parent iframe to resize...
						parent.autoResize('myisl_canvas');
					});
				});
			</script>

			<?php
			if($team->value('no_finances') == 0)
			{
				?>
			<div class="financial-details-container row collapse expanded">
				<h4 class="section-label">Financial Details</h4>

				<div class="medium-6 columns first-column">
					<h5>My Order</h5>
					<table class="order">
						<?php
							$items = $finance->Fetch_Items( array( $reservation['order_item_id'] ), true, true );
	
							foreach( $items as $id => $data )
							{
								echo '<tr class="">';
								echo '<td>' . $data->item_verbiage . '</td>';
								echo '<td align="right">$' . number_format( $data->price, 2 ) . '</td>';
								echo '<tr>';
								
								$all_items[] = $data->id;
							}
							
							echo "<tr class='totals-row'>";
							echo "<td>Grand Total</td>";
							echo "<td align='right'>$" . number_format( $finance->Calculate_ItemsCost(), 2 ) . "</td>";
							echo "</tr>";
						?>
					</table>
					<a data-open="cancel-form" id="cancel_form"><?php echo ($cancel_text)?></a>
					<div id="cancel-form" class="reveal" data-reveal>
						<h4>Cancel Your ISL RSVP</h4>
						<p>$85 app fee is non-refundable.</p>
						<p>Cancellations for any reason more than 45 days prior to departure date receive a full refund,15 to 45 days prior to departure date receive up to 80%, and less than 15 days prior to departure date receive no refund.</p>
						<p>In the event that ISL must cancel a team, participants have the option of receiving a full refund or transferring to another team.</p>
						<a href="/about/refund-policy/" target="_blank">View full refund policy</a>
						<p>&nbsp;</p>
					    <div>
					    	<form>
								<textarea cols=60 rows=5 placeholder="Cancellation Reason" id="cancel_reason" name="cancel_reason"></textarea>
								<a class="button" id="cancel_button" name="cancel_button"><?php _e($cancel_text)?></a>
								<input type="hidden" id="cancel_rsvp" name="cancel_rsvp" value="<?php echo $_GET['rsvp']?>">
							</form>
				        </div>
					    <!--<a class="button">Cancel RSVP</a>-->
					    <button class="close-button" data-close aria-label="Close modal" type="button">
								<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				<div class="medium-6 columns">
					<h5>My Payments</h5>
					<table class="order">
						<?php
						$pays = 0;
						$costs = $finance->items_cost($all_items, true);
						foreach($finance->payments($all_items) as $payments)
						{
//echo var_dump($payments);
//							echo var_dump($payments);
							if((int)$payments['amount'] > 0 || $costs[$payments['order_item_id']]['item_type'] == 1)
							{
								$item = '';
								$override_item = false;
							
								if(!empty($payments['special']) && ($payments['special'] == 'donate refund' || $payments['special'] == 'donate' || $payments['special'] == 'donate anyonymous'))
								{
									$item_desc = "Donation";
									$override_item = true;
								}
								if(empty($payments['special']) && !empty($payments['note']))
								{
									$item_desc = $payments['note'];
									$override_item = true;
								}


								if(!$override_item)
								{
									$item_desc = $finance->Fetch_ItemVerbiage($costs[$payments['order_item_id']]['item_type']);
								}

								if(floatval($payments['amount']) < 0)
								{
									//$item_desc = "Refund for " . $item_desc;
									$item_desc = $item_desc;
								}

								echo "<tr>
								<td>" . $payments['created_when'] . "</td>
								<td>" . $item_desc . "</td>
								<td align='right'>$" . number_format($payments['amount']*(-1),2) . "</td></tr>";
								$pays += $payments['amount'];	
							}
						}

						echo "<tr class='totals-row'><td colspan='2'>Balance Due</td><td align='right'>$" . number_format($finance->Calculate_ItemsBalance(),2) . "</td></tr>";
						?>
					</table>

					<?php
					if(!$history && $finance->Calculate_ItemsBalance() > 0) :
						$paymentUrl = "/payment/?team_id=" . $_GET['team_id'] . "&member_id=" . get_current_user_id() . "&rsvp={$rsvp->get_rsvp_id()}" ?>
					<a class='make-payment-button button float-right' href='<?php echo $paymentUrl;?>' target='_top'>Make a Payment now</a>
					<?php endif; ?>
					
				</div>
			</div>
       		<?php
			}
			?>
        </div>
    </main>
    
</div> <!-- end #inner-content --> 


 
<?php
	wp_enqueue_script('cmd_cancel_rsvp', plugins_url() . '/command/js/cancel.rsvp.js', array('jquery'));	
	wp_localize_script('cmd_cancel_rsvp', 'svars', array('ajax' => admin_url('admin-ajax.php')));

/*if(!empty($balance_due))
{
	?>
    <script>
		var rsvp_link = window.parent.document.getElementById('order');
		rsvp_link.innerHTML = "RSVP ($<?php _e($balance_due);?>)";
	</script>
    <?php	
}*/



?>


<?php wp_footer() ?>