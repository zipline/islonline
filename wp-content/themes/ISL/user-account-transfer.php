<?php
/*
Template Name: User Transfer
*/
?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>
<?php
//
//Setup classes
include_once(CC_CLASSES_DIR . 'finance.class.php');
include_once(CC_CLASSES_DIR . 'rsvp.class.php');

$rsvp = new RSVP();
$finance = new FINANCE();
$team = new TEAM();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;

if(!empty($_GET['rsvp']))
{
	$rsvp->set_rsvp_id($_GET['rsvp']);
	$rsvp->fetch_rsvp();
	
	$reservation = $rsvp->get_rsvp();
}

//
//Reservation give-aways
//
$gives = $rsvp->by_user($reservation['user_id']);

if(!empty($reservation))
{
	//$team = new TEAM();
	$team->set_team($reservation['team_id']);
	
	$gives[$reservation['id']] = array('team' => $reservation['team_id'],
									   'post' => $reservation['post_id'],
									   'hash' => $team->value('hash'),
									   'start_date' => $team->arrival_date(),
									   'set' => true
									  );
}

if(!empty($gives))
{
	wp_enqueue_script('cmd_share_rsvp', plugins_url() . '/command/js/share.rsvp.js', array('jquery'), '4.7.0');	
	wp_localize_script('cmd_share_rsvp', 'svars', array('ajax' => admin_url('admin-ajax.php')));

	echo "<HR>";
	echo "<h3>Transfer RSVPs</h3>";
	echo "<p>You have purchased " . count($gives) . " reservation" . (count($gives)>1 ? "s" : '') . ", which you can give away.</p>";
	echo "<p>If you have purchased an trip for yourself and cannot travel, or have purchased a trip as a gift, you may email an invitation to that RSVP below. <strong>You can 'Revoke' your invitation anytime prior to their acceptance of it.</strong></p>";

	echo "<div class='row small-collapse'>";
		echo "<div class='medium-5 columns small-collapse'>";
			echo "<div class='small-8 columns'><b>Team</b></div>";
			echo "<div class='small-4 columns'><b>Date</b></div>";
		echo "</div>";
		echo "<div class='medium-7 columns small-collapse show-for-medium'>";
			echo "&nbsp;";
		echo "</div>";
	echo "</div>";

	echo "<div id='rsvpGive' class='row small-collapse'>";
	foreach($gives as $res_id => $res_data)
	{
		//
		//See if we've already passed this one out...
		$give_email = get_post_meta($res_data['post'], '_cmd_rsvp_shared_to', true);
		$give_email_add = '';
		$title = "give_" . $res_data['post'] . "_" . $reservation['user_id'] . "_" . $res_id;

		if(!empty($res_data['set']) && $res_data['set'] == true && empty($give_email))
		{
			$give_email_add = "Assigned to you!";
		}
		
		echo "<div class='medium-5 columns small-collapse'>";
			echo "<div class='small-8 columns'>" . $res_data['hash'] . "</div>";
			echo "<div class='small-4 columns'>" . date("m/d/Y",strtotime(str_replace("-","/",$res_data['start_date']))) . "</div>";
		echo "</div>";
		echo "<div class='medium-7 columns small-collapse'>";
			echo "<div class='medium-7 columns'>
							<input type='text' 
							   size='50' 
							   maxlength='50' 
							   placeholder=\"Enter friend's email address\" 
							   name='" . $title . "' 
							   id='" . $title . "'
							   " . (($give_email) ? ' disabled' : '') . "
							   value='" . (($give_email . $give_email_add) ? $give_email . $give_email_add : "") . "'
						></div>";

			echo "<div class='medium-5 columns'>
					<input type='button' 
							   value='Give' 
							   class='give_button button' 
							   id='" . $title . "_g_button'
							   " . (($give_email) ? ' disabled' : '') . ">";
			echo "<input type='button' 
							   value='Revoke' 
							   class='revoke_button button' 
							   id='" . $title . "_r_button'
							   " . (($give_email) ? '' : ' disabled') . ">
				</div>";
		echo "</div>";	
	}
	echo "</div>";
		
}
else
{
	echo "You've only purchased reservations for yourself thus far. ";
	echo "Why don't you invite a friend? ";
	echo "If you purchase another reservation, you'll be able to transfer it to that friend and bring them along on your adventure.";
	
}
?>

<?php wp_footer(); ?>
