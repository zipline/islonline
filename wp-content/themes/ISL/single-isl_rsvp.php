<?php
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "rsvp.class.php");
$rsvp = new RSVP();
?>

<?php
wp_enqueue_script('cmd_school_selector', plugins_url() . '/command/js/school.selector.js', array('jquery', 'jquery-ui-autocomplete'), NULL, true );
wp_localize_script('cmd_school_selector', 'svars', array('ajax_url' => admin_url('admin-ajax.php')));
?>

<?php acf_form_head(); ?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>

<style>
	.ui-menu 
	{
		background-color: black;
		text-align: left;
	}
	.ui-menu-item
	{
		background-color: black;
		color: white;
	}
	.ui-menu-item:hover
	{
		background-color: blue;
		color: white;
	}

</style>

<?php 

$updatedMsg = "<div class='success callout' data-closable>
  <p>Your information as been updated!</p>
  <button class='close-button' aria-label='Dismiss alert' type='button' data-close>
    <span aria-hidden='true'>&times;</span>
  </button>
</div>";

acf_form(array('html_updated_message' => $updatedMsg)); 

if($rsvp->profile_complete(get_the_ID()))
{
	?>
	<script>
		parent.complete_item('trip_link');		
	</script>
	<?php
}
else
{
	?>
	<script>
		parent.incomplete_item('trip_link');		
	</script>
	<?php
}
?>

<?php wp_footer(); ?>
<!-- </body>	 -->
<!-- </html> -->
