<?php
/*
Template Name: Find a Team
*/
?>
<?php
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style',
                 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "rsvp.class.php";


//
//Gather Team, Country, Program Details
//
$country = new COUNTRY();
$program = new PROGRAM();
$team = new TEAM();
$rsvp = new RSVP();

$search_team = isset($_POST['search_form']) ? $_POST['search_form'] : '';
$sel_prog = isset($_POST['program']) ? $_POST['program'] : 0;
$sel_cntry = isset($_POST['country']) ? $_POST['country'] : 0;
$trip_length = isset($_POST['trip_length']) ? $_POST['trip_length'] : 0;
$order_by = isset($_POST['order_by']) ? $_POST['order_by'] : 'date_early';
$view_by = isset($_POST['view_by']) ? $_POST['view_by'] : 'card';

$beginning_date_month = isset($_POST['beginning_date_month']) ? $_POST['beginning_date_month'] : date('m');
$beginning_date_year = isset($_POST['beginning_date_year']) ? $_POST['beginning_date_year'] : date('Y');
$ending_date_month = isset($_POST['ending_date_month']) ? $_POST['ending_date_month'] : '12';
$ending_date_year = isset($_POST['ending_date_year']) ? $_POST['ending_date_year'] : date("Y")+4;

$begin_date = $beginning_date_month . "/01/" . $beginning_date_year;
$end_date = $ending_date_month . "/01/" . $ending_date_year;


if(!empty($search_team))
{
    $team_id = $team->find_team_by_name($search_team);
    if(!empty($team_id))
    {
        $team->set_team($team_id);
        $post = $team->value('post');
        $url = get_permalink($post);
        
        header ('Location: ' . $url);
        exit;       
        $coming = array();      
    }
    else
    {
        $coming = array();      
    }
}
else
{
    $coming = $team->upcoming($sel_prog, $sel_cntry, $trip_length, $begin_date, $end_date, $order_by);
}

?>
<?php get_header(); ?>
<?php
    $src = get_field('image')['sizes']['large'];     
    $vposition = get_field('image_position'); 
    $hposition = get_field('image_position_hz');
    $color = get_field('title_color');
    $overlay = get_field('banner_image_overlay');
?>

<div class="short-banner find-a-team" style="background-image: url(<?php echo $src ?>); background-position: <?php echo $vposition; ?> <?php echo $hposition; ?>;">
    <header class="entry-header">
        <h1 class="entry-title" style="color: <?php echo $color; ?>;"><?php the_title(); ?></h1>
    </header>  

    <div class="team-code-search-container">
        <form role="search" class="search-form" method="POST" enctype="multipart/form-data" action="<?php _e($_SERVER['REQUEST_URI'])?>">
            <label>
                <input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Custom Program Code', 'jointswp' ) ?>" id="search_form" name="search_form" value="<?php echo $search_team ?>"/>
            </label>
            <input type="submit" class="search-submit button" value="<?php echo esc_attr_x( 'Lookup', 'jointswp' ) ?>" />
        </form>
    </div>
</div>

<div class="search-bar row">
    <form name="opts" id="opts" method="POST" enctype="multipart/form-data" action="<?php _e($_SERVER['REQUEST_URI'])?>">
        <div class="country search-selector">
            <label for="country"><strong>WHERE</strong> do you want to go?:</label>
            <div class="select-wrap <?php //echo ($sel_cntry ? 'selected' : ''); ?>">
                <select name="country" id="country" onchange="javascript:document.getElementById('opts').submit()"><?php _e($country->selector($sel_cntry, true, true, $sel_prog))?></select>
                <i class="fa fa-chevron-down"></i>
            </div>
        </div>

        <div class="program search-selector">
            <label for="program"><strong>WHAT</strong> do you want to do?:</label> 
            <div class="select-wrap <?php //echo ($sel_prog ? 'selected' : ''); ?>">
                <select  name="program" id="program" onchange="javascript:document.getElementById('opts').submit()"><?php _e($program->selector($sel_prog, true, false, true, $sel_cntry))?></select>
                <i class="fa fa-chevron-down"></i>
            </div>
        </div>

        <div class="date-range search-selector" id="date-range-select-group">
            <label for="date-range"><strong>WHEN</strong> do you want to go?</label>        
            <div class="select-wrap">
                <span id="date-range-picker-trigger">Pick a date range</span>
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-chevron-up"></i>
            </div>

            <!-- the date-range picker popover -->
            <div class="date-range-picker">
                <div class="beginning-group">
                    <label for="beginning-date">Beginning:</label>
                    <div class="select-wrap">
                        <select name="beginning_date_month" id="beginning_date_month">
                            <option value="01"<?php echo ($beginning_date_month == '01') ? ' SELECTED' : '' ?>>January</option>
                            <option value="02"<?php echo ($beginning_date_month == '02') ? ' SELECTED' : '' ?>>February</option>
                            <option value="03"<?php echo ($beginning_date_month == '03') ? ' SELECTED' : '' ?>>March</option>
                            <option value="04"<?php echo ($beginning_date_month == '04') ? ' SELECTED' : '' ?>>April</option>
                            <option value="05"<?php echo ($beginning_date_month == '05') ? ' SELECTED' : '' ?>>May</option>
                            <option value="06"<?php echo ($beginning_date_month == '06') ? ' SELECTED' : '' ?>>June</option>
                            <option value="07"<?php echo ($beginning_date_month == '07') ? ' SELECTED' : '' ?>>July</option>
                            <option value="08"<?php echo ($beginning_date_month == '08') ? ' SELECTED' : '' ?>>August</option>
                            <option value="09"<?php echo ($beginning_date_month == '09') ? ' SELECTED' : '' ?>>September</option>
                            <option value="10"<?php echo ($beginning_date_month == '10') ? ' SELECTED' : '' ?>>October</option>
                            <option value="11"<?php echo ($beginning_date_month == '11') ? ' SELECTED' : '' ?>>November</option>
                            <option value="12"<?php echo ($beginning_date_month == '12') ? ' SELECTED' : '' ?>>December</option>
                        </select>
                        <i class="fa fa-chevron-down"></i>
                    </div>
                    <div class="select-wrap">
                        <select name="beginning_date_year" id="beginning_date_year">
                            <?php
                            // just get the next 5 years (who plans farther out than that?!)
                            for($i = date("Y"); $i < date("Y")+5; $i++){
                                echo "<option value='".$i."'" . (($beginning_date_year == $i) ? ' SELECTED' : '') . ">".$i."</option>";
                            } ?>
                        </select>
                        <i class="fa fa-chevron-down"></i>
                    </div>
                </div>

                <div class="ending-group">
                    <label>Ending:</label>
                    <div class="select-wrap">
                        <select name="ending_date_month" id="ending_date_month">
                            <option value="01"<?php echo ($ending_date_month == '01') ? ' SELECTED' : '' ?>>January</option>
                            <option value="02"<?php echo ($ending_date_month == '02') ? ' SELECTED' : '' ?>>February</option>
                            <option value="03"<?php echo ($ending_date_month == '03') ? ' SELECTED' : '' ?>>March</option>
                            <option value="04"<?php echo ($ending_date_month == '04') ? ' SELECTED' : '' ?>>April</option>
                            <option value="05"<?php echo ($ending_date_month == '05') ? ' SELECTED' : '' ?>>May</option>
                            <option value="06"<?php echo ($ending_date_month == '06') ? ' SELECTED' : '' ?>>June</option>
                            <option value="07"<?php echo ($ending_date_month == '07') ? ' SELECTED' : '' ?>>July</option>
                            <option value="08"<?php echo ($ending_date_month == '08') ? ' SELECTED' : '' ?>>August</option>
                            <option value="09"<?php echo ($ending_date_month == '09') ? ' SELECTED' : '' ?>>September</option>
                            <option value="10"<?php echo ($ending_date_month == '10') ? ' SELECTED' : '' ?>>October</option>
                            <option value="11"<?php echo ($ending_date_month == '11') ? ' SELECTED' : '' ?>>November</option>
                            <option value="12"<?php echo ($ending_date_month == '12') ? ' SELECTED' : '' ?>>December</option>
                        </select>
                        <i class="fa fa-chevron-down"></i>
                    </div>
                    <div class="select-wrap">
                        <select name="ending_date_year" id="ending_date_year">
                            <?php
                            for($i = date("Y"); $i < date("Y")+5; $i++){
                                echo "<option value='".$i."'" . (($ending_date_year == $i) ? ' SELECTED' : '') . ">".$i."</option>";
                            } ?>
                        </select>
                        <i class="fa fa-chevron-down"></i>
                    </div>
                </div>

                <a href="javascript:check_dates();" class="search-within-dates button">Search within selected dates</a>
            </div>
        </div>

        <div class="trip-length search-selector">
            <label for="trip-length"><strong>HOW LONG</strong> do you want to stay?</label>
            <div class="select-wrap <?php echo ($trip_length ? 'selected' : ''); ?>">
                <select name="trip_length" id="trip_length" onchange="javascript:document.getElementById('opts').submit()">
                    <option value=0 <?php selected($trip_length,0)?>> Pick a trip length</option>
                    <option value=1 <?php selected($trip_length,1)?>> Less than one week</option>
                    <option value=2 <?php selected($trip_length,2)?>> Up to two weeks</option>
                    <option value=3 <?php selected($trip_length,3)?>> Longer than two weeks</option>
                 </select>
                <i class="fa fa-chevron-down"></i>
            </div>
        </div>

        <a class="show-all-trips clearfix" href="javascript:clean_reload();">Show all available trips</a>
	<input type="hidden" name="order_by" id="order_by" value="<?php echo $order_by?>">
    </form>
</div>

<!-- /////////////////////////////////////////////// -->

<!-- CONTENT -->
<div id="content" class="search-results">

    <div id="inner-content" class="row">

        <main id="search-main" role="main">
            <h2>Upcoming Programs</h2>
            <div class="sort">
                <!-- <label>Filter Results:</label> -->
                <div class="view-buttons">
                    <a class="card-view <?php _e($view_by == 'card' ? 'active' : '')?>" onclick="javascript:change_view('card')"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                    <a class="list-view <?php _e($view_by == 'list' ? 'active' : '')?>" onclick="javascript:change_view('list')"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                </div>

                <select name="order_field" id="order_field" onchange="javascript:order_field()">
                    <option value="date_early" <?php selected($order_by, 'date_early')?>>
                        Date: Earliest first</option>
                    <option value="date_late" <?php selected($order_by, 'date_late')?>>
                        Date: Latest first</option>
                    <option value="price_low" <?php selected($order_by, 'price_low')?>>
                        Price: Low to High</option>
                    <option value="price_high" <?php selected($order_by, 'price_high')?>>
                        Price: High to Low</option>
                </select>
            </div> 

            <!-- Search Results -->
            <div class="team-results">
                <!-- Grid View -->
                <div id="grid" class="row small-up-1 medium-up-2 large-up-4 <?php _e($view_by == 'card' ? 'active' : '')?>" data-equalizer data-equalize-on="medium" data-equalize-by-row="true">
                    <?php

                    if(count($coming)>0)
                    {
                        foreach($coming as $data)
                        {
                            $programs = array();
                            $countries = array();
                            foreach(explode(",",$data['program_id']) as $program_id)
                            {
                                $program->set_program($program_id);
                                $programs[$program_id] = $program->get_name();
                            }
                            foreach(explode(",",$data['country_id']) as $country_id)
                            {
                                $country->set_country($country_id);

								$reg_label = '';
								if(isset($data['region_id']))
								{
									foreach(explode(',', $data['region_id']) as $region_ids)
									{
										$country->set_region($region_ids);
										$this_region = $country->region_details();

										if($this_region['country_id'] == $country_id && $this_region['title'] != 'ALL')
										{
											$reg_label .= $this_region['title'] . ' & ';
										}

									}	

									if(strlen($reg_label) > 2 && substr($reg_label,-3) == ' & ')
									{
										$reg_label = ': ' . substr($reg_label, 0, -3);	
									}
								}
								
								$countries[$country_id] = $country->get_name() . $reg_label;
                            }
                            
                            $stime = strtotime($data['start_date']);
                            $etime = strtotime($data['end_date']);
                            $format_stime = date("M d, Y", $stime);
                            $format_etime = date("M d, Y", $etime);
	                        $signed_up_count = $rsvp->count_rsvp($data['id']);
	                        $open = $data['minimum_volunteers'] - $signed_up_count;
                            $avail = max(0, $data['maximum_volunteers'] - $rsvp->count_rsvp($data['id']));
                            $sold_out = !(bool)$avail;
                            
                            if(has_post_thumbnail($data['post_id']))
                            {
                                $source = wp_get_attachment_image_src(get_post_thumbnail_id($data['post_id']), 'gallery-thumb');    
                                $image = $source[0];
                            }
                            else
                            {
                                $pid = $program->post_id();
                                $source = get_field('image', $pid)['sizes']['gallery-thumb'];
                                
                                if($source)
                                {
                                    $image = $source;
                                }
                                else 
                                {
                                    $cid = $country->post_id();
                                    $source = get_field("image", $cid)['sizes']['gallery-thumb'];
                                    $image = $source;
                                }
                            }
                    ?>
                    
                    <div class="column">
                        <a href="<?php _e($data['url'])?>" class="team" data-equalizer-watch>
                            <div class="team-banner" style="background-image: url('<?php echo $image; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center center;"></div>
                                <h5><?php _e($program->display_program_list($programs))?> <span>in</span> <?php _e($country->display_country_list($countries))?></h5>

                            <div class="details">
                                    <div class="dates">
                                        <div class="date"><!-- Start Date -->
                                            <span class="month"><?php _e(date("M", $stime))?></span><br />
                                            <span class="day"><?php _e(date("d", $stime))?></span><br />
                                            <span class="year"><?php _e(date("Y", $stime))?></span><br />
                                        </div>
                                        <div><b>to</b></div>
                                        <div class="date"><!-- End Date -->
                                            <span class="month"><?php _e(date("M", $etime))?></span><br />
                                            <span class="day"><?php _e(date("d", $etime))?></span><br />
                                            <span class="year"><?php _e(date("Y", $etime))?></span><br />
                                        </div>
                                    </div>
                                
                             
                                    <div class="description"><?php _e($data['teaser'])?></div>
                                    <div class="count">
	                                    <?php
		                                    echo "{$signed_up_count} Volunteer(s) on this program.";
	                                    ?>
                                    </div>
                            </div>
                            <div class="bottom">
                             <?php if($open >0 || $open <= 0 && !$sold_out ) { ?>
                                <h5 class="cost"><?php _e('$' . $data['cost'])?></h5>
                                <span href="<?php _e($data['url'])?>" class="button">Program Details</span>
                                <?php } if($sold_out) { ?>
                                <h5 class="sold-out"><del><?php _e('$' . $data['cost'])?></del> SOLD OUT!</h5>
                                <span href="<?php _e($data['url'])?>" class="button secondary disabled">Program Details</span>
                                <?php } ?>   
                            </div>
                        </a>
                    </div>
                    
                    <?php
                        }
                    }
                    else
                    {
                        echo "<div style='padding-left: 1.25rem;'>";
                        echo "There are no matching teams. Please try different criteria.";
                        echo "</div>";
                        
                    }
                    ?>
                </div><!-- End Grid View -->

                <!-- List View -->
                <div id="list" class="<?php _e($view_by == 'list' ? 'active' : '')?>">
                    <?php
                    //$coming = $team->upcoming($prg_filter, $cty_filter);

                    if(count($coming)>0)
                    {
                        foreach($coming as $data)
                        {
                            $programs = array();
                            $countries = array();
                            foreach(explode(",",$data['program_id']) as $program_id)
                            {
                                $program->set_program($program_id);
                                $programs[$program_id] = $program->get_name();
                            }
                            foreach(explode(",",$data['country_id']) as $country_id)
                            {
                                $country->set_country($country_id);
                                $countries[$country_id] = $country->get_name();
                            }
                            
                            $stime = strtotime($data['start_date']);
                            $etime = strtotime($data['end_date']);
                            $format_stime = date("M d, Y", $stime);
                            $format_etime = date("M d, Y", $etime);
	                        $signed_up_count = $rsvp->count_rsvp($data['id']);
	                        $open = $data['minimum_volunteers'] - $signed_up_count;
	                        $avail = max(0, $data['maximum_volunteers'] - $rsvp->count_rsvp($data['id']));
                            $sold_out = !(bool)$avail;

                            if(has_post_thumbnail($data['post_id']))
                            {
                                $source = wp_get_attachment_image_src(get_post_thumbnail_id($data['post_id']), 'single-post-thumbnail');    
                                $image = $source[0];
                            }
                            else
                            {
                                $pid = $program->post_id();
                                $source = get_field("image", $pid);
                                
                                if($source)
                                {
                                    $image = $source['url'];
                                }
                                else 
                                {
                                    $cid = $country->post_id();
                                    $source = get_field("image", $cid);
                                    $image = $source['url'];
                                }
                            }
                                ?>
                                <div class="team row">
                                    <div class="large-3 medium-4 columns">
                                        <h6><?php _e($program->display_program_list($programs))?> <span>in</span> <?php _e($country->display_country_list($countries))?></h6>
                                        <div class="date">
                                            <?php _e(date("M", $stime))?> <?php _e(date("d", $stime))?>, <?php _e(date("Y", $stime))?> to
                                            <?php _e(date("M", $etime))?> <?php _e(date("d", $etime))?>, <?php _e(date("Y", $etime))?>
                                        </div>
                                    </div>
                                    <div class="large-6 medium-2 columns">
                                        <div class="description"><?php _e($data['teaser'])?>
                                        <div class="count">
	                                        <?php
		                                        echo "{$signed_up_count} Volunteer(s) on this program.";
	                                        ?>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="large-1 medium-1 columns">
                                        <div class="price">
                                        <?php _e('$' . $data['cost'])?>
                                        </div>
                                    </div>
                                    <div class="large-2 medium-2 columns">
                                        <a href="<?php _e($data['url'])?>" class="button">Program Details</a>
                                    </div>
                                </div>
                        
                        <?php
                                }
                            }
                            else
                            {
                                echo "<p>";
                                echo "There are no matching teams. Please try different criteria.";
                                echo "</p>";
                            }
                        ?>
                </div> <!-- /List View -->
            </div> <!-- /Search Results -->
        </main> <!-- /#search-main -->
    </div> <!-- /#inner-content -->
</div>

<script>
    function clean_reload() 
	{
        document.getElementById('country').value = 0;
        document.getElementById('program').value = 0;
        document.getElementById('ending_date_month').value = '12';
        document.getElementById('ending_date_year').value = '<?php echo date("Y")+4?>';
        document.getElementById('beginning_date_month').value = '<?php echo date("m")?>';
        document.getElementById('beginning_date_year').value = '<?php echo date("Y")?>';
        document.getElementById('trip_length').value = 0;
        document.getElementById('opts').submit();
    }

    function order_field() {
        document.getElementById('order_by').value = document.getElementById('order_field').value;
        document.getElementById('opts').submit();   
    }

    function change_view(view) {
        document.getElementById('view_by').value = view;    
    }

	function check_dates()
	{
		var end_date_month = document.getElementById('ending_date_month').value;
		var end_date_year = document.getElementById('ending_date_year').value;
		var start_date_month = document.getElementById('beginning_date_month').value;
		var start_date_year = document.getElementById('beginning_date_year').value;

		var end_date = new Date(end_date_year, end_date_month);
		var start_date = new Date(start_date_year, start_date_month);

		if(end_date < start_date)
		{
			alert("The ending date occurs after the starting date.");
		}
		else
		{
			document.getElementById('opts').submit()
		}
	}
	
    jQuery(document).ready(function() {

		var dateRangePicker = jQuery('#date-range-picker-trigger');
        var dateRangeSelectGroup = jQuery('#date-range-select-group');

        dateRangePicker.on('click', function(event) {
            event.preventDefault();
            if (dateRangeSelectGroup.hasClass('active')) {
                dateRangeSelectGroup.removeClass('active');
            } else {
                dateRangeSelectGroup.addClass('active');
            }
        });
	});

</script>
<?php get_footer(); ?>