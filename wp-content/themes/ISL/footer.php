					<footer class="footer" role="contentinfo">
						<div id="inner-footer"> 
							<div class="row collapse" data-equalize-on="large" data-equalizer>
								<div class="large-7 medium-12 columns teal" data-equalizer-watch>
									<div class="section">
										<div class="logo"><?php get_template_part( 'svgs/svg', 'logo-white'); ?></div>
										<h4><span>1-877-779-8075</span></h4>
									</div>
								</div>
								<div class="large-5 medium-12 columns dkteal" data-equalizer-watch>
									<div class="search section">

										<form role="search" class="search-form" method="POST" enctype="multipart/form-data" action="<?php echo get_permalink(4411); ?>">
											<label>
												<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Custom Program Code', 'jointswp' ) ?>" id="search_form" name="search_form" />
											</label>
											<input type="submit" class="search-submit button" value="<?php echo esc_attr_x( 'Lookup', 'jointswp' ) ?>" />
										</form>

									</div>
								</div>
							</div>
							
							<div class="row collapse" data-equalize-on="medium" data-equalizer>
								<div class="large-7 medium-7 columns white bottom" data-equalizer-watch>
									<div class="section footer-menu">
									<nav role="navigation">
			    						<?php joints_footer_links(); ?>
			    					</nav>
			    					</div>
			    					<div class="large-12 medium-12 columns">
										<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.<br />
										<small><i>ISL with Globe and Heart of ISL logos, are registered trademarks of International Service Learning.</i></small></p>
										
									</div>
			    				</div>
			    				<div class="large-5 medium-5 columns dkblue" data-equalizer-watch>
			    					<div class="newsletter section">
			    					<div class="sm-icons">
			    						<div class="small-3 columns">
			    						<a href="https://twitter.com/islonline" target="_blank"><i class="fa fa-twitter"></i></a>
			    					</div>
			    					<div class="small-3 columns">
			    						<a href="https://www.facebook.com/islonline" target="_blank"><i class="fa fa-facebook"></i></a>
			    						</div>
			    					<div class="small-3 columns">
			    						<a href="https://www.youtube.com/user/ISLonline" target="_blank"><i class="fa fa-youtube"></i></a>
			    						</div>
			    					<div class="small-3 columns">
			    						<a href="http://www.instagram.com/islonline/" target="_blank"><i class="fa fa-instagram"></i></a>
			    					</div>
			    					</div>
			    					<h3>Subscribe to the ISL Newsletter:</h3>
			    					<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
			    					<div class="translate">
			    						<small class="float-left">Translate this site:&nbsp;&nbsp;</small><?php echo do_shortcode('[google-translator]'); ?>
			    					</div>
			    					</div>
								</div>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
		</div><!-- Main Wrapper -->
		</div>
	</body>
</html> <!-- end page -->