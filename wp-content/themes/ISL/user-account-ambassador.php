<?php
/*
Template Name: User Ambassador
*/
?>

<?php

require_once(get_template_directory().'/my-isl-subpage-head.php'); 

wp_head();

//
//Setup classes
require_once CC_CLASSES_DIR . "user.class.php";
require_once CC_CLASSES_DIR . "tabs.class.php";
require_once CC_CLASSES_DIR . "acf.link.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";


$user = new USER();
$page = new TAB_MANAGE();
$rsvp = new RSVP();
$adv_custom_flds = new ACF_LINK();

$usr = wp_get_current_user();
$user_info = $usr;
$user_role = $user->user_group($usr->ID);

if(!empty($_POST['action']) && $_POST['action']=="update")
{
	foreach($_POST as $index=>$value)
	{
		if($index == 'cap_code')
			update_user_meta($_GET['user_id'], '_cmd_comm_ambassador_cap_code', $value);
	}
}
		
?>


<H1>Documents</H1>

<?php

$args = array(
  'post_type'   => 'cp_rep_docs',
  'post_status' => 'publish',
 );
 
$docs = new WP_Query( $args );
if( $docs->have_posts() )
{
	echo "<ul>";

	while( $docs->have_posts() )
	{
    	$docs->the_post();
        $doc_type = get_post_meta($post->ID, 'resource_type', true);

		
		echo "<LI>";
		switch($doc_type)
		{
			case 'HTML Page':
				echo "<a href=''>" . get_the_title() . "</a>";
				break;
			case 'URL':
				$url = get_post_meta($post->ID, 'url', true);
				echo "<a href='" . $url . "' target='_blank'>" . get_the_title() . "</a>";
				break;
			case 'PDF Document':
				$pid = get_post_meta($post->ID, 'pdf_document', true);
				echo "<a href='" . get_the_guid($pid) . "' target='_blank'>" . get_the_title() . "</a>";
				break;
			default:
				echo "<a href='" . get_the_guid($doc_id) . "'  " . $classname . " id='doc_check_" . $doc_id . "' target='_blank'>" . $doc_title . "</a>";
				break;
		}

		echo "</li>";
	}

	echo "</ul>";
}
else
{
  esc_html_e( 'No documents!', 'text-domain' );
}

$page = new TAB_MANAGE();
?>

<div id="user-account-profile" class="iframe-inner">
    <form name="u_info" method = "POST" action="<?php $page->GET_path(array('save'))?>">
        <table>

            <?php 
			//
			//Cap Code count
			global $wpdb;

			$query = "SELECT count(*) FROM " . $wpdb->prefix . "command_order_items WHERE item_type = 61 AND item_id = " . $user_info->ID . " and quantity > 0 and price < 0";
		  
		  	$cnt = $wpdb->get_var($query);

			?>
		
			<tr><td><h2>CAP Code</h2></td><td> <?php echo ((isset($cnt) && $cnt > 0) ? '(' . $cnt . ' uses)' : '')?></td></tr>
        	<?php
				$cap_code = get_user_meta($user_info->ID, '_cmd_comm_ambassador_cap_code', true);

				echo "<TR>";
				echo "<th><label for='cap_code'>CAP Code</label></th>";
				echo "<td><input type='text' name='cap_code' id='cap_code' value='" . $cap_code . "' class='regular-text ltr' /></td>";
				echo "</TR>";				
        	?>
        </table>

        <input type="hidden" name="action" value="update" />
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $_GET['user_id']?>" />

		<p>
       		You may change your Campus Ambassador Program (CAP) Code whenever you like. However, if you change your code, your old code will no longer be functional and anyone trying to use it will not get credit, nor will the system credit you. You will not lose any prior Ambassador credits
		</p>
        <p>
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save"  />
        </p>
    </form>
</div>


<?php wp_footer(); ?>
