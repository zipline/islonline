<?php
/*
Template Name: Read Info Docs
*/
?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta class="foundation-mq">
<?php wp_head(); ?>
<style type="text/css" media="screen">
.print-logo {display: none;}
</style>
<style type="text/css" media="print">
h1 {font-size: 24px;} h2, h3, h4, h5 {font-size:20px;} h6{font-size:16px;} p, ul, li, ol {font-size: 13px; line-height: 1.3;}
h1, h2, h3, h4, h5, h6, p, li, a {-webkit-transform: translateZ(0);
transform: translateZ(0);}
.print-logo {display: block; margin: 0 auto 0; width: 115px;}
#content {padding: 40px 0 0;}
</style>
<?php
//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "docs.class.php");
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "rsvp.class.php");

$docs = new DOCS();
$team = new TEAM();
$rsvp = new RSVP();

$doc_type = get_post_meta($_GET['doc_id'], 'resource_type', true);
$doc_info = get_post_meta($_GET['doc_id'], 'html_post', true);
$signature = get_post_meta($_GET['doc_id'], 'signature_required', true);

$docs->post_id($_GET['post_id']);
$docs->fetch_read();
$all_docs = $docs->list_read();

wp_enqueue_script('cmd_document_signature', plugins_url() . '/command/js/doc.sig.js', array('jquery'));	
wp_localize_script('cmd_document_signature', 'svars', array('ajax' => admin_url('admin-ajax.php')));

?>
	<div class="print-logo"><img src='<?php echo get_template_directory_uri(); ?>/assets/images/circle-logo.png' /></div>
   	<header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>        

    <div id="content" class="document-container">
    	<div id="inner-content" class="row">
    
            <main id="main" class="medium-12 small-centered columns" role="main">

			<?php

	            echo "<div class='document'>";
				//echo $doc_info;
				the_field( 'html_post', $_GET['doc_id']);
				echo "</div>";			

				if(!empty($signature) and $signature==true)
				{
					echo "<input type='text' id='signature' name='signature' required
							placeholder='Sign here' size=60 maxlength=60
							" . ((!empty($all_docs[$_GET['doc_id']])) ? 'disabled = true' : '') . "
							" . ((!empty($all_docs[$_GET['doc_id']])) ? 'value="' . $all_docs[$_GET['doc_id']] . '"' : '') . "
							></input>";
					echo "<input type='hidden' name='doc_id' id='doc_id' value='" . $_GET['doc_id'] . "'>";
					echo "<input type='hidden' name='post_id' id='post_id' value='" . $_GET['post_id'] . "'>";
					echo "<input type='button' class='button' id='log_read' value='Submit Signature'>";		
				}
	        ?>
	    	</main>
	    </div>


    </div> <!-- end #content --> 
    
<?php wp_footer() ?>