<?php 
$homepage = 99;
if( get_field('activate_alert', $homepage) ): ?>
	<div id="alert" class="callout" data-closable>
		<i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;
 		<?php the_field('alert_text', $homepage); ?>
		<?php if( get_field('clear_alert', $homepage) ): ?> -->
		  <button class="close-button" aria-label="Close alert" type="button" data-close>
		    <span aria-hidden="true">&times;</span>
		  </button>
		<?php endif; ?>
	</div>
<?php endif; ?>
<div id="masthead">
	<div class="top-bar" id="top-bar-menu">
		<div class="top-bar-left float-left hide-for-large">
			<ul class="menu">
				<li><a data-toggle="off-canvas"><i class="fa fa-bars"></i></a></li>
			</ul>
		</div>

		<div class="nav-left float-left">
			<?php left_nav(); ?>
		</div>

		<div id="logo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" /></a></div>

		<div class="nav-right float-right show-for-medium">
			<ul class="menu">
				<li id="info-panel-trigger" class="menu-item">
					<a><span><i class="fa fa-chevron-down"></i> Info</span></a>
				</li>
			</ul>
			<?php right_nav(); ?>
		</div>
	</div>

	<div id="nav-panel" class="nav-panel"><?php info_nav(); ?></div>	
</div>