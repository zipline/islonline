

<?php

if(isset($_REQUEST['itemid']))
{
	$team_id = $wpdb->get_var("SELECT item_id FROM wp_command_order_items WHERE id=" . $_REQUEST['itemid']);
	include_once(CC_CLASSES_DIR . 'team.class.php');

	$team= new TEAM();
	$team->set_team($team_id);

	$set_adds = get_post_meta($team->value('post'), "_cmd_add_ons");
	$cost = '$' . $set_adds[0][$_REQUEST['addon']];

	$display = true;
}
else
{
	$cost = "Set by Team";
	$display = false;
}

?>
		  
<?php the_field('description'); ?>
<?php if ($cost!='Set by Team'){
    ?>
    <p><strong>Cost: <?php echo $cost; ?></strong></p>
    <?php
}
?>
<p><a class="button" href="/medical-professional-reference/">Medical Professional Reference</a>
    <a class="button" href="/mpr-request/">MPR Request</a></p>
<?php
if($display)
{
	?>
	<a class = "button joiner" type="button" name="addtocart2_0" id="addtocart2_0">Add to Cart</a>
	<?php
}
?>

<form name="team_add" id="team_add">
    <input type="hidden" name="item_id" id="item_id_0" value="<?php _e($_REQUEST['itemid'])?>">
    <input type="hidden" name="item_quan" id="item_quan_0" value="1">
    <input type="hidden" name="add_on" id="add_on_0" value="<?php _e($_REQUEST['addon'])?>">
    <input type="hidden" name="item_type" id="item_type_0" value="add-on">
    <input type="hidden" name="item_cost" id="item_cost_0" value="<?php echo preg_replace('/[^0-9.,]/', '', $cost)?>">
</form>