<?php 
// Adjust the amount of rows in the grid
$grid_columns = 2; ?>

<?php if( 0 === ( $wp_query->current_post  )  % $grid_columns ): ?>

    <div class="row archive-grid" data-equalizer> <!--Begin Row:--> 

<?php endif; ?> 

		<!--Item: -->
		<div class="large-6 medium-6 columns panel" data-equalizer-watch>
			<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) ); ?>
				
            		<div class="featured-image" style="background-image: url('<?php echo $image; ?>'); background-size: cover; background-position: center center;"></div>
                    <h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <span><?php echo get_the_date(); ?></span>
                    <?php the_excerpt(); ?>
			</article> <!-- end article -->
		</div>

<?php if( 0 === ( $wp_query->current_post + 1 )  % $grid_columns ||  ( $wp_query->current_post + 1 ) ===  $wp_query->post_count ): ?>

   </div> <!--End Row: --> 

<?php endif; ?>