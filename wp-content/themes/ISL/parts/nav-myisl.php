<a title="order" id="order_link" onclick='javascript:canvas("order")' style="background-color: #fafafa;">Dashboard<i class="fa fa-tachometer" aria-hidden="true"></i><span class="notification">2</span></a>
                        
<a title="trip" id="trip_link" onclick='javascript:canvas("trip")'>Volunteer Profile
<?php
	if($rsvp->profile_complete($post_id))
	{
		?><span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span><?php
	}
	else
	{
		?><span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span><?php
	}
?></a>
	
<a title="docs" id="list-info-docs_link" onclick='javascript:canvas("list-info-docs")'>Trip Documents
<?php
	if($docs->documents_complete())
	{
		?><span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span><?php
	}
	else
	{
		?><span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span><?php
	}
?></a>

<a title="passport" id="passport_link" onclick='javascript:canvas("passport")'>Passport Info
<?php
	if($rsvp->has_passport($rsvp_id))
	{
		?><span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span><?php
	}
	else
	{
		?><span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span><?php
	}
?></a>

<a title="flight" id="flight_link" onclick='javascript:canvas("flight")'>Flight Info
<?php
	if($rsvp->has_travel($rsvp_id))
	{
		?><span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span><?php
	}
	else
	{
		?><span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span><?php
	}
?></a>

<a title="chat" id="chat_link" onclick='javascript:canvas("chat")'>Program Chat
<?php
	$chatter->set_user($current_user->ID);
	$chatter->team($team_id);
	$chatter->privacy(false);
?>
<i class="fa fa-comment" aria-hidden="true"></i><span class="notification"><?php _e($chatter->unread())?></span>
</a>

<a title="fund" id="fundraising_link" onclick='javascript:canvas("fundraising")'>Fundraising Page<i class="fa fa-money" aria-hidden="true"></i><span class="notification">1</span></a>
<a>Team Add-ons<i class="fa fa-medkit" aria-hidden="true"></i></a>

<a title="chat" id="private-chat_link" onclick='javascript:canvas("private-chat")'>Private Chat
<?php
	$chatter->set_user($current_user->ID);
	$chatter->team($team_id);
	$chatter->privacy(true);
?>
<i class="fa fa-comment" aria-hidden="true"></i><span class="notification"><?php _e($chatter->unread())?></span></a>

<BR>
<a title="account" id="user-profile_link" onclick='javascript:canvas("user-profile")'>Account Info<i class="fa fa-user" aria-hidden="true"></i></a>
<a>Order History<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
<a title="settings" id="user-contact_link" onclick='javascript:canvas("user-contact")'>Settings<i class="fa fa-cog" aria-hidden="true"></i></a>