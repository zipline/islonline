<?php
//
//Gather Team, Country, Program Details
//
$country = new COUNTRY();
$program = new PROGRAM();
$team = new TEAM();
$rsvp = new RSVP();

$sel_prog = isset($_POST['program']) ? $_POST['program'] : 0;
$sel_cntry = isset($_POST['country']) ? $_POST['country'] : 0;

$coming = $team->upcoming($sel_prog, $sel_cntry);
?>
<div class="row small-up-1 medium-up-2 large-up-4" data-equalizer data-equalize-on="medium">
	<?php
	$coming = $team->upcoming($prg_filter, $cty_filter);

	if(count($coming)>0)
	{
		foreach($coming as $data)
		{
			$programs = array();
			$countries = array();
			foreach(explode(",",$data['program_id']) as $program_id)
			{
				$program->set_program($program_id);
				$programs[] = $program->get_name();
			}
			foreach(explode(",",$data['country_id']) as $country_id)
			{
				$country->set_country($country_id);
				$countries[] = $country->get_name();
			}
			
			$stime = strtotime($data['start_date']);
			$etime = strtotime($data['end_date']);
			$format_stime = date("M d, Y", $stime);
			$format_etime = date("M d, Y", $etime);
			$signed_up_count = $rsvp->count_rsvp($data['id']);
			$open = $data['minimum_volunteers'] - $signed_up_count;

			if(has_post_thumbnail($team))
				{
					$source = wp_get_attachment_image_src(get_post_thumbnail_id($team), 'single-post-thumbnail');	
					$image = $source[0];
				}
				else
				{
					$pid = $program->post_id();
					
					if(has_post_thumbnail($pid))
					{
						$source = wp_get_attachment_image_src(get_post_thumbnail_id($pid), 'single-post-thumbnail');
						$image = $source[0];
					}
					else
					{
						$cid = $country->post_id();
						$source = wp_get_attachment_image_src(get_post_thumbnail_id($cid), 'single-post-thumbnail');
						$image = $source[0];
					}
				}
			?>
			
			
                <div class="column">
                	<div class="team" data-equalizer-watch>
                	<div class="team-banner" style="background-image: url('<?php echo $image; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center center;"></div>
                	<h5><?php _e(implode(", ", $programs))?> <span>in</span> <?php _e(implode(", ", $countries))?></h5>
                    <div class="large-12 medium-12 columns">
                    	<div class="dates">
                            <div class="date"><!-- Start Date -->
                                <span class="month"><?php _e(date("M", $stime))?></span><br />
                                <span class="day"><?php _e(date("d", $stime))?></span><br />
                                <span class="year"><?php _e(date("Y", $stime))?></span><br />
                            </div>
                            <div><b>to</b></div>
                            <div class="date"><!-- End Date -->
                                <span class="month"><?php _e(date("M", $etime))?></span><br />
                                <span class="day"><?php _e(date("d", $etime))?></span><br />
                                <span class="year"><?php _e(date("Y", $etime))?></span><br />
                            </div>
            			</div>
                    </div>
                    <div class="large-12 medium-12 columns">
                        <div class="description"><?php _e($data['teaser'])?></div>
	                    <div class="count">
		                    <?php
			                    echo "{$signed_up_count} Volunteer(s) on this program.";
		                    ?>
	                    </div>
                    </div>
                    <div class="large-12 medium-12 columns">
                    	<h5 class="cost"><?php _e('$' . $data['cost'])?></h5>
                        <a href="<?php _e($data['url'])?>" class="button">Program Details</a>
                    </div>
                	</div>
                </div>
            

			<?php
		}
	}
	?>
</div>