<?php 
    $src = get_field('image')['sizes']['large'];     
    $vposition = get_field('image_position'); 
    $hposition = get_field('image_position_hz');
    $color = get_field('title_color');
    $overlay = get_field('banner_image_overlay');
?>

<div class="banner" style="background-image: url(<?php echo $src ?>); background-position: <?php echo $vposition; ?> <?php echo $hposition; ?>;">
	
    	<header class="entry-header">
			<h1 class="entry-title" style="color: <?php echo $color; ?>;"><?php the_title(); ?></h1>
		</header>  
    <div class="overlay" style="background-color: <?php echo $overlay; ?>;"></div>      
</div>