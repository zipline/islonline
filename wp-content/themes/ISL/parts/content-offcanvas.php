<div class="off-canvas position-left" id="off-canvas" data-off-canvas data-position="left">
	<?php joints_off_canvas_nav(); ?>
	<ul class="menu">
	<li class="menu-item">
	<?php
		if ( is_user_logged_in() ) { ?>
		    <a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
		<?php } else { ?>
			<a href="/login"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>
	<?php } ?>	
	</li>
	</ul>
</div>