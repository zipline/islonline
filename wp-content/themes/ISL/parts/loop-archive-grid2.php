<?php 
// Adjust the amount of rows in the grid
$grid_columns = 3;
?>

<?php if( 0 === ( $wp_query->current_post  ) % $grid_columns ): ?>

<div class="row archive-grid" data-equalizer data-equalize-by-row="true"> <!--Begin Row:--> 

<?php endif; ?> 

	<!--Item: -->
	<div class="article columns" data-equalizer-watch>
	
		<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
			<?php
				if (has_post_thumbnail()) {
				    $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' );
				    $thumbnail_url = $thumbnail_data[0];
				} else {
					$thumbnail_url = get_template_directory_uri() . '/assets/images/placeholder.jpg';
				}
			?>
			<a href="<?php echo get_the_permalink(); ?>">
				<section class="featured-image" itemprop="articleBody" style="background-image: url('<?php echo $thumbnail_url; ?>'); background-size: cover; background-position: center center;"></section>
			</a>
			
			<div class="excerpt">
				<header class="article-header">
					<h3 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>	
					<?php get_template_part( 'parts/content', 'byline' ); ?>				
				</header> <!-- end article header -->	
								
				<section class="entry-content" itemprop="articleBody">
					<?php the_excerpt('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); ?> 
				</section> <!-- end article section -->
			</div>
							    							
		</article> <!-- end article -->
	</div>

<?php if( 0 === ( $wp_query->current_post + 1 )  % $grid_columns ||  ( $wp_query->current_post + 1 ) ===  $wp_query->post_count ): ?>

</div>  <!--End Row: --> 

<?php endif; ?>