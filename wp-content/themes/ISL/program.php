<?php
/*
Template Name: Program
*/
?>

<?php
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "rsvp.class.php";

$team = new TEAM();
$country = new COUNTRY();
$program = new PROGRAM();
$rsvp = new RSVP();

$program->set_program($program->by_post(get_the_ID()));
$coming = $team->upcoming($program->get_program(), 0);
// printX($program->get_program());
?>

<?php get_header(); ?>

<?php get_template_part( 'banner' ); ?>

    <div id="content">
    <a class="button tiny mobile-anchor" href="#team-sidebar">View Programs</a>
        <div class="row collapse">
    
            <!-- <main id="main" class="large-8 <?= ($coming ? 'medium-8' : 'large-push-2'); ?> columns" role="main"> -->
            <main id="main" class="large-8 medium-8 columns program" role="main">
                <?php the_flex_content(); ?>
            </main> <!-- end #main -->

			<div id="team-sidebar" class="large-4 medium-4 columns">

				<div class="start-your-journey text-center">
					<a href="<?php echo get_permalink(280854); ?>" class="button align-center">Start Your Journey with ISL</a>
				</div>

				<h3>Upcoming Programs</h3>
				
				<?php
				if(count($coming))
					{ ?>
				<div class="team-feed">
					<?php
					$i = 0;
					foreach($coming as $data) :
						if($i==24) break;
						$countries = array();
						foreach(explode(",",$data['country_id']) as $country_id) {
							$country->set_country($country_id);
							
							$reg_label = '';
							foreach(explode(',', $data['region_id']) as $region_ids)
							{
								$country->set_region($region_ids);
								$this_region = $country->region_details();
								
								if($this_region['country_id'] == $country_id && $this_region['title'] != 'ALL')
								{
									$reg_label .= $this_region['title'] . ' & ';
								}
								
							}	
							
							if(strlen($reg_label) > 2 && substr($reg_label,-3) == ' & ')
							{
								$reg_label = ': ' . substr($reg_label, 0, -3);	
							}
							
							$countries[$country_id] = $country->get_name() . $reg_label;
						}

						$programs = array();
						foreach(explode(",",$data['program_id']) as $program_id)
						{
							$program->set_program($program_id);
							$programs[$program_id] = $program->get_name();
						}

						$stime = strtotime($data['start_date']);
						$etime = strtotime($data['end_date']);
						$signed_up_count = $rsvp->count_rsvp($data['id']);
						$open = $data['minimum_volunteers'] - $signed_up_count;
						$avail = max(0, $data['maximum_volunteers'] - $rsvp->count_rsvp($data['id']));
						$sold_out = !(bool)$avail;
						?>
						<div class="team">
							<h5><?php _e($program->display_program_list($programs))?> in <?php _e($country->display_country_list($countries))?>
								<?php if($open >0 || $open <= 0 && !$sold_out ) { ?>
                                	<span><?php _e('$' . $data['cost'])?></span>
                            	<?php } if($sold_out) { ?>
                                	<span class="sold-out"><del><em><?php _e('$' . $data['cost'])?></del></em></span>
                           		<?php } ?> 
							</h5>
							<div class="description"><?php _e($data['teaser'])?></div>
							<div class="dates">
								<div class="date"><!-- Start Date -->
									<span class="month"><?php _e(date("M", $stime))?></span><br />
									<span class="day"><?php _e(date("d", $stime))?></span><br />
									<span class="year"><?php _e(date("Y", $stime))?></span><br />
								</div>
								<div><b>to</b></div>
								<div class="date"><!-- End Date -->
									<span class="month"><?php _e(date("M", $etime))?></span><br />
									<span class="day"><?php _e(date("d", $etime))?></span><br />
									<span class="year"><?php _e(date("Y", $etime))?></span><br />
								</div>
							</div>
							<div class="details">
								<?php if($open >0 || $open <= 0 && !$sold_out ) { ?>
                                <a href="<?php _e($data['url'])?>" class="button tiny">Program Details</a>
                                <?php } if($sold_out) { ?>
                                <span href="<?php _e($data['url'])?>" class="button secondary disabled">Program Details</span>
                                <h5 class="sold-out"><em>SOLD OUT!</em></h5>
                                <?php } ?>
								<div class="count">
									<?php
										echo "{$signed_up_count} Volunteer(s) on this program.";
									?>
								</div>
							</div>  
							<div class="share">Share this Program: <a class="twitter" href="http://twitter.com/home?status=Join this @islonline program and make a difference! <?php _e($data['url'])?>" title="Share this post on Twitter!" target="_blank"><i class="fa fa-twitter"></i></a><a href="http://www.facebook.com/sharer.php?u=<?php _e($data['url'])?>" title="Share this post on Facebook!" target="_blank"><i class="fa fa-facebook"></i></a></div>
						</div>
						<?php $i++ ?>
					<?php
					endforeach;
					?>
					<a class="button secondary" href="<?php echo get_permalink(4411); ?>">Search More Programs</a>
					</div> <!-- End Team feed -->
					<?php } else
					{ ?>
						<div class="no-teams">
							<p>No programs right now, but check back soon. New programs are added all the time! Maybe check out some of our other available programs.</p>
							<a href='<?php echo get_permalink(4411); ?>' class='button align-center'>Find a Program</a>
							<p>Have a location and program in mind, but don't see what you're looking for?</p>
							<a href='<?php echo get_permalink(4017) ?>' class='button align-center'>Create a Program</a>
						</div>
					<?php } ?>
			</div> <!-- End sidebar -->

        </div> <!-- end #inner-content -->

	    <div class="row">
		    <div class="start-your-journey text-center hidden-4">
			    <a href="<?php echo get_permalink(280854); ?>" class="button align-center">Start Your Journey with ISL</a>
		    </div>
	    </div>
    <?php edit_post_link('edit'); ?>
    </div> <!-- end #content --> 

<?php get_footer(); ?>