<?php
/*
Template Name: Fundraiser
*/
?>

<?php
//
//Kick the user to the login page if they aren't logged in
//
if(!is_user_logged_in() == 1)
{
echo "Saying something here";
	header ('Location: http://new.islonline.org/login/');	
}
?>

<?php
//
//Get user data
$current_user = wp_get_current_user();
wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

//
//Setup classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "tabs.class.php");

$rsvp = new RSVP();
$page = new TAB_MANAGE();

$rsvp->set_rsvp_id($_REQUEST['rsvp']);
$rsvp->fetch_rsvp();


if(isset($_REQUEST['saving']) && $_REQUEST['saving'] == "Save Now")
{
	$rsvp->fund_language($_REQUEST['title'], $_REQUEST['amount'], $_REQUEST['blurb']);	
}

$fields = $rsvp->fund_language();

?>
    <div id="myisl-content">
        <form name="fundraiser" id="fundraiser" method="POST" enctype = "multipart/form-data" action="<?php echo $page->GET_path(array('save'));?>">
        <table>
            <TR>
                <TD>Title</td>
                <td><input type="text" name="title" id="title" size="60" value="<?php _e(!empty($fields['title']) ? $fields['title'] : '');?>"></TD>
            </TR>
            <TR>
                <TD>Amount</td>
                <td><input type="number" name="amount" id="amount" size="60" value="<?php _e(!empty($fields['amount']) ? $fields['amount'] : '');?>"></TD>
            </TR>
            <TR>
                <TD>Blurb</TD>
                <td><textarea id="blurb" name="blurb" cols="60" rows="20"><?php _e(!empty($fields['blurb']) ? $fields['blurb'] : '');?></textarea></TD>
            </TR>
                
        </table>
		<br>
        <input type="hidden" name="rsvp" value="<?php _e($_REQUEST['rsvp']) ?>">
        <button type="submit" id="saving" name="saving" class="button">Save</button>
        </form>
    </div>

<?php wp_footer() ?> 