<?php get_header(); ?>


<?php 
    $src = get_field('image', 4373)['sizes']['xlarge'];     
    $vposition = get_field('image_position', 4373); 
    $hposition = get_field('image_position_hz', 4373);
    $color = get_field('title_color', 4373);
    $overlay = get_field('banner_image_overlay', 4373);
?>

<div class="short-banner" style="background-image: url(<?php echo $src ?>); background-position: <?php echo $vposition; ?> <?php echo $hposition; ?>;">
	<header class="entry-header">
		<h1 class="entry-title" style="color: <?php echo $color; ?>;">The ISL Blog</h1>
	</header>  
	
    <div class="overlay" style="background-color: <?php echo $overlay; ?>;"></div>      
</div>
			
<div class="cat-menu">
	<!-- <span>Categories:</span> --><?php wp_list_categories('title_li=&exclude=1'); ?>
</div>
<div id="content">
	<div id="blog">
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">
		    
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive-grid2' ); ?>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
																								
		    </main> <!-- end #main -->

		</div> <!-- end #inner-content -->
	</div>
</div> <!-- end #content -->

<?php get_footer(); ?>