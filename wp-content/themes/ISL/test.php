<?php
/*
Template Name: Testing Page
*/
?>

<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

//
//Requirements
require_once (CC_CLASSES_DIR . 'Twilio/autoload.php');

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

//
// Your Account SID and Auth Token from twilio.com/console
$options = get_option('cmd_center', array());
$sid = !empty($options['twilio_sid_text']) ? $options['twilio_sid_text'] : '';
$token = !empty($options['twilio_token_text']) ? $options['twilio_token_text'] : '';
$send_from = !empty($options['twilio_phone']) ? $options['twilio_phone'] : '';
$send_to = '5092806603';
$message = 'This is a sample message';

/*
echo var_dump($options);
echo $sid . "<br>";
echo $token . "<br>";
echo $send_from . "<br>";
echo $send_to . "<br>";
echo $message . "<br>";
*/

if(!empty($sid) && !empty($token) && !empty($send_from) && !empty($send_to) && !empty($message))
{
	$client = new Client($sid, $token);
	
	// Use the client to do fun stuff like send text messages!
	$client->messages->create(
		// the number you'd like to send the message to
		$send_to,
		array(
			// A Twilio phone number you purchased at twilio.com/console
			'from' => $send_from,
			// the body of the text message you'd like to send
			'body' => $message
		)
	);
}