<?php
/*
Template Name: Home
*/

$home_bg_img = get_field('main_hero')['sizes']['xlarge'];

function insert_og_image() {
    global $home_bg_img;
    echo '<meta property="og:image" content="'.$home_bg_img.'" />';
}
add_action('wp_head','insert_og_image');
function override_og_title(){
    return 'International Service Learning';
}
add_filter('wpseo_title', 'override_og_title');

get_header();

$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "country.class.php";


?>

<div id="home-hero"> 
	<?php if (get_field('media_picker')=="video") { ?>
	<div class="bg-video">
		<video autoplay muted id="bgvid" loop poster="<?php the_field('main_hero') ?>">
            <source src="<?php echo get_field('main_hero_video'); ?>" type="video/mp4">
            <source src="<?php echo get_field('video_webm'); ?>" type="video/webm">
        </video>
	</div>
	<?php }
	if(get_field('media_picker') == "image") { ?>
	<div class="bg-image" data-interchange="[<?php echo get_template_directory_uri(); ?>/assets/images/bg-mobile.jpg, small],[<?php echo $home_bg_img ?>, medium]"></div>
	<?php } ?>
	
	<div class="home-slick-slider">
		<?php if( have_rows('hero_slider') ): ?>

			<?php while( have_rows('hero_slider') ): the_row();
				// vars
				$buttonText = get_sub_field('button_text');
    			$targetUrl = (get_sub_field('button_target') ? get_sub_field('button_target')->guid : '');
    			$quote = get_sub_field('testimonial');
				$imagePosition = get_sub_field('image_position');
				if ($imagePosition == 'left') {
					$textPosition = 'right';
				} elseif ($imagePosition == 'right') {
					$textPosition = 'left';
				} else {
					$textPosition = 'full';
				}
				?>

				<div class="slide"> 
					<div class="arrow-up"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-up.svg" /></div>
					<div class="slide-panel">
						<div class="row collapse">
							<div class="large-12 medium-12 columns">
								<?php if ($imagePosition == "right" || $imagePosition == "left") : ?>
								<div class="image">
									<img class="<?php echo $imagePosition; ?>" src="<?php echo get_sub_field('image')['sizes']['large']; ?>" />
								</div>
								<?php endif; ?>

								<div class="text <?php echo $textPosition; ?>">

									<h2><?php echo get_sub_field('title'); ?></h2>
									
									<?php echo get_sub_field('description'); ?>
									
									
									<div class="slide-text-bottom">
									<?php if(get_sub_field('testimonial') ) : ?>
										
										<div class="button-container">
											<a href="<?php echo $targetUrl; ?>" class="button"><?php echo $buttonText; ?></a>
										</div>
										
										<div class="quote-container">
											<div class="quote">
												<?php the_field('quote', $quote->ID); ?>
												<span>&mdash;<?php the_field('source', $quote->ID); ?></span>
											</div>
										</div>
									<?php else : ?>
										<div class="button-container full">
											<a href="<?php echo $targetUrl; ?>" class="button"><?php echo $buttonText; ?></a>
										</div>
									<?php endif; ?>
									</div> <!--row-->
								</div>
							</div>
						</div>
					</div>
				</div>

			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	</div>
</div><!--end hero banner -->

<div id="about" class="row teal">
	<div class="text-box">
		<?php the_field('about_isl'); ?>
	</div>
	<?php if(get_field('add_button') ) : ?>
		<a class="button" href="<?php echo the_field('button_link'); ?>"><?php the_field('button_text'); ?></a>
	<?php endif; ?>
</div>

<div class="featured-quote">
	<h2>“<?php the_field('big_quote'); ?>”<span>—<?php the_field('big_quote_source'); ?></span></h2>
</div>

<div id="home-hero-2" style="background-image: url(<?php echo get_field('secondary_hero')['sizes']['xlarge']; ?>);"></div>

<div id="featured-trips">
	<div class="title"><h2>Picture yourself on one of these learning adventures…</h2></div>
	<div class="row collapse"> <!--Featured Trips Wrapper -->
		<!--Team image is inserted as background image for trip div (could just be inserted as variable and echoed below) -->
		<!--Individual Team Wrapper -->
		<?php
			$program = new PROGRAM();
			$country = new COUNTRY();
			$team = new TEAM();
			
			$featured = explode(",",$team->featured_teams());

			foreach($featured as $team_id)
			{
				$team->set_team($team_id);
				$post_id = $team->value('post');
				if(!empty($post_id))
				{
					$trip_date = date("M Y", strtotime($team->arrival_date()));
					$program->set_program(@array_shift($team->schedule_programs()));
					$prgm = $program->get_name();
					$country->set_country(@array_shift($team->schedule_countries()));
					$ctry = $country->get_name();
					
					if(has_post_thumbnail($post_id))
					{
						//$source = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');
						$source = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'gallery-thumb');	
						$image = $source[0];
					}
					else
					{
						$pid = $program->post_id();
						$source = get_field("image", $pid)['sizes']['gallery-thumb'];

						if($source)
						{
							$image = $source;
						}
						else 
						{
							$cid = $country->post_id();
							$source = get_field("image", $cid)['sizes']['gallery-thumb'];
							$image = $source;
						}
					}
				}
				else
				{
					$ctry = 'We should never see this.';
					$prgm = 'As long as there are at least eight active trips.';
					$trip_date = '';
					$image = '';
					$post_id = 0;
				}
		?>

				
        		<div class="trip large-3 medium-6 columns" style="background-image: url(<?php echo $image?>);">
					<a href="<?php _e(the_permalink($post_id))?>">
						<div class="team">
							<!--Country--><h3><?php _e($ctry)?></h3>
							<!--Program--><h4><?php _e($prgm)?> <br />
	                        <!--Month/Year--><span><?php _e($trip_date)?></span></h4>
                            <?php //echo var_dump($image)?>
						</div>
					</a>
				</div><!-- End Team Wrapper -->	
		<?php
			}
		?>
	</div> <!-- End Wrapper-->

</div>

<div id="internship-block" class="feature-block" style="background-image: url(<?php echo get_field('internship_background_image'); ?>);">
	<div class="title">
		<h2 class="white-text"><?php echo get_field('internship_headline'); ?></h2>
		<h3 class="white-text"><?php echo get_field('internship_blurb'); ?></h3>
		<a class="button" href="<?php echo (get_field('internship_link') ? get_field('internship_link')->guid : ''); ?>">Learn more</a>
	</div>
</div>

<!-- <div id="key-message"> 
	<div class="text-box row" data-equalizer data-equalize-on="large">
		<div class="large-9 medium-12 columns" data-equalizer-watch>
			<?php the_field('featured_text'); ?>
		</div>
		<div class="large-3 medium-12 columns button-container" data-equalizer-watch>
			<a href="<?php the_field('featured_text_button_link'); ?>" class="large button"><?php the_field('featured_text_button'); ?></a>
		</div>
	</div>
</div>
 -->

<div id="ratings" class="dkblue">
	<div class="ratings-slider">
	<?php if( have_rows('ratings_slider') ): ?>

		<?php while( have_rows('ratings_slider') ): the_row(); ?>
			<div class="slide"> 
					<a href="<?php echo get_sub_field('link'); ?>" target="_blank">
					<div class="rating"><?php echo get_sub_field('rating'); ?></div>
					<img src="<?php echo get_sub_field('logo')['sizes']['medium']; ?>" />
					</a>
			</div>
		<?php endwhile; ?>

	<?php endif; ?>
	</div>
</div>

<div id="subscribe" class="teal">
	<div class="form">
		<h2><?php the_field('subscribe_header'); ?></h2>
		<h3><?php the_field('subscribe_text'); ?></h3>
		<?php echo do_shortcode('[gravityform id="1" title="false" description="false" tabindex=6]'); ?>
	</div>
</div>

<div id="blog-feed">
	<div class="row">
	<div class="medium-11 medium-centered columns">	
	<?php
    ///////////////////////////////
    // BEGIN - recent blogs loop
    global $post;
    $recentBlogPosts = get_posts('posts_per_page=2');
    if ( have_posts() ) : 
        foreach ($recentBlogPosts as $index=>$post) : 
            setup_postdata( $post );
    ?>
	        <div class="large-6 columns">
					<?php
					if (has_post_thumbnail()) {
					    $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium_large' );
					    $thumbnail_url = $thumbnail_data[0];
					} else {
						$thumbnail_url = get_template_directory_uri() . '/assets/images/placeholder.jpg';
					}
					?>
	        		<a href="<?php echo the_permalink(); ?>"><div class="featured-image <?php echo ($thumbnail_url ? 'present' : ''); ?>" style="background-image: url('<?php echo $thumbnail_url; ?>');"></div></a>
	                <h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>
	                <span><?php echo get_the_date(); ?></span>
	                <?php the_excerpt(); ?>
	        </div>
    <?php
        wp_reset_postdata();
        endforeach;
    endif;
    wp_reset_query();

    // END - recent blogs loop
    /////////////////////////////
	?>
	</div>
	</div>
	<a href="/blog" class="button">Explore more on the blog</a>
</div>

<div id="rep-signup" style="background-image: url(<?php echo get_field('rep_program_background')['sizes']['xlarge']; ?>);">
	<div class="overlay"></div>
	<div class="form">
		<h2 class="white-with-shadow">Be your university's ISL Ambassador!</h2>
		<h4 class="white-with-shadow"><?php the_field('ambassador_program_blurb'); ?></h4>
		<p><strong>Sign up for more info…</strong></p>
		<?php echo do_shortcode('[gravityform id="2" title="false" description="false" tabindex=10]'); ?>

	</div>
</div>

<div class="quote-slider">
		<?php if( have_rows('testimonial_slider') ): ?>

			<?php while( have_rows('testimonial_slider') ): the_row(); 

				// vars
    			$quote = get_sub_field('quote');
				$image = get_sub_field('image')['sizes']['large'];
				?>

					<div class="slide" style="background-image: url(<?php echo $image; ?>);"> 
						<div class="overlay"></div>
						<div class="quote">
							<?php if(get_sub_field('quote') ) : ?>
							<?php the_field('quote', $quote->ID); ?>
							<span>&mdash;<?php the_field('source', $quote->ID); ?></span>
							<?php endif; ?>
						</div>
					</div>

			<?php endwhile; ?>

		<?php endif; ?>
	</div>
</div>

<!-- GR edited 2018-04-27 to add additional functionality to this block -->
<!-- <div class="featured-block" style="background-image: url(<?php echo get_field('feature_block_background')['sizes']['xlarge']; ?>);">
	<a href="http://store.islonline.org/" class="button" target="_blank">Shop the ISL Store!</a>
</div>-->
<!-- <div class="featured-block" style="background-image: url(<?php echo get_field('feature_block_background')['sizes']['xlarge']; ?>);">
	<?php 
		$link = get_field('feature_button_link');
		$label = get_field('feature_button_label');
		$color = get_field('feature_button_color');
	?>
	<a href="<?php echo $link ?>" style="background: <?php echo $color ?>;" class="button" target="_blank"><?php echo $label ?></a>
</div> -->

<?php get_footer(); ?>