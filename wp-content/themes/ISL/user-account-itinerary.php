<?php
/*
Template Name: User Itinerary
*/
?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>
<style type="text/css" media="screen">
.print-logo {display: none;}
</style>
<style type="text/css" media="print">
.itinerary-container .itinerary .header, 
.itinerary-container .itinerary .footer,
.itinerary-container .itinerary .day {
    border: 0px solid !important;
    border-bottom: 1px solid #cccccc !important;
    width: 90%;
    margin: 0 auto;
    display: block;
}

.itinerary-container .itinerary  p {
        line-height: 1.4;
        margin-bottom: .5em;
        font-size: .85em;
    }

.button, hr {
    display: none !important;
}
.print-logo {display: block; margin: 0 auto 0; width: 115px;}

	
.day h6 {
    visibility: hidden;
}

.section {
    background: transparent !important;
    float: left;
}

</style>
<?php
//
//Setup classes
require_once CC_CLASSES_DIR . "itinerary.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";

$itinerary = new ITINERARY();
$rsvp = new RSVP();

//
//2018-03-11 DJT added an Admin override for back-side printing.
//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
//if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
if(!(isset($_GET['admino']) && $_GET['admino'] == 'admin_over_print') && !$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;

$itinerary->production(true);
$itinerary->set_team($_GET['team_id']);
$itineraryDetails = $itinerary->details();

?>
        <div class="row collapse">
            <div class="itinerary-container small-12 columns">
                <div>
                    <div class="print-logo"><img src='<?php echo get_template_directory_uri(); ?>/assets/images/circle-logo.png' /></div>
                    <div class='itinerary'>
                        <div class='header'>
                            <div class="text">
                            <?php echo stripslashes($itinerary->header()); ?> 
                            </div>
                        </div>

                        <?php
                        if (count($itineraryDetails) > 0)
						{
    					   foreach ($itineraryDetails as $id=>$data)
						   {
							?>

							<div class='day'>
								<h6>Day <?php echo $data['day'] . ": " . $data['title']; ?></h6>
								<div class='description'><?php echo stripslashes($data['description']); ?></div>
							</div>
							<?php
						   }
						}
						else
						{
							echo "<div class='day'>";
							echo "Your trip itinerary will appear here. Check back soon.";
							echo "</div>";
						}
						?>

        				<div class='footer'>
                            <div class="text">
                            <?php echo stripslashes($itinerary->footer()); ?>
                            </div>
                        </div>
    				</div>
                </div>
            </div>
            <a class="button" href="javascript:window.print()">Print</a>
        </div>

<?php wp_footer(); ?>
