<?php get_header(); ?>

<div class="short-banner" style="background-image: url('<?php echo get_template_directory_uri() . '/assets/images/jungle-hero-1.jpg'; ?>');">
	<header class="entry-header">
		<h1><?php the_archive_title();?></h1>
			<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
	</header>
</div>
			
	<div id="content">
	<div id="blog">
		<div id="inner-content" class="row">
		
		    <main id="main" class="large-12 medium-12 columns" role="main">
			    

		
		    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive-grid2' ); ?>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
		
			</main> <!-- end #main -->
	
			<?php get_sidebar(); ?>
	    
	    </div> <!-- end #inner-content -->
	</div>
	</div> <!-- end #content -->
<div class="cat-menu">
<span>Categories:</span><?php wp_list_categories('title_li=&exclude=1'); ?>
</div>
<?php get_footer(); ?>