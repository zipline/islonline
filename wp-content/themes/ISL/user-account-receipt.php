<?php
/*
Template Name: User Receipt
*/
?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>
<?php
//
//Setup classes
include_once(CC_CLASSES_DIR . 'finance.class.php');
$finance = new FINANCE();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();

$check = $finance->get_transaction(array($_GET['transact_id']));

if($check['created_by'] == $current_user->ID)
{
	echo $finance->show_receipt($_GET['transact_id'], $check['action'], $check['method']);
}
else
{
	echo "No Transaction.";
}

?>

<?php wp_footer(); ?>
