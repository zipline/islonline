<?php
/*
Template Name: Addons
*/
?>

<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');
include_once(CC_CLASSES_DIR . 'tabs.class.php');
include_once(CC_CLASSES_DIR . 'rsvp.class.php');
include_once(CC_CLASSES_DIR . 'finance.class.php');

$team = new TEAM();
$rsvp = new RSVP();
$finance = new FINANCE();
$this_page = new TAB_MANAGE();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;


$team->set_team($_REQUEST['team_id']);
$set_adds = get_post_meta($team->value('post'), "_cmd_add_ons");

if(isset($set_adds[0]))
{
	foreach($set_adds[0] as $add_post_id => $add_cost)
	{
		$set_adds[0][$add_post_id] = $add_cost;
	}
}

$rsvp->set_rsvp_id($_GET['rsvp']);
$rsvp->fetch_rsvp();
	
$reservation = $rsvp->get_rsvp();
$add_ons = $finance->add_ons($reservation['order_item_id']);

//
//Save the data, or reset the fields if necessary.
if(isset($_GET['save']) && $_GET['save']=='true' && !empty($_REQUEST['add_on_id']) && $_REQUEST['add_on_id'] > 0 && isset($set_adds[0]))
{
	//
	//Now that we have a viable save option, time to add the add-on on.
	foreach($set_adds[0] as $add_on_post_id => $add_on_cost)
	{
		//
		//If the add on is the one we're looking for, then add it.
		if($add_on_post_id == $_REQUEST['add_on_id'])
		{
			$rsvp = new RSVP();

			$rsvp->set_rsvp_id($_REQUEST['rsvp']);
			$res_data = $rsvp->fetch_rsvp();
			$original_item = $finance->fetch_item($res_data['order_item_id']);

			$finance->set_order_id($original_item['order_id']);

			$item_data = array('type' => 4,
							   'item_id' => $_REQUEST['add_on_id'],
							   'note' => '',
							   'add_on' => $res_data['order_item_id'],
							   'quantity' => 1,
							   'price' => $add_on_cost
							   );
			$item_ins_id = $finance->new_item($item_data);
					
			//echo var_dump($_REQUEST);
		}
	}
}

require_once(get_template_directory().'/my-isl-subpage-head.php');

wp_head();

if(!empty($set_adds[0])) {
?>
    <form name="data" id="data" method="POST" enctype="multipart/form-data" action="<?php echo $this_page->GET_path(array('save')) . '&save=true'?>">
        <input type="hidden" name="add_on_id" id="add_on_id" value="0">
    </form>
    
    <div class="wrap">
        <!-- <table class="widefat">
            <TR>
                <TH>Cost</TH>
                <TH>Add-on</TH>
                <TH>&nbsp;</TH>
            </TR>
            
            <?php 
            foreach($set_adds[0] as $add_on_post_id => $add_on_cost)
            {
                $add_on_data = get_post($add_on_post_id, ARRAY_A);

				$url = $add_on_data['guid'] . '&addon=' . $reservation['order_item_id'] . '&itemid=' . $add_on_data['ID'];
                echo "<TR>";
                    echo '<td>$' . (!empty($add_on_cost) ? number_format($add_on_cost,2) : '') . '</td>';
                    echo '<td><a href="' . $url . '" target="_blank">' . (!empty($add_on_data['post_title']) ? $add_on_data['post_title'] : '') . '</a></TD>';
                    
                    $found = false;
                    foreach($add_ons as $add_id => $add_data)
                    {
                        if($add_data['item_id'] == $add_on_post_id)
                        {
                            $found = true;
                        }
                    }
    
                    if($found)
                    {
                        echo '<td>Congratulations!</td>';
                    }
                    else
                    {
                        echo '<td><a class="button-secondary" onclick="javascript:package(' . $add_on_data['ID'] . ')">Add this Package</a></td>';
                    }
                echo "</TR>";
            }
            ?>


        </table> -->
    <div class="row small-up-2 medium-up-3 medium-collapse" data-equalizer data-equalize-on="medium" data-equalize-by-row="true">
        <?php 
            foreach($set_adds[0] as $add_on_post_id => $add_on_cost)
            {
                $add_on_data = get_post($add_on_post_id, ARRAY_A);

				$url = $add_on_data['guid'] . '&itemid=' . $reservation['order_item_id'] . '&addon=' . $add_on_data['ID']; 
                $excerpt = get_field('excerpt', $add_on_data['ID']);
                $description = get_field('description', $add_on_data['ID']);
                $src = get_field('featured_image', $add_on_data['ID'])['sizes']['medium'];
            echo '<div class="column medium-collapse">';
            echo '<div class="addon-container" data-equalizer-watch>';
            echo '<div class="thumb" style="background: url('. $src .');"></div>';
            echo '<div class="text">';
            echo '<h4>' . (!empty($add_on_data['post_title']) ? $add_on_data['post_title'] : '') . '</h4>';
            echo '<b>$' . (!empty($add_on_cost) ? number_format($add_on_cost,2) : '') . '</b>';
            echo '<p>' . $excerpt . '</p>';
            echo '<a class="button small" href="' . $url . '" target="_blank">Learn More</a>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
           }
        ?>
    </div>


    </div>
    
    <script>
        function package(pid)
        {
            var which = document.getElementById('add_on_id');
            which.value = pid;
            
            document.getElementById('data').submit();				
        }
    </script>
<?php
}
else
{
	echo "<p>There are no add-ons for this adventure yet. Check back later, or talk to your VEC about possible opportunities.</p>";
}
?>

<?php wp_footer() ?>   
