<?php
/*
Template Name: Receipt Copy 1016-11-16
*/
?>

<?php
//
//Test Data
// Account Number: 4032039649918960
// Card Type: Visa
// Expiration: 01/2021
// CVV2: Any

//
//Make sure we should be here, otherwise get out of here
if(empty($_POST['MethodAmount']) || ($_POST['MethodAmount'] != 'balance' && $_POST['MethodAmount'] != 'deposit'))
{
 	wp_safe_redirect( wp_get_referer() );
}

/** move this if into the bit above? */
if(empty($_POST['paythrough']))
{
	wp_safe_redirect( wp_get_referer() );
}
else
{
	$page_type = $_POST['paythrough'];
}

//
// Include required library files.
require_once CC_CLASSES_DIR . "paypal/config.php";
require_once CC_CLASSES_DIR . "paypal/autoload.php";
require_once CC_CLASSES_DIR . "paypal/PayPal/PayPal.php";
require_once CC_CLASSES_DIR . "paypal/PayPal/PayFlow.php";
require_once CC_CLASSES_DIR . "cart.class.php";
require_once CC_CLASSES_DIR . "finance.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "user.class.php";
require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "program.class.php";
require_once CC_CLASSES_DIR . "itinerary.class.php";
require_once CC_CLASSES_DIR . "email.class.php";

require_once CB_CLASSES_DIR . "chatter.class.php";

//
// Create PayPal object.
$PayPalConfig = array(
					'Sandbox' => $sandbox,
					'APIUsername' => $payflow_username, 
					'APIPassword' => $payflow_password, 
					'APIVendor' => $payflow_vendor, 
					'APIPartner' => $payflow_partner, 
					'PrintHeaders' => $print_headers, 
					'LogResults' => $log_results,
					'LogPath' => $log_path,
					);

//
//Instantiate PayPal object
$PayPal = new angelleye\PayPal\PayFlow($PayPalConfig);

if($page_type == "checkout")
{
	//
	//Fetch the cart and validate it
	
	$cart = new CART();
	$cart->validate();
	$team = new TEAM();

	//
	//Check Rep_code
	if(!empty($_REQUEST['repcode']) && !empty($_REQUEST['fee_count']))
	{
		global $qpdb;
		
		$query="SELECT user_id FROM " . $wpdb->prefix . "usermeta WHERE meta_key='rep_code' and meta_value='" . $_REQUEST['repcode'] . "'";	
		$rep_id = $wpdb->get_var($query);
		
		$settings = get_option('cmd_center');
		if(!empty($settings['campus_ambassador_fee']))
		{
			$fee_reduce = $settings['campus_ambassador_fee'] * $_REQUEST['fee_count'];
		}
		else
		{
			$fee_reduce = 0;
		}
		
	}
	else
	{
		$rep_id = false;	
		$fee_reduce = 0;
	}

	//
	//Amount to process...
	if($_POST['MethodAmount'] == 'balance')
	{
		$process_amount = $cart->total($cart->type('total'), "float") - $fee_reduce;	
	}
	if($_POST['MethodAmount'] == 'deposit')
	{
		$process_amount = $cart->total($cart->type('deposit')) + $cart->total($cart->type('fee'));	
	}
}
else
{

	$rsvp = new RSVP();
	$finance = new FINANCE();
	
	$rsvp->set_id_by_team_user($_REQUEST['team_id'], $_REQUEST['member_id']);
	$rsvp_data = $rsvp->fetch_rsvp();

	$item = $finance->fetch_item($rsvp_data['order_item_id']);
	$add_ons = $finance->add_ons($rsvp_data['order_item_id']);
	$all_items[] = $rsvp_data['order_item_id'];

	$cost = $item['price'];
	foreach($add_ons as $index => $itm)
	{
		$all_items[] = $itm['id'];
		$cost += $itm['price'];
	}		

	$pays = 0;
	foreach($finance->payments($all_items) as $payments)
	{
		$paid_amount[$payments['order_item_id']] = $payments['amount'];
		$pays += $payments['amount'];	
	}
	

	//
	//Amount to process...
	if($_POST['MethodAmount'] == 'balance')
	{
		$process_amount = ($cost - $pays);
	}
	if($_POST['MethodAmount'] == 'deposit')
	{
		$process_amount = $_POST['alt_pay'];
	}
}



if($process_amount > 0)
{
	//
	// Prepare request arrays
	$DPFields = array(
		'paymentaction' => 'Sale', 
		'ipaddress' => $_SERVER['REMOTE_ADDR'], 
		'returnfmfdetails' => '0'
	);
	
	//
	//Set basic credit card details				
	$CCDetails = array(
						'TENDER' => 'C',					 											// Required
						'TRXTYPE' => 'S',					 											// Required
						'ACCT' => $_POST['acctnum'], 													// Required
						'EXPDATE' => date('m', strtotime($_POST['exp_month'])) . substr($_POST['exp_year'],2,2),  // Required
						'CVV2' => $_POST['cvv2'],
						'AMT' => number_format($process_amount,2)										// Required
					);
	
	$PayPalRequestData = $CCDetails;
}

//
//Check to see if we have a logged in user, if not, then we need to create one
if(!is_user_logged_in())
{
	$user = new USER();
	
	//
	//Create a new user
	$user_info['user_login'] = $user->temp_user_name();
	$user_info['user_pass'] = $_POST['lastname'];
	$user_info['first_name'] = $_POST['firstname'];
	$user_info['last_name'] = $_POST['lastname'];
	$user_info['user_email'] = $_POST['email'];
		
	$new_user = $user->create_user($user_info);
	
	//
	//Log in the user
	if($new_user)
	{
		wp_set_current_user($new_user, $user_info['user_login']);
		wp_set_auth_cookie($new_user);
		do_action('wp_login', $user_info['user_login']);		
	}
	else
	{
		//
		//Error, user cannot be created for some reason
	}
}

?>

<?php get_header(); ?>

Receipt


<?php
//echo var_dump($_POST);

/*
Run card for processing

If successful
	Write RSVP record for all 0 items
	Write payment record for all items
	Send Receipt
	Remove Cart Items
	Open MyISL Page
Else
	Error


*/

//
// Pass data into class for processing with PayPal and load the response array into $PayPalResult
//$PayPalResult = $PayPal->DoDirectPayment($PayPalRequestData);
if($process_amount > 0)
{
	$PayPalResult = $PayPal->ProcessTransaction($PayPalRequestData);
}

//
//Write Transaction Record
$finance = new FINANCE();

if($process_amount > 0)
{
	$trans_data = 	array('amount' => $PayPalResult['AMT'],
						  'form' => $_POST['card'],
						  'response' => $PayPalResult['RESPMSG'],
						  'transaction_id' => $PayPalResult['PNREF'],
						  'raw_request' => $PayPalResult['RAWREQUEST'],
						  'raw_response' => $PayPalResult['RAWRESPONSE']
						  );
}
else
{
	$trans_data = array('amount' => 0,
						'form' => 'No Payment',
						'response' => 'Approved',
						'transaction_id' => date('Ymdhis'),
						'raw_request' => 'No Request Data,  No Payment Due',
						'raw_response' => 'Approved with 0 down'
						);
						
	$PayPalResult['RESPMSG'] = "Approved";
}
$finance->new_transaction($trans_data);	

//
//If our request was approved.... [ACK] == Success
if(!empty($PayPalResult['RESPMSG']) && $PayPalResult['RESPMSG'] === 'Approved')
{
	if($page_type=="checkout")
	{
		//
		//Start a new Order and new RSVP
		$finance->new_order();
		$rsvp = new RSVP();

		//
		//Migrate Cart Items to Order Items and RSVP Items
		foreach($cart->organized_items() as $cart_id => $cart_data)
		{
			//
			//Reset list of "primary" items for this cart grouping
			$primaries = array();
			foreach($cart_data as $item_id => $itm_data)
			{
				//
				//Set the list of primaries to a tmp so we can iterate and unset each
				$tmp_primaries = $primaries;

				for($cnt = 1; $cnt <= $itm_data['quantity']; $cnt++)
				{
					//
					//If we have an add_on, then apply it to the "next" primary item
					if($itm_data['type']==0)
					{
						if(!empty($tmp_primaries))
						{
							$apply_to = current($tmp_primaries);
							unset($tmp_primaries[key($tmp_primaries)]);	
						}
						else
						{
							$apply_to = 0;	
						}
						
						$break_point = strrpos($item_id, "_fee");
						if($break_point > 0)
						{
							$item_id - substr($item_id, 0, $break_point);
						}
					}


					$item_data = 	array('type' => $itm_data['type'],
										  'item_id' => $item_id,
										  'note' => '',
										  'add_on' => (($itm_data['add_on'] > 0) ? $apply_to : $itm_data['add_on']),
										  'quantity' => 1,
										  'price' => $itm_data['cost']
										  );
					$item_ins_id = $finance->new_item($item_data);
	
					//
					//If we don't have an add_on then....
					if($itm_data['type'] != 0)
					{
						$primaries[$item_ins_id] = $item_ins_id;	
					}
					
					//
					//RSVP
					if($item_data['type'] == 1)
					{
						$rsvp_for = ($cnt == 1 ? get_current_user_id() : 0);
						$rsvp->new_rsvp($item_id, $rsvp_for, $item_ins_id);
						$main_item = $item_ins_id;

						//
						//Add CAP to the cart
						if(!empty($settings['campus_ambassador_fee']) && !empty($_REQUEST['repcode'])) 
						{
							//
							//Add discount for CAP code
							$cap_data = array('type' => 61,
											  'item_id' => $item_id,
											  'note' => $_REQUEST['repcode'],
											  'add_on' =>  $item_ins_id,
											  'quantity' => 1,
											  'price' => ($settings['campus_ambassador_fee'] * -1)
											  );
							
							$cap_ins_id = $finance->new_item($cap_data);
							$finance->payment($cap_ins_id, 0);
						}

					}
	
					//
					//Mark payments
					switch($_POST['MethodAmount'])
					{
						case 'deposit':
							switch($item_data['type'])
							{
								case 1:
									$mod_amount = $cart->deposit_amount($item_id);
									break;
								default:
									$mod_amount = $item_data['price'];
									break;	
							}
							break;
						case 'balance':
							$mod_amount = $item_data['price'];
							break;
					}
					$finance->payment($item_ins_id, $mod_amount);
				}
			}
		}
	}
	else
	{
		//
		//Figure out how to divide payment...
		$items = array();
		foreach($finance->payments($all_items) as $index=>$data)
		{
			if(empty($items[$data['order_item_id']]['payments'][0]))
			{
				$items[$data['order_item_id']]['payments'][0] = 0;
			}
		
			$items[$data['order_item_id']]['payments'][0] += $data['amount'];
			$items[$data['order_item_id']]['payments'][$data['created_when']] = $data['amount'];
		}
		
		foreach($finance->items_cost($all_items) as $index=>$data)
		{
			if(empty($items[$data['id']]['cost'][0]))
			{
				$items[$data['id']]['cost'][0] = 0;
			}
		
			$items[$data['id']]['cost'][0] += $data['price'];
			$items[$data['id']]['cost'][$data['created_when']] = $data['price'];
			$items[$data['id']]['add_on'] = $data['add_on'];
			$items[$data['id']]['item_type'] = $data['item_type'];
		}
		
		foreach($all_items as $item_id)
		{
			if(!empty($items[$item_id]['payments'][0]))
			{
				$cur_pay = $items[$item_id]['payments'][0];	
			}
			else
			{
				$cur_pay = 0;	
			}
			$items[$item_id]['balance'] = $items[$item_id]['cost'][0] - $cur_pay; 	
		}

		//
		//Apply payment to Application Fees, then major items, then add-ons
		$finance->new_order();
		$checks = array_keys($items);
		$passes = 4;
		$funds = $process_amount;
		
		if(!empty($_REQUEST['donate']))
		{
			$special = "Donation";
		}
		else
		{
			$special = NULL;
		}
			
		
		while ($passes > 0 && count($checks) > 0)
		{
			foreach($items as $item_id => $data)
			{
				if($passes == 3 && $data['item_type'] == 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff, 0, '', $special);	
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
				}
				
				if($passes == 2 && $data['item_type'] == 1 && $data['add_on'] == 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff, 0, '', $special);	
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
					
				}

				if($passes == 1 && $data['item_type'] > 1 && $data['add_on'] == 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff, 0, '', $special);	
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
					
				}

				if($passes == 2 && $data['item_type'] > 1 && $data['add_on'] > 0)
				{
					if($data['balance'] > 0)
					{
						$payoff = min($funds, $data['balance']);
						$finance->payment($item_id, $payoff, 0, '', $special);
						$funds -= $payoff;
						unset($checks[$item_id]);
					}
					
				}

				$passes --;
			}
		}
					
	}

	//
	//Clear Cart
	if($page_type == "checkout")
	{	
		//$cart->clear_cart();
		//echo var_dump($cart->list_items());
	}
// Write the contents of the response array to the screen for demo purposes.
//echo '<pre />';
//print_r($PayPalResult);


?>	
<div class="cart-banner"></div>

    <div id="content">
    
        <div id="inner-content" class="row">
    
			<main id="cart" class="large-10 medium-12 small-centered columns" role="main"> 

	            <h2>Receipt for your Payment</h2>
	            <hr />   
	            <div class="team-cart row">
	                
					<?php
					$objects = 0;
					$teams = 0;
					$total = 0;
					$team_ids = array();

					if($page_type=="checkout")
					{
						foreach($cart->organized_items() as $cart_id => $cart_data)
						{
							$discount = 0;
							
							echo "<div class='item large-12 medium-12 columns'>";
		
							$count_fees = 0;
							$line_sub_total = 0;
							$trip_id = 0;
		
							foreach($cart_data as $item_id => $item_data)
							{
								
								switch($item_data['type'])
								{	
									case 0:
										$count_fees += $item_data['quantity'];
										$line_sub_total += ($item_data['cost'] * $item_data['quantity']);
									?>
										<div class="details medium-6 columns">
											<span class="small">+ Non-refundable Application Fee</span>
										</div>
										
										<!-- <div class="medium-3 columns">
											&nbsp;
										</div> -->
		
										<div class="team-totals medium-6 columns">
											<span class="small"><strong>App Fee</strong></span>: $<?php _e($item_data['cost'])?><br />
										</div>
									<?php
										break;
									case 1:
										//
										//Gather Team, Country, Program Details
										//
										$team = new TEAM();
										$country = new COUNTRY();
										$program = new PROGRAM();
										$itinerary = new ITINERARY();
										$rsvp = new RSVP();
										
										$trip_id = $item_id;
										$objects ++;
										$teams ++;
										$team->set_team($item_id);
										$team_ids[] = $item_id;
										$country_list = $country->country_list($team->schedule_countries());
										
										//
										//Build program listings with line breaks between each.
										$programs = '';
										foreach($team->get_schedule() as $id=>$schedule_data)
										{
											$program->set_program($schedule_data['program']);
											$programs .= $program->get_name() . "<BR>";		
										}
										if(strlen($programs) > 3)
										{
											$programs =substr($programs,0,-4);
										}
		
										$line_sub_total += ($item_data['cost'] * $item_data['quantity']);
		
				
										?>
											<h5>Program: <?php _e($team->value('code'))?></h5>
											<div class="details medium-6 columns">
												<b>Program Details:</b> <?php _e($programs)?> in <?php _e($country_list)?><br />
												<input type="hidden" name="item_id" id="item_id_<?php _e($item_id)?>" value="<?php _e($team->get_team())?>">
												<input type="hidden" name="item_quan" id="item_quan_<?php _e($item_id)?>" value="1">
												<input type="hidden" name="add_on" id="add_on_<?php _e($item_id)?>" value="0">
												<input type="hidden" name="item_type" id="item_type_<?php _e($item_id)?>" value="1">
												<input type="hidden" name="item_cost" id="item_cost_<?php _e($item_id)?>" value="<?php _e($team->trip_cost())?>">
											</div>
				
											<div class="quantity medium-3 columns">
												 <b>Quantity:</b><br /> 
													<B><?php echo $item_data['quantity']?></B>									
											</div>
				
											<div class="team-totals medium-3 columns">
												<b>Trip Cost:</b> $<?php _e($item_data['cost'])?><br />
		
											</div>
										</div>
										<?php
										break;
									default:
										$objects++;
									?>
										<div class="details medium-6 columns">
											<span class="small">+ Shouldn't see this yet</span>
										</div>
										<div class="quantity medium-3 columns">
											&nbsp;
										</div>
										<div class="team-totals medium-3 columns">
											$ <?php _e($item_data['cost'])?><br />
										</div>

									<?php
										break;
								}
							}
						}
						?>

	                        <div class="subtotal">
	                            <div class="quantity medium-3 columns">
									&nbsp;
	                            </div>
	                            <div class="team-totals medium-3 columns">
	                                <span><b>Subtotal:</b> $<?php _e(number_format($line_sub_total,2) )?></span>
	                            </div>
	                        </div> <!-- End Subtotal -->

	                        <div class="subtotal">
	                            <div class="quantity medium-3 columns">
									&nbsp;
	                            </div>
	                            <div class="team-totals medium-3 columns">
	                                <span><b>Payment:</b> $<?php _e(number_format($process_amount,2) )?></span>
	                            </div>
	                        </div> <!-- End Subtotal -->

	                        <div class="subtotal">
	                            <div class="quantity medium-3 columns">
									&nbsp;
	                            </div>
	                            <div class="team-totals medium-3 columns">
	                                <span><b>Balance Due:</b> $<?php _e(number_format($line_sub_total - $process_amount,2) )?></span>
	                            </div>
	                        </div> <!-- End Subtotal -->

						</div> <!-- End large-12 class -->
						<?php
					} //End foreach cart-organized
					?>
					<hr />
	          	Complete your MyISL Volunteer Profile<br />
				<input class="button primary" type="button" value="Go to your MyISL Page" onclick="window.location.href = '/my-isl/'">
	            </div> <!-- End team cart -->   
	          
			</main><!-- End Main -->
		</div><!-- End Inner Content -->
	</div><!-- End Content -->

<?php
	
	//
	//Send email receipt
	foreach($team_ids as $this_team_id)
	{
echo $this_team_id;
		$all_order_ids = array();
		foreach($rsvp->order_id_by_team($this_team_id) as $this_order_id)
		{
			$all_order_ids[] = $this_order_id;
		}
echo var_dump($all_order_ids);
		
		$email->to($_POST['firstname'] . ' ' . $_POST['last_name'], $_POST['email'])	;
		$email_from('ISL Receipt', 'no-reply@islonline.org');
		$email->subject('ISL Receipt');
		$email->message($finance->get_invoice($this_team_id, $all_order_ids));
		$email_message->send();
	}
	
			
	/** Build teams and users for chatterbox plugin */
	if($page_type=="checkout")
	{
		foreach($_REQUEST['team_ids'] as $tid)
		{
			$uid = get_current_user_id();
			$chat_box = new CHATTERBOX();
			$chat_box->team($tid);
			$team->set_team($tid);
			$team->get_staff();
			$team_name = $team->value("code");
			if(!$chat_box->select_team($team_name))
			{
				$ids = $team->get_staff();
				$chat_box->privacy(false);
				$chat_box->insert_team($team_name,true);
				$chat_box->insert_user();
				foreach($ids as $staff_id)
				{
					$chat_box->insert_user($staff_id);
				}
				$chat_box->privacy(true);
				$chat_box->insert_team($team_name);
				$chat_box->insert_user();
				foreach($ids as $staff_id)
				{
					$chat_box->insert_staff($staff_id);
				}
			}
		}
	}
}
else
{ ?>
	<div class="cart-banner"></div>

	    <div id="content">
	    
	        <div id="inner-content" class="row">
	    
				<main id="cart" class="large-10 medium-12 small-centered columns" role="main"> 
					<?php
					//	error goes here...
					//foreach($PayPalResult['ERRORS'] as $index=>$error)
					//{
						//echo "Error " . $error['L_ERRORCODE'] . ": " . $error['L_LONGMESSAGE'] . "<br>";
					//}
					//echo $PayPalResult['RAWRESPONSE'];	
					echo "<h2>Processing Error</h2>";
					echo "There were problems processing your credit card. The following errors were found:<br /><br />";
					echo "Error(s): " . $PayPalResult['RESPMSG'] . "<br /><br />";
					echo "The items have not been removed from your cart. Please try again shortly.";
					echo "<br /><br />";
					echo "<a href='/checkout'><i class='fa fa-long-arrow-left aria-hidden='true'></i>&nbsp;Return to Checkout</a>";
					}

					?>
				</main>
			</div>
		</div>

<?php get_footer(); ?>

