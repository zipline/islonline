<?php get_header(); ?>

<?php
/*	if (has_post_thumbnail()) {
		$thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' );
		$thumbnail_url = $thumbnail_data[0];
		} else {
			$thumbnail_url = get_template_directory_uri() . '/assets/images/placeholder.jpg';
		}*/
?>
<div id="blog-single">
	<?php
	if (has_post_thumbnail()) {
		$thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' );
		$thumbnail_url = $thumbnail_data[0]; ?>
		
	<div class="short-banner row" style="background-image: url('<?php echo $thumbnail_url; ?>'); background-size: cover; background-position: center center;"></div>
	<?php } else { ?>
	<div class="no-banner"></div>
	<?php	} ?>
	<div id="content">
		<div id="inner-content" class="row">
			<main id="main" class="large-10 medium-10 small-centered columns" role="main">
				<header class="article-header">	
					<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
				</header> <!-- end article header --> 	

			<?php get_template_part( 'parts/content', 'byline' ); ?>
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			    	<?php get_template_part( 'parts/loop', 'single' ); ?>
			    	
			    <?php endwhile; else : ?>
			
			   		<?php get_template_part( 'parts/content', 'missing' ); ?>

			    <?php endif; ?>

			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->
		<?php edit_post_link('edit'); ?>
	</div> <!-- end #content -->
</div>

<div class="cat-menu">
	<span>Categories:</span><?php wp_list_categories('title_li=&exclude=1'); ?>
</div>
<?php get_footer(); ?>