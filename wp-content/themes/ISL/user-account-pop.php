<?php
/*
Template Name: POP
*/

error_reporting(E_ALL);
ini_set("display_errors",1);

//
//Setup classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "user.class.php");
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "country.class.php");
require_once ($plugin_path . "program.class.php");
require_once ($plugin_path . "/tclpdf/tcpdf.php");

$user = new USER();
$rsvp = new RSVP();
$team = new TEAM();
$country = new COUNTRY();
$program = new PROGRAM();

	if ($_SERVER['REMOTE_ADDR'] == "74.93.28.29" || $_SERVER['REMOTE_ADDR'] == "74.93.28.30"){
		wp_set_current_user(33232);
		wp_set_auth_cookie(33232);
	}

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
/*
if(isset($_GET['rsvp']) && $_GET['rsvp'] > 0 && !$rsvp->valid($current_user->ID, $_GET['rsvp']))
{
	exit;
}
*/

$rsvp->set_rsvp_id($_GET['rsvp']);
$rsvp->fetch_rsvp();
$reservation = $rsvp->get_rsvp();

$team->set_team($reservation['team_id']);

$countries = array();
foreach($team->get_schedule() as $data)
{
	$country->set_country($data['country']);
	$country->set_region($data['region']);
	$this_region = $country->region_details();

	if(!isset($countries[$data['country']]))
	{
		$countries[$data['country']] = $country->get_name();
	}

	if($this_region['title'] != 'ALL')
	{
		if(substr($countries[$data['country']], -3) != ' & ')
		{
			$countries[$data['country']] .= ": ";
		}
		$countries[$data['country']] .= $this_region['title'] . ' & ';
	}
}

foreach($countries as $index => $cntry_data)
{
	if(strlen($cntry_data) > 2 && substr($cntry_data,-3) == ' & ')
	{
		$countries[$index] = substr($countries[$index], 0, -3);	
	}
	if(strlen($cntry_data) > 2 && substr($cntry_data,-1) == ':')
	{
		$countries[$index] = substr($countries[$index], 0, -1);	
	}
}

//$country_list = $country->display_country_list($country->country_array($team->schedule_countries()));
$country_list = implode(' & ', $countries);

//
//Build program listings with line breaks between each.
$programs = array();
foreach($team->get_schedule() as $id=>$schedule_data)
{
	if($program->is_program($schedule_data['program']))
	{
		$program->set_program($schedule_data['program']);
		$programs[$schedule_data['program']] = $program->get_name();
	}
}
$program_list = $program->display_program_list($programs);
$program_title = $program_list . ' in ' . $country_list;

$departure = DateTime::createFromFormat('m/d/Y', $team->departure_date());
$arrival = DateTime::createFromFormat('m/d/Y', $team->arrival_date());
$today = DateTime::createFromFormat('m/d/Y', date('m/d/Y'));

//echo var_dump($departure);
//echo var_dump($today);

$interval = $departure->diff($arrival);
$total_days = $interval->format('%d');
$service_days = ($total_days - 2);

$service_hours = ($service_days - ceil($service_days / 7)) * 8;

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = WP_CONTENT_DIR . '/default-pics/logo.png';
        $this->Image($image_file, 15, 10, '', 18, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
    }
}

	if($departure <= $today)
	{
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		//$pdf->addTTFfont('/var/www/dev.islonline.org/wp-content/plugins/command/includes/ttf/RobotoCondensed-Regular.ttf', 'TrueTypeUnicode','', 32);
			
		// set default header data
		$pdf->SetHeaderData( PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
		{
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('robotocondensed', '', 13, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();

		//
		//2018-03-21 DJT Removed dtop shadow
		// set text shadow effect
		//$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

		// Set some content to print
		$html = '';
		$html .= '<BR>';
		$html .= $departure->Format('m/d/Y') . '<BR><BR>';
		$html .= 'To Whom It May Concern: </BR>';
		$html .= 'International Service Learning (ISL) is a humanitarian organization that offers volunteers the unique opportunity to be part of an international team providing health and service work in an intercultural, experiential, and sustainable manner. The ISL experience offers volunteers the opportunity to step out of their own culture to learn first-hand about the realities of another part of the world and global health as a field. To that end, we provide an immersive experience with the aim of bringing positive change to not only the communities we touch, but each volunteer as well.<BR><BR>';

 		$html .= 'This letter is to inform you that ' . $user->user_name($_GET['user_id']) . ' was a member of a team of volunteers who participated in a ' . $program_list . ' service learning program with ISL in ' . $country_list . '. The team received special training seminars with local medical professionals and began a program of caregiving to our partner communities, who have little to no access to healthcare services. The participation in this program involved shadowing and assisting a licensed professional in service work under very challenging circumstances.<BR><BR>';

                                $html .= 'Our ' . $program_list . ' program is structured around full service days that require intercultural and interpersonal skills, serving long hours on the field, and working as a team to complete tasks. ' . get_user_meta($_GET['user_id'], 'first_name', true) . '\'s team travel dates were from ' . $team->arrival_date() . ' to ' . $team->departure_date() . '. ' . get_user_meta($_GET['user_id'], 'first_name', true) . ' participated in ' . $service_hours . ' hours of training/clinical work and cultural involvement.<BR><BR>';

		$html .= 'We appreciate the many volunteers who join our teams to bring hope and care to those who, because of their circumstances, do not have access otherwise to the resources ISL\'s volunteers provide.  If you have any further questions, please feel free to contact me.<BR><BR>';

		$html .= 'Regards,';

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', 30, $html, 0, 1, 0, true, '', true);

		//15 x 24
		$file_path = WP_CONTENT_DIR . '/plugins/command/images/jb_sig.png';
		$pdf->Image($file_path, 15, 207, 36, 22, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
		
		$sig_block = '';
		$sig_block .= 'Jonathan Birnbaum <BR>';
		$sig_block .= 'Owner/Executive Director <BR>'; 
		$sig_block .= 'Tel: 509-321-7560 <BR>';
		$sig_block .= '125 McKinley Ave <BR>';
		$sig_block .= 'Kellogg, ID 83837 <BR>';
		$sig_block .= '<BR>';
		$sig_block .= 'www.islonline.org';
		
		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', 228, $sig_block, 0, 1, 0, true, '', true);

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output('pop_letter.pdf', 'I');

		//============================================================+
		// END OF FILE
		//============================================================+
	}
	else
	{
	    echo 'Information here will be displayed after ';
	    echo $departure->Format('m/d/Y');
		//echo "Your letter will be here once your trip is complete";
	}