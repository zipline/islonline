<?php
/*
Template Name: User Settings Backup
*/
?>

<?php
//
//Setup classes
wp_enqueue_script('cmd_settings_pics', plugins_url() . '/command/js/settings.pics.js', array('jquery'), '4.6.9');	
wp_localize_script('cmd_settings_pics', 'svars', array('ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form')
														));

$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "user.class.php");
$user = new USER();

?>

<?php acf_form_head(); ?>
<?php wp_head(); ?>

<?php 

$user_id = get_current_user_id();
$fields = array(21681);


if($user->has_cap('cmd_isl_rep', $user_id))
{
	$fields[] = 20758; 	
}


acf_form(array('post_id' => 'user_' . $user_id,
				 'field_groups' => $fields
			 )
	  );

?>

<?php wp_footer(); ?>
