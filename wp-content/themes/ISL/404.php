<?php get_header(); ?>

<div class="page-404">		
	<div id="content">
		

		<div id="inner-content" class="row">
	
			<main id="main" class="large-6 medium-8 small-centered columns" role="main">

				<article id="content-not-found">
				
					<header class="article-header">
						<h3><?php _e( 'Not All Who Wander Are Lost', 'jointswp' ); ?></h3>
					</header> <!-- end article header -->
			
					<section class="entry-content">
						<p><?php _e( 'But you might be. The article you were looking for was not found, but maybe try looking again!', 'jointswp' ); ?></p>
					</section> <!-- end article section -->

					<section class="search">
					    <p><?php get_search_form(); ?></p>
					</section> <!-- end search section -->
			
				</article> <!-- end article -->
	
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->
	</div> <!-- end #content -->
</div>

<?php get_footer(); ?>