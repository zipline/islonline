<?php
/*
Template Name: User History
*/
?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>
<?php
//
//Setup classes
include_once(CC_CLASSES_DIR . 'finance.class.php');
include_once(CC_CLASSES_DIR . 'rsvp.class.php');

$rsvp = new RSVP();
$finance = new FINANCE();
$team = new TEAM();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;

if(!empty($_GET['rsvp']))
{
	$rsvp->set_rsvp_id($_GET['rsvp']);
	$rsvp->fetch_rsvp();
	
	$reservation = $rsvp->get_rsvp();
}

?>

<?php
$orders = $finance->transactions_by_user($current_user->ID);


//
//Need to cut out order information if team->value('no_finances') == 1
if(!empty($orders) && count($orders) > 0)
{
	echo "<div class='order-history'>";
	echo "<TABLE>";
	echo "<TR><TH>Item</TH><TH>Price</TH><TH>Payments</TH></TR>";
	
	foreach($orders as $index => $order_data)
	{
//echo var_dump($order_data);
		if(!empty($order_data['action']))
		{
			switch($order_data['action'])
			{
				case 'checkout':
					$title = 'Order' . $order_data['item'];
					break;
				case 'payment':
					$title = "Payment" . $order_data['item'];
					break;
				case 'donate':
					$title = "Donation";
					break;
				default:
					$title = $order_data['type'];
					break;
			}
			
			//$title = (($order_data['action'] == 'checkout') ? 'Order: ' : 'Payment: ') . $order_data['item'];
		}
		else
		{
			$title = $order_data['type'];
		}
		$url = '<a href="/shw_usr_rcpt/?transact_id=' . $order_data['transact'] . '" target="_blank">' . $title . '</a>';
		
		if($title != "Donation")
		{
			echo "<TR>";
			echo "  <TD>" . $url. (($order_data['hash'] != '') ? ' (' . $order_data['hash'] . ')' : '') . "</TD>";
			echo "  <TD>" . (substr($title,0,8) == "Donation" ? '&nbsp;' : $order_data['total']) . "</TD>";
			echo "  <TD>" . number_format($order_data['payment'] * (-1),2) . "</TD>";
			echo "</TR>";

		}
	}
	
	echo "</TABLE>";
	echo "</div>";
}
else
{
	echo "No orders";
}

?>

<?php wp_footer(); ?>
