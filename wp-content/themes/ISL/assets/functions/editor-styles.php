<?php
// cloud.typography.com fonts for admin
function load_custom_fonts($init) {
    $stylesheet_url = '//cloud.typography.com/7325974/7408752/css/fonts.css';

    if(empty($init['content_css'])) {
        $init['content_css'] = $stylesheet_url;
    } else {
        $init['content_css'] = $init['content_css'].','.$stylesheet_url;
    }

    return $init;
}
add_filter('tiny_mce_before_init', 'load_custom_fonts');

// Adds your styles to the WordPress editor
add_action( 'init', 'add_editor_styles' );
function add_editor_styles() {
    add_editor_style( get_template_directory_uri() . '/assets/css/style.css' );
}