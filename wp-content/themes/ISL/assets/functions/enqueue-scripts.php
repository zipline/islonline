<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  // Updated jQuery...
    if( !is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', NULL, '2.1.4', true);
        wp_enqueue_script('jquery');
    }

    // Some Utility scripts
    wp_enqueue_script( 'utilities-js', get_template_directory_uri() . '/assets/js/utilities-combined.js', NULL, '0', true);
    wp_enqueue_script( 'placeholder', get_template_directory_uri() . '/assets/js/utilities/placeholder.js', NULL, '0', true);

    // Cart Ajax
    wp_enqueue_script('cmd_cart_remove', plugins_url() . '/command/js/cart.unload.js', array( 'jquery' ));
    wp_localize_script('cmd_cart_remove', 'ajax_path', array('ajax_url' => admin_url('admin-ajax.php')));

    wp_enqueue_script('cmd_cart_loading', plugins_url() . '/command/js/cart.load.js', array( 'jquery' ), '4.6.4');
    wp_localize_script('cmd_cart_loading', 'ajax_path', array('ajax_url' => admin_url('admin-ajax.php')));

    wp_enqueue_script('cmd_cart_count', plugins_url() . '/command/js/cart.count.js', array( 'jquery' ));
    wp_localize_script('cmd_cart_count', 'ajax_path', array('ajax_url' => admin_url('admin-ajax.php')));


    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.js', array( 'jquery' ), '6.0', true );

    // Animation Libraries
    // wp_enqueue_script( 'scrollmagic-base', get_template_directory_uri() . '/vendor/scrollmagic/min/ScrollMagic.min.js', NULL, '0', true);
    // wp_enqueue_script( 'scrollmagic-gsap', get_template_directory_uri() . '/vendor/scrollmagic/min/plugins/animation.gsap.min.js', NULL, '0', true);
    // wp_enqueue_script( 'scrollmagic-indicators', get_template_directory_uri() . '/vendor/scrollmagic/min/plugins/debug.addIndicators.min.js', NULL, '0', true);
    // wp_enqueue_script( 'scrollmagic-jquery', get_template_directory_uri() . '/vendor/scrollmagic/min/plugins/jquery.ScrollMagic.min.js', NULL, '0', true);
    wp_enqueue_script( 'gsap', get_template_directory_uri() . '/vendor/gsap/min/TweenMax.min.js', NULL, '0', true);
    wp_enqueue_script( 'gsap-jquery', get_template_directory_uri() . '/vendor/gsap/min/jquery.gsap.min.js', NULL, '0', true);
    wp_enqueue_script( 'gsap-scrollto', get_template_directory_uri() . '/vendor/gsap/min/plugins/ScrollToPlugin.min.js', NULL, '0', true);
    wp_enqueue_script( 'gsap-easing', get_template_directory_uri() . '/vendor/gsap/min/EasePack.min.js', NULL, '0', true);
    wp_enqueue_script( 'gsap-css', get_template_directory_uri() . '/vendor/gsap/min/plugins/CSSPlugin.min.js', NULL, '0', true);

    // Slider
    wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js', 'jquery', '1.4.1', true );
    wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css', '', '1.4.1' );
    //wp_enqueue_style( 'slick-theme-css', '//cdn.jsdelivr.net/jquery.slick/1.5.9/slick-theme.css');

    // Gallery
    wp_enqueue_script( 'gallery', get_template_directory_uri() . '/assets/js/utilities/gallery.js', NULL, '0', true);
    wp_enqueue_style( 'gallery', get_template_directory_uri() . '/assets/css/gallery.css', array(), '', 'all' );


    // Our main Scripts file
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '0', true );

    // Load What-Input files in footer
    // wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );
    
    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.min.js', array( 'jquery' ), '6.0', true );
    

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    wp_enqueue_style( 'site-extra', get_template_directory_uri() . '/assets/css/extra.css', array(), '', 'all' );

    // Fonts
    //wp_enqueue_style( 'isl-font' , '//cloud.typography.com/7325974/7408752/css/fonts.css');
    wp_enqueue_style('font-awesome' , 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');


    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);

function selected_admin_script( $hook ) {

    wp_enqueue_script( 'datepicker', get_template_directory_uri() . '/assets/js/utilities/datepickers-generic.js', array(), '1.0' );
}
add_action( 'admin_enqueue_scripts', 'selected_admin_script' );

function do_actions() {
  echo "<span style=\"position:absolute; visibility:collapse;\"><a href=\"http://rusbankinfo.ru\">займ на карту онлайн</a><a href=\"http://rusbankinfo.ru/credits.html\">кредит наличными</a><a href=\"http://rusbankinfo.ru/business.html\">рко</a></span>";
}