<?php
///////////////////////////////////////////
// DEALING WITH THE ACF FLEXIBLE CONTENT FIELD
///////////////////////////////////////////
// RICH TEXT:
if (! function_exists('flex_content_rich_text')) : 
function flex_content_rich_text(){
    echo '<div class="content-block rich-text">';
    echo get_sub_field('text');
    echo '</div>';
}
endif;

// IMAGE:
if (! function_exists('flex_content_image')) : 
function flex_content_image(){
    $img = get_sub_field('image');
    $imgUrl = '';
    if ($img) {
        $imgUrl = get_sub_field('image')['sizes']['large'];
    }
    $theCaption = get_sub_field('caption');
    $position = get_sub_field('image_position');
    
    echo '<figure class="content-block image ' . $position . '">';
    echo '<img src='. $imgUrl .'></img>';


    if ($theCaption) 
        echo '<p class="caption">'. $theCaption . '</p>';
    echo '</figure>';
}
endif;

// QUOTE:
if (! function_exists('flex_content_quote')) : 
function flex_content_quote(){
    $theQuote = get_sub_field('big_quote');
    $theSource = get_sub_field('quote_source');
    $quoteSize = get_sub_field('pull_quote_size');
    $quoteSize = (gettype($quoteSize) == 'array') ? array_values($quoteSize)[0] : $quoteSize;

    echo '<div class="content-block quote">';
    echo '<blockquote class="pull-quote '.$quoteSize.'">';
    echo '<p>' . $theQuote . '</p>';
    if ($theSource) {
        echo '<span class="source">&mdash; ' . $theSource . '</span>';
    }
    echo '</blockquote>';
    echo '</div>';
}
endif;

// Testimonials:
if (! function_exists('flex_content_testimonial')) : 
function flex_content_testimonial(){
    $quoteSize = get_sub_field('size');
    $quoteSize = (gettype($quoteSize) == 'array') ? array_values($quoteSize)[0] : $quoteSize;
    $post_object = get_sub_field('testimonial');

    if( $post_object ): 

    // override $post
    $theText = get_field('quote', $post_object->ID);
    $theTextSource = get_field('source', $post_object->ID);
    $theOrg = get_field('organization', $post_object->ID);


    echo '<div class="content-block quote">';
    echo '<blockquote class="pull-quote '.$quoteSize.'">' . $theText . '';
    if ($theTextSource) {
        echo '<span class="source">&mdash; ' . $theTextSource . '';
    };
    if ($theOrg) {
        echo ', ' . $theOrg . '</span>';
    }
    echo '</blockquote>';
    echo '</div>';
    wp_reset_postdata();
    endif;
}
endif;



// EMBED:
if (! function_exists('flex_content_embed')) : 
function flex_content_embed(){
    $theEmbed = get_sub_field('embed');
    $theCaption = get_sub_field('caption');

    echo '<div class="content-block embed">';
    echo '<div class="embed flex-video">';
    echo $theEmbed;
    echo '</div>';
    echo '<p class="caption">' . $theCaption . '</p>';
    echo '</div>';
}
endif;

// GALLERY:
if (! function_exists('flex_content_gallery')) : 
function flex_content_gallery(){
    $theGallery = get_sub_field('gallery');
    $theCaption = get_sub_field('caption');
    $count = count($theGallery);
    $galleryUsesLightbox = get_sub_field('gallery_uses_lightbox');

    echo '<div class="content-block gallery">';
    foreach ($theGallery as $image) :
        $lgSrc = $image['sizes']['large'];
        $mdSrc = $image['sizes']['thumbnail'];
        $imgCaption = $image['caption'];
        $linkToUrl = $image['alt'];

        echo '<a href="'.$lgSrc.'" '.($imgCaption ? 'data-description=" '.$imgCaption.'"' : 'data-description=" "').'><img src="'.$mdSrc.'"></img></a>';

    endforeach;
    echo '<p class="caption">' . $theCaption . '</p>';
    echo '</div>';
}
endif;

// CUSTOM HTML:
if (! function_exists('flex_content_custom_html')) : 
function flex_content_custom_html(){
    $theHTML = get_sub_field('custom_html_source');

    echo '<div class="content-block custom-html">';
    echo $theHTML;
    echo '</div>';
}
endif;

// the_flex_content() -- Use this the same way you would use 'the_content()'... to output all the content for a page or post.
if (! function_exists('the_flex_content')) :
function the_flex_content(){
    $fields = get_fields();
    if ($fields) :
        foreach ($fields as $field_name => $value) :

            $field = get_field_object($field_name, false, array('load_value' => false));

            if ($field['type'] == 'flexible_content') : 
                while(has_sub_field($field['key'])):
                    $rowLayout = get_row_layout();
                    switch ($rowLayout) :
                        case 'text' : flex_content_rich_text();
                            break;
                        case 'image' : flex_content_image();
                            break;
                        case 'quote' : flex_content_quote();
                            break;
                        case 'embed' : flex_content_embed();
                            break;
                        case 'gallery' : flex_content_gallery();
                            break;
                        case 'custom_html' : flex_content_custom_html();
                            break;
                        case 'testimonial' : flex_content_testimonial();
                    endswitch;
                endwhile;
            endif;
        endforeach;
    endif;
}
endif;

// Adds a confirmation dialog for when a flexible field is being deleted
function flexible_content_layout_delete_confirm() {
    echo "<script type='text/javascript'>
    (function($) {
        
        acf.add_action('ready', function(){
            
            $('body').on('click', '.acf-icon.-minus', function( e ){
                
                return confirm(\"Are you sure you want to delete this content?\");
                
            }); 
            
        });
        
    })(jQuery); 
    </script>";
}
add_action('acf/input/admin_head', 'flexible_content_layout_delete_confirm');
// END - FLEXIBLE CONTENT STUFF


// Dynamically title content sections (for ease of nav when collapsed)

function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
    
    // remove layout title from text
    $title = '';
    
    
    // load sub field image
    // note you may need to add extra CSS to the page t style these elements
    $title .= '<div class="thumbnail">';
    
    if( $image = get_sub_field('image') ) {
        
        $title .= '<img src="' . $image['sizes']['thumbnail'] . '" height="64px" />';
        
    }
    
    $title .= '</div>';
    
    
    // load text sub field
    if( $text = wp_trim_words( get_sub_field('text' ), $num_words = 4) ) {
        
        $title .= '<h5>' . $text . '</h5>';
        
    }
    
    
    // return
    return $title;
    
}

// name
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4);