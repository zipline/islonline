<?php
// Register menus
register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu', 'jointswp' ),   // Main nav in header
		'footer-links' => __( 'Footer Links', 'jointswp' ),
        'primary-left' => __( 'Primary Left', 'jointswp' ),// Left Main
        'primary-right' => __( 'Primary Right', 'jointswp' ), // Right Main
        'info' => __( 'Info', 'jointswp' ) // Info Mega Menu
	)
);
//Left Menu
function left_nav() {
    wp_nav_menu(array(
        'menu'            => 'Primary Left',
        'theme_location'  => 'primary-left',
        'container'       => '',
        'menu_id'         => 'primary-left',
        'link_before'     => '<span>',
        'link_after'      => '</span>',
        'walker'=> new isl_nav_walker()
    ));
}

function right_nav() {
    wp_nav_menu(array(
        'menu'            => 'Primary Right',
        'theme_location'  => 'primary-right',
        'container'       => '',
        'menu_id'         => 'primary-right',
        'link_before'     => '<span>',
        'link_after'      => '</span>'
    ));
}

function info_nav() {
    wp_nav_menu(array(
        'menu'            => 'Info',
        'theme_location'  => 'info',
        'container'       => false,
        'menu_id'         => 'info',
        'link_before'     => '<span>',
        'link_after'      => '</span>'
    ));
}

// The Top Menu
function joints_top_nav() {
	 wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'menu_class' => 'vertical medium-horizontal menu',       // Adding custom nav class
        'items_wrap' => '<ul id="%1$s" class="%2$s" data-responsive-menu="accordion medium-dropdown">%3$s</ul>',
        'theme_location' => 'main-nav',        			// Where it's located in the theme
        'depth' => 5,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Topbar_Menu_Walker()
    ));
} /* End Top Menu */

// The Off Canvas Menu
function joints_off_canvas_nav() {
	 wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'menu_class' => 'vertical menu',       // Adding custom nav class
        'items_wrap' => '<ul id="%1$s" class="%2$s" data-accordion-menu>%3$s</ul>',
        'theme_location' => 'main-nav',        			// Where it's located in the theme
        'depth' => 5,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Off_Canvas_Menu_Walker()
    ));
} /* End Off Canvas Menu */

// The Footer Menu
function joints_footer_links() {
    wp_nav_menu(array(
    	'container' => 'false',                              // Remove nav container
    	'menu' => __( 'Footer Links', 'jointswp' ),   	// Nav name
    	'menu_class' => 'menu',      					// Adding custom nav class
    	'theme_location' => 'footer-links',             // Where it's located in the theme
        'depth' => 0,                                   // Limit the depth of the nav
    	'fallback_cb' => ''  							// Fallback function
	));
} /* End Footer Menu */

// Header Fallback Menu
function joints_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => '',      // Adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // Before each link
        'link_after' => ''                             // After each link
	) );
}

// Footer Fallback Menu
function joints_footer_links_fallback() {
	/* You can put a default here if you like */
}