<?php 

// Force the Gravity Forms JS to happen in the footer
add_filter('gform_init_scripts_footer', '__return_true');
// The next two function/filters are to ensure that GF still works when the AJAX option is enabled. 
// (as GF likes to put its Ajax-related scripts in the page right after the form, and not at the bottom LIKE IT SHOULD.)
function wrap_gform_cdata_open( $content = '' ) {
    $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
    return $content;
}
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_close( $content = '' ) {
    $content = ' }, false );';
    return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );

add_filter( 'gform_confirmation_anchor', '__return_true' );