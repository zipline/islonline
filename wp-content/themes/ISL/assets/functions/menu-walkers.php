<?php 
// Big thanks to Brett Mason (https://github.com/brettsmason) for the awesome walker
class Topbar_Menu_Walker extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = Array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"menu\">\n";
    }
}

class Off_Canvas_Menu_Walker extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = Array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"vertical nested menu\">\n";
    }
}

/**
* filter menu items to replace login link with logout link when user logged in
* @param string $item_output
* @param WP_Post $item
* @return string
*/
function menu_add_login_logout($item_output, $item) {
    if ($item->type == 'custom' && strpos($item->url, 'login') !== false) {
        if (is_user_logged_in()) {
            $item_output = '<li class="menu-item"><a href="/my-isl"><span>My ISL</span></a><ul class="sub-menu">';
            $logout_url = wp_logout_url();

            if (login_user_role() != "volunteer")
			{
				if(login_user_role() != "enhanced" || (login_user_role() == "enhanced" && poc_not_ambassador()))
				{
                	$item_output .= '<li><a href="'.admin_url().'">Admin</a></li>';
				}
            }

            $item_output .= '<li class="menu-item"><a href="'. $logout_url .'"><span>Logout</span></a></li></ul></li>';
        }
    }
    if ($item->type == 'custom' && strpos($item->url, 'logout') !== false) {
        if (!is_user_logged_in()) {
            $item_output = '';
        }
    }
 
    return $item_output;
}
add_filter('walker_nav_menu_start_el', 'menu_add_login_logout', 10, 2);

function login_user_role()
{
	try
	{
		$roles = get_user_meta(get_current_user_id(), 'wp_capabilities', true);
		if(is_array($roles))
		{
			$roles = array_keys($roles);
			reset ($roles);
			return current($roles);
		}
		else
		{
			return "volunteer";
		}
	}
	catch (Exception $e)
	{
		return "volunteer";
	}
}

function poc_not_ambassador()
{
	if(get_user_meta(get_current_user_id(), 'cmd_isl_rep', true))
	{
		if(get_user_meta(get_current_user_id(), 'cmd_custom_team_poc', true))
		{
			return true;
		}
		
		return false;
	}
	
	return true;
}