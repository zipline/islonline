jQuery(document).ready(function(){
    jQuery('.hasDatepicker').each(function(){
        jQuery(this).datepicker({
            changeMonth : true
            ,changeYear : true
            ,dateFormat : 'mm-dd-yy',
            beforeShow: function(event, ui) {
           jQuery('#ui-datepicker-div').addClass('open');
        },
        onClose: function(event, ui) {
            jQuery('#ui-datepicker-div').removeClass('open');
        }
        });
    });
});