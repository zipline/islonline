jQuery(document).foundation();

jQuery(document).ready(function() {
    
    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();
    
	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );
	
	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='flex-video'/>");

    // placeholders - requires that placeholder.js be present
    $('input, textarea').placeholder();

    $('.home-slick-slider').slick({
    	autoplay: false,
    	speed: 400,
    	slidesToShow: 1,
    	slidesToScroll: 1,
    	centerMode: true,
    	centerPadding: '-100px',
    	autoplaySpeed: 2000,
    	focusOnSelect: true,
    	dots: true,
    	initialSlide: 1,
    	prevArrow: '<button type="button" class="slick-prev"><img src="/wp-content/themes/ISL/assets/images/arrow-left.svg" /></button>',
    	nextArrow: '<button type="button" class="slick-next"><img src="/wp-content/themes/ISL/assets/images/arrow-right.svg" /></button>',
    	responsive: [
    	    {
    	      breakpoint: 640,
    	      settings: {
    	        arrows: false
    	      }
    	    }
    	  ]
    });

    $('.ratings-slider').slick({
    	autoplay: false,
    	speed: 500,
    	centerMode: true,
    	centerPadding: '50',
    	autoplaySpeed: 2000,
    	slidesToShow: 3,
    	  responsive: [
    	    {
    	      breakpoint: 800,
    	      settings: {
    	        arrows: false,
    	        centerMode: false,
    	        centerPadding: '40px',
    	        slidesToShow: 2
    	      }
    	    },
    	    {
    	      breakpoint: 600,
    	      settings: {
    	        arrows: false,
    	        dots: true,
    	        centerMode: false,
    	        centerPadding: '40px',
    	        slidesToShow: 1
    	      }
    	    }
    	  ]		
    });

    $('.quote-slider').slick({
    	dots: true,
    	autoplay: false,
    	speed: 500,
    	autoplaySpeed: 2000,
    	slidesToShow: 3,
        centerMode: true,
        centerPadding: '0px',
    	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
    	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right"></i></button>',
    	  responsive: [
    	    {
    	      breakpoint: 1024,
    	      settings: {
    	        arrows: false,
                centerMode: true,
                centerPadding: '0px',
    	        slidesToShow: 1
    	      }
    	    },
    	    {
    	      breakpoint: 800,
    	      settings: {
    	        arrows: false,
                centerMode: true,
                centerPadding: '0px',
    	        slidesToShow: 1
    	      }
    	    }
    	  ]		
    });

    $('.gallery').each(function() { 
        $(this).magnificPopup({
            delegate: 'a', 
            type: 'image',
            gallery: {
              enabled:true
            },
            image: {
            	titleSrc: function(item) {
    				return '<p>' + item.el.attr('data-description') + '</p>';
    			}
            }
        });
    });
                       
  	$('.view-buttons a').click(function() {
        $("a.active").removeClass("active");
        $(this).addClass('active');
	});

    $('.view-buttons a').click(function() {
        if( $("a.list-view").hasClass('active') ) {
            $('#list').addClass("active",1000, "easeOutBounce" );
            $('#grid').removeClass("active",1000, "easeOutBounce" );
        } else if( $("a.card-view").hasClass('active')) {
            $('#list').removeClass("active",1000, "easeOutBounce" );
            $('#grid').addClass("active",1000, "easeOutBounce" );
        }
    });


    // Nav
    var infoTrigger = $('#info-panel-trigger');
    var navPanel = $('#nav-panel');
    var showInfoNavPanel = function() {
        var x = infoTrigger.offset().left;
        var y = infoTrigger.offset().top;
        var h = infoTrigger.height();
        infoTrigger.addClass('focus');
        navPanel.css({
            display: 'block'
            ,position: 'fixed'
            ,left: x
            ,top: (y - $(document).scrollTop() + h)
        });
    };
    var hideInfoNavPanel = function() {
        navPanel.css({display: 'none'});
        infoTrigger.removeClass('focus');
    };
    navPanel.on('mouseover', showInfoNavPanel);
    navPanel.on('mouseleave', hideInfoNavPanel);
    infoTrigger.on('mouseenter', showInfoNavPanel);
    infoTrigger.on('mouseleave', hideInfoNavPanel);

    // Sticky Title Bar
    if ($('body').hasClass('home')) {
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            if (scroll > 140) {
                $('#masthead').addClass('stuck');
            }
            if (scroll < 140) {
                $('#masthead').removeClass('stuck'); 
            }
        });
    } else {
        $('#masthead').addClass('stuck');
    }
});