<?php
/*
Template Name: Registration
*/
?>

<?php get_header(); ?>
	<?php 
    $src = get_field('image')['sizes']['large'];     
    $vposition = get_field('image_position'); 
    $hposition = get_field('image_position_hz');
    $color = get_field('title_color');
    $overlay = get_field('banner_image_overlay');
?>

<div class="short-banner" style="background-image: url(<?php echo $src ?>); background-position: <?php echo $vposition; ?> <?php echo $hposition; ?>;">
	
	<header class="entry-header">
		<h1 class="entry-title" style="color: <?php echo $color; ?>;"><?php the_title(); ?></h1>
	</header>  
    
    <div class="overlay" style="background-color: <?php echo $overlay; ?>;"></div>      

</div>
			
<div id="content">
	<div class="large-6 medium-8 small-centered columns">
		<div class="register">
			<?php echo do_shortcode('[wppb-register form_name="standard-registration"]'); ?>
		</div>
	</div>
		
</div> <!-- end #content -->

<script>
jQuery(document).ready(function()
{
	var timer;
	
	jQuery('#content').on('keyup', '#username', function () 
	{
		clearTimeout(timer);
		timer = setTimeout( function()
		{
			var username = jQuery('#username');
			var userfield = jQuery('.wppb-default-username');

			jQuery.post(
				'<?php echo admin_url( 'admin-ajax.php' )?>', 
				{
					'action': 'cmd_user_name_check',
					'data':   {'usernameToCheck': username.val()} 
				}, 
				function(response)
				{ 
					if(response == 1) 
					{
						//username.css('border-color', 'green 5px solid');
						console.log('Green');
						//username.attr('class', 'validateGreen');
						//username.addClass('validateGreen');
						userfield.addClass('available');
						//username.removeClass('validateRed');
						userfield.removeClass('unavailable');
					} 
					else 
					{
						console.log('Red');
						//username.addClass('validateRed');
						userfield.addClass('unavailable');
						//username.removeClass('validateGreen');
						userfield.removeClass('available');
					}
				}
		   );
		}, 750);
	});
});
</script>
	
<?php get_footer(); ?>
