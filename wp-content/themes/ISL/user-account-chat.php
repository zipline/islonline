<?php
/*
Template Name: Chatter
*/
?>
<?php require_once(get_template_directory().'/my-isl-subpage-head.php'); ?>
<?php wp_head(); ?>

<?php

require_once(CC_CLASSES_DIR . 'rsvp.class.php');
require_once(CC_CLASSES_DIR . 'user.class.php');
$rsvp = new RSVP();

//
//Make sure this user is good for this RSVP or exit
$current_user = wp_get_current_user();
//if(!$rsvp->valid($current_user->ID, $_GET['rsvp']))
//{
//	exit;
//}

//
//Check to see if chat is on for public chat
if(isset($_GET['team_id']) && isset($_GET['private']) && $_GET['private'] != 'true')
{
	include_once(CC_CLASSES_DIR . 'team.class.php');
	$team = new TEAM();
	
	$team->set_team($_GET['team_id']);
	
	if($team->value('team_chat') == 0)
	{
		exit;
	}
}

if( isset( $_GET['admin'] ) && $_GET['admin'] == 0 )
{
	$user = new USER();
	$the_user_id = (($user->user_group(get_current_user_id())=="super" || $user->user_group(get_current_user_id())=="admin") ? 0 : get_current_user_id());

	if( $the_user_id == 0 )
	{
		add_filter('chatterbox_last_read_user', 'dash_last_read_user_override', 99, 1);
	}
}

function dash_last_read_user_override($user_id)
{
	return 0;
}

//
//Checking all values are set
if (isset($_GET['team_id']) && isset($_GET['user_id']) && isset($_GET['private'])) 
{

	if($_GET['private'] == 'true')
	{
		?>
		<script>
			window.parent.document.getElementById("private_chat_notification").style.visibility = 'hidden';
		</script>
		<?php	
	}
	else
	{
		?>
		<script>
			window.parent.document.getElementById("public_chat_notification").style.visibility = 'hidden';
		</script>
		<?php	
	}
	do_shortcode('[chatterbox_chat team_id=' . $_GET['team_id'] . ' user_id=' . $_GET['user_id'] . ' private=' . $_GET['private'] . ' page=' . get_the_title() . ']');
} 
else 
{
	exit;
}

wp_footer();
?>