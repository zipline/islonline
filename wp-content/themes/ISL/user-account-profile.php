<?php
/*
Template Name: User Profile
*/
?>
<?php

require_once(get_template_directory().'/my-isl-subpage-head.php'); 

wp_head();

//
//Setup classes
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_script('cmd_settings_pics', plugins_url() . '/command/js/settings.pics.js', array('jquery'), '4.7.3');	
wp_localize_script('cmd_settings_pics', 'svars', array( 'banner' => 0,
													    'user' => 1,
														'ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form')
														));
require_once CC_CLASSES_DIR . "user.class.php";
require_once CC_CLASSES_DIR . "tabs.class.php";
require_once CC_CLASSES_DIR . "acf.link.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";


$user = new USER();
$page = new TAB_MANAGE();
$rsvp = new RSVP();
$adv_custom_flds = new ACF_LINK();

$usr = wp_get_current_user();
$user_info = $usr;
$user_role = $user->user_group($usr->ID);




if(!user_can($usr->ID, 'upload_files'))
{
	$usr_upload = new WP_USER($usr->ID);
	$usr_upload->add_cap('upload_files');
}


//
//Make sure this user is good for this RSVP or exit
if(isset($_GET['rsvp']) && $_GET['rsvp'] > 0 && !$rsvp->valid($current_user->ID, $_GET['rsvp']))
	exit;


//var_dump($user_role);
//exit;

if(!empty($_POST['action']) && $_POST['action']=="update")
{
	$fld_list = $_POST;
	$user_fields = array('url', 'email', 'display_name', 
						 'first_name', 'last_name', 'nickname', 'description'
						);

	//
	//Fix role if it's changed.
	/*
	if(!empty($_POST['role']) && $user->user_group($user_info->ID) != $_POST['role'])
	{

		switch($user_role)
		{
			case 'super':
				$allow = true;
				break;
			case 'admin':
				if($_POST['role'] == 'staff')
				{
					$allow=true;
				}
			case 'staff':
				if($_POST['role'] == 'enhanced')
				{
					$allow=true;
				}
			default:
				$allow = false;
		}

		if($allow)
		{
			$user_updates = array('ID' => $_GET['user_id'],
								  'role' => $_POST['role']);
			wp_update_user($user_updates);
			unset($_POST['role']);
		}
	}
	*/

	//
	//Fix capabilities for users
	/*
	$groups = $user->list_groups();
	foreach($groups as $group_id => $data)
	{
		$user->set_group($group_id);
		$caps = $user->cap_details();
		
		foreach($caps as $cap_id => $cap_data)
		{
			if(isset($_POST[$cap_data['code']]))
			{
				update_user_meta($user_info->ID, $cap_data['code'], sanitize_text_field($_POST[$cap_data['code']]));
				unset($_POST[$cap_data['code']]);
			}
			else
			{
				delete_user_meta($user_info->ID, $cap_data['code']);
			}
		}
	}
	*/
	
	//
	//Save non- role/capability options	
	// All role and capability variables should be unset by now.
	foreach($_POST as $index=>$value)
	{
		if($index == 'cap_code')
			update_user_meta($_GET['user_id'], '_cmd_comm_ambassador_cap_code', $value);

		//
		//ACF Fields
		if($index == "acf")
		{
			foreach($value as $acf_index=>$acf_value)
			{
				//
				//ACF field so handle differently
				if(substr($acf_index,0,6) == "field_" && !empty($fld_list['acf'][$acf_index]))
				{
					$adv_custom_flds->save_field_group($acf_index, $fld_list['acf'], $_GET['user_id'], NULL, true);
				}
			}
		}
		
		//
		//Standard user field, so handle it normally
		if(!empty($fld_list[$index]) && in_array($index, $user_fields))
		{
			$user_updates = array('ID' => $_GET['user_id'],
								  'user_url' => trim($_POST['url']),
								  'user_email' => trim($_POST['email']),
								  'display_name' => trim($_POST['display_name']),
								  'first_name' => trim($_POST['first_name']),
								  'last_name' => trim($_POST['last_name']),		
								  'nickname' => trim($_POST['nickname']),
								  'description' => trim($_POST['description'])
								 ); 
			wp_update_user($user_updates);
			
			foreach($user_fields as $field)
			{
				unset($fld_list[$field]);
			}
		}

		if(!empty($value) && $index=="password" && $value != "****************")
		{
			wp_set_password($value, $_GET['user_id']);	
	
			if($_GET['user_id'] == $user_info->ID)
			{
    			do_action( 'wp_login', $user_info->user_login );
			}
		}
	}
}


?>
<div id="user-account-profile" class="iframe-inner">
    <form name="u_info" method = "POST" action="<?php $page->GET_path(array('save'))?>">
        <table>
            <tr><td colspan="2"><h4>Account Basics</h4></td></tr>

        	<tr class="user-user-login-wrap">
        		<th><label for="user_login">Username</label></th>
        		<td><input type="text" name="user_login" id="user_login" value="<?php _e($user_info->user_login)?>" disabled="disabled" class="regular-text" /> <span class="description">Usernames cannot be changed.</span></td>
        	</tr>

        	<tr class="user-first-name-wrap">
        		<th><label for="first_name">First Name</label></th>
        		<td><input type="text" name="first_name" id="first_name" value="<?php echo $user_info->first_name?>" class="regular-text" /></td>
        	</tr>

            <tr class="user-last-name-wrap">
                <th><label for="last_name">Last Name</label></th>
                <td><input type="text" name="last_name" id="last_name" value="<?php echo $user_info->last_name?>" class="regular-text" /></td>
            </tr>

            <tr class="user-nickname-wrap">
                <th><label for="nickname">Nickname <span class="description">(required)</span></label></th>
                <td><input type="text" name="nickname" id="nickname" value="<?php echo $user_info->nickname?>" class="regular-text" /></td>
            </tr>

            <tr class="user-display-name-wrap">
                <th><label for="display_name">Display name publicly as</label></th>
                <td>
                    <select name="display_name" id="display_name">
                    	<option <?php selected($user_info->display_name, $user_info->nickname)?>>
                        	<?php echo $user_info->nickname?>
                        </option>
                    	<option <?php selected($user_info->display_name, $user_info->first_name)?>>
                        	<?php echo $user_info->first_name?>
                        </option>
                    	<option <?php selected($user_info->display_name, $user_info->last_name)?>>
                        	<?php echo $user_info->last_name?>
                        </option>
                    	<option <?php selected($user_info->display_name, $user_info->first_name . ' ' . $user_info->last_name)?>>
                        	<?php echo $user_info->first_name . ' ' . $user_info->last_name?>
                        </option>
                    	<option <?php selected($user_info->display_name, $user_info->last_name . ' ' . $user_info->first_name)?>>
                        	<?php echo $user_info->last_name . ' ' . $user_info->first_name?>
                        </option>
                    </select>
                </td>
            </tr>

        	<tr class="user-email-wrap">
        		<th>
                	<label for="email">Email <span class="description">(required)</span></label>
                </th>
        		<td>
                	<input type="email" name="email" id="email" value="<?php echo $user_info->user_email?>" class="regular-text ltr" />
        		</td>
        	</tr>

            <tr id="pass" class="user-pass1-wrap">
                <th><label for="password">Password</label></th>
                <td>
                    <input class="text password-input" value="****************" id="password" name="password" type="password"/>
                    <button type="button" 
                            class="button button-secondary wp-generate-pw hide-if-no-js"
                            onClick="javascript:write_pass()">
                        Generate New Password
                    </button>
                </td>
            </tr>

            <tr><td colspan="2"><h4>Personal Information</h4></td></tr>

            <?php
                $content = $adv_custom_flds->get_fields(4761);
                
                foreach($content as $fld_id => $fld_data)
                {
                    echo "<TR>";
                    echo "<th><label for='acf-" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
                    echo "<td>" . $adv_custom_flds->build_field($fld_id, $_GET['user_id']);
                    echo "</td>";
                    echo "</TR>";

                }
            ?>

        	<tr class="user-url-wrap">
        		<th>
                	<label for="url">Website</label>
                </th>
        		<td>
                	<input type="url" name="url" id="url" value="<?php echo $user_info->user_url?>" class="regular-text ltr" />
                </td>
        	</tr>

        	<tr class="user-description-wrap">
        		<th>
                	<label for="description">Biographical Info</label>
                </th>
        		<td>
                	<textarea name="description" 
                    		  id="description" 
                              rows="5" 
                              cols="30"><?php echo trim($user_info->description);?>
                    </textarea>
        			<span class="description">Share a little biographical information to fill out your profile.</span>
                    <span class="description">This may be shown publicly.</span>
                </td>
        	</tr>


            <tr><td colspan="2"><h4>Additional Contact Information</h4></td></tr>

        	<?php
        		$content = $adv_custom_flds->get_fields(4860);
        		
        		foreach($content as $fld_id => $fld_data)
        		{
        			echo "<TR>";
        			echo "<th><label for='acf-" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
                	echo "<td>" . $adv_custom_flds->build_field($fld_id, $_GET['user_id']) . "</td>";
        			echo "</TR>";				
        		}
        	?>

        </table>

        <input type="hidden" name="action" value="update" />
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $_GET['user_id']?>" />

        <p>
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save"  />
        </p>
    </form>
</div>


<script>
	function write_pass() {
		document.getElementById('password').value = '<?php echo wp_generate_password()?>';	
	}

    jQuery(document).ready(function() {
        var source = jQuery('#image_src_field_56f882d3ccb2a').attr('src');
        jQuery('#header_user_image').attr('src', source);

        jQuery('#image_src_field_56f882d3ccb2a').on('load', function() {
            var source = jQuery('#image_src_field_56f882d3ccb2a').attr('src');
            jQuery('#header_user_image').attr('src', source);
        });

		jQuery('#acf-field_57249f7a0c63c').each(function(){
			jQuery(this).datepicker('destroy');

			jQuery(this).datepicker({
				changeMonth : true,
				changeYear : true,
                yearRange: "1900:<?php echo date('Y'); ?>",
				dateFormat : 'mm/dd/yy',
				beforeShow: function(event, ui) {
                   jQuery('#ui-datepicker-div').addClass('open');
                },
                onClose: function(event, ui) {
                    jQuery('#ui-datepicker-div').removeClass('open');
                }
			});
		});
    });
</script>

<?php wp_footer(); ?>