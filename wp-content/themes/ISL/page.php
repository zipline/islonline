
<?php get_header(); ?>
	
<?php get_template_part( 'banner' ); ?>

    <div id="content">
    
        <div id="inner-content" class="row">
    
            <main id="main" class="large-8 medium-12 small-centered columns" role="main">
        
    	      	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>
            
            </main> <!-- end #main -->
 
        </div> <!-- end #inner-content -->
    <?php edit_post_link('edit'); ?>
    </div> <!-- end #content --> 

<?php get_footer(); ?>