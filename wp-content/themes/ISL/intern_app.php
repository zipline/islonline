<?php
/*
Template Name: Internship App
*/
?>
<?php
if( !is_user_logged_in() )
{
	auth_redirect();
}

wp_enqueue_script("cmd_field_redirects", CC_JS_PATH . 'intern.js', array('jquery'), "0.2");
wp_localize_script('cmd_field_redirects', 'svars', array( 'banner' => 0,
													    'user' => 1,
														'ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form')
														));

$usr = wp_get_current_user();
if(!user_can($usr->ID, 'upload_files'))
{
	$usr_upload = new WP_USER($usr->ID);
	$usr_upload->add_cap('upload_files');
}


?>
<?php acf_form_head(); ?>
<?php get_header(); ?>

<div class="no-banner"></div>
    <div id="content">
    
        <div class="row">
    
            <main id="main" class="large-10 medium-12 small-centered columns" role="main">
                <?php the_flex_content(); ?>
            
            
            <div class="section">
				<h3>Internship Application</h3>
			
				<?php 
					$base_path = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;

					$post_html = '';
					if ( isset( $_REQUEST['country'] ) && isset( $_REQUEST['id'] ) ) {
						$post_html .= '<input type="hidden" name="country" id="country" value="' . $_REQUEST['country'] . '">';
						$post_html .= '<input type="hidden" name="intern" id="intern" value="' . $_REQUEST['id'] . '">';
					}

				if(isset($_REQUEST['pid']))
					{
						$post_id = $_REQUEST['pid'];
						$full_path = $base_path;
						
						$args = array('post_id'		=> $post_id,
									  'return' => $full_path,
									  'submit_value'		=> 'Update Internship',
									  'html_after_fields' => $post_html
									 );
					}
					else
					{
						$post_id = 'new_post';	
						$full_path = $base_path . '&pid=%post_id%';
						
							
						
						$args = array('post_id'		=> $post_id,
									  'new_post'	=> array(
											'post_type'		=> 'isl_intern_app',
											'post_status'		=> 'draft'
															),
									  'return' => $full_path,
									  'submit_value'		=> 'Request Internship',
									  'html_after_fields' => $post_html
									 );
					}
				
					if($post_id != "new_post")
					{
						echo "<div class='callout success' data-closable>Thank you for your request. We will be in contact with you soon.</div>";
					}
				
					acf_form($args);
				?>
			</div>
			</main> <!-- end #main -->
		</div>
	<div> 

<?php _e(get_footer())?>