<?php

//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "cart.class.php";
require_once $plugin_path . "itinerary.class.php";

//
//Gather Team, Country, Program Details
//
$team = new TEAM();
$country = new COUNTRY();
$program = new PROGRAM();
$itinerary = new ITINERARY();

$team->set_team($team->find_team_by_name(single_post_title('', false)));
$team_post = $team->value("post");
$team_img_post = get_post_meta($team_post, "_thumbnail_id", true);

if(!empty($team_img_post));
{
	$team_image = wp_get_attachment_image($team_img_post);
}


$country_list = $country->display_country_list($country->country_array($team->schedule_countries()));

//
//Build program listings with line breaks between each.
$programs = array();
foreach($team->get_schedule() as $id=>$schedule_data)
{
	$program->set_program($schedule_data['program']);
	$programs[$schedule_data['program']] = $program->get_name();
}
$program_list = $program->display_program_list($programs);


?>

<?php get_header(); ?>


<div id="team">
    <div class="banner" style="background: #dddddd;" <?php echo !empty($team_image) ? $team_image : '' ?>>
        
            <header class="entry-header">
                <h1><?php _e($program_list)?> in <?php _e($country_list)?></h1>
                <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
                <a class = "hollow button joiner" type="button" name="addtocart_0" id="addtocart_0">Join this Team!</a>
            </header>        
    </div>
    <div id="content">
        
            <div id="inner-content" class="row">
        
                <main id="main" class="large-8 medium-8 columns" role="main">
                <h2>About This Team</h2>
                <b>Country:</b> <?php _e($country_list)?><br />
                <div class="row collapse">
                <div class="medium-6 columns">
                    <b>Arrival Date:</b> <?php _e($team->arrival_date())?><br />
                    <b>Departure Date:</b> <?php _e($team->departure_date())?><br />
                    <b>Duration:</b> <?php _e($team->trip_duration())?> days<br />
                </div>
                <div class="medium-6 columns">
                    <b>Arrival City:</b> <?php _e($team->value('arrival_city'))?><br />
                    <b>Departure City:</b> <?php _e($team->value('departure_city'))?><br />
                    <b>Cost:</b> $<?php _e($team->trip_cost())?><br />
                </div>
                </div>
                <div class="details">
                    <?php _e($team->get_blurb('teaser')); //Team Teaser goes here?><br />
                </div>
                <a class = "button joiner" type="button" name="addtocart2_0" id="addtocart2_0">Join this Team!</a>
                <div class="section">  
                <h3>About <?php _e($program_list)?></h3>
                 	<?php
					$prog_posts = array();
					foreach($team->get_schedule() as $id=>$schedule_data)
					{
						$program->set_program($schedule_data['program']);
						if(empty($prog_posts[$program->post_id()]))
						{	
							echo count($prog_posts) > 0 ? "<BR>" : '';
							_e(do_shortcode('[acf field="program_teaser" post_id="' . $program->post_id() . '"]'));
							echo "<a href='" . get_permalink($program->post_id()) . "' class='button small'>Learn More</a>";
							$prog_posts[ $program->post_id()] = true;
						}
					}
					?>
                    
                </div>
                <div class="section">
                    <h3>About <?php _e($country_list)?></h3>
    				<?php
    				$ctry_posts = array();
					foreach($team->schedule_countries() as $id)
    				{
						$country->set_country($id)	;
						if(empty($ctry_posts[$country->post_id()]))
						{
							echo count($ctry_posts) > 0 ? "<BR>" : '';
							_e(do_shortcode('[acf field="country_teaser" post_id="' . $country->post_id() . '"]'));
							echo "<a href='" . get_permalink($country->post_id()) . "' class='button small'>Learn More</a>";
							$ctry_posts[$country->post_id()] = true;
						}
    				}   
    				?>
                </div>
                
                </main> <!-- end #main -->

                    <div class="medium-4 columns">
                        <h2>Itinerary</h2>
                        <div>
						<?php
                        //
						//Itenerary Goes Here
						//
						$itinerary->production(true);
						$itinerary->set_team($team->get_team());
						
						echo "<div class='itinerary'>";
						echo "<div class='header'>" . stripslashes($itinerary->header()) . "</div>";

						$details = $itinerary->details();
						if(count($details)>0)
						{
							foreach($details as $id=>$data)
							{
								echo "<div class='day'><b>Day " . $data['day'] . ": " . $data['title'] . "</b></div>";
								echo "<div class='desc'>" . stripslashes($data['description']) . "</div>";
							}
						}
						
						echo "<div class='footer'>" . stripslashes($itinerary->footer()) . "</div>";
						echo "</div>";
						?>
                        </div>
                    </div>
                    <div class="sharing medium-12 columns">
                        <hr />
                        <h2>Share this team!</h2>
                        <h3><a href="https://twitter.com/islonline" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.facebook.com/islonline" target="_blank"><i class="fa fa-facebook"></i></a></h3>
                    </div>
            </div> <!-- end #inner-content -->

    </div> <!-- end #content --> 
</div>

	<form name="team_add" id="team_add">
    	<input type="hidden" name="item_id" id="item_id_0" value="<?php _e($team->get_team())?>">
        <input type="hidden" name="item_quan" id="item_quan_0" value="1">
        <input type="hidden" name="add_on" id="add_on_0" value="0">
        <input type="hidden" name="item_type" id="item_type_0" value="1">
        <input type="hidden" name="item_cost" id="item_cost_0" value="<?php _e($team->trip_cost())?>">
    </form>
        


<?php _e(get_footer())?>
