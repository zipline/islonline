<?php
/*
Template Name: Small Banner
*/
?>

<?php get_header(); ?>
	<?php 
    $src = get_field('image')['sizes']['large'];     
    $vposition = get_field('image_position'); 
    $hposition = get_field('image_position_hz');
    $color = get_field('title_color');
    $overlay = get_field('banner_image_overlay');
?>

<div class="short-banner" style="background-image: url(<?php echo $src ?>); background-position: <?php echo $vposition; ?> <?php echo $hposition; ?>;">
	
    	<header class="entry-header">
			<h1 class="entry-title" style="color: <?php echo $color; ?>;"><?php the_title(); ?></h1>
		</header>  
    <div class="overlay" style="background-color: <?php echo $overlay; ?>;"></div>      
</div>
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-8 medium-8 medium-centered columns" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
