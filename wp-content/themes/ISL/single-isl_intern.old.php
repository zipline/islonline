<?php

//
//Javascript
wp_enqueue_script('cmd_intern_fields', plugins_url() . '/command/js/intern.js', array('jquery'), '4.7.0');	
wp_localize_script('cmd_intern_fields', 'svars', array('ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form'),
													    'user' => 1
														));


//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "cart.class.php";
require_once $plugin_path . "itinerary.class.php";
require_once $plugin_path . "rsvp.class.php";

//echo var_dump($_REQUEST);

//
//Gather Team, Country, Program Details
//
$team = new TEAM();
$country = new COUNTRY();
$program = new PROGRAM();
$itinerary = new ITINERARY();
$rsvp = new RSVP();

$team->set_team($team->find_team_by_name(single_post_title('', false)));
$team_post = $team->value("post");
$team_img_post = get_post_meta($team_post, "_thumbnail_id", true);

if(!empty($team_img_post))
{
	$source = wp_get_attachment_image_src($team_img_post, 'large');
	$image = $source[0];
}
else
{
	//
	//Find program image
	if(has_post_thumbnail($team->value('post')))
	{
		$source = wp_get_attachment_image_src(get_post_thumbnail_id($team->value('post')), 'large');
		$image = $source[0];
	}
	else
	{
		$prgms = $team->schedule_programs();
		$prgm = $program->set_program($prgms[0]);
		$pid = $program->post_id();
		$source = get_field("image", $pid);
									
		if($source)
		{
			$image = $source['url'];
		}
		else 
		{
			$cntrys = $team->schedule_countries();
			$cntry = $country->set_country($cntrys[0]);
			$cid = $country->post_id();
			$source = get_field("image", $cid);
			$image = $source['url'];
		}
	}
}

$country_list = $country->display_country_list($country->country_array($team->schedule_countries()));

//
//Build program listings with line breaks between each.
$programs = array();
foreach($team->get_schedule() as $id=>$schedule_data)
{
	$program->set_program($schedule_data['program']);
	$programs[$schedule_data['program']] = $program->get_name();
}
$program_list = $program->display_program_list($programs);

$description = the_field('program_teaser');

?>

<?php acf_form_head();?>

<?php
/*
//
//Redirect to payment page
?>
<form name="data" id="data" method="POST" enctype="multipart/form-data" action="/receipt">
	<input type="hidden" name="repcode" id="repcode" value="0">
	<input type="hidden" name="team_id" id="team_id" value="<?php echo $team->value('id') ?>">
	<input type="hidden" name="member_id" id="member_id" value="<?php echo $user->ID ?>"> 
	<input type="hidden" name="method" id="method" value="defined">
	<input type="hidden" name="action" id="action" value="checkout"> 
	<input type="hidden" name="team_ids[]" id="team_id" value="<?php echo $team->value('id'); ?>" />
</form>			

<script type="javascript">
	document.getElementByID('data').submit();
</script>
<?php
*/

?>

<?php get_header(); ?>

<div id="team">
    <div class="banner" style="background-image: url(<?php echo $image ?>);">
    	<header class="entry-header">
        	<h1><?php _e($program_list)?> in <?php _e($country_list)?></h1>
                <?php //echo '<h2 class="entry-title">' . $team->value('hash') . '</h2>'; ?>
 		</header>    
    <div class="overlay"></div>    
    </div>
    
	<div id="content">
	    <div id="inner-content" class="row">
    	    <main id="main" class="large-8 medium-8 small-centered columns" role="main">

                <h2>About This Internship</h2>
                <b>Country:</b> <?php _e($country_list)?><br />
                <div class="row collapse">
					<div class="medium-6 columns">
						<b>Duration:</b> <?php _e($team->trip_duration())?> days<br />
						<b>Cost:</b> $<?php _e($team->trip_cost())?><br />
					</div>
                </div>
                
                <div class="details">
                    <?php _e($team->get_blurb('teaser')); //Team Teaser goes here?><br />
                </div>
                <hr />

                <div class="section">  
                	<h3>About <?php _e($program_list)?></h3>
                 	<?php
					$prog_posts = array();
					foreach($team->get_schedule() as $id=>$schedule_data)
					{
						$program->set_program($schedule_data['program']);
						if(empty($prog_posts[$program->post_id()]))
						{	
							echo count($prog_posts) > 0 ? "<BR>" : '';
							_e(do_shortcode('[acf field="program_teaser" post_id="' . $program->post_id() . '"]'));
							echo "<a href='" . get_permalink($program->post_id()) . "' class='button small'>Learn More</a>";
							$prog_posts[ $program->post_id()] = true;
						}
					}
					?>
                    
                </div>
                <div class="section">
                    <h3>About <?php _e($country_list)?></h3>
    				<?php
    				$ctry_posts = array();
					foreach($team->schedule_countries() as $id)
    				{
						$country->set_country($id)	;
						if(empty($ctry_posts[$country->post_id()]))
						{
							echo count($ctry_posts) > 0 ? "<BR>" : '';
							_e(do_shortcode('[acf field="country_teaser" post_id="' . $country->post_id() . '"]'));
							echo "<a href='" . get_permalink($country->post_id()) . "' class='button small'>Learn More</a>";
							$ctry_posts[$country->post_id()] = true;
						}
    				}   
    				?>
                </div>
				
				<hr />

				<div class="section">
					<h3>Application</h3>
					<?php 
						acf_form(array(
							'post_id'		=> 'new_post',
							'new_post'		=> array(
								'post_type'		=> 'isl_intern',
								'post_status'		=> 'draft'
							),
							'submit_value'		=> 'Request Internship',
							'updated_message' => 'Thank you for your request. We will be in contact with you soon.'
						));
					?>
				</div> 			
  			</main> <!-- end #main -->

 		</div> <!-- end #inner-content -->
	</div> <!-- end #content -->
</div> <!-- end #team --> 

<form name="team_add" id="team_add">
	<input type="hidden" name="item_id" id="item_id_0" value="<?php _e($team->get_team())?>">
	<input type="hidden" name="item_quan" id="item_quan_0" value="1">
	<input type="hidden" name="add_on" id="add_on_0" value="0">
	<input type="hidden" name="item_type" id="item_type_0" value="1">
	<input type="hidden" name="item_cost" id="item_cost_0" value="<?php _e($team->trip_cost())?>">
</form>


<?php _e(get_footer())?>
