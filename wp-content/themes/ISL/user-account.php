<?php
/*
Template Name: My ISL
*/
?>

<?php
//
//Kick the user to the login page if they aren't logged in
//
if(!is_user_logged_in() == 1)
{
	header ('Location: ' . site_url() . '/login/');	
}
?>

<?php
include_once(CC_CLASSES_DIR . 'rsvp.class.php');

//
//Get user data
$current_user = wp_get_current_user();

$usr_upload = new WP_USER($current_user->ID);
$usr_upload->remove_cap('upload_files');
			
//
// Check to see if this is a transfer, and if so, move it
if(!empty($_GET['transfer']))
{
	//$base = urldecode($_GET['transfer']);
	$base = $_GET['transfer'];
	$items = explode("___" , $base);
	//
	//Check items to make sure all is as it should be....
	if(trim($current_user->user_email) == trim($items[0]))
	{
		if(get_post_meta($items[2], '_cmd_rsvp_shared_by', true) == $items[1])
		{
			//
			//Find out the RSVP number of our item
			$which_num = $wpdb->get_var('SELECT id 
										 FROM ' . $wpdb->prefix . 'command_RSVP 
										 WHERE (user_id=0 OR user_id=' . $items[1] . ') AND post_id=' . $items[2]);

			if($which_num == $items[3])
			{
				//
				// Current user email = email that was sent
				// Shared by value matches user who shared the reservation
				// Reservation matches post
				//
				// Delete the sharing information regardless
				delete_post_meta($items[2], '_cmd_rsvp_shared_by');
				delete_post_meta($items[2], '_cmd_rsvp_shared_to');
				
				//
				//Check RSVP to see if one is already active
				$rsvp = new RSVP();
				$rsvp->set_rsvp_id($items[3]);
				$rsvp->fetch_rsvp();
				$reservation = $rsvp->get_rsvp();
				
				//
				//Check to see if the user already has an RSVP for the team
				if( !$rsvp->get_rsvp_id ( $rsvp->set_id_by_team_user ( $reservation['team_id'] , $current_user->ID) ) )
				{
					$rsvp_table = $wpdb->prefix . 'command_RSVP';
					$rsvp_update = array("user_id" => $current_user->ID, "modified_by" => $current_user->ID);
					$rsvp_where = array("id" => $items[3], 'post_id' => $items[2]);

					$wpdb->update($rsvp_table, $rsvp_update, $rsvp_where);
				}
			}
		}
	}
}


function complete_html($complete = false)
{
	if($complete)
	{
		echo "<span class=\"completed\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>";
	}
	else
	{
		echo "<span class=\"incomplete\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>";
	}
}

//
//Setup classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "docs.class.php");
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "user.class.php");

require_once ($_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/chatterbox/classes/chatter.class.php");


$rsvp = new RSVP();
$docs = new DOCS();
$team = new TEAM();
$user = new USER();
$chatter = new CHATTERBOX();

$user_reservations = $rsvp->for_user($current_user->ID);
$rsvp_chooser = '';


//
//Check to see there is an upcoming trip, or work around selected trip
if(empty($_POST['rsvp_id']))
{
	$rsvp_id = $rsvp->next_for_user(); 
	
	if(empty($rsvp_id))
	{
		$rsvp_id = $rsvp->last_for_user();	
	}
}
else
{
	$rsvp_id = $_POST['rsvp_id'];
}

//
//Set core variables based on selected RSVP
if(!empty($rsvp_id) && !empty($user_reservations[$rsvp_id]))
{
	$team_id = $user_reservations[$rsvp_id]['team'];
	$hash = $user_reservations[$rsvp_id]['hash'];
	$days = $user_reservations[$rsvp_id]['days_until']; 
	$post_id = $user_reservations[$rsvp_id]['post'];
	$post_name = get_post($post_id)->post_name;

	$team->set_team($team_id);
	$link = get_permalink($team->value('post'));
	$share_url = site_url("team/" . $team_id);

	do_action('cmd_chat_check', $team_id, $team->value('hash'));

	if($days > 0)
	{
		$banner_message = "<i class='fa fa-plane'></i> <span class='team-hash'>" . $hash . "</span> departs in " . $days . " days!<div class='share-links'>Share your trip now: <a class='twitter' href='http://twitter.com/home?status=Join this @islonline program and make a difference! $link #islonline' title='Share this post on Twitter!' target='_blank'><i class='fa fa-twitter'></i></a>&nbsp;<a class='facebook' href='http://www.facebook.com/sharer.php?u=$link' title='Share this post on Facebook!' target='_blank'><i class='fa fa-facebook'></i></a></div>";
	} 
	else
	{
		$banner_message = "<i class='fa fa-compass'></i> Find a new Program and continue Exploring!";
	}
	
	//
	//Configure drop down
	if(count($user_reservations) > 1)
	{
		$res = array();
		
		$rsvp_chooser = '<h4 class="section-label">SELECT YOUR PROGRAM</h4>';
		$rsvp_chooser .= "<FORM name='rsvp_chooser' method='POST' action='" . $_SERVER['REQUEST_URI'] . "'>";
		$rsvp_chooser .="<SELECT id='rsvp_id' name='rsvp_id'
								 onchange='javascript:document.rsvp_chooser.submit()'>";

		foreach($user_reservations as $res_id=>$res_data)
		{
			if(!in_array($res_data['hash'], $res))
			{
				$rsvp_chooser .= "<OPTION value='" . $res_id . "' " . 
									(($res_id == $rsvp_id) ? " SELECTED" : "") . "
									>" . $res_data['hash'] . "</OPTION>";
				$res[] = $res_data['hash'];
			}
		}

		$rsvp_chooser .= "</SELECT>";
		$rsvp_chooser .= "</FORM>";

		if(empty($res) || count($res)<2)
		{
			$rsvp_chooser = '';
		}
	}
	
	$docs->post_id($team->value('post'));
	$docs->fetch(true, false);
	$docs->set_custom(true);
	$docs->fetch_read();
}
else
{
	$team_id =0;
	$post_id = 0;
	$banner_message = "<i class='fa fa-compass'></i> <a href='" . get_permalink(4411) . "' target='_parent'>Find a Program and start Exploring!</a>";
}

if(!empty(get_user_meta($current_user->ID, '_cmd_comm_ambassador_status', true)) &&
   get_user_meta($current_user->ID, '_cmd_comm_ambassador_status', true) == 'true')
{
	$ambassador = true;
}
else
{
	$ambassador = false;
}

?>

<?php get_header(); ?>
<style type="text/css" media="print">

   #masthead,
   .team-banner,
   #left-navigation,
   footer.footer {
        display: none !important;
   }

   #user,
   #content {
   	padding-top: 0px !important;
   }
</style>	
<?php
if (has_post_thumbnail()) 
{
	$thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' );
	$thumbnail_url = $thumbnail_data[0];
}
?>

<div id="user">
	<div class="team-banner">
		<h3>&nbsp; <?php echo $banner_message?></h3>
	</div>
	
	<div id="content">
		<div id="inner-content" class="row expanded collapse">
			<main id="main" class="large-12 large-centered columns" role="main">

				<div class="row expanded collapse">
					<div id="left-navigation" class="large-4 medium-12 columns">

						<header>
							<h1>
								<div class="avatar" style="background-image: url(<?php echo $user->avatar($current_user->ID); ?>);"></div><span class="name"><?php _e($current_user->first_name . " " . $current_user->last_name)?></span>
							</h1>
						</header>        

						<?php if ($rsvp_chooser): ?>
						<div id='rsvp_chooser'>
							<?php echo $rsvp_chooser; ?>
						</div>
						<?php endif ?>
				
			  			<ul id="accordion" class="accordion" data-accordion data-options="allowAllClosed: true;">
				  			<li class="accordion-item" data-accordion-item>

								<a href="#" class="accordion-title">Menu</a>

								<div class="left-nav-link-container accordion-content" data-tab-content>

									<a title="dashboard" id="dash_link" class="canvas-load-link active" data-canvas-load-key="dash">Dashboard<i class="fa fa-tachometer" aria-hidden="true"></i></a>

									<?php 
									if($team_id > 0) : 
										$statusIcon = '<span class="status"><i class="fa fa-check" aria-hidden="true"></i><i class="fa fa-times" aria-hidden="true"></i></span>';
									?>
										<a title="trip" id="trip_link" class="canvas-load-link completable <?= ((!empty($post_id) && $rsvp->profile_complete($post_id)) ? 'complete' : 'incomplete'); ?>" data-canvas-load-key="trip">Volunteer Profile <?= $statusIcon; ?></a>

										<a title="docs" id="list-info-docs_link" class="canvas-load-link completable <?= ($docs->documents_complete() ? 'complete' : 'incomplete'); ?>" data-canvas-load-key="list-info-docs">Trip Documents <?= $statusIcon; ?></a>

										<a title="passport" id="passport_link" class="canvas-load-link completable <?= ($rsvp->has_passport($rsvp_id) ? 'complete' : 'incomplete'); ?>" data-canvas-load-key="passport">Passport Info <?= $statusIcon; ?></a>

										<a title="flight" id="flight_link" class="canvas-load-link completable <?= ($rsvp->has_travel($rsvp_id) ? 'complete' : 'incomplete'); ?>" data-canvas-load-key="flight">Flight Information <?= $statusIcon; ?></a>

										<a title="itinerary" id="itinerary_link" class="canvas-load-link" data-canvas-load-key="itinerary">Itinerary<i class="fa fa-map-o" aria-hidden="true"></i></a>

										<?php 
										
										if($team->value('team_chat') == 1)
										{
											$chatter->team($team_id);
											$chatter->privacy(false);
											
											?>
											<a title="chat" id="chat_link" class="canvas-load-link" data-canvas-load-key="chat">
												Program Chat
												<i class="fa fa-comment" aria-hidden="true"></i>
												<?php if($chatter->unread() > 0) : ?>
													<span id="public_chat_notification" class="notification"><?php _e($chatter->unread())?></span>
												<?php endif; ?>
											</a>
											<?php
										}
										?>

										<?php
										if(apply_filters('chatterbox_private_chat', $current_user->ID, $team_id))
										{
											$chatter->team($team_id);
											$chatter->privacy(true);
											
											?>
											<a title="chat" id="private-chat_link" class="canvas-load-link" data-canvas-load-key="private-chat">
												Staff Chat
												<i class="fa fa-comment" aria-hidden="true"></i>
												<?php 
												if($chatter->unread()>0)
												{
													?>
													<span id="private_chat_notification" class="notification"><?php _e($chatter->unread())?></span></a>
													<?php
												}
												?>
											</a>
											<?php 
										}
										?>

										<?php
										if($team->value('no_finances') == 0)
										{
											?>
										<a title="fund" id="fundraising_link" class="canvas-load-link" data-canvas-load-key="fundraising">Fundraising Page<i class="fa fa-money" aria-hidden="true"></i></a>
											<?php
										}
										?>
										
										<a title="addon" id="add-ons_link" class="canvas-load-link" data-canvas-load-key="add-ons">Add-ons<i class="fa fa-medkit" aria-hidden="true"></i></a>

									<?php
									endif;
									?>
									<a title="account" id="user-profile_link" class="canvas-load-link" data-canvas-load-key="user-profile">My Profile<i class="fa fa-user" aria-hidden="true"></i></a>
									
									<?php
									if($ambassador)
									{
										?>
										<a title="ambassador-data" id="ambassador-data_link" class="canvas-load-link" data-canvas-load-key="ambassador-data">Ambassador<i class="fa fa-compass" aria-hidden="true"></i></a>
										<?php																				
									}
									?>
									
									<?php
									if($team_id > 0)
									{
										?>
										<a title="history" id="history_link" class="canvas-load-link" data-canvas-load-key="history">Order History<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
										
										<a title="transfer" id="transfer_link" class="canvas-load-link" data-canvas-load-key="transfer">Transfer<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
										<?php
									}
									?>
									<a title="settings" id="settings_link" class="canvas-load-link" data-canvas-load-key="settings">Contact Settings<i class="fa fa-cog" aria-hidden="true"></i></a>

									<?php 
									if($team_id > 0)
									{
										?>
										<a title="mpa-contact" id="mpa-contact_link" class="canvas-load-link" data-canvas-load-key="mpa-contact">Post Trip<i class="fa fa-user-md" aria-hidden="true"></i></a>
										<?php
									}
									?>

									<?php
                                    if ($_SERVER['REMOTE_ADDR'] == "74.93.28.29" || $_SERVER['REMOTE_ADDR'] == "74.93.28.30"){
                                        if($team_id > 0)
                                        {
                                            ?>
                                            <a title="pop" id="pop_link" class="canvas-load-link" data-canvas-load-key="pop">POP & MPA Request<i class="fa fa-user-md" aria-hidden="true"></i></a>
                                            <?php
                                        }
                                    }
									?>
								</div> <!-- End accordian content -->
							</li>
						</ul>
					</div> <!-- End left navigation -->

					<div id="myisl-body" class="large-8 medium-12 columns">
						<h2 id="sub_page_title"></h2>
						<p id='sub_page_info'></p>
						<div id="canvas-container">
							<?php $base_path = '/dash?user_id=' . $current_user->ID . '&team_id=' . $team_id . "&rsvp=" . $rsvp_id; ?>
							<iframe 
								name="myisl_canvas" 
								id="myisl_canvas" 
								src='<?php echo $base_path?>' 
								frameborder=0 
								width="100%" 
								scrolling="no" 
								onload="canvasLoaded();">
							</iframe>
							<div class="overlay"></div>
						</div> <!-- end is = canvas -->
					</div> <!-- end is = myisl-body -->
				</div> <!-- end class = row -->
			</main> <!-- end #main -->
		</div> <!-- end #inner-content -->
	</div> <!-- end #content --> 
</div><!-- end #user -->


<script language="JavaScript">

	function canvasLoaded() {
		autoResize('myisl_canvas');
		$('#canvas-container .overlay').fadeOut(200);
	}

	function autoResize(id) {
		jQuery(document).ready(function(){
			if (!id) return;
			var iFrame = $('#'+id);
			
			iFrame.css('height', 200);
			var OriginalHeight = 200;
			
			setTimeout(function(){
				setInterval(function(){
					var iFrameInner = (iFrame[0].contentWindow || iFrame[0].contentDocument);
					
					if(iFrameInner)
					{
						if(iFrameInner.document.body != null)
						{
							var iFrameInnerHeight = iFrameInner.document.body.scrollHeight;

							//
							//2016-05-06 DJT removed < calculation
							//if(iFrameInnerHeight > (OriginalHeight + 50) || iFrameInnerHeight < (OriginalHeight - 50))
							if(iFrameInnerHeight > (OriginalHeight + 50))
							{
								console.log('iFrameInnerHeight', iFrameInnerHeight);
								OriginalHeight = iFrameInnerHeight;
								iFrame.height(iFrameInnerHeight);
							}
						}
					}
				}, 1500);	
			}, 300);

		});
	};

	var last_load_key = "dash_link";

	function loadCanvas(key) {
		// This function loads the sub-page into the Canvas

		var url = '/' + key + '?user_id=' + <?php _e($current_user->ID) ?> + '&team_id=' + <?php _e($team_id)?> + '&rsvp=' + <?php _e($rsvp_id)?>;

		var canvasIframe = $('#myisl_canvas');
		$('#canvas-container .overlay').fadeIn(200);
		var banner = document.getElementById('sub_page_title');
		var subinfo = document.getElementById('sub_page_info');

		var current_load_key = key + "_link";

		$('#'+last_load_key).removeClass('active');
		$('#'+current_load_key).addClass('active');

		last_load_key = current_load_key;

		switch(key)
		{
			case "user-profile":
				banner.innerHTML = 'My Profile';
				subinfo.innerHTML = '';
				break;
			case "chat":
				banner.innerHTML = 'Program Chat';
				subinfo.innerHTML = "Use this to chat with other volunteers registered to your program! For privacy reasons we do not provide you with any other program members' information. The use of the program chat is optional; only your name will be shown if you choose to participate.";
				url += '&private=false';
				break;
			case "fundraising":
				banner.innerHTML = 'Fundraising Page';
				subinfo.innerHTML = '';
				break;
			case "private-chat":
				banner.innerHTML = 'Staff Chat';
				subinfo.innerHTML = 'Use this staff chat to contact your Volunteer Experience Coordinator directly!';
				url += '&private=true';
				break;
			case "dash":
				banner.innerHTML = '';
				subinfo.innerHTML = '';
				break;
			case "trip":
				banner.innerHTML = 'Volunteer Profile'; 
				subinfo.innerHTML = '';
				url = '/isl_rsvp/<?php _e(!empty($post_name) ? $post_name : '');?>';
				break;
			case "passport":
				banner.innerHTML = 'Passport Info';
				subinfo.innerHTML = '';
				break;
			case "flight":
				banner.innerHTML = 'Flight Information';
				subinfo.innerHTML = '';
				break;
			case "itinerary":
				banner.innerHTML = 'Itinerary';
				subinfo.innerHTML = '';
				break;
			case "add-ons":
				banner.innerHTML = 'Add-ons';
				subinfo.innerHTML = '';
				break;
			case "list-info-docs":
				banner.innerHTML = 'Trip Documents';
				subinfo.innerHTML = '';
				break;
			case "history":
				banner.innerHTML = 'Order History';
				subinfo.innerHTML = '';
				break;
			case "transfer":
				banner.innerHTML = 'Reservation Transfers';
				subinfo.innerHTML = '';
				break;
			case "settings":
				banner.innerHTML = 'Contact Settings';
				subinfo.innerHTML = '';
				break;
			case "mpa-contact":
				banner.innerHTML = '';
				subinfo.innerHTML = '';
				break;
			case "ambassador-data":
				banner.innerHTML = 'Ambassador Information';
				subinfo.innerHTML = '';
				break;
		}
	
		<?php
		if (empty($team_id)) { ?>
			if(key != 'settings' && key != 'user-profile' && key != 'ambassador-data')
			{
				url = '/dash?user_id=' + <?php _e($current_user->ID) ?> + '&team_id=' + <?php _e($team_id)?> + '&rsvp=' + <?php _e($rsvp_id)?>;
			}
			<?php
		}
		?>

		// Load the iFrame! (and give it a class so we can manipulate it if we want to.)
		canvasIframe.attr('src', url);
		canvasIframe.attr('class', '');
		canvasIframe.attr('class', key);

		if (!$('html').hasClass('no-touch')) 
		{
			$('#accordion').foundation('up', $('.accordion-content'));
		}
	}

	$(document).ready(function() {
		$('#canvas-container .overlay').fadeOut(2);

		// Setup events
		$('body').on('click', '.canvas-load-link', function(event) {
			event.preventDefault();

			loadCanvas($(this).data('canvas-load-key'));
		});
	});

	function complete_item(item_id)
	{
		jQuery('#'+item_id).first().addClass('complete').removeClass('incomplete');
		//jQuery('#'+item_id +"> i").first().addClass('fa-check').removeClass('fa-times');
	}

	function incomplete_item(item_id)
	{
		jQuery('#'+item_id).first().addClass('incomplete').removeClass('complete');
		//jQuery('#'+item_id +"> i").first().addClass('fa-times').removeClass('fa-check');
	}

</script>


<?php get_footer(); ?>