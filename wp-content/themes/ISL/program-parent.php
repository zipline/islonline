<?php
/*
Template Name: Program Parent Page
*/
?>

<?php get_header(); ?>
	
<?php get_template_part( 'short-banner' ); ?>

    <div id="content">
    
        <div id="inner-content" class="row">
    
        	<main id="main" class="row small-up-1 medium-up-2" data-equalizer data-equalize-by-row="true" roll="main">

				<?php 
					query_posts(array(
						'post_parent' 	=> 4013, 
						'post_type' 	=> 'page',
						'orderby'		=> 'title', 
						'order' 		=> 'asc',
						'post_status'	=> 'publish'
					)); 
					while ( have_posts() ) : 
						the_post();
					$teaser = get_field('program_teaser');
					$thumbnail_url = get_field('image')['sizes']['large'];
				?>

				<div class="parent column" data-equalizer-watch>
					<a href="<?php the_permalink();?>">
						<h2><?php echo the_title(); ?></h2>
						<div class="thumb" style="background-image: url('<?php echo $thumbnail_url; ?>'); background-size: cover; background-position: center center;">
							<!-- <span class="button small float-center">Explore!</span> -->
						</div>
						<p><?php echo $teaser; ?></p>
					</a>
				</div>	

				<?php
					endwhile;
					wp_reset_query();
				?>
			</main> <!-- end #main -->

        </div> <!-- end #inner-content -->

    </div> <!-- end #content --> 

<?php get_footer(); ?>