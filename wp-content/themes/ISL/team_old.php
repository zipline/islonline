<?php
/*
Template Name: Team
*/
?>

<?php

// MOVED to assets/functions/enqueue-scripts.php

//function enqueue_ajax_cart_features()
//{
	//wp_enqueue_script('jquery');

	//wp_enqueue_script('cmd_cart_loading', plugins_url() . '/command/js/cart.load.js', array( 'jquery' ));
	//wp_localize_script('cmd_cart_loading', 'ajax_path', array('ajax_url' => admin_url('admin-ajax.php')));

	//wp_enqueue_script('cmd_cart_count', plugins_url() . '/command/js/cart.count.js', array( 'jquery' ));
	//wp_localize_script('cmd_cart_count', 'ajax_path', array('ajax_url' => admin_url('admin-ajax.php')));
//}
//add_action ('wp_enqueue_scripts', 'enqueue_ajax_cart_features');


//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "cart.class.php";

//
//Gather Team, Country, Program Details
//
$team = new TEAM();
$country = new COUNTRY();
$program = new PROGRAM();

$team->set_team($team->find_team_by_name(substr($pagename, 5)));

$country_list = $country->country_list($team->schedule_countries());

//
//Build program listings with line breaks between each.
$programs = '';
foreach($team->get_schedule() as $id=>$schedule_data)
{
	$program->set_program($schedule_data['program']);
	$programs .= $program->get_name() . "<BR>";		
}
if(strlen($programs) > 3)
{
		$programs =substr($programs,0,-4);
}

?>

<?php get_header(); ?>



<div class="banner" style="background: Aquamarine;">
    
        <header class="entry-header">
            <h1><?php _e($programs)?> in <?php _e($country_list)?></h1>
            <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
        </header>        
</div>
<div id="content">
    
        <div id="inner-content" class="row">
    
            <main id="main" class="large-8 medium-8 columns" role="main">
        
            <div style="background-color:grey">
                <table style="width:100%">
                    <tr>
                        <td style="width:33%">Country: <?php _e($country_list)?></td>
                        <td style="width:33%"></td>
                        <td style="width:33%" rowspan=0>
                            <button class = "button" type="button" name="addtocart" id="addtocart">
                                    Join this Program!
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>ARRIVAL: <?php _e($team->arrival_date())?></td>
                        <td>ARRIVAL CITY: <?php _e($team->value('arrival_city'))?></td>
                        <td></td>
                    </tr>
                    <TR>
                        <TD>DEPARTURE: <?php _e($team->departure_date())?></TD>
                        <td>Departure City: <?php _e($team->value('departure_city'))?></td>
                        <td></td>
                    </TR>
                    <TR>
                        <TD>Duration: <?php _e($team->trip_duration())?></TD>
                        <TD>Cost: <?php _e($team->trip_cost())?></TD>
                        <TD></TD>
                    </TR>
                    <TR><TD colspan=2>Just x more volunteers to make this trip a go!</TD><td></td><td></td></TR>            
                </table>
            </div>
            
<?php
$cart = new CART(false);
echo 'Items: ' . $cart->count_items();
?>
            
            <p>And then we go here</p>
            
            </main> <!-- end #main -->

                <div class="medium-4 columns">
                    Itenerary Goes Here
                </div>
  

        </div> <!-- end #inner-content -->

    </div> <!-- end #content --> 

	<form name="team_add" id="team_add">
    	<input type="hidden" name="item_id" id="item_id" value="<?php _e($team->get_team())?>">
        <input type="hidden" name="item_quan" id="item_quan" value="1">
        <input type="hidden" name="add_on" id="add_on" value="0">
        <input type="hidden" name="item_type" id="item_type" value="0">
    </form>
        


<?php _e(get_footer())?>
