<?php
/*
Template Name: Checkout
*/

//
// action = checkout (initial rsvp puschase), donate (donation to an rsvp), payment (payment to RSVP)
// method = balance (cost and app fees), deposit (deposits and app fees), defined (defined amount)
//


//
//Set classes and core variables
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "user.class.php";
require_once CC_CLASSES_DIR . "cart.class.php";

$rsvp = new RSVP();
$page = basename(get_permalink());

//
//Determine why we are here (checkout and method)
if($page == 'checkout')
{
	$action = 'checkout';

	if(isset($_POST['method']))
	{
		$method = $_POST['method'];
	}
	else
	{

        if ($_SERVER['REMOTE_ADDR'] == "74.93.28.29" || $_SERVER['REMOTE_ADDR'] == "74.93.28.30"){
            print_r($_POST);
            die;
        }

        $cart = new CART();
		$cart->Count();
		
		header('Location: /cart');	
		exit;
	}
}
else 
{
	if(!empty($_REQUEST['donate']) && $_REQUEST['donate']==true)
	{
		$action = 'donate';
		$method = 'defined';
	}
	else
	{
		$action = 'payment';
		$method = 'defined';
	}
}


//
// Determine how much to pay based off of action
if($action == 'checkout')
{
	require_once CC_CLASSES_DIR . "cart.class.php";
	$cart = new CART();
	$cart->validate();

	$due_block = '';
	$cfl_remove = 0;

	//
	//Determine amount to pay based upon what's in cart and method
	if($method == "deposit")
    {
		foreach( $cart->Processed() as $main_item_id => $item_types )
		{
			foreach( $item_types as $item_type => $item )
			{
				foreach( $item as $item_id => $item_data )
				{
					switch( $item_type )
					{
						case 'reservation':
							$team = new TEAM();
							$team->set_team($item_id);

							if($team->value('no_finances') == 1)
							{
								$cfl_remove += $item_data['cost'] * $item_data['quantity'];
							}
							break;

						case 'fee':
							$team = new TEAM();
							$team->set_team($item_id);

							if($team->value('no_finances') == 1)
							{
								$cfl_remove += $item_data['cost'] * $item_data['quantity'];
							}
							break;
					}
				}
			}
		}

		$balance_due = $cart->Total('Deposit') + $cart->Total('Fee') - $cfl_remove;
	}
	else
	{
		$balance_due = $cart->Total();
	}
	
}
else
{
	require_once CC_CLASSES_DIR . "finance.class.php";
	$finance = new FINANCE();

	if(!empty($_REQUEST['donate']) && $_REQUEST['donate']==true)
	{
		$rsvp->set_rsvp_id($_REQUEST['rsvp']);
	}
	else
	{
		if ( isset( $_REQUEST['rsvp'] ) && !empty($_REQUEST['rsvp']) ) {
			$rsvp->set_rsvp_id($_REQUEST['rsvp']);
		} else {
			$rsvp->set_id_by_team_user($_REQUEST['team_id'], $_REQUEST['member_id']); //this data isn't being passed in...
		}
	}
	$rsvp_data = $rsvp->fetch_rsvp();

	$item = $finance->fetch_item($rsvp_data['order_item_id']);
	$add_ons = $finance->add_ons($rsvp_data['order_item_id']);
	$all_items[] = $rsvp_data['order_item_id'];

	$cost = $item['price'];

	foreach($add_ons as $index => $itm)
	{
		$all_items[] = $itm['id'];
		$cost += $itm['price'];
	}		

	$pays = 0;
	foreach($finance->payments($all_items) as $payments)
	{
		$pays += $payments['amount'];	
	}

	$balance_due = ($cost - $pays);
}

$balance_format = number_format($balance_due, 2);


if(is_user_logged_in())
{
	$current_user = wp_get_current_user();
	$tbday = preg_replace('/[^0-9.]+/', '', get_user_meta($current_user->ID, 'birthdate', true));	
	
	if(!empty($tbday))
	{
		$bday = substr($tbday, 0, 2) . "/" . substr($tbday, 2, 2) . "/" . substr($tbday, 4,4);
	}
	else
	{
		$bday = '';
	}
}

get_header(); ?>
<div id="checkout">
<div class="no-banner"></div> 

    <div id="content">
    
        <div id="inner-content" class="row">
				<?php

					if(isset($_REQUEST['member_id']))
					{
						$for_who = get_user_by('ID', $_REQUEST['member_id']);
						$whom = $for_who->first_name;
					}
					else
					{
						$whom = "Someone";	
					}
			
			
					switch($action)
					{
						case 'checkout':
							echo "<H2>Checkout Now</H2>";
							break;
						case 'donate':
							echo "<H2>Donate to " . $whom . "'s ISL Trip Fund</H2>";
							break;
						case 'payment':
							echo "<H2>Make a payment for " . $whom . "'s ISL Trip</H2>";
							break;
							
					}
				?>

                <main id="main" class="large-10 medium-10 small-centered columns" role="main"> 
				<?php
				if(!is_user_logged_in())
				{?>
            	<p>Already have an ISL account? <a <a data-open="login">Log in now!</a></p>
            	<?php
                }?>
                <hr />
				<!--<?php //show_cart(false);  ?>-->


				<div class='row'>
					<form name="data" id="data" method="POST" enctype="multipart/form-data" action="/receipt">
                    	<input type="submit" value="submit" visible="false" name="sub_btn" id="sub_btn" style="background: transparent; border: none !important; font-size:0; margin: 0;">
						<div class="large-6 medium-12 columns">
							<?php
							if($action == "checkout" && $cart->Count('Reservation') > 0)
							{
								?>
								<h3>Traveler's Info</h3>
                              	<?php
								echo $rsvp->payment_personal(true);
							}
							else
							{
								?>
								<!--  -->
                            	<!-- 2018-05-06 DJT changed Payee to Card Holder -->
								<!-- <h3>Payee Info</h3> -->
								<h3>Card Holder Info</h3>
								<?php
								echo $rsvp->payment_personal(true);
							}

							if($action=="donate")
							{
								?>
                                Add a note on the volunteer's public fundraising page: <br /> 
								<textarea name="note" id="note" cols="50" rows="10"></textarea>
                             	<input type="checkbox" name="anonymous" value="yes"> Click here to make this donation anonymously.
                              	<?php
							}
							
							if($action == "checkout" && $cart->Count('Reservation') > 0)
							{
								wp_enqueue_script('jquery-ui-datepicker');

								?>
                                Birthdate* <br /> 
                                <input type="text" name="birthdate" id="birthdate" class='datepicker' onchange="javascript:minorcheck()" required="true" <?php echo(!empty($bday) ? "value='$bday'" : "")?>>
                                <div name="minordisc" id="minordisc" style="display: none;">
                                    <input type="checkbox" name="minoragree" id="minoragree" value="OFF">
                                    I have read and agree to the <a data-open="minor-agree"> Under 18 disclaimer</a>
                                </div>
 
   								<script>
									jQuery(document).ready(function(){
										jQuery('.datepicker').each(function(){
		
											jQuery(this).datepicker('destroy');
		
											jQuery(this).datepicker({
												changeMonth : true,
												changeYear : true,
												yearRange: "1900:<?php echo date('Y'); ?>",
												dateFormat : 'mm/dd/yy'
											});
										});
									});
								</script>
                              	<?php
							}
							
							if($action=="checkout" || $action=="payment")
							{
								echo $rsvp->payment_address(true);
							}
							?>
						</div>
						
                        <div class="large-6 medium-12 columns">
							<?php

							if($balance_due > 0)
							{
							?>
								<h3>Billing Info</h3>
								<div class="panel teal-outline">
									<h4>Payment Info</h4>
									<?php _e($rsvp->payment_card(true)) ?>
								</div>
							<?php
							}

							//
							//Payment box
							?>
							<div class='panel dkteal'>
							<?php
								switch($action)
								{
									case "checkout":

										if($balance_due > 0)
										{
											switch($method)
											{
												case"balance":
													echo "<h3>Pay Balance in Full Now</h3><p>$" . number_format($balance_due,2) . "</p>";
													break;
												case "deposit":
													echo "<h3>Pay Deposits and Fees Now</h3><p>$" . number_format($balance_due,2) . "</p>";
													echo "<p>" . $due_block . "</p>";
													break;
												default:
													echo "<h3>Pay Now</h3>";
											}
										}
										else
										{
											echo "<h3>Finish Checkout</h3><P>$0.00 Due Today</P>";
										}
										break;

									case "donate":
										echo "<h3>Make Donation Now</h3>";
										echo "<input type='number' name='alt_pay' id='alt_pay' min='0' max='" . $balance_due . "' step='0.01' placeholder='Enter dollar amount'/>";
										break;

									case "payment":
										echo "<h3>Make a Payment Now</h3>";
										echo "<p>Balance Due: $" . $balance_format . "</p>";
										echo "<input type='number' name='alt_pay' id='alt_pay' min='0' max='" . $balance_due . "' step='0.01' placeholder='Enter dollar amount'/>";
										break;

									default:
										echo "<h3>Pay Now</h3>";
								}
								?>
                           
								<?php
								if($balance_due > 0)
								{
								?>
									<a class="button" name="pay_button" id="pay_button">Pay Now</a>
								<?php	
								}
								else
								{
								?>
									<a class="button" name="pay_button" id="pay_button">Go!</a>
								<?php	
								}
								?>
								</div>
                        </div>   
				
						<?php
                        if($action == "checkout" && $cart->count('Reservation') > 0)
                        {
                            ?>
                            <div class="large-12 medium-12 columns part_agree">
                                <input type="checkbox" name="termsagree" id="termsagree" required="true" value="OFF">
                                <p>I have read and agreed to the <a data-open="part-agree">Participation Agreement</a></p>
                            </div>
                            <?php
                        }
                        ?>
                            		
						<?php
						if($action == "donate")
						{
							?>
	                        <input type="hidden" name="donate" id="donate" value="<?php echo !empty($_REQUEST['donate']) ? $_REQUEST['donate'] : false;?>">
							<?php
						}
						?>
						<?php
						if(!empty($_REQUEST['rsvp']) && $_REQUEST['rsvp']==true)
						{
							?>
	                        <input type="hidden" name="rsvp" id="rsvp" value="<?php echo !empty($_REQUEST['rsvp']) ? $_REQUEST['rsvp'] : false;?>">
							<?php
						}
						?>
                        <input type="hidden" name="fee_count" id="fee_count" value="<?php echo !empty($_POST['fee_count']) ? $_POST['fee_count'] : 0?>">
                        <input type="hidden" name="repcode" id="repcode" value="<?php echo !empty($_POST['repcode']) ? $_POST['repcode'] : 0?>">
                        <input type="hidden" name="team_id" id="team_id" value="<?php _e(!empty($_REQUEST['team_id']) ? $_REQUEST['team_id'] : '') ?>">
                        <?php
						if($action=="checkout")
						{
							$earliest_date = "";
							if(!empty($cart->Items()))
							{
								foreach($cart->Items() as $tid => $cart_data)
								{
									if(!empty($cart_data[1]))
									{
										/** get earliest start date for the minor check here */
										global $wpdb;
										$query = "select start_date from wp_command_schedule WHERE team_id = $tid ORDER BY start_date ASC limit 1";
										$team_start = $wpdb->get_var($query);
										if(empty($earliest_date) || strtotime($team_start) < strtotime($earliest_date))
										{
											$earliest_date = $team_start;
										}

									 ?>
										<input type="hidden" name="team_ids[]" id="team_id" value="<?php echo $tid; ?>" />
									<?php
									}
								}
							}
						}
						?>
                        <input type="hidden" name="member_id" id="member_id" value="<?php _e(!empty($_REQUEST['member_id']) ? $_REQUEST['member_id'] : '') ?>"> 
                        <input type="hidden" name="method" id="method" value="<?php echo $method?>">
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>"> 
					</form>
				</div>
			</main>
		</div>
	</div>
</div>
<?php //echo var_dump($_REQUEST)?>

    <div id="login" class="reveal" data-reveal>
        <?php echo do_shortcode('[wppb-login]'); ?>
        <p class="login">New to ISL? <a href="/register">Create an account.</a></p>
        <button class="close-button" data-close aria-label="Close modal" type="button">
  			<span aria-hidden="true">&times;</span>
		</button>
    </div>


    <div id="part-agree" class="reveal" data-v-offset="200" data-reveal>
        <div><?php the_field('html_post', 4567); ?></div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
  			<span aria-hidden="true">&times;</span>
		</button>
    </div>

    <div id="minor-agree" class="reveal" data-v-offset="200" data-reveal>
        <div><?php the_field('html_post', 21494); ?></div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
  			<span aria-hidden="true">&times;</span>
		</button>
    </div>
    
<script>

	function paynow()
	{
		//
	}

	function blink()
	{
		jQuery('#pay_button').fadeOut(500).fadeIn(500);
	}

    jQuery('#termsagree').on('click', function(e)
    {
        if(jQuery(this).val() === 'ON') {
            jQuery(this).val('OFF');
        }
        else{
            jQuery(this).val('ON');
        }
    })

    jQuery('#minoragree').on('click', function(e)
    {
        if(jQuery(this).val() === 'ON') {
            jQuery(this).val('OFF');
        }
        else{
            jQuery(this).val('ON');
        }
    })

	jQuery('#pay_button').on('click', function(e)
	{
		e.preventDefault();
		
		var isValid = true;
		
		jQuery('input,textare,select').filter('[required]:visible').each(function()
		{
			if(jQuery(this).val() ==='')
				isValid = false;
            if(jQuery(this).is(":checkbox")) {
                if (jQuery(this).val() === 'OFF') {
                    isValid = false;
                }
            }
		})

		if(isValid)
		{
			var pay_button = document.getElementById('pay_button');
			pay_button.innerText = 'Processing';
			pay_button.setAttribute("disabled", '');

			setInterval (blink, 1500);

			// console.log('submit');
			jQuery('#data').submit();
		}
		else
		{
			document.getElementById('sub_btn').click();
		}
		
	})
<?php
if($action == "checkout")
{
	?>

	<?php
	if(!empty($bday))
	{
		echo "minorcheck();";	
	}
	?>
<?php error_log(print_r($earliest_date,true)); ?>

	function minorcheck()
	{
		var birthdate = document.getElementById('birthdate');
		var bday = new Date(birthdate.value);
		var today = new Date('<?php echo date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>');
		//var today = new Date('<?php echo date("Y-m-d",strtotime($earliest_date)); ?>');
		var age = Math.floor((today.getTime() - bday.getTime()) / (1000 * 60 * 60 * 24 * 365.25));

		var minoragree = document.getElementById('minoragree');
		var minordisc = document.getElementById('minordisc');

		if(age < 18)
		{
			minordisc.style.display = "inline";
			minoragree.required = true;
		}
		else
		{
			minordisc.style.display = 'none';
			minoragree.required = false;
			minoragree.checked = false;
		}
	}
	<?php
}
?>

jQuery(document).ready(function(){
		jQuery('#birthdate').each(function(){

			jQuery(this).datepicker('destroy');

			jQuery(this).datepicker({
				changeMonth : true,
				changeYear : true,
				yearRange: "1900:<?php echo date('Y'); ?>",
				dateFormat : 'mm/dd/yy',
				beforeShow: function(event, ui) {
               jQuery('#ui-datepicker-div').addClass('open');
            },
            onClose: function(event, ui) {
                jQuery('#ui-datepicker-div').removeClass('open');
            }
			});
		});
	});

</script>
<?php 

//echo var_dump($finance->payments($all_items));
//echo var_dump($finance->items_cost($all_items));

?>

<?php get_footer(); ?>