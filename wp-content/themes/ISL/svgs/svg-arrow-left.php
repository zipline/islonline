<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="31px" height="36px" viewBox="0 0 31 36" enable-background="new 0 0 31 36" xml:space="preserve">
<g>
	<path fill="#FFFFFF" fill-opacity="0.78" d="M3.827,21.005c-2.655-1.698-2.655-4.478,0-6.177L24.479,1.616c2.655-1.699,4.827-0.509,4.827,2.643v27.318
		c0,3.151-2.172,4.341-4.827,2.642L3.827,21.005z"/>
</g>
</svg>
