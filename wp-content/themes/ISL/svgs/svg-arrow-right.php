
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="31px" height="36px" viewBox="0 0 31 36" enable-background="new 0 0 31 36" xml:space="preserve">
<g>
	<path fill="#FFFFFF" fill-opacity="0.78" d="M27.479,21.005c2.655-1.698,2.655-4.478,0-6.177L6.828,1.616C4.172-0.083,2,1.106,2,4.258v27.318
		c0,3.151,2.172,4.341,4.827,2.642L27.479,21.005z"/>
</g>
</svg>
