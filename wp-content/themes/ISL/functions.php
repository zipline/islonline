<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 
require_once(get_template_directory().'/assets/functions/menu-walkers.php'); 
require_once(get_template_directory().'/assets/functions/flyout-menu.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Utility Functions
require_once(get_template_directory().'/assets/functions/utilities.php');

// Form
require_once(get_template_directory().'/assets/functions/forms.php');

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 

// Adds easy Wordpress functionality for ACF Flexible Content
require_once(get_template_directory().'/assets/functions/flexible-content.php');
//
//Command functions
require_once('functions_command.php');

// Adds site styles to the WordPress editor
require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
require_once(get_template_directory().'/assets/functions/login.php'); 

// Customize the WordPress admin
require_once(get_template_directory().'/assets/functions/admin.php'); 

// Custom URL rewriting...
function sendme_rewrite_rule() { 
    add_rewrite_rule(
        'sendme/([0-9]+)/?$',
        'sendme/?rsvp=$1',
        'bottom'
    );
}
add_action( 'init', 'sendme_rewrite_rule' );