<?php
/*
Template Name: Internship Parent
*/
?>

<?php
require_once (CC_CLASSES_DIR . 'country.class.php');

$country = new COUNTRY();

if(isset($_POST['country_filter']))
{
	$country_filter = $_POST['country_filter'];
}
else
{
	$country_filter = '0';
}

if(isset($_POST['placement_filter']))
{
	$placement_filter = $_POST['placement_filter'];
}
else
{
	$placement_filter = '0';
}

$placement_field = get_field_object('field_5969622397ecf');
$options = $placement_field['choices'];
$placement_options = '<option value=0' . (($placement_filter == 0) ? ' SELECTED' : '') . '>Pick a Placement Type</option>';

foreach($options as $index => $value)
{
	$placement_options .= '<option value=' . $index . (($placement_filter == $index) ? ' SELECTED' : '') . '>' . $value . '</option>';
}


if($placement_filter > 0 && $country_filter > 0)
{
	$filter = array (
					'relation' => 'AND',
					array(
							'key' => 'placement_type',
							'value' => $placement_filter,
							'compare' => '='
							),
					array(
							'key' => 'country',
							'value' => $country_filter,
							'compare' => '='
							)
					);
} 
elseif ($placement_filter > 0)
{
	$filter = array (
					array(
							'key' => 'placement_type',
							'value' => $placement_filter,
							'compare' => '='
							),
					);
	
} 
elseif ($country_filter > 0)
{
	$filter = array (
					array(
							'key' => 'country',
							'value' => $country_filter,
							'compare' => '='
							)
					);

}

?>


<?php get_header(); ?>

<?php get_template_part( 'banner' ); ?>

    <div id="content">
    
        <div class="row collapse">
    
            <!-- <main id="main" class="large-8 <?= ($coming ? 'medium-8' : 'large-push-2'); ?> columns" role="main"> -->
            <main id="main" class="large-10 medium-12 small-centered columns" role="main">
                <?php the_flex_content(); ?>
            </main> <!-- end #main -->

			
			<div class="large-10 medium-12 small-centered columns">
                <hr />
                <div class="row collapse">
                <div class="large-7 columns"><h3>Current Internship Opportunities</h3></div>
                <div class="large-5 columns">
       		    <form name="opts" id="opts" method="POST" enctype="multipart/form-data" action="<?php _e($_SERVER['REQUEST_URI'])?>">
        		<div id="internship-filters" class="country search-selector">
               
					<SELECT name="country_filter" id="country_filter"  onchange="javascript:document.getElementById('opts').submit()"> 
               			<?php echo $country->selector($country_filter, true, true); ?> 
               		</SELECT>
					<SELECT name="placement_filter" id="placement_filter"  onchange="javascript:document.getElementById('opts').submit()"> 
               			<?php echo $placement_options; ?> 
               		</SELECT>
				
             	</div>
				</form>
              	</div>
               	</div>
                <div class="row small-up-1 medium-up-2 large-up-3" data-equalizer data-equalize-on="medium" data-equalize-by-row="true">
			    <?php 
//                  'post_parent'   => 21391, 

					$base_query = array(
                        'post_type'     => 'isl_intern',
                        'orderby'       => 'title', 
                        'order'         => 'asc',
                        'post_status'   => 'publish'
						);
						
					if(isset($filter))
					{
						$base_query['meta_query'] = $filter;
					}

					query_posts($base_query);
					
                    while ( have_posts() ) : 
                        the_post();
                    $teaser = get_field('placement_focus');
                    $thumbnail_url = get_field('image')['sizes']['large'];
                    $country = get_field('country');
                ?>
                <div class="column">
                <div class="internship" data-equalizer-watch>
                <div class="thumb" style="background-image: url('<?php echo $thumbnail_url; ?>'); background-size: cover; background-position: center center;"></div>
                <div class="text">
                <h3><?php echo the_title(); ?> in <?php echo $country; ?></h3>
                <p><?php echo $teaser; ?></p>
                <a class="button small" href="<?php the_permalink();?>">Learn More</a>
                </div>
                </div>  
                </div>
                <?php
                    endwhile;
                    wp_reset_query();
                ?>
				</div>
			</div> <!-- End sidebar -->
			
        
        </div> <!-- end #inner-content -->
    <?php edit_post_link('edit'); ?>
    </div> <!-- end #content --> 

<?php get_footer(); ?>


