<style>
#team .itinerary-container .itinerary .day .description {
    font-size: 0.875rem;
    white-space: unset;
}
	.page .section {
    float: none !important;
}
	.layoutArea .column {
    float: none !important;
}
	#team .itinerary-container .itinerary .header, #team .itinerary-container .itinerary .footer {
		float: none !important;
	}
	.footer .column {
    float: none !important;
    padding: 0 !important;
	}
		.itinerary #team .section {
    padding: 0 !important;
}
		.itinerary span {
    font-family: "Gotham Narrow SSm A", "Gotham Narrow SSm B", sans-serif !important;
    color: #fff !important;
}
		.itinerary .page .section {
    max-width: 100%;
}
		#team .itinerary-container .itinerary .day:nth-child(odd) {
    background-color: #00aedf !important;
}
		.itinerary .day .description {
    margin-top: -60px !important;
}
	#team .section {
    padding: 0 !important;
}
</style><?php

//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "program.class.php";
require_once $plugin_path . "cart.class.php";
require_once $plugin_path . "itinerary.class.php";
require_once $plugin_path . "rsvp.class.php";

//
//Gather Team, Country, Program Details
//
$team = new TEAM();
$country = new COUNTRY();
$program = new PROGRAM();
$itinerary = new ITINERARY();
$rsvp = new RSVP();

$team->set_team($team->find_team_by_name(single_post_title('', false)));
$team_post = $team->value("post");
$team_img_post = get_post_meta($team_post, "_thumbnail_id", true);

$open = $team->value('min') - $rsvp->count_rsvp($team->value('id'));
$avail = max(0, $team->value('max') - $rsvp->count_rsvp($team->value('id')));
$sold_out = !(bool)$avail;

$program_image = '';
if(!empty($team_img_post))
{
	$source = wp_get_attachment_image_src($team_img_post, 'large');
	$program_image = $source[0];
}
else
{
	//
	//Find program image
	if(has_post_thumbnail($team->value('post')))
	{
		$source = wp_get_attachment_image_src(get_post_thumbnail_id($team->value('post')), 'large');
		$program_image = $source[0];
	}
	else
	{
		$prgms = $team->schedule_programs();
		$prgm = $program->set_program($prgms[0]);
		$pid = $program->post_id();
		$source = get_field("image", $pid);

		if($source)
		{
			$program_image = $source['url'];
		}
		else 
		{
			$cntrys = $team->schedule_countries();
			$cntry = $country->set_country($cntrys[0]);
			$cid = $country->post_id();
			$source = get_field("image", $cid);
			$program_image = $source['url'];
		}
	}
}

$countries = array();
foreach($team->get_schedule() as $data)
{
	$country->set_country($data['country']);
	$country->set_region($data['region']);
	$this_region = $country->region_details();

	if(!isset($countries[$data['country']]))
	{
		$countries[$data['country']] = $country->get_name();
	}

	if($this_region['title'] != 'ALL')
	{
		if(substr($countries[$data['country']], -3) != ' & ')
		{
			$countries[$data['country']] .= ": ";
		}
		$countries[$data['country']] .= $this_region['title'] . ' & ';
	}
}

foreach($countries as $index => $cntry_data)
{
	if(strlen($cntry_data) > 2 && substr($cntry_data,-3) == ' & ')
	{
		$countries[$index] = substr($countries[$index], 0, -3);	
	}
	if(strlen($cntry_data) > 2 && substr($cntry_data,-1) == ':')
	{
		$countries[$index] = substr($countries[$index], 0, -1);	
	}
}

//$country_list = $country->display_country_list($country->country_array($team->schedule_countries()));
$country_list = implode(' & ', $countries);

//
//Build program listings with line breaks between each.
$programs = array();
foreach($team->get_schedule() as $id=>$schedule_data)
{
	if($program->is_program($schedule_data['program']))
	{
		$program->set_program($schedule_data['program']);
		$programs[$schedule_data['program']] = $program->get_name();
	}
}
$program_list = $program->display_program_list($programs);
$program_title = $program_list . ' in ' . $country_list;
$program_hash = $team->value('hash');
$program_blurb = $team->get_blurb('teaser');

// Make sure we have good OpenGraph data...
function override_og_title(){
    global $program_title;
    global $program_hash;
    return $program_title. ' | ' . $program_hash;
}
add_filter('wpseo_title', 'override_og_title');

function override_og_description(){
    global $program_blurb;
    return $program_blurb;
}
add_filter('wpseo_metadesc', 'override_og_description');

function insert_og_image() {
    global $program_image;
    echo '<meta property="og:image" content="'.$program_image.'" />';
}
add_action('wp_head','insert_og_image');

?>

<?php get_header(); ?>

<div id="team">
    <div class="banner" style="background-image: url(<?php echo $program_image ?>);">
            <header class="entry-header">
                <h1><?php echo $program_title; ?></h1>
                <h2><?php echo $program_hash; ?></h2>

                <?php
				if(!$sold_out) : ?>
					<a class = "button joiner" type="button" name="addtocart_0" id="addtocart_0">Join this Program!</a>
				<?php
                else : ?>
					<h3 class="sold-out">SOLD OUT</h3>
				<?php
				endif;
				?>
            </header>    
            <div class="overlay"></div>    
    </div>
    <div id="content">
        <div id="inner-content" class="row">
            <?php 
            $itinerary->production(true);
            $itinerary->set_team($team->get_team());
            $itineraryDetails = $itinerary->details();

			$showingItinerary = (count($itineraryDetails) > 0) && $itinerary->show_public();
            ?>
            <main id="main" class="<?= $showingItinerary ? '' : 'medium-push-2'?> large-8 medium-8 columns" role="main">
                <h2>About this Program</h2>
                <div class="about-this-team-content">
                    <p><b>Country:</b> <?php _e($country_list)?></p>
                    <div class="row collapse">
                        <div class="medium-6 columns">
                            <p><b>Arrival Date:</b> <?php _e($team->arrival_date())?></p>
                            <p><b>Departure Date:</b> <?php _e($team->departure_date())?></p>
                            <p><b>Duration:</b> <?php _e($team->trip_duration())?> days</p>
                        </div>
                        <div class="medium-6 columns">
                            <p><b>Arrival City:</b> <?php _e($team->value('arrival_city'))?></p>
                            <p><b>Departure City:</b> <?php _e($team->value('departure_city'))?></p>
                            <?php
							if($team->value('no_finances') == 0)
							{
								?>
									<p><b>Cost:</b> $<?php _e($team->trip_cost())?></p>
                       			<?php
                       		}
                       		?>
                        </div>
                    </div>
                </div>

                <?php if (!empty($team->get_blurb('teaser'))) : ?>
                <div class="details">
                    <?php _e($team->get_blurb('teaser')); //Team Teaser goes here?><br />
                </div>
                <?php endif; ?>
                
                <?php
				if (!$sold_out) : ?>
					<a class = "button joiner" type="button" name="addtocart2_0" id="addtocart2_0">Join this Program!</a>
				<?php endif; ?>

                <div class="section first">  
                    <h3>About <?php _e($program_list)?></h3>
                 	<?php
					$prog_posts = array();
					foreach($team->get_schedule() as $id=>$schedule_data)
					{
						$program->set_program($schedule_data['program']);
						if(empty($prog_posts[$program->post_id()]))
						{	
							echo count($prog_posts) > 0 ? "<BR>" : '';
							_e(do_shortcode('[acf field="program_teaser" post_id="' . $program->post_id() . '"]'));
							echo "<a href='" . get_permalink($program->post_id()) . "' class=' small'>Learn More</a>";
							$prog_posts[ $program->post_id()] = true;
						}
					}
					?>
                </div>

                <div class="">
                    <h3>About <?php _e($country_list)?></h3>
    				<?php
    				$ctry_posts = array();
					foreach($team->schedule_countries() as $id)
    				{
						$country->set_country($id)	;
						if(empty($ctry_posts[$country->post_id()]))
						{
							echo count($ctry_posts) > 0 ? "<BR>" : '';
							_e(do_shortcode('[acf field="country_teaser" post_id="' . $country->post_id() . '"]'));
							echo "<a href='" . get_permalink($country->post_id()) . "' class=' small'>Learn More</a>";
							$ctry_posts[$country->post_id()] = true;
						}
    				}   
    				?>
                </div>
                
            </main> <!-- end #main -->

            <?php
            if ($showingItinerary) : ?>
            <div class="itinerary-container medium-4 columns">
                <h2>Itinerary</h2>
                <div>
                    <div class='itinerary'>
                        <div class='header'>
                            <div class="text">
                                <?php echo stripslashes($itinerary->header()); ?> 
                            </div>
                        </div>

                    <?php
                        if (count($itineraryDetails) > 0) : 
    					   foreach ($itineraryDetails as $id=>$data) 
							{
						?>
                        <div class='day'>
                            <h6>Day <?php echo $data['day'] . ": " . $data['title']; ?></h6>
                            <div class='description'><?php echo stripslashes($data['description']); ?></div>
                        </div>
                        <?php 
							}
                        endif;
                        ?>

        				<div class='footer'>
                            <div class="text">
                                <?php echo stripslashes($itinerary->footer()); ?>
                            </div>
                        </div>
    				</div>
                </div>
            </div>
            <?php 
            endif;

			if (!$sold_out) : ?>
				<div class="sharing-container medium-12 columns">
					<!-- <hr /> -->
					<h2>Share this Program!</h2>
					<h3>
                        <a class="twitter" href="http://twitter.com/home?status=Join this @islonline program and make a difference! <?php the_permalink(); ?>" title="Share this post on Twitter!" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share this post on Facebook!" target="_blank"><i class="fa fa-facebook"></i></a>
                    </h3>
					<a class = "button joiner" type="button" name="addtocart3_0" id="addtocart3_0">Join this Program!</a>
                </div>
            <?php endif; ?>
        </div> <!-- end #inner-content -->
    </div> <!-- end #content --> 
</div>

<form name="team_add" id="team_add">
    <input type="hidden" name="item_id" id="item_id_0" value="<?php _e($team->get_team())?>">
    <input type="hidden" name="item_quan" id="item_quan_0" value="1">
    <input type="hidden" name="item_type" id="item_type_0" value="rsvp">
    <input type="hidden" name="item_cost" id="item_cost_0" value="<?php _e($team->trip_cost())?>">
</form>
        


<?php _e(get_footer())?>