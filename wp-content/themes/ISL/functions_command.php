<?php

add_filter('wppb_send_credentials_checkbox_logic', 'wppbc_send_credentials_enabled', 10, 2);
function wppbc_send_credentials_enabled($requestdata, $form){
   return '<li class="wppb-form-field wppb-send-credentials-checkbox"><input id="send_credentials_via_email" type="hidden" name="send_credentials_via_email" value="sending"/></li>';
}

add_action('wp_head', 'stop_uploads');
function stop_uploads()
{
		$roles = get_user_meta(get_current_user_id(), 'wp_capabilities', true);

		if($roles)
		{
			$roles = array_keys($roles);
			reset ($roles);
		}
		else
		{
			$roles[] = 'volunteer';
		}
		
		if(current($roles) == "volunteer")
		{
			$usr_upload = new WP_USER(get_current_user_id());
			//$usr_upload->remove_cap('upload_files');
		}
	
}

add_action('admin_init', 'check_volunteers');
function check_volunteers()
{
		$roles = get_user_meta(get_current_user_id(), 'wp_capabilities', true);

		if($roles)
		{
			$roles = array_keys($roles);
			reset ($roles);
		}
		else
		{
			$roles[] = 'volunteer';
		}
		
		if(current($roles) == 'volunteer' && is_admin() && !(defined( 'DOING_AJAX' ) && DOING_AJAX))
		{
			wp_redirect( home_url() );	
		}
}


//
//Template for showing the lsit of countries with their associate links
//
function short_country_list()
{
	global $wpdb;
	
	$table_name = $wpdb->prefix . "posts"	;
	$list = '';
	
	$query = "SELECT post_name, SUBSTRING(post_name, LOCATE('_',post_name)+1) country
			  FROM $table_name
			  WHERE post_status = 'publish' and post_type = 'page' and post_name like 'country_%'
			  ORDER BY country";
	$results = $wpdb->get_results($query, ARRAY_A);

	//
	//Build listing using line breaks <BR> to separate each list item. 
	//Could also work easily into an ordered or unordered list 
	foreach($results as $result)
	{
		$list .= "<a href='/" . $result['post_name'] . "'>" . ucfirst($result['country']) . "</a><br>";	
	}
	$list = substr($list,0,-4);

	return $list;
}

//
//Converts the short_country_list function into a shortcode
//
//add_shortcode('list_countries_links' , 'short_country_list');


//
//Template for showing the list of programs with their associated links
//
function short_program_list()
{
	global $wpdb;
	
	$table_name = $wpdb->prefix . "posts"	;
	$list = '';
	
	$query = "SELECT post_name, SUBSTRING(post_name, LOCATE('_',post_name)+1) program
			  FROM $table_name
			  WHERE post_status = 'publish' and post_type = 'page' and post_name like 'program_%'
			  ORDER BY program";
	$results = $wpdb->get_results($query, ARRAY_A);

	//
	//Build listing using line breaks <BR> to separate each list item. 
	//Could also work easily into an ordered or unordered list 
	foreach($results as $result)
	{
		$list .= "<a href='/" . $result['post_name'] . "'>" . ucfirst($result['program']) . "</a><br>";	
	}
	$list = substr($list,0,-4);

	return $list;
}

//
//Converts the short_program_list into a shortcode
//
//add_shortcode('list_program_links' , 'short_program_list');

//
//Template for showing the cart contents
//

function show_cart( $display_only = false )
{
	include_once CC_CLASSES_DIR . "team.class.php";
	require_once CC_CLASSES_DIR . 'cart.class.php';
	
	$team = new TEAM();
	$cart = new CART();
	
	$products = $cart->Items();
	$cur_items = array();

	if(isset($products) && count($products) > 0)
	{
		foreach($products as $type => $whats)
		{
			foreach($whats as $item => $data)
			{
				$cur_items[$item] = $data;	
			}
		}
		
	}
	
	//
	//If we have items in our cart, then display them, else say so
	if(count($cur_items))
	{
		$subtotal =0;
		
		echo "<form name='data' method='POST' enctype='multipart/form-data' action='../checkout'>";

		echo "<table>";
		echo "<TR><TD>Item</TD><TD>Quantity</TD><TD>Cost</TD>";
		
		foreach($cur_items as $item_id=>$data)
		{
			$team->set_team($item_id);
			$line_total = $data[1] * $data[2];
			echo"<TR><TD>" . $team->value('hash') . "</TD><TD>" . $data[2] . "</TD><TD>$" . number_format($line_total,2) . "</TD></TR>";
			$subtotal += $line_total;
		}

		echo "<TR><TD Colspan=2><B>Total</B><TD>$" . number_format($subtotal,2) . "</TD></TR>";
		if($display_only == 'false')
		{
			echo "<TR><TD Colspan=3 align=\"right\"><button class=\"button\" type=\"submit\" name=\"purchase\">Buy Now</button></TD></TR>";
		}
		echo "</table>";
		echo "</form>";
	}
	else
	{
		echo "There are no teams in your cart. Let's find one!<br />";
		echo "<a href='", get_permalink(4411), "' class='button'>Find a Program</a>";
	}
}