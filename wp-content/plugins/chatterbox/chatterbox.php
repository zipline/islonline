<?php
/**
 * @package Chatterbox
 * @Version 0.1
 *
*/

/*
 Plugin Name: Chatterbox
 Plugin URI: N/A
 Description: Chat plugin
 Author: Illumatech - David Thompson
 Version: 0.1
 Author URI: N/A
*/

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Make sure wp-includes/pluggable.php is included
if(!function_exists('wp_get_current_user')) { include(ABSPATH . 'wp-includes/pluggable.php'); }

//
//Constant Definitions
define ('CB_VERSION', '0.1');
define ('CB_PLUGIN_URL', plugin_dir_url(__FILE__));
define ('CB_PLUGIN_DIR', plugin_dir_path(__FILE__));
define ('CB_ADMIN_DIR', CB_PLUGIN_DIR . 'admin/');
define ('CB_CSS_DIR', CB_PLUGIN_URL . 'css/');
define ('CB_CLASSES_DIR', CB_PLUGIN_DIR . 'classes/');
define ('CB_INCLUDES_DIR', CB_PLUGIN_DIR . 'includes/');
define ('CB_JS_DIR', CB_PLUGIN_URL . 'js/');



//
//Run proper activations
register_activation_hook ( __FILE__, 'chatterbox_class_activate' );


//
//Function to call the install scripting to build/load necessary data structures
function chatterbox_class_activate()
{
	include ( CB_ADMIN_DIR . 'install.php' );
	$install = new CHATTERBOX_INSTALL();	
}

//
//Load all Shortcodes
foreach( glob(CB_INCLUDES_DIR . "shortcodes/*.php") as $filename)
{
	include $filename;
}

//
//Load all Callbacks
foreach( glob(CB_INCLUDES_DIR . "callbacks/*.php") as $filename)
{
	include $filename;
}

?>