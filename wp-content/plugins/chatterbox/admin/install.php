<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

Class CHATTERBOX_INSTALL
{

	function __construct()
	{
		self::index_create();
		self::team_create();
		self::team_users_create();
		self::message_create();
	}

	function index_create()
	{
		global $wpdb;
		
		//
		//Create INDEX table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "chatterbox_index
				(
				 id int(11) NOT NULL AUTO_INCREMENT,
				 created datetime DEFAULT CURRENT_TIMESTAMP,
				 created_by int(11) NOT NULL,
				 PRIMARY KEY (id)
				)"; 
		self::execute_query($wpdb, $query);
	}
	
	function team_create()
	{
		global $wpdb;

		//
		//Create TEAM table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "chatterbox_teams
				(
				 id varchar(25) NOT NULL,
				 title varchar(100) NOT NULL,
				 created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				 created_by int(11) NOT NULL,
				 PRIMARY KEY (id),
				 UNIQUE title (title)
				)"; 
		self::execute_query($wpdb, $query);
	}

	function team_users_create()
	{
		global $wpdb;
		
		//
		//Create TEAM USERS table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "chatterbox_users
				(
				 team_id varchar(25) NOT NULL,
				 user_id int(11) NOT NULL,
				 created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				 created_by int(11) NOT NULL,
				 PRIMARY KEY (team_id, user_id)
				)"; 
		self::execute_query($wpdb, $query);
	}

	function message_create()
	{
		global $wpdb;
		
		//
		//Create MESSAGE table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "chatterbox_messages
				(
				 id int(11) NOT NULL auto_increment,
				 team_id varchar(25) NOT NULL,
				 user_id int(11) NOT NULL,
				 message text NOT NULL,
				 visible tinyint NOT NULL DEFAULT 1,
				 created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				 PRIMARY KEY (id),
				 INDEX team_id (team_id)
				)"; 
		self::execute_query($wpdb, $query);
	}


	function execute_query($db, $query)
	{
		if(substr($query,6) == "CREATE")
		{
			$db->query($query) || die (trigger_error($query . ':' . $db->last_error, E_USER_ERROR));			 
		} 
		else 
		{
			$affect = $db->query($query);
			
			if(!$affect && isset($db->last_error) && $db->last_error != '')
			{
				trigger_error("((" . $query . ")) : " . $db->last_error, E_USER_ERROR);
			}
		}
	}

}

?>