<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * CHATTERBOX Class: Contains the user-called functions for team management
 */
CLASS CHATTERBOX
{
	/** @var double $team: The Primary team. */
	private $team = NULL;
	
	/** @var double $team: The Primary team. */
	private $team_name = NULL;

	/** @var boolean $private: Determines if chat should be private or team. */
	private $private = 'false';

	/** @var integer $user: The id of the user */
	private $user = NULL;
	
	/** @var integer $user: The id of the user */
	private $real_user = NULL;

	/** @var integer Last message id */
	private $last_id = 0;
	private $last_id_original = NULL;
	
	/** @var string $messages_table: The wordpress message table name */
	private $messages_table = NULL;
	/** @var string $index_table: The wordpress index table name */
	private $index_table = NULL;
	/** @var string $teams_table: The wordpress team table name */
	private $teams_table = NULL;
	/** @var string $users_table: The wordpress user table name */
	private $users_table = NULL;
	/** @var string $users_table: The wordpress user table name */
	private $wp_users_table = NULL;
	
	/* @var string $last_index = user_meta label for last read message for the given team. */
	private $last_index = "_chatterbox_last_index_for_";
	

	public function __construct($cookie = false)
	{
		global $wpdb;
		
		$this->messages_table = $wpdb->prefix . 'chatterbox_messages';
		$this->index_table = $wpdb->prefix . 'chatterbox_index';
		$this->teams_table = $wpdb->prefix . 'chatterbox_teams';
		$this->users_table = $wpdb->prefix . 'chatterbox_users';
		$this->wp_users_table = $wpdb->prefix . 'users';
		
		$this->set_user(get_current_user_id());
		$this->real_user = get_current_user_id();
	}

	public function get_messages_table() {
		return $this->messages_table;
	}

	public function get_last_id() {
		return $this->last_id;
	}

	/**
	 * set the current user
	 *
	 * @param integer $uid Id of the user
	 */
	public function set_user($uid)
	{
		if($uid==0 && has_filter('chatterbox_user_check'))
		{
			$uid = apply_filters('chatterbox_user_check', $this->real_user);
		}

		$this->user = $uid;
	}


	/**
	 * Sets or Returns the privacy setting
	 *
	 * @param boolean $private (optional). If set, then sets the privacy
	 *
	 * @return boolean (conditional): The current privacy setting if @param was not passed.
	 */
	public function privacy($private = false)
	{

		if(func_num_args() > 0)
		{
			$this->private = $private;
		}
		else
		{
			return $this->private;
		}
	}

	/**
	 * Sets or Returns the base team id
	 *
	 * @param boolean $team_id (optional). If set, then sets the team_id
	 *
	 * @return integer (conditional): The current team id if @param was not passed.
	 */
	public function team($team_id = 0)
	{

		if(func_num_args() > 0)
		{
			$this->team = $team_id;
		}
		else
		{
			return $this->team;
		}
	}
	
	/**
	 * Sets or Returns the base team id
	 *
	 * @param boolean $team_id (optional). If set, then sets the team_id
	 *
	 * @return integer (conditional): The current team id if @param was not passed.
	 */
	public function team_title($team_name = '')
	{

		if(func_num_args() > 0)
		{
			$this->team_name = $team_name;
		}
		else
		{
			return $this->team_name;
		}
	}
	
	/**
	 * Sets or Returns the last message id
	 *
	 * @param boolean $message_id (optional). If set, then sets the message_id
	 *
	 * @return integer (conditional): The current last message id if @param was not passed.
	 */
	public function last_id($last_id = 0)
	{

		if(func_num_args() > 0)
		{
			$this->last_id = $last_id;
		}
		else
		{
			return $this->last_id;
		}
	}

	public function fetch_last_read($member_id = 0)
	{
		global $wpdb;
		
		$query = "SELECT last_read FROM " . $this->users_table . " WHERE user_id=" . $member_id . " AND team_id='" . self::team_id() . "'";
		$last_read = $wpdb->get_var($query);	

		return $last_read;
	}

	public function update_last_read($member_id = 0)
	{
		global $wpdb;

		$user_id = apply_filters('chatterbox_last_read_user', $member_id);
		//$user_id = $member_id;
		//echo "l" . self::last_id();
		//echo "t" . self::team_id();
		//echo "u" . $user_id;
		$wpdb->update($this->users_table, array('last_read' => self::last_id()), array('user_id' => $user_id, 'team_id' => self::team_id()));

	}
	
	/**
	 *Writes the message into the message table
	*/
	public function send_message($message)
	{
		$validated = self::validate(self::team(), $this->user, $this->private);
		
		if($validated)
		{
			global $wpdb;
			$fixed_message = str_replace('%0A', '<BR \>', urlencode($message));
			$data = array('team_id' => self::team_id(), 'user_id' => $this->real_user, 'message' => $fixed_message);
			$message_id = $wpdb->insert($this->messages_table, $data);

			self::last_id($message_id);
			self::update_last_read($this->real_user);
		}
		else
		{
			global $wpdb;
			$data = array('team_id' => self::team_id(), 'user_id' => $this->real_user, 'message' => self::team_id() . " " . self::team());
			$message_id = $wpdb->insert($this->messages_table, $data);
		}
	}

	public function get_messages()
	{
		//
		//If the user is valid to see this data then run the data
		if(self::validate(self::team(), $this->user, $this->private) || 1)
		{
		
			if(empty($this->last_id_original))
			{
				$this->last_id_original = self::fetch_last_read($this->real_user);	
				$mark_last_read = true;
			}

			$final = 'A';
			$records = 0;

			$ret_val =  "<?xml version='1.0' encoding='utf-8' ?>";
			$ret_val .= "<return_data>";
			$ret_val .= "<team_id>" . self::team_id() . "</team_id>";
			$ret_val .= "<user_id>" . $this->user . "</user_id>";
			//$ret_val .= "<team>" . self::team() . "</team>";
			//$ret_val .= "<privacy>" . self::privacy() . "</privacy>";
			//$ret_val .= "<last_read>" . $this->last_id_original . "</last_read>";

			if(!empty(self::team_id()))
			{
				global $wpdb;
				$query = "SELECT SQL_NO_CACHE u.display_name, m.id, m.message, m.user_id, m.created 
						  FROM " . $this->messages_table . " m INNER JOIN " . $this->wp_users_table . " u
								ON m.user_id = u.ID
						  WHERE m.team_id = '" . self::team_id() . "'" .
						  ((self::last_id() > 0) ? " AND m.id >= " . self::last_id() : "") . "
						  AND m.visible = 1
						  ORDER BY m.id DESC";		 	
				$results = $wpdb->get_results($query, ARRAY_A);

				$ret_val .= "<chatlog>";

				if(!empty($results))
				{
					foreach($results as $id=>$data)
					{
						if($data['id'] != self::last_id())
						{
							$when = date("M d, Y - h:i:s", strtotime($data['created']));

							//$ret_val .= " last_id: " . $this->last_id_original;
							//$ret_val .= " data_id: " . $data['id'];
							$origination = (($this->last_id_original == $data['id']) ? true : false);
							if($mark_last_read && $origination && self::last_id() == 0)
							{
								$mark_last_read = false;
								$ind_last_read = ' cb_last_read';
							}
							else
							{
								$ind_last_read = '';	
							}

							if($data['user_id'] == get_current_user_id())
							{
								$ret_val .=  "<div data-m-id='" . $data['id'] . "' class='message_line'>";
								$ret_val .=  "   <div class='message-container user'>";
								$ret_val .=  "       <div class='cb_message_user_name'>Me</div>";
								$ret_val .=  "       <div class='cb_message_text" . $ind_last_read . "'>" . stripslashes(urldecode($data['message']))."</div>";

								$ret_val .=  "       <div class='cb_message_datestamp'>" . $when;
								if (user_can($this->real_user, 'super') || user_can($this->real_user, 'admin')) {
									$ret_val .= "   <span style='margin-left: 5px'>";
									$ret_val .= "      <a href='#' data-m-id='" . $data['id'] . "' class='delete_line' style='text-decoration:none;'>Delete</a>";
									$ret_val .= "   </span>";
								}
								$ret_val .=  "       </div>";
								$ret_val .=  "   </div>";

								$ret_val .=  "   <div class='cb_user_data avatar-container'><div class='avatar' style='background-image:url(" . self::avatar(get_current_user_id()) . ")'></div></div>";
								$ret_val .=  "</div>";
							}
							else
							{
								$ret_val .=  "<div data-m-id='" . $data['id'] . "' class='message_line'>";
								$ret_val .=  "   <div class='cb_user_other avatar-container'><div class='avatar' style='background-image:url(" . self::avatar($data['user_id']) . ")'></div></div>";
								$ret_val .=  "   <div class='message-container'>";
								$ret_val .=  "       <div class='cb_message_user_name'>" . $data['display_name'] . "</div>";
								$ret_val .=  "       <div class='cb_message_text" . $ind_last_read . "'>" . stripslashes(urldecode($data['message'])) . "</div>";

								$ret_val .=  "       <div class='cb_message_datestamp'>" . $when;
								if (user_can($this->real_user, 'super') || user_can($this->real_user, 'admin')) {
									$ret_val .= "   <span style='margin-left: 5px'>";
									$ret_val .= "      <a href='#' data-m-id='" . $data['id'] . "' class='delete_line' style='text-decoration:none;'>Delete</a>";
									$ret_val .= "   </span>";
								}
								$ret_val .=  "       </div>";
								$ret_val .=  "   </div>";

								$ret_val .=  "</div>";
							}

							//
							//Grab the first record since we now display backwards.
							if($final=='A')
							{
								$final = $data['id'];
							}
							$records++;
						}
						else
						{
							if(count($results)>1)
							{	
								'';
							}
							else
							{
								$final = self::last_id();
								break;
							}
						}
					}
				}

				$ret_val .= "</chatlog>";
				$ret_val .= "<query>" . $query . "</query>";

			}

			$ret_val .= "<final>" . $final . "</final>";
			$ret_val .= "<records>" . $records . "</records>";
			$ret_val .= "</return_data>";

			self::last_id($final);
			self::update_last_read($this->real_user);

			return $ret_val;
		}
		else
		{
			$ret_val =  "<?xml version='1.0' encoding='utf-8' ?>";
			$ret_val .= "<return_data>";
			$ret_val .= "<team_id>" . self::team_id() . "</team_id>";
			$ret_val .= "<user_id>" . $this->user . "</user_id>";
			$ret_val .= "<chatlog>";
			//$ret_val .= "Permission Denied";
			$ret_val .= "</chatlog>";

			return $ret_val; 		
		}
	}
	
	public function validate($team_id = 0, $user_id = 0, $private = true)
	{
		global $wpdb;
		$valid = false;
		
		//
		//Grab all current data so we can swap it around
		$tmp_team = self::team();
		$tmp_private = self::privacy();
		$tmp_user = $this->user;
		
		//
		//Change everything to our passed variables
		self::team($team_id);
		self::set_user($user_id);
		self::privacy($private);
		
		//
		//Check the user and team combination
		$query = "SELECT created FROM " . $this->users_table . " WHERE team_id='" . self::team_id() . "' AND user_id=" . $this->user;
		return $user_id . " " . $this->user;

		$check = $wpdb->get_var($query);

		if(!empty($check))
		{
			$valid = true;
		}
		

		//
		//Change everything back to original
		self::team($tmp_team);
		self::set_user($tmp_user);
		self::privacy($tmp_private);

		return apply_filters('chatterbox_validate', $valid);
	}
	
	public function avatar($user_id)
	{
		return apply_filters('chatterbox_avatar', $user_id);
	}
	
	/**
	 * Selects a team based upon the team_name
	 *
	 * @param string $team_name. The name to check for
	 *
	 * @return boolean: Did the selection find something
	 */
	public function select_team($team_name = '')
	{
		if(!empty($team_name))
		{
			global $wpdb;

			$query = "SELECT id FROM " . $this->teams_table . " WHERE title='" . $team_name . "'";
			$temp_team = $wpdb->get_var($query);
			
			if(!empty($temp_team))
			{
				self::team($temp_team);
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Selects a list of teams based upon the team_name
	 *
	 * @param string $team_name. The name to check for
	 *
	 * @return array: List of team id's associated with an ISL team OR false if there are none
	 */
	public function select_teams($team_name = '')
	{
		if(!empty($team_name))
		{
			global $wpdb;

			$temp_teams = $wpdb->get_col(
				$wpdb->prepare("SELECT id FROM $this->teams_table WHERE title LIKE '%s'", $team_name.'%')
			);
			
			if(!empty($temp_teams))
			{
				return $temp_teams;
			}
		}
		
		return false;
	}
	
	/**
	 * Inserts a new team into the database
	 *
	 * @param string $team_name. The name to apply to the created team
	 * @param boolean $base_team. Determine if buildling a new open team, get index value
	 *
	 * @return boolean: Did the insertion complete properly
	 */
	public function insert_team()
	{
		if(!empty($this->team_name) && !$this->team_exists(self::team_id()))
		{
			global $wpdb;

			$data = array('id' => self::team_id(),
						  'title' => self::team_name(),
						  'created_by' => get_current_user_id()
						 );
			if($wpdb->insert($this->teams_table, $data))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Updates a team with a new title
	 *
	 * @param string $team_name. The name to apply to the team
	 *
	 * @return boolean: Did the insertion complete properly
	 */
	public function update_team()
	{
		if(!empty($team_name))
		{
			if(!empty(self::team_id()))
			{
				global $wpdb;

				if($wpdb->update($this->teams_table, array('title' => self::team_name()), array('id' => self::team_id())))
				{
					return true;
				}
			}

		}
		return false;
	}

	/**
	 * Checks if the team chat exists already
	 *
	 * @param string $team_id: the ID of the team to be checked
	 *
	 * @return boolean: true if the team exists, false if it doesn't exist
	 */
	public function team_exists($team_id)
	{
		global $wpdb;
		$query="SELECT id FROM $this->teams_table WHERE id = '" . self::team_id() . "' LIMIT 1";
		$result = $wpdb->get_row($query);

		if(empty($result))
		{
			return false;
		}
		return true;

	}

	/**
	 * Inserts a new user onto a team
	 *
	 * @param integer $user_id. The user_id to insert onto the team
	 *
	 * @return boolean: Did the insertion complete properly
	 */
	public function insert_user($user_id = 0)
	{
		$success = false;

		if(isset($user_id) || func_num_args() > 0)
		{
			if(!empty(self::team_id()) && !self::check_user_exists($user_id, self::team_id()))
			{
				global $wpdb;

				$data = array('team_id' => self::team_id(),
							  'user_id' => $user_id,
							  'created_by' => $this->real_user
							 );
				if($wpdb->insert($this->users_table, $data))
				{
					$success = true;
				}
			}
		}

		return $success;
	}

	/**
	 * Removes the user from the chats
	 *
	 * @param integer $user_id The ID of the user
	 * @param integer $team_id The ID of the main team
	 *
	 */
	public function remove_user($user_id)
	{
		$success = false;
		global $wpdb;
		$cur_privacy = self::privacy();

		self::privacy(false);
		$wpdb->delete($this->users_table, array('user_id' => $user_id, 'team_id' => self::team_id()),array("%d","%s"));

		self::privacy(true);
		$wpdb->delete($this->users_table, array('user_id' => $user_id, 'team_id' => self::team_id()),array("%d","%s"));

		self::privacy($cur_privacy);
		return $success;
	}

	/**
	 * Determine how many users exist for the fiven team
	 *
	 * @return integer the number of users
	 */
	public function count_users()
	{
		global $wpdb;

		$query = "SELECT count(*) FROM " . $this->users_table . "
				  WHERE team_id = '" . self::team_id() . "'";
		$count = max(0, $wpdb->get_var($query));
		
		return $count;		
	}

	/**
	 * Set last read value or return last read value
	 *
	 * @param integer $index_value the value to set as "last read"
	 *
	 * @return integer the last read value
	 */
	public function last_read($index_value = 0)
	{

		if(func_num_args() > 0)
		{
			update_user_meta($this->user,  $this->last_index . self::team_id(), $index_value);
		}
		else
		{
			return get_user_meta($this->user, $this->last_index . self::team_id(), true);
		}
					
	}

	/**
	 * Determine how many messages for the given team are not yet read
	 *
	 * @return integer the number of unread messages
	 */
	public function unread()
	{
		global $wpdb;

		$query = "SELECT count(*) FROM " . $this->messages_table . "
				  WHERE team_id = '" . self::team_id() . "' AND
				  		user_id != " . $this->user . " AND
						id > " . max(0, self::fetch_last_read($this->user));
		$count = max(0, $wpdb->get_var($query));
		return $count;		
	}

	/**
	 * Determine how many messages for the user are not read yet
	 *
	 * @return array the number of unread messages in team => name, count, private
	 */
	public function unread_by_user()
	{
		global $wpdb;
		$unread = array();
		
//		$query = "SELECT cu.team_id, cu.user_id, cu.last_read, ct.title, count(cm.id) unread, max(cm.id) last_message
//					FROM " . $this->users_table . " cu
//						INNER JOIN " . $this->teams_table . " ct ON cu.team_id = ct.id
//						INNER JOIN " . $this->messages_table . " cm ON cu.team_id = cm.team_id
//					WHERE
//						cu.user_id = " . $this->user . "
//					GROUP BY
//						cu.team_id, cu.user_id, cu.last_read, ct.title
//					HAVING
//						cu.last_read < last_message
//					ORDER BY
//						unread DESC";
		$query = "SELECT cu.team_id, cu.user_id, cu.last_read, ct.title, sum(case when cm.id >= cu.last_read then 1 else 0 end) unread, max(cm.id) last_message
					FROM " . $this->users_table . " cu
						INNER JOIN " . $this->teams_table . " ct ON cu.team_id = ct.id
						INNER JOIN " . $this->messages_table . " cm ON cu.team_id = cm.team_id
					WHERE
						cu.user_id = " . $this->user . "
					GROUP BY
						cu.team_id, cu.user_id, cu.last_read, ct.title
					HAVING
						cu.last_read < last_message
					ORDER BY
						unread DESC";
//		echo $query;die;
		$base_listing = $wpdb->get_results($query, 'OBJECT');
		//return array('query'=>$query);
	
		if(!empty($base_listing))
		{
			foreach($base_listing as $row)
			{
				//
				//First, check to see if we're looking at Admin, and if so, remove any chats that have been 
				if( $this->user == 0 )
				{
					//
					//If we have a group chat, then just update it and move on 
					if( strpos( $row->team_id, '.' ) == 0 )
					{
						//
						//Get old values
						$old_team = self::team();
						$old_privacy = self::privacy();
						
						//
						//Update
						self::last_id( $row->last_message );
						self::team( $row->team_id );
						self::privacy( false );
						
						self::update_last_read(0);
						
						//
						//Restore values
						self::team( $old_team );
						self::privacy( $old_privacy );
						
						$valid = false;
					}
					else
					{
						//
						//Since we're not group chat, check to see if another admin has caught the thread up
						$priv_user = substr( $row->team_id, strrpos( $row->team_id, '.' ) + 1 );
						
						$last_admin_message_read = $wpdb->get_var( "SELECT max(last_read) FROM " . $this->users_table . " WHERE team_id = '" . $row->team_id . "' AND user_id != " . $priv_user );
						
						//
						//Update the last message for the admin
						if( isset( $last_admin_message_read ) )
						{
							$wpdb->query( "UPDATE "  . $this->users_table . " SET last_read=" . $last_admin_message_read . " WHERE team_id='" . $row->team_id . "' AND user_id!=" . $priv_user . "");
						}
						
						//
						//Check to see if the last updated is the last message...
						$last_message = $wpdb->get_var( "SELECT max(id) FROM " . $this->messages_table . " WHERE team_id = '" . $row->team_id . "'" );
						
						//
						//If both "last messages" are the same, then we don't need to add this one, otherwise we do.
						if($last_admin_message_read == $last_message )
						{
							$valid = false;
						}
						else
						{
							$valid = true;
						}
					}
				}
				else
				{
					$valid = true;
				}
				
				if( $valid )
				{
					$unread[$row->team_id] = array('team_id' => $row->team_id, 
												   'user_id' => $row->user_id, 
												   'title' => $row->title, 
												   'last_read' => $row->last_read, 
												   'last_message' => $row->last_message,
												   'unread' => $row->unread,
												   'private' => false);

					//
					//check for private
					$check_one = strrpos($row->team_id, '.') ;
					$check_two = strrpos($row->title, '-', strrpos($row->title, '-') -1);

					$unread[$row->team_id]['check_one'] = $check_one;
					$unread[$row->team_id]['check_two'] = $check_two;

					if($check_one > 0 && $check_two > 0)
					{
						$check_one_val = substr($row->team_id, $check_one + 1);
						$check_two_val = substr($row->title, strrpos($row->title, '-') + 1);

						$unread[$row->team_id]['check_one_val'] = $check_one_val;
						$unread[$row->team_id]['check_two_val'] = $check_two_val;

						$check_three = ($check_one_val == $check_two_val);

						if($check_three)
						{
							$user = get_userdata($check_one_val);

							$unread[$row->team_id]['private'] = true;
							$unread[$row->team_id]['team_id'] = substr($row->team_id, 0, $check_one);
							$unread[$row->team_id]['title'] = substr($row->title, 0, $check_two-8) . ' (' . $user->display_name . ')';
						}
					}
				}
			}
		}
		
		return $unread;		
	}


	/**
	 * Returns the modified team id based on the privacy options
	 *
	 * @return string: The team_id that should be used for inserts and lookups
	 */
	private function team_id()
	{
		$base_id = self::team();

		if(!empty($base_id))
		{
			if(self::privacy() == 'true')
			{
				$decimals = strlen(number_format($this->user));
				$divisor = pow(10,$decimals);
				$base_id = number_format($base_id + ($this->user / $divisor), $decimals);
			}
		}
		return $base_id;
	}

	/**
	 * Returns the modified team id based on the privacy options
	 *
	 * @return string: The team_id that should be used for inserts and lookups
	 */
	private function team_name()
	{
		$base_id = self::team_title();

		if(!empty($base_id))
		{
			if(self::privacy() == 'true')
			{
				$base_id .=  '-private-' . $this->user;
			}
		}
		return $base_id;
	}

	/**
	 * Checks if the user is already in the users table
	 *
	 * @return boolean: true if the user doesn't exist OR false if the user does exist
	 */
	private function check_user_exists( $user_id, $team_id )
	{
		$exists = false;

		if(is_numeric($user_id) && !empty($team_id))
		{
			global $wpdb;

			$query = "SELECT user_id, team_id FROM $this->users_table WHERE user_id = " . $user_id . " AND team_id = '" . $team_id . "'";
			$check_user = @$wpdb->get_var($query);

			if(!is_null($check_user))
			{
				if((float)$check_user === (float)$user_id)
				{
					$exists = true;
				}

			}
		}
		return $exists;
	}


	/**
	 * Checks if the volunteer exists by way of their private chat
	 *
	 * @return array list of team ids for the user as a volunteer (where they have a private chat)
	 */
	private function get_staff_volunteer_teams($staff_id)
	{

		global $wpdb;

		$user_list = $wpdb->get_results(
			"SELECT user_id, team_id FROM $this->users_table WHERE team_id like '%.". $staff_id ."'"
		);

		$team_list = array();
		if($user_list)
		{
			foreach($user_list as $user)
			{
				$team_id = explode('.',$user->team_id);
				$team_list[] = $user->team_id;
				$team_list[] = $team_id[1];
			}
		}
		return $team_list;
	}
}
