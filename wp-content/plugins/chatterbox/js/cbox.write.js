
jQuery(document).ready(function()
{
	
	var online = navigator.onLine;

	if(online)
	{

		jQuery('#add_text').on('click', function()
		{
			var text = jQuery('#data_input').val();

			var data = 
			{
				'action': 'cbox_write',
				'message': text,
				'team_id': svars_write.team_id,
				'user_id': svars_write.user_id,
				'privacy': svars_write.private,
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: svars_write.ajax,
				data: data,
				success:function(data)
				{
					//console.log(data);
					jQuery('#data_input').val('');
					jQuery('#data_input').focus();
					//var last_input = (data-1);
					//jQuery('#last_read').val(last_input);

				},
				error:function(xml, status,error)
				{
					console.log('Error: ' + error);
				}
			});			
		});
	}
});

function delete_line(element) {

	var online = navigator.onLine;
	
	if(online && !cbox_running)
	{
		var deleting_message = jQuery(element).data('m-id');
		var previous_message = jQuery(element).parents('.message_line').prev().find('.delete_line').data('m-id');
		var last_read = jQuery('#last_read').val();

		if (deleting_message == last_read) {
			// Change last read to previous message
			jQuery('#last_read').val(previous_message);
			last_read = previous_message;
			// console.log('changing: ' + deleting_message + ' to: ' + previous_message);
		}
		
		// console.log(last_read);

		var data =
			{
				'action': 'cbox_write',
				'm_id': jQuery(element).data('m-id'),
				'last_id': last_read,
			};

		jQuery.ajax(
			{
				type: 'POST',
				url: svars_write.ajax,
				data: data,
				success:function(data)
				{
					// console.log(data);
					jQuery(element).parents('.message_line').fadeOut(300, function() {
						jQuery(this).remove();
					});

					// jQuery('.message_line').remove();
					// jQuery('#data_input').val('');
					// jQuery('#data_input').focus();
					//var last_input = (data-1);
					//jQuery('#last_read').val(last_input);

				},
				error:function(xml, status,error)
				{
					console.log('Error: ' + error);
				}
			});
	} else {
		setTimeout(function() {
			delete_line(element);
		}, 500);
	}
}