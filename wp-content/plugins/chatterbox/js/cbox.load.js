jQuery(document).ready(function()
{
	var online = navigator.onLine;

	if (online) {
		var text = jQuery('#data_input').val();

		jQuery('#data_input').val('');
		jQuery('#data_input').focus();
	}

    if (document.getElementById('popout-trigger') && window.self == window.top) {
        jQuery('#popout-trigger').css('display', 'none');
        jQuery('.chatterbox__container').removeClass('has-popout');
    } else {
        jQuery('.chatterbox__container').addClass('has-popout');
    }
});
