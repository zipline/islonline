var cbox_running = false;

jQuery(document).ready(function($)
{

	setInterval(function()
	{

		var online = navigator.onLine;
		
		if(online)
		{

			if(!cbox_running)
			{
				cbox_running = true;

				var text = jQuery('#data_input').val();
				var last = jQuery('#last_read').val();
				//alert(last);
				// console.log("before response: " + last);

				var data = 
				{
					'action': 'cbox_read',
					'team_id': svars_read.team_id,
					'user_id': svars_read.user_id,
					'privacy': svars_read.private,
					'last_id': last
				};

				jQuery.ajax(
				{
					type: 'POST',
					url: svars_read.ajax,
					data: data,
					success:function(xml)
					{
						// console.log('AJAX SUCCESS', xml);
						var chat_holder = jQuery('.chatterbox_wrapper');

						var chatlog = jQuery(xml).find("chatlog").html();
						var lastid = jQuery(xml).find("final").text();
						var records = jQuery(xml).find("records").text();
						//var last_read = jQuery(xml).find("last_read").text();
						//var user_id = jQuery(xml).find("user_id").text();
						//console.log("User: " + user_id);
						//console.log("Last: " + last_read);
						//console.log(chatlog);
						//var query = jQuery(xml).find("query").text();
						//var team = jQuery(xml).find("team").text();
						//var team_id = jQuery(xml).find("team_id").text();
						//var privacy = jQuery(xml).find("privacy").text();

						//console.log(svars.team_id + ":" + team + ":" + team_id);
						//console.log(svars.private + ":" + privacy);
						//console.log(query);

						// Handle deleted latest message
						if (lastid == 'A') {
							// Change last read to previous message
							// console.log("response: " + lastid);

							var last_message = jQuery('.message_line').last();
							var previous_message = jQuery(last_message).prev().data('m-id');

							if (previous_message)  {
								lastid = previous_message;
							}

						}




						var A = chat_holder.prop('scrollHeight');
						var B = chat_holder.prop('scrollTop');
						var C = chat_holder.prop('offsetHeight');

						if( A - B === C && records >= 1)
						{
							low_scroll = 1;
						}
						else
						{
							low_scroll =0;
						}

						//
						// TRICKERY!!! 
						// this isn't the "right" way to reverse the order of messages, but hey, it works. :) 
						chatMsgArray = $.parseHTML(chatlog);

						if (chatMsgArray && chatMsgArray.length > 0) {
							// console.log(chatMsgArray);
							chatMsgArray.reverse();
							chatlog = '';
							$.each(chatMsgArray, function(index, el) {
								chatlog += "" + $('<div>').html(el).html();
							});
						}
						chat_holder.append(chatlog);
						// end of trickery


						// chat_holder.prepend(chatlog);
						jQuery('#last_read').val(lastid);

						if(last == 0 || low_scroll == 1) {
							chat_holder.animate({ scrollTop: chat_holder.prop('scrollHeight')}, 240);
						}
						
						cbox_running = false;

						jQuery('.delete_line').unbind('click');

						jQuery('.delete_line').on('click', function(e) {
							delete_line(this);
							e.preventDefault();
						});

					},
					error:function(xml, status,error)
					{
						cbox_running = false;
						//alert('Error: ' + error);
					}
				});	
			}
		}
	}, 1500);
});