<?php

function chatterbox_master( $attributes )
{
	$var = shortcode_atts( array('user_id' => 0, 'team_id' => 0, 'private' => 0), $attributes);

	//
	//Make sure we have non-zero user and team
	if(!empty($var['user_id']) && !empty($var['team_id']))
	{
		//
		//Enqueue style sheet for the Chatterbox
		wp_enqueue_style('chatterbox_style', CB_CSS_DIR . "chatterbox.css", false, NULL);

		//
		//Enqueue script for loading chat box
		wp_enqueue_script('cbox_load_data', CB_JS_DIR . 'cbox.load.js', array('jquery'));
		wp_localize_script('cbox_load_data', 'svars', 
						   array('ajax' => admin_url('admin-ajax.php')));

		//
		//Enqueue script for reading chat data
		wp_enqueue_script('cbox_read_data', CB_JS_DIR . 'cbox.read.js', array('jquery'), '4.6.4');
		wp_localize_script('cbox_read_data', 'svars', 
						   array('team_id' => $var['team_id'], 
						   		 'user_id' => $var['user_id'],
						   		 'private' => $var['private'],
								 'ajax' => admin_url('admin-ajax.php')));

		//
		//Enqueue script for writing new data
		wp_enqueue_script('cbox_write_data', CB_JS_DIR . 'cbox.write.js', array('jquery'));
		wp_localize_script('cbox_write_data', 'svars', 
						   array('team_id' => $var['team_id'], 
						   		 'user_id' => $var['user_id'],
						   		 'private' => $var['private'],
								 'ajax' => admin_url('admin-ajax.php')));


		//
		//Dump in empty divs to hold data

		echo "<div id='chatter-content'>";

		echo "   <div class='data'>";
		echo "      <textarea id='data_input' class='data_input'
							  placeholder='Enter new message here' 
							  rows=2 cols=100>
					</textarea>  ";
		echo "";
		echo "      <button class='button' id='add_text'>Say It!</button>";			
		echo "   </div>";

		echo "   <div class='chatterbox_wrapper'>";
		echo "      <div class='message_line'>";
		echo "         <div class='cb_user_other'>Admin<br />A long time ago...</div>";
		echo "         <div class='cb_message_other'>I started the conversation, let's keep it going!</div>";
		echo "      </div>";
		//echo "      <div class='message_line'>";
		//echo "         <div class='cb_message_data'>Continuing</div>";
		//echo "         <div class='cb_user_data'>Me<br />2016-03-29 8:00</div>";
		//echo "      </div>";
		echo "   </div>";

		echo "</div>";
		
		echo "<input type='hidden' name='last_read' id='last_read' value='0'>";

	}
}

add_shortcode('chatterbox_chat_master' , 'chatterbox_master');
