<?php

function chatterbox_chat( $attributes = array(), $func_call = false )
{
	if($func_call)
	{
		$var = $attributes;	
	}
	else
	{
		$var = shortcode_atts( array('user_id' => 0, 'team_id' => 0, 'private' => 0, 'page' => ''), $attributes);
	}

	//echo var_dump($var);
	//
	//Make sure we have non-zero user and team
	if(!empty($var['user_id']) && !empty($var['team_id']))
	{
		//
		//Enqueue style sheet for the Chatterbox
		wp_enqueue_style('chatterbox_style', CB_CSS_DIR . "chatterbox.css", false, NULL);

		//
		//Enqueue script for loading chat box
		wp_enqueue_script('cbox_load_data', CB_JS_DIR . 'cbox.load.js', array('jquery'));
		wp_localize_script('cbox_load_data', 'svars_load', 
						   array('ajax' => admin_url('admin-ajax.php')));

		//
		//Enqueue script for reading chat data
		wp_enqueue_script('cbox_read_data', CB_JS_DIR . 'cbox.read.js', array('jquery'), '4.6.5');
		wp_localize_script('cbox_read_data', 'svars_read', 
						   array('team_id' => $var['team_id'], 
						   		 'user_id' => $var['user_id'],
						   		 'private' => $var['private'],
								 'ajax' => admin_url('admin-ajax.php')));

		//
		//Enqueue script for writing new data
		wp_enqueue_script('cbox_write_data', CB_JS_DIR . 'cbox.write.js', array('jquery'));
		wp_localize_script('cbox_write_data', 'svars_write', 
						   array('team_id' => $var['team_id'], 
						   		 'user_id' => $var['user_id'],
						   		 'private' => $var['private'],
						   		 'm_id' => 0,
								 'ajax' => admin_url('admin-ajax.php')));


		//
		//Can only do popout if we have a proper page to call
		$popoutable = false;
		$popout_url = '';
		$popout_opts = '';
		if (!empty($var['page']))
		{
			$popout_url = $var['page'] . '?user_id=' . $var['user_id'] . '&team_id=' . $var['team_id'] . '&private=' . $var['private'];
			$popout_opts = 'fullscreen=1, location=0, menubar=0, status=0, toolbar=0, titlebar=0, height=640, width=480, left=100';
			$popoutable = true;
		}

		echo "<div class='chatterbox__container ".($popoutable ? 'has-popout' : '')."'>";
		echo "    <div class='shelf-shadow'></div>";
		echo "    <div class='chatterbox_wrapper chatterbox__messages-container'></div>";
		echo "    <div class='chatterbox__compose-container'>";
/*
		echo "        <textarea id='data_input' class='data_input'
						placeholder='Enter new message here'
						onkeypress='if (event.keyCode == 13 && !event.shiftKey) document.getElementById(\"add_text\").click();'>
					  </textarea>
					  <button class='button x' id='add_text'>Send</button>
				  </div>";
*/
		echo "        <textarea id='data_input' class='data_input'
						placeholder='Enter new message here'>
					  </textarea>
					  <button class='button x' id='add_text'>Send</button>
				  </div>";
		echo "    <a id='popout-trigger' onclick='javascript:window.open(\"" . $popout_url . "\", target=\"blank\", \"" . $popout_opts . "\")'> open in window <i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></a>";
		echo "</div>";
		echo "<input type='hidden' name='last_read' id='last_read' value='0'>";
	}
}

add_shortcode('chatterbox_chat' , 'chatterbox_chat');
?>