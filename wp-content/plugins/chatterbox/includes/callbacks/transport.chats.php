<?php

//
//Includes
require CB_CLASSES_DIR . "chatter.class.php";
$chatter = NEW CHATTERBOX();

//
//Callback for cbox.read.js to read new chat lines
add_action('wp_ajax_cbox_read','cbox_read_callback');
add_action('wp_ajax_nopriv_cbox_read','cbox_read_callback');

function cbox_read_callback()
{
	$team_id = ((!empty($_POST['team_id'])) ? $_POST['team_id'] : '');
	$user_id = ((!empty($_POST['user_id'])) ? $_POST['user_id'] : '');
	$last_id = ((!empty($_POST['last_id'])) ? $_POST['last_id'] : 0);
	$privacy = ((!empty($_POST['privacy'])) ? $_POST['privacy'] : 'true');

	if(!empty($team_id) && !empty($user_id))
	{
		global $chatter;

		$chatter->set_user($user_id);
		$chatter->team($team_id);
		$chatter->privacy($privacy);
		$chatter->last_id($last_id);

		echo $chatter->get_messages();
	}

	wp_die();
}

//
//Callback for cbox.write.js to enter chat lines
add_action('wp_ajax_cbox_write','cbox_write_callback');
add_action('wp_ajax_nopriv_cbox_write','cbox_write_callback');

function cbox_write_callback()
{
	global $wpdb;
	global $chatter;

	$current_user = get_current_user_id();

	if (!empty( $_POST['m_id']) && (user_can($current_user, 'super') || user_can($current_user, 'admin'))) {
		// Delete message
//
		if (!empty($_POST['last_id'])) {
			$chatter->last_id($_POST['last_id']);
			$chatter->update_last_read($current_user);
		}

		$result = $wpdb->delete($chatter->get_messages_table(), array('id' => $_POST['m_id']), array('%d'));

		if ($result) {
			wp_send_json_success(array(
				'message' => 'deleted: ' . $_POST['m_id'],
				'last_id' => $chatter->get_last_id(),
			));
		} else {
			wp_send_json_error();
		}

		return;
	}

	$team_id = ((!empty($_POST['team_id'])) ? $_POST['team_id'] : '');
	$user_id = ((!empty($_POST['user_id'])) ? $_POST['user_id'] : '');
	$privacy = ((!empty($_POST['privacy'])) ? $_POST['privacy'] : 'true');
	$message = trim(((!empty($_POST['message'])) ? $_POST['message'] : ''));

	if(!empty($team_id) && !empty($user_id) && !empty($privacy) && !empty($message))
	{
		global $chatter;

		$chatter->set_user($user_id);
		$chatter->team($team_id);
		$chatter->privacy($privacy);

		$chatter->send_message($message);
	}

	wp_die();
}

?>