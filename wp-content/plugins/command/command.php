<?php
/**
 * @package Command_Center
 * @Version 0.1
 *
*/

/*
 Plugin Name: Command Center
 Plugin URI: N/A
 Description: ISL Core module for site control
 Author: Illumatech - David Thompson
 Version: 0.1
 Author URI: N/A
*/

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Make sure wp-includes/pluggable.php is included
if(!function_exists('wp_get_current_user')) { include(ABSPATH . 'wp-includes/pluggable.php'); }

//
//Constant Definitions
define ('CC_VERSION', '0.1');
define ('CC_PLUGIN_URL', plugin_dir_url(__FILE__));
define ('CC_PLUGIN_DIR', plugin_dir_path(__FILE__));

define ('CC_ADMIN_DIR', CC_PLUGIN_DIR . 'admin/');
define ('CC_USER_DIR', CC_PLUGIN_DIR . 'user/');
define ('CC_CONTROL_DIR', CC_PLUGIN_DIR . 'control/');
define ('CC_CLASSES_DIR', CC_PLUGIN_DIR . 'classes/');
define ('CC_INCLUDES_DIR', CC_PLUGIN_DIR . 'includes/');
define ('CC_AJAX_DIR', CC_PLUGIN_DIR . 'ajax_callbacks/');

define ('CC_CPOST_DIR', CC_INCLUDES_DIR . 'cposts/');
define ('CC_SHORTS_DIR', CC_INCLUDES_DIR . 'shortcodes/');
define ('CC_LINKS_DIR', CC_INCLUDES_DIR . 'links/');
define ('CC_MESSAGES_DIR', CC_INCLUDES_DIR . 'messages/');

define ('CC_CSS_PATH', '/wp-content/plugins/command/css/');
define ('CC_JS_PATH', '/wp-content/plugins/command/js/');


//
//Run proper activations
register_activation_hook ( __FILE__, 'class_activate' );

//
//dashboard mods
include (CC_CONTROL_DIR . "dashboard.php");

//
//Load if user has minimum permissions
//if(current_user_can('edit_pages'))
//{
	require_once ( CC_PLUGIN_DIR . 'settings.php');
	require_once ( CC_PLUGIN_DIR . 'command_center.php');
 
	add_action ('user_admin_menu', array ('COMMAND_CENTER', 'init'));
	add_action ('admin_menu', array ('COMMAND_CENTER', 'init'));

	//
	//Activate if we are admin
	if(is_admin())
		$my_settings = new COMMAND_CENTER_SETTINGS();	
//}

// 
// Load styles for CC pages
function load_command_center_styles() {
	wp_enqueue_style('command_center_styles', CC_CSS_PATH . 'style.css');
}
add_action( 'admin_enqueue_scripts', 'load_command_center_styles' );

//
//Function to call the install scripting to build/load necessary data structures
function class_activate()
{
	include ( CC_CONTROL_DIR . 'install.php' );
	$install = new COMMAND_INSTALL();	
}

//
//Add capabilities to user-page
global $pagenow;
if($pagenow == "user-edit.php")
{
	include CC_USER_DIR	. 'user.caps.php';
}

//
//Load all Shortcodes
//foreach( glob(CC_SHORTS_DIR . "*.php") as $filename)
//{
//	include $filename;
//}

//
//Load all Ajax Hanlders
foreach( glob(CC_AJAX_DIR . "*.php") as $filename)
{
	include $filename;
}


//
//Load all Ajax Hanlders
foreach( glob(CC_MESSAGES_DIR . "*.php") as $filename)
{
	include $filename;
}


//
//Load all Custom Posts
foreach( glob(CC_CPOST_DIR . "*.php") as $filename)
{
	include $filename;
	//flush_rewrite_rules();
}

//
//Load all Custom Links
foreach( glob(CC_LINKS_DIR . "*.php") as $filename)
{
	include $filename;
}


//
//Filter images in media library
add_filter( 'ajax_query_attachment_args', 'cc_media_library_filter_admin', 10, 1);

function cc_media_library_filter_admin( $query = array() )
{
	$user_id = get_current_user_id();
	
	if ($user_id)
	{
			$query['author'] = $user_id;
	}
	
	return $query;
	
}