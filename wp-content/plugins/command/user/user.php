<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');


//
//Load Classes
require_once (CC_CLASSES_DIR . "country.class.php");
require_once (CC_CLASSES_DIR . "program.class.php");
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "team.support.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "user.class.php");
require_once (CC_CLASSES_DIR . "acf.link.class.php");
require_once (CC_CLASSES_DIR . "message.class.php");

$team = new TEAM();
$support = new TEAM_SUPPORT();
$page = new TAB_MANAGE();
$program = new PROGRAM();
$country = new COUNTRY();
$rsvp = new RSVP();
$user = new USER();
$adv_custom_flds = new ACF_LINK();
$message = new MESSAGES();

$usr = wp_get_current_user();

$_GET['rsvp'] = isset($_REQUEST['rsvp_id']) ? $_REQUEST['rsvp_id'] : isset($_REQUEST['rsvp']) ? $_REQUEST['rsvp'] : '';

/////////////////////////////////////////
// IF WE HAVE A USER...
/////////////////////////////////////////
if (isset($_GET['user_id'])) :
	//Get User Name
	if($_GET['user_id']=='new')
	{
		//
		//Have a User, let's do stuff with it
		$page->SetTabs(array('vacct' => 'Account'));
		$page->SetCurrent('vacct');
	
		$user_name = "NEW VOLUNTEER";
		
		$avi = site_url('/wp-content/uploads/2016/03/avatar.png');
	}
	else 
	{
		//
		//Have a User, let's do stuff with it
		$poss_pages = array('vacct' => 'Account',
							 'vorder' => 'Order Info',
							 'vprofile' => 'Academic Profile', 
							 'vhealth' => 'Health Profile', 
							 'vemergency' => 'Emergency Contact', 
							 'vchat' => 'Staff Chat', 
							 'vdocs' => 'Docs', 
							 'vpass' => 'Passport',
							 'vflight' => 'Flight Info', 
							 'vemail' => 'Messages' 
						   );
		
		if($user->user_group(get_current_user_id()) == "enhanced" || $user->user_group(get_current_user_id()) == "volunteer")
		{
			unset($poss_pages['vchat']);
		}
		
		//
		//Check for Ambassador
		if(get_user_meta($_GET['user_id'], '_cmd_comm_ambassador_post', true) ||
		  $user->has_cap('cmd_isl_rep', ($_GET['user_id'])))
		{
			$poss_pages = array_merge($poss_pages, array('vambassador' => 'Ambassador'));
		}

		$page->SetTabs($poss_pages);
		
		$page->SetCurrent(!empty($_GET['cur_tab']) ? $_GET['cur_tab'] : 'vacct');

		$user_info = get_userdata($_GET['user_id']);
		
		if($user_info)
		{
			$user_name = $user_info->first_name . " " . $user_info->last_name; 
			$avi = get_post_field('guid', get_user_meta($_GET['user_id'],'local_avatar',true));
			
			//
			//Cap Code count
			global $wpdb;
			$query = "SELECT count(*) FROM " . $wpdb->prefix . "command_order_items WHERE item_type = 61 AND item_id = " . $user_info->ID . " and quantity > 0 and price < 0";
			$cnt = $wpdb->get_var($query);
		}
		else
		{
			echo "<script>";
			echo "window.location = 'admin.php?page=cc-volunteers&user_id=new'";
			echo "</script>";
		}

		//
		//If poc, then check to make sure we can view the user
		if($user->user_group($usr->ID) == "enhanced")
		{
			$allow = false;
			if( get_user_meta( $usr->ID, 'cmd_custom_team_poc', true))
			{
				$checks = $user->users_ids( array ('poc' => $usr->ID));

				if(in_array($user_info->ID, $checks))
				{
					$allow = true;
				}
			}

			if(!$allow)
			{
				echo "<script>";
				echo "window.location = 'admin.php?page=cc-volunteers'";
				echo "</script>";
				wp_die();
			}

		}

		//
		//Check to see there is an upcoming trip, or work around selected trip
		$rsvp = new RSVP();
		$user_reservations = $rsvp->for_user($_GET['user_id']);
		$rsvp_chooser = '';

		if(
			(
				empty($_GET['rsvp']) &&
				empty($_POST['rsvp_id'])
			)
			
			|| 
			
			(
				(!empty($_GET['rsvp']) && 
				 !isset($user_reservations[$_GET['rsvp']])
				)
			
			&& 
			
				(!empty($_POST['rsvp_id']) && 
				 !isset($user_reservations[$_POST['rsvp_id']])
				)
			)
		  )
		{
			$rsvp_id = $rsvp->next_for_user(); 

			if(empty($rsvp_id))
			{
				$rsvp_id = $rsvp->last_for_user();	
			}
		}
		else
		{
			if(	empty($_GET['rsvp']) )
			{
				$rsvp_id = $_POST['rsvp_id'];
			}
			else
			{
				$rsvp_id = $_GET['rsvp'];
			}
		}
	}

    //
	//Set core variables based on selected RSVP
	if(!empty($rsvp_id))
	{
		$team_id = $user_reservations[$rsvp_id]['team'];
		$hash = $user_reservations[$rsvp_id]['hash'];
		$team_url = "<a href='/wp-admin/admin.php?page=cc-teams&team_id=" . $team_id . "' target='_blank'>" . $hash . "</a>";
		$days = $user_reservations[$rsvp_id]['days_until']; 
		$post_id = $user_reservations[$rsvp_id]['post'];
		$post_name = get_post($post_id)->post_name;

		$team->set_team($team_id);
		$team_status = $team->value('status');
		$start_date = $team->arrival_date();
		
		if(!apply_filters('chatterbox_private_chat', (!empty($_GET['team_id']) ? $_GET['team_id'] : ''), $_GET['user_id']))
		{
			unset($poss_pages['vchat']);	
		}

		//
		//Configure drop down
		if(count($user_reservations) > 1)
		{
			$res = array();
			
			$rsvp_chooser = '';
			$rsvp_chooser .= "<FORM name='rsvp_chooser' method='POST' action='" . 
						$page->GET_path(array('team_id','rsvp')) . 
							"'>";
			$rsvp_chooser .="<SELECT id='rsvp' name='rsvp' 
									 onchange='javascript:document.rsvp_chooser.submit()'>";
	
			foreach($user_reservations as $res_id=>$res_data)
			{
				if(!in_array($res_data['hash'], $res))
				{
					$rsvp_chooser .= "<OPTION value='" . $res_id . "' " . 
										(($res_id == $rsvp_id) ? " SELECTED" : "") . "
										>" . $res_data['hash'] . "</OPTION>";
					$res[] = $res_data['hash'];
				}
			}
	
			$rsvp_chooser .= "</SELECT>";
			$rsvp_chooser .= "</FORM>";
	
			if(empty($res) || count($res)<2)
			{
				$rsvp_chooser = '';
			}
		}

		//
		//Gather Defined Data here
		global $wpdb;
		
		$vec = '';
		$tm = '';
		$query = "SELECT um.user_id, um.meta_value, u.display_name
					FROM " . $wpdb->prefix . "usermeta um INNER JOIN
						 " . $wpdb->prefix . "users u on u.ID = um.user_id 
				  WHERE meta_key = '" . 'cmd_team' . $team_id . '_staff_pos' . "'
				  ORDER BY display_name";
		$possibles = $wpdb->get_results($query, ARRAY_A);

		foreach($possibles as $data)
		{
			$staff[$data['meta_value']][$data['user_id']] = $data['display_name'];
		}

		if(!empty($staff['VEC']))
		{
			foreach($staff['VEC'] as $id=>$name)
			{
				$vec .= $name . "<BR>";	
			}
		}

		if(!empty($staff['Team Leader']))
		{
			foreach($staff['Team Leader'] as $id=>$name)
			{
				$tm .= $name . "<BR>";	
			}
		}
		$number = get_post_meta($post_id, '_cmd_team_leader', true);
		if(!empty($number)){
			$tm_user = get_userdata($number);
			$tm = $tm_user->display_name;
		}
		
	}
	else
	{
		$team_id =0;
		$team_status = "N/A";
		$start_date = "N/A";
		$rsvp_chooser = "<i>No Orders</i>";
		$team_url = 'None';
		$post_id = 0;
		$vec = '';
		$tm = '';
	}

			
	/** note processing */
	$meta_tag = $team_id ."_volunteer_note";
	if(isset($_REQUEST['submit_volunteer_note']) || isset($_REQUEST['volunteer_note']))
	{
		update_user_meta($_REQUEST['user_id'],$meta_tag,$_REQUEST['volunteer_note']);
	}

	$volunteer_note = get_user_meta($_REQUEST['user_id'],$meta_tag,true);
	$volunteer_note = !empty($volunteer_note)? $volunteer_note : null;

    /////////////////////////////////////
    // BEGIN HTML
    /////////////////////////////////////
    ?>
	<div class="page-header wrap">
        <a class="back-to-list-arrow" href='<?php echo $page->GET_path(array('cur_tab','user_id','save','team_id','rsvp','use_post'))?>'>
            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
        </a>

        <div class="avatar-wrap">
           	<img src="<?php _e($user->avatar($_GET['user_id']))?>" id="header_user_image"/>
        </div>

        <h1><?php _e($user_name . ((isset($cnt) && $cnt > 0) ? ' (' . $cnt . ' CAP uses)' : ''))?></h1>
    </div>

    <div class="header-table-wrap">
        <div class="user-details">
        	<dl class="left">
            	<dt>Email</dt>
                    <dd><?php echo (!empty($user_info) ? $user_info->user_email : "")?></dd>
                <dt>Phone</dt>
                    <dd><?php echo (!empty($user_info) ?  get_user_meta($_GET['user_id'] , 'primary_phone', true) : "")?></dd>
                <dt>Upcoming Team</dt>
                    <dd><?php echo $team_url ?></dd>
                <dt>Status</dt>
                    <dd><?php echo $support->status_value($team->value('status'))?></dd>
            </dl>
            <dl class="right">
                <dt>Start Date</dt>
                    <dd><?php echo $start_date?></dd>
                <dt>VEC</dt>
                    <dd><?php echo $vec?></dd>
                <dt>Team Leader</dt>
                    <dd><?php echo $tm?></dd>
            </dl>
        </div>

        <div id="user_notes">
            <div class="note-left">
                <h3>Notes (internal)</h3>
                <form id="volunteer_note_form"method="POST" enctype="multipart/form-data" action="<?=$page->GET_path()?>">
                    <textarea name="volunteer_note" placeholder="Type your message here"><?php echo $volunteer_note; ?></textarea>
                    <input id="sbmt_vlnt_nt" class="button button-primary" type="submit" name="submit_volunteer_note" value="Save Note" />
                </form>
            	<script>
            		jQuery("#volunteer_note_form").submit( function( event ) {
            			event.preventDefault();
            			jQuery("#sbmt_vlnt_nt").val("Saving...");
            			var form_data = jQuery("#volunteer_note_form").serialize();
            			jQuery.post("<?php echo $page->GET_path(); ?>",form_data).done(function(data) {
            				jQuery("#sbmt_vlnt_nt").val("Save Note");
            			});
            		});
            	</script>
            </div>

            <div class="note-right">
                <h3>Order History</h3>
                <?php echo $rsvp_chooser;?>
            </div>
        </div>
    </div>

	<?php
	if(!empty($rsvp_id))
	{
		$page->ShowTabs(array('save', 'use_post'), array('team_id' => $team_id, 'rsvp' => $rsvp_id));
	}
	else
	{
		if(isset($_GET['user_id']) && $_GET['user_id'] != 'new')
		{
			unset($poss_pages['vorder']);
			unset($poss_pages['vprofile']);
			unset($poss_pages['vhealth']);
			unset($poss_pages['vemergency']);
			unset($poss_pages['vchat']);
			unset($poss_pages['vdocs']);
			unset($poss_pages['vflight']);
			unset($poss_pages['vemail']);

			$page->SetTabs($poss_pages);
			$page->SetCurrent(!empty($_GET['cur_tab']) ? $_GET['cur_tab'] : 'vacct');

			$page->ShowTabs(array('save', 'use_post'), array());
		}

	}

	//
	//Deal with tabs
	switch ($page->GetCurrent())
	{
		case 'vchat':
			include (CC_USER_DIR . "tabs/v.chat.php");
			break;	
		case 'vacct':
			include (CC_USER_DIR . "tabs/v.account.php");
			break;	
		case 'vprofile':
			include (CC_USER_DIR . "tabs/v.profile.php");
			break;
		case 'vhealth':
			include (CC_USER_DIR . "tabs/v.health.php");
			break;
		case 'vemergency':
			include (CC_USER_DIR . "tabs/v.emergency.php");
			break;
		case 'vorder':
			include (CC_USER_DIR . "tabs/v.order.php");
			break;
		case 'vdocs':
			include (CC_USER_DIR . "tabs/v.docs.php");
			break;
		case 'vpass':
			include (CC_USER_DIR . "tabs/v.passport.php");
			break;
		case 'vflight':
			include (CC_USER_DIR . "tabs/v.flight.php");
			break;
		case 'vemail':
			include (CC_USER_DIR . "tabs/v.email.php");
			/** form processing */
			if(isset($_POST['submit_message']))
			{
				$message->set_recipient($_REQUEST['user_id']);
				$message->set_team_id($_REQUEST['team_id']);
				$message->set_rsvp_id($_REQUEST['rsvp']);
				$message->set_subject($_REQUEST['message_subject']);
				$message->set_message_text($_REQUEST['message_text']);
				$cur_user = wp_get_current_user();
				$message->set_sender($cur_user->ID);
				$message->add_message('Workaround');
				//do_action('cmd_queue_trigger',"Personal Message Sent",array(),array("recipient" => $_REQUEST['user_id'],"team_id" => $_REQUEST['team_id']));
			}
			break;
		case 'vambassador':
			include (CC_USER_DIR . "tabs/v.ambassador.php");
			break;
		default:
			break;
	}
endif;

/////////////////////////////////////////
// IF WE *DO NOT* HAVE A USER...
/////////////////////////////////////////
if (!isset($_GET['user_id'])) :

	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script('cmd_school_selector', plugins_url() . '/command/js/school.selector.js', array('jquery', 'jquery-ui-autocomplete'), NULL, true );
	wp_localize_script('cmd_school_selector', 'svars', array('ajax_url' => admin_url('admin-ajax.php')));

	$currec = ((!empty($_POST['cur_rec'])) ? $_POST['cur_rec'] : 0);

	?>

	<div class='page-header wrap'>
    	<h1>Volunteers</h1>

    	<a class='page-title-action' href='<?=$page->GET_path(array('ambassador'),array('user_id'=>'new'))?>'>Add New</a>

	    <form id="vol_search_too" name="search_too" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('ambassador'))?>">
		<span>
          	<input name="code" id="code" class="regular-text" type="text" size='20' maxlength='20' 
				<?php echo "value='" . (!empty($_POST['code']) ? $_POST['code']  : '') . "'"?> placeholder="First or Last Name Search"/>
	        <button type="submit" name="submit_fln" id="submit_fln" class="button" value="Search"><i class="fa fa-caret-right" aria-hidden="true"></i></button>
		</span>
		</form>
       
        <a id="advanced_search_toggle" class="button" onClick="javascript:toggle_search()">
            Advanced Search
            <i class="arrow-up fa fa-caret-up" aria-hidden="true"></i>
            <i class="arrow-down fa fa-caret-down" aria-hidden="true"></i>
        </a>
			
    	<a class='page-title-action' href='<?=$page->GET_path(array('ambassador'),array('team_id'=>'new'))?>'>My Volunteers<i class="fa fa-search" aria-hidden="true"></i></a>

       	<a class='page-title-action' href='<?=$page->GET_path(array('ambassador'),array('ambassador'=>'true'))?>'>Ambassadors<i class="fa fa-search" aria-hidden="true"></i></a>
    </div>

	<form id="vol_search" name="vol_search" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('ambassador'))?>">
    	<table id='search_table' class='standard-large-table wrap'>
    		<tr>
            	<td>
                	<input type='text' name='email' id='email' placeholder='Email' value='<?php _e(!empty($_POST['email']) ? $_POST['email'] : '')?>''>
    			</td>
       			<td>
                	<SELECT name="country" id="country"><?php echo $country->selector(isset($_POST['country'])? $_POST['country'] : 0,true) ?></SELECT>
                </td>
    			<td>
                	<input 
                        name="joindateafter" 
                        id="joindateafter" 
                        class="regular-text" 
                        type="text"
                        placeholder="Join Date After"
                        value='<?php _e((isset($_POST['joindateafter']) ? $_POST['joindateafter'] : ''))?>' <?php _e((!empty($_POST['joindateafter']) ? '' : 'placeholder="Join Date After"'))?>
                    />
                	<span class="date-icon"></span>
                </td>
    			<td>
      				<input name="joindatebefore" id="joindatebefore" class="regular-text" type="text" value='<?php _e((isset($_POST['joindatebefore']) ? $_POST['joindatebefore'] : ''))?>' <?php _e((!empty($_POST['joindatebefore']) ? '' : 'placeholder="Join Date Before"'))?>/>
                	<span class="date-icon"></span>
                </td>
                <td>&nbsp;</td>
    		</tr>

    		<tr>
            	<td>
                	<input type='text' name='phone' id='phone' placeholder='Phone' value='<?php _e(!empty($_POST['phone']) ? $_POST['phone'] : '')?>''>
    			</td>
    			<td>
                	<SELECT name="program" id="program"><?php echo $program->selector(isset($_POST['program'])? $_POST['program'] : 0,true) ?></SELECT>
                </td>
    			<td>
                	<input name="teamstartafter" id="teamstartafter" class="regular-text" type="text" value='<?php _e((isset($_POST['teamstartafter']) ? $_POST['teamstartafter'] : ''))?>' <?php _e((!empty($_POST['teamstartafter']) ? '' : 'placeholder="Team Start Date After"'))?>/>
                	<span class="date-icon"></span>
                </td>
    			<td>
                	<input name="teamstartbefore" id="teamstartbefore" class="regular-text" type="text" value='<?php _e((isset($_POST['teamstartbefore']) ? $_POST['teamstartbefore'] : ''))?>' <?php _e((!empty($_POST['teamstartbefore']) ? '' : 'placeholder="Team Start Date Before"'))?>/>
                	<span class="date-icon"></span>
                </td>
    			<td><button type="submit" name="submit_ms" id="submit_ms" class="button"><i class="fa fa-search" aria-hidden="true"></i></button></td>
    		</tr>

    		<tr>
            	<td>
					<select name="role" id="role"><?php echo $user->group_selector(isset($_POST['role']) ? $_POST['role'] : 0, true, array('enhanced')) ?> </select>
    			</td>
    			<td>
                	<div class="school_select" id="school_select">
                		<input type='text' name='school' id="school" value='<?php _e((isset($_POST['school']) ? $_POST['school'] : ''))?>' <?php _e((!empty($_POST['school']) ? '' : 'placeholder="School"'))?>/>
					</div>
                </td>
    			<td>
                	<select name="vec" id="vec">
    					<?php echo $user->user_select($user->fetch_vec(), !empty($_POST['vec']) ? $_POST['vec'] : 0, 'VEC (ANY)', true)?></select>
                </td>
    			<td>
                	<select name="team_lead" id="team_lead">
    					<?php echo $user->user_select($user->fetch_team_managers(), !empty($_POST['team_lead']) ? $_POST['team_lead'] : 0, 'TEAM LEADER (ANY)', true)?></select>
                </td>
    			<td><button type="" name="reset" id="reset" class="button" onclick="javascript:reset_form()">ALL</i></button></td>
    		</tr>
  <!-- 2018-03-27 Added new line of filters -->
           	<tr>
            	<td>
					<select name="countryo" id="countryo">
						<option value = 0 <?php echo (!empty($_POST['countryo']) && $_POST['countryo'] == 0 ? ' selected' : '');?>>--SELECT COUNTRY--</option> 
						<option value = 100 <?php echo (!empty($_POST['countryo']) && $_POST['countryo'] == 100 ? ' selected' : '');?>>Canada</option>
						<option value = 400 <?php echo (!empty($_POST['countryo']) && $_POST['countryo'] == 400 ? ' selected' : '');?>>Puerto Rico</option>
						<option value = 500 <?php echo (!empty($_POST['countryo']) && $_POST['countryo'] == 500 ? ' selected' : '');?>>United States</option>
					</select>
    			</td>
    			<td>
                	<div class="state" id="state">
                		<?php echo $rsvp->Selector_States(!empty($_POST['state']) ? $_POST['state'] : 0)?>
					</div>
                </td>
    			<td>
                	<select name="role_new" id="role_new">
						<option value = 0 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 0 ? ' selected' : '');?>>--SELECT ROLE--</option>
						<option value = 1 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 1 ? ' selected' : '');?>>Faculty</option>
						<option value = 2 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 2 ? ' selected' : '');?>>Undergrad</option>
						<option value = 3 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 3 ? ' selected' : '');?>>Graduate Student</option>
						<option value = 4 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 4 ? ' selected' : '');?>>Medical Professional</option>
						<option value = 5 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 5 ? ' selected' : '');?>>High School Student</option>
						<option value = 6 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 6 ? ' selected' : '');?>>School Staff</option>
						<option value = 99 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 99 ? ' selected' : '');?>>Other</option>
						<option value = 100 <?php echo (!empty($_POST['role_new']) && $_POST['role_new'] == 100 ? ' selected' : '');?>>Undefined</option>
					</select>
                </td>
    			<td>&nbsp;</td>
    			<td>
				    <button type="" name="csv" id="csv" class="button">CSV</i></button>
                </td>
    		</tr>
    	</table> <!-- /#search_table -->

        <input type="hidden" name="qnum" id="qnum" value="100">
        <input type="hidden" name="cur_rec" id="cur_rec" value="<?php _e($currec) ?>">
    </form>

    <script>
		function reset_form()
		{
		 	document.getElementById('code').value = '';	
		 	document.getElementById('email').value = '';	
		 	document.getElementById('country').value = '';	
		 	document.getElementById('joindateafter').value = '';	
		 	document.getElementById('joindatebefore').value = '';	
		 	document.getElementById('phone').value = '';	
		 	document.getElementById('program').value = '';	
		 	document.getElementById('teamstartafter').value = '';	
		 	document.getElementById('teamstartbefore').value = '';	
		 	document.getElementById('role').value = '0';	
		 	document.getElementById('school').value = '';	
		 	document.getElementById('vec').value = '';	
		 	document.getElementById('team_lead').value = '';
		 	document.getElementById('countryo').value = '';
		 	document.getElementById('state').value = '';
		 	document.getElementById('role_new').value = '';
		 	document.getElementById('cur_rec').value = '';
			
			//document.getElementById('submit').click();
		}
		
		
		function toggle_search() {
            var table = jQuery('#search_table');
            var toggle = jQuery('#advanced_search_toggle');

            if (table.hasClass('open')) {
                table.removeClass('open');
                toggle.removeClass('open');
            } else {
                table.addClass('open');
                toggle.addClass('open');
            }
        }

    	jQuery(document).ready(function(){
            toggle_search();

            var datepickerOptions = {
                dateFormat : 'mm-dd-yy',
                defaultDate: 0,
                //minDate: 0,
                beforeShow: function(event, ui) {
                   jQuery('#ui-datepicker-div').addClass('open');
                },
                onClose: function(event, ui) {
                    jQuery('#ui-datepicker-div').removeClass('open');
                }
            };

    		jQuery('#joindateafter').datepicker(datepickerOptions);
    		jQuery('#joindatebefore').datepicker(datepickerOptions);
    		jQuery('#teamstartafter').datepicker(datepickerOptions);
    		jQuery('#teamstartbefore').datepicker(datepickerOptions);
    	});
    </script>
    
    <div class="main-list-wrapper">
    <table id="vol_info" class="volunteers-list standard-large-table fixed striped">
        <thead>
        	<tr>
                <th>Name</th>
                <th>Team ID</th>
                <th>Email</th>
            	<th class="large">Phone</th>
                <th class="large">School</th>
                <!-- 2018-03-11 DJT Changed lagel to "Permissions" from "Roles" -->
                <!-- <th class="medium">Permissions</th> -->
                <th class="medium">Permissions</th>
                <th colspan="2">Volunteer Chart Notes</th>
            </tr>
        </thead>

        <?php
    	//
    	//Fetch all user information
    	$filters = array();
    	!empty($_POST['code']) ? $filters['first_name'] = $_POST['code'] : '';
    	!empty($_POST['code']) ? $filters['last_name'] = $_POST['code'] : '';

    	!empty($_POST['email']) ? $filters['user_email'] = $_POST['email'] : '';
    	!empty($_POST['country']) ? $filters['country'] = $_POST['country'] : '';
    	!empty($_POST['joindateafter']) ? $filters['joindateafter'] = $_POST['joindateafter'] : '';
    	!empty($_POST['joindatebefore']) ? $filters['joindatebefore'] = $_POST['joindatebefore'] : '';

    	!empty($_POST['phone']) ? $filters['phone'] = $_POST['phone'] : '';
    	!empty($_POST['program']) ? $filters['program'] = $_POST['program'] : '';
    	!empty($_POST['teamstartafter']) ? $filters['teamstartafter'] = $_POST['teamstartafter'] : '';
    	!empty($_POST['teamstartbefore']) ? $filters['teamstartbefore'] = $_POST['teamstartbefore'] : '';
    	
    	!empty($_POST['role']) ? $filters['role'] = $_POST['role'] : '';
    	!empty($_POST['school']) ? $filters['school'] = $_POST['school'] : '';
    	!empty($_POST['vec']) ? $filters['vec'] = $_POST['vec'] : '';
    	!empty($_POST['team_lead']) ? $filters['team_lead'] = $_POST['team_lead'] : '';

    	!empty($_POST['countryo']) ? $filters['countryo'] = $_POST['countryo'] : '';
    	!empty($_POST['state']) ? $filters['state'] = $_POST['state'] : '';
    	!empty($_POST['role_new']) ? $filters['role_new'] = $_POST['role_new'] : '';

		!empty($_GET['ambassador']) ? $filters['ambassador'] = 1 : '';

		$user_data = $user->user_list($currec, 100, $filters);
        $csv_data = array();

        /////////////////////////
        // VOLUNTEERS LIST LOOP
        /////////////////////////
    	foreach($user_data as $user_id=>$data) {
    		$user_reservations = $rsvp->for_user($user_id);
    		$rsvp_id = $rsvp->next_for_user();
    		if(empty($rsvp_id))
    		{
    			$rsvp_id = $rsvp->last_for_user();
    		}

    		if($rsvp_id > 1)
    		{

    			if(!empty($user_reservations[$rsvp_id]))
    			{
    				$team_id = $user_reservations[$rsvp_id]['team'];
    				$hash = $user_reservations[$rsvp_id]['hash'];
    			
    				$team_url = "<a href='/wp-admin/admin.php?page=cc-teams&team_id=" . $team_id . "' target='_blank'>" . $hash . "</a>";
    			}
    			else
    			{
    				$team_url = "No Reservations";	
    				$team_id = 0;
    			}
    		}
    		else
    		{
    			$team_url = "No Reservations";	
    			$team_id = 0;
    		}

    		if(isset($_REQUEST['edit_volunteer_note']) || isset($_REQUEST['volunteer_note']))
    		{
    			update_user_meta($_REQUEST['volunteer_id'],$_REQUEST['team_id'] ."_volunteer_note",$_REQUEST['volunteer_note']);
    		}

    		$volunteer_note = get_user_meta($user_id, $team_id ."_volunteer_note", true);
    		$volunteer_note = !empty($volunteer_note)? $volunteer_note : '<span class="placeholder-text">Enter notes here</span>';
    		$volunteer_note = '<span class="volunteer_note note_field" onclick="editVolunteerNote('. $user_id .','.$team_id .');">'. $volunteer_note .'</span>';

            // Phone number formatting...
            $bad_phone_number = explode("\n", get_user_meta($user_id, 'primary_phone', true));
            $nicer_phone_number = '';
            foreach($bad_phone_number as $number) {
                $nicer_phone_number .= preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '($1) $2-$3', $number). "\n";
            }

			$the_school = isset($user_reservations[$rsvp_id]['post']) ? get_field('school', $user_reservations[$rsvp_id]['post']) : '';

		    // CSV data
		    if (isset( $_POST['csv'])) {

			    // Get and user's state
			    $state = get_user_meta($user_id, 'state', true);
			    if ($field = get_field_object('field_5721d2418b823')) {
				    $states = $field['choices'];
				    $state_name = array_key_exists($state, $states) ? $states[$state] : 'N/A';
			    }

			    // Build csv data array
			    $csv_data[] = array(
			        'name'       => $data['first_name'] . " " . $data['last_name'],
				    'team_id'    => !empty($user_reservations[$rsvp_id]) ? $hash : $team_url,
				    'email'      => $data['email'],
				    'phone'      => rtrim($nicer_phone_number),
				    'school'     => $the_school,
				    'permission' => ucfirst($user->user_title($user_id)),
				    'state'      => $state_name,
			    );

		    }

			//
			//If$user->user_group($user_id)
    		echo "<tr>";
    		//
			//2018-03-11 DJT altered order of names
            //echo "    <td><a href='" . $page->GET_path(array('user_id','save')) . "&user_id=" . $user_id . "'>" . $data['last_name'] . ", " . $data['first_name'] . "</a></td>";
			echo "    <td><a href='" . $page->GET_path(array('user_id','save')) . "&user_id=" . $user_id . "'>" . $data['first_name'] . " " . $data['last_name'] . "</a></td>";
            echo "    <td>" . $team_url . "</td>";
    		echo "    <td style='word-wrap: break-word;'>" . $data['email'] . "</td>";
    		echo "    <td>" . $nicer_phone_number . "</td>";
    		echo "    <td>" . $the_school . "</td>";
    		echo "	  <td>" . ucfirst($user->user_title($user_id)) . "</td>";
    		echo "    <td colspan='2' id=\"volunteer_note_". $user_id ."\">" .$volunteer_note. "</td>";
    		echo "</tr>";
    	}

        if (isset( $_POST['csv'])) {

//                echo '<pre>';print_r($csv_data);die;

            $path_to_dir = WP_CONTENT_DIR . '/uploads/zl-user-csv/';
            if (!file_exists( $path_to_dir)) {
	            mkdir( $path_to_dir, 0777, true );
            }

            $csv_file = 'volunteerCSV-' . wp_create_nonce(date("YmdH:i")) . '.csv';

            $output = fopen($path_to_dir . $csv_file, 'w');

            if ($output) {

	            $fields = array(
		            'name'       => 'Name',
		            'team_id'    => 'Team ID',
		            'email'      => 'Email',
		            'phone'      => 'Phone',
		            'school'     => 'School',
		            'permission' => 'Permissions',
		            'state'      => 'State',
	            );

	            fputcsv( $output, $fields );

	            // Add each row to the csv file
	            foreach ( $csv_data as $row ) {
		            fputcsv( $output, $row );
	            }

	            // Close it up
	            fclose( $output );

	            echo '<div style="text-align:center;padding:10px;"><button class="button" style="background:#f15a2b;border:0px;padding:2px 10px;"><a style="color:#fff;text-decoration: none;" href="/wp-content/uploads/zl-user-csv/'. $csv_file . '">Download CSV</a></button></div>';
            }

    	}
//echo var_dump($user->list_cap_codes());
        ?>
    </table> <!-- /#vol_info -->
    </div>

    <script>
    	function editVolunteerNote(volunteer_id,team_id)
    	{
    		var form = jQuery('<form method="POST" class="edit_note_form edit_volunteer_note_form" action="<?php echo $page->GET_path(); ?>">');
            var container = jQuery("#volunteer_note_"+ volunteer_id);
            var note_text = container.children('.volunteer_note').html();

            if (note_text == '<span class="placeholder-text">Enter notes here</span>') {
                note_text = 'Enter notes here';
            }
    		form.append('<input type="hidden" name="volunteer_id" value="'+volunteer_id+'" />');
    		form.append('<input type="hidden" name="team_id" value="'+team_id+'" />');
    		form.append('<input type="text" class="note_field" placeholder="Enter notes here" name="volunteer_note" value="'+note_text+'" />');
    		form.append('<input type="submit" class="save-note button button-primary" name="edit_volunteer_note" value="Save Note" /> ');
    		form.append('<button type="button" class="cancel-save-note button button-primary" onclick="resetVolunteerNote('+ volunteer_id + ',' + team_id + ',\'' + note_text + '\')">X</button>');
    		container.html(form);
            container.find('input.note_field').select();
    	}

    	function resetVolunteerNote(volunteer_id,team_id,note_text)
    	{
    		var container = jQuery("#volunteer_note_"+ volunteer_id);
            var note_text = note_text;

            if (note_text == '' || note_text == 'Enter notes here') {
                note_text = '<span class="placeholder-text">Enter notes here</span>';
            } else {
                note_text = note_text;
            }

    		container.html('<span class="volunteer_note note_field" onclick="editVolunteerNote('+ volunteer_id + ',' + team_id + ');">' + note_text + '</span>');
    	}

    	jQuery(document).on("submit",".edit_volunteer_note_form",function(event)
    	{
    		event.preventDefault();
    		var t_id = jQuery("input[name='team_id']",this).val();
    		var v_id = jQuery("input[name='volunteer_id']",this).val();
    		var v_note = jQuery("input[name='volunteer_note']",this).val();

    		jQuery.post("<?php echo $page->GET_path(); ?>",jQuery(this).serialize()).done(function()
    		{
    			resetVolunteerNote(v_id,t_id,v_note);
    		});
    	});
    </script>

    <hr>

    <div class="pagination-wrapper">
        <a class="pagination-arrow first-page" onclick="javascript:reload(0)"><i class="fa fa-fast-backward" aria-hidden="true"></i></a>
        <a class="pagination-arrow prev-page" onclick="javascript:reload(<?php _e(max(0,$currec-100))?>)"><i class="fa fa-step-backward" aria-hidden="true"></i></a>

    	<input type="text" name="start_cnt" id="start_cnt" disabled="true" size="6" value="<?php _e($currec)?>">
        to
    	<input type="text" name="end_cnt" id="end_cnt" disabled="true" size="6" value="<?php _e($currec+99)?>">

        <a class="pagination-arrow next-page" onclick="javascript:reload(<?php _e(max(0,$currec+100))?>)"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
        <a class="pagination-arrow last-page" onclick="javascript:reload(<?php _e($user->Count_users()-100)?>)"><i class="fa fa-fast-forward" aria-hidden="true"></i></a>
        
        of 
        
        <input type="text" name="ttl_cnt" id="ttl_cnt" disabled="true" size="6" value="<?php _e($user->Count_users())?>">
    </div>

    <script>
    	function reload(start_num)
    	{
    		document.getElementById('cur_rec').value = start_num;
    		document.getElementById('vol_search').submit();
    		console.log(document.getElementById('cur_rec').value);
    	}
    </script>

<?php
endif;
?>