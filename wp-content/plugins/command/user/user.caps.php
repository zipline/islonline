<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Bring in necessary class(es)
include_once(CC_CLASSES_DIR . 'user.class.php');
$command_user = new USER();

function cmd_admin_add_caps($user_id, $user_role)
{
	if (!current_user_can('edit_user',get_current_user_id()))
	{
		return false;
	}
	
	global $command_user;
	$user_role = $command_user->user_group($user_id);

	$groups = $command_user->list_groups();
	$pass = 0;

	foreach($groups as $group_id => $data)
	{
		if($command_user->cap_count($group_id) && $user_role == $data['code'])
		{
			if($pass == 0)
			{
			?>
				<tr><th><label for="caps"><?php _e('Capabilities') ?></label></th>	
					<td>
			<?php
			}
			
			$pass++;
			$command_user->set_group($group_id);
			echo $command_user->cap_checks($user_id);			
		}
	}
}
add_action ('admin_end_profile_post_role', 'cmd_admin_add_caps');


function cmd_admin_write_caps($user_id)
{

	if (!current_user_can('edit_user',get_current_user_id()))
	{
		return false;
	}
	
	global $command_user;
	
	$groups = $command_user->list_groups();

	foreach($groups as $group_id => $data)
	{
		$command_user->set_group($group_id);
		$caps = $command_user->cap_details();
		
		foreach($caps as $cap_id => $cap_data)
		{
			if(isset($_POST[$cap_data['code']]))
			{
				update_user_meta($user_id, $cap_data['code'], sanitize_text_field($_POST[$cap_data['code']]));
			}
			else
			{
				delete_user_meta($user_id, $cap_data['code']);
			}
		}
	}

}
add_action ('edit_user_profile_update', 'cmd_admin_write_caps');

