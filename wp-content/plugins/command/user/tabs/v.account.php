<?php
require_once CC_CLASSES_DIR . "user.class.php";
$user = new USER();
$user_role = $user->user_group($usr->ID);

//
//Setup classes
wp_enqueue_script('cmd_settings_pics', plugins_url() . '/command/js/settings.pics.js', array('jquery'), '4.7.0');	
wp_localize_script('cmd_settings_pics', 'svars', array( 'banner' => 0,
													    'user' => 1,
														'ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form')
														));
if(!empty($_POST['action']) && $_POST['action']=="update")
{
	if($_GET['user_id']=='new')
	{
		//
		//Create a new user
		$user_info['user_login'] = $_POST['user_login'];
		$user_info['user_pass'] = $_POST['password'];
		$user_info['first_name'] = $_POST['first_name'];
		$user_info['last_name'] = $_POST['last_name'];
		$user_info['user_email'] = $_POST['email'];

		$new_user = $user->create_user($user_info);
		
		if(isset($new_user) && is_numeric($new_user) && !is_wp_error($new_user))
		{
			$user_info = get_userdata($new_user);
		
			$_GET['user_id'] = $user_info->ID;
		}
		else
		{
			if(is_wp_error($new_user))
			{
				$error = $new_user->get_error_message();
				unset($new_user);
				unset($user_info);
			}
		}
	}
		
	$fld_list = $_POST;
	$user_fields = array('url', 'email', 'display_name', 
						 'first_name', 'last_name', 'nickname', 'description'
						);

	//
	//Fix role if it's changed.
	if(!isset($error))
	{
		if($user->user_group($user_info->ID) != $_POST['role'])
		{
			$allow = false;

			switch($user_role)
			{
				case 'super':
					$allow = true;
					break;
				case 'admin':
					if($_POST['role'] == 'staff')
					{
						$allow=true;
					}
				case 'staff':
					if($_POST['role'] == 'enhanced')
					{
						$allow=true;
					}
				case 'enhanced':
					if($_POST['role'] == 'volunteer')
					{
						$allow=true;
					}
					break;
				default:
					$allow = false;
			}

			if($allow)
			{
				$user_updates = array('ID' => $_GET['user_id'],
									  'role' => $_POST['role']);

				wp_update_user($user_updates);

				$usr_upload = new WP_USER($user_info->ID);
				$usr_upload->remove_cap('upload_files');

				unset($_POST['role']);
			}
		}
	}

	if(!isset($error))
	{
		//
		//Fix capabilities for users
		$groups = $user->list_groups();
		foreach($groups as $group_id => $data)
		{
			$user->set_group($group_id);
			$caps = $user->cap_details();

			foreach($caps as $cap_id => $cap_data)
			{
				if(isset($_POST[$cap_data['code']]))
				{
					update_user_meta($user_info->ID, $cap_data['code'], sanitize_text_field($_POST[$cap_data['code']]));
					unset($_POST[$cap_data['code']]);
				}
				else
				{
					delete_user_meta($user_info->ID, $cap_data['code']);
				}
			}
		}

		//
		//Save non- role/capability options	
		// All role and capability variables should be unset by now.
		foreach($_POST as $index=>$value)
		{
			//
			//ACF Fields
			if($index == "acf")
			{
				foreach($value as $acf_index=>$acf_value)
				{
					//
					//ACF field so handle differently
					if(substr($acf_index,0,6) == "field_" && !empty($fld_list['acf'][$acf_index]))
					{
						$adv_custom_flds->save_field_group($acf_index, $fld_list['acf'], $_GET['user_id'], NULL, true);
					}
				}
			}

			//
			//Standard user field, so handle it normally
			if(!empty($fld_list[$index]) && in_array($index, $user_fields))
			{
				$user_updates = array('ID' => $_GET['user_id'],
									  'user_url' => trim($_POST['url']),
									  'user_email' => trim($_POST['email']),
									  'display_name' => isset($_POST['display_name']) ? trim($_POST['display_name']) : '',
									  'first_name' => trim($_POST['first_name']),
									  'last_name' => trim($_POST['last_name']),		
									  'nickname' => trim($_POST['nickname']),
									  'description' => trim($_POST['description'])
									 ); 
				wp_update_user($user_updates);

				foreach($user_fields as $field)
				{
					unset($fld_list[$field]);
				}
			}

			if(!empty($value) && $index=="password" && $value != "****************")
			{
				wp_set_password($value, $_GET['user_id']);	

				if($_GET['user_id'] == $user_info->ID)
				{
					do_action( 'wp_login', $user_info->user_login );					
				}
			}
		}

		if($_GET['user_id']=='new' && $new_user > 0)
		{
			$new_path = site_url('wp-admin/admin.php?page=cc-volunteers&user_id=' . $new_user);

			echo "<script>";
			echo "  window.location = '$new_path';";
			echo "</script>";
		}
	}
}

$user_login_set = 'disabled';
if($_GET['user_id']=='new')
	$user_login_set = '';

if(!isset($user_info))
{
	$user_info = new stdClass();
	$user_info->user_login = '';
	$user_info->ID = '';
	$user_info->first_name = '';
	$user_info->last_name = '';
	$user_info->display_name = '';
	$user_info->nickname = '';
	$user_info->user_email = '';
	$user_info->user_url = '';
	$user_info->description = '';
}

?>
<script>
jQuery(document).ready(function()
{
	var timer;
	
	jQuery('.u_info').on('keyup', '#user_login', function () 
	{
		clearTimeout(timer);
		timer = setTimeout( function()
		{
			var username = jQuery('#user_login');
			var userfield = jQuery('#username');

			jQuery.post(
				'<?php echo admin_url( 'admin-ajax.php' )?>', 
				{
					'action': 'cmd_user_name_check',
					'data':   {'usernameToCheck': username.val()} 
				}, 
				function(response)
				{ 
					if(response == 1) 
					{
						//username.css('border-color', 'green 5px solid');
						console.log('Green');
						//username.attr('class', 'validateGreen');
						//username.addClass('validateGreen');
						userfield.addClass('available');
						userfield.removeClass('unavailable');
						jQuery('#submit').prop('disabled',false);
					} 
					else 
					{
						console.log('Red');
						//username.attr('class', 'validateRed');
						userfield.addClass('unavailable');
						userfield.removeClass('available');
						jQuery('#submit').prop('disabled',true);
					}
				}
		   );
		}, 750);
	});
});
</script>

<style>

	.validateGreen 
	{
		border: green 5px solid;
	}

	.validateRed 
	{
		border: red 5px solid;
	}
</style>


<form class="u_info" name="u_info" method = "POST" action="<?php $page->GET_path(array('save'))?>">
 
	<table>
	<?php
	if(isset($error))
	{
		echo "<tr><TD colspan=2><B>Error: " . $error . "</B></TD></TR>";
	}
	?>

	<tr><td><h2>Name</h2></td></tr>

		<tr class="user-user-login-wrap">
			<th><label for="user_login">Username</label></th>
			<td id='username'><input type="text" name="user_login" id="user_login" value="<?php _e($user_info->user_login)?>" <?php echo $user_login_set?> class="regular-text" /> <span class="description">Usernames cannot be changed.</span></td>
		</tr>

		<tr class="user-role-wrap"><th><label for="role">Role</label></th>
			<td><select name="role" id="role">
					<?php echo $user->group_selector($user->user_group($user_info->ID), false)?>
					<option value="">&mdash; No role for this site &mdash;</option>
	            </select>
	        </td>
	    </tr>
		
	    <?php
		if($_GET['user_id'] != 'new')
		{
			if (current_user_can('edit_user',get_current_user_id()))
			{
				$user_role = $user->user_group($user_info->ID);

				$groups = $user->list_groups();
				$pass = 0;

				foreach($groups as $group_id => $data)
				{
					if($user->cap_count($group_id) && $user_role == $data['code'])
					{
						if($pass == 0)
						{
						?>
							<tr><th><label for="caps"><?php _e('Capabilities') ?></label></th>	
								<td>
						<?php
						}

						$pass++;
						$user->set_group($group_id);
						echo $user->cap_checks($user_info->ID);			
					}
				}
			}
		}
		?>
	    
		<tr class="user-first-name-wrap">
			<th><label for="first_name">First Name</label></th>
			<td><input type="text" name="first_name" id="first_name" value="<?php echo $user_info->first_name?>" class="regular-text" /></td>
		</tr>

	    <tr class="user-last-name-wrap">
	        <th><label for="last_name">Last Name</label></th>
	        <td><input type="text" name="last_name" id="last_name" value="<?php echo $user_info->last_name?>" class="regular-text" /></td>
	    </tr>

	    <tr class="user-nickname-wrap">
	        <th><label for="nickname">Nickname <span class="description">(required)</span></label></th>
	        <td><input type="text" name="nickname" id="nickname" value="<?php echo $user_info->nickname?>" class="regular-text" /></td>
	    </tr>

	    <?php
		if(isset($_GET['user_id']) && $_GET['user_id'] != 'new')
		{
			?>
			<tr class="user-display-name-wrap">
				<th><label for="display_name">Display name publicly as</label></th>
				<td>
					<select name="display_name" id="display_name">
						<option <?php selected($user_info->display_name, $user_info->nickname)?>>
							<?php echo $user_info->nickname?>
						</option>
						<option <?php selected($user_info->display_name, $user_info->first_name)?>>
							<?php echo $user_info->first_name?>
						</option>
						<option <?php selected($user_info->display_name, $user_info->last_name)?>>
							<?php echo $user_info->last_name?>
						</option>
						<option <?php selected($user_info->display_name, $user_info->first_name . ' ' . $user_info->last_name)?>>
							<?php echo $user_info->first_name . ' ' . $user_info->last_name?>
						</option>
						<option <?php selected($user_info->display_name, $user_info->last_name . ' ' . $user_info->first_name)?>>
							<?php echo $user_info->last_name . ' ' . $user_info->first_name?>
						</option>
					</select>
				</td>
			</tr>
			<?php
			}
			?>
	<tr><td><h2>Contact Info</h2></td></tr>

		<tr class="user-email-wrap">
			<th>
	        	<label for="email">Email <span class="description">(required)</span></label>
	        </th>
			<td>
	        	<input type="email" name="email" id="email" value="<?php echo $user_info->user_email?>" class="regular-text ltr" />
			</td>
		</tr>

		<tr class="user-url-wrap">
			<th>
	        	<label for="url">Website</label>
	        </th>
			<td>
	        	<input type="url" name="url" id="url" value="<?php echo $user_info->user_url?>" class="regular-text ltr" />
	        </td>
		</tr>

	<tr><td><h2>About the user</h2></td></tr>

		<tr class="user-description-wrap">
			<th>
	        	<label for="description">Biographical Info</label>
	        </th>
			<td>
	        	<textarea name="description" 
	            		  id="description" 
	                      rows="5" 
	                      cols="30"><?php echo trim($user_info->description);?>
	            </textarea>
				<p class="description">Share a little biographical information to fill out your profile.</p>
	            <p class="description">This may be shown publicly.</p>
	        </td>
		</tr>

	<tr><td><h2>Account Management</h2></td></tr>

		<tr id="pass" class="user-pass1-wrap">
			<th><label for="password">Password</label></th>
			<td>
				<input class="text" value="****************" id="password" name="password"/>
				<button type="button" 
	            		class="button button-secondary wp-generate-pw hide-if-no-js"
	                    onClick="javascript:write_pass()">
	            	Generate New Password
	            </button>
			</td>
		</tr>
		
	<tr><td><h2>User Account</h2></td></tr>

	<tr><td>&nbsp;</td><td>

		<?php
			$content = $adv_custom_flds->get_fields(4761);

			foreach($content as $fld_id => $fld_data)
			{
				echo "<tr>";
				echo "    <th><label for='acf-" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
	      		echo "    <td>" . $adv_custom_flds->build_field($fld_id, $_GET['user_id']) . "</td>";
				echo "</tr>";
			}
		?>

	</td></tr>

	<tr><td><h2>User Contact</h2></td></tr>

		<?php
			$content = $adv_custom_flds->get_fields(4860);
			
			foreach($content as $fld_id => $fld_data)
			{
				echo "<TR>";
				echo "<th><label for='acf-" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
	        	echo "<td>" . $adv_custom_flds->build_field($fld_id, $_GET['user_id']) . "</td>";
				echo "</TR>";				
			}
		?>

	<tr><td><h2>Contact Preference</h2></td></tr>

		<?php
			$content = $adv_custom_flds->get_fields(21681);
			
			foreach($content as $fld_id => $fld_data)
			{
				echo "<TR>";
				echo "<th><label for='acf-" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
	        	echo "<td>" . $adv_custom_flds->build_field($fld_id, $_GET['user_id']) . "</td>";
				echo "</TR>";				
			}
		?>
	</td></tr>

	</table>

	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $_GET['user_id']?>" />

	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Update User"  /></p>

</form>

<script>
	jQuery(document).ready(function()
	{
		var source = jQuery('#image_src_field_56f882d3ccb2a').attr('src');
		jQuery('#header_user_image').attr('src', source);

		jQuery('#image_src_field_56f882d3ccb2a').on('load', function()
		{
			var source = jQuery('#image_src_field_56f882d3ccb2a').attr('src');
			jQuery('#header_user_image').attr('src', source);
		});
	});
	
	function write_pass()
	{
		// alert('hello');
		document.getElementById('password').value = '<?php echo wp_generate_password()?>';	
	}
</script>
