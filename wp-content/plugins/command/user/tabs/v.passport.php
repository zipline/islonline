<?php
/*
Template Name: User Flight
*/

//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "team.support.class.php");
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "tabs.class.php");

wp_enqueue_script('jquery-ui-datepicker');
// wp_enqueue_script('command-date', CC_JS_PATH . 'dates2.js', 'jquery-ui-datepicker', '4.6.5');
 wp_enqueue_script('command-date', CC_JS_PATH . 'datepickers-generic.js', 'jquery-ui-datepicker', '4.6.5');
// wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
//wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

$team = new TEAM();
$support = new TEAM_SUPPORT();
$rsvp = new RSVP();
$page = new TAB_MANAGE();


$rsvp->set_rsvp_id($_REQUEST['rsvp']);
$rsvp->fetch_rsvp();

$team->set_team($_REQUEST['team_id']);
$prep_ready = $support->status_value($team->value('status'), 'prep');
$prep_ready = true;

$reservation = $rsvp->get_rsvp();


if(isset($_FILES) && isset($_FILES['image']))
{
	if(!empty($_FILES['image']['tmp_name']))
	{
		global $wpdb;
		$success = true;
		$errors = array();
		$image_name = $_FILES['image']['name'];
		//error_log(print_r($_FILES,true));
		
		/** filesize logic */
		$acceptable_filetypes = array('image/jpeg','image/png','image/gif');
		if(!in_array($_FILES['image']['type'],$acceptable_filetypes))
		{
			/** error for image not being correct filetype */
			$success = false;
			//error_log("File is not an jpeg/gif/png image");
			$errors[] = "The file you uploaded is not an accepted image type. Please upload a jpeg, gif, or png file.";
		}

		if($_FILES['image']['size'] > (1024*1024*8))
		{
			/** error for filesize too large */
			$success = false;
			//error_log("File size is > 100000kB");
			$errors[] = "Maximum file size is 8MB. Please use a smaller image.";
		}

		$base_image = wp_get_image_editor($_FILES['image']['tmp_name']);

		if( ! is_wp_error( $base_image ))
		{
			$ratio = (1025 * 1024) / $_FILES['image']['size'];
			$tmp_filename = date('Ymdhns') . rand(100000, 999999);
			$tmp_filepath = WP_CONTENT_DIR . '/user-pics/';
			//echo $ratio;

			$base_image_size = $base_image->get_size();
			$base_image->resize($base_image_size['width'] * $ratio, $base_image_size['height'] * $ratio, false);
			$mod_image = $base_image->save($tmp_filepath . $tmp_filename);
		}
		else
		{
			$success = false;
			$errors[] ="The file you uploaded cannot be manipulated by our site.";
		}

		if($success)
		{
			$image = base64_encode(file_get_contents($mod_image['path']));
			$wpdb->update($wpdb->prefix . 'command_RSVP', array('passport' => $image), array('post_id' => $reservation['post_id']));
			
			update_post_meta($reservation['post_id'], '_cmd_passport_photo', $image_name);
		}
		else
		{
			$wpdb->update($wpdb->prefix . 'command_RSVP', array('passport' => ''), array('post_id' => $reservation['post_id']));
			
			delete_post_meta($reservation['post_id'], '_cmd_passport_photo');
		}

		//
		//Delete the temp file
		if(!empty($mod_image))
		{
			unlink($mod_image['path']);
		}
	
	}
}

if(!empty($_POST['passport_num']))
{
	update_post_meta($reservation['post_id'], '_cmd_passport_num', $_POST['passport_num']);	
}
if(!empty($_POST['passport_country']))
{
	update_post_meta($reservation['post_id'], '_cmd_passport_country', $_POST['passport_country']);	
}
if(!empty($_POST['passport_issue']))
{
	update_post_meta($reservation['post_id'], '_cmd_passport_issue', $_POST['passport_issue']);	
}
if(!empty($_POST['passport_expire']))
{
	update_post_meta($reservation['post_id'], '_cmd_passport_expire', $_POST['passport_expire']);	
}

$number = get_post_meta($reservation['post_id'], '_cmd_passport_num', true);
$country = get_post_meta($reservation['post_id'], '_cmd_passport_country', true);
$issue = get_post_meta($reservation['post_id'], '_cmd_passport_issue', true);
$expire = get_post_meta($reservation['post_id'], '_cmd_passport_expire', true);
$img_name = get_post_meta($reservation['post_id'], '_cmd_passport_photo', true);

if(isset($issue) && !empty($issue))
{
	if(!date_create_from_format("m-d-Y", $issue))
	{
		if(strpos($issue, "-"))
		{
			$temp_date = date_create_from_format('Y-m-d', $issue);
		}
		else
		{
			$temp_date = date_create_from_format('m/d/Y', $issue);
		}
		
		if($temp_date)
		{
			$issue = $temp_date->format('m/d/Y');
		}
	}
}

if(isset($expire) && !empty($expire))
{
	if(!date_create_from_format("m-d-Y", $expire))
	{
		if(strpos($issue, "-"))
		{
			$temp_date = date_create_from_format('Y-m-d', $expire);
		}
		else
		{
			$temp_date = date_create_from_format('m/d/Y', $expire);
		}
		
		if($temp_date)
		{
			$expire = @$temp_date->format('m/d/Y');
		}
	}
}

function data_uri($reservation)
{
	global $wpdb;
	$img_name = get_post_meta($reservation['post_id'], '_cmd_passport_photo', true);
	
	$query = "SELECT passport FROM " . $wpdb->prefix . "command_RSVP WHERE id=" . $_REQUEST['rsvp'];
	$image = $wpdb->get_var($query);

	//$contents = file_get_contents($file);
  	//$base64   = base64_encode($image); 
  	return ('data: image/jpeg; base64,' . $image);	
}

?>

<?php
if($prep_ready)
{

if($rsvp->has_passport($_REQUEST['rsvp']))
{ ?>
<script type="text/javascript">
	parent.complete_item("passport_link");
</script>
<?php
}
?>
<div name="pimg" id="pimg">

    <form name="picupload" class="u_info" method="POST" id="picupload" enctype="multipart/form-data" action="<?php echo $page->GET_path(array('save'));?>">
    <table>
    	<tr><td><h2>Passport Information</h2></td></tr>
        <TR>
            <Th>Passport Number</TH>
            <td><input type="text" name="passport_num" id="passport_num" size="30" value="<?php _e($number);?>"></TD>
        </tr>
        <tr>
            <Th>Passport Country</Th>
            <td><input type="text" name="passport_country" id="passport_country" size="30" value="<?php _e($country);?>"></TD>
        </TR>
        <TR>
            <TH>Passport Issue Date</TH>
            <TD><input type="text" class="regular-text datepicker" name="passport_issue" id="passport_issue" size="30" value="<?php _e($issue);?>"></TD>
        </tr>
        <tr>
            <TH>Passport Expiration</TH>
            <TD><input type="text" class="regular-text datepicker" name="passport_expire" id="passport_expire" size="30" value="<?php _e($expire);?>"></TD>
        </TR>
        
        <TR>    
    		<TD style="verticle-align: top" colspan=5>Please upload a picture of your passport. Files must be a <B>jpeg</B>, <B>gif</B>, or <B>png</B> and no more than 8MB in size.</TD>
        </TR>

        <TR>    
    	<TD style="verticle-align: top" colspan=4>
            <div class="file-input-container button secondary">
                Upload file
    			<input name="image" id="image" type="file" title="" onchange="javascript:document.getElementById('picupload').submit();upload_image_progress();"/>
            </div>
	    </TD>
        </TR>
		
        <TR>
            <!-- <TD style="verticle-align: top">Image </TD> -->

    		<?php
    		if(!empty($errors))
    		{
    			echo "<TD colspan=4><div id='img_space'>";
    			foreach($errors as $error)
    			{
    				echo "<P style='color:red'>" . $error . "</P>";
    			}
    			echo "</div></td>";
    		}
    		else if (isset($img_name) && strlen($img_name)>0)
            {
                echo "<td colspan='4'><div id='img_space'></div><img src='" . data_uri($reservation) . "' height=400/></td>";	
            }
    		else
    		{
                echo "<td colspan='4'><div id='img_space'></div></td>";	
    		}

            ?>
        </TR>
    </table>
    <input type="hidden" name="rsvp" value="<?php _e($_REQUEST['rsvp']) ?>">
     <input class="button primary-button" type="submit" id="saving" value="Save Now">
    </form>

    <script type="text/javascript">
    	function upload_image_progress() {
    		var parent_el = document.getElementById('img_space').parentElement;
    		parent_el.innerHTML = parent_el.innerHTML + "<p>Processing. . .</p>";
    	}
    </script>

</div>

<?php
}
else
{
	echo "Once your Team is ready, you'll receive a message and be able to upload your Passport Information. You should be hearing from us soon!";	
}
?>