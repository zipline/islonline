<?php
/*
Template Name: User Flight
*/


//
//Required classes
//$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
//require_once ($plugin_path . "team.class.php");
//require_once ($plugin_path . "team.support.class.php");
//require_once ($plugin_path . "rsvp.class.php");
//require_once ($plugin_path . "tabs.class.php");

$script_path = "/wp-content/plugins/command/js/";
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_script('command-flight', $script_path . 'flight.build.js', 'jquery-ui-datepicker', '', true);

wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
//wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

//$team = new TEAM();
$support = new TEAM_SUPPORT();
//$rsvp = new RSVP();
//$page = new TAB_MANAGE();

$rsvp->set_rsvp_id($rsvp_id);
$rsvp->fetch_rsvp();

$team->set_team($_REQUEST['team_id']);
$prep_ready = $support->status_value($team->value('status'), 'prep');
$reservation = $rsvp->get_rsvp();

//
//Save data if we have it
if(!empty($_POST['saving']) && $_POST['saving']=="Save Now")
{
	$flight_info = array();

	for($i = $_POST['count_fld']; $i >= 0; $i--)
	{
		if(!empty($_POST['airline_' . $i]) && 
		   !empty($_POST['flight_' . $i]) &&
		   !empty($_POST['depart_' . $i]) &&
		   !empty($_POST['ddate_' . $i]) &&
		   !empty($_POST['arrive_' . $i]) &&
		   !empty($_POST['adate_' . $i])
		)
		{
			$flight_info[] = array('airline' => $_POST['airline_' . $i], 'flight' => $_POST['flight_' . $i], 'seat' => $_POST['seat_' . $i],
								   'depart' => $_POST['depart_' . $i], 'ddate' => $_POST['ddate_' . $i], 'dtime' => $_POST['dtime_' . $i],
 								   'arrive' => $_POST['arrive_' . $i], 'adate' => $_POST['adate_' . $i], 'atime' => $_POST['atime_' . $i]); 		
		}
	}

	$rsvp->set_flight_info($flight_info);

}

//
//Fetch data for the Flight Itinerary
$flight_data = $rsvp->get_flight_info();
?>

<?php
if($prep_ready)
{
?>
<br>
<div name="flight-info" id="flight-info">       
	<form name="flight" id="flight" method="POST" enctype = "multipart/form-data" action="<?php echo $page->GET_path(array('save'));?>">
    	<table name="flight_inf" id="flight_inf">
 			<TR>
                <TD colspan="5">
                    <input type="submit" class="button" name="saving" id="saving" value="Save Now">
                    -- OR -- &nbsp;&nbsp;  	
                    <input type="button" class="button add-new" value="Add Additional Flight Information">
                </TD>
            </TR>
			<?php
			foreach($flight_data as $index => $data)
			{
			?>	
            <TR>
                <table name="t_<?php _e(count($flight_data) - $index)?>" id="t_<?php _e(count($flight_data) - $index)?>"> 
                    <TR>
                        <td>Airline:</td>
                        <TD colspan=4><input type="text" name="airline_<?php _e($index)?>" id="airline_<?php _e($index)?>" size="40" value="<?php _e($data['airline'])?>"></TD>
                    </tr>
                    <tr>
                        <td>Flight:</td>
                        <TD colspan=4><input type="text" name="flight_<?php _e($index)?>" id="flight_<?php _e($index)?>" size="40" value="<?php _e($data['flight'])?>"></TD>
                    </tr>
                    <tr>
                        <td>Seat:</td>
                        <TD colspan=4><input type="text" name="seat_<?php _e($index)?>" id="seat_<?php _e($index)?>" size="40" value="<?php _e($data['seat'])?>"></TD>
                    </TR>
                    <TR>    
                        <TD>Departing:</TD>
                        <TD colspan=4><input type="text" name="depart_<?php _e($index)?>" id="depart_<?php _e($index)?>" size="30" placeholder="Which City / State / Country" value="<?php _e($data['depart'])?>"></TD>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <TD colspan=2>Date:<input type="text" class="datepic" name="ddate_<?php _e($index)?>" id="ddate_<?php _e($index)?>" size="33" value="<?php _e($data['ddate'])?>"></TD>
                        <TD colspan=2>Time:<input type="text" name="dtime_<?php _e($index)?>" id="dtime_<?php _e($index)?>" size="33" value="<?php _e($data['dtime'])?>"></TD>
                    </TR>
                    <TR>
                        <TD>Arriving:</TD>
                        <TD colspan=4><input type="text" name="arrive_<?php _e($index)?>" id="arrive_<?php _e($index)?>" size="30" placeholder="Which City / State / Country" value="<?php _e($data['arrive'])?>"></TD>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <TD colspan=2>Date:<input type="text" class="datepic" name="adate_<?php _e($index)?>" id="adate_<?php _e($index)?>" size="33" value="<?php _e($data['adate'])?>"></TD>
                        <TD colspan=2>Time:<input type="text" name="atime_<?php _e($index)?>" id="atime_<?php _e($index)?>" size="33" value="<?php _e($data['atime'])?>"> </TD>
                    </TR>
                </table>
            </TR>
			<?php
			}
			?>
            
        </table>
        
        <br />
        <input type="hidden" name="count_fld" id="count_fld" value="<?php _e(count($flight_data))?>">
        <input type="hidden" name="rsvp" value="<?php _e($_REQUEST['rsvp']) ?>">

    </form>
</div>

<?php
}
else
{
	echo "Once your Team is ready, you'll receive a message letting you know everything is good on our side. <BR />";
	echo "At that point in time, you should purchase your flights and setup travel for your trip. <BR />";
	echo "DO NOT make any purchases until you hear from us that it's time. <BR />";
	echo "You should be hearing from us soon!";	
}
?>

<?php wp_footer() ?>