<?php
//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "docs.class.php");
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "message.admin.class.php");

wp_enqueue_script('cmd_document_signature', plugins_url() . '/command/js/doc.sig.js', array('jquery'));	
wp_localize_script('cmd_document_signature', 'svars', array('ajax' => admin_url('admin-ajax.php')));

wp_enqueue_script("cmd_doc_redirects", CC_JS_PATH . 'user.docs.js', NULL, "0.3");
wp_localize_script('cmd_doc_redirects', 'svars', array( 'banner' => 0,
													    'user' => 1,
														'ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form')
														));

wp_enqueue_script("cmd_docs_user", CC_JS_PATH . 'user.hidden.docs.js', NULL, "0.3");
wp_localize_script('cmd_docs_user', 'svars', array( 'banner' => 0,
													    'user' => 1,
														'ajax_url' => admin_url('admin-ajax.php'),
														'upload_url' => admin_url('async-upload.php'),
														'nonce' => wp_create_nonce('media-form')
														));


$user_id = isset($_REQUEST['user_id'])? $_REQUEST['user_id'] : null;

$docs = new DOCS($user_id);
$team = new TEAM();
$rsvp = new RSVP();

$rsvp_id = isset($_REQUEST['rsvp_id']) ? $_REQUEST['rsvp_id'] : $_REQUEST['rsvp'];
$team->set_team($rsvp->team($rsvp_id));
$docs->post_id($team->value('post'));
$docs->fetch(true, false);
$docs->fetch_read();

//
//Add custom document
//
//2018-04-04 DJT Changed from isset to empty test
//if(isset($_POST['wp_custom_attachment']))
if(!empty($_POST['wp_custom_attachment']))
{
	$docs->post_id($team->value('post'));
	$docs->set_user($user_id);
	$docs->add_user_list( array($_POST['wp_custom_attachment']) );

			$event ="user_document_added";
			$admin_msg = new ADMIN_MESSAGES();
			$admin_msg->set_event(200);
			$admin_msg->load();
			$message = new MESSAGES();
			$message->set_message_text($admin_msg->get_message());
			$message->set_subject($admin_msg->get_subject());
			$message->set_recipient($user_id);
			$message->set_team_id($team->get_team());
			$message->set_sender(1);
			$message->add_message($event);
}
	
//
//Add custom document
//
//2018-04-04 DJT Changed from isset to empty test
//if(isset($_POST['wp_user_attachment']))
if(!empty($_POST['wp_user_attachment']))
{
	$docs->post_id($team->value('post'));
	$docs->set_user($user_id);
	$docs->add_user_list( array($_POST['wp_user_attachment']) );
	

}


if(isset($_REQUEST['uncheck_docs']))
{
	$read_docs = $docs->list_read();
	foreach($read_docs as $d_key => $sig)
	{
		if(!isset($_REQUEST['doc_check_'.$d_key]) && !empty($d_key))
		{
			$event ="volunteer_document_unchecked";
			$admin_msg = new ADMIN_MESSAGES();
			$admin_msg->set_event($event);
			$admin_msg->load();
			$docs->remove_read_document($d_key);
			$message = new MESSAGES();
			$message->set_message_text($admin_msg->get_message());
			$message->set_recipient($user_id);
			$message->set_team_id($team->get_team());
			$message->set_sender(1);
			$message->add_message($event);
		}
	}
}

$docs->set_custom(true);
$docs->fetch(true, true);
$docs->fetch_read();

?>
    <div id="content" class='v-tab'>

		<form method="POST" id="docs_form" action="<?php echo $page->GET_path(); ?>" >
		<?php
		foreach($docs->team_docs() as $doc_id => $doc_title)
        {
            $doc_type = get_post_meta($doc_id, 'resource_type', true);
			$signature = get_post_meta($doc_id, 'signature_required', true);
			$classname = '';
			if(empty($signature) || $signature == false)
			{
				$classname = "class='no_req_check' name='" . $doc_id . "'";
			}
			
            echo "<div class='document' id='" . $doc_id . "'>";
			echo "<input type='checkbox' name='doc_check_" . $doc_id . "' id='doc_check_" . $doc_id . "' " . ($docs->is_read($doc_id)  ? 'checked' : '') . ">&nbsp;&nbsp;";
			
			switch($doc_type)
			{
				case 'HTML Page':
					echo "<a href='' onclick=\"window.open('read-info-docs?doc_id=" . $doc_id . "&rsvp=" . $rsvp_id . "&post_id=" . $team->value('post') . "', 'newwindow', 'width=800', 'height=800')\" " . $classname . ">" . $doc_title . "</a>";
					break;
				case 'URL':
					$url = get_post_meta($doc_id, 'url', true);
					echo "<a href='" . $url . "' " . $classname . " id='doc_check_" . $doc_id . "' target='_blank'>" . $doc_title . "</a>";
					break;
				case 'PDF Document':
					$pid = get_post_meta($doc_id, 'pdf_document', true);
					echo "<a href='" . get_the_guid($pid) . "'  " . $classname . " target='_blank'>" . $doc_title . "</a>";
					break;
				default:
					echo "<a href='" . get_the_guid($doc_id) . "'  " . $classname . " target='_blank'>" . $doc_title . "</a>";
					break;
			}
			
            echo "</div><BR>\r\n";
    
        }
		echo "<input type='hidden' name='post_id' id='post_id' value='" . $team->value('post') . "'>";

        ?>
				<input type="submit" name="uncheck_docs" id="uncheck_docs" value="Uncheck Selected Docs" class="button button-primary" />
		<BR>
		<HR>	
		<?php

		echo '<div id="wp_cust_att"><input type="text" id="wp_custom_attachment" name="wp_custom_attachment" value="" size="25" /></div>';
		echo '<div id="wp_user_att"><input type="text" id="wp_user_attachment" name="wp_user_attachment" value="" size="25" /></div>';
     
		?>
			</form>
    </div> <!-- end #content --> 
 
 <?php wp_footer(); ?>