<?php

wp_register_script('jquery-ui-autocomplete', '/wp-includes/js/jquery/ui/jquery.ui.autocomplete.min.js', array('jquery'));
wp_enqueue_script('cmd_date_selector', plugins_url() . '/command/js/dates.js', array('jquery', 'jquery-ui-autocomplete'), NULL, true );

if(!empty($_POST['action']) && $_POST['action']=="update")
{
	$fld_list = $_POST;

	foreach($_POST as $temp=>$val)
	{
		if($temp == "acf")
		{
			foreach($val as $index=>$value)
			{
				//
				//ACF field so handle differently
				if(substr($index,0,6) == "field_" && !empty($fld_list['acf'][$index]))
				{
					$adv_custom_flds->save_field_group($index, $fld_list['acf'], $_GET['user_id'], $post_id, true);
				}
			}
		}
		
		if($temp == 'amb_status')
		{
			update_user_meta($_GET['user_id'], '_cmd_comm_ambassador_status', $val);

			switch($val)
			{
				case 'true':
					if($user->user_group($user_info->ID) == 'volunteer')
					{
						$user_updates = array('ID' => $_GET['user_id'],
											  'role' => 'enhanced');
						wp_update_user($user_updates);
						
						//
						//Make ISL Rep Capability
						update_user_meta($_GET['user_id'], 'cmd_isl_rep', 'true');
					}
					break;
				case 'expire':
					update_user_meta($_GET['user_id'], '_cmd_comm_ambassador_edate', date('m-d-Y'));

				case 'false':
				case 'reject':
					delete_user_meta($_GET['user_id'], 'cmd_isl_rep');
					
					if($user->user_group($_GET['user_id']) == 'enhanced')
					{
						$caps = 0;
						$user->set_group(1);
						foreach($user->cap_details as $cap_id => $cap_details)
						{
							if($user->has_cap($_GET['user_id'], $cap_details['kode']))
							{
								$caps++;
							}

						}

						if(empty($caps))
						{
							$user_updates = array('ID' => $_GET['user_id'],
												  'role' => 'volunteer');
							wp_update_user($user_updates);
						}
					}
					
					break;
			}
		}

		if($temp == 'amb_code')
			update_user_meta($_GET['user_id'], '_cmd_comm_ambassador_cap_code', $val);
		
		if($temp == 'amb_sdate')
			update_user_meta($_GET['user_id'], '_cmd_comm_ambassador_request_date', $val);
		
		if($temp == 'amb_edate')
			update_user_meta($_GET['user_id'], '_cmd_comm_ambassador_edate', $val);
		
	}
}

$post_id = get_user_meta($_GET['user_id'], '_cmd_comm_ambassador_post', true);
$status = get_user_meta($_GET['user_id'], '_cmd_comm_ambassador_status', true);
$cap_code = get_user_meta($_GET['user_id'], '_cmd_comm_ambassador_cap_code', true);
$sdate= get_user_meta($_GET['user_id'], '_cmd_comm_ambassador_request_date', true);
$edate= get_user_meta($_GET['user_id'], '_cmd_comm_ambassador_edate', true);

if(empty($cap_code))
{
	global $wpdb;

	$usr = get_user_by("ID", $_GET['user_id']);
	$base_code = strtolower(substr($usr->last_name,0,3) . substr($usr->first_name,0,3));
	$base_query = 'SELECT user_id FROM ' . $wpdb->prefix . 'usermeta WHERE meta_key = "_cmd_comm_ambassador_cap_code" and meta_value=';
	$base_inc = '';
	
	while ($wpdb->get_var($base_query . '"' . $base_code . $base_inc . '"') != null)
	{
		$base_inc++;
	}
	$cap_code = $base_code . $base_inc;
}

if(empty($edate))
{
	if(!empty($sdate))
	{
		$start_date = date_create_from_format('m-d-Y', $sdate);
	}
	else
	{
		$start_date = new DateTime('now');
	}
	$edate = date('m-d-Y',  $start_date->getTimestamp() + (60 * 60 * 24 * 365.25));
}
?>



<form class="u_info" name="u_info" method = "POST" action="<?php $page->GET_path(array('save'))?>">

	<table>

		<?php
			echo "<TR>";
		  	echo '<TH><label for="amb_status">Status</label></TH>';
		  	echo '  <TD>';
		  	echo '  <select id="amb_status" name="amb_status">';
		  	echo '    <option value="false"' . (($status=='false') ? ' selected' : '') . '>Pending</option>';
		  	echo '    <option value="true"' . (($status=='true') ? ' selected' : '') . '>Accepted</option>';
		  	echo '    <option value="reject"' . (($status=='reject') ? ' selected' : '') . '>Rejected</option>';
		  	echo '    <option value="expire"' . (($status=='expire') ? ' selected' : '') . '>Expired</option>';
		  	echo '  </select>';
		  	echo '  </TD>';
		  	echo '</TR>';
			echo "<TR>";
		  	echo '<TH><label for="amb_code">CAP Code</label></TH>';
		  	echo '<TD><input type="text" id="amb_code" name="amb_code" value="' . $cap_code . '"></input></TD>';
		  	echo '</TR>';
			echo "<TR>";
		  	echo '<TH><label for="amb_sdate">Effective Date</label></TH>';
		  	echo '<TD><input type="text" id="amb_sdate" name="amb_sdate" class="datepick" value="' . $sdate . '"></input></TD>';
		  	echo '</TR>';
			echo "<TR>";
		  	echo '<TH><label for="amb_edate">Expiration</label></TH>';
		  	echo '<TD><input type="text" id="amb_edate" name="amb_edate" class="datepick" value="' . $edate . '"></input></TD>';
		  	echo '</TR>';
			
			$content = $adv_custom_flds->get_fields(35181);
			
			foreach($content as $fld_id => $fld_data)
			{
				echo "<TR>";
				echo "<th><label for='" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
	        	echo "<td>" . $adv_custom_flds->build_field($fld_id, $_GET['user_id'], $post_id, $fld_data) . "</td>";
				echo "</TR>";				
			}
		?>

	</table>

	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $_GET['user_id']?>" />

	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Update User"  /></p>

</form>
