<?php
/*
Template Name: User Flight
*/

//
//Required classes
//$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
//require_once ($plugin_path . "team.class.php");
//require_once ($plugin_path . "team.support.class.php");
//require_once ($plugin_path . "rsvp.class.php");
//require_once ($plugin_path . "tabs.class.php");

$script_path = "/wp-content/plugins/command/js/";
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_script('command-flight', $script_path . 'flight.build.js', 'jquery-ui-datepicker', '0.1', true);
wp_localize_script('command-flight', 'vars', array('to' => $team->value('arrival_city'), 'from' => $team->value('departure_city')));

wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
//wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

//$team = new TEAM();
$support = new TEAM_SUPPORT();
//$rsvp = new RSVP();
//$page = new TAB_MANAGE();

$rsvp->set_rsvp_id($rsvp_id);
$rsvp->fetch_rsvp();

$team->set_team($team_id);
$prep_ready = $support->status_value($team->value('status'), 'prep');
$traveled = $support->status_value($team->value('status'), 'trav');
$reservation = $rsvp->get_rsvp();

//
//Save data if we have it
if(!empty($_POST['saving']) && $_POST['saving']=="Save Now")
{
	$flight_info = array();
	
	
	//
	//Sequence into Travel to (direction 0) and Trevel from (direction 1)
	for($i = $_POST['count_fld']; $i >= 0; $i--)
	{
		if(isset($_POST['direction_' . $i]) && $_POST['direction_' . $i] == 0)
		{
			if(!empty($_POST['airline_' . $i]) && 
			   !empty($_POST['flight_' . $i]) &&
			   !empty($_POST['depart_' . $i]) &&
			   !empty($_POST['ddate_' . $i]) &&
			   !empty($_POST['arrive_' . $i]) &&
			   !empty($_POST['adate_' . $i])
			)
			{
				$flight_info[] = array('direction' => 0,
									   'airline' => $_POST['airline_' . $i], 'flight' => $_POST['flight_' . $i], 'seat' => 0,
									   'depart' => $_POST['depart_' . $i], 'ddate' => $_POST['ddate_' . $i], 'dtime' => $_POST['dtime_' . $i],
									   'arrive' => $_POST['arrive_' . $i], 'adate' => $_POST['adate_' . $i], 'atime' => $_POST['atime_' . $i]); 		
			}
		}
	}
	for($i = $_POST['count_fld']; $i >= 0; $i--)
	{
		if(isset($_POST['direction_' . $i]) && $_POST['direction_' . $i] == 1)
		{
			if(!empty($_POST['airline_' . $i]) && 
			   !empty($_POST['flight_' . $i]) &&
			   !empty($_POST['depart_' . $i]) &&
			   !empty($_POST['ddate_' . $i]) &&
			   !empty($_POST['arrive_' . $i]) &&
			   !empty($_POST['adate_' . $i])
			)
			{
				$flight_info[] = array('direction' => 1,
									   'airline' => $_POST['airline_' . $i], 'flight' => $_POST['flight_' . $i], 'seat' => 0,
									   'depart' => $_POST['depart_' . $i], 'ddate' => $_POST['ddate_' . $i], 'dtime' => $_POST['dtime_' . $i],
									   'arrive' => $_POST['arrive_' . $i], 'adate' => $_POST['adate_' . $i], 'atime' => $_POST['atime_' . $i]); 		
			}
		}
	}

	$rsvp->set_flight_info($flight_info);
	$flight_fees = array('arrive' => !empty($_POST['off_arrival']) ? "checked" : '',
				 		 'depart' => !empty($_POST['off_depart']) ? "checked" : '');
	$rsvp->set_flight_fees($flight_fees);

}

//
//Fetch data for the Flight Itinerary
$flight_data = $rsvp->get_flight_info();
$flight_fees = $rsvp->get_flight_fees();

?>
<div id="content" class='v-tab'>
<div name="flight-info" id="flight-info">  
<?php
if($prep_ready || $traveled)
{
?>
	<form name="flight" id="flight" method="POST" enctype = "multipart/form-data" action="<?php echo $page->GET_path(array('save'));?>">
    	<table name="flight_information" id="flight_information">
 			<TR>
                <TD colspan="5">
                    <input type="submit" class="button" name="saving" id="saving" value="Save Now">
                    -- OR -- &nbsp;&nbsp;  	
                    <input type="button" class="button add-segment" value="Add Additional Flight Information">
                </TD>
            </TR>
			<TR><TD>Late or Early Arrival</TD><TD><input type="checkbox" name="off_arrival" id="off_arrival" <?php echo $flight_fees['arrive']?>></TD></TR>
			<TR><TD>Late or Early Departure</TD><TD><input type="checkbox" name="off_depart" id="off_depart" <?php echo $flight_fees['depart']?>></TD></TR>
			<?php
			foreach($flight_data as $index => $data)
			{
			?>	
            <TR>
                <table name="t_<?php _e(count($flight_data) - $index)?>" id="t_<?php _e(count($flight_data) - $index)?>"> 
					<tr>
						<th><label>Direction:</label></th>
						<td>
							<select name="direction_<?php _e(count($flight_data) - $index)?>" id="direction_<?php _e(count($flight_data) - $index)?>" required>
								<option value="0" <?php echo isset($data['direction']) && $data['direction'] == 0 ? " selected" : ""?>>Travel To <?php echo $team->value('arrival_city')?></option>
								<option value="1" <?php echo isset($data['direction']) && $data['direction'] == 1 ? " selected" : ""?>>Travel From <?php echo $team->value('departure_city')?></option>
							</select>
						</td>
					</tr>
	                <TR>
                        <td>Airline:</td>
                        <TD colspan=4><input type="text" name="airline_<?php _e(count($flight_data) - $index)?>" id="airline_<?php _e(count($flight_data) - $index)?>" size="40" value="<?php _e($data['airline'])?>"></TD>
                    </tr>
                    <tr>
                        <td>Flight:</td>
                        <TD colspan=4><input type="text" name="flight_<?php _e(count($flight_data) - $index)?>" id="flight_<?php _e(count($flight_data) - $index)?>" size="40" value="<?php _e($data['flight'])?>"></TD>
                    </tr>
                    <TR>    
                        <TD>Departing:</TD>
                        <TD colspan=4><input type="text" name="depart_<?php _e(count($flight_data) - $index)?>" id="depart_<?php _e(count($flight_data) - $index)?>" size="30" placeholder="Which City / State / Country" value="<?php _e($data['depart'])?>"></TD>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <TD colspan=2>Date:<input type="text" class="datepic" name="ddate_<?php _e(count($flight_data) - $index)?>" id="ddate_<?php _e(count($flight_data) - $index)?>" size="33" value="<?php _e($data['ddate'])?>"></TD>
                        <TD colspan=2>Time:<input type="text" name="dtime_<?php _e(count($flight_data) - $index)?>" id="dtime_<?php _e(count($flight_data) - $index)?>" size="33" value="<?php _e($data['dtime'])?>"></TD>
                    </TR>
                    <TR>
                        <TD>Arriving:</TD>
                        <TD colspan=4><input type="text" name="arrive_<?php _e(count($flight_data) - $index)?>" id="arrive_<?php _e(count($flight_data) - $index)?>" size="30" placeholder="Which City / State / Country" value="<?php _e($data['arrive'])?>"></TD>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <TD colspan=2>Date:<input type="text" class="datepic" name="adate_<?php _e(count($flight_data) - $index)?>" id="adate_<?php _e(count($flight_data) - $index)?>" size="33" value="<?php _e($data['adate'])?>"></TD>
                        <TD colspan=2>Time:<input type="text" name="atime_<?php _e(count($flight_data) - $index)?>" id="atime_<?php _e(count($flight_data) - $index)?>" size="33" value="<?php _e($data['atime'])?>"> </TD>
                    </TR>
                </table>
            </TR>
			<?php
			}
			?>
            
        </table>
        
        <br />
        <input type="hidden" name="count_fld" id="count_fld" value="<?php _e(count($flight_data))?>">
        <input type="hidden" name="rsvp" value="<?php _e($_REQUEST['rsvp']) ?>">

    </form>
    <?php
    }
    else
    {
        echo "<p>Once your Team is ready, you'll receive a message letting you know everything is good on our side.</p>";
        echo "<p>At that point in time, you should purchase your flights and setup travel for your trip. </p>";
        echo "<p><strong>DO NOT make any purchases until you hear from us that it's time. </strong></p>";
        echo "<p>You should be hearing from us soon!</p>";  
    }
    ?>
</div>
</div>


<?php wp_footer() ?>