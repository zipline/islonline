<div id="main" class='v-tab'>
	<h2>Send user a message</h2>
	<form id="volunteer_message_form"method="POST" enctype="multipart/form-data" action="<?=$page->GET_path()?>">
		<input type="text" name="message_subject" placeholder="Message Subject" />
		<textarea style="width: 100%;" name="message_text" placeholder="Type your message here"></textarea>
		<input class="button button-primary" type="submit" name="submit_message" value="Send Message" />
	</form>
</div>

<div class='message-archive'>
<h3>Message Archive</h3>
<?php

//
//A2018-03-16 DJT Added for testing messages
if($_GET['user_id'] == 0)
{
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	require_once(CC_CLASSES_DIR . 'message.admin.class.php');
	
	$admin = new ADMIN_MESSAGES();
	$admin->set_event(84);
	$admin->load();
	echo stripslashes($admin->get_message());
	
	$messages = new MESSAGES();
	$messages->set_recipient(1);
	$messages->set_team_id(1745);
	$message->set_message_text(stripslashes($admin->get_message()));
	
	echo $message->replace_text();
	
	//$P$Bzx3shE2iZ.XaHRTmxdMaursDvohV51
}
	
$message = new MESSAGES();
$all_messages = $message->get_recipient_messages($_GET['user_id']);
		  
if(count($all_messages) > 0)
{
	echo "<TABLE class='standard-large-table form-table striped'>";
	echo "<TR><TH class='large'>Created</TH>
			  <TH>Subject</TH>
			  <TH>Message</TH>
			  <TH class='large'>Read</TH>
			  <TH class='large'>Team</TH>
		  </TR>";
	
	foreach($all_messages as $ind=>$data)
	{
		$team->set_team($data->team);
		
		//
		//2018-03-16 djt Added stripslashes
		echo "<TR><TD>" . str_replace(' ', '<BR>', $data->created_at) . "</TD>
				  <TD> " . $data->subject . "</TD>
				  <TD> " . stripslashes(html_entity_decode($data->msg_text)) . "</TD>
				  <TD> " . str_replace(' ', '<BR>', $data->read_at) . "</TD>
				  <TD> " . $team->value('hash') . "</TD>
			  </TR>";
		
	}
	
	echo "</TABLE>";
}

else 
if(count($all_messages) == 0) {
	echo "<p><em>No messages sent.</em></p>";
}


		  ?>