<?php
//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once $plugin_path . "team.class.php";
require_once $plugin_path . "rsvp.class.php";
require_once $plugin_path . "finance.class.php";
require_once $plugin_path . "country.class.php";
require_once $plugin_path . "message.class.php";
require_once $plugin_path . "user.class.php";

$team = new TEAM();
$rsvp = new RSVP();
$finance = new FINANCE();
$country = new COUNTRY();
$message = new MESSAGES();
$user=  new USER();

$team->set_team($team_id);
$country_list = $country->country_list($team->schedule_countries());

if(!empty($rsvp_id))
{
	$rsvp->set_rsvp_id($rsvp_id);
	$rsvp->fetch_rsvp();
	
	$reservation = $rsvp->get_rsvp();
	$item = $finance->fetch_item($reservation['order_item_id']);
	$add_ons = $finance->add_ons($reservation['order_item_id']);
	
	$all_items[] = $reservation['order_item_id'];

	$start = strtotime(str_replace("-","/",$team->arrival_date()));
	$now = time();
	if($start - $now < 0)
	{
		$history = 1;
	}
	else
	{
		$history = 0;
	}
	
	$finance->Fetch_Items(array($reservation['order_item_id']), false);
}
else
{
	?>
    	<main id="main" class='v-tab' role="main">
			<p>No Trips found for this Volunteer.</p>
         </main>      
   
    <?php	
	exit;
}

?>
    	<main id="main" class='v-tab' role="main">
			
        	<div class="row collapse">
        		<b>Country:</b> <?php _e($country_list)?><br />
        		<div class="medium-6 columns">
            		<b>Arrival Date:</b> <?php _e($team->arrival_date())?><br />
            		<b>Departure Date:</b> <?php _e($team->departure_date())?><br />
            		<b>Duration:</b> <?php _e($team->trip_duration())?> days<br />
        		</div>
        		<div class="medium-6 columns">
            		<b>Arrival City:</b> <?php _e($team->value('arrival_city'))?><br />
            		<b>Departure City:</b> <?php _e($team->value('departure_city'))?><br />
            		<b>Payments Due:</b><span name="bdue" id="bdue"></span><br />
        		</div>
			</div>
		
        	<hr />
            
            <h4>Order Details:</h4>
            <table class="order" style="border: hidden; width: 100%">
            	<tr class="border_bottom">
            		<td><?php _e($team->value('hash'))?></td>
                    <td align='right'>$<?php _e($finance->Calculate_ItemsCost())?></td>
                </tr>

                <?php
				$added_cost = 0;
				foreach($add_ons as $index => $itm)
				{
					$all_items[] = $itm['id'];
					echo "<tr class=". 'border_bottom' .">";
						echo "<td>" . $finance->Fetch_ItemVerbiage($itm['item_type'], $itm['id']) . "</td>";
						echo "<td align='right'>$" . number_format($itm['price'],2) . "</td>";
					echo "</tr>";	
					$added_cost += $itm['price'];
				}
				
				echo "<tr>";
					echo "<td><b>GRAND TOTAL</b></td>";
					echo "<td align='right'><B>$" . number_format(($finance->Calculate_ItemsCost() + $added_cost),2) . "</b></td>";
				echo "</tr>";
				?>
			</table>

            <hr />

            <!--<h4>Payments:</h4>-->
                <table class="order" style="border: hidden;width: 100%">                     
				<TR>
                	<TH style="width: 200px; text-align: left;">Payment Date</TH>
                	<TH style="width: 200px; text-align: left;">For</TH>
                	<TH style="width: 200px; text-align: left;">By</TH>
                    <TH style="width: 200px; text-align: right;">Amount</TH>
                </TR>

				<?php

				//
				//$cost['id'] = $payments['order_item_id']
				$pays = 0;
				$costs = $finance->items_cost($all_items, true);
//				echo var_dump($all_items);
				foreach($finance->payments($all_items) as $payments)
				{
					if($payments['amount'] != 0)
					{
						$item = '';
						$override_item = false;

						if(!empty($payments['special']) && ($payments['special'] == 'donate refund' || $payments['special'] == 'donate' || $payments['special'] == 'donate anyonymous'))
						{
							$item = "Donation";
							$override_item = true;
						}
						if(empty($payments['special']) && !empty($payments['note']))
						{
							$item = $payments['note'];
							$override_item = true;
						}
						if(!empty($payments['special']) && $payments['special'] == 'fee' && !empty($payments['note']))
						{
							$item = $payments['note'];
							$override_item = true;
						}


						if(!$override_item)
						{
							$item = $finance->Fetch_ItemVerbiage($costs[$payments['order_item_id']]['item_type']);
						}

						if(floatval($payments['amount']) < 0 && substr($payments['note'],0,4) != 'Spcl')
						{
							//$item = "Refund for " . $item;
							$item = $item;
						}

						$payed_by = get_user_by("ID", $payments['created_by']);
						$whom = $payed_by->first_name . " " . $payed_by->last_name;

						echo "<tr><td>" . $payments['created_when'] . "</td>
								  <td>" . $item . "</td>
								  <td>" . $whom . "</td>	
								  <td align='right'>$" . number_format($payments['amount'] * (-1),2) . "</td></tr>";
						$pays += $payments['amount'];

					}
				}
				echo "<TR>";
				echo "   <TD><B>TOTAL PAYMENTS</B></TD>";
				echo "   <TD></TD><TD></TD>";
				echo "   <TD style='text-align: right'><B>$" . number_format($pays,2) . "</B></TD>";
				echo "</TR>";
				?>
           </table>
           <BR>
	<?php
				if(!$history && ($team->trip_cost() + $added_cost - $pays) > 0)
				{
					echo "<TR><TD colspan=3><a class=" .'button'. " href='/payment/?team_id=" . $_GET['team_id'] . "&member_id=" . $_GET['user_id'] . "&rsvp=$rsvp_id" . "' target='_top'>Make a Payment now</a></TD></TR>";
				}
				
				$balance_due = number_format($finance->Fetch_Balance(false, $reservation['order_id']),2);
				?>
				<hr />
                
                <?php
				$url = site_url() . "/wp-admin/admin.php?page=cc-pops";
				$url .= "&display=finance";
				$url .= "&team=" . $team_id;
				$url .= "&member=" . $_GET['user_id'];
				$url .= "&rsvp=" . $rsvp_id;
				?>
				
				<?php
			   	$user_role = $user->user_group(get_current_user_id());
			   
			   	if($user_role == "super" || $user_role == "admin" || $user_role == "staff")
				{
					?>
					<small><a href='<?php echo $url;?>' target="_blank" class='button button-primary'>Edit Financials</a></small>
					<?php
				}
			    ?>
            
        </main>


<?php
	$bdue_label = (!empty($balance_due) ? " $" . $balance_due : " No Balance Due");

	if($bdue_label != " No Balance Due")
	{
		$tad = $team->arrival_date();
		$tda = date("m-d-Y" , mktime(0, 0, 0, substr($tad, 0, 2), substr($tad, 3, 2)-45, substr($tad, 6)));;
		$bdue_label .= " by " . $tda;
	}
?>

<script>
	document.getElementById('bdue').innerHTML = "<?php _e($bdue_label);?>";
</script>
