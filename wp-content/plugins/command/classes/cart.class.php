<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * CART Class contains all of the functionality for the shopping carts
 */
CLASS CART
{
	/** @var array $cart List of items for current shopping cart. */
	private $cart = array();
	
	/** @var string $cart_table Table usef for the cart in the database. */
	private $cart_table = NULL;
	
	/** @var string $cart_table Table usef for the cart in the database. */
	private $items_type_table = NULL;
	
	/** @var string $cart_code Alpha-numeric identifier for the cart. */
	private $cart_code = NULL;
	
	/** @var string $cart_code Alpha-numeric identifier for the cart. */
	private $item_types = NULL;

	//
	//2018-02-21 DJT Changed from 1 or 0 to boolean with true indicating md5 used and false indicating user_id
	//
	/** @var boolean $md5 Boolean to use md5 or the user id. */
	private $md5 = 0;							// Required. md5 (0) or user id (1)
	
	/** @var string $temp Message text */
	private $temp = '';
	
	/** @var integer $user The id of the user */
	private $user = NULL;

	private $backtrace = false;
	private $jq_cookie = true;
	
	public function __construct( $Cookie = false, $Force_user = 0 )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;
		
		//
		//Set core variables
		$this->cart_table = $wpdb->prefix . 'command_cart';
		$this->items_type_table = $wpdb->prefix . 'command_order_item_types';
		$this->cart = array();

		//
		//Get items information
		self::fetch_ItemTypes();
		
		//
		//Set user information
		if($Force_user > 0)
		{
			$this->user = $Force_user;
		}
		else
		{
			if( is_user_logged_in() )
			{
				$this->user = get_current_user_id();
			}
			else
			{
				$this->user = 0;
			}
		}
		
		//
		//2018-02-21 DJT Changed from complex isset check to basic empty check. Now usingi booleans instead of numbers
		//
		//Set MD5 based on user
		//$this->md5 = ( isset( $this->user ) && $this->user > 0 ? 1 : 0);
		$this->md5 = ( empty( $this->user ) ? true : false);
		
		if( !$this->md5 )
		{
			self::check_merge();
		}

		//
		//Check if there is a current cart and set up the info
		self::get_cart_code( $Cookie );
		self::fetch_Cart();
	}

	/**
	 * Returns the current message
	 *
	 * @return string The message.
	 */
	public function message()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		return $this->temp;
	}

	public function Money($Amount)
	{
		setlocale(LC_MONETARY, 'en_US');
		return (money_format('%+.2n', $Amount));
	}
	
	///////////////////////////////////////////////////////////////////////////////////
	//
	// Functions to deal with items in the cart
	//
	///////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */
	public function add_RSVP( $TeamID, $Cost, $Quantity )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		self::add_item( $TeamID, $TeamID, self::Type( 'Reservation' ), $Cost, $Quantity );
	}
	
	
	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */
	public function add_AddOn( $TeamID, $AddOnID, $Cost, $Quantity )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		self::add_item( $TeamID, $AddOnID, self::Type( 'Add-on' ), $Cost, $Quantity );
	}
	
	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */
	public function add_Fees( $TeamID, $Verbiage, $Cost, $Quantity )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		self::add_item( $TeamID, $TeamID, self::Type( 'Fees' ), $Cost, $Quantity, $Verbiage );
	}

	
	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */
	public function add_CAP( $UserID )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;

		$settings = get_option( 'cmd_center' );

		if( !empty( $settings['campus_ambassador_fee'] ) )
		{
			$cap_reduce = $settings['campus_ambassador_fee'];
		}
		else
		{
			$cap_reduce = 0;
		}
		
		//
		//Check for proper CapCode
		if(!get_user_meta($UserID, '_cmd_comm_ambassador_status', true) == 'true')
		{
			$cap_reduce = 0;
		}

		//
		//If there's a cap reduction, then we can add it to everything
		if( $cap_reduce > 0 )
		{
			//
			//Cycle through cart for each main item 
			foreach( $this->cart as $main_item_id => $main_item_types )
			{
				//
				//Cycle through each items connected types
				foreach( $main_item_types as $this_type_id => $this_type_data )
				{
					//
					//If the main item type is 1, then we can add a fee
					if( $this_type_id == 1 )
					{
						self::add_item( $main_item_id, $UserID, self::Type( 'CAP' ), $cap_reduce * (-1), $this_type_data[$main_item_id]['quantity'] );
					}
				}
			}
		}
	}
	
	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */
	public function add_Coupon( $CouponCode, $ClearPrior = true, $Override_DateUses = false )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;
		$discount = 0;
		$quantity = 0;

		//
		//Find all flavors
		$coupon_args = array(
				'posts_per_page'  	=> 1,
				'post_type'       	=> 'isl_coupon',
				'fields'          	=> 'ids',
				'meta_key'			=> 'code',
				'meta_value'		=> $CouponCode,
				'post_status'		=> 'publish'
			  );
		$coupon_id = get_posts( $coupon_args );

		//
		//If we found a coupon...
		if( !empty( $coupon_id ) )
		{
			$coupon_post_id = current( $coupon_id );
			$coupon_info = get_fields( $coupon_post_id );
			if($ClearPrior == true)
			{
				//
				//Cycle through cart for each main item 
				foreach( $this->cart as $main_item_id => $main_item_types )
				{
					unset($this->cart[$main_item_id][self::Type( 'coupon' )] );
				}
			}
			
			//
			//If the coupon have information about it...
			if( !empty( $coupon_info ) )
			{
				//
				//Cycle through cart for each main item 
				foreach( $this->cart as $main_item_id => $main_item_types )
				{
					//
					//Cycle through each items connected types
					foreach( $main_item_types as $this_type_id => $this_type_data )
					{
						//
						//If we have a legitiatmate item...						
						if( $this_type_id == self::Type( 'Reservation' ) ||
						    $this_type_id == self::Type( 'Service' ) ||
						    $this_type_id == self::Type( 'Merchandise' ) ||
						    $this_type_id == self::Type( 'Add-on' )
						  )
						{
							//
							//If there is a straight dollar discount...
							if( !empty($coupon_info['discount']) && substr( $coupon_info['discount'], 0, 1 ) == "$" )
							{
								$discount = floatval(preg_replace("/[^0-9.]/", "", substr( $coupon_info['discount'], 1 )));
								$quantity = 1;
							}

							//
							//If there is a percentage discount...
							if( !empty($coupon_info['discount']) && substr( $coupon_info['discount'], -1 ) == "%" )
							{
								$percent = floatval(preg_replace("/[^0-9.]/", "", substr( $coupon_info['discount'], 0, -1 )));

								$discount = $this_type_data[$main_item_id]['cost'] * $percent / 100;
								$quantity = $this_type_data[$main_item_id]['quantity'];
							}

							//
							//If there's a possible discount, then check other parameters...
							if( $discount > 0 )
							{
								//
								//Check uses
								$query = 'SELECT count(*) 
										  FROM ' . $wpdb->prefix . 'command_order_items 
										  WHERE item_type=' . self::Type( 'Coupon' ) . ' AND 
										  		item_id = ' . $coupon_post_id;
								$uses = $wpdb->get_var( $query );

								if( $uses < $coupon_info['uses'] || $coupon_info['uses'] == 0 || $Override_DateUses )
								{
									//
									//Check expiration date...
									unset($expir_date);
									date_default_timezone_set('America/Los_Angeles');
									$poss_expir_date = date_create_from_format( 'm/d/Y', $coupon_info['expiration'] );
									
									if(!empty($poss_expir_date))
									{
										$expir_date = $poss_expir_date->getTimestamp();
									}
									
									if( empty( $coupon_info['expiration'] ) || $expir_date > time() || $Override_DateUses)
									{
										//
										//Check teams
										if( empty( $coupon_info['team']) )
										{
											self::set_item( $main_item_id, $coupon_post_id, self::Type( 'Coupon' ), $discount * (-1), $quantity );
										}
										else
										{
											foreach( $coupon_info['team'] as $index => $team_data )
											{
												//
												//Find team post to match with team_data->ID
												$team_post = $wpdb->get_var('SELECT post_id FROM ' . $wpdb->prefix . 'command_TEAM WHERE id=' . $main_item_id);
												
												if( $team_data->ID == $team_post)
												{
													self::set_item( $main_item_id, $coupon_post_id, self::Type( 'Coupon' ), $discount * (-1), $quantity );
												}
											}
										}
									}
								}
							}	//End Check for Discounts
							
						}
					}
				}
			}	//End check for Coupon Info
		}
	}
	

	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */
	public function set_RSVP( $TeamID, $Cost, $Quantity )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		if( isset( $this->cart[$TeamID][self::Type( 'Reservation' )][$TeamID]['quantity'] ) )
		{
			$diff = $Quantity - $this->cart[$TeamID][self::Type( 'Reservation' )][$TeamID]['quantity'];
			
			if( $diff > 0 )
			{
				self::add_RSVP( $TeamID, $Cost, $diff);
			}
			
			if( $diff < 0 )
			{
				self::remove_Item( $TeamID, self::Type( 'Reservation' ), $TeamID, abs( $diff ) );
			}
		}
		else
		{
			self::add_RSVP( $TeamID, $Cost, $Quantity );
		}

	}
	
	
	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */
	public function set_AddOn( $TeamID, $AddOnID, $Cost, $Quantity )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		self::set_item( $TeamID, $AddOnID, self::Type( 'Add-on' ), $Cost, $Quantity );
	}
	
	public function clear_Coupons()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		foreach($this->cart as $main_item_id => $data)
		{
			unset($this->cart[$main_item_id][self::Type( 'coupon' )] );
		}
	}
	
	/**
	 * Removes the item(s) from the shopping cart
	 *
	 * @param int $item_type type of item
	 * @param int $item_id id of the item
	 * @param int $add_on id of the add_on item
	 * @param int $quantity quantity of the item
	 */
	public function remove_Item( $MainItem = 0, $ItemType = 'All', $ItemID = 0, $Quantity = 0)
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		//
		//Get the types
		if( strtolower( $ItemType ) == "all" )
		{
			$item_types = array_keys( $this->item_types );	
		}
		else
		{
			$item_types = array( $ItemType );
		}

		foreach( $item_types as $which_type )
		{
			//
			//Check if the item is set
			if( isset( $this->cart[$MainItem][$which_type][$ItemID] ) )
			{
				//
				// subtract $quantity of item from shopping cart
				$this->cart[$MainItem][$which_type][$ItemID]['quantity'] -= $Quantity;

				//
				//Check for each add-on items
				foreach( $this->cart[$MainItem] as $type_id => $items )
				{
					if( $type_id != $which_type )
					{
						foreach( $items as $item_id => $item_data )
						{
							$this->cart[$MainItem][$type_id][$item_id]['quantity'] -= $Quantity;

							if($this->cart[$MainItem][$type_id][$item_id]['quantity'] <=0 )
							{
								unset( $this->cart[$MainItem][$type_id][$item_id] );
							}

						}

					}
				}
			}

		}

		self::Validate();
		//
		//2018-02-21 DJT Validate() already performs an update_Cart() so removing
		//self::update_Cart();
	}
	

	///////////////////////////////////////////////////////////////////////////////////////////
	//
	// Functions to deal with the cart as a whole
	//
	///////////////////////////////////////////////////////////////////////////////////////////
	

	/**
	 * Empties the current cart
	 */
	public function Clear()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		unset( $this->cart );
		$this->cart = array();
		self::clear_cookie();

		self::update_Cart();
	}

	
	/**
	 * Iterates through shopping cart items and makes sure items are of type
	 * 1, 2, or 3
	 */
	public function Validate($Override_DateUses = false)
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		$cur_cart = $this->cart;
		self::Clear();

		//
		//Cycle through cart for each main item 
		foreach( $cur_cart as $main_item_id => $main_item_types )
		{
			//
			//Cycle through each items connected types
			foreach( $main_item_types as $this_type_id => $this_type_data )
			{
				//
				//If we have a legitiatmate item...						
				if( $this_type_id == self::Type( 'Reservation' ) ||
					$this_type_id == self::Type( 'Service' ) ||
					$this_type_id == self::Type( 'Merchandise' ) ||
					$this_type_id == self::Type( 'Add-on' ) || 
				    $this_type_id == self::Type( 'Fees' )
				  )
				{

					foreach( $this_type_data as $item_id => $item_data )
					{
						if( $item_data['quantity'] > 0 )
						{
							self::add_Item( $main_item_id, $item_id, $this_type_id, $item_data['cost'], $item_data['quantity'] );
						}
					}
		
				}
				
				if( $this_type_id == self::Type( 'CAP' ) )
				{
					foreach( $this_type_data as $item_id => $item_data )
					{
						self::add_CAP( $item_id );
					}
				}
				
				if( $this_type_id == self::Type( 'Coupon' ) )
				{
					foreach( $this_type_data as $item_id => $item_data )
					{
						$coupon_code = get_post_meta( $item_id, 'code', true );
						self::add_Coupon( $coupon_code , true, $Override_DateUses);
					}
				}
		
			}
		}

		self::update_Cart();
	}


	public function Items()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		return $this->cart;
	}
	public function get_cart()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		return $this->cart;
	}
	
	
	/**
	 * Organizes the current cart into Main Item, Fees, Merchanidse, then other stuff based upon add-on field
	 */
	public function Processed()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		$return_cart = array();
		
		foreach( $this->cart as $main_item_id => $main_item_types )
		{
			foreach( $main_item_types as $item_type => $items )
			{
				foreach( $items as $item_id => $item_data )
				{
					$return_cart[$main_item_id]
								[strtolower($this->item_types[$item_type]['title'])]
								[$item_id] = array('cost' => $item_data['cost'],
												   'quantity' => $item_data['quantity'],
												   'item_fld' => $this->item_types[$item_type]['item_field'],
												   'main' => $this->item_types[$item_type]['main'],
												   'item_type_id' => $item_type
												   );
				}
			}
			
		}
		
		//echo var_dump($return_cart);
		return $return_cart;
		
	}


	//////////////////////////////////////////////////////////////////////////
	//
	// Aggregate Functions
	//
	//////////////////////////////////////////////////////////////////////////
		
	/**
	 * Iterates through the shopping cart and counts all the items.
	 * 
	 * @return integer the total count of items in the shopping cart
	 */
	public function Count( $Type = 'all', $Item = 'all', $Format = 'float' )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		$total = 0;
		$item_type = self::Type( $Type );

		//
		//2018-02-21 DJT Changed to get cart items from the cookie.
		//               This corresponds to changes in /ajax_callbacks/cart.php cmd_cart_count_callback function
		//self::Read_CookieCartItems();
		$this->cart = array();
		self::fetch_Cart();
		
		//
		//Cycle through cart for each main item 
		foreach( $this->cart as $main_item_id => $main_item_types )
		{
			//
			//Cycle through each items connected types
			foreach( $main_item_types as $this_type_id => $this_type_data )
			{
				if( $this_type_id == $item_type ||
				    $item_type == 100 || 
				    ( $this->item_types[$this_type_id]['main'] == 1 && $item_type == 101 )
				  )
				{
					foreach( $this_type_data as $item_id => $item_data )
					{
						if($Item == 'all' || $Item == $item_id)
						{
							$total += $item_data['quantity'];
						}
					}
				}
			}
		}

		switch($Format)
		{
			case "string":
				return number_format($total,2);
				break;
			case "float":
				return $total;
				break;
		}
	}

	
	/**
	 * calculates the total for a specific type, or the total for the cart if no
	 * type is passed as an argument
	 *
	 * @param int $type the type of item to calculate the total of
	 */
	public function Total( $Type = 'all', $Item = 'all', $Format = "float" )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		$total = 0;
		$item_type = self::Type( $Type );

		//
		//Cycle through cart for each main item 
		foreach( $this->cart as $main_item_id => $main_item_types )
		{
			//
			//Cycle through each items connected types
			foreach( $main_item_types as $this_type_id => $this_type_data )
			{
				
				if( $item_type == self::Type( 'All' ) ||
				    $item_type == self::Type( 'Deposit' ) ||
				   	$item_type == $this_type_id ||
				    ( $item_type == self::Type( 'Main' ) && $this->item_types[$this_type_id]['main'] == 1 )
				  )
				{
					foreach( $this_type_data as $item_id => $item_data )
					{
						if($Item == 'all' || $Item == $item_id)
						{
							if( $item_type == self::Type( 'Deposit' ) )
							{
								if( $this_type_id == self::Type ( 'Reservation' ) )
								{
									$total += self::fetch_Deposit( $item_id ) * $item_data['quantity'];
								}
							}
							else
							{
								$total += ($item_data['quantity'] * $item_data['cost']);
							}
						}
					}
				}
			}
		}
		
		switch($Format)
		{
			case "string":
				return number_format($total,2);
				break;
			case "float":
				return $total;
				break;
		}

	}


	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Private functions for the cart
	//
	///////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * Adds an item to the shopping cart.
	 *
	 * @param int $item_type type of item added
	 * @param int $item_id id of item
	 * @param int $add_on the item_id of the base item for which this item adds onto
	 * @param int $cost cost of item
	 * @param int $quantity quantity of items
	 */
	private function add_Item( $main_item = 0, $item_id = 0, $item_type = 0, $cost = 0, $quantity = 0, $verbiage = '' )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		if( isset( $this->cart[$main_item][$item_type][$item_id] ) )
		{
			$this->cart[$main_item][$item_type][$item_id]['quantity'] += $quantity;
		}
		else
		{
			$this->cart[$main_item][$item_type][$item_id] = array( 'cost' => $cost, 'quantity' => $quantity, 'note' => $verbiage );
		}
		
		//
		//Check for fee
		if( $item_type == 1 )
		{
			self::add_fee( $main_item, $quantity );
		}
	
		self::update_Cart();
	}

	
	/**
	 * Sets a specific quantity of items, regardless of what was before
	 *
	 * @param int $item_type type of item added
	 * @param int $item_id id of item
	 * @param int $add_on the item_id of the base item for which this item adds onto
	 * @param int $cost cost of item
	 * @param bool $cookie resend cookie or not...don't resend in the middle of a page
	 */
	private function set_item( $main_item, $item_id = 0, $item_type = 0, $cost = 0, $quantity = 0)
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		//
		//2018-03-08 DJT Added note for consistency.
		$this->cart[$main_item][$item_type][$item_id] = array( 'cost' => $cost, 'quantity' => $quantity, 'note' => '' );

		//
		//Check for fee
		if( $item_type == 1 )
		{
			self::add_Fee( $main_item, $quantity );
		}
	
		self::update_Cart();
	}

	
	/**
	 * Gets the appfee from the database and appends it to the cart as an add-on.
	 *
	 * @param int $item_id the id of the item
	 * @param int $quantity the quantity of the item
	 */

	private function add_Fee( $MainItem, $Quantity )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;

		// 
		// build and execute query to get appfee from the database
		$query = "SELECT appfee FROM " . $wpdb->prefix . "command_TEAM WHERE id = " . $MainItem;
		$value = $wpdb->get_var( $query );
		
		if( !empty( $value ) )
		{
			self::add_Item( $MainItem, $MainItem, 0, $value, $Quantity );
		}
	}


	/**
	 * Turns the text representation of the type of item and converts it into the
	 * id/number representation of the item type
	 *
	 * @param string $type the text representation of the item type
	 *
	 * @return integer the id/number representation of the item type
	 */
	private function Type( $Type = 'all' )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		if( strtolower( $Type ) == 'all' )
		{
			return 100;
		}
		elseif ( strtolower( $Type ) == 'main' )
		{
			return 101;
		}
		else
		{
			foreach( $this->item_types as $type_id => $type_data )
			{
				if( strtolower( $type_data['title'] ) == strtolower( $Type ) )
				{
					return $type_id;
				}
			}
		}

		return 0;
	}
		

	private function fetch_ItemTypes()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;
		
		$query = 'SELECT id, title, verbiage, item_field, main, function FROM ' . $this->items_type_table;
		//echo $query;
		$results = $wpdb->get_results( $query, OBJECT_K);
		
		if( !empty( $results ) )
		{
			foreach( $results as $row )
			{
				$this->item_types[$row->id] = array( 'title' => $row->title, 
													 'verbiage' => $row->verbiage,
												   	 'item_field' => $row->item_field,
													 'main' => $row->main,
													 'function' => $row->function
												   );
			}

			//
			//2018-02-21 DJT Removed since item_type[100] is already in items table
			/*
			$this->item_types[100] = array( 'title' => 'Total', 
										 	'verbiage' => 'Total',
										 	'item_field' => '',
										 	'main' => 0,
										 	'function' => ''
									   	  );
			*/
			$this->item_types[101] = array( 'title' => 'Main', 
										 	'verbiage' => 'Main',
										 	'item_field' => '',
										 	'main' => 0,
										 	'function' => ''
									   	  );

		}
	}

	
	/**
	 * Returns the deposit amount for the team with the specific item_id
	 *
	 * @param int $item_id the id of the item
	 *
	 * @return float $value - the deposit value for the team
	 */
	private function fetch_Deposit( $ItemID )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;
		$query = "SELECT deposit FROM " . $wpdb->prefix . "command_TEAM WHERE id = " . $ItemID;
		$value = $wpdb->get_var($query);

		return (empty($value) ? 0 : (float)$value);
	}
		
	
	//////////////////////////////////////////////////////////////////////////////////////
	//
	// Functions to deal with ins and outs of the cart from the user and db
	//
	//////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * Get the current cart from the database, if one exists.
	 */
	private function fetch_Cart()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;
		
		//
		//2018-02-21 DJT removed md5_or_id check since md5 and user ids should never cross
		//
		//Find current cart info if there is any...
		/*
		$query = "SELECT items FROM " . $this->cart_table . " 
				  WHERE kode='" . $this->cart_code . "' AND md5_or_id = " . $this->md5;
		*/
		$query = "SELECT items FROM " . $this->cart_table . " 
				  WHERE kode='" . $this->cart_code . "'";
		$products = $wpdb->get_var( $query );

		//
		//2018-02-21 DJT changed from old function to new function
		//self::unpack_Cart( $products );
		self::Unpack_CartItems( $products );

	}

	/**
	 * Updates cookie for user's shopping cart and adds cookie data to db
	 */
	private function update_Cart()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		global $wpdb;
		
		//
		//2018-092-27 DHT Clkause to replace or delete
		if(empty($this->cart))
		{
			$wpdb->delete( $this->cart_table, array ('kode' => $this->cart_code));	
			$wpdb->delete( $this->cart_table, array ('kode' => get_current_user_id()));	
		}
		else
		{
			$wpdb->replace( $this->cart_table, array('kode' => $this->cart_code,
													 'md5_or_id' => $this->md5,
													 'items' => self::pack_Cart(),
													 'expires' => date("Y-m-d h:i:s", time()+60*60*24*14)
													 )
						  );
		}
		
		//
		//2018-02-21 DJT Remove second global $wpdb call
		//
		//Little cleanup, remove all empyt cart items
		//global $wpdb;
		$wpdb->delete( $this->cart_table, array( 'items' => '' ) );

		//
		//2018-02-26 DJT Removed check which will add to functions themselves
		//if( !headers_sent() )
		//{
			//
			//2018-02-26 DJT Added clear in case use status changed
			self::clear_cookie();
			self::write_cookie();
		//}
	}


	////////////////////////////////////////////////////////////////////////////
	//
	//Private pack and unpack cart items
	//
	////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * Packs the cart data into a colon (:) and plus (+) separated string
	 *
	 * @return string  colon-separated string of cart data
	 */
	private function pack_Cart()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		$data = '';

		//
		//2018-02-27 DJT Added check to skip data check if cart is empty.
		if( !empty ( $this->cart ) )
		{
			foreach( $this->cart as $main_item_id => $this_types_data)
			{
				foreach( $this_types_data as $this_type_id => $item_data)
				{
					foreach( $item_data as $item_id => $item_info )
					{
						$data .= $main_item_id . ":" . $this_type_id . ":" . $item_id . ":" . $item_info['cost'] . ":" . $item_info['quantity']  . "+";	
					}
				}
			}

			if(strlen($data) > 1)
			{
				$data = substr($data, 0, -1);
			}
		}

		return $data;
	}

	//
	//2018-02-21 DJT Removed Function since the renamed Unpack_CartItems does the same process but better
	/**
	 * Unpacks the cart data from the colon and plus separated string format
	 *
	 * @param string $data the cart data to be unpacked into an array
	 */
	/*
	private function unpack_Cart( $Data = '' )
	{
		//
		// clear current cart first
		unset( $this->cart );
		$this->cart = array();

		//
		// make sure there is data of the correct length
		if( strlen( $Data ) >= 5 )
		{
			$items = explode( "+", $Data );

			foreach( $items as $item )
			{
				list( $main_item_id, $item_type, $item_id, $cost, $quantity ) = explode( ":", $item );
//				$this->cart[$main_item_id][$item_type][$item_id] = array( 'cost' => $cost, 'quantity' => $quantity);	
				$this->cart[$main_item_id][$item_type][$item_id] = array( 'cost' => $cost, 'quantity' => $quantity, 'note' => '' );
			}
		}
	}
	*/
	
	//
	//2018-02-21 DJT changed function name and $data to $PackedData with corresponding variable translation throughout
	/**
	 * Merges packed cart data to the current cart
	 *
	 * @param $string $data packed cart data
	 */
	//private function merge_cart ( $data = '' )
	private function Unpack_CartItems( $PackedData = '' )
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		if( strlen( $PackedData ) >= 5 )
		{
			$items = explode( "+", $PackedData );
			
			foreach( $items as $item )
			{
				//
				//2018-02-21 DJT added check for 5th item
				//if(isset($item[1]) && isset($item[2]) && isset($item[3]) && isset($item[4]))
				if( isset( $item[1] ) && isset( $item[2] ) && isset( $item[3] ) && isset( $item[4] ) && isset( $item[5] ) )
				{
					//
					//2018-02-21 DJT Changed list() to new cart format
					//list($type, $id, $add, $cost, $quantity) = explode(":", $item);
					list( $main_item_id, $item_type, $item_id, $cost, $quantity ) = explode( ":", $item );

					//
					//2018-02-21 DJT Changed to new cart format for check and flipped check boolean logic
					//
					// if there is an item already in the cart with the same id, add the
					// quantity, else create a new item
					//if(isset($this->cart[$type][$id]))
					if( empty( $this->cart[$main_item_id][$item_type][$item_id]['quantity'] ) )
					{
						//
						//2018-02-21 DJT Changed to new cart build string
						//$this->cart[$type][$id] = array($add, $cost, $quantity);	
						$this->cart[$main_item_id][$item_type][$item_id] = array( 'cost' => $cost, 'quantity' => $quantity, 'note' => '' );
					}
					else
					{
						//
						//2018-02-21 DJT Changed to new cart quantity location
						//$this->cart[$type][$id][2] += $quantity;	
						$this->cart[$main_item_id][$item_type][$item_id]['quantity'] += $quantity;
					}
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////
	//
	//private cookie functions
	//
	/////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Checks the cart cookie data and merges the new cart data and deletes old
	 * cart data
	 */
	private function check_merge()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		if(isset($_COOKIE['islonline_org_cartcode']))
		{
			$old_code = $_COOKIE['islonline_org_cartcode'];

			if((string)$old_code != (string)$this->user )
			{
				//
				//2018-02-21 DJT Changed $this->md5 to boolean
				//
				//Set card code and md5 for getting pre-user data
				$this->cart_code = $old_code;
				$this->md5 = true;

				//
				//2018-02-21 DJT changed method to get cart items from cookie instead of DB
				//               Clear cookies too now so old data is removed.
				//
				//Get the old cart
				//self::get_cart_items();
				//$temp_cart = self::pack_cart();
				self::fetch_Cart();
				self::clear_cookie();
					
				//
				//2018-02-21 DJT Changed $this->md5 to boolean
				//
				//Reset cart code and md5
				$this->cart_code = $this->user;
				$this->md5 = false;				

				//
				//2018-02-21 DJT Changed old get_cart to new Fetch_Cart
				//
				//Merge the new cart
				//self::get_cart();
				self::fetch_Cart();

				//
				//2018-02-21 DJT Since Read_CookieCartItems and fetch_Cart run Unpack_CartItems, no need to do it twice.
				//unset($this->cart);
				//$this->cart = array();
				//self::Unpack_CartItems($temp_cart);
				
				//
				//2018-02-26 DJT Added clear_cookie() to explicitely remove ALL old cookies before adding new one.
				//
				//Save the new cart
				self::update_cart();
				self::clear_cookie();
				self::write_cookie();
				
				//
				//2018-02-21 DJT Removed md5_or_id as not using it anymore
				//
				//Delete the old pre-user cart
				global $wpdb;
				//$wpdb->delete($this->cart_table, array('kode' => $old_code, 'md5_or_id' => '0'));
				$wpdb->delete($this->cart_table, array('kode' => $old_code));
			}
		}
	}

	/**
	 * Loads cart data from cookies
	 *
	 * @param string $cookie cookie data
	 */
	private function get_cart_code($cookie)
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		if(isset($_COOKIE['islonline_org_cartcode']))
		{
			$this->cart_code = $_COOKIE['islonline_org_cartcode'];
		}
		else 
		{
			self::make_cart_code($cookie);
		}

		//
		//2018-02-21 DJT if we are md5, then we cannot have a numeric cart code as that would indicate a logged in user.
		if( $this->md5 && is_numeric( $this->cart_code ) )
		{
			self::make_cart_code( $cookie );
		}
		
		if($cookie)
		{
			self::write_cookie();
		}
	}

	//
	//2018-02-21 Added function to pull cart dara from cookie.
	/**
	 * Loads cart data from cookies
	 *
	 * @param string $cookie cookie data
	 */
	private function Read_CookieCartItems()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		$this->cart = array();

		if(isset($_COOKIE['islonline_org_cartitems']))
		{
			self::Unpack_CartItems( $_COOKIE['islonline_org_cartitems'] );
		}
	}

	/**
	 * Makes the cart code for the cookie
	 *
	 * @param string $cookie cookie data
	 */
	private function make_cart_code($cookie)
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		//
		//2018-02-21 Changed to use boolean md5 as well as alternated eleements by new boolean sequence
		//if($this->md5 > 0)
		if( $this->md5 )
		{
			//$this->cart_code = $this->user;
			$this->cart_code = md5(md5(dechex(rand(1,4000000000))) . md5(dechex(rand(1,4000000000))));
		}
		else
		{
			//$this->cart_code = md5(md5(dechex(rand(1,4000000000))) . md5(dechex(rand(1,4000000000))));
			$this->cart_code = $this->user;
		}
	}

	/**
	 * Sets the cookie(s) for the cart
	 */
	private function write_cookie()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		//
		//2018-03-02 DJT Check headers to determine if we write using php or jquery.  Added jQuery cookies
		if( !headers_sent() )
		{
			setcookie('islonline_org_cartcode', $this->cart_code, time()+60*60*24*14, "/", "islonline.org");
			setcookie('islonline_org_cartitems', self::pack_Cart(), time()+60*60*24*14, "/", "islonline.org");
		}
		else
		{
			$cookie_data = '{ expires: ' . time()+60*60*24*14 . ',
							  domain: \'islonline.org\',
							  path: \'/\'}';
			if(empty($this->jq_cookie))
			{
				?>
				<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
				<?php
				$this->jq_cookie = true;
			}
			?>
			<script>
				jQuery(document).ready(function()
				{
					Cookies.set('islonline_org_cartcode', '<?php echo $this->cart_code ?>' <?php echo $cookie_data ?>);	
					Cookies.set('islonline_org_cartitems', '<?php echo self::pack_Cart ?>' <?php echo $cookie_data ?>);	
					Console.log('write via jq');
				})
			</script>
			<?php
		}
	}

	/**
	 * Clears the cookie(s) for the cart
	 */
	private function clear_cookie()
	{
		if ($this->backtrace) {	echo __FUNCTION__ . ": " . debug_backtrace()[1]['function'] . " - (" .  count($this->cart) . ")<BR>"; }
		
		//
		//2018-03-02 DJT Check headers to determine if we write using php or jquery.  Added jQuery cookies
		if( !headers_sent() )
		{
			setcookie('islonline_org_cartcode', '', time()-1, "/", "islonline.org");
			setcookie('islonline_org_cartitems', '', time()-1, "/", "islonline.org");
		}
		else
		{
			if(empty($this->jq_cookie))
			{
				?>
				<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
				<?php
				$this->jq_cookie = true;
			}
			
			?>
			<script>
				jQuery(document).ready(function()
				{
					Cookies.remove('islonline_org_cartcode', { path: '/', domain: 'islonline.org'});	
					Cookies.remove('islonline_org_cartitems', { path: '/', domain: 'islonline.org'});	
					Console.log('clear via jq');
				})
			</script>
			<?php
		}
	}
}