<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "team.support.class.php";
require_once CC_CLASSES_DIR . "user.class.php";


/**
 * RSVP class
 * Manages RSVP data
 */
CLASS RSVP
{
	/** @var integer $user ID of the user */
	private $user = NULL;

	/** @var array $user_data List of user data */
	private $user_data = NULL;

	/** @var string $user_next_rsvp Next RSVP for user */
	private $user_next_rsvp = NULL;

	/** @var string $user_last_rsvp Last RSVP for user */
	private $user_last_rsvp = NULL;

	/** @var string $rsvp_table Database table for RSVP objects */
	private $rsvp_table = NULL;

	/** @var integer $rsvp_id ID of RSVP */
	private $rsvp_id = NULL;

	/** @var array $rsvp List of RSVP attributes */
	private $rsvp = NULL;
	
	/** @var string $order_items_table Database table for order items */
	private $order_items_table = NULL;
	
	/** @var string $post_meta table index for Fundraiser title and blurb */
	private $fund_meta_key = "_cmd_fund_title_blurb";

	/**
	 * Default Constructor
	 * Sets the DB Tables and the current user, and loads the current user's data
	 */
	public function __construct()
	{
		global $wpdb;
		$this->order_items_table = $wpdb->prefix . "command_order_items";
		$this->rsvp_table = $wpdb->prefix . "command_RSVP";
		$this->rsvp = 0;
		
		$this->user = get_current_user_id();
		
		if($this->user > 0)
		{
			self::get_user_data();
		}
		else
		{
			$this->user_data = array();
		}
	}

	/**
	 * RSVP Functions
	 */

	public function change_user($user_id)
	{
		$this->user = $user_id;
		self::get_user_data();
	}
	
	/**
	 * Sets or retrieves the fundraiser language - title and blurb
	 *
	 * @param string $title - the title of the Fundriaser page
	 * @param string $blurb - the blurb of the fundraiser
	 */
	public function fund_language($title = '', $amount = 0, $blurb = '')
	{
		if(func_num_args() > 0)
		{
			if(!empty($this->rsvp['post_id']))
			{
				update_post_meta($this->rsvp['post_id'], $this->fund_meta_key, array('title' => $title, 'amount' => $amount, 'blurb' => $blurb)); 
			}
		}
		else
		{
			$temp = get_post_meta($this->rsvp['post_id'], $this->fund_meta_key);
			
			if(!empty($temp[0]))
			{
				return $temp[0];
			}
			else
			{
				return array('title' => '', 'blurb' => '');
			}
			
		}
	}

	public function set_flight_fees($flight_info = array())
	{
		if(!empty($this->rsvp))
		{
			if(!empty($flight_info))
			{
				update_post_meta($this->rsvp['post_id'], '_cmd_flight_fee_data', $flight_info);	
			}
			else
			{
				delete_post_meta($this->rsvp['post_id'], '_cmd_flight_fee_data');
			}
		}
	}

	public function get_flight_fees()
	{

		if(!empty($this->rsvp))
		{
			$core = get_post_meta($this->rsvp['post_id'], '_cmd_flight_fee_data', false);
		}
		else
		{
			$core = false;
		}
		
		if(!empty($core[0]))
		{
			return $core[0];	
		}
		else
		{
			return array('arrive' => '', 'depart' => '');
		}
	}

	/**
	 * Sets the flight information
	 *
	 * @param array $flight_info (index)(airline, flight, seat, depart, date, time, arrive, date, time)
	 */
	public function set_flight_info($flight_info = array())
	{
		if(!empty($this->rsvp))
		{
			if(!empty($flight_info))
			{
				update_post_meta($this->rsvp['post_id'], '_cmd_flight_data', $flight_info);	
			}
			else
			{
				delete_post_meta($this->rsvp['post_id'], '_cmd_flight_data');
			}
		}
	}

	/**
	 * Gets the flight information
	 *
	 * @param array $flight_info ($index => array (airline, flight, seat, depart, date, time, arrive, date, time))
	 */
	public function get_flight_info()
	{

		if(!empty($this->rsvp))
		{
			$core = get_post_meta($this->rsvp['post_id'], '_cmd_flight_data', false);
		}
		else
		{
			$core = false;
		}

		if(!empty($core[0]))
		{
			return $core[0];	
		}
		else
		{
			return array(1 => array('direction' => 0,
									'airline' => '', 'flight' => '', 'seat' => '',
									'depart' => '', 'ddate' => '', 'dtime' => '',
									'arrive' => '', 'adate' => '', 'atime' => ''));
										
		}
	}


	/**
	 * Sets the RSVP id
	 *
	 * @param integer $id ID for RSVP
	 */
	public function set_rsvp_id($id = 0)
	{
		$this->rsvp_id = $id;
	}

	/**
	 * Gets the RSVP id
	 *
	 * @return integer ID of the RSVP object
	 */
	public function get_rsvp_id()
	{
		return $this->rsvp_id;
	}

	/**
	 * Gets the RSVP object 
	 *
	 * @return array RSVP object
	 */
	public function get_rsvp()
	{
		return $this->rsvp;
	}

	/**
	 * Sets the RSVP id by the team and user id's
	 *
	 * @param integer $team ID of the team
	 * @param integer $user ID of the user
	 */
	public function set_id_by_team_user($team, $user)
	{
		global $wpdb;
		$this->rsvp_id = $wpdb->get_var("SELECT id
								   		 FROM " . $this->rsvp_table . "
								   		 WHERE team_id = " . $team . " AND user_id = " . $user);
		
		return $this->rsvp_id;
	}

	/**
	 * Deterines if the flight information is uploaded
	 * 
	 * @return boolean
	 */
	public function has_travel($rsvp_id)
	{
		//
		//2018-03-15 DJT added new array
		$directions = array();
		
		if(!empty($this->rsvp))
		{
			$core = get_post_meta($this->rsvp['post_id'], '_cmd_flight_data', false);
		}

		if(!empty($core[0]))
		{

			foreach($core[0] as $ind=>$data)
			{
				$directions[$data['direction']] = 1;
			}
			
			//
			//2018-03-15 DJT changed return true if $core[0] was set to check set directions
			//return true;
			if(count($directions) == 2)
			{
				return true;
			}
			else
			{
				return false;
			}
			
			
		}
		else
		{
			return false;
		}

	}

	/**
	 * Deterines if the rsvp has a passport uploaded for the current rsvp
	 * 
	 * @return string The temporary user name
	 */
	public function has_passport($rsvp_id)
	{
		$counter = 0;
		self::set_rsvp_id($rsvp_id);
		self::fetch_rsvp();
		

		if(!empty($this->rsvp['post_id']))
		{
			$number = get_post_meta($this->rsvp['post_id'], '_cmd_passport_num', true);
			$country = get_post_meta($this->rsvp['post_id'], '_cmd_passport_country', true);
			$issue = get_post_meta($this->rsvp['post_id'], '_cmd_passport_issue', true);
			$expire = get_post_meta($this->rsvp['post_id'], '_cmd_passport_expire', true);
			$img = get_post_meta($this->rsvp['post_id'], '_cmd_passport_photo', true);

			if(!empty($number))
			{
				$counter++;
			}
			if(!empty($country))
			{
				$counter++;
			}
			if(!empty($issue))
			{
				$counter++;
			}
			if(!empty($expire))
			{
				$counter++;
			}
			if(!empty($img))
			{
				$counter++;
			}
		}
		
		return ($counter == 5) ? true : false;
	}


	/**
	 * Gets the current rsvp data from the database and sets it to $this rsvp
	 * 
	 * @return array RSVP object
	 */
	public function fetch_rsvp()
	{
		$rsvp_id = self::get_rsvp_id();

		if(!empty($rsvp_id))
		{
			global $wpdb;
			$query = "SELECT r.id, r.team_id, r.user_id, r.order_item_id, r.post_id, o.order_id, 
                          o.created_by purchaser, o.created_when purchase_date
                           FROM " . $this->rsvp_table . " r INNER JOIN " . $this->order_items_table . " o
                                ON r.order_item_id = o.id
                           WHERE r.id=" . $rsvp_id;
			//echo $query;
			$results = $wpdb->get_row($query, ARRAY_A);

			if(!empty($results))
			{
				$this->rsvp = $results;
			}
			else
			{
				$this->rsvp = array();
			}
			
			return $this->rsvp;
		}
	}
	
	/**
	 * Gets the current rsvp data from the database and sets it to $this rsvp
	 * 
	 * @return array RSVP object
	 */
	public function order_id_by_rsvp($rsvp = 0)
	{
		if(!empty($rsvp))
		{
			global $wpdb;
			$results = $wpdb->get_row("SELECT id, team_id, user_id, order_item_id, post_id 
									   FROM " . $this->rsvp_table . " 
									   WHERE id=" . $rsvp, 
									  ARRAY_A);

			if(!empty($results))
			{
				$rsvp_check = $results;
			}
			else
			{
				$rsvp_check = array();
			}
			
			if(!empty($rsvp_check))
			{
				$query = "SELECT order_id FROM wp_command_order_items WHERE id=" . $rsvp_check['order_item_id'];
				return $wpdb->get_var($query); 				
			}
		}

		return false;
	}

/**
	 * Gets the current rsvp data from the database and sets it to $this rsvp
	 * 
	 * @return array RSVP object
	 */
	public function order_id_by_team($team = 0, $user = 0)
	{
		if(!empty($team))
		{
			global $wpdb;

			if($user==0)
			{
				$user = wp_get_current_user();
				$user_id = $user->ID;
			}
			else
			{
				$user_id = $user;
			}

			//
			//2018-03-22 DJT Added join to oi to check quantity to make sure we have a legitimate RSVP
			$query = "SELECT r.id, team_id, user_id, order_item_id, post_id 
					  FROM " . $this->rsvp_table . " r INNER JOIN " . $this->order_items_table . " oi 
					  		ON r.order_item_id = oi.id
			    	  WHERE r.team_id=" . $team . " AND r.user_id=" . $user_id . " AND oi.quantity > 0";
			$results = $wpdb->get_row($query, ARRAY_A);

			if(!empty($results))
			{
				$rsvp_check = $results;
			}
			else
			{
				$rsvp_check = array();
			}
			
			if(!empty($rsvp_check))
			{
				$query = "SELECT order_id FROM wp_command_order_items WHERE id=" . $rsvp_check['order_item_id'];
				return $wpdb->get_var($query); 				
			}
		}

		return false;
	}

	/**
	 * Gets the current rsvp data from the database and sets it to $this rsvp
	 * 
	 * @return array RSVP object
	 */
	public function order_items($order_id = 0)
	{
		if(!empty($order_id))
		{
			global $wpdb;
			$results = $wpdb->get_results("SELECT id FROM wp_command_order_items WHERE order_id = " . $order_id, ARRAY_A);

			if(!empty($results))
			{
				foreach($results as $id)
				{
					$ids[] = $id['id'];	
				}

				return $ids;
			}
		}

		return array();
	}

	/**
	 * Creates new rsvp and saves it to the db
	 * 
	 * @param integer $team_id Id of the team
	 * @param integer $user_id Id of the user
	 * @param integer $order_item_id Id of order item
	 */
	public function new_rsvp($team_id = 0, $user_id = 0, $order_item_id = 0)
	{

		if($team_id > 0)
		{
			global $wpdb;
			//
			//2018-03-23 DJT Added new classes in to change hor rsvps are handled for existing blanks.
			$team = new TEAM();
			$support = new TEAM_SUPPORT();
			$user = new USER();
			$id_to_assign = 0;
			
			//
			//2018-03-24 DJT Added check to see if this is an rsvp_add team & there's a user being assigned
			$team->set_team($team_id);
			
			if($support->type_value($team->value('type'), 'rsvp') == 1 && $user_id > 0)
			{
				$query = "SELECT r.id, r.created_by 
							FROM " . $this->rsvp_table . " r INNER JOIN " . $this->order_items_table . " pay 
								ON r.order_item_id = pay.id
							WHERE 
								r.team_id = " . $team_id . " AND r.user_id = 0 AND pay.quantity > 0
							ORDER BY
								r.id";
				$results = $wpdb->get_results($query);

				foreach($results as $data)
				{
					//echo var_dump($data);
					$creator = $data->created_by;
					if($user->user_group($creator) == 'super' || $user->user_group($creator) == 'admin' || $user->user_group($creator) == 'staff')
					{
						$id_to_assign = $data->id;
						break(1);
					}
				}
			}

			//
			//2018-03-24 DJT moved insert into else statement to allow for rsvp update assignment
			//If we have a viable blank rsvp to assign, then assign it, otherwise create an rsvp
			if($id_to_assign > 0)
			{
				$wpdb->update($this->rsvp_table,
							  array('user_id' => $user_id, 'modified_by' => $this->user),
							  array('id' => $id_to_assign)
							 );
				self::set_rsvp_id($id_to_assign);
			}
			else
			{
				$wpdb->insert($this->rsvp_table, array('team_id' => $team_id,
													   'user_id' => $user_id,
													   'order_item_id' => $order_item_id,
													   'post_id' => self::build_page('team ' . $team_id, 'user ' . $user_id),
													   'title' => '',
													   'created' => date("Y-m-d h:i:s"),
													   'created_by' => $this->user
													   ));
				self::set_rsvp_id($wpdb->insert_id);
			}
			//
			//2018-03-23 DJT Added check to make sure creating the RSVP doesn't error when a blank RSVP is added.
			if(!empty($user_id))
			{
				do_action('cmd_rsvp_create_after', $team_id, $user_id);
			}
			//
			//2018-03-24 DJT moved to above if block for each block type
			//self::set_rsvp_id($wpdb->insert_id);
		}
	}
	
	/**
	 * Moves current RSVP to new team
	 *
	 * @param integer $current_rsvp ID of the current RSVP
	 * @param integer $new_team ID of the new team
	 * @param integer $new_order_item_id ID of the new order item
	 */
	public function migrate($current_rsvp, $new_team, $new_order_item_id)
	{
		global $wpdb;
		
		self::set_rsvp_id($current_rsvp);
		$cur_data = self::fetch_rsvp();
		
		self::new_rsvp($new_team, $cur_data['user_id'], $new_order_item_id);
		$new_data = self::fetch_rsvp();
		
		//
		//Move passport
		$query = "SELECT passport FROM " . $this->rsvp_table . " WHERE id=" . $current_rsvp;
		//echo $query;
		$image = $wpdb->get_var($query);
		//echo $image;
		
		$wpdb->update($this->rsvp_table, array('passport' => $image), array('id' => self::get_rsvp_id()));
		
		$query = "UPDATE " . $wpdb->prefix . "postmeta 
				  SET post_id = " . $new_data['post_id'] . " 
				  WHERE post_id = " . $cur_data['post_id'] . " AND meta_key like '_cmd_passport_%'";
		//echo $query;
		$wpdb->query($query);
		
		
		//
		//Move other ACF data
		$query = "SELECT meta_id, meta_value, meta_key FROM " . $wpdb->prefix . "postmeta WHERE post_id = " . $cur_data['post_id'] . " && meta_value like 'field_%'";
		//echo $query;
		$fields = $wpdb->get_results($query);
		
		foreach($fields as $data)
		{
			$wpdb->update($wpdb->prefix . "postmeta", 
						  array('post_id' => $new_data['post_id']), 
						  array('meta_id' => $data->meta_id));
			
			$wpdb->update($wpdb->prefix . "postmeta", 
						  array('post_id' => $new_data['post_id']), 
						  array('meta_key' => substr($data->meta_key, 1), 'post_id' => $cur_data['post_id']));
			
		}
	}
	
	/**
	 * Gets the list of RSVPs for a team
	 *
	 * @param integer $team_id ID of the team
	 *
	 * @return array List of RSVP objects for the team
	 */
	public function list_rsvp($team_id)
	{
		global $wpdb;
		return $wpdb->get_col('SELECT r.user_id 
								FROM ' . $this->rsvp_table . ' r LEFT JOIN ' . $this->order_items_table . ' o
									ON r.order_item_id = o.id
								WHERE r.team_id=' . $team_id . ' AND (o.quantity IS NULL OR o.quantity > 0)');
	}
	
	public function list_rsvp_by_team($team_id)
	{
		global $wpdb;
		$teams = $wpdb->get_results('SELECT r.id, r.order_item_id, r.post_id, r.user_id, o.created_by purchaser 
								  FROM ' . $this->rsvp_table . ' r LEFT JOIN ' . $this->order_items_table . ' o
									ON r.order_item_id = o.id
								  WHERE r.team_id=' . $team_id . ' AND (o.quantity IS NULL OR o.quantity > 0)', ARRAY_A);
		if(!empty($teams))
		{
			foreach($teams as $data)
			{
				$rets[$data['id']] = $data;
			}
			return $rets;
			exit;
		}
		
		return array();
	
	}

	/**
	 * Gets the number of RSVPs for a team
	 *
	 * @param integer $team_id ID of the team
	 *
	 * @return integer number of RSVP's for the team
	 */
	public function count_rsvp($team_id)
	{
		global $wpdb;
		
		$count = $wpdb->get_var('SELECT count(r.post_id) 
	  							  FROM ' . $this->rsvp_table . ' r INNER JOIN ' . $this->order_items_table . ' o
									ON r.order_item_id = o.id
								WHERE r.team_id=' . $team_id . ' AND o.quantity > 0');
		return max(0,$count);	
	}

	/**
	 * Get's team id from reservation
	 *
	 * @param integer $reservation_id ID of the reservation
	 *
	 * @return integer ID of the team for the reservation
	 */
	public function team($reservation_id)
	{
		global $wpdb;
		return $wpdb->get_var('SELECT team_id FROM ' . $this->rsvp_table . ' WHERE id=' . $reservation_id);
	}

	/**
	 * Gets post id from reservation
	 *
	 * @param integer $reservation_id ID of the reservation
	 *
	 * @return integer ID of the post
	 */
	public function post($reservation_id)
	{
		global $wpdb;
		return $wpdb->get_var('SELECT post_id FROM ' . $this->rsvp_table . ' WHERE id=' . $reservation_id);
	}

	/**
	* Determine if the profile information is complete or not
	*
	* @param int $post_id of the reservation
	*
	* @return boolean
	*/
	public function profile_complete($post_id)
	{
		global $wpdb;
		
		$counter = 0;
		$keys = array("school",
					  "academic_major",
					  "academic_courses",
					  "professional_experience",
					  "languages_spoken",
					  "are_you_taking_any_medications", 
					  "do_you_have_any_food_allergies",
					  "emergency_contact", 
					  "emergency_contact_relationship",
					  "emergency_contact_primary_phone");

		$query = "SELECT meta_key, meta_value 
				  FROM " . $wpdb->prefix . "postmeta 
				  WHERE post_id = " . $post_id . " AND	
				  	meta_key in ('" . implode("','", $keys) . "')";
		$possibles = $wpdb->get_results($query, ARRAY_A);

		if(!empty($possibles) && count($possibles) == count($keys))
		{
			foreach($possibles as $dataset)
			{
				if(!empty($dataset['meta_value']))
				{
					$counter ++;	
				}
			}
		}
		return (count($keys) == $counter) ? true : false;			
	}

	
	public function valid($user_id, $rsvp_id)
	{
		global $wpdb;
		
		$query = "SELECT post_id FROM " . $this->rsvp_table . " WHERE user_id = " . $user_id . " AND id = " . $rsvp_id;
		return $wpdb->get_var($query);
	}
	
	/**
	 * Gets the user info for the reservation
	 * 
	 * @param integer $user_id ID of the user
	 *
	 * @return array User data for reservation
	 */
	public function for_user($user_id)
	{
		global $wpdb;
		unset($this->user_next_rsvp);
		$reservation = array();
		
		//
		// get user info
		$query = "SELECT r.id, r.team_id, r.post_id, ct.hash, cs.start_date 
				  FROM " . $this->rsvp_table . " r 
				  		inner join " . $wpdb->prefix . "command_TEAM ct ON r.team_id = ct.id
						inner join " . $wpdb->prefix . "command_schedule cs on ct.id = cs.team_id 
						inner join " . $wpdb->prefix . "command_team_status ts ON ct.team_status_id = ts.id
						inner join " . $wpdb->prefix . "command_order_items oi ON r.order_item_id = oi.id
				  WHERE r.user_id = " . $user_id . " and oi.quantity > 0
				  	
				  ORDER BY ts.user_sequence DESC, cs.start_date DESC";
		$results = $wpdb->get_results($query, ARRAY_A);

		//
		// parse user info into reservations
		foreach($results as $data)
		{
			if(!isset($reservation[$data['id']]))
			{
				$reservation[$data['id']] = array('team' => $data['team_id'],
												  'post' => $data['post_id'],
												  'hash' => $data['hash'],
												  'start_date' => $data['start_date']);
				$reservation[$data['id']]['days_until'] = self::days_until($data['start_date']);
				
				//
				// add next rnvp or last rsvp
				if(strtotime($data['start_date']) > time() && !isset($this->user_next_rsvp))
				{
					$this->user_next_rsvp = $data['id'];
				}
				else if(strtotime($data['start_date']) < time())
				{
					$this->user_last_rsvp = $data['id'];
				}
				
			}
		}
		return $reservation;
	}
	
	/**
	 * Gets reservation data for user owner of rsvp
	 * 
	 * @param int The user's id
	 */
	public function by_user($user_id)
	{
		global $wpdb;
		$reservation = array();
		
		$query = "SELECT r.id, r.team_id, r.post_id, ct.hash, cs.start_date 
				  FROM " . $this->rsvp_table . " r 
				  		inner join " . $wpdb->prefix . "command_TEAM ct ON r.team_id = ct.id
						inner join " . $wpdb->prefix . "command_schedule cs on ct.id = cs.team_id 
						inner join " . $wpdb->prefix . "command_order_items oi on r.order_item_id = oi.id
						inner join " . $wpdb->prefix . "command_orders o on oi.order_id = o.id		
				  WHERE r.user_id = 0 AND
				  		o.created_by = " . $user_id . "
				  ORDER BY cs.start_date, r.id";
		$results = $wpdb->get_results($query, ARRAY_A);

		foreach($results as $data)
		{
			if(!isset($reservation[$data['id']]))
			{
				$reservation[$data['id']] = array('team' => $data['team_id'],
												  'post' => $data['post_id'],	 
												  'hash' => $data['hash'],
												  'start_date' => $data['start_date']);
				$reservation[$data['id']]['days_until'] = self::days_until($data['start_date']);
			}
		}

		return $reservation;
	}

	/**
	 * Gets next RSVP id for the user
	 *
	 * @return integer Next RSVP ID
	 */
	public function next_for_user()
	{
		return (isset($this->user_next_rsvp) ? $this->user_next_rsvp : 0);	
	}

	/**
	 * Gets last RSVP id for the user
	 *
	 * @return integer Last RSVP ID
	 */
	public function last_for_user()
	{
		return (isset($this->user_last_rsvp) ? $this->user_last_rsvp : 0);	
	}

	/**
	 * Gets the days until the date identified
	 *
	 * @param string $what_date Date in string format
	 *
	 * @return integer Number of days until identified date
	 */
	private function days_until($what_date)
	{
		$tomorrow = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));
		$start = strtotime($what_date);
		
		$difference = max(0, $start - $tomorrow);
		$days = round($difference / (3600*24));
		
		return $days;
	}
	
	//
	//2018-03-27 DJT Addition of state selector
	public function Selector_States($Selected = 0)
	{
		$output = '';
        $output .= '<select name="state" id="state">' . self::state_options($Selected) . '</select>';
		
		return $output;
	}
	
	/**
	 * Payment Fields
	 */

	/**
	 * Creates table-formated form fields for user personal data
	 * 
	 * @param boolean $add_user If true, set values for inputs to user data
	 * 
	 * @return string $output The HTML formatted form output
	 */
	public function payment_personal($add_user = FALSE)
	{
		$output = '';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="firstname">First Name*</label></TH>';
        $output .= '    <TD><input name="firstname" id="firstname" class="regular-text required_personal" type="text" value="' . (($add_user && isset($this->user_data['first_name'])) ? $this->user_data['first_name'] : '') . '" size="50" maxlength="50" required="true"/></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="lastname">Last Name*</label></TH>';
        $output .= '    <TD><input name="lastname" id="lastname" class="regular-text required_personal" type="text" value="' . (($add_user && isset($this->user_data['last_name'])) ? $this->user_data['last_name'] : '') . '" size="50" maxlength="50" required="true"/></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="phone">Phone*</label></TH>';
		//
		//2018-04-13 DJT Added phone number disclaimer, moved TD close
        //$output .= '    <TD><input name="phone" id="phone" class="regular-text required_personal" type="text" value="' . (($add_user && isset($this->user_data['phone'])) ? $this->user_data['phone'] : '') . '" size="50" maxlength="14" required="true"/></TD>';
        $output .= '    <TD><input name="phone" id="phone" class="regular-text required_personal" type="text" value="' . (($add_user && isset($this->user_data['phone'])) ? $this->user_data['phone'] : '') . '" size="50" maxlength="14" required="true"/>';
        $output .= '    International Service Learning will ONLY use phone numbers to communicate important travel-related information with our volunteers.';
        $output .= '    </TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="email">Email*</label></TH>';
        $output .= '    <TD><input name="email" id="email" class="regular-text required_personal" type="text" value="' . (($add_user && isset($this->user_data['email'])) ? $this->user_data['email'] : '') . '" size="50" maxlength="50" required="true"/></TD>';
        $output .= '</TR>';

		return $output;
	}

	/**
	 * Builds the payment address form in table format
	 * 
	 * @param boolean $add_user If true, set values for inputs to user data
	 * 
	 * @return string $output The HTML formatted form output
	 */
	public function payment_address($add_user = FALSE)
	{

		$output = '';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="addr1">Address Line 1*</label></TH>';
        $output .= '    <TD><input name="addr1" id="addr1" class="regular-text required_addr" type="text" value="' . (($add_user && isset($this->user_data['addr1'])) ? $this->user_data['addr1'] : '') . '" size="50" maxlength="50" required="true"/></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="addr2">Address Line 2</label></TH>';
        $output .= '    <TD><input name="addr2" id="addr2" class="regular-text" type="text" value="' . (($add_user && isset($this->user_data['addr2'])) ? $this->user_data['addr2'] : '') . '" size="50" maxlength="50"/></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="city">City*</label></TH>';
        $output .= '    <TD><input name="city" id="city" class="regular-text required_addr" type="text" value="' . (($add_user && isset($this->user_data['city'])) ? $this->user_data['city'] : '') . '" size="50" maxlength="50" required="true"/></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="state">State*</label></TH>';
        $output .= '    <TD><select name="state" id="state" class="required_addr required="true"">' . (($add_user && isset($this->user_data['state'])) ? self::state_options($this->user_data['state']) : self::state_options()) . '</select></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="zip">Zip Code* / Postal Code*</label></TH>';
        $output .= '    <TD><input name="zip" id="zip" class="regular-text required_addr" type="text" value="' . (($add_user && isset($this->user_data['zip'])) ? $this->user_data['zip'] : '') . '" size="50" maxlength="10" required="true"/></TD>';
        $output .= '</TR>';

		return $output;
	}

	/**
	 * Builds the payment card form in table format
	 * 
	 * @param boolean $add_user If true, set values for inputs to user data
	 * 
	 * @return string $output The HTML formatted form output
	 */
	public function payment_card($add_user = '')
	{
		//
		//2018-02-16 DJT Removed exdtra quote from end of required = "true" for both card and exp_year
		
		$output = '';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="card">Card Type</label></TH>';
        $output .= '    <TD><select name="card" id="card" class="required_card" required="true">' . self::card_options('') . '</select></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        //
		//2018-05-06 DJT Changed Account Number to Card Number
		//$output .= '    <TH scope="row"><label for="acctnum">Account Number</label></TH>';
        $output .= '    <TH scope="row"><label for="acctnum">Card Number</label></TH>';
        $output .= '    <TD><input name="acctnum" id="acctnum" class="regular-text" type="text" value="" size="50" maxlength="50" required="true"/></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="exp_month">Expiration</label></TH>';
        $output .= '    <TD>';
        $output .= '         <select name="exp_month" id="exp_month" class="required_card" required="true">' . self::month_options('') . '</select>';
        $output .= '         <select name="exp_year" id="exp_year" class="required_card" required="true">' . self::year_options() . '</select></TD>';
        $output .= '</TR>';
        $output .= '<TR>';
        $output .= '    <TH scope="row"><label for="cvv2">CVV2</label></TH>';
        $output .= '    <TD><input name="cvv2" id="cvv2" class="regular-text required_addr" type="text" value="" size="50" maxlength="10" required="true"/></TD>';
        $output .= '</TR>';

		return $output;
	}


	/**
	 * Loads wordpress's current user data into this's user_data
	 */
	private function get_user_data()
	{
		if($this->user > 0)
		{
			$current_user = get_user_by('ID', $this->user);

			$this->user_data['first_name'] = $current_user->user_firstname;
			$this->user_data['last_name'] = $current_user->user_lastname;
			$this->user_data['email'] = $current_user->user_email;
					
			$this->user_data['addr1'] = get_field('mailing_address_line_1', 'user_' . $current_user->ID);
			$this->user_data['addr2'] = get_field('mailing_address_line_2', 'user_' . $current_user->ID);
			$this->user_data['city'] = get_field('city', 'user_' . $current_user->ID);
			$this->user_data['state'] = get_field('state', 'user_' . $current_user->ID);
			$this->user_data['zip'] = get_field('zip_code', 'user_' . $current_user->ID);
			$this->user_data['phone'] = get_field('primary_phone', 'user_' . $current_user->ID);
		}
		else
		{
			$this->user_data = array();
		}
	}
	
	/**
	 * Gets the Drop-down HTML for different cards
	 *
	 * @param string $selected Value of the selected CC field
	 *
	 * @return string HTML output for CC drop-down
	 */
	private function card_options($selected = '')
	{
		
		$output = '';
		$output .= '     <option value=""' . ($selected=='' ? ' SELECTED' : '') . '>-- SELECT --</option>';
		$output .= '     <option value="AMEX"' . ($selected=='AMEX' ? ' SELECTED' : '') . '>American Express</option>';
		$output .= '     <option value="DISC"' . ($selected=='DISC' ? ' SELECTED' : '') . '>Discover</option>';
		$output .= '     <option value="MACA"' . ($selected=='MACA' ? ' SELECTED' : '') . '>Mastercard</option>';
		$output .= '     <option value="VISA"' . ($selected=='VISA' ? ' SELECTED' : '') . '>VISA</option>';

		return $output;
	}

	/**
	 * Gets HTML for drop-down of states
	 *
	 * @param string $selected value of the selected state
	 *
	 * @return string HTML formatted string for drop down for states
	 */
	private function state_options($selected = '')
	{
		
		$output = '';
		$output .= '     <option value=""' . ($selected=='' ? ' SELECTED' : '') . '>-- SELECT STATE--</option>';
		$output .= '     <option value="1"' . ($selected=='1' ? ' SELECTED' : '') . '>Alabama</option>';
		$output .= '     <option value="2"' . ($selected=='2' ? ' SELECTED' : '') . '>Alaska</option>';
		$output .= '     <option value="3"' . ($selected=='3' ? ' SELECTED' : '') . '>Arizona</option>';
		$output .= '     <option value="4"' . ($selected=='4' ? ' SELECTED' : '') . '>Arkansas</option>';
		$output .= '     <option value="5"' . ($selected=='5' ? ' SELECTED' : '') . '>California</option>';
		$output .= '     <option value="6"' . ($selected=='6' ? ' SELECTED' : '') . '>Colorado</option>';
		$output .= '     <option value="7"' . ($selected=='7' ? ' SELECTED' : '') . '>Connecticut</option>';
		$output .= '     <option value="8"' . ($selected=='8' ? ' SELECTED' : '') . '>Delaware</option>';
		$output .= '     <option value="61"' . ($selected=='61' ? ' SELECTED' : '') . '>District Of Columbia</option>';
		$output .= '     <option value="9"' . ($selected=='9' ? ' SELECTED' : '') . '>Florida</option>';
		$output .= '     <option value="10"' . ($selected=='10' ? ' SELECTED' : '') . '>Georgia</option>';
		$output .= '     <option value="11"' . ($selected=='11' ? ' SELECTED' : '') . '>Hawaii</option>';
		$output .= '     <option value="12"' . ($selected=='12' ? ' SELECTED' : '') . '>Idaho</option>';
		$output .= '     <option value="13"' . ($selected=='13' ? ' SELECTED' : '') . '>Illinois</option>';
		$output .= '     <option value="14"' . ($selected=='14' ? ' SELECTED' : '') . '>Indiana</option>';
		$output .= '     <option value="15"' . ($selected=='15' ? ' SELECTED' : '') . '>Iowa</option>';
		$output .= '     <option value="16"' . ($selected=='16' ? ' SELECTED' : '') . '>Kansas</option>';
		$output .= '     <option value="17"' . ($selected=='17' ? ' SELECTED' : '') . '>Kentucky</option>';
		$output .= '     <option value="18"' . ($selected=='18' ? ' SELECTED' : '') . '>Louisiana</option>';
		$output .= '     <option value="19"' . ($selected=='19' ? ' SELECTED' : '') . '>Maine</option>';
		$output .= '     <option value="20"' . ($selected=='20' ? ' SELECTED' : '') . '>Maryland</option>';
		$output .= '     <option value="21"' . ($selected=='21' ? ' SELECTED' : '') . '>Massachusetts</option>';
		$output .= '     <option value="22"' . ($selected=='22' ? ' SELECTED' : '') . '>Michigan</option>';
		$output .= '     <option value="23"' . ($selected=='23' ? ' SELECTED' : '') . '>Minnesota</option>';
		$output .= '     <option value="24"' . ($selected=='24' ? ' SELECTED' : '') . '>Mississippi</option>';
		$output .= '     <option value="25"' . ($selected=='25' ? ' SELECTED' : '') . '>Missouri</option>';
		$output .= '     <option value="26"' . ($selected=='26' ? ' SELECTED' : '') . '>Montana</option>';
		$output .= '     <option value="27"' . ($selected=='27' ? ' SELECTED' : '') . '>Nebraska</option>';
		$output .= '     <option value="28"' . ($selected=='28' ? ' SELECTED' : '') . '>Nevada</option>';
		$output .= '     <option value="29"' . ($selected=='29' ? ' SELECTED' : '') . '>New Hampshire</option>';
		$output .= '     <option value="30"' . ($selected=='30' ? ' SELECTED' : '') . '>New Jersey</option>';
		$output .= '     <option value="31"' . ($selected=='31' ? ' SELECTED' : '') . '>New Mexico</option>';
		$output .= '     <option value="32"' . ($selected=='32' ? ' SELECTED' : '') . '>New York</option>';
		$output .= '     <option value="33"' . ($selected=='33' ? ' SELECTED' : '') . '>North Carolina</option>';
		$output .= '     <option value="34"' . ($selected=='34' ? ' SELECTED' : '') . '>North Dakota</option>';
		$output .= '     <option value="35"' . ($selected=='35' ? ' SELECTED' : '') . '>Ohio</option>';
		$output .= '     <option value="36"' . ($selected=='36' ? ' SELECTED' : '') . '>Oklahoma</option>';
		$output .= '     <option value="37"' . ($selected=='37' ? ' SELECTED' : '') . '>Oregon</option>';
		$output .= '     <option value="38"' . ($selected=='38' ? ' SELECTED' : '') . '>Pennsylvania</option>';
		$output .= '     <option value="39"' . ($selected=='39' ? ' SELECTED' : '') . '>Rhode Island</option>';
		$output .= '     <option value="40"' . ($selected=='40' ? ' SELECTED' : '') . '>South Carolina</option>';
		$output .= '     <option value="41"' . ($selected=='41' ? ' SELECTED' : '') . '>South Dakota</option>';
		$output .= '     <option value="42"' . ($selected=='42' ? ' SELECTED' : '') . '>Tennessee</option>';
		$output .= '     <option value="43"' . ($selected=='43' ? ' SELECTED' : '') . '>Texas</option>';
		$output .= '     <option value="44"' . ($selected=='44' ? ' SELECTED' : '') . '>Utah</option>';
		$output .= '     <option value="45"' . ($selected=='45' ? ' SELECTED' : '') . '>Vermont</option>';
		$output .= '     <option value="46"' . ($selected=='46' ? ' SELECTED' : '') . '>Virginia</option>';
		$output .= '     <option value="47"' . ($selected=='47' ? ' SELECTED' : '') . '>Washington</option>';
		$output .= '     <option value="48"' . ($selected=='48' ? ' SELECTED' : '') . '>West Virginia</option>';
		$output .= '     <option value="49"' . ($selected=='49' ? ' SELECTED' : '') . '>Wisconsin</option>';
		$output .= '     <option value="50"' . ($selected=='50' ? ' SELECTED' : '') . '>Wyoming</option>';
		$output .= '     <option value="51"' . ($selected=='51' ? ' SELECTED' : '') . '>Alberta</option>';
		$output .= '     <option value="52"' . ($selected=='52' ? ' SELECTED' : '') . '>British Columbia</option>';
		$output .= '     <option value="53"' . ($selected=='53' ? ' SELECTED' : '') . '>Manitoba</option>';
		$output .= '     <option value="54"' . ($selected=='54' ? ' SELECTED' : '') . '>New Brunswick</option>';
		$output .= '     <option value="55"' . ($selected=='55' ? ' SELECTED' : '') . '>Newfoundland and Labrador</option>';
		$output .= '     <option value="56"' . ($selected=='56' ? ' SELECTED' : '') . '>Nova Scotia</option>';
		$output .= '     <option value="57"' . ($selected=='57' ? ' SELECTED' : '') . '>Ontario</option>';
		$output .= '     <option value="58"' . ($selected=='58' ? ' SELECTED' : '') . '>Prince Edward Island</option>';
		$output .= '     <option value="59"' . ($selected=='59' ? ' SELECTED' : '') . '>Quebec</option>';
		$output .= '     <option value="60"' . ($selected=='60' ? ' SELECTED' : '') . '>Saskatchewan</option>';
		$output .= '     <option value="70"' . ($selected=='70' ? ' SELECTED' : '') . '>Puerto Rico</option>';

		return $output;
	}

	/**
	 * Gets HTML for drop-down of months
	 *
	 * @param string $selected value of the selected month
	 *
	 * @return string HTML formatted string for drop down for months
	 */
	private function month_options($selected = '')
	{
		
		$output = '';
		$output .= '     <option value=""' . ($selected=='' ? ' SELECTED' : '') . '>-- MONTH --</option>';
		$output .= '     <option value="JAN"' . ($selected=='JAN' ? ' SELECTED' : '') . '>January</option>';
		$output .= '     <option value="FEB"' . ($selected=='FEB' ? ' SELECTED' : '') . '>February</option>';
		$output .= '     <option value="MAR"' . ($selected=='MAR' ? ' SELECTED' : '') . '>March</option>';
		$output .= '     <option value="APR"' . ($selected=='APR' ? ' SELECTED' : '') . '>April</option>';
		$output .= '     <option value="MAY"' . ($selected=='MAY' ? ' SELECTED' : '') . '>May</option>';
		$output .= '     <option value="JUN"' . ($selected=='JUN' ? ' SELECTED' : '') . '>June</option>';
		$output .= '     <option value="JUL"' . ($selected=='JUL' ? ' SELECTED' : '') . '>July</option>';
		$output .= '     <option value="AUG"' . ($selected=='AUG' ? ' SELECTED' : '') . '>August</option>';
		$output .= '     <option value="SEP"' . ($selected=='SEP' ? ' SELECTED' : '') . '>September</option>';
		$output .= '     <option value="OCT"' . ($selected=='OCT' ? ' SELECTED' : '') . '>October</option>';
		$output .= '     <option value="NOV"' . ($selected=='NOV' ? ' SELECTED' : '') . '>November</option>';
		$output .= '     <option value="DEC"' . ($selected=='DEC' ? ' SELECTED' : '') . '>December</option>';

		return $output;
	}

	/**
	 * Gets HTML for drop-down of years
	 *
	 * @param string $selected value of the selected year
	 *
	 * @return string HTML formatted string for drop down for years
	 */
	private function year_options($all = false, $selected = '')
	{
		
		$output = '';
		$output .= '     <option value=""' . ($selected=='' ? ' SELECTED' : '') . '>-- YEAR --</option>';

		$start_year = (($all) ? 2000 : date("Y"));
		$end_year = date("Y") + 20;
		
		for($i = $start_year; $i <= $end_year; $i++)
		{ 
			$output .= '     <option value="' . $i . '"' . ($selected==$i ? ' SELECTED' : '') . '>' . $i . '</option>';
		}
		
		return $output;
	}

	/**
	 * Builds WP page for rsvp
	 * 
	 * @param string $title Page title
	 * @param string $name Page name
	 * 
	 * @return integer $post_id This page/post's id
	 */
	private function build_page($title = '', $name = '')
	{
		$field_data=array(	'post_author' => $this->user,
						'post_date' => date('Y-m-d H:i:s'),
						'post_date_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
						'post_content' => ' ',
						'post_title' => $title,
						'post_status' => 'publish',
						'comment_status' => 'closed',
						'ping_status' => 'closed',
						'post_name' => $name,
						'post_modified' => date('Y-m-d H:i:s'),
						'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
						'post_parent' => 0,
						'menu_order' => 0,
						'post_type' => 'isl_rsvp',
						'page_template' => '',
						'comment_count' => 0
					);
		$post_id = wp_insert_post($field_data, true);	
	
		return $post_id;
	}

}