<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * TEAM class
 * manages team data
 */
CLASS TEAM
{
	/** @var integer $team ID of the team */
	private $team = NULL;

	/** @var array $team_data Array of team data */
	private $team_data = array();

	/** @var array $teams List of teams */
	private $teams = array();

	/** @var array $schedule Schedule in array form */
	private $schedule = array();

	/** @var datetime $departure Departure date/time */
	private $departure = NULL;

	/** @var datetime $arrival Arrival date/time */
	private $arrival = NULL;

	/** @var float $cost Cost for the trip */
	private $cost = 0;
		
	/** @var array $translate Array of fields for the database table */
	private $translate = array();

	/** @var array $formats Not sure */
	private $formats = array();

	/** @var string $team_table Database table for teams */
	private $team_table = NULL;

	/** @var string $sched_table Database table for schedules */
	private $sched_table = NULL;

	/** @var string $poc_label Database suffix for PoC (Point of Contact) */
	private $poc_label = '_command_team_poc';

	/** @var string $poc_key String Key for the Staff for chats */
	private $poc_key = "_staff_pos";
	
	/** @var integer $lead_days Number of days for lead */
	private $lead_days = 2;

	/** @var integer $custom_post_id ID for custom post for teams */
	private $custom_post_id = 4087;

	private $possible_team_count = 0;
	
	/** @var integer $user ID of the user */
	private $user = NULL;
	
	/**
	 * Default Constructor
	 *
	 * Sets up the DB tables, and DB connection, loads the translate table to have local fields associated with DB fields and creates a blank schedule
	 */
	public function __construct()
	{
		global $wpdb;
		$this->team_table = $wpdb->prefix . "command_TEAM";
		$this->sched_table = $wpdb->prefix . "command_schedule";

		$this->user = get_current_user_id();

		$this->team_data = self::blank();
		
		$this->translate['id'] = 'id';
		$this->translate['post'] = 'post_id';
		$this->translate['status'] = 'team_status_id';
		$this->translate['type'] = 'team_type_id';
		$this->translate['page_id'] = 'page_id';
		$this->translate['code'] = 'kode';
		$this->translate['hash'] = 'hash';
		$this->translate['arrival_city'] = 'arrival_city';
		$this->translate['arrival_country'] = 'arrival_country';
		$this->translate['departure_city'] = 'departure_city';
		$this->translate['departure_country'] = 'departure_country';
		$this->translate['min'] = 'minimum_volunteers';
		$this->translate['max'] = 'maximum_volunteers';
		$this->translate['cost'] = 'cost';
		$this->translate['appfee'] = 'appfee';
		$this->translate['deposit'] = 'deposit';
		$this->translate['team_chat'] = 'team_chat';
		$this->translate['messaging'] = 'messaging';
		$this->translate['no_finances'] = 'no_finances';
		
		self::blank_schedule();
	}
	
	/**
	 * Methods to deal with a single Team
	 */

	/**
	 * Builds team data if the object is a team, else clear team data
	 * 
	 * @param integer $team_id The id of the team
	 */
	public function set_team($team_id)
	{
		//
		// if the team exists, clear it and rebuild the data
		if(self::is_team($team_id))
		{
			global $wpdb;
			$this->team = $team_id;
			$fields = self::field_list();
			
			$query = "SELECT " . $fields . " FROM " . $this->team_table . " WHERE id=" . $team_id;
			//echo $query;
			$results = $wpdb->get_row($query, ARRAY_A);
				
			foreach($this->translate as $field=>$table_field)
			{
				$this->team_data[$field] = $results[$table_field];
			}

			unset($this->arrival);
			unset($this->departure);
			$this->cost = 0;
			$this->poc_key = "cmd_team". strtolower($this->team) . $this->poc_key;
			
			self::generate_schedule();
			self::get_poc();
			
			self::valid_check();

		}
		//
		// else clear the current team data
		else
		{
			$this->team_data = self::blank();
			unset($this->team);
		}
	}

	/**
	 * Returns the team id
	 *
	 * @return integer Team id
	 */
	public function get_team()
	{
		return isset($this->team) ? $this->team : NULL;
	}
	
	/**
	 * Gets team id by name of the team
	 *
	 * @param string $check Name of the team or hash of the team
	 *
	 * @return integer Team ID
	 */
	public function find_team_by_name($check)
	{
		global $wpdb;
		
		if(substr($check,0,1) == "#")
		{
			$hash = $check;
			$team = substr($check, 1);
		}
		else
		{
			$hash = '#' . $check;
			$team = $check;
		}
		$query = "SELECT id FROM " . $this->team_table . " WHERE hash='" . $hash . "' OR kode='" . $team . "'";
		$check = $wpdb->get_var($query);

		return $check;
	}

	/**
	 *  Gets the value for the field from the team data
	 *
	 *  @param string $field Name of the field to get the value for
	 *
	 *  @return string Value of the field requested OR null if the team isn't set
	 */
	public function value($field)
	{
		return isset($this->team) ? $this->team_data[$field] : NULL;
	}

	/**
	 *  Checks if the team exists
	 *
	 *  @param integer $team_id ID of the team
	 *
	 *  @return boolean True if team exists ELSE false
	 */
	public function is_team($team_id)
	{
		if(empty($team_id))
		{
				return false;
		}
		else
		{
			global $wpdb;
			$check = $wpdb->get_var("SELECT id FROM " . $this->team_table . " WHERE id=" . $team_id);
			return $check==$team_id ? TRUE : FALSE;
		}
	}

	/**
	 *  Checks if the team is a custom team
	 *
	 *  @param integer $team_id ID of the team
	 *
	 *  @return boolean True if team is a custom team
	 */
	public function is_custom_team($team_id = 0)
	{
		if(!empty($team_id))
		{
			global $wpdb;
			$check = $wpdb->get_var("SELECT post_id FROM " . $this->team_table . " WHERE id=" . $team_id);
			return get_post_meta($check, '_cmd_custom_team_build', true);
		}
		else
		{
			return get_post_meta(self::value('post'), '_cmd_custom_team_build', true);
		}
	}

	/**
	 *  Checks if the team is an internship request
	 *
	 *  @param integer $team_id ID of the team
	 *
	 *  @return boolean True if team is an internship request
	 */
	public function is_internship($team_id = 0)
	{
		if(!empty($team_id))
		{
			global $wpdb;
			$check = $wpdb->get_var("SELECT post_id FROM " . $this->team_table . " WHERE id=" . $team_id);
			return get_post_meta($check, '_cmd_internship_request', true);
		}
		else
		{
			return get_post_meta(self::value('post'), '_cmd_internship_request', true);
		}
	}

	/**
	 * Builds the team object and adds data
	 * 
	 * @param array $elements The elements that will be used to build the team
	 */
	public function build($elements = array())
	{
		if(isset($elements['hash']) &&
		   isset($elements['type']) &&
		   isset($elements['status']) &&
		   isset($elements['min']) &&
		   isset($elements['max']) &&
		   self::unique_hash($elements['hash'])
		  )
		{
			global $wpdb;

			$team_type = $wpdb->get_var('SELECT cpost FROM ' . $wpdb->prefix . 'command_team_type WHERE id=' . $elements['type']);

			//
			//Generate empty post
			$post_id = self::generate_post($elements['hash'],$elements['hash'], $team_type);

			//
			//Fix max elements if they are too small...
			$elements['max'] = max($elements['max'],$elements['min']);
			
			//
			//Arrange data via translations
			$data = array();
			$format = array();
			foreach($elements as $field=>$value)
			{
				$data[$this->translate[$field]] = $value;			
			}
			$data['post_id'] = $post_id;
			$data['created'] = date('Y-m-d h:i:s');
			$data['created_by'] = $this->user;
			$data['kode'] = $elements['hash'];

			//
			//Perform the actual insertion and make sure it happened
			$temp_team_id = 0;
			$check = $wpdb->insert($this->team_table, $data);

			if($check)
			{
				$temp_team_id = $wpdb->insert_id;
			}
			else
			{
				wp_delete_post($post_id,true);
			}

			self::set_team($temp_team_id);
			self::update_cp();

			return;
		}

		self::set_team(0);
	}

	/**
	 * Updates the element to the value in the database and object
	 * 
	 * @param string $element The item/element to be changed
	 * @param string $value The new value of the element/item
	 */
	public function update($element, $value)
	{

		//
		// update the team to be last modified now by the current user
		if(isset($this->team))
		{
			global $wpdb;
			$old_value = $this->value($element);

			do_action('cmd_team_update_prior', $this->team, $element, $this->team_data[$element] , $value);
			
			$updates = array($this->translate[$element] => $value, 
							 'modified' => date('Y-m-d h:i:s'),
							 'modified_by' => $this->user
							 );
			$success = $wpdb->update($this->team_table, $updates, array('id' => $this->team));

			if($success)
			{
				do_action('cmd_team_update_after', $this->team, $element, $this->team_data[$element] , $value);
				$this->team_data[$element] = $value;

				if($element == "hash")
				{
					if(strlen($value) <=3 )
					{
						$wpdb->update($this->team_table, array('hash' => '#' . $this->value('code') ), array('id' => $this->team));
					}
				}
				
				if($element == "type")
				{
					//
					//Update Post status
					$cpost = $wpdb->get_var("SELECT cpost FROM " . $wpdb->prefix . "command_team_type WHERE id=" . $value);
					wp_update_post(array('ID' => $this->team_data['post'], 
										 'post_type' => $cpost,
										 'post_modified' => date('Y-m-d H:i:s'), 
										 'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s'))
										));
				}
				
				/** STATUS PROCESSING */
				if($element == 'status')
				{
					//
					//Update Post status
					$query = "SELECT id, pre, avail, prep, trav, cancel FROM " . $wpdb->prefix . "command_team_status";
					$statuses = $wpdb->get_results($query, OBJECT_K);
					
					if($statuses[$value]->pre == 1)
						wp_update_post(array('ID' => $this->team_data['post'], 
											 'post_status' => 'draft', 
											 'post_modified' => date('Y-m-d H:i:s'), 
											 'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s'))
											));

					if($statuses[$value]->avail == 1)
						wp_update_post(array('ID' => $this->team_data['post'], 
											 'post_status' => 'publish', 
											 'post_modified' => date('Y-m-d H:i:s'), 
											 'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s'))
											));

					if($statuses[$value]->prep == 1)
						wp_update_post(array('ID' => $this->team_data['post'], 
											 'post_status' => 'publish', 
											 'post_modified' => date('Y-m-d H:i:s'), 
											 'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s'))
											));

					if($statuses[$value]->trav == 1)
						wp_update_post(array('ID' => $this->team_data['post'], 
											 'post_status' => 'private', 
											 'post_modified' => date('Y-m-d H:i:s'), 
											 'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s'))
											));

					if($statuses[$value]->cancel == 1)
						wp_update_post(array('ID' => $this->team_data['post'], 
											 'post_status' => 'trash', 
											 'post_modified' => date('Y-m-d H:i:s'), 
											 'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s'))
											));


				}

				//
				// update the hash for the post
				if($element == 'hash')
				{
					$updates = array ('ID' => $this->team_data['post'],
									  'post_title' => $value,
									  'post_name' => strtolower(str_replace(' ', '-', $value))
									 );
					wp_update_post ($updates);
				}
			}
			
			self::update_cp();
		}
	}

	/**
	 * Sets the point of contact for the team
	 * 
	 * @param array $ids id's of the poc
	 */
	public function set_poc($ids = array())
	{
		if(!empty($ids))
		{
			update_post_meta($this->team_data['post'], $this->poc_label, $ids);
			self::get_poc();
		}
	}

	/**
	 * Sets this team_data's poc info from team post data
	 */
	public function get_poc()
	{
		if(!empty($this->team_data['post']))
		{
			$data = get_post_meta($this->team_data['post'], $this->poc_label, false);
		}

		if(!empty($data))
		{
			$this->team_data['poc'] = $data[0];
		}
		else
		{
			$this->team_data['poc'] = array();
		}
	}
	
	/**
	 * Methods to deal with multiple Teams, such as lists
	 */

	public function possible_teams()
	{
		return $this->possible_team_count;
	}
	
	/**
	 * Gets the number of teams
	 *
	 * @return integer Number of teams
	 */
	public function team_count()
	{
		return count($this->teams);
	}

	/**
	 * Gets the list of teams
	 *
	 * @return array List of teams OR an empty array if there are no teams
	 */
	public function get_list()
	{
		return count($this->teams) > 0 ? $this->teams : array();
	}
	
	private function user_role()
	{
		$roles = get_user_meta(get_current_user_id(), 'wp_capabilities', true);
		$roles = array_keys($roles);
		reset ($roles);
		return current($roles);
	}
		/**
	 * Creates list of teams
	 *
	 * @param array $criteria Criteria for creating the team list
	 * @param integer $start The start of the team list
	 * @param integer $records The number of records to be requested
	 */
	public function create_list($criteria = array(), $start = 0, $records = 50)
	{
		global $wpdb;
		$clause = "";
		$date_set = 0;
		$anytime = false;
		$usr = wp_get_current_user();

		
		if(in_array("enhanced", $usr->roles))
		{
			$poc = get_user_meta( $usr->ID, 'cmd_custom_team_poc', true);	
		}
		else
		{
			$poc = false;
		}
		
		//
		//2018-02-19 DJT Added removal of cancelled teams if not other status is chosen.
		if(empty($cancel_check) && !isset($criteria['status']))
		{
			$cancel_check = true;
			$clause .= $this->translate['status'] . " !=5 AND ";
		}

		//
		// iterate through criteria to build query pieces
		foreach($criteria as $element=>$value)
		{
			if($element == 'code')
			{
				if(strlen($value) > 2)
				{
					$hash = (substr($value, 0, 1) != '#' ? '#%' . $value : $value);
					$code = (substr($value, 0, 1) == '#' ? substr($value,1) : $value);
					$clause .= "(hash like '" . $hash . "%' OR kode like '%" . $code . "%') AND "	;
				}
				$anytime = true;
			} 
			else
			{

				if(isset($this->translate[$element]) && (is_array($value) || $value > 0))
				{
					if(is_array($value))
					{
						$em ='';
						foreach($value as $index_val=>$true_val)
						{
							$em .= "'" . $true_val . "', ";
						}
					
						$clause .= $this->translate[$element] . " in(" . substr($em, 0, -2) . ") AND ";	
					}
					else
					{
						$clause .= $this->translate[$element] . "='" . $value . "' AND ";	
					}
					
					if($this->translate[$element] == 'team_type_id' && $value == 26)
					{
						$anytime = true;
						$schedule = true;
					}
					
				}
				else
				{
					if($element == 'custom_teams' && $value > 0)
					{
						$c_teams = array();
						$c_teams[] = 0;
						
						$query = "SELECT team.id
									FROM " . $this->team_table . " team 
										INNER JOIN " . $wpdb->prefix . "postmeta meta ON team.post_id = meta.post_id
										INNER JOIN " . $wpdb->prefix . "command_team_status cts on team.team_status_id = cts.id
									WHERE meta.meta_key =  '_cmd_custom_team_build' AND
										meta.meta_value > 0 AND
										cts.pre = 1"; 
						$possibles = $wpdb->get_results($query, ARRAY_A);
						
						foreach($possibles as $data)
						{
							$c_teams[] = $data['id'];
						}			
						
						$clause .= $this->team_table . ".id in (" . implode(",",$c_teams) . ") AND ";
						
						$anytime = true;
					}
					if($element == 'internship' && $value > 0)
					{
						$i_teams = array();
						$i_teams[] = 0;
						
						$query = "SELECT team.id
									FROM " . $this->team_table . " team 
										INNER JOIN " . $wpdb->prefix . "postmeta meta ON team.post_id = meta.post_id
										INNER JOIN " . $wpdb->prefix . "command_team_status cts on team.team_status_id = cts.id
									WHERE meta.meta_key =  '_cmd_internship_request' AND 
										meta.meta_value > 0 AND
										cts.pre = 1"; 
						$possibles = $wpdb->get_results($query, ARRAY_A);
						
						foreach($possibles as $data)
						{
							$i_teams[] = $data['id'];
						}			
						
						$clause .= $this->team_table . ".id in (" . implode(",",$i_teams) . ") AND ";
						
						$anytime = true;
					}
					if($element == 'country' && !empty($value))
					{
						if(is_array($value))
						{
							$clause .= "cs.country_id in (" . implode(",", $value) . ") AND ";
						}
						else
						{
							$clause .= "cs.country_id = " . $value . " AND ";
						}
						$schedule = true;
					}
					if($element == 'program' && !empty($value))
					{
						if(is_array($value))
						{
							$clause .= "cs.program_id in (" . implode(",", $value) . ") AND ";
						}
						else
						{
							$clause .= "cs.program_id = " . $value . " AND ";
						}
						$schedule = true;
					}
					if($element == 'start' && strlen($value)>6)
					{
						$clause .= "DATE(cs.start_date) >= STR_TO_DATE('" . $value . "', '%m-%d-%Y') AND ";
						$schedule = true;
						$date_set = 1;	
					}
					if($element == 'end' && strlen($value) > 6)
					{
						$clause .= "DATE(cs.end_date) <= STR_TO_DATE('" . $value . "', '%m-%d-%Y') AND ";
						$schedule = true;
						$date_set = 1;	
					}
					if($element == 'vec' && $value > 0)
					{
						$vec_teams = array();
						
						$query = "SELECT um.user_id, um.meta_key, um.meta_value, u.display_name
									FROM " . $wpdb->prefix . "usermeta um INNER JOIN
										 " . $wpdb->prefix . "users u on u.ID = um.user_id 
								  WHERE um.user_id = " . $value . " AND
								  	    um.meta_key like 'cmd_team%_staff_pos' AND 
										um.meta_value = 'VEC'
								  ORDER BY display_name";
						$possibles = $wpdb->get_results($query, ARRAY_A);

						foreach($possibles as $data)
						{
							$vec_teams[] = substr($data['meta_key'],8,strpos($data['meta_key'],'_',8)-8) ;
						}			
						
						if(!empty($vec_teams))
						{
							$clause .= $this->team_table . ".id in (" . implode(",",$vec_teams) . ") AND ";
						}
									
					}
					if($element == 'tm' && $value > 0)
					{
						$tm_teams = array();
						
						$query = "SELECT um.user_id, um.meta_key, um.meta_value, u.display_name
									FROM " . $wpdb->prefix . "usermeta um INNER JOIN
										 " . $wpdb->prefix . "users u on u.ID = um.user_id 
								  WHERE um.user_id = " . $value . " AND
								  	    um.meta_key like 'cmd_team%_staff_pos' AND 
										um.meta_value = 'Team Manager'
								  ORDER BY display_name";
						$possibles = $wpdb->get_results($query, ARRAY_A);
						
						foreach($possibles as $data)
						{
							$tm_teams[] = substr($data['meta_key'],8,strpos($data['meta_key'],'_',8)-8) ;
						}			
						
						if(!empty($tm_teams))
						{
							$clause .= $this->team_table . ".id in (" . implode(",",$tm_teams) . ") AND ";
						}
									
					}
				}
			}
		}
		if(!$date_set && !$anytime)
		{
			$first = mktime(0, 0, 0, date("m"), 1, date("Y"));
//			$first = mktime(0, 0, 0, 7, 1, date("Y"));
			$clause .= "DATE(cs.end_date) >= ('" . date("Y-m-d", $first) . "') AND ";
			$schedule = true;
		}

		//
		// query the database to get the teams
		$fields = self::field_list();
		$query = "SELECT DISTINCT " . $fields . " FROM " . $this->team_table;

		if(isset($schedule) && $schedule == true)
		{
			$query .= " LEFT JOIN " . $wpdb->prefix . "command_schedule cs ON " . $this->team_table . ".id = cs.team_id "; 
		}

		$query .= !empty($clause) ? " WHERE " . substr($clause,0,-4) : "";	

		if(isset($schedule) && $schedule == true)
		{
			$query .= " ORDER BY cs.start_date";
		}
		else
		{
			if(!empty($criteria['custom_teams']) && $criteria['custom_teams'] > 0)
			{
				$query .= " ORDER BY " . $this->team_table . ".id DESC";
			}
			else
			{
				$query .= " ORDER BY " . $this->team_table . ".id";
			}
		}
		
		$cnt_teams = "SELECT count(*) FROM (" . $query . ") cnts";
		$this->possible_team_count = $wpdb->get_var($cnt_teams);
		
		$query .= " LIMIT $records OFFSET $start";
		
		$recset = $wpdb->get_results($query, ARRAY_A);

		$this->teams = array();
		// 
		// adds teams to this
		foreach($recset as $record)
		{
			$add_team = false;
			switch(self::user_role())
			{
				case 'enhanced':
					if($poc)
					{
						$poc_data = get_post_meta($record['post_id'], 'cmd_team_poc', false);

						if($poc_data)
						{
							foreach($poc_data[0] as $email=>$name_info)
							{
								if($email == $usr->user_email)
								{
									$add_team = true;
								}
							}
						}
					}
					break;
				case 'staff':
				default:
					$add_team = true;
			}
			
			if($add_team)
			{
				$this->teams[$record['id']] = array();
				foreach($this->translate as $common_field=>$table_field)
				{
					$this->teams[$record['id']][$common_field] = $record[$table_field];
				}
			}
		}
	}

	/**
	 * Builds and returns a list of upcoming teams
	 * 
	 * @param integer $program Program id to for teams in a team
	 * @param integer $country Country id for teams in a country
	 */
	public function upcoming($program = 0, $country = 0, $trip_length = 0, 
							 $begin_date = '', $end_date = '', $order_by = 'date_early', $pages = array('standard','minors'))
	{
		global $wpdb;
		$post_table = $wpdb->prefix . "posts";
		$status_table = $wpdb->prefix . "command_team_status";
		$type_table = $wpdb->prefix . "command_team_type";
		$skip_dates = false;
		$lead_date = date("Y-m-d h:i:s", mktime(0, 0, 0, date("m"), date("d") + $this->lead_days, date("Y")));
		$countries = array();
		$programs = array();
		$teams = array();

		if(!empty($begin_date))
		{
			$b_month = substr($begin_date, 0,2);
			$b_day = substr($begin_date, 3,2);
			$b_year = substr($begin_date, 6);
			$begin_date = date("Y-m-d h:i:s", mktime(0, 0, 0, $b_month, $b_day, $b_year));				
		}

		if(!empty($end_date))
		{
			$e_month = substr($end_date, 0,2);
			$e_day = substr($end_date, 3,2);
			$e_year = substr($end_date, 6);
			$end_date = date("Y-m-d h:i:s", mktime(0, 0, 0, $e_month, $e_day, $e_year));				
		}

		if(in_array('internship', $pages))
		{
			$skip_dates = true;
		}
		
		//////////////////////////////////////////////////////
		// BUILD THE QUERY
		//////////////////////////////////////////////////////
		$query = "SELECT DISTINCT		
				 " . $this->team_table . ".id, 
				 " . $this->team_table . ".cost, 
				 " . $this->team_table . ".minimum_volunteers, 
				 " . $this->team_table . ".maximum_volunteers, 
				 " . $this->team_table . ".post_id, 
				 " . $this->team_table . ".kode as code, 
				 " . $post_table . ".post_title as team_name,"; 

		if($trip_length > 0)
		{
			$query .= "date_limits.programs program_id,
					   date_limits.countries country_id, ";
		}
		else
		{
			$query .= $this->sched_table . ".program_id, ";
			$query .= $this->sched_table . ".country_id, ";
			$query .= $this->sched_table . ".region_id, ";
		}
		
		if($trip_length > 0)
		{
			$query .= "date_limits.s_date start_date, 
				       date_limits.e_date end_date,
				 	   date_limits.days ";
			
		}
		else
		{
			$query .= $this->sched_table . ".start_date, ";
			$query .= $this->sched_table . ".end_date ";
		}

		$query .= "FROM " . $this->team_table . "
				   inner join " . $post_table . " on " . $this->team_table . ".post_id = " . $post_table . ".ID 	
				   inner join " . $status_table . " on " . $this->team_table . ".team_status_id = " . $status_table . ".id
				   inner join " . $type_table . " on " . $this->team_table . ".team_type_id = " . $type_table . ".id
				   inner join " . $this->sched_table . " on " . $this->team_table . ".id = " . $this->sched_table . ".team_id";
				
		if($trip_length > 0)
		{
			$query .= " inner join 
						(SELECT 
  							edate.team_id, 
							min(sdate.start_date) s_date, 
							max(edate.end_date) e_date, 
							DATEDIFF(max(edate.end_date), min(sdate.start_date)) +1 days,
							GROUP_CONCAT(DISTINCT sdate.program_id SEPARATOR',') programs,
							GROUP_CONCAT(DISTINCT sdate.country_id SEPARATOR',') countries
						 FROM 
  							wp_command_schedule sdate 
  								INNER JOIN
									wp_command_schedule edate on sdate.team_id = edate.team_id
						GROUP BY
  							edate.team_id
						HAVING" 
						  . ($trip_length == 1 ? ' DATEDIFF(e_date, s_date) < 6' : '')
						  . ($trip_length == 2 ? ' DATEDIFF(e_date, s_date) >= 6 AND DATEDIFF(e_date, s_date) <= 13' : '')
						  . ($trip_length == 3 ? ' DATEDIFF(e_date, s_date) >= 14' : '')
						  . ($trip_length == 10 ? ' DATEDIFF(e_date, s_date) <= 29' : '')
						  . ($trip_length == 11 ? ' DATEDIFF(e_date, s_date) >= 30 AND DATEDIFF(e_date, s_date) <= 46' : '')
						  . ($trip_length == 12 ? ' DATEDIFF(e_date, s_date) >= 46 AND DATEDIFF(e_date, s_date) <= 61' : '')
						  . ($trip_length == 13 ? ' DATEDIFF(e_date, s_date) >= 62' : '')
					 	  .
					 ") date_limits on wp_command_TEAM.id = date_limits.team_id";
		}
		
		$query .= " WHERE 
			     " . $post_table . ".post_status = 'publish' AND 
				 " . $status_table . ".avail = 1 AND
				 " . $type_table . ".public = 1 AND
				 " . $type_table . ".active = 1 AND
				 " . $type_table . ".page in ('" . implode("','", $pages) . "')"
				  . (!$skip_dates ? " AND " . $this->sched_table . ".start_date > '" . (!empty($begin_date) ? $begin_date : $lead_date) . "'" : '')
				  . (!$skip_dates ? " AND " . $this->sched_table . ".start_date < '" . (!empty($end_date) ? $end_date : '2099-12-31') . "'" : '');
				  				
		$query .= " ORDER BY ";

		if($trip_length > 0)
		{
			$query .= (($order_by == "date_early") ? "date_limits.s_date " : '');
			$query .= (($order_by == "date_late") ? "date_limits.s_date DESC " : '');
		}
		else	
		{
			$query .= (($order_by == "date_early") ?  $this->sched_table . ".start_date " : ''); 
			$query .= (($order_by == "date_late") ?  $this->sched_table . ".start_date DESC " : '');
		}
		
		$query .= (($order_by == "price_low") ? $this->team_table . ".cost " : '');
		$query .= (($order_by == "price_high") ? $this->team_table . ".cost DESC " : '');

		//echo $query;
		$results = $wpdb->get_results($query, ARRAY_A);
		//echo $query;
		
		//
		// iterate through results to build the list of teams
		foreach($results as $data)
		{
			//
			//Checking country? and country matches
			if($country > 0)
			{
				if(isset($data['country_id']) && in_array($country, explode(',', $data['country_id'])))
				{
					$countries[] = $data['id'];
				}
			}
			else
			{
				$countries[] = $data['id'];
			}
	
			//
			//Checking program? and program matches
			if($program > 0)
			{
				if(isset($data['program_id']) && in_array($program, explode(',', $data['program_id'])))
				{
					$programs[] = $data['id'];
				}
			}
			else
			{
				$programs[] = $data['id'];
			}
	
		}

		//
		//Find all $countries and $programs that interset, as these are the ones we need to return
		$team_list = array_intersect($countries, $programs);
		
		//
		//Cycle through results again, but pick out from list and correct dates and multiples
		foreach($results as $data)
		{
			if(in_array($data['id'], $team_list))
			{
				if(isset($teams[$data['id']])	)
				{
					$teams[$data['id']]['end_data'] = $data['end_date'];				
					$teams[$data['id']]['program_id'] .= "," . $data['program_id'];				
					$teams[$data['id']]['country_id'] .= "," . $data['country_id'];				
					$teams[$data['id']]['region_id'] .= "," . $data['region_id'];				
				}
				else
				{
					$teams[$data['id']] = $data;	
					$teams[$data['id']]['teaser'] = get_post_meta($data['post_id'], 'teaser', true);
				}

				$teams[$data['id']]['url'] = get_permalink($data['post_id']);
				
				if(!strpos($teams[$data['id']]['url'],'/internships/') && !strpos($teams[$data['id']]['url'],'/team/'))
				{
					unset($teams[$data['id']]);
				}

			}
		}

		return $teams;
	}
	
	/**
	 * Gets the list of featured teams
	 * 
	 * @param array The list of featured teams
	 */
	public function featured_teams()
	{
		global $wpdb;
		
		$missing = 0;
		$tmp = explode(",", get_post_meta(4633, 'featured_teams', true));

		for($x = 0; $x <=7; $x++)
		{
			if(!isset($tmp[$x]) || empty($tmp[$x]) || $tmp[$x] == '')
			{
				$tmp[$x] = 0;
				$missing++;

				if(!isset($start_index))
				{
					$start_index = $x;	
				}
			}
		}

		if($missing)
		{
			$query = "SELECT DISTINCT ct.id 
					  FROM " . $this->team_table . " ct 
					  	INNER JOIN " . $this->sched_table . " cs on ct.id = cs.team_id
						INNER JOIN " . $wpdb->prefix . "command_team_type ctt on ct.team_type_id = ctt.id 
						INNER JOIN " . $wpdb->prefix . "command_team_status cts on ct.team_status_id = cts.id 
					  WHERE	cts.avail = 1 AND
					  	  	ctt.public = 1 AND
							ctt.cpost = 'isl_team' AND 
							ct.id not in (" . implode(",", $tmp) . ") AND
							cs.start_date > now()
					 ORDER BY cs.start_date DESC
					 LIMIT " . $missing;
			$options = $wpdb->get_results($query, ARRAY_A);
			foreach($options as $ids)
			{
				$tmp[$start_index] = $ids['id'];
				$start_index++;					
			}
		}

		return implode(",", $tmp);
	}

	/**
	 * Checks if the hash is unique
	 *
	 * @param string $hash The hash to be compared
	 * @param boolean $skipcurrent If true, skip the current team's hash
	 *
	 * @return boolean True if this hash is unique OR false if this hash is NOT unique
	 */
	public function unique_hash($hash = "", $skipcurrent = true)
	{
		global $wpdb;
		$query = "SELECT id FROM " . $this->team_table . " WHERE hash='" . $hash . "'";
		if($skipcurrent && isset($this->team))
		{
			$query .= " AND id != " . $this->team;	
		}
		$hash_check = $wpdb->get_var($query);
		return ((is_null($hash_check)) ? TRUE : FALSE);
	}
	
	/**
	 * checks if the code is unique
	 * 
	 * @param string $code The code to be compared
	 * @param boolean $skipcurrent If true, skip the current team's code
	 *
	 * @return boolean True if this code is unique false if this hash is NOT unique
	 */
	public function unique_code($code = "", $skipcurrent = true)
	{
		global $wpdb;
		$query = "SELECT id FROM " . $this->team_table . " WHERE kode='" . $code . "'";
		if($skipcurrent && isset($this->team))
		{
			$query .= " AND id != " . $this->team;	
		}
		$code_check = $wpdb->get_var($query);
		return ((is_null($code_check)) ? TRUE : FALSE);
	}


	/**
	 * Deal with Blurbs
	 */

	/**
	 * Saves blurb to database
	 *
	 * @param string $which Name/ID of blurb
	 * @param string $data Info for the blurb
	 */
	public function save_blurb($which, $data)
	{
		if(is_null($data))
		{
			delete_post_meta($this->team_data['post'], $which);
		}
		else
		{
			if(!add_post_meta($this->team_data['post'], $which, $data, TRUE))
			{
				update_post_meta($this->team_data['post'], $which, $data);
			}
		}
	}

	/**
	 * Gets the blurb data
	 *
	 * @param string $which Name/ID of the blurb to get
	 *
	 * @return string Value of the blurb OR false if the blurb doesn't exist
	 */
	public function get_blurb($which)
	{
		if(isset($this->team_data['post']))
		{
			return get_post_meta($this->team_data['post'], $which, TRUE);
		}
		else
		{
			return false;
		}
	}

	/**
	 * Save the image
	 *
	 * @param string $img_name Name of the image
	 */
	public function save_image($img_name)
	{
		if(!add_post_meta($this->team_data['post'], 'cmd_image', $img_name, TRUE))
		{
			update_post_meta($this->team_data['post'], 'cmd_image', $img_name);
		}
	}
	
	/**
	 * Gets the image
	 *
	 * @return string Image name for the team
	 */
	public function get_image()
	{	
		return get_post_meta($this->team_data['post'], 'cmd_image', TRUE);
	}
	
	
	/**
	 * Deal with Itineraries
	 */

	/**
	 * Updates and returns the current schedule
	 *
	 * @param integer $override The id of the team, if 0 this team
	 *
	 * @return array Schedule info
	 */
	public function get_schedule($override = 0)
	{	

		// if the override > 0 (if it is the id of the team)
		// clear the info for the current schedule, and load data
		//  for the team requested
		if($override > 0)
		{
			$temp_arrival = !empty($this->arrival) ? $this->arrival : "Unknown";
			$temp_schedule = !empty($this->schedule) ? $this->schedule : "Unknown";
			$temp_cost = $this->cost;
			$temp_departure = !empty($this->departure) ? $this->departure : "Unknown";
			
			unset($this->arrival);
			$this->schedule = array();
			$this->cost = 0;
			unset($this->departure);
			
			self::generate_schedule($override);
			
			$arrival = !empty($this->arrival) ? $this->arrival : "Unknown";
			$schedule = !empty($this->schedule) ? $this->schedule : "Unknown";
			$cost = $this->cost;
			$departure = !empty($this->departure) ? $this->departure : "Unknown";
			
			$this->arrival = $temp_arrival;
			$this->schedule = $temp_schedule;
			$this->cost = $temp_cost;
			$this->departure = $temp_departure;
			
			return $schedule;
		}
		else
		{
			if(isset($this->schedule))
			{
				return $this->schedule;	
			}
			else
			{
				self::blank_schedule();
				return $this->schedule;
			}

		}
		
	}

	/**
	 * Gets array of countries for schedule
	 *
	 * @return array List of countries
	 */
	public function schedule_countries()
	{
		$values = array();
		if(!empty($this->schedule))
		{
			foreach($this->schedule as $id=>$data)
			{
				$values[] = $data['country'];
			}
		}
		return $values;
	}

	/**
	 * Gets list of programs for schedule
	 *
	 * @return array List of programs for schedule
	 */
	public function schedule_programs()
	{
		$values = array();
		if(!empty($this->schedule))
		{
			foreach($this->schedule as $id=>$data)
			{
				$values[] = $data['program'];
			}
		}
		return $values;
	}

	/**
	 * Gets arrival date for team
	 *
	 * @return string Date of arrival
	 */
	public function arrival_date()
	{
		return isset($this->arrival) ? $this->arrival : NULL;
	}

	/**
	 * Gets departure date for team
	 *
	 * @return string Date of departure
	 */
	public function departure_date()
	{
		return isset($this->departure) ? $this->departure : NULL;
	}

	/**
	 * Calculates the trip duration
	 * 
	 * @param int Number of days for trip duration OR 0 if the arrival or departure are not set
	 */
	public function trip_duration()
	{
		if(isset($this->arrival) && isset($this->departure))
		{
			$end = mktime(0,0,0,substr($this->departure,0,2),substr($this->departure,3,2),substr($this->departure,6,4));
			$start = mktime(0,0,0,substr($this->arrival,0,2),substr($this->arrival,3,2),substr($this->arrival,6,4));
			return round(($end-$start) / 86400) + 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Gets cost of the trip
	 *
	 * @return float Cost of the trip
	 */
	public function trip_cost()
	{
		return isset($this->team_data['cost']) ? $this->team_data['cost'] : NULL;
	}

	/**
	 *  deletes the segment for the team, and updates the team schedule
	 * 
	 * @return int $segment_id - the if of the segment to be deleted
	 */
	public function delete_segment($segment_id = 0)
	{
		if($segment_id > 0)
		{
			global $wpdb;
			$itin_table = $wpdb->prefix . "command_schedule";
		
			$wpdb->delete($itin_table, array('id' => $segment_id));		
		}

		self::generate_schedule();
		$new_team_code = self::generate_team_code();
		self::update('code',$new_team_code);
	}
	
	/**
	 * Saves the segment data and generates the schedule for this team
	 *
	 * @param int $segment_id The id of the segment
	 * @param int $country_id The id of the country
	 * @param int $region_id The id of the region
	 * @param int $program_id The id of the program
	 * @param float $cost The cost of the segment of the schedule
	 * @param string $start_date The start date of the segment
	 * @param string $end_date The end date of the segment
	 */
	public function save_segment($segment_id, $country_id, $region_id, $program_id, $cost, $start_date, $end_date)
	{
		global $wpdb;
		//
		// build the data for the segment
		$sdate = substr($start_date,6,4) . "-" . substr($start_date,0,2) . "-" . substr($start_date,3,2);
		$edate = substr($end_date,6,4) . "-" . substr($end_date,0,2) . "-" . substr($end_date,3,2);
				
		$itin_table = $wpdb->prefix . "command_schedule";
		$data = array('team_id' => $this->team,
					  'country_id' => $country_id,
					  'region_id' => $region_id,
					  'program_id' => $program_id,
					  'cost' => $cost,
					  'start_date' => $sdate,
					  'end_date' => $edate,
					  );

		// update/create segment logic
		if($segment_id > 0)
		{
			$data['id'] = $segment_id;
			$data['modified'] = date("Y-m-d h:i:s");
			$data['modified_by'] = $this->user;
			$wpdb->replace($itin_table, $data);
		}
		else
		{
			$data['created'] = date("Y-m-d h:i:s");
			$data['created_by'] = $this->user;
			$wpdb->insert($itin_table, $data);
			$data['id'] = $wpdb->insert_id;
		}

		//
		// generate the new schedule
		self::generate_schedule();
		$new_team_code = self::generate_team_code();
		self::update('code',$new_team_code);

		return $data['id'];
	}

	/**
	 * Gets the ID's of the staff for a team
	 *
	 * @return array List of user Id's for the staff for the country
	 */
	public function get_staff()
	{
		$staff = get_users(
			array(
				'meta_key' => $this->poc_key,
				'fields' => array('ID')
			)
		);

		$ids = array();
		foreach($staff as $id)
		{
			$ids[] = $id->ID;
		}
		return $ids;
	}

	/**
	 * Private Methods
	 */

	/**
	 * Gets the fields for the database table
	 *
	 * @return string Fields in a string
	 */
	private function field_list()
	{
		$fields = "";
		foreach($this->translate as $common_field=>$table_field)
		{
			$fields .= $this->team_table . "." . $table_field . ',';
		}
		$fields = substr($fields,0,-1);
		return $fields;
	}

	/**
	 * Builds and gets a blank list of fields where the translate field is the key
	 *
	 * @return array List of fields
	 */
	private function blank()
	{
		$blank_array = array();
		foreach($this->translate as $common_field => $table_field)
		{
			$blank_array[$common_field] = NULL;
		}
		return $blank_array;
	}
	
	/**
	 * Sets the schedule to a blank/empty state
	 */
	public function blank_schedule($force = false)
	{
		if($force)
		{
			$this->schedule = array();
		}

		$this->schedule[0] = array('country' => NULL,
									'region' => NULL,
									'program' => NULL,
									'cost' => 0.00,
									'start_date' => NULL,
									'end_date' => NULL,
								    'duration' => '',
									);	
	}


	/**
	 * Check the validity of a Team and turns it off if it isn't functional
	 *
	 */
	 private function valid_check()
	 {
		$checks = 3;

		//
		//Make sure there is at least one Program
		foreach(self::schedule_programs() as $program_id);
		{
			if(!empty($program_id))
			{
				$programs[] = $program_id;
			}
		}
		if(!empty($programs))
		{
			$checks--;
		}

		//
		//Make sure there is a country
		foreach(self::schedule_countries() as $country_id);		
		{
			if(!empty($country_id))
			{
				$countries[] = $country_id;
			}
		}
		if(!empty($countries))
		{
			$checks--;
		}
		
		
		//
		//Check to make sure end date is past start date
		$arrive = self::arrival_date();
		$depart = self::departure_date();
		
		if(!empty($arrive) && !empty($depart))
		{
			$checks--;	
		}
		
		
		//
		//If there are unfulfilled checks, then put team into draft mode
		if($checks > 0)
		{
			self::update('status', 8);
		}
	 }


	/**
	 * Generates the schedule for the team
	 * 
	 * @param int $override The team id to generate the schedule for
	 */
	private function generate_schedule($override = 0)
	{
		global $wpdb;
		//
		// if override = 0, set the team id for this team
		if($override == 0)
		{
			unset($this->schedule);
			$which_team = $this->team;
		}

		//
		// else set the team id to load the schedule for
		else
		{
			$which_team = $override;
		}

		$query = "SELECT id, country_id, region_id, program_id, cost, 
					DATE_FORMAT(start_date,'%m/%d/%Y') sdate, 
					DATE_FORMAT(end_date,'%m/%d/%Y') edate,
					DATEDIFF(end_date,start_date)+1 duration
				  FROM " . $wpdb->prefix . "command_schedule 
				  WHERE team_id = " . $which_team . " ORDER BY start_date";
		$results = $wpdb->get_results($query,ARRAY_A);

		// 
		// iterate through schedule segments and add them to this schedule
		foreach($results as $record)
		{
			if(!isset($this->arrival))
			{
				$this->arrival = $record['sdate'];
			}
			

			$this->schedule[$record['id']] = array('country' => $record['country_id'],
													 'region' => $record['region_id'],
													 'program' => $record['program_id'],
													 'cost' => $record['cost'],
													 'start_date' => $record['sdate'],
													 'end_date' => $record['edate'],
												     'duration' => $record['duration']
													 );	
			$this->cost += $record['cost'];
		}

		if(!isset($this->departure))
		{
			if(isset($record['edate']))
			{
				$this->departure = $record['edate'];
			}
		}

		//self::blank_schedule();
	}

	/**
	 * Generates the team code for this team based on the country and program data
	 *
	 * @param string The unique code for the team
	 */
	private function generate_team_code()
	{

		//
		//[Country_Codes][Team_Start_Date][Program_Codes]-[IP/R]	
		//
		global $wpdb;
		
		$start = "000000";
		$countries = "";
		$programs = "";
		if(!empty($this->schedule))
		{
			foreach($this->schedule as $id=>$data)
			{
				if($start == "000000")
				{
					$start = substr($data['start_date'],0,2) . substr($data['start_date'],3,2). substr($data['start_date'],8,2);
				}
	
				$c_query = 'SELECT kode FROM ' . $wpdb->prefix . 'command_country WHERE id=' . $data['country'];
				$c_temp = $wpdb->get_var($c_query);
				$countries[$c_temp] = $c_temp;
	
				$p_query = 'SELECT kode FROM ' . $wpdb->prefix . 'command_program WHERE id=' . $data['program'];
				$p_temp = $wpdb->get_var($p_query);
				$programs[$p_temp] = $p_temp;
			}
	
			$t_query = 'SELECT abbv FROM ' . $wpdb->prefix . 'command_team_type WHERE id=' . $this->team_data['type'];
			$team_type = $wpdb->get_var($t_query);
	
			$code = implode('',$countries) . $start . implode('',$programs) . "-" . $team_type;
		}
		else
		{
			$code = rand(1000000000, 1000000000000)	;
		}
		
		$add = '';
		while(!self::unique_code($code . $add))
		{
			empty($add) ? $add = 0 : '';
			$add++;
		}
		
		return $code . $add;
	}
	
	/**
	 * Generate a new post with the post title and post name used
	 *
	 * @param string $post_title Title of the post
	 * @param string $post_name Name of the post
	 */
	private function generate_post($post_title = "", $post_name = "", $type = "isl_team")
	{

		//
		//Change from page to post
		return self::build_page($post_title, $post_name, $type);
		wp_die();

		/*		
		//
		//Generate a Post so we can use it to hold most of the container data
		$field_data=array(	'post_author' => $this->user,
			'post_date' => date('Y-m-d H:i:s'),
			'post_date_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
			'post_content' => 'Never to be seen',
			'post_title' => $post_title,
			'post_status' => 'draft',
			'comment_status' => 'closed',
			'ping_status' => 'closed',
			'post_name' => $post_name,
			'post_modified' => date('Y-m-d H:i:s'),
			'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
			'post_parent' => 0,
			'menu_order' => 0,
			'post_type' => 'page',
			'page_template' => 'team.php',
			'comment_count' => 0
		);
		$post_id = wp_insert_post($field_data,true);

		return $post_id;	
		*/
	}

	/**
	 * Builds the WP page for the team
	 *
	 * @param string $title The title of the page
	 * @param string $name The name of the page
	 *
	 * @return integer $post_id The id of the new post
	 */
	private function build_page($title = '', $name = '', $type = 'isl_team')
	{

		$field_data=array(	'post_author' => $this->user,
						'post_date' => date('Y-m-d H:i:s'),
						'post_date_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
						'post_content' => ' ',
						'post_title' => ((substr($title,0,1) == '#') ? substr($title,1) : $title),
						'post_status' => 'draft',
						'comment_status' => 'closed',
						'ping_status' => 'closed',
						'post_name' => $name,
						'post_modified' => date('Y-m-d H:i:s'),
						'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
						'post_parent' => 0,
						'menu_order' => 0,
						'post_type' => $type,
						'page_template' => '',
						'comment_count' => 0
					);
		$post_id = wp_insert_post($field_data, true);	
	
		return $post_id;
	}

	/**
	 * Updates the post for this team
	 */
	private function update_cp()
	{

		self::create_list(array(), 0, 100000000);
		
		$pre_info = "";
		$pre_info .= 'a:14:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";';
		$pre_info .= 'i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";';
		$pre_info .= 'a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}';

		$team_list = '';
		$cnt=0;
		foreach($this->teams as $id=>$data)
		{
			$team_list .= "i:" . $id . ";s:" . strlen($data['code']) . ":\"" . $data['code'] . "\";";		
			$cnt++;
		}
		$team_list .= '}';
		
		$write = $pre_info . 's:7:"choices";a:' . $cnt . ':{' . $team_list;
		$write .= 's:13:"default_value";a:0:{}s:10:"allow_null";i:1;s:8:"multiple";i:1;s:2:"ui";';
		$write .= 'i:1;s:4:"ajax";i:1;s:11:"placeholder";s:0:"";s:8:"disabled";i:0;s:8:"readonly";i:0;}}';
				 
		wp_update_post(array('ID' => $this->custom_post_id, 'post_content' => $write));

	}

}