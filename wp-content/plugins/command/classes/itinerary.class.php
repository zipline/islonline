<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * ININERARY class
 * handles Itinerary functionality
 */
CLASS ITINERARY
{
	/** @var integer $itinerary Itinerary id */
	private $itinerary = NULL;

	/** @var array $itineraries List of itineraries */
	private $itineraries = array();

	/** @var array $details List of details */
	private $details = array();

	/** @var boolean $production Boolean if site is in production or development/testing */
	private $production = NULL;

	/** @var integer $user User id */
	private $user = 0;

	/** @var string $itinerary_table Database table for itineraries */
	private $itinerary_table = NULL;

	/** @var string $days_table Database table for days */
	private $days_table = NULL;

	/** @var string $live_table database table for live team itineraries */
	private $live_table = NULL;

	/** @var integer $team_id Team id */
	private $team_id = NULL;

	/**
	 * Default Constructor
	 * Loads the defaults for the Itinerary object
	 */
	public function __construct()
	{
		global $wpdb;
		$this->user = get_current_user_id();
		
		$this->itinerary_table = $wpdb->prefix . "command_itinerary";
		$this->days_table = $wpdb->prefix . "command_itin_days";
		$this->live_table = $wpdb->prefix . "command_TEAM_ITINERARY";

		$this->production = false;
	}

	/**
	 * Sets the production attribute
	 *
	 * @param boolean $setting Set the itinerary object to production or not.
	 */
	public function production($setting = false)
	{
		$this->production = $setting;
	}

	/**
	 * Sets the team id
	 *
	 * @param integer $team_id ID of the team.
	 */
	public function set_team($team_id=0)
	{
		$this->team_id = $team_id;
		$this->itinerary = 0;
		self::fetch_details();
	}
	
	/**
	 * Single Itinerary check functions
	 */
	public function show_public($yesno = "false")
	{
		if(isset($this->team_id))
		{
			global $wpdb;

			$team_post = $wpdb->get_var("SELECT post_id FROM " . $wpdb->prefix . "command_TEAM WHERE id=" . $this->team_id);

			if($team_post > 0)
			{

				if(func_num_args() > 0)
				{
					update_post_meta($team_post, 'public_itinerary', $yesno);
				}
				else
				{
					$check = get_post_meta($team_post, 'public_itinerary', true);

					return (($check == "true") ? true : false);
				}
			}
		
		}
	}
	
	/**
	 * Checks if itinerary exists and sets it, and gets details for the itinerary
	 *
	 * @param integer $itinerary_id Id of the itinerary
	 * @param boolean $all If true, will load all itineraries
	 */
	public function set_itinerary($itinerary_id, $all=false)
	{
		if(self::is_itinerary($itinerary_id))
		{
			$this->itinerary = $itinerary_id;
			
			if(!isset($this->itineraries) || !isset($this->itineraries[$itinerary_id]))
			{
				self::fetch_itineraries($itinerary_id, $all);	
			}
			self::fetch_details();
		}
	}
	/**
	 * Gets the itinerary
	 *
	 * @return array Itinerary information
	 */
	public function get_itinerary()
	{

		if(isset($this->itinerary))
		{
			return $this->itinerary;
		}
	}

	/**
	 * Checks if the itinerary exists
	 *
	 * @param integer $itinerary Itinerary id
	 *
	 * @return array The current ID if it is in the current list of itineraries OR
	 * @return boolean True if itinerary exists, false if not
	 */
	public function is_itinerary($itinerary_id)
	{
		if(isset($this->itineraries) && count($this->itineraries)>0)
		{
			return($this->itineraries[$itinerary_id]);	
		}
		else
		{
			global $wpdb;
			$check = $wpdb->get_var("SELECT id FROM " . $this->itinerary_table . " WHERE id = " . $itinerary_id);
			return (($check === false) ? false : true);
		}
	}

	/**
	 * Single Itinerary reference functions
	 */

	/**
	 * Returns the name of the current itinerary
	 *
	 * @return string Title/name of current itinerary OR
	 * @return boolean false If there is no current itinerary
	 */
	public function name()
	{
		if(isset($this->itinerary))
		{
			if(!empty($this->itineraries[$this->itinerary]['title']))
			{
				return $this->itineraries[$this->itinerary]['title'];
			}
			else
			{
				return false;
			}
		}
	}

	/**
	 * Gets the programs for the current itinerary
	 *
	 * @return array List of programs for current itinerary
	 */
	public function programs()
	{
		if(isset($this->itinerary))
		{
			return $this->itineraries[$this->itinerary]['programs'];
		}
	}

	/**
	 * Checks if if program is in this itineraries program list
	 *
	 * @param integer $program_id The id of the program
	 *
	 * @return boolean True if the program is in the itinerary's programs OR false if the program is not
	 */
	public function in_programs($program_id = 0)
	{
		if(isset($this->itinerary))
		{
			if(!empty($this->itineraries[$this->itinerary]['programs']))
			{
				$programs = explode(",", $this->itineraries[$this->itinerary]['programs']);
				return in_array($program_id, $programs);
			}
			else
			{
				return false;
			}
		}
	}

	/**
	 * Return the list of countries for the current itinerary
	 *
	 * @return array List of countries for the current itinerary
	 */
	public function countries()
	{
		if(isset($this->itinerary))
		{
			return $this->itineraries[$this->itinerary]['countries'];
		}
	}

	/**
	 * Checks if requested country is in current itinerary countries list
	 * 
	 * @param integer $country_id The country id
	 *
	 * @return boolean True if the country is in the current itinerary country list OR false if not
	 */
	public function in_countries($country_id = 0)
	{
		if(isset($this->itinerary))
		{
			if(!empty($this->itineraries[$this->itinerary]['countries']))
			{
				$countries = explode(",", $this->itineraries[$this->itinerary]['countries']);
				return in_array($country_id, $countries);
			}
			else
			{
				return false;
			}
		}
	}

	/**
	 * Gets the itinerary header
	 *
	 * @return string Header attribute for the current itinerary OR '' if there the itinerary header is empty
	 */
	public function header()
	{
		if(empty($this->itineraries[$this->itinerary]['header']))
		{
			return '';
		}
		else
		{
			return $this->itineraries[$this->itinerary]['header'];
		}
	}

	/**
	 * Gets the itinerary footer
	 *
	 * @return string Footer attribute for the current itinerary
	 */
	public function footer()
	{
		if(isset($this->itinerary) && isset($this->itineraries[$this->itinerary]))
		{
			return $this->itineraries[$this->itinerary]['footer'];
		}
	}

	/**
	 * Gets the number of days for the itinerary
	 *
	 * @return integer Number of days for an itinerary
	 */
	public function days()
	{
		if(isset($this->itinerary))
		{
			return $this->itineraries[$this->itinerary]['days'];
		}
	}

	/**
	 * Gets the details for the itinerary
	 *
	 * @return array List of details for the current itinerary OR return empty array if no details
	 */
	public function details()
	{
		if(isset($this->details))
		{
			return $this->details;
		}
		else
		{
			return array();
		}
	}

	/**
	 * Resets/Clears the details for this itinerary
	 */
	public function clear_details()
	{
		$this->details = array();
	}

	/**
	 * Add detail to the details list
	 *
	 * @param string $day Date for the detail
	 * @param string $title Title of detail
	 * @param string $description Description of detail
	 */
	public function add_detail($day, $title = NULL, $description)
	{
		$this->details[$day] = array($title, $description);	
	}

	/**
	 * Builds a string of HTML options for a select box for itineraries
	 *
	 * @param integer $which The current itinerary
	 * @param boolean $addzero If true, include the blank/any item at the top of the list
	 * @param boolean $all Currently doesn't do anything
	 *
	 * @return string The string of HTML option items
	 */
	public function selector($which = 0, $addzero = false, $all = false)
	{
		$option_list = '';
		
		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . "> -- ANY --</OPTION>";
		}
		foreach(self::listing(true) as $id => $data)
		{
			$option_list .= "<OPTION VALUE=" . $id . (($which==$id) ? " SELECTED" : "") . ">" . $data['title'] . "</OPTION>";		
		}
		
		return $option_list;
			
	}
	/**
	 * Deactivates the identified itinerary
	 *
	 * @param integer @itinerary_id Id of the itinerary to be deactivated
	 */
	public function deactivate($itinerary_id)
	{
		global $wpdb;
		$wpdb->update($this->itinerary_table, array('active' => 0), array('id' => $itinerary_id));
	}

	/**
	 * Itinerary build and update functions
	 */

	/**
	 * Builds and saves new itinerary
	 *
	 * @param string $title The title of the itinerary
	 * @param string $header Header info
	 * @param string $footer Footer info
	 * @param array $countries List of countries
	 * @param array $programs Lst of programs
	 * 
	 * @return boolean True if itinerary exists and is updated OR false if the itinerary exists and isn't updated OR
	 * @return integer The new itinerary's id if the itinerary is new
	 */
	public function build($title, $header = NULL, $footer = NULL, $countries, $programs)
	{
		if(!$this->production)
		{
			global $wpdb;
			$created = date("Y-m-d h:i:s");
			
			$fields = array('title'=>$title,
							'header'=>$header,
							'footer'=>$footer,
							'countries'=>$countries,
							'programs'=>$programs);
			if(isset($this->itinerary) && self::is_itinerary($this->itinerary))
			{
				$fields['modified_by'] = $this->user;
				$fields['modified'] = $created;	
				$where = array('id' => $this->itinerary);
				$results =  $wpdb->update($this->itinerary_table, $fields, $where);
	
				return ($results === false ? false : true);
				
			}
			else
			{
				$fields['created'] = $created;
				$fields['created_by'] = $this->user;
		
				$wpdb->insert($this->itinerary_table, $fields);
				$this->itinerary = $wpdb->insert_id;
		
				return $this->itinerary;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * Builds details for the itinerary
	 */
	public function build_details()
	{
		if($this->production)
		{
			self::build_details_production();
		}
		else
		{
			self::build_details_control();
		}
	}

	/**
	 * Returns list of itineraries
	 *
	 * @param boolean $refresh If true, refresh the itineraries before getting them
	 * @param boolean $all If true get all itineraries
	 *
	 * @return array List of current itineraries, if they are set
	 */
	public function listing($refresh = false, $all = false)
	{
		if($refresh)
		{
			$production_value = $this->production;
			$this->production = false;
			
			self::fetch_itineraries(0, $all);
		
			$this->production = $production_value;
		}
		
		if(isset($this->itineraries))
		{
			return $this->itineraries;
		}
	}

	/**
	 * Removes and updates details for itinerary in database table
	 */
	public function apply_template()
	{
		//
		// if current itinerary is > 0 and team id > 0...
		if($this->itinerary > 0 && $this->team_id >0)
		{
			global $wpdb;
			//
			// delete the team id from the live table
			$wpdb->delete($this->live_table, array('team_id' => $_GET['team_id']));
			$this->production = false;

			self::fetch_itineraries();

			//
			// insert the details into the live table
			foreach($this->details() as $data)
			{
				$field_list = array('team_id' => $this->team_id,
									'day' => $data['day'],
									'title' => $data['title'],
									'description' => $data['description'],
									'created' => date("Y-m-d h:i:s"),
									'created_by' => $this->user
									);
				
				$wpdb->insert($this->live_table, $field_list);
			}
			
			//
			//Header and Footer
			$field_list = array('team_id' => $this->team_id,
								'day' => 10000,
								'title' => 'header',
								'description' => self::header(),
								'created' => date("Y-m-d h:i:s"),
								'created_by' => $this->user
								);
			
			$wpdb->insert($this->live_table, $field_list);

			$field_list = array('team_id' => $this->team_id,
								'day' => 0,
								'title' => 'footer',
								'description' => self::footer(),
								'created' => date("Y-m-d h:i:s"),
								'created_by' => $this->user
								);
			
			$wpdb->insert($this->live_table, $field_list);

		}
	}
	
	/**
	 * Private functions
	 */

	/**
	 * Builds details for production environment
	 */
	private function build_details_production()
	{
		
		if(isset($this->team_id))
		{
			if(isset($this->details) && count($this->details)>0)
			{
				global $wpdb;
				
				//
				// remove team info 
				$wpdb->delete($this->live_table, array("team_id" => $this->team_id));

				//
				// insert new team info
				foreach($this->details as $day => $data)
				{
					$things_to_insert = array('team_id' => $this->team_id,
											  'title' => $data[0],
											  'day' => $day,
											  'description' => $data[1],
											  'created' => date("Y-m-d h:i:s"),
											  'created_by' => $this->user
											 );
					$wpdb->insert($this->live_table, $things_to_insert);
				}
			}
		}
	}

	/**
	 *  Clears the itinerary info from the db, then inserts new data
	 */
	private function build_details_control()
	{

		// 
		// if current itinerary is set and current details are set and > 0
		if(isset($this->itinerary) && isset($this->details) && count($this->details)>0)
		{
			global $wpdb;
			//
			// remove itinerary info from db
			$wpdb->delete($this->days_table, array("itinerary_id" => $this->itinerary));
			
			//
			// add new details for itinerary
			foreach($this->details as $day => $data)
			{
				$things_to_insert = array('itinerary_id' => $this->itinerary,
											'title' => $data[0],
											'day' => $day,
											'description' => $data[1],
											'created' => date("Y-m-d h:i:s"),
											'created_by' => $this->user
										 );
				$wpdb->insert($this->days_table, $things_to_insert);
			}
		}
	}

	/**
	 * Fetch itineraries
	 *
	 * @param integer $id Id of itinerary
	 * @param boolean $all If true get all itineraries
	 */
	private function fetch_itineraries($id=0, $all = false)
	{
		if($this->production)
		{
			//Do nothing, there are no production itinerary pulls handled here, details only
		}
		else
		{
			self::fetch_itineraries_control($id, $all);
		}
	}

	/**
	 * Loads itinerary data from the database
	 *
	 * @param int $id Itinerary id
	 * @param  boolean $all If true load all itineraries (even inactive ones)
	 */
	private function fetch_itineraries_control($id = 0, $all = false)
	{
		global $wpdb;
		
		//
		// get itinerary data from db
		$query = "SELECT id, title, header, footer, countries, programs, active
				  FROM " . $this->itinerary_table . "
				  WHERE 1=1 " .  
				  (($id > 0) ? " AND id = " . $id : "") . 
				  ((!$all) ? " AND active = 1" : "") . 
				  " ORDER BY title";
		$results = $wpdb->get_results($query, ARRAY_A);

		//
		// load itineraries into this object
		foreach($results as $record)
		{
			$this->itineraries[$record['id']] = array('title' => $record['title'],
													  'header' => $record['header'],
													  'footer' => $record['footer'],
													  'countries' => $record['countries'],
													  'programs' => $record['programs'],
													  'active' => $record['active'],	
													  'days' => 0
													 );	
		}
		
		//
		// get length (in days) from db
		$query = "SELECT itinerary_id, count(" . $this->days_table . ".id) cnt 
				  FROM " . $this->days_table . " INNER JOIN " . $this->itinerary_table . "
				  	ON " . $this->days_table . ".itinerary_id = " . $this->itinerary_table . ".id
				  WHERE 1=1 " . 
				  (($id > 0) ? " AND itinerary_id = " . $id : "") .
				  ((!$all) ? " AND active = 1" : "") . 
				  " GROUP BY itinerary_id";
		$results = $wpdb->get_results($query, ARRAY_A);

		//
		// append duration to itinerary data
		foreach($results as $record)
		{
			$this->itineraries[$record['itinerary_id']]['days'] = $record['cnt'];
		}
	}

	/**
	 * Fetches the details for the itinerary
	 */
	private function fetch_details()
	{
		if($this->production)
		{
			self::fetch_details_production();
		}
		else
		{
			self::fetch_details_control();
		}
	}

	/**
	 * Fetches details from db for itinerary
	 */
	private function fetch_details_control()
	{
		if(isset($this->itinerary))
		{
			global $wpdb;
			unset($this->details);
	
			$query = "SELECT id, day, title, description 
					  FROM " . $this->days_table . " 
					  WHERE itinerary_id = " . $this->itinerary . " 
					  ORDER BY day";
			$results = $wpdb->get_results($query, ARRAY_A);
	
			foreach($results as $record)
			{
				$this->details[$record['id']] = array('day' => ($record['day']),
													  'title' => $record['title'],
													  'description' => $record['description']
													 );	
			}
		}
	}

	/**
	 * Fetches details for current team from db and loads them
	 */
	private function fetch_details_production()
	{
		if(isset($this->team_id))
		{
			global $wpdb;
			unset($this->details);
	
			//
			// query for team data
			$query = "SELECT day, title, description 
					  FROM " . $this->live_table . " 
					  WHERE team_id = " . $this->team_id . " 
					  ORDER BY day";
			$results = $wpdb->get_results($query, ARRAY_A);

			// 
			// add data to this object
			foreach($results as $record)
			{
//				$this->details[$record['title']] = array('day' => ($record['day']),
//														  'title' => $record['title'],
//														  'description' => $record['description']
//														 );	
				$this->details[$record['day']] = array('day' => ($record['day']),
													   'title' => $record['title'],
													   'description' => $record['description']
													  );	
			}

			//
			// move details['10000']['description'] to itinerary header
			if(isset($this->details['10000']['description']))
			{
				$this->itineraries[$this->itinerary]['header']=$this->details['10000']['description'];
				unset($this->details['10000']);
			}

			//
			// move details['0']['description'] to itinerary footer
			if(isset($this->details['0']['description']))
			{
				$this->itineraries[$this->itinerary]['footer']=$this->details['0']['description'];
				unset($this->details['0']);
			}
		}
	}
}