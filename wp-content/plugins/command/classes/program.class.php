<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');
	/**
	 * PROGRAM class
	 * manages functionality for program objects
	 */
CLASS PROGRAM
{
	/** @var integer $program ID of the program */
	private $program = NULL;

	/** @var array $programs List of programs */
	private $programs = array();

	/** @var string $program_table Database table for program data */
	private $program_table = NULL;

	/** @var array $program_types List of program types */
	private $program_types = array();

	/** @var array $ptypes List of ptypes */
	private $ptypes = array();

	/** @var integer $custom_post_id Custom Post ID for programs */
	private $custom_post_id = 4086;

	/**
	 * Default constructor
	 * Sets the program table attribute
	 */
	public function __construct()
	{
		global $wpdb;
		$this->program_table = $wpdb->prefix . "command_program";
	}
	
	/**
	 * Sets the current program
	 *
	 * @param integer $program_id ID of the program to be set
	 */
	public function set_program($program_id, $Check_all = true)
	{
		if(self::is_program($program_id, $Check_all))
		{
			$this->program = $program_id;
		}
		else
		{
			unset($this->program);
		}
	}

	/**
	 * Gets the current program
	 *
	 * @return integer Current program ID
	 */
	public function get_program()
	{
		if(isset($this->program))
		{
			return $this->program;
		}
	}

	/**
	 * Returns the name/title of the program
	 *
	 * @return string Program title
	 */
	public function get_name()
	{
		if(isset($this->program))
		{
			return $this->programs[$this->program]['title'];
		}
	}
	
	/**
	 * Returns the name/title of the program
	 *
	 * @return string Program abbreviation
	 */
	public function get_abbv()
	{
		if(isset($this->program))
		{
			return $this->programs[$this->program]['code'];
		}
	}

	/**
	 * Gets the program title
	 *
	 * @param integer $program_id ID of the program to get the title of
	 *
	 * @return string Title of the program OR "N/A" if $program_id param equals 0
	 */
	public function program($program_id = 0)
	{
		if($program_id > 0)
		{
			return $this->programs[$program_id]['title'];
		}
		else
		{
			return "N/A";
		}
	}

	/**
	 * Gets the program details
	 *
	 * @return array List of detals for this program
	 */
	public function details()
	{
		if(isset($this->programs) && self::is_program(self::get_program()))
		{
			return $this->programs[self::get_program()];
		}
	}

	/**
	 * Sets post_id for program if it passed, or gets the post_id from the db
	 * 
	 * @param int $default The post_id
	 * @param boolean $write If true, update the row instead of get it
	 * 
	 * @return int $post_id The post_id of the program
	 */
	public function post_id($default = 0, $write = false)
	{
		global $wpdb;

		//
		// update the post_id in the db
		if($write && $default > 0)
		{
			$wpdb->update($this->program_table, array("post_id" => $default), array("id" => $this->program));				
		}
		//
		// get the post_id and return it
		elseif ($default == 0)
		{
			$post_id = $wpdb->get_var("SELECT post_id from " . $this->program_table . " WHERE id=" . $this->program);
			return $post_id;
		}
	}

	/**
	 * Gets the id of the program by the post id
	 *
	 * @param integer $post_id ID of the post
	 *
	 * @return integer ID of the program OR 0 if there is no program associated with the post id
	 */
	public function by_post($post_id =0)
	{
		global $wpdb;

		if(!count($this->programs))
		{
			self::initialize_programs();
		}
		
		foreach($this->programs as $id=>$data)
		{
			if($data['post'] == $post_id)
			{
				return $id;
				wp_die();
			}
		}
		return 0;
	}
	/**
	 * Builds the program and saves it to the database
	 *
	 * @param string $title Title of the program
	 * @param integer $kode Code for the program
	 * @param integer $active Acts as boolean 1 = true
	 */
	public function build($title, $kode, $active=1)
	{
		global $wpdb;
		$wpdb->insert($wpdb->prefix . "command_program",
							array('title'=>$title,
								  'kode'=>$kode,
								  'program_type_id'=>0,
								  'active'=>$active,
								  'sequence'=>0,
								  'created'=>date('Y-m-d h:i:s'),
								  'created_by'=>get_current_user_id()
								  )
					 );
		$this->program = $wpdb->insert_id;

		self::update_cp();
	}

	/**
	 * Saves the element value pair to the database
	 *
	 * @param string $element The element name
	 * @param string $value The value of the element
	 */
	public function save_info($element, $value)
	{
		if(isset($this->program))
		{
			global $wpdb;
			$wpdb->update($wpdb->prefix . "command_program", array($element => $value), array('id' => $this->program));
		}
		
		if($element == "active")
		{
			self::update_cp();
		}
	}

	/**
	 * Gets the list of programs
	 *
	 * @param boolean $all If true, get all programs OR if false, just get the current program
	 *
	 * @return array List of programs
	 */
	public function list_programs($all = false)
	{
		self::initialize_programs($all);
		return $this->programs;
	}
	
	/**
	 * Builds a string of countries based on the $list_items input, and returns the list
	 * 
	 * @param array $list_items The array of program id's
	 * 
	 * @param string $list Comma-separated list of programs with ampersand between last two
	 */
	public function display_program_list($list_items = array())
	{
		//
		//Format countries and programs...
		$list_formatted = '';

		if(!empty($list_items) && count($list_items) > 0)
		{
			$temp_list = $list_items;
			
			foreach($list_items as $id => $name)
			{
				if(count($temp_list) > 2)
				{
					$list_formatted .= $name . ", ";		
				}
				if(count($temp_list) == 2)
				{
					if(count($list_items) == 2)
					{
						$list_formatted .= $name . " & ";		
					}
					else
					{
						$list_formatted .= $name . " & ";		
					}
				}
				if(count($temp_list) < 2)
				{
					$list_formatted .= $name;		
				}
			
				unset($temp_list[$id]);
			}
			
			return $list_formatted;

		}
	}

	/**
	 * Builds the options for a drop-down menu input
	 * 
	 * @param int $which The currently selected program id
	 * @param boolean $addzero If true, append the -- ANY -- item to the front of the options 
	 * @param boolean $all Inclued all programs
	 * @param boolean $current_only If true only include the current active programs
	 * @param int $country_id Used to check if current program is connected to the country
	 *
	 * @return string The HTML formatted list for the dropdown
	 */
	public function selector($which = 0, $addzero = false, $all = false, $current_only = false, $country_id = 0, $public = true, $pages = array('standard','minors'))
	{
		$option_list = '';
		self::initialize_programs($all);
		
		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . ">Pick a program</OPTION>";
		}

		foreach($this->programs as $prog_id => $prog_data)
		{
			if(
				(($current_only && self::is_current($prog_id, $country_id, $public, $pages)) || !$current_only || ($which==$prog_id)) && 
				 $this->programs[$prog_id]['active']
			  )
			{

				$option_list .= "<OPTION VALUE=" . $prog_id . (($which==$prog_id) ? " SELECTED" : "") . ">" . $prog_data['title'] . "</OPTION>";		
			}
		}
		
		return $option_list;
			
	}
	
	/**
	 * Checks if the program exists
	 *
	 * @param integer @program_id ID of the program to be checked
	 * @param boolean $all If true, load all programs to check
	 *
	 * @return boolean True if the program exists OR false if not
	 */
	public function is_program($program_id, $all = false)
	{
		self::initialize_programs($all);

		if(isset($this->programs[$program_id]))
		{
			return true;	
		}
		else
		{
			return false;
		}
	}

	/**
	 * Checks if the program exists and is active
	 * 
	 * @param intger $program_id The id of the program
	 * @param intger $country_id The id of the country
	 * 
	 * @return boolean True if the program exists
	 */
	private function is_current($program_id, $country_id = 0, $public = false, $pages = array('standard','minors'))
	{
		global $wpdb;
		
		$today = date("Y-m-d");
		$query = "SELECT team.id, min(sched.start_date) start
				  FROM  " . $wpdb->prefix . "command_TEAM team
				  	INNER JOIN " . $wpdb->prefix . "command_schedule sched on team.id=sched.team_id
					INNER JOIN " . $wpdb->prefix . "command_team_status cts on team.team_status_id = cts.id
					INNER JOIN " . $wpdb->prefix . "command_team_type ctt on team.team_type_id = ctt.id
				  WHERE 
				  sched.program_id = " . $program_id . " AND
				  		" . (($country_id > 0) ? "sched.country_id = " . $country_id . " AND" : '') . " 
						" . (($public == true) ? "ctt.public=1 AND" : '') . "
				  		ctt.page in ('" . implode("','", $pages) . "') AND
						cts.avail=1 AND 
						start_date >= '" . $today . "'
				  GROUP BY team.id";
		
		$id = $wpdb->get_var($query);
		
		return isset($id);
	}

	/**
	 * Initializes programs to be used
	 *
	 * @param boolean $all If true, find ALL programs
	 */
	private function initialize_programs($all = false)
	{
		if(!isset($this->programs) || !count($this->programs))
		{
			self::find_programs($all);
		}
	}
	
	/**
	 * Sets $this object's programs
	 * 
	 * @return boolean $all If true, get all of the programs, if false, just get active programs
	 */
	private function find_programs($all = false)
	{
		global $wpdb;

		self::find_program_types();
		$query = "SELECT id, post_id, title, program_type_id, kode, exp_related, images, sequence, active
					FROM " . $wpdb->prefix . "command_program" . 
					(($all) ? '' : " WHERE active=1") . 
					" ORDER BY title";
		$programs = $wpdb->get_results($query, OBJECT_K);
		$this->programs = array();
		
		foreach($programs as $program_data)
		{
			if(!empty($this->ptypes[$program_data->program_type_id]))
			{
				$type_title = $this->ptypes[$program_data->program_type_id];
			}
			else
			{
				$type_title = "<B>Unclassified</B>";
			}

			$this->programs[$program_data->id] = array('title' => $program_data->title,
														'post' => $program_data->post_id,
														'type' => $program_data->program_type_id,
														'type_title' => $type_title,
														'code' => $program_data->kode,
														'exp_related' => $program_data->exp_related,
														'images' => $program_data->images,
														'sequence' => $program_data->sequence,
														'active' => $program_data->active);
		}
	}

	/**
	 * Finds/loads the program types
	 */
	private function find_program_types()
	{
		global $wpdb;
		
		$query = "SELECT id, title
					FROM " . $wpdb->prefix . "command_program_type"; 
		$ptypes = $wpdb->get_results($query, OBJECT_K);

		foreach($ptypes as $ptype)
		{
			$this->ptypes[$ptype->id] = $ptype->title;
		}
	}
	
	/**
	 * Updates custom post data
	 */
	private function update_cp()
	{
		self::find_programs(true);
		
		$pre_info = "";
		$pre_info .= 'a:9:{s:4:"type";s:8:"checkbox";s:12:"instructions";s:0:"";s:8:"required";';
		$pre_info .= 'i:0;s:17:"conditional_logic";i:0;';
		$pre_info .= 's:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}';

		$program_list = '';
		$cnt=0;
		foreach($this->programs as $id=>$data)
		{
			if($data['active'] == 1)
			{
				$program_list .= "i:" . $id . ";s:" . strlen($data['title']) . ":\"" . $data['title'] . "\";";		
				$cnt++;
			}
		}
		$program_list .= '}';
		
		$write = $pre_info . 's:7:"choices";a:' . $cnt . ':{' . $program_list . 
				 's:13:"default_value";a:0:{}s:6:"layout";s:10:"horizontal";s:6:"toggle";i:0;}';
				 
		wp_update_post(array('ID' => $this->custom_post_id, 'post_content' => $write));
		wp_update_post(array('ID' => 20724, 'post_content' => $write));
		
	}
}