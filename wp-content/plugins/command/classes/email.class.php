<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');
require_once ( dirname(__FILE__) . '/cmonitor.class.php');

require_once(CC_CLASSES_DIR.'Twilio/autoload.php');

use Twilio\Rest\Client;

/**
 * EMAIL class
 * Manages email messages
 */
CLASS EMAIL EXTENDS CMONITOR
{
	/** @var array $from List of from addresses */
	private $from = array();

	/** @var array $reply_to List of addresses to reply to */
	private $reply_to = array();

	/** @var array $send_to List of addresses to send to */
	private $send_to = array();

	/** @var array $send_cc List of carbon-copy addresses to send to */
	private $send_cc = array();

	/** @var array $send_bcc List of blind carbon-copy addresses to send to */
	private $send_bcc = array();

	/** @var string $subject The subject of the email message */
	private $subject = NULL;

	/** @var string $message The message of the email */
	private $message = NULL;

	/** @var string $signature Signature for user for sending email */
	private $signature = NULL;

	/** @var boolean $group  */
	private $group = false;

	/** @var boolean $clean_race If true, clean the racially inappropriate words from message */
	private $clean_race = false;

	/** @var boolean $cleann_swear If true, clean swear words from the message */
	private $clean_swear = false;

	/** @var array $options List of options for the email message */
	private $options = array();

	/** @var array $errors List of errors for the email */
	private $errors = array();

	/** @var integer $error_level Error level of error */
	private $error_level = 0;

	/** @var integer $max_error Current maximum error level */
	private $max_error = 0;
	
	/** @var integer $user User ID */
	private $user = NULL;

	/**
	 * Default Constructor
	 * Resets everything and sets the current user id
	 *
	 * @param boolean $cookie 
	 */
	public function __construct($cookie = false)
	{
		self::reset_all();

		$this->user = get_current_user_id();
	}

	/**
	 * Resets
	 */

	/**
	 * Resets the addresses
	 */
	public function reset_addresses()
	{
		$this->from = array();
		$this->reply_to = array();
		$this->send_to = array();
		$this->send_cc = array();
		$this->send_bcc = array();
	}

	/**
	 * Resets the message
	 */
	public function reset_message()
	{
		$this->subject = '';
		$this->message = '';
		$this->signature = '';
	}

	/**
	 * Resets the options
	 */
	public function reset_options()
	{
		$this->group = false;
		$this->options = array();
		$this->clean_race = false;
		$this->clean_swear = false;
	}
	
	/**
	 * Calls reset_addresses, reset_message, and reset_options to reset entire message
	 */
	public function reset_all()
	{
		self::reset_addresses();
		self::reset_message();
		self::reset_options();
	}
	
	public function counts()
	{
		$ret_var['to'] = count($this->send_to);
		$ret_var['cc'] = count($this->send_cc);
		$ret_var['bcc'] = count($this->send_bcc);
		
		return $ret_var;
	}
	
	/**
	 * Name Setting Functions
	 */

	/**
	 * Adds name/address combo to the to address array
	 *
	 * @param string $name Name of the user
	 * @param string $address Email address of the user
	 *
	 * @return boolean True if address is added, false if not
	 */
	public function to($name = '', $address = '')
	{
		return self::add_address($this->send_to, $name, $address);
	}

	/**
	 * Adds name/address combo to the cc address array
	 *
	 * @param string $name Name of the user
	 * @param string $address Email address of the user
	 *
	 * @return boolean True if address is added, false if not
	 */
	public function cc($name = '', $address = '')
	{
		return self::add_address($this->send_cc, $name, $address);
	}

	/**
	 * Adds name/address combo to the bcc address array
	 *
	 * @param string $name Name of the user
	 * @param string $address Email address of the user
	 *
	 * @return boolean True if address is added, false if not
	 */
	public function bcc($name = '', $address = '')
	{
		return self::add_address($this->send_bcc, $name, $address);
	}

	/**
	 * Adds name/address combo to the from address array
	 *
	 * @param string $name Name of the user
	 * @param string $address Email address of the user
	 *
	 * @return boolean True if address is added, false if not
	 */
	public function from($name = '', $address = '')
	{
		return self::add_address($this->from, $name, $address);
	}

	/**
	 * Adds name/address combo to the reply to address array
	 *
	 * @param string $name Name of the user
	 * @param string $address Email address of the user
	 *
	 * @return boolean True if address is added, false if not
	 */
	public function reply_to($name = '', $address = '')
	{
		return self::add_address($this->reply_to, $name, $address);
	}

	/**
	 * Name getting functions
	 */

	/**
	 * Returns the send_to address list
	 *
	 * @return array List of addresses that the message is to be sent to
	 */
	public function get_to()
	{
		return $this->send_to;
	}

	/**
	 * Returns the cc address list
	 *
	 * @return array List of addresses in the cc field
	 */
	public function get_cc()
	{
		return $this->send_cc;
	}

	/**
	 * Returns the bcc address list
	 *
	 * @return array List of addresses in the bcc field
	 */
	public function get_bcc()
	{
		return $this->send_bcc;
	}

	/**
	 * Returns the from address list
	 *
	 * @return array List of addresses in the from field
	 */
	protected function get_from()
	{
		return $this->from;
	}

	/**
	; * Returns the reply to address list
	 *
	 * @return array List of addresses in the reply to field
	 */
	protected function get_reply_to()
	{
		return $this->reply_to;
	}
	
	/**
	 * Message setting functions
	 */
	
	/**
	 * Validates and sets the subject of the email
	 *
	 * @param string $subject The text to be the subject of the email
	 *
	 * @return boolean True if subject is set, false if it isn't
	 */
	public function subject($subject = '')
	{
		if(!empty($subject))
		{
			self::clean_text($subject);
			$this->subject = $subject;
			return true;
		}
		
		self::add_error(__FUNCTION__, 'Subject is empty.', 1);
		return false;
	}

	/**
	 * Validates and sets the message for the email
	 *
	 * @param string $message The message text to be set
	 *
	 * @return boolean True if message text is valid and set, false if not
	 */
	public function message($message = '')
	{
		if(!empty($message))
		{
			self::clean_text($message);
			$this->message = $message;
			return true;
		}
		
		self::add_error(__FUNCTION__, 'Message Body is empty.', 1);
		return false;
	}

	/**
	 * Validates and sets the signature for the email message
	 *
	 * @param string $signature The signature for the email message
	 *
	 * @return boolean True if the signature is set, false if not
	 *
	 */
	public function signature($signature = '')
	{
		if(!empty($signature))
		{
			self::clean_text($signature);
			$this->signature = $signature;
			return true;
		}
		
		self::add_error(__FUNCTION__, 'Signature is empty.', 0);
		return false;
	}

	/**
	 * Message getting functions
	 */

	/**
	 * Returns the subject of the email message
	 *
	 * @return string The subject of the message
	 */
	protected function get_subject()
	{
		return $this->subject;
	}

	/**
	 * Returns the message of the email message
	 *
	 * @return string The message of the message
	 */
	protected function get_message()
	{
		return $this->message;
	}

	/**
	 * Returns the signature of the email message
	 *
	 * @return string The signature of the message
	 */
	protected function get_signature()
	{
		return $this->signature;
	}

	/**
	 * Sending Options
	 */
	
	/**
	 * Sets the email group and validates cc and bcc email accordingly
	 *
	 * @param boolean $group If false clear send_cc and send_bcc arrays
	 */
	public function group($group = false)
	{
		$this->group = $group;

		/** If not a group email (then single emails), then we can kill the cc and bcc */
		if(!$group)
		{
			$this->send_cc = array();
			$this->send_bcc = array();
		}
	}
	
	/**
	 * Returns the group
	 *
	 * @return integer The ID of the group
	 */
	protected function is_group()
	{
		return $this->group;
	}
	
	/**
	 * Sets the options for the email message
	 *
	 * @param array $options The array of options to be set
	 */
	public function options($options = array())
	{
		$this->options = $options;
	}

	/** 
	 * Returns the options for the email message
	 *
	 * @return array List of email options
	 */
	protected function get_options()
	{
		return $this->options;
	}

	/**
	 * Sets the clean_race variable
	 *
	 * @param boolean $check Whether the message has been cleaned for racialy inappropirate words or not
	 */
	public function clean_race($check = false)
	{
		$this->clean_race = $check;
	}

	/**
	 * Sets the clean_swearing variable
	 *
	 * @param boolean $check Whether the message has been cleaned for swear words or not
	 */
	public function clean_swearing($check = false)
	{
		$this->clean_swear = $check;
	}

	/**
	 * Calls clean_race and clean_swear with a boolean of the language being cleaned or not
	 *
	 * @param boolean $check Whether the message has been cleaned for swear/racial words or not
	 */
	public function clean_language($check = true)
	{
		$this->clean_race = $check;
		$this->clean_swear = $check;
	}
	

	/**
	 * Send the message
	 * 
	 * @return result of parent::send_message or false if fails
	 */
	public function send()
	{
		if(!self::error_level())
		{
			parent::__construct();
			return parent::send_message();
		}
		else
		{
			self::add_error(__FUNCTION__, 'Cannot send due to errors.', 1);
		}
			
		return false;
	}


	/**
	 * Private class functions
	 */

	/**
	 * Validates and adds email address if it isn't already added to the group
	 *
	 * @param reference $group The group object/array to add the address to
	 * @param string $name The name of the address
	 * @param string $address The address used
	 *
	 * @return boolean True if success, false if failure
	 */
	private function add_address(& $group, $name = '', $address)
	{
		if(self::validate($address))
		{
			if(!in_array($address, array_keys($group)))
			{
				$group[trim($address)] = trim($name);
				return true;
				wp_die();
			}
			else
			{
				self::add_error(__FUNCTION__, 'Address already exists.', 0);
			}
		}
		else
		{
			self::add_error(__FUNCTION__, 'Malformed Email Address', 1);
		}

		return false;
	}

	/**
	 * Validates the email address
	 *
	 * @param string $address The email address to be validated
	 *
	 * @return boolean True if address is valid, false if invalid
	 */
	private function validate($address = '')
	{
		if(!empty($address))
		{
			//
			//Valid email regular expression
//			$regex = '/^[_a-z0-9\+-]+(\.[_a-z0-9\+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,6})$/';
//
//			if (preg_match($regex, trim($address)))
//			{
//				return true;
//			}

			if ( is_email( $address ) ) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Removes obscene language from text
	 * 
	 * @param string reference The text to be cleaned
	 */
	private function clean_text(&$text = '')
	{
		$bad_words = array('asshole', 'asswipe', 
						   'blowjob', 'blow job', 'bullshit', 'buttfucker', 'butt fucker', 
						   'cum', 'cumslut', 'cum slut', 'cunt', 
						   'dickhead', 'dickface', 'dickwad', 'dumbass', 'dumb ass', 'dumbfuck',
						   'feltch', 'fuck', 'fucked', 'fucker', 'fuckin', 'fucking', 'fucks', 'fucktard', 
						   'jizz', 'jizzed', 'jizzing', 
						   'motherfucker', 'motherfucking', 
						   'poonani', 'poonany', 'poontang', 
						   'queef', 
						   'schlong', 'shit', 
						   'titfuck', 'tittyfuck', 'titty fuck', 'twat', 'twats', 'twatwaffle',
						   'vajayjay'
						   );
						   
		$racism = array('coon',
						'dago', 'deggo',
						'heeb',
						'kike', 'kyke',  
						'nigaboo', 'nigga', 'nigger', 'niggers', 'niglet', 
						'paki', 
						'sandnigger', 'sand nigger', 
						'wetback', 'wop'
						) ; 

		if($this->clean_race)
		{
			$text = str_ireplace(self::pad_words($racism), '*****', $text, $race_count);
			
			if($race_count)
			{
				self::add_error(__FUNCTION__, 'Racial replacements performed: ' . $race_count, 0);
			}
		}

		if($this->clean_swear)
		{
			$text = str_ireplace(self::pad_words($bad_words), '*****', $text, $swear_count);

			if($swear_count)
			{
				self::add_error(__FUNCTION__, 'Swearing replacements performed: ' . $swear_count, 0);
			}
		}

	}

	/**
	 * Adds padding to the left and right of the words
	 *
	 * @param pointer &$word_array Pointer to the word array to be edited
	 */
	private function pad_words(&$word_array)
	{
		foreach($word_array as $index=>$word)
		{
			$word_array[$index] = ' ' . $word . ' ';	
		}
	}


	/**
	 * Error handling functions
	 */

	/**
	 * Returns true if an error is present, or false if there are no errors
	 *
	 * @return boolean True if errors, false if no errors
	 */
	public function error()
	{
		return (!empty($this->errors) ? true : false);
	}

	/**
	 * Returns the max error level
	 *
	 * @return integer The maximum error level of the errors
	 */
	public function error_level()
	{
		return $this->max_error;
	}

	/**
	 * Returns the list of errors
	 */
	public function error_list()
	{
		return $this->errors;
	}
	
	/**
	 * Builds an error message out of the errors that have occurred
	 * 
	 * @return string Error message string
	 */
	public function error_list_text()
	{
		$err_message = '';
		
		foreach($this->errors as $err_data)
		{
			$err_message .= $err_data[0] . PHP_EOL;
			$err_message .= $err_data[1] . PHP_EOL;
			$err_message .= '' . PHP_EOL;
		}
		
		return $err_message;
	}

	/**
	 * Adds an error to the error list
	 *
	 * @param string $function The string name of the function that caused the error
	 * @param string $message The error message to be shown
	 * @param integer $error_level The lever of the error
	 */
	private function add_error($function, $message, $error_level)
	{
		$this->errors[] = array(__CLASS__ . ': ' . $function, $message, $error_level);
		$this->max_error = max($this->max_error, $error_level);		
	}

	/**
	 * Get "Canned" email message to send based on trigger info and sets it to the body of the email message
	 *
	 * @param string $trigger_type: the type of trigge
	 *
	 */
	public function set_canned_message($trigger_type,$subject)
	{
		global $wpdb;

		/*
		$message = $wpdb->get_row(
			$wpdb->prepare(
			"SELECT *
			FROM wp_command_admin_message
			WHERE event = %s",
			 $trigger_type));

		if(!empty($message))
		{
			$this->from("notice@islonline.org","notice@islonline.org");
			$this->message($message->message);
			$this->subject($message->subject);
		}
		*/

		$this->from("notice@islonline.org","notice@islonline.org");
		$this->message(email_new_myisl($trigger_type));
		$this->subject($subject);
		
		
	}

	/**
	 * Builds canned email message and sends it out
	 *
	 * @param string $trigger_type: the type of trigger
	 * @param object $recipient: recipient data to be used to build recipient for the email message
	 *
	 */
	public function email_notification($trigger_type,$recipient,$subject = NULL)
	{
		$contact_type=get_user_meta($recipient->ID,"contact_preference",true);
		$this->reset_all();

		if(empty($contact_type) OR in_array("Email",$contact_type))
		{
			$this->to($recipient->display_name,$recipient->user_email);
			$this->set_canned_message($recipient->user_firstname,empty($subject) ? "New ISL Message" : $subject);
			$this->send();
		}

		if(!empty($contact_type) && in_array("SMS",$contact_type))
		{
			$sms = get_user_meta($recipient->ID,"primary_phone",true);
			$sms = empty($sms)? get_user_meta($recipient->ID,"secondary_phone",true) : $sms;
			$options = get_option('cmd_center',array());
			$sid = !empty($options['twilio_sid_text'])? $options['twilio_sid_text'] : '';
			$token = !empty($options['twilio_token_text'])? $options['twilio_token_text'] : '';
			$send_from = !empty($options['twilio_phone'])? $options['twilio_phone'] : '';

			if(!empty($sid) && !empty($token) && !empty($send_from) && !empty($sms))
			{

					$client = new Client($sid,$token);
					$client->messages->create(
						$sms,
						//
						//2018-04-04 DJT Changed message
						//array(
						//	'from' => $send_from,
						//	'body' => 'You have a new message in your MyISL account: ' . site_url('/my-isl')
						//)
						array(
							'from' => $send_from,
							'body' => 'You have an important message from International Service Learning, please check your myISL portal here: ' . site_url('/my-isl')
						)
					);
				}
		}
	}
}