<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//2018-03-24 DJT Updated team_type table as follows
//ALTER TABLE  `wp_command_team_type` ADD  `rsvp` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `public` ;
//Update wp_command_team_type SET rsvp=1 WHERE id = 4;

/**
 * TEAM_SUPPORT class
 * Functionality for team support
 */
CLASS TEAM_SUPPORT
{

	/** @var string $team_status_table The name of the db table for team status data */
	private $team_status_table = NULL;

	/** @var string $team_type_table The name of the db table for team type */
	private $team_type_table = NULL;


	/** @var array $statuses List of different statuses */
	private $statuses = array();

	/** @var array $types List of different types */
	private $types = array();

	/**
	 * Default Constructor 
	 * Sets the DB tables and initializes statuses and types
	 */
	public function __construct()
	{
		global $wpdb;

		$this->team_status_table = $wpdb->prefix . 'command_team_status';
		$this->team_type_table = $wpdb->prefix . 'command_team_type';
		
		self::initialize_statuses();
		self::initialize_types();
	}
	
	/**
	 *Methods to deal with a single status
	 */

	/**
	 * Checks if status exists, and returns it or null
	 *
	 * @param integer $status_id ID of the status
	 * @param string $field Field variable for the status
	 *
	 * @return string Value of the field variable for the status
	 */
	public function status_value($status_id, $field = "title")
	{
		return isset($this->statuses[$status_id][$field]) ? $this->statuses[$status_id][$field] : NULL;
	}
	

	/**
	 * Methods to deal with a single type
	 *
	 * @param string $type Which team type by name
	 *
	 * @return int value of the type
	 */
	public function type_by_fld($type, $fld = 'title')
	{
		foreach($this->types as $id=>$data)
		{
			if(strtolower($data[$fld]) == strtolower($type))
			{
				return $id;
				wp_die();
			}
		}
		return false;
	}

	/**
	 * Methods to deal with a single type
	 *
	 * @param integer $type_id ID of the type of support
	 * @param string $field Name of the field. Default value: "title"
	 *
	 * @return string Returns type based on type_id and field
	 */
	public function type_value($type_id, $field = "title")
	{
		return isset($this->types[$type_id][$field]) ? $this->types[$type_id][$field] : NULL;
	}

	/**
	 * Methods to deal with a single type
	 *
	 * @param string $type Which team type by name
	 *
	 * @return int value of the type
	 */
	public function status_by_name($type)
	{
		foreach($this->statuses as $id=>$data)
		{
			if(strtolower($data['title']) == strtolower($type))
			{
				return $id;
				wp_die();
			}
		}
		return false;
	}
	
	/**
	 * Methods to deal with a single type
	 *
	 * @param int $type_id Which team type by id
	 *
	 * @return str the tooltip associated with the type
	 */
	public function status_tip($type_id)
	{
		return self::status_value($type_id, 'tip');
	}

	//
	//Prebuilt Selectors
	//	

	/**
	 * Builds the HTML option list for statuses
	 *
	 * @param int $which The id of the current status
	 * @param boolean $addzero If true, add the top empty choice
	 *
	 * @return string HTML formatted options
	 */
	public function status_selector($which = 0, $addzero = false)
	{
		$option_list = '';
		
		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . ">Status (ANY)</OPTION>";
		}
		foreach($this->statuses as $id => $data)
		{
			$option_list .= "<OPTION VALUE=" . $id . (($which==$id) ? " SELECTED" : "") . ">" . $data['title'] . "</OPTION>";		
		}
		
		return $option_list;
	}

	/**
	 * Builds the HTML option list for types
	 *
	 * @param int $which The id of the current type
	 * @param boolean $addzero If true, add the top empty choice
	 *
	 * @return string HTML formatted options
	 */
	public function type_selector($which = 0, $addzero = false)
	{
		$option_list = '';
		
		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . ">Programs Type (ANY)</OPTION>";
		}
		foreach($this->types as $id => $data)
		{
			if($data['active'] == 1 || $id==$which)
			{
				$option_list .= "<OPTION VALUE=" . $id . (($which==$id) ? " SELECTED" : "") . ">" . $data['title'] . "</OPTION>";
			}
		}
		
		return $option_list;
	}
	

	/**
	 *Private Methods
	 */

	/**
	 * Gets the statuses from the db and adds to this->statuses
	 */
	private function initialize_statuses()
	{
		global $wpdb;

		$query = "SELECT id, title, kode, tip, prep, pre, avail, trav, cancel
						FROM " . $this->team_status_table . " 
						ORDER BY sequence asc";
		$result = $wpdb->get_results($query, OBJECT_K);

		foreach($result as $data)
		{
			$this->statuses[$data->id] = array('title' => $data->title,
											   'code' => $data->kode,
											   'tip' => $data->tip,
											   'prep' => $data->prep,
											   'pre' => $data->pre,
											   'avail' => $data->avail,
											   'trav' => $data->trav,
											   'cancel' => $data->cancel
											  );
		}
	}

	/**
	 * Gets the types from the db and adds to this->types
	 */
	private function initialize_types()
	{
		global $wpdb;

		//
		//2018-03-23 DJT Updated field data to include the rsvp field
		$query = "SELECT id, title, abbv, public, page, active, rsvp
					FROM " . $this->team_type_table . " 
					ORDER BY id asc";
		$result = $wpdb->get_results($query, OBJECT_K);

		foreach($result as $data)
		{
			//
			//2018-13-23 DJT Updated field data to include the rsvp attribute
			$this->types[$data->id] = array('title' => $data->title,
											'abbv' => $data->abbv,
											'public' => $data->public,
											'rsvp' => $data->rsvp,
											'page' => $data->page,
											'active' => $data->active);
		}
	}
}