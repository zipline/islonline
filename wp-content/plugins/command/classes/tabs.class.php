<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * TAB_MANAGE class
 * functionality for managing tabs
 */
Class TAB_MANAGE
{
	/** @var array $tab_list List of tabs*/
	private $tab_list = array();

	/** @var string $tab_current Current tab identifier */
	private $tab_current = NULL;

	/** @var string $page_div Page div identifier */
	private $page_div = NULL;

	/** @var string $tab_name Name of tab */
	private $tab_name = NULL;

	/**
	 * Default Constructor
	 * Sets the tabs, and sets the current tab
	 */
	public function __construct()
	{
		self::SetTabs();
		self::SetCurrent();
		$this->tab_name = 'cur_tab';
	}

	/**
	 * Sets the tab list
	 *
	 * @param array $tab_list Associative array of tabs ("id" => "tab name")
	 */
	public function SetTabs ($tab_list = array('tab1' => 'First Tab'))
	{
		if(is_array($tab_list))
		{
			$this->tab_list = $tab_list;
		}
	}
	
	/**
	 * Returns the list of tabs
	 *
	 * @return array List of tabs.
	 */
	public function GetTabs()
	{
		reset($this->tab_list);
		return $this->tab_list;
	}

	/**
	 * Returns is the named tab exists
	 *
	 * @string $tab the name of the tab to check
	 *
	 * @return boolean if the tab exists
	 */
	public function IsTab($tab = '')
	{
		if(!empty($this->tab_list))
		{
			return !empty($this->tab_list[$tab]);
		}
		return false;
	}

	/**
	 * Resets the tab list to an empty array
	 */
	public function ResetTabs ()
	{
		$this->tab_list = array();
	}
	
	/**
	 * Sets the current tab name
	 *
	 * @param string $tab Name of the current tab
	 */
	public function SetTabName($tab)
	{
		$this->tab_name = $tab;	
	}

	/**
	 * Returns the current tab name
	 *
	 * @return string The current tab's name
	 */
	public function GetTabName()
	{
		return $this->tab_name;
	}

	/**
	 * Sets the current tab
	 * 
	 * @param string $current The current tab to be set
	 */
	public function SetCurrent ($current = 'tab1')
	{
		if(isset($current) && isset($this->tab_list[$current]))
		{
			$this->tab_current = $current;	
		} 
		elseif (isset($this->tab_list)) 
		{
			reset($this->tab_list);
			$this->tab_current = current($this->tab_list);	
		}
	}
	
	/**
	 * Gets the current tab
	 *
	 * @return string The current tab identifier OR return 0 if there is no current tab set
	 */
	public function GetCurrent ()
	{
		if(isset($this->tab_current) && isset($this->tab_list[$this->tab_current]))
		{
			return $this->tab_current;
			exit;
		} 
		elseif (isset($this->tab_list)) 
		{
			reset($this->tab_list);
			self::SetCurrent(current($this->tab_list));	
			return $this->tab_current;
			exit;
		}
		
		return 0;
	}

	/**
	 * Builds and outputs the HTML for the tabs
	 *
	 * @param array $removes Array of get variables to be ignored
	 *
	 * @return boolean True if the tablist is set and the number of items in tab list > 1 OR false if the tablist is not set or count is < 1
	 */
	public function ShowTabs ($removes = array(), $adds = array())
	{
		if(isset($this->tab_list) && count($this->tab_list)>=1)
		{
			if(!isset($this->tab_current))
			{
				self::SetCurrent();
			}
			
			echo '<div id="icon-themes" class="icon32"><br></div>';
   			echo '<div class="nav-tab-wrapper">';
    			
			foreach( $this->tab_list as $tab => $name ){
       			$class = ( $tab == $this->tab_current ) ? ' nav-tab-active' : '';
				$removes[] = $this->tab_name;
       			echo "<a class='nav-tab$class' href='" . self::strip_url_vars($removes, $adds) . "&" . $this->tab_name . "=" . $tab . "'>$name</a>";
			}
		    echo '</div>';

			return true;
	  }
		else
		{
			return false;
		}
	}

	/**
	 * Gets the GET part of the url
	 *
	 * @return string GEt part of the url
	 */
	public function GET_path($removes=array(),$adds=array())
	{
		return self::strip_url_vars($removes,$adds);
	}
	
	/**
	 * Compiles and strips url variables to be output as a new url
	 * 
	 * @param array $which The url variables to be ignored
	 * @param array $adds Other url variables to be added
	 * 
	 * @return string The new url with vars added
	 */
	private function strip_url_vars($which = array(), $adds = array())
	{
		//
		//Find our base path, likely including current tab
		$url = 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ? 's' : '') . "://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		$scheme = parse_url($url,PHP_URL_SCHEME);
		$host = parse_url($url,PHP_URL_HOST);
		$path = parse_url($url,PHP_URL_PATH);
		$query = parse_url($url,PHP_URL_QUERY);

		$gets = explode('&',$query);

		// 
		// iterates through url query variables, and adds them to params, except
		// for variables in which array
		foreach($gets as $parameter)
		{

			$temp = explode('=',$parameter);
			if(!in_array($temp[0],$which))
			{
				$params[$temp[0]] = $temp[1];
			}
		}
		//
		// adds elements to the params array
		if(isset($adds) && count($adds)>0)
		{
			foreach($adds as $item=>$value)
			{
				$params[$item]=$value;
			}
		}

		return $scheme . '://' . $host . $path . (count($params) ? '?' : '') . http_build_query($params);
	}
}