<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * COUNTRY class
 * contains the Functionality for Country interactions
 */
CLASS COUNTRY
{
	/** @var integer $country The id of the country */
	private $country = NULL;

	/** @var array $countries List of country ids */
	private $countries = array();

	/** @var string $country_table The string for the table in the db for countries */
	private $country_table = NULL;

	/** @var integer $custom_post_id The id of the custom post for countries */
	private $custom_post_id = 4085;

	/** @var integer $region The id of the region in which this country resedise */
	private $region = NULL;

	/** @var array $regions List of region ids */
	private $regions = array();

	/** @var string $region_table The db table where regions reside */
	private $region_table = NULL;
	
	/** @var integer $user The id of the current user */
	private $user = NULL;

	/**
	 * Constructor instantiates a country object
	 */
	public function __construct()
	{
		/** Load the WP database object */
		global $wpdb;
		
		/** Set the current user */
		$this->user = get_current_user_id();

		/** Set the country table and region table */
		$this->country_table = $wpdb->prefix . "command_country";
		$this->region_table = $wpdb->prefix . "command_regions";

		/** Build the countries and regions arrays */
		self::build_countries(true);
		self::build_regions();
	}
	
	/**
	 * Country Methods
	 */

	/**
	 * Sets the country id
	 *
	 * @param integer $country_id The id of the country
	 */
	public function set_country($country_id)
	{
		if(self::is_country($country_id))
		{
			$this->country = $country_id;
		}
		else
		{
			unset($this->country);
		}
	}

	/**
	 * Gets the country id
	 *
	 * @return integer The id of the country
	 */
	public function get_country()
	{
		if(isset($this->country))
		{
			return $this->country;
		}
	}

	/**
	 * Gets the name of the country
	 *
	 * @return string The name of the country
	 */
	public function get_name()
	{
		if(isset($this->country))
		{
			return $this->countries[$this->country]['title'];
		}
	}

	/**
	 * Gets the name of the country
	 *
	 * @return string The name of the country
	 */
	public function get_abbv()
	{
		if(isset($this->country))
		{
			return $this->countries[$this->country]['code'];
		}
	}

	/**
	 * Gets the details for the current country
	 *
	 * @return array The details for the current country
	 */
	public function details()
	{
		if(isset($this->countries) && isset($this->country) && self::is_country($this->country))
		{
			return $this->countries[$this->country];
		}
	}

	/**
	 * Check if the current country exists
	 *
	 * @param integer $country_id The id of the country to be checked
	 *
	 * @return boolean true if the country id is in the list of countries OR false if the country is not in the list of countries
	 */
	public function is_country($country_id)
	{
		return boolval(isset($this->countries[$country_id]));
	}

	/**
	 *  Adds the country to the country table
	 *
	 *  @param string $title The name of the country
	 *  @param string $kode The country code(?)
	 *  @param integer $active The boolean marking if the country is active or not
	 */
	public function build($title, $kode, $passport=0, $active=1)
	{
		global $wpdb;
		$wpdb->insert($this->country_table,array('title'=>$title,
												 'kode'=>$kode,
												 'passport'=>$passport,
												 'created'=>date('Y-m-d h:i:s'),
												 'created_by' => get_current_user_id(),
												 'active'=>$active));
		$this->country = $wpdb->insert_id;
		
		self::update_cp();
	}

	/**
	 *  Updates the id, or returns it depending on $default being either 0 or not 0
	 *
	 *  @param integer $default The post_id of the country, 0 to return the post id instead of get it
	 *  @param boolean $write If write is true, update the post_id
	 *
	 *  @return integer The Post id if the $default != 0
	 */
	public function post_id($default = 0, $write = false)
	{
		global $wpdb;

		if($write && $default > 0)
		{
			$wpdb->update($this->country_table, array("post_id" => $default), array("id" => $this->country));				
		}
		elseif ($default == 0)
		{
			$post_id = $wpdb->get_var("SELECT post_id from " . $this->country_table . " WHERE id=" . $this->country);
			return $post_id;
		}
	}

	/**
	 * Return the id of the country based on post_id
	 *
	 * @param integer $post_id The post id of the country
	 *
	 * @return integer The id of the country OR 0 if there is no country with that post_id OR return 0 if no matching country is found
	 */
	public function by_post($post_id =0)
	{
		global $wpdb;
		
		foreach($this->countries as $id=>$data)
		{
			if($data['post_id'] == $post_id)
			{
				return $id;
				wp_die();
			}
		}
		return 0;
	}

	/**
	 *  Updates this Country's $element attribute
	 *
	 *  @param string $element The attribute to be updated
	 *  @param string $value The value to set the attribute to
	 */
	public function save_info($element, $value)
	{
		if(isset($this->country))
		{
			global $wpdb;
			$wpdb->update($this->country_table, array($element => $value), array('id' => $this->country));
		}
		
		if($element == 'title')
		{
			$post_id = $this->countries[$this->country]['post_id'];
			$post_title = $value;
			$post_name = strtolower(str_replace(' ', '_', $post_title));
			wp_update_post(array('ID' => $post_id, 'post_title' => $post_title, 'post_name' => $post_name));
		}
		
		if($element == 'active')
		{
			self::update_cp();
		}
	}

	/**
	 * Region Methods
	 */

	/**
	 * Sets the region
	 *
	 * @param integer @region_id The id of the region to be set
	 */
	public function set_region($region_id)
	{
		if(self::is_region($region_id))
		{
			$this->region = $region_id;	
		}
	}

	/**
	 * Gets the current region
	 *
	 * @return integer The id of the current region
	 */
	public function get_region()
	{
		if(isset($this->region))
		{
			return $this->region;
		}
	}	

	public function build_region($country_id, $title, $kode, $description, $active)
	{
		global $wpdb;
		$wpdb->insert($this->region_table,array('country_id' => $country_id,
											    'title'=>$title,
												'kode'=>$kode,
												'description' => urlencode($description),
												'created'=>date('Y-m-d h:i:s'),
												'created_by' => get_current_user_id(),
												'active'=>$active));
		$this->region = $wpdb->insert_id;
	}

	/**
	 * Get the region details for the current region
	 *
	 * @return array The array of details for the current region
	 */
	public function region_details()
	{
		if(isset($this->regions) && self::is_region($this->region))
		{
			return $this->regions[$this->region];
		}
	}

	/**
	 * Checks if the region is in the regions list
	 *
	 * @param integer $region_id The id of the region to be checked
	 *
	 * @param boolean $all Doesn't currently do anything
	 */
	public function is_region($region_id, $all = false)
	{
		return isset($this->regions[$region_id]);
	}

	public function save_region_info($element, $value)
	{
		if(isset($this->region))
		{
			global $wpdb;
			$wpdb->update($this->region_table, array($element => $value), array('id' => $this->region));
		}
	}


	/**
	 * Methods to deal with multiples
	 */

	/**
	 * Builds a string of countries based on the $list_items input, and returns the list
	 * 
	 * @param array $list_items The array of country id's
	 * 
	 * @param string $list Comma-separated list of countries
	 */
	public function country_list($list_items = array())
	{
		$list = '';
		foreach($list_items as $country_id)
		{
			if(isset($this->countries[$country_id]))
			{
				$list .= $this->countries[$country_id]['title'] . ", ";
			}
		}
		if(strlen($list) > 2)
		{
			$list = substr($list,0,-2);
		}
		
		return $list;
	}

	/**
	 * Builds an array of countries based on the $list_items input, and returns the array
	 * 
	 * @param array $list_items The array of country id's
	 * 
	 * @param string $list array in form of id->name
	 */
	public function country_array($list_items = array())
	{
		$list = array();
		foreach($list_items as $country_id)
		{
			if(isset($this->countries[$country_id]))
			{
				$list[$country_id] = $this->countries[$country_id]['title'];
			}
		}

		return $list;
	}

	/**
	 * Builds a string of countries based on the $list_items input, and returns the list
	 * 
	 * @param array $list_items The array of country id's
	 * 
	 * @param string $list Comma-separated list of countries with ampersand between last two
	 */
	public function display_country_list($list_items = array())
	{
		//
		//Format countries and programs...
		$list_formatted = '';

		if(!empty($list_items) && count($list_items) > 0)
		{
			$temp_list = $list_items;
			
			foreach($list_items as $id => $name)
			{
				if(count($temp_list) > 2)
				{
					$list_formatted .= $name . ", ";		
				}
				if(count($temp_list) == 2)
				{
					if(count($list_items) == 2)
					{
						$list_formatted .= $name . " & ";		
					}
					else
					{
						$list_formatted .= $name . " & ";		
					}
				}
				if(count($temp_list) < 2)
				{
					$list_formatted .= $name;		
				}
			
				unset($temp_list[$id]);
			}
			
			return $list_formatted;

		}
	}

	public function list_countries($all = false)
	{
		self::build_countries($all);
		return $this->countries;
	}

	public function list_regions()
	{
		return $this->regions;			
	}
	
	/**
	 * Builds HTML select list with current item selected
	 *
	 * @param int $which The selected country id
	 * @param boolean $addzero If set appends an -- ANY -- selection to the top of the options
	 * @param boolean $current_only If set append the selected item to the options
	 *
	 * @param string $option_list The HTML output of options
	 */
	public function selector($which = 0, $addzero = false, $current_only = false, $program = 0, $public = true, $pages = array('standard','minors') )
	{
		$option_list = '';

		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . ">Pick a country</OPTION>";
		}
		foreach($this->countries as $country_id => $country_data)
		{
			if(
				(($current_only && self::is_current($country_id, $program, $public, $pages)) || !$current_only) && 
				 $this->countries[$country_id]['active']
			  )
			{
				$option_list .= "<OPTION VALUE=" . $country_id . (($which==$country_id) ? " SELECTED" : "") . ">" . $country_data['title'] . "</OPTION>";		

			}
		}
		
		return $option_list;
			
	}
	
	/**
	 * Builds HTML code for options for drop down of regions in a country
	 * 
	 * @param int $which Determines which item is selected
	 * @param boolean $addzero If true, appends empty (any) option to top of options list
	 *
	 * @return string $option_list The HTML string of options for the drop-down
	 */
	public function region_selector($which = 0, $addzero = false, $add_text = " -- ANY --")
	{
		$option_list = '';
		$regions_in_c = self::regions_in_country();
		
		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . "> " . $add_text . " </OPTION>";
		}
		foreach($regions_in_c as $id => $data)
		{
			$option_list .= "<OPTION VALUE=" . $id . (($which==$id) ? " SELECTED" : "") . ">" . $data['title'] . "</OPTION>";		
		}
		
		return $option_list;
			
	}


	//
	//Private Methods
	//

	//
	// is_current
	// Checks if a program exists in a country
	// Parameters:
	// 	int $country_id - the id of the country
	// 	int $program_id - the id of the program
	// Returns:
	// 	boolean - if the id is set, if there is a team in a country, true, else return false
	private function is_current($country_id, $program_id, $public = false, $pages = array('standard','minors'))
	{
		global $wpdb;
		
		$today = date("Y-m-d");
		$query = "SELECT team.id, min(sched.start_date) start
				  FROM  " . $wpdb->prefix . "command_TEAM team
				  	INNER JOIN " . $wpdb->prefix . "command_schedule sched on team.id=sched.team_id
					INNER JOIN " . $wpdb->prefix . "command_team_status cts on team.team_status_id = cts.id
					INNER JOIN " . $wpdb->prefix . "command_team_type ctt on team.team_type_id = ctt.id
				  WHERE sched.country_id = " . $country_id . " AND
				  		" . (($program_id > 0) ? "sched.program_id = " . $program_id . " AND" : '') . " 
						" . (($public == true) ? "ctt.public=1 AND" : '') . "
				  		ctt.page in ('" . implode("','", $pages) . "') AND
						cts.avail=1 AND 
						start_date >= '" . $today . "'
				  GROUP BY team.id";
		//echo $query;
		$id = $wpdb->get_var($query);
		//echo var_dump($id);
		return isset($id);
	}

	/**
	 *  Builds the list of countries from the database
	 */
	private function build_countries($all = false)
	{
		global $wpdb;

		$query = "SELECT id, post_id, title, kode, images, passport, active
					FROM " . $this->country_table . 
					(($all) ? '' : " WHERE active=1") . " 
					ORDER BY title asc";
		$countries = $wpdb->get_results($query, ARRAY_A);
		$this->countries = array();

		foreach($countries as $data)
		{
			$this->countries[$data['id']] = array('title' => $data['title'],
												  'post_id' => $data['post_id'],
												  'code' => $data['kode'],
											      'images' => $data['images'],
												  'passport' => $data['passport'],
												  'active' => $data['active']
												  );
		}
	}

	/**
	 *  Builds the list of regions from the database
	 */
	private function build_regions()
	{
		global $wpdb;

		$query = "SELECT id, country_id, title, kode, description, images, active
					FROM " . $this->region_table . "
					ORDER BY title asc";
		$regions = $wpdb->get_results($query, ARRAY_A);

		foreach($regions as $data)
		{
			$this->regions[$data['id']] = array('country_id' => $data['country_id'],
											    'title' => $data['title'],
												'code' => $data['kode'],
												'description' => $data['description'],
												'active' => $data['active']
												);
		}
	}

	/**
	 * Creates a list of regions for a country
	 *
	 * @return array The array of regions in a country
	 */
	private function regions_in_country()
	{
		$r_in_c_list = array();
		if(isset($this->country) && $this->country)
		{
			foreach($this->regions as $id => $data)
			{
				if($data['country_id'] == $this->country)
				{
					$r_in_c_list[$id]=$data;
				}
			}
		}
		return $r_in_c_list;
	}

	/**
	 * Builds a string of data about <something> and saves it to the database
	 */
	private function update_cp()
	{
		self::build_countries();
		
		$pre_info = "";
		$pre_info .= 'a:9:{s:4:"type";s:8:"checkbox";s:12:"instructions";s:0:"";s:8:"required";';
		$pre_info .= 'i:0;s:17:"conditional_logic";i:0;';
		$pre_info .= 's:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}';

		$country_list = '';
		$cnt=0;
		foreach($this->countries as $id=>$data)
		{
			if($data['active']==1)
			{
				$country_list .= "i:" . $id . ";s:" . strlen($data['title']) . ":\"" . $data['title'] . "\";";		
				$cnt++;
			}
		}
		$country_list .= '}';
		
		$write = $pre_info . 's:7:"choices";a:' . $cnt . ':{' . $country_list . 
				 's:13:"default_value";a:0:{}s:6:"layout";s:10:"horizontal";s:6:"toggle";i:0;}';
				 
		wp_update_post(array('ID' => $this->custom_post_id, 'post_content' => $write));
		wp_update_post(array('ID' => 20725, 'post_content' => $write));
		
	}
}