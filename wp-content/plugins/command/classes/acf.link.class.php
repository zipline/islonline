<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * ACF_LINK class
 * Functionality for linking ACF for Admin usage
 */
CLASS ACF_LINK
{
	/** @var array $fields list of fields and their parameters. */
	private $fields = NULL;
	
	private $group = NULL;
	
	private static $date_ui = false;

	private static $media_queued = false;

	/**
	 * Default Constructor
	 * Sets the path and calls the initiation function
	 */
	public function __construct()
	{
		//
		//Something may go here eventually
	}

	private function get_field_group($field_id)
	{
		global $wpdb;
		
		$query = 'SELECT post_parent FROM ' . $wpdb->prefix . 'posts WHERE post_name ="' . $field_id . '"';
		$ret_val = $wpdb->get_var($query);
		
		return $ret_val;
	}
	
	public function save_field_group($field_id, & $data, $user_id = 0, $post_id = 0, $unset = false)
	{
		$fld_group = self::get_field_group($field_id);
		self::get_fields($fld_group);

		if(!empty($this->fields))
		{
			foreach($this->fields as $fld_name => $fld_data)
			{
				$fld_type = self::field_type($fld_data['fld_id']);
				$data_write = empty($data[$fld_name]) ? '' : $data[$fld_name];

				if($fld_type=="checkbox" || $fld_type=="post_object")
				{
					if(!empty($data_write) && is_array($data_write))
					{
						$tmp_arr = array();

						foreach($data_write as $index => $should_be_on)
						{
							$tmp_arr[] = $index;
						}

						$data_write = $tmp_arr;
					}
				}

				switch(self::form_type())
				{
					case 'user_form':
						update_user_meta($user_id, "_" . $fld_data['short'], $fld_name);
						update_user_meta($user_id, $fld_data['short'], $data_write);
						break;
					case 'post_type':
					default:
						update_post_meta($post_id, "_" . $fld_data['short'], $fld_name);
						update_post_meta($post_id, $fld_data['short'], $data_write);
						break;
				}
			}	

			foreach($this->fields as $fld_name => $fld_data)
			{
				//if($unset && !empty($data[$fld_name]))
				//{
					unset($data[$fld_name]);
				//}
			}
		}
	}

	public function get_fields ($post_id = 0)
	{
		$this->fields = array();
		
		if($post_id > 0)
		{
			global $wpdb;
			
			$group = get_post_field('post_content', $post_id, 'display');

			if(!empty($group))
			{
				$this->group = unserialize($group);	
	
				$query = "SELECT ID, post_content, post_title, post_name, post_excerpt 
						  FROM " . $wpdb->prefix . "posts
						  WHERE post_parent = " . $post_id . " AND post_type = 'acf-field'
						  ORDER BY menu_order";
				$fields = $wpdb->get_results($query, ARRAY_A);

				if(!empty($fields))
				{
					foreach($fields as $fld_num => $fld_data)
					{
						$this->fields[$fld_data['post_name']] = array('fld_id' => $fld_data['post_name'],
																	  'content' => $fld_data['post_content'],
																	  'title' => $fld_data['post_title'],
																	  'short' => $fld_data['post_excerpt'],
																	  'post_id' => $fld_data['ID']);					
					}
					return $this->fields;
				}
			}
		}
		return false;	
	}

	public function form_data()
	{
		if(!empty($this->group))
		{
			return $this->group;
		}
		return false;
	}

	public function form_type()
	{
		if(!empty($this->group))
		{
			return $this->group['location'][0][0]['param'];	
		}
	}
	
	public function get_field ($field_id)
	{
		if(!empty($this->fields))
		{
			if(isset($this->fields[$field_id]))
			{
				return $this->fields[$field_id];
			}
		}
		return false;
	}

	public function build_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$fld_check = self::field_type($field_id);

		if(!empty($fld_check))
		{
			switch($fld_check)
			{
				case 'date_picker':
					return self::date_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'email':
					return self::email_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'image':
					return self::image_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'radio':
					return self::radio_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'select':
					return self::select_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'text':
					return self::text_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'number':
					return self::number_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'checkbox':
					return self::checkbox_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'textarea':
					return self::textarea_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'repeater':
					return self::repeater_field($field_id, $user_id, $post_id, $fld_data);
					break;
				case 'post_object':
					return self::post_object_field($field_id, $user_id, $post_id, $fld_data);
					break;
				default:
					break;
			}
		}
	}

	private function field_type($field_id)
	{
		if(!empty($this->fields))
		{
			if(isset($this->fields[$field_id]))
			{
				$core_fld = unserialize($this->fields[$field_id]['content']);
				return $core_fld['type'];
			}
		}
		return false;
	}

	private function post_id($field_id)
	{
		if(!empty($this->fields))
		{
			if(isset($this->fields[$field_id]))
			{
				return $this->fields[$field_id]['post_id'];
			}
		}
		return false;
	}

	private function build_div($fld_data)
	{
		$ret_val = '';
		
		if(!empty($fld_data['content']))
		{
			$flds = unserialize($fld_data['content']);
			$values = array('class' => '', 'id' => '');

			if(!empty($flds['wrapper']))
			{
				$wrap = $flds['wrapper'];
				if(!empty($wrap['class']))
				{
					$values['class'] = $wrap['class'];				
				}
	
				if(!empty($wrap['id']))
				{
					$values['id'] = $wrap['id'];				
				}
			}
		
			$ret_val = "<div";
			$ret_val .= " class='acf-field acf-" . $fld_data['fld_id'] . (!empty($values['class']) ? ' ' . $values['class'] : '') . "'";
			$ret_val .= (!empty($values['id']) ? " id = '" . $values['id'] . "'" : '');
			$ret_val .= ">";
		}

		return $ret_val;
	}

	private function date_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		if(!self::$date_ui)
		{
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_style('jquery-style',
							 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

			self::$date_ui = true;
		}

		$field_info_arr = self::get_field($field_id);
		$field_info_content = unserialize($field_info_arr['content']);

		$act_value = self::field_value($field_id, $user_id, $post_id);
		$arr_value = date_parse_from_format('Ymd', $act_value);
		$mod_value = date($field_info_content['display_format'], mktime(0, 0, 0, $arr_value['month'], $arr_value['day'], $arr_value['year']));
		
		$ret_string = self::build_div($fld_data);
		$ret_string .= '<input name="acf[' . $field_id . ']" id="acf-' . $field_id . '" 
					    class="regular-text datepick" type="text"
						value="' . $mod_value . '">';
		$ret_string .= "</div>";

		$ret_string .= '<script type="text/javascript">';
		$ret_string .= '     jQuery(document).ready(function() {';
		$ret_string .= '          jQuery("#' . $field_id . '").datepicker({';
		$ret_string .= '               dateFormat : "dd-mm-yy"';
		$ret_string .= '          });';
		$ret_string .= '     });';
		$ret_string .= "</script>";
    
		return $ret_string;	
	}
	
	private function email_field($field_id, $user_id = 0, $post_id = 0, $fld_data  = array())
	{

		$field_info_arr = self::get_field($field_id);
		$field_info_content = unserialize($field_info_arr['content']);
		$field_info_instructions = $field_info_content['instructions'];

		$ret_string = self::build_div($fld_data);
		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		$ret_string .= "<input type='email' 
							  name='acf[" . $field_id . "]' 
							  id='acf-" . $field_id . "' 
							  value='" . self::field_value($field_id, $user_id, $post_id) . "' 
							  class='regular-text' />";
		$ret_string .= "</div>";
		
		return $ret_string;		
	}

	private function image_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		if(!self::$media_queued)
		{
			wp_enqueue_media();
			wp_enqueue_script('cmd_media_uploader', 
							  plugins_url() . '/command/js/media.uploader.new.js');
			self::$media_queued = true;
		}

		$ret_string = self::build_div($fld_data);
		
		$img_post = self::field_value($field_id, $user_id, $post_id);

		if(!empty($img_post))
		{
			$meta_data = get_post_meta($img_post, '_wp_attached_file', true);
			$img_path = site_url('/wp-content/uploads/') . $meta_data;
		}
		else
		{	
			$meta_data = '';
			$img_path = '';
		}
	
		$ret_string .= '<input type="hidden" name="acf[' . $field_id . '"] id="acf-' . $field_id . '" value="' . $img_post . '">';

		$ret_string .= '<input type="hidden" name="image_id_' . $field_id . '" id="image_id_' . $field_id .'" value="">';
		$ret_string .= '<input type="hidden" name="image_url_' . $field_id . '" id="image_url_' . $field_id .'" value="">';

		if(!empty($img_path))
		{
			$ret_string .= '<a href="' . $img_path . '" target="_blank">';
		}
		
		$ret_string .= '<img src="' . $img_path . '" name="image_src_' . $field_id . '" id="image_src_' . $field_id . '" alt="" style="width:60px;height:60px;border-radius: 50%; vertical-align: middle;">';

		if(!empty($img_path))
		{
			$ret_string .= '</a>';
		}

		$ret_string .= '<span style="verticle-align: middle;">&nbsp;&nbsp;&nbsp; <a href="#" name="insert_my_media_' . $field_id . '" id="insert_my_media_' . $field_id . '" class="button image_add_btn">Add Image</a></span>';

		$ret_string .= "</div>";

		return $ret_string;
	}

	private function radio_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$field_info_arr = self::get_field($field_id);
		$field_info_content = unserialize($field_info_arr['content']);
		$field_info_instructions = $field_info_content['instructions'];
		$field_options_arr = $field_info_content['choices'];

		$ret_string = self::build_div($fld_data);
		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";

		foreach($field_options_arr as $p1 => $label)
		{
			$options = explode(":", $label);

			$ret_string .= '<INPUT type="radio" 
								   name="acf[' . $field_id . ']" 
								   value="' . $options[0] . '"
								   ' . (($options[0] == self::field_value($field_id, $user_id, $post_id)) ? ' checked' : '') . '
								   >' . $options[0] . '<br />';
		}

		$ret_string .= "</div>";
		
		return $ret_string;	
	}

	private function checkbox_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$ret_string = self::build_div($fld_data);
		$cnt = 1;

		$field_info_arr = self::get_field($field_id);
		$field_info_content = unserialize($field_info_arr['content']);
		$field_info_instructions = $field_info_content['instructions'];

		$field_options_arr = $field_info_content['choices'];
		$field_layout = $field_info_content['layout'];

		$checks = self::field_value($field_id, $user_id, $post_id);

		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		foreach($field_options_arr as $p1 => $label)
		{
			$options = explode(":", $label);
			//echo var_dump($p1);

			$ret_string .= '<INPUT type="checkbox" 
								   name="acf[' . $field_id . '][' .  $p1 . '] 
								   value="' . $p1 . '"
								   ' . (is_array($checks) && in_array($p1, $checks) ? ' checked' : '') . '
								   >' . $options[0] . ' ';

			if($field_layout == 'vertical')
			{
				$ret_string .= "<br />";
			}
			else
			{
				$cnt++;
				if($cnt % 6 == 0)
				{
					$ret_string .="<br /";
				}
			}
		}

		$ret_string .= "</div>";
		
		return $ret_string;	
	}
 
	private function select_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$field_info_arr = self::get_field($field_id);
		$field_info_content = unserialize($field_info_arr['content']);
		$field_options_arr = $field_info_content['choices'];
		$field_info_instructions = $field_info_content['instructions'];

		$multiple = (($field_info_content['multiple'] == 1) ? ' multiple' : '');
		
		$ret_string = self::build_div($fld_data);
		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		$ret_string .= '<SELECT name="acf[' . $field_id . ']" id="acf-' . $field_id . '"' . $multiple . '>';

		foreach($field_options_arr as $index => $label)
		{
			$ret_string .= '<OPTION value="' . $index . '"';
			$ret_string .= (($index == self::field_value($field_id, $user_id, $post_id)) ? ' SELECTED' : '');
			$ret_string .= '>' . $label . '</OPTION>'; 			

		}
		
		$ret_string .= '<SELECT>';	
		$ret_string .= "</div>";
		
		return $ret_string;	
	}

	private function text_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$field_info_arr = self::get_field($field_id);

		$field_info_content = unserialize($field_info_arr['content']);
		$field_info_instructions = $field_info_content['instructions'];

		$ret_string = self::build_div($fld_data);
		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		$ret_string .= '<input type="text" 
							  name="acf[' . $field_id . ']" 
							  id="acf-' . $field_id . '" 
							  value="' . self::field_value($field_id, $user_id, $post_id, $fld_data) . '" 
							  class="regular-text" />';
		$ret_string .= '</div>';
		
		return $ret_string;		
	}

	private function number_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$field_info_arr = self::get_field($field_id);

		$field_info_content = unserialize($field_info_arr['content']);
		$field_info_instructions = $field_info_content['instructions'];

		$ret_string = self::build_div($fld_data);
		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		$ret_string .= '<input type="number" 
							  name="acf[' . $field_id . ']" 
							  id="acf-' . $field_id . '" 
							  value="' . self::field_value($field_id, $user_id, $post_id) . '" 
							  class="regular-text" />';
		$ret_string .= '</div>';
		
		return $ret_string;		
	}

	private function textarea_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$field_info_arr = self::get_field($field_id);

		$field_info_content = unserialize($field_info_arr['content']);
		$field_info_instructions = $field_info_content['instructions'];

		$fld_data = unserialize($this->fields[$field_id]['content']);
		$fld_rows = $fld_data['rows'];
		
		$ret_string = self::build_div($fld_data);
		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		$ret_string .= '<textarea 
						  name="acf[' . $field_id . ']" 
						  id="acf-' . $field_id . '" 
						  rows="' . $fld_rows . '"
						  cols="45"	
						  class="regular-text">' . 
						  self::field_value($field_id, $user_id, $post_id) . '
					  </textarea>';	  
		$ret_string .= "</div>";
		
		return $ret_string;		
	}
	
	private function repeater_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$original_fields = $this->fields;
		$original_groups = $this->group;
		
		$fld_data = unserialize($this->fields[$field_id]['content']);
		$min_rows = $fld_data['min'];
		$max_rows = $fld_data['max'];
		
		
		$content = self::get_fields($original_fields[$field_id]['post_id']);
		//$field_info_instructions = $field_info_content['instructions'];

		$this->group = $original_groups;
		
		//$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		$ret_string = "<table>";

		for($repeats = 0; $repeats <= ($max_rows-1); $repeats++)
		{
			foreach($content as $fld_id => $fld_data)
			{
				$fld_data['repeats'] = $repeats;
				
				$ret_string .= "<TR>";
				$ret_string .=  "<th><label for='acf-" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
				$ret_string .=  "<td>" . self::build_field($fld_id, 0, $post_id, $fld_data) . "</td>";
				$ret_string .=  "</TR>";				
			}
		}
		
		$ret_string .= "</table>";
	
		$this->fields = $original_fields;

		return $ret_string;
	}
	
	private function post_object_field($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		$field_info_arr = self::get_field($field_id);

		$field_info_content = unserialize($field_info_arr['content']);
		$field_info_instructions = $field_info_content['instructions'];

		$cnt = 1;
		$ret_string = self::build_div($fld_data);
		$ret_string .= "<p style='font-style: italic;'>" . $field_info_instructions . "</p>";
		$ret_string .= "<UL>";

		$post_type = $field_info_content['post_type'][0];
		$checks = self::field_value($field_id, $user_id, $post_id);
		
		//
		//Find all flavors
		$flavor_args = array(
				'posts_per_page'  	=> -1,
				'post_type'       	=> $post_type,
				'fields'          	=> array('ids', 'post_title'),
				'meta_key'			=> 'flavor_name',
				'orderby'			=> 'meta_value',
				'order'				=> 'ASC',
				'post_status'		=> 'publish'
			  );
		$flavors = get_posts($flavor_args);

		//echo var_dump($flavors);

		$checks = self::field_value($field_id, $user_id, $post_id);

		foreach($flavors as $fld_id => $info)
		{
			$ret_string .= '<LI><INPUT type="checkbox" 
								   name="acf[' . $field_id . '][' . $info->ID . ']
								   value="' . $info->ID . '"
								   ' . (is_array($checks) && in_array($info->ID, $checks) ? ' checked' : '') . '
								   >' . $info->post_title . '</LI> ';
		}

		$ret_string .= "</UL></div>";
		
		return $ret_string;	

	}

	private function field_value($field_id, $user_id = 0, $post_id = 0, $fld_data = array())
	{
		global $wpdb;
		$value = '';
		
		switch(self::form_type())
		{
			case 'user_form':
				$table = $wpdb->prefix . 'usermeta';
				
				if(is_numeric($user_id))
				{
					$id_param = ' user_id = ' . $user_id;
				}
				else
				{
					$id_param = ' user_id = 0';
				}
				break;
			case 'post_type':
			default:
				$table = $wpdb->prefix . 'postmeta';
				
				if(!empty($post_id))
				{
					$id_param = ' post_id = ' . $post_id;
				}
				else
				{
					$id_param = ' 0 = 1';
				}
				break;
		}
		
		$query = "SELECT meta_key FROM " . $table . " where meta_value = '" . $field_id . "' AND " . $id_param;

		$field_identifier = $wpdb->get_var($query);

		if(!empty($field_identifier))
		{ 
			$actual_field = substr($field_identifier, 1);
			
			if(!empty($fld_data['repeats']))
			{
				$replacement = $fld_data['repeats'] . '_' . $fld_data['short'];
				$replacing = '0' . '_' . $fld_data['short'];
				
				$actual_field = str_replace($replacing, $replacement, $actual_field);
			}

			switch(self::form_type())
			{
				case 'user_form':
					$value = get_user_meta($user_id, $actual_field, true);
					break;
				case 'post_type':
				default:
					$value = get_post_meta($post_id, $actual_field, true);
					break;
			}
		}

		return $value;
		
	}

	
}