<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');
require_once (dirname(__FILE__) . '/cmonitor/csrest_transactional_classicemail.php');

/**
 * CMONITOR Class
 * defines functionality to be utilized in other classes in this plugin
 */
ABSTRACT CLASS CMONITOR
{
	/** @var Integer Numeric limit of emails to be sent at a time. */
	private $email_limit = 25;
	/** @var Boolean Success. */
	private $success = 0;
	/** @var Wrap. */
	private $wrap = NULL;
	/** @var Array List of errors that have occurred. */
	private $errors = array();
	/** @var Integer Level of the error */
	private $error_level = 0;
	/** @var Integer Maximum number of errors */
	private $max_error = 0;

	/** @var String API Key */
	const API_KEY = 'a57d29a39e46c6e8dbd1f6a88096f736';
	/** @var String Client ID */
	const CLIENT_ID = '10956f4b9aa1e6a3bc5682cec80fb14d';

	/**
	 * Default Constructor
	 *
	 * @param Boolean $cookie Appears to do nothing currently
	 */
	public function __construct($cookie = false)
	{
		$auth = array("api_key" => self::API_KEY);
		$this->wrap = new CS_REST_Transactional_ClassicEmail($auth, NULL);
		$this->wrap->set_client(self::CLIENT_ID);
	}
	
	/**
	 * Name Getting Functions
	 */

	/**
	 * Get the to field for an email
	 */
	abstract protected function get_to();
	
	/**
	 * Get the CC field for an email
	 */
	abstract protected function get_cc();

	/**
	 * Get the BCC field for an email
	 */
	abstract protected function get_bcc();

	/**
	 * Get the From field for an email
	 */
	abstract protected function get_from();

	/**
	 * Get the reply-to field for an email
	 */
	abstract protected function get_reply_to();

	/**
	 * Message Getting Functions
	 */

	/**
	 * Get the subject of the message
	 */
	abstract protected function get_subject();

	/**
	 * Get the message text
	 */
	abstract protected function get_message();

	/**
	 * Get the signature of the message
	 */
	abstract protected function get_signature();

	/**
	 * Message Handling Functions
	 */

	/**
	 * Check if the item is a group
	 */
	abstract protected function is_group();

	/**
	 * Get the options
	 */
	abstract protected function get_options();

	/**
	 * 
	 */
	abstract public function group();

	/**
	 * Send message
	 */
	abstract public function send();

	/**
	 * Builds the message and sends it
	 */
	protected function send_message()
	{
		$build = array();
		$options = array();
		$response = '';
		$tries = 0;
		$group_name = '';
		$this->success = 0;
		
		//
		//Set who we are
		$from = self::format_address($this->get_from());
		$build['From'] = array_shift($from);

		if(!empty($this->get_reply_to()))
		{
			$replyto = self::format_address($this->get_reply_to());
			$build['ReplyTo'] = array_shift($replyto);
		}
		else
		{
			$build['ReplyTo'] = $build['From'];
		}
		
		//
		//Build the message, etc
		$build['Subject'] = $this->get_subject();
		$build['HTML'] = $this->get_message();
		
		if(!empty($this->get_signature()))
		{
			$build['HTML'] .= '<br><br>' . $this->get_signature();
		}
		
		//
		//Set options
		$opts = $this->get_options();
		
		if(!empty($opts))
		{
			if(!empty($opts['group_name']))
			{
				$group_name = $opts['group_name'];
			}
			
			if(!empty($opts['TrackOpens']))
			{
				$options['TrackOpens'] = true;
			}
			if(!empty($opts['TrackClicks']))
			{
				$options['TrackClicks'] = true;
			}
			if(!empty($opts['InlineCSS']))
			{
				$options['InlineCSS'] = true;
			}
		}

		if($this->is_group())
		{
			//
			//Can set to and cc and bcc here since it's a group email	
			$to = self::queue_address('to');
			$cc = self::queue_address('cc');
			$bcc = self::queue_address('bcc');

			if(!empty($to))
			{
				$build['To'] = $to;
			}
			if(!empty($cc))
			{
				$build['CC'] = $cc;
			}
			if(!empty($bcc))
			{
				$build['BCC'] = $bcc;
			}
			
			$response = $this->wrap->send($build, $group_name, '', $options);

		}
		else
		{
			//
			//Run names one at a time as we're set to single email
			foreach(self::format_address($this->get_to()) as $address)
			{
				$build['To'] = $address;
				$tries++;
				$response[] = $this->wrap->send($build, $group_name, '', $options);
			}
			
		}
		
		return $response;
	}

	/**
	 * Returns list of emails based on which type of to method it is (to, cc, bcc)
	 *
	 * @param string $which - the type of send method
	 *
	 * @return array $address_list - the formatted list of addresses for that sending method
	 */
	private function queue_address($which)
	{
		switch ($which)
		{
			case 'to':
				$address_list = self::format_address($this->get_to());
				break;
			case 'cc':
				$address_list = self::format_address($this->get_cc());
				break;
			case 'bcc':
				$address_list = self::format_address($this->get_bcc());
				break;	
		}

		return $address_list;
	}

	/**
	 * Formats the addresses in the address_list to "name < address >" format and 
	 * adds them to an array which is returned
	 *
	 * @param array $address_list - associative array of email addresses
	 *
	 * @return array $formatted_list - array of formatted email addresses
	 */
	private function format_address($address_list = array())
	{
		$formatted_list = array();

		if(!empty($address_list))
		{
			foreach($address_list as $addy => $name)
			{
				if(empty($name))
				{
					$formatted_list[] = $addy;
				}
				else
				{
					$formatted_list[] = $name . " <" . $addy . ">";
				}
			}
			
		}
		return $formatted_list;
	}

	/**
	 * Error handling functions
	 */

	/**
	 * Returns whether there is an error or not
	 *
	 * @return Boolean True if there is an error | False if not
	 */
	public function error()
	{
		return (!empty($this->errors) ? true : false);
	}

	/**
	 * Returns the max error
	 *
	 * @return Integer The max error level
	 */
	public function error_level()
	{
		return $this->max_error;
	}

	/**
	 * Returns the list of errors.
	 *
	 * @return Array The list of errors
	 */
	public function error_list()
	{
		return $this->errors;
	}

	/**
	 * Adds an error to the current error array, and checks if this error
	 *
	 * @param string $function The function which caused the error
	 * @param string $message The error message for the error
	 * @param integer $error_level The error level for the error
	 */
	private function add_error($function, $message, $error_level)
	{
		$this->errors[] = array($function, $message, $error_level);
		$this->max_error = max($this->max_error, $error_level);		
	}
}