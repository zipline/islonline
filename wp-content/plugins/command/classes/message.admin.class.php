<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');


require_once (CC_CLASSES_DIR . "email.class.php");

//
//2018-04-16 DJT Table alteration for finance update
//ALTER TABLE  `wp_command_admin_message` ADD  `finance` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `notify` ;


/**
 * TEAM_NOTE class
 * Functionality for team notes
 */
CLASS ADMIN_MESSAGES
{
	
	/** @var string $team_note_table: Database table for notes */
	private $admin_messages_table;

	/** @var string $event: Event trigger */
	private $event;

	/** @var array $meta_info: array of potential meta info used to build event trigger */
	private $meta_info = array();

	/** @var string $message: Message text for event */
	private $message;

	/** @var string $subject: Message subject */
	private $subject;

	/** @var string $subject: Message subject */
	private $notify;
	
	//
	//2018-04-16 DJT new finance variable
	/** @var string $subject: Message subject */
	private $finance;

	/** @var array $event_types: Static list of event types to be used */
	public static $event_types = array(
		"status_change_open_to_go" => "Team Status Change from Open to Go",
		"status_change_open_to_closed" => "Team Status Change from Open to Closed",
		"new_personal_message" => "New Personal Message",
		"new_team_message" => "New Team Message",
		"60_days_before_trip_not_2/3_paid" => "60 Days Until Trip and Less Than 2/3 Paid",
		"30_days_before_trip_not_2/3_paid" => "30 Days Until Trip and Not 2/3 Paid",
		"14_days_before_trip_not_2/3_paid" => "14 Days Until Trip and Not 2/3 Paid",
		"2_days_before_trip_not_paid" => "2 Days Until Trip and Not Fully Paid",
		"2_days_before_trip" => "2 Days Until Trip",
		"volunteer_document_unchecked" => "Volunteer Document Unchecked",
		"team_document_updated" => "Team Document Updated",
		"user_document_added" => "A new Document has been added",
	);

	/**
	 * Default Constructor 
	 * Sets the DB tables and initializes statuses and types
	 */
	public function __construct()
	{
		global $wpdb;

		$this->admin_messages_table = $wpdb->prefix . 'command_admin_message';
		$this->set_event("");
		$this->set_message("");
		$this->set_subject("");
	}

	public function set_event($event)
	{
		//if($this->is_message($event))
		//{
			$this->event = $event;
		//}
	}

	public function get_event()
	{
		return $this->event;
	}

	public function set_subject($subject)
	{
		$this->subject = $subject;
	}

	public function get_subject()
	{
		return $this->subject;
	}
	public function set_message($message)
	{
		$this->message = $message;
	}

	public function get_message()
	{
		return $this->message;
	}

	public function set_notify($notify)
	{
		$this->notify = $notify;
	}

	public function get_notify()
	{
		return $this->notify;
	}

	//
	//2018-04-16 DJT NEw Finance func tions
	public function set_finance($finance)
	{
		$this->finance = $finance;
	}

	public function get_finance()
	{
		return $this->finance;
	}

	public function set_meta_info($meta_info)
	{
		$this->meta_info = $meta_info;
	}

	public function add_meta_info($meta_key, $meta_value)
	{
		$this->meta_info[$meta_key] = $meta_value;
	}

	public function get_meta_info()
	{
		return $this->meta_info;
	}

	/**
	 * Load the Admin Message from the db based on the current event
	 */
	public function load()
	{
		global $wpdb;
		$message = $wpdb->get_row("SELECT * from $this->admin_messages_table WHERE event = \"$this->event\" LIMIT 1");
		if(!empty($message))
		{
			$this->set_meta_info(unserialize($message->meta_info));
			$this->set_message($message->message);
			$this->set_subject($message->subject);
			$this->set_notify($message->notify);
			//
			//2018=-4=16 DJT Added new variable
			$this->set_finance($message->finance);
		}
	}

	/**
	 * Add the current Admin Message to the db
	 */
	public function add()
	{
		global $wpdb;
		$meta_info = serialize($this->meta_info);

		//
		//2018=-4=16 DJT Added new variable
		$wpdb->insert(
			$this->admin_messages_table,
			array(
				'event' => $this->event,
				'message' => $this->message,
				'subject' => $this->subject,
				'notify' => $this->notify,
				'finance' => $this->finance,
				'meta_info' => serialize($this->meta_info),
			),
			array(
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
			)
		);
	}

	/**
	 * Edit the current Admin Message
	 */
	public function edit()
	{
		global $wpdb;

		$wpdb->update(
			$this->admin_messages_table,
			array(
				'message' => $this->message,
				'subject' => $this->subject,
				'notify' => $this->notify,
				'finance' => $this->finance,
				'meta_info' => serialize($this->meta_info),
			),
			array( 'event' => $this->event ),
			array(
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
			),
			array( '%s' )
		);
	}

	/**
	 * Remove the current Admin Message for the event
	 */
	public function destroy()
	{
		global $wpdb;

		$wpdb->query($wpdb->prepare(
			"DELETE FROM $this->admin_messages_table
			 WHERE event = %s",
			 $this->event)
		);
		$wpdb->query($wpdb->prepare(
			"DELETE FROM wp_command_trigger
			 WHERE id = %s",
			 $this->event)
		);
	}

	/**
	 * Get all Admin Messages
	 */
	public function get_all()
	{
		global $wpdb;

		$messages = $wpdb->get_results(
			"SELECT * FROM $this->admin_messages_table"
		);

		return $messages;
	}

	/**
	 * Returns a trimed event list down to unused events for drop down list
	 *
	 * @return array: List of unused events
	 */
	public function trim_event_list()
	{
		global $wpdb;
		$used_events = $wpdb->get_col(
			"SELECT event
			FROM $this->admin_messages_table");

		$return_events = $this::$event_types;
		foreach($used_events as $used_event)
		{
			unset($return_events[$used_event]);
		}
		return $return_events;
	}

	public function active_event_list()
	{
		global $wpdb;
		return $wpdb->get_col(
			"SELECT event
			FROM $this->admin_messages_table");
	}

	/**
	 * Check if message is an actual message
	 */
	public function is_message($event)
	{
		return array_key_exists($event,$this::$event_types);
	}

	/**
	 * Checks if message is being used currently
	 *
	 * @param string $event: The event to be checked
	 *
	 * @return boolean: true if the event is in active (in the db) or false if it isn't
	 */
	public function is_active($event)
	{
		global $wpdb;
		return $wpdb->get_var("SELECT COUNT(event)
			FROM $this->admin_messages_table");
	}
}