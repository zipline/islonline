<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "email.class.php");

/**
 * TRIGGERS class
 * Functionality for triggers which can have actions appended to them and messages
 */
CLASS TRIGGERS
{
	
	/** @var string $trigger_table: Database table for triggers*/
	private $trigger_table;

	/** @var string $trigger_event_table: Database table for trigger events*/
	private $trigger_event_table;

	/** @var string $triggermeta_table: Database table for trigger metadata*/
	private $triggermeta_table;

	/** @var string $type: Type of trigger (event,schedule) */
	private $type;

	/** @var string $name: Name of the tirgger */
	private $name;

	/** @var string $full_name: Full human-readable name, parsed from trigger metadata */
	private $full_name;

	/** @var array $meta_data: Meta data that contains the specifics for the trigger */
	private $meta_data = array();

	/** @var integer $run_time: The integer value of the hour (0-23) to run the trigger at */
	private $run_time;

	
	private $notify = 0;
	
	/**
	 * Default Constructor 
	 * Sets the DB tables and initializes statuses and types
	 */
	public function __construct($trigger_id = null)
	{
		global $wpdb;

		$this->trigger_table = $wpdb->prefix . 'command_trigger';
		$this->trigger_event_table = $wpdb->prefix . 'command_trigger_event';
		$this->triggermeta_table = $wpdb->prefix . 'command_triggermeta';
		$this->notify = 0;
		
		if(!empty($trigger_id))
		{

		}
	}

	/**
	 * Gets all of the current triggers set or triggers of a specificy type
	 *
	 * @param string $trig_type: Type of trigger to get 
	 *
	 * @return: array: List of triggers that exist in std obj format
	 */
	public function get_all_triggers($trig_type = null)
	{
		global $wpdb;

		$query="SELECT * FROM $this->trigger_table";

		if($trig_type != null)
		{
			$query .= " WHERE trigger_type = $trig_type";
		}

		return $wpdb->get_results($query);
	}

	public function build_trigger_meta($trigger_meta)
	{
		$this->meta_data=unserialize($trigger_meta);
	}

	public function compress_trigger_meta()
	{
		return serialize($this->meta_data);
	}

	public function set_meta_data($meta_data)
	{
		$this->meta_data=$meta_data;
	}

	public function get_meta_data()
	{
		return $this->meta_data;
	}

	public function get_trigger($trigger_id)
	{
		global $wpdb;

		$query="SELECT * FROM $this->trigger_table
			WHERE id = $trigger_id";

		return $wpdb->get_results($query);
	}

	public function set_name($name)
	{
		$this->name=$name;
	}

	public function get_name()
	{
		return $this->name;
	}

	public function set_notify($notify = 0)
	{
		$this->notify = $notify;
	}
	
	public function build_full_name()
	{
		$full_name = "";
		$meta = $this->get_meta_data();
		end($meta);
		$end_key = key($meta);
		reset($meta);
		foreach($meta as $meta_key => $meta_value)
		{
			if(!in_array($meta_key,array("type",'interval','filter_value')))
			{
				$full_name .=$this->build_sub_name($meta_key,$this->get_from_meta($meta_key));
			}
			else if(in_array($meta_key,array('interval','filter_value')))
			{
				$full_name .=$this->build_sub_name($meta_key, $meta_value);
			}
		}
		$this->full_name = $full_name;
		return $this->full_name;
	}

	private function build_sub_name($key,$val)
	{
		switch($key)
		{
			case "name":
				return $val;
				break;

			case "old_value":
				return " from ". $val;
				break;

			case "new_value":
				return " to ". $val;
				break;

			case "interval":
				return $val ." ";

			case "unit":
				return $val ." ";

			case "filter":
				if($val != 'Everyone')
					return " and ". $val;
				break;
				
			case "filter_value":
				if($val != 'Everyone')
					return " is ". $val;
				break;
				
			case "action":
				return ", ". $val;

			default:
				return $key .": ". $val;
		}
	}

	public function get_full_name()
	{
		return $this->full_name;
	}

	public function get_from_meta($key)
	{
		global $wpdb;
		if(!in_array($key,array('interval','filter_value')))
		{
			$key_id=$this->meta_data[$key];
			$query="SELECT meta_value FROM wp_command_triggermeta WHERE id=$key_id";
			$value = $wpdb->get_var($query);
		}
		else
		{
			$value = $this->meta_data[$key];
		}
		
		return $value;
	}

	public function set_type_from_meta()
	{
		$this->set_type($this->get_from_meta('type'));
	}

	public function set_name_from_meta()
	{
		$this->set_name($this->get_from_meta('name'));
	}

	public function load($trigger_obj)
	{
		
		if(isset($trigger_obj[0]))
		{
			$this->build_trigger_meta($trigger_obj[0]->meta);
			$this->set_type($trigger_obj[0]->trigger_type);
			$this->set_run_time($trigger_obj[0]->run_time);
			$this->set_notify($trigger_obj[0]->notify);
		}
		else
		{
			//echo var_dump($trigger_obj);
		}
	}

	public function read($trigger_id)
	{
		$obj = $this->get_trigger($trigger_id);
		$this->load($obj);
	}

	public function set_type($type)
	{
		$this->type = $type;
	}

	public function get_type()
	{
		return $this->type;
	}

	public function set_run_time($run_time = null)
	{
		$this->run_time = $run_time;
	}

	public function get_run_time()
	{
		return $this->run_time;
	}

	public function get_notify()
	{
		return $this->notify;
	}

	public function get_all_meta()
	{
		global $wpdb;

		$query="SELECT * FROM $this->triggermeta_table ORDER BY id ASC";

		return $wpdb->get_results($query);
	}

	public function get_joined_meta($meta_id = "is null")
	{
		global $wpdb;

		$return_results = array();
		$query="SELECT * from wp_command_triggermeta WHERE meta_id $meta_id";

		$results = $wpdb->get_results($query);

		if(!empty($results))
		{
			foreach($results as $result)
			{
				if(strtolower($result->meta_key) == 'filter_value')
				{
					//$return_results[$result->meta_key] = array( 'P' => array('value' => 'Item 1'), 'Q' => array('value' => 'Item 2'));

					$filter = $wpdb->get_var('SELECT meta_value FROM ' . $wpdb->prefix . 'command_triggermeta WHERE id = ' . $result->meta_id);
					switch ($filter)
					{
						case 'country':
							$query = "SELECT id, title FROM " . $wpdb->prefix . "command_country WHERE active=1";
							$vals = $wpdb->get_results($query);
							break;
						case 'program':
							$query = "SELECT id, title FROM " . $wpdb->prefix . "command_program WHERE active=1";
							$vals = $wpdb->get_results($query);
							break;
						case 'region':
							$query = "SELECT id, title FROM " . $wpdb->prefix . "command_regions WHERE active=1 and title !='ALL'";
							$vals = $wpdb->get_results($query);
							break;
						case "Profile":
						case "Passport":
						case "Flight":
						case "Emergency Contact":
							$vals = (object) array( (object) array('id' => '1', 'title' => 'Missing'),
												    (object) array('id' => '2', 'title' => 'Complete')
												   );
							break;
						default:
							//echo $result->meta_id;
							$vals = array('value' => 'NULL');
					}
					foreach($vals as $data)
					{
						//$return_results[$result->meta_key][$data->id] = array('value' => $data->title);
						//echo $result->meta_key . ":" . $data->title . "<BR>"; 
						$return_results[$result->meta_key][$data->title] = array('value' => $data->title);
					}
				}
				else
				{
					$return_results[$result->meta_key][$result->id] = array('value' =>$result->meta_value);
					
					$sub_results = $this->get_joined_meta("= ". $result->id);
					$keys = array_keys($sub_results);
					foreach($keys as $key)
					{
						$return_results[$result->meta_key][$result->id][$key] = $sub_results[$key];
					}
				}
			}
		}

		return $return_results;
	}

	public function save()
	{
		global $wpdb;

		$meta_data = $this->compress_trigger_meta();

		$wpdb->insert(
			$this->trigger_table,
			array(
				'trigger_type' => $this->type,
				'notify' => $this->notify,
				'name' => $this->name,
				'meta' => $meta_data,
				'run_time' => $this->run_time
			),
			array('%s','%d','%s','%s','%d')
		);

		return $wpdb->insert_id;
	}

	/**
	 * Adds the trigger to the que of triggers
	 *
	 * @param string $trigger_name: name of the trigger to be executed
	 * @param array $trigger_args: list of arguments that are used for the specific trigger
	 */
	public function queue_trigger($trigger_name, $trigger_args = array(), $unserialized_trigger_object = array())
	{
		global $wpdb;
		/** get the trigger based on trigger meta */
		$query="SELECT * FROM $this->trigger_table
			WHERE name = \"$trigger_name\"";

		$possible_triggers = $wpdb->get_results($query);

		if(empty($possible_triggers))
		{
			return null;
		}
		/** identify correct trigger */
		$identified_triggers = $this->identify_triggers($possible_triggers,$trigger_args);

		$serialized_trigger_object=serialize($unserialized_trigger_object);

		$trigger_events = $this->add_triggers_to_queue($identified_triggers,$serialized_trigger_object);

		foreach($trigger_events as $event)
		{
			$trigger = $this->get_trigger_from_event($event);
			if(is_null($trigger->run_time))
			{
				$this->pull_trigger($event);
			}
		}
	}

	private function get_trigger_from_event($event_id)
	{
		global $wpdb;
		$query="SELECT * FROM $this->trigger_table as t
			JOIN $this->trigger_event_table as te
			ON t.id=te.trigger_id
			WHERE te.id = $event_id";

		$trigger = $wpdb->get_row($query);
		return $trigger;
	}

	/**
	 * Identify specific trigger(s) from a list of triggers with the same name
	 *
	 * @param array $possible_triggers: List of possible triggers
	 * @param array $trigger_args: List of trigger specific args to parse through to determine the correct trigger(s)
	 *
	 * @return array: List of identified triggers (trigger with identified name and attributes)
	 */
	private function identify_triggers($possible_triggers,$trigger_args)
	{
		$identified_triggers = array();
		foreach($possible_triggers as $t_key => $possible_trigger)
		{
			
			$trigger_meta_data =unserialize($possible_trigger->meta);

			$this->set_meta_data($trigger_meta_data);
			$valid_trigger = true;
			//echo var_dump($trigger_meta_data) . "<BR><BR>";
			foreach($trigger_meta_data as $meta_key => $meta_value)
			{
				$meta_value = $this->get_from_meta($meta_key);
				if(isset($trigger_args[$meta_key]) && strtolower($meta_value) != strtolower($trigger_args[$meta_key]))
				{
					$valid_trigger = false;
					break;
				}
			}
			if($valid_trigger)
			{
				$identified_triggers[] = $possible_trigger;
			}
		}

		return $identified_triggers;
	}

	/**
	 * Insert list of triggers to the trigger queue (trigger_event table)
	 *
	 * @param array $trigger_list: List of triggers to be added
	 *
	 * @return array: List of new triggre event ids
	 */
	private function add_triggers_to_queue($trigger_list,$serialized_trigger_object = null)
	{
		global $wpdb;
		$last_ids = array();
		foreach($trigger_list as $it_id => $identified_trigger)
		{
			if($identified_trigger->run_time == null)
			{
				$wpdb->insert($this->trigger_event_table,
					array(
						'trigger_id' => $identified_trigger->id,
						'trigger_obj' => $serialized_trigger_object,
					),
					array(
						'%s',
						'%s',
					)
				);
				$last_ids[] = $wpdb->insert_id;
			}
		}
		return $last_ids;
	}

	/**
	 * Gets and executes the triggers for the current hour
	 *
	 * @param integer $execute_hour: the integer value for the hour of execution for the triggers
	 */
	public function load_triggers($execute_hour)
	{
		/** get the triggers for the current hour that haven't been run */
		global $wpdb;

		$query="SELECT te.id as event_id FROM
			$this->trigger_table AS t
			JOIN $this->trigger_event_table AS te
			ON $this->trigger_event_table.trigger_id = $this->trigger_table.id
			WHERE executed = 0 AND run_time = $execute_hour";

		$events = $wpdb->get_results($query);
		foreach($events as $e_key => $event)
		{
			$this->pull_trigger($event_id);
		}
	}

	/**
	 * Executes the trigger based on ID
	 *
	 * @param integer $trigger_id: ID of the trigger to be executed/pulled
	 *
	 * @return boolean: True if it executes fully, false if an error happens
	 */
	public function pull_trigger($trigger_event_id)
	{
		global $wpdb;

		$trigger=$this->get_trigger_from_event($trigger_event_id);

		switch($trigger->name)
		{
			case "Team Status Changed":
				$this->team_status_changed($trigger_event_id);
				break;

			case "Team Document Changed":
				$this->team_document_changed($trigger_event_id);
				break;

			case "Team Document Added":
				$this->team_document_added($trigger_event_id);
				break;

			case "Personal Message Sent":
				$this->personal_message_sent($trigger_event_id);
				break;

			case "Team Message Sent":
				$this->team_message_sent($trigger_event_id);
				break;

			case "New Sign-up":
				$this->new_sign_up($trigger_event_id);
				break;

			case "Before Trip Departure":
				$this->before_trip_departure($trigger_event_id);
				break;

			case "After Trip Departure":
				$this->after_trip_departure($trigger_event_id);
				break;

			case "Before Trip Return":
				$this->before_trip_return($trigger_event_id);
				break;

			case "After Trip Return":
				$this->after_trip_return($trigger_event_id);
				break;

			default:
				break;
		}
		$this->update_execution_time($trigger_event_id);
	}

	/**
	 * Updates the executed_at field for the trigger event
	 * 
	 * @param integer $trigger_event_id: ID of the event to be updated
	 */
	private function update_execution_time($trigger_event_id)
	{
		global $wpdb;

		$wpdb->query(
			$wpdb->prepare(
				"UPDATE $this->trigger_event_table SET executed_at = now() WHERE id = %d",
				$trigger_event_id
			)
		);
	}

	/**
	 * Trigger specific functions
	 */

	private function get_trigger_data($event_id)
	{
		global $wpdb;

		$query="SELECT * FROM
			$this->trigger_table AS t
			JOIN $this->trigger_event_table as te
			ON t.id=te.trigger_id
			WHERE te.id= $event_id";
		$trigger_data = $wpdb->get_row($query);

		return $trigger_data;
	}

	private function get_rsvp_starting_at_interval($interval, $unit)
	{
		global $wpdb;

		/** remove the S from the unit (plural) and uppercase it for SQL */
		$unit = strtoupper(substr($unit,0,-1));

		$query = "SELECT r.id as rsvp_id, t.post_id as post_id, r.team_id AS team_id, r.user_id AS user_id from wp_command_schedule as s JOIN wp_command_RSVP as r on r.team_id = s.team_id JOIN wp_command_TEAM as t on t.id=r.team_id  INNER JOIN wp_command_order_items coi on r.order_item_id = coi.id WHERE t.messaging = 1 AND DATE(start_date) = DATE(DATE_ADD(now(), INTERVAL $interval $unit)) AND coi.quantity > 0";

		$rsvp_ids = $wpdb->get_results($query);

		return $rsvp_ids;
	}

	private function get_rsvp_starting_before_interval($interval, $unit)
	{
		global $wpdb;

		/** remove the S from the unit (plural) and uppercase it for SQL */
		$unit = strtoupper(substr($unit,0,-1));

		$query = "SELECT r.id as rsvp_id, t.post_id as post_id, r.team_id AS team_id, r.user_id AS user_id from wp_command_schedule as s JOIN wp_command_RSVP as r on r.team_id = s.team_id JOIN wp_command_TEAM as t on t.id=r.team_id INNER JOIN wp_command_order_items coi on r.order_item_id = coi.id  WHERE t.messaging = 1 AND DATE(start_date) = DATE(DATE_ADD(now(), INTERVAL $interval $unit)) AND coi.quantity > 0";

		$rsvp_ids = $wpdb->get_results($query);

		return $rsvp_ids;
	}

	private function get_rsvp_starting_after_interval($interval, $unit)
	{
		global $wpdb;

		/** remove the S from the unit (plural) and uppercase it for SQL */
		$unit = strtoupper(substr($unit,0,-1));

		$query = "SELECT r.id as rsvp_id, t.post_id as post_id, r.team_id AS team_id, r.user_id AS user_id 
                  from wp_command_schedule as s 
                  JOIN wp_command_RSVP as r on r.team_id = s.team_id 
                  JOIN wp_command_TEAM as t on t.id=r.team_id 
                  INNER JOIN wp_command_order_items coi on r.order_item_id = coi.id  
                  WHERE t.messaging = 1 AND DATE(start_date) = DATE(DATE_ADD(now(), INTERVAL -$interval $unit)) AND coi.quantity > 0";

		$rsvp_ids = $wpdb->get_results($query);

		return $rsvp_ids;
	}

	private function get_rsvp_ending_before_interval($interval, $unit)
	{
		global $wpdb;

		/** remove the S from the unit (plural) and uppercase it for SQL */
		$unit = strtoupper(substr($unit,0,-1));

		$query = "SELECT r.id as rsvp_id, t.post_id as post_id, r.team_id AS team_id, r.user_id AS user_id 
                  from wp_command_schedule as s 
                  JOIN wp_command_RSVP as r on r.team_id = s.team_id 
                  JOIN wp_command_TEAM as t on t.id=r.team_id 
                  INNER JOIN wp_command_order_items coi on r.order_item_id = coi.id  
                  WHERE t.messaging = 1 AND  DATE(end_date) = DATE(DATE_ADD(now(), INTERVAL $interval $unit)) AND coi.quantity > 0";

		$rsvp_ids = $wpdb->get_results($query);

		return $rsvp_ids;
	}

	private function get_rsvp_ending_after_interval($interval, $unit)
	{
		global $wpdb;

		/** remove the S from the unit (plural) and uppercase it for SQL */
		$unit = strtoupper(substr($unit,0,-1));

		$query = "SELECT r.id as rsvp_id, t.post_id as post_id, r.team_id AS team_id, r.user_id AS user_id 
                  from wp_command_schedule as s 
                  JOIN wp_command_RSVP as r on r.team_id = s.team_id 
                  JOIN wp_command_TEAM as t on t.id=r.team_id 
                  INNER JOIN wp_command_order_items coi on r.order_item_id = coi.id  
                  WHERE t.messaging = 1 AND DATE(end_date) = DATE(DATE_ADD(now(), INTERVAL -$interval $unit)) AND coi.quantity > 0";

		$rsvp_ids = $wpdb->get_results($query);

		return $rsvp_ids;
	}

	private function check_rsvp_payments($rsvps, $payment_pct = 1)
	{
		$user_ids = array();
		foreach($rsvps as $rsvp_a)
		{
			$rsvp = new RSVP();
			$finance = new FINANCE();
			$team = new TEAM();
			$team->set_team($rsvp_a->team_id);
			$rsvp->set_rsvp_id($rsvp_a->rsvp_id);
			$reservation = $rsvp->fetch_rsvp();
			
			unset($all_items);

			if ($team->no_finances == 0){
                if(!empty($reservation['order_item_id']))
                {
                    $finance->Fetch_Items(array($reservation['order_item_id']), true);
                    $cost = floatval($finance->Calculate_ItemsCost());
                    $balance_due = floatval($finance->Calculate_ItemsBalance());
                    $max_balance_due = $cost * (1 - floatval($payment_pct));

                    if($balance_due > $max_balance_due && $balance_due != 0)
                    {
                        $user_ids[] = $rsvp_a;
                    }

                    /*
                    $all_items[] = $reservation['order_item_id'];
                    $pays = 0;
                    foreach($finance->payments($all_items) as $payments)
                    {
                        $pays += $payments['amount'];
                    }
                    $total_cost = number_format(($team->trip_cost() + $pays),2);
                    if(number_format($pays,2) < number_format($total_cost * $payment_pct))
                    {
                        $user_ids[] = $rsvp_a;
                    }
                    */
                }
            }
		}
		return $user_ids;
	}

	private function parse_filter($filter,$filter_args = array())
	{
		global $wpdb;

		switch(strtolower($filter))
		{
			case "country":
				/** found in command_schedule (id) associated by rsvp team_id*/
				/** title in wp_command_country */
				$filtered_rsvps = array();
				foreach($filter_args['rsvp'] as $rsvp)
				{
					$query="SELECT title FROM wp_command_country as c
						JOIN wp_command_schedule as s
						ON s.country_id = c.id
						WHERE s.team_id = $rsvp->team_id
						AND LOWER(c.title) = \"". $filter_args['value'] ."\"";
					if(!empty($wpdb->get_var($query)))
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "region":
				/** found in command_schedule (id) associated by rsvp team_id*/
				/** title in wp_command_regions */
				$filtered_rsvps = array();
				foreach($filter_args['rsvp'] as $rsvp)
				{
					$query="SELECT title FROM wp_command_regions as r
						JOIN wp_command_schedule as s
						ON s.region_id = r.id
						WHERE s.team_id = $rsvp->team_id
						AND LOWER(r.title) = \"". $filter_args['value'] ."\"";
					if(!empty($wpdb->get_var($query)))
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "program":
				/** found in command_schedule (id) associated by rsvp team_id*/
				/** title in wp_command_program */
				$filtered_rsvps = array();
				foreach($filter_args['rsvp'] as $rsvp)
				{
					$query="SELECT title FROM wp_command_program as p
						JOIN wp_command_schedule as s
						ON s.program_id = p.id
						WHERE s.team_id = $rsvp->team_id
						AND LOWER(p.title) = \"". $filter_args['value'] ."\"";
					if(!empty($wpdb->get_var($query)))
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "less than 1/3 paid":
				return $this->check_rsvp_payments($filter_args['rsvp'],0.333);
				break;

			case "less than 2/3 paid":
				return $this->check_rsvp_payments($filter_args['rsvp'],0.666);
				break;

			case "less than 100% paid":
				return $this->check_rsvp_payments($filter_args['rsvp'],1.0);
				break;

			case "unread team documents":
				$filtered_rsvps = array();
				foreach($filter_args['rsvp'] as $rsvp_id => $rsvp)
				{
					$team_docs = get_post_meta($rsvp->post_id,"team_docs",true);
					$user_docs = get_post_meta($rsvp->post_id,"read_docs_". $rsvp->user_id,true);
					$team_cnt = substr_count($team_docs,';');
					$user_cnt = count($user_docs);
					if($team_cnt > $user_cnt)
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "profile":
				$act_rsvp = new RSVP();
				$filtered_rsvps = array();

				foreach($filter_args['rsvp'] as $rsvp_id => $rsvp)
				{
					$act_rsvp->set_rsvp_id($rsvp->rsvp_id);
					$act_rsvp_data = $act_rsvp->fetch_rsvp();
					$required = 0;
					$found = 0;

					foreach(acf_get_fields(4849) as $index => $data)
					{		
						if(substr($data['name'],0,10) != 'Emergency')
						{
							if($data['required'] == true)
							{
								$required++;

								if(!empty(get_field($data['name'], $act_rsvp_data['post_id'])))
								{
									$found++;
								}
							}
						}
					}

					if($required == $found && !empty($filter_args['value']))
					{
						if($filter_args['value'] == 'complete')
						{
							$filtered_rsvps[] = $rsvp;
						}
					}
					else
					{
						if($filter_args['value'] == 'missing')
						{
							$filtered_rsvps[] = $rsvp;
						}
					}

				}
				return $filtered_rsvps;
				break;

			case "passport":
				$act_rsvp = new RSVP();
				$filtered_rsvps = array();
				
				foreach($filter_args['rsvp'] as $rsvp_id => $rsvp)
				{
					$act_rsvp->set_rsvp_id($rsvp->rsvp_id);
					$act_rsvp_data = $act_rsvp->fetch_rsvp();

					if($act_rsvp->has_passport($rsvp->rsvp_id) && !empty($filter_args['value']) && $filter_args['value'] == 'complete')
					{
						$filtered_rsvps[] = $rsvp;
					}
					if(!$act_rsvp->has_passport($rsvp->rsvp_id) && !empty($filter_args['value']) && $filter_args['value'] == 'missing')
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "flight":
				$act_rsvp = new RSVP();
				$filtered_rsvps = array();
				
				foreach($filter_args['rsvp'] as $rsvp_id => $rsvp)
				{
					$act_rsvp->set_rsvp_id($rsvp->rsvp_id);
					$act_rsvp_data = $act_rsvp->fetch_rsvp();

					if($act_rsvp->has_travel($rsvp->rsvp_id) && !empty($filter_args['value']) && $filter_args['value'] == 'complete')
					{
						$filtered_rsvps[] = $rsvp;
					}
					if(!$act_rsvp->has_travel($rsvp->rsvp_id) && !empty($filter_args['value']) && $filter_args['value'] == 'missing')
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "emergency contact":
				$act_rsvp = new RSVP();
				$filtered_rsvps = array();

				foreach($filter_args['rsvp'] as $rsvp_id => $rsvp)
				{
					$act_rsvp->set_rsvp_id($rsvp->rsvp_id);
					$act_rsvp_data = $act_rsvp->fetch_rsvp();
					$required = 0;
					$found = 0;

					foreach(acf_get_fields(21041) as $index => $data)
					{		
						if(strtolower(substr($data['name'],0,9)) == 'emergency')
						{
							if($data['required'] == true)
							{
								$required++;

								$fld_data = get_field($data['key'], $act_rsvp_data['post_id']);
								if(!empty($fld_data) && strlen($fld_data) >= 1)
								{
									$found++;
								}
							}
						}
					}

					if($required == $found && !empty($filter_args['value']))
					{
						if($filter_args['value'] == 'complete')
						{
							$filtered_rsvps[] = $rsvp;
						}
					}
					else
					{
						if($filter_args['value'] == 'missing')
						{
							$filtered_rsvps[] = $rsvp;
						}
					}

				}
				return $filtered_rsvps;
				break;

			case "team status is go":
				$filtered_rsvps = array();
				foreach($filter_args['rsvp'] as $rsvp)
				{
					$query="SELECT t.id FROM wp_command_TEAM as t
						WHERE t.team_status_id = 9 AND t.messaging = 1 AND
						t.id = $rsvp->team_id limit 1";
					if(!empty($wpdb->get_var($query)))
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "team status is closed":
				$filtered_rsvps = array();
				foreach($filter_args['rsvp'] as $rsvp)
				{
					$query="SELECT t.id FROM wp_command_TEAM as t
						WHERE t.team_status_id = 2 AND t.messaging = 1 AND 
						t.id = $rsvp->team_id limit 1";
					if(!empty($wpdb->get_var($query)))
					{
						$filtered_rsvps[] = $rsvp;
					}
				}
				return $filtered_rsvps;
				break;

			case "everyone":
				return $filter_args['rsvp'];
				break;
				
			default:
				break;
		}
	}
	public function execute_simple_filter($event_id, $start = true, $before = true)
	{
		$trigger_data = $this->get_trigger_data($event_id);

		$this->read($trigger_data->trigger_id);
		if($start && $before)
		{
			$rsvps = $this->get_rsvp_starting_before_interval($this->get_from_meta('interval'),$this->get_from_meta('unit'));
		}
		else if ($start && !$before)
		{
			$rsvps = $this->get_rsvp_starting_after_interval($this->get_from_meta('interval'),$this->get_from_meta('unit'));
		}
		else if (!$start && $before)
		{
			$rsvps = $this->get_rsvp_ending_before_interval($this->get_from_meta('interval'),$this->get_from_meta('unit'));
		}
		else if (!$start && !$before)
		{
			$rsvps = $this->get_rsvp_ending_after_interval($this->get_from_meta('interval'),$this->get_from_meta('unit'));
		}
//		print_r($rsvps);die;

		$filter_array = array('rsvp' => $rsvps);
		if(isset($this->meta_data['filter_value']))
		{
			$filter_array['value'] = strtolower($this->meta_data['filter_value']);
		}
		
		$parsed_rsvps = $this->parse_filter($this->get_from_meta('filter'),$filter_array);

		if(array_key_exists('action',$this->meta_data)){
			/** execute the action */
			$action = $this->get_from_meta('action');

			if(!empty($parsed_rsps))
			{
				foreach($parsed_rsvps as $rsvp)
				{
			
					switch($action)
					{
						case "Close Team":
							$team = new TEAM();
							$team->set_team($rsvp->team_id);
							$team->update("status",2);
							break;

						case "Mark Team Completed":
							$team = new TEAM();
							$team->set_team($rsvp->team_id);
							$team->update("status",3);
							break;

						default:
							break;
					}
				}
			}
		}
		else
		{
			global $wpdb;
			$message = new MESSAGES();
			$admin_message = new ADMIN_MESSAGES();
			//
			//2016-04-16 DJT added team class for finance check
			$team = new TEAM();
			
			$admin_message->set_event($trigger_data->trigger_id);
			$admin_message->load();

			if(!empty($parsed_rsvps))
			{
				foreach($parsed_rsvps as $rsvp)
				{
					//
					//2018-04-16 settign the team to check for team hidden admin messages
					$team->set_team($rsvp->team_id);
					
					//
					//Added clause to skip message
					if($team->value('no_finances') == 1 && $admin_message->get_finance() == 1)
					{
						//
						//Don't do anything since it's no finance and a finance message
						//
					}
					else
					{
						
						$message->set_message_text($admin_message->get_message());
						$message->set_sender(1);
						$message->set_recipient($rsvp->user_id);
						$message->set_rsvp_id($rsvp->rsvp_id);
						$message->set_team_id($rsvp->team_id);
						$message->set_subject($admin_message->get_subject());
						$message->add_message($trigger_data->trigger_id);

						//
						//Send VEC a copy of the message
						if($admin_message->get_notify() == 1)
						{
							$meta_team_staff_key = 'cmd_team' . $rsvp->team_id . '_staff_pos';

							$query = "SELECT um.user_id, um.meta_value, u.display_name, u.user_email
										FROM " . $wpdb->prefix . "usermeta um INNER JOIN
											 " . $wpdb->prefix . "users u on u.ID = um.user_id 
									  WHERE meta_key = '" . $meta_team_staff_key . "' AND
											meta_value = 'VEC'
									  ORDER BY display_name";
							$possibles = $wpdb->get_results($query, ARRAY_A);

							foreach($possibles as $data)
							{
								$e_mail = new EMAIL();
								$e_mail->from( 'TC NOTIFY', 'donotreply@islonline.org' );
								$e_mail->to( $data['display_name'], $data['user_email'] );
								$e_mail->subject('TC NOTICE: ' . $admin_message->get_subject());
								$e_mail->message('<html>' . strip_tags(stripslashes(html_entity_decode($message->get_message_text()))) . '</html>');
								$e_mail->send();
							}
						}
					}
				}
			}
		}
	}

	public function check_exists($trigger_id)
	{
		global $wpdb;
		$query="SELECT id FROM wp_command_trigger WHERE id= $trigger_id";
		if(empty($wpdb->get_var($query)))
		{
			return false;
		}
		return true;
	}

	/**
	 * Runs team status changed action
	 *
	 * @param integer $event_id: ID of the trigger event being fired
	 *
	 */
	private function team_status_changed($event_id)
	{
		$trigger_data = $this->get_trigger_data($event_id);

		$message = new MESSAGES();
		$admin_message = new ADMIN_MESSAGES();
		
		$admin_message->set_event($trigger_data->trigger_id);
		$admin_message->load();

		$team_info = unserialize($trigger_data->trigger_obj);

		$team_obj = new TEAM();
		$team_obj->set_team($team_info['team_id']);
		
		/** If POC is empty, use admin account, else use team POC */
		$team_poc = empty($team_obj->value('poc'))? 1 : $team_obj->value('poc');

		$message->set_message_text($admin_message->get_message());
		$message->set_sender($team_poc);
		$message->set_team_id($team_obj->get_team());
		$message->set_subject($admin_message->get_subject());
		$message->add_team_message($team_obj->get_team(),$trigger_data->trigger_id);
	}

	private function team_document_changed($event_id)
	{
		$trigger_data = $this->get_trigger_data($event_id);
		$trigger_obj = unserialize($trigger_data->trigger_obj);
		$doc_id = $trigger_data->doc_id;

		$admin_message = new ADMIN_MESSAGES();
		$admin_message->set_event($trigger_data->trigger_id);
		$admin_message->load();

		global $wpdb;
		$query_data = $wpdb->get_results(
			"SELECT p.meta_key AS meta_key,t.id AS team_id, t.post_id AS team_post_id
			FROM wp_command_TEAM AS t
			JOIN wp_postmeta AS p
			ON p.post_id = t.post_id
			WHERE t.team_status_id in(2,4,9)
			AND t.messaging = 1
			AND p.meta_key like '%read_docs_%'
			AND p.meta_value like '%$doc_id%'");

		if(!empty($query_data))
		{
			foreach($query_data as $item)
			{
				$user_id = substr($item->meta_key,10);
				$docs = new DOCS($user_id);
				$docs->post_id($item->team_post_id);
				$docs->fetch();
				$docs->remove_read_document($doc_id);

				$team = new TEAM();
				$team->set_team($item->team_id);
				$message = new MESSAGES();
				$message->set_message_text($admin_message->get_message());
				$message->set_subject($admin_message->get_subject());
				$message->set_recipient($user_id);
				$message->set_team_id($item->team_id);
				$message->set_sender(1);
				$message->add_message($trigger_data->trigger_id);
			}
		}
	}

	private function team_document_added($event_id)
	{
		$trigger_data = $this->get_trigger_data($event_id);
		$trigger_obj = unserialize($trigger_data->trigger_obj);
		$team_id = $trigger_obj['team_id'];

		$admin_message = new ADMIN_MESSAGES();
		$admin_message->set_event($trigger_data->trigger_id);
		$admin_message->load();

		$message = new MESSAGES();
		$message->set_message_text($admin_message->get_message());
		$message->set_subject($admin_message->get_subject());
		$message->set_team_id($team_id);
		$message->set_sender(1);
		$message->add_team_message($team_id,$trigger_data->trigger_id);
	}

	private function personal_message_sent($event_id)
	{
		$trigger_data = $this->get_trigger_data($event_id);

		$message = new MESSAGES();
		$admin_message = new ADMIN_MESSAGES();
		
		$admin_message->set_event($trigger_data->trigger_id);
		$admin_message->load();

		$trigger_obj = unserialize($trigger_data->trigger_obj);
		$message->set_message_text($admin_message->get_message());
		$message->set_sender(1);
		$message->set_recipient($trigger_obj['recipient']);
		$message->set_team_id($trigger_obj['team_id']);
		$message->set_subject($admin_message->get_subject());
		$message->add_message($trigger_data->trigger_id);
	}

	private function team_message_sent($event_id)
	{
		$trigger_data = $this->get_trigger_data($event_id);

		$message = new MESSAGES();
		$admin_message = new ADMIN_MESSAGES();
		
		$admin_message->set_event($trigger_data->trigger_id);
		$admin_message->load();

		$team_obj = new TEAM();
		$team_obj_id = unserialize($trigger_data->trigger_obj);
		$team_obj->set_team($team_obj_id['team_id']);

		/** If POC is empty, use admin account, else use team POC */
		$team_poc = empty($team_obj->value('poc'))? 1 : $team_obj->value('poc');

		$message->set_message_text($admin_message->get_message());
		$message->set_sender($team_poc);
		$message->set_team_id($team_obj->get_team());
		$message->set_subject($admin_message->get_subject());
		$message->add_team_message($team_obj->get_team(),$trigger_data->trigger_id);
	}

	private function new_sign_up($event_id)
	{
		$trigger_data = $this->get_trigger_data($event_id);

		$message = new MESSAGES();
		$admin_message = new ADMIN_MESSAGES();
		
		$admin_message->set_event($trigger_data->trigger_id);
		$admin_message->load();

		$trigger_obj = unserialize($trigger_data->trigger_obj);
		$message->set_message_text($admin_message->get_message());
		$message->set_sender(1);
		$message->set_recipient($trigger_obj['user_id']);
		$message->set_team_id($trigger_obj['team_id']);
		$message->set_subject($admin_message->get_subject());
		$message->add_message($trigger_data->trigger_id);
	}

	private function before_trip_departure($event_id)
	{
		$this->execute_simple_filter($event_id);
	}

	private function after_trip_departure($event_id)
	{
		$this->execute_simple_filter($event_id,true,false);
	}

	private function before_trip_return($event_id)
	{
		$this->execute_simple_filter($event_id,false,true);
	}

	private function after_trip_return($event_id)
	{
		$this->execute_simple_filter($event_id,false,false);
	}
}