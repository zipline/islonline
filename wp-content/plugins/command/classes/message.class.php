<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');


require_once (CC_CLASSES_DIR . "email.class.php");
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "program.class.php");
require_once (CC_CLASSES_DIR . "country.class.php");
require_once (CC_CLASSES_DIR . "acf.link.class.php");

/**
 * TEAM_NOTE class
 * Functionality for team notes
 */
CLASS MESSAGES
{
	
	/** @var string $team_note_table: Database table for notes */
	private $messages_table;

	/** @var integer $team_id: ID of the team */
	private $team_id;

	/** @var integer $sender: ID of the message sender */
	private $sender;

	/** @var integer $recipient: ID of the message recipient */
	private $recipient;

	/** @var string $message: Message text for the message */
	private $message;

	/** @var boolean $is_read: true if message has been read, false if not */
	private $is_read;

	/** @var string $subject: Subject for the message */
	private $subject;

	/** @var string $rsvp_id: id of the RSVP for the person receiving the message. */
	private $rsvp_id;

	/**
	 * Default Constructor 
	 * Sets the DB tables and initializes statuses and types
	 */
	public function __construct()
	{
		global $wpdb;

		$this->messages_table = $wpdb->prefix . 'command_message';
	}

	public function set_sender($sender)
	{
		$this->sender = $sender;
	}

	public function get_sender()
	{
		return $this->sender;
	}

	public function set_recipient($recipient)
	{
		$this->recipient = $recipient;
	}

	public function set_subject($subject)
	{
		$this->subject = htmlspecialchars($subject);
	}

	public function get_subject()
	{
		return $this->subject;
	}

	public function get_recipient()
	{
		return $this->recipient;
	}

	public function set_message_text($message)
	{
		$this->message = htmlspecialchars($message);
	}

	public function get_message_text()
	{
		return $this->message;
	}

	public function set_team_id($team_id)
	{
		$this->team_id = $team_id;
	}

	public function get_team_id()
	{
		return $this->team_id;
	}

	public function set_rsvp_id($rsvp_id)
	{
		$this->rsvp_id = $rsvp_id;
	}

	public function get_rsvp_id()
	{
		return $this->rsvp_id;
	}

	public function set_is_read($is_read)
	{
		if($is_read != false)
		{
			$this->is_read = true;
		}
		else
		{
			$this->is_read = false;
		}
	}

	public function get_is_read()
	{
		return $this->is_read;
	}

	function clean_messages($data = array())
	{
		
		if(func_num_args() > 0)
		{
			foreach($data as $id => $elements)
			{
				if(isset($elements->msg_text))
				{
					$data[$id]->msg_text = html_entity_decode(htmlspecialchars_decode($elements->msg_text));
				}

				if(isset($elements->subject))
				{
					$data[$id]->subject = html_entity_decode(htmlspecialchars_decode($elements->subject));
				}
				
			}
		}
		
		return $data;
	}
	/**
	 * Gets all messages sent by a user
	 *
	 * @param $user_id: ID of the sender
	 * @param $team_id: ID of the team
	 *
	 * @return array: results of the query for getting sender messages
	 */
	public function get_sender_messages($user_id, $team_id = null,$hidden = true)
	{
		global $wpdb;

		$query = "SELECT * FROM $this->messages_table
			WHERE sender = $user_id";

		if(!empty($team_id))
		{
			$query .= " AND team_id = $team_id";
		}

		if($hidden)
		{
			$query .= " AND hide_at IS NULL";
		}
		
		$query .= " ORDER BY created_at DESC";
		
		$result = $wpdb->get_results($query);
		return self::clean_messages($result);
	}

	/**
	 * Gets all messages received by a user
	 *
	 * @param $user_id: ID of the recipient
	 * @param $team_id: ID of the team
	 *
	 * @return array: results of the query for getting recipient messages
	 */
	public function get_recipient_messages($user_id, $team_id = null,$hide = true,$read = false)
	{
		global $wpdb;

		$query = "SELECT * FROM $this->messages_table
			WHERE recipient = $user_id";

		if(!empty($team_id))
		{
			$query .= " AND team = $team_id";
		}
		
		if($hide)
		{
			$query .= " AND hide_at IS NULL";
		}

		if($read)
		{
			$query .= " AND read_at IS NULL";
		}
		
		$query .= " ORDER BY created_at DESC";

		$result = $wpdb->get_results($query);
		return self::clean_messages($result);
	}

	/**
	 * Gets all messages for a team
	 *
	 * @param $team_id: ID of the team
	 *
	 * @return array: results of the query for getting team messages
	 */
	public function get_team_messages($team_id)
	{
		global $wpdb;

		$query = "SELECT * FROM $this->messages_table
			WHERE team = $team_id ORDER BY created_at DESC";
		
		$result = $wpdb->get_results($query);
		
		//echo var_dump($result);
		return self::clean_messages($result);
	}

	/**
	 * Add a message to the team notes
	 *
	 */
	public function add_message($event = null)
	{
		global $wpdb;
		$this->replace_text();
		
		if(!empty($this->recipient))
		{
			$wpdb->insert(
				$this->messages_table,
				array(
					'sender' => $this->sender,
					'recipient' => $this->recipient,
					'team' => $this->team_id,
					'subject' => $this->subject,
					'msg_text' => $this->message,
				),
				array(
					'%d','%d','%d','%s','%s',
				)
			);
		}
		
		if(!empty($event))
		{
			$this->email_notification($event);
		}
	}

	/**
	 * Marks the message as read in the database
	 *
	 * @param integer $msg_id: ID of the message
	 *
	 */
	public function read_msg($msg_id)
	{
		global $wpdb;
		date_default_timezone_set('America/Los_Angeles');		
		
		$wpdb->update(
					$this->messages_table,
					array('read_at' => date('Y-m-d H:i:s')),
					array('id' => $msg_id)
		);
	}

	/**
	 * Marks the message as hid in the database
	 *
	 * @param integer $msg_id: ID of the message
	 *
	 */
	public function hide_msg($msg_id)
	{
		global $wpdb;
		date_default_timezone_set('America/Los_Angeles');		
		
		$wpdb->update(
			$this->messages_table,
			array(
				'hide_at' => date('Y-m-d H:i:s'),
			),
			array(
				'id' => $msg_id,
			),
			array(
				'%s'
			),
			array(
				'%d'
			)
		);
	}

	public function add_team_message($team_id,$event = null)
	{
		global $wpdb;

		$base_message = $this->message;
		$base_subject = $this->subject;
		
		//get all users on a team
		//
		$query = "select distinct(r.user_id), r.id
			FROM wp_command_RSVP r inner join wp_command_order_items oi on r.order_item_id = oi.id 
			WHERE r.team_id = $team_id and oi.quantity > 0 AND r.user_id > 0";
		$team_members = $wpdb->get_results($query);

		foreach ($team_members as $member)
		{
			self::set_message_text($base_message);
			self::set_subject($base_subject);
			
			$this->set_recipient($member->user_id);
			$this->rsvp_id = $member->id;
			$this->add_message($event);
		}

	}

	public function email_notification($event)
	{
		$recipient = get_userdata($this->recipient);
		/** add SMS magic in here */

		if(!empty($recipient))
		{
			$email = new EMAIL();
			$email->email_notification($event,$recipient,$this->subject);
		}
	}


	/**
	 * Replaces text in the Subject and Message text 
	 *
	 */
	//
	//2018-03-16 DJT Changed from private to public to allow for testing.
	public function replace_text()
	{
		/** First Name (usermeta), Last Name(usermeta), Display Name(users display_name), Address(usermeta mailing_address_line_1, mailing_address_line_2), Phone(primary_phone), Email(users, user_email)
		 * Program, Country, Departure Date, Return Date
		 * VEC
		 * Team Leader
		 */
		date_default_timezone_set('America/Los_Angeles');
		
		$team = new TEAM();
		$team->set_team($this->team_id);

		$program = new PROGRAM();
		$country = new COUNTRY();
		$adv_custom_flds = new ACF_LINK();

		/** Get User Data */
		global $wpdb;
		$user_query = "SELECT 
							u.display_name as 'Display Name', u.user_email as 'Email', fn.meta_value as 'First Name',
							ln.meta_value as 'Last Name', IFNULL(phn.meta_value, 'No Phone on File') as 'Phone',
							IF(CONCAT(IFNULL(add1.meta_value,''),', ',IFNULL(add2.meta_value,''), ', ',IFNULL(city.meta_value,''),', ',IFNULL(state.meta_value,''),' ',IFNULL(zip.meta_value,'')) = ', , ,', 'No Address on File', CONCAT(IFNULL(add1.meta_value,''),', ',IFNULL(add2.meta_value,''), ', ',IFNULL(city.meta_value,''),', ',IFNULL(state.meta_value,''),' ',IFNULL(zip.meta_value,''))) as 'Address' 
					FROM " . $wpdb->prefix . "users as u 
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'first_name') fn on u.id = fn.user_id
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'last_name') ln on u.id = ln.user_id
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'mailing_address_line_1') add1 on u.id = add1.user_id
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'mailing_address_line_2') add2 on u.id = add2.user_id
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'city') city on u.id = city.user_id
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'state') state on u.id = state.user_id
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'zip_code') zip on u.id = zip.user_id
						left join 
							(SELECT user_id, meta_value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $this->recipient . " and meta_key = 'primary_phone') phn on u.id = phn.user_id

					WHERE
						u.id = " . $this->recipient;

		$user_data = $wpdb->get_row($user_query);

		if(!empty($user_data))
		{
			$user_data = get_object_vars($user_data);

			$content = $adv_custom_flds->get_fields(4860);
			foreach($content as $fld_id => $fld_data)
			{
				if($fld_data['title'] == "State")
				{
					$unsd_data = unserialize($fld_data['content']);
					foreach($unsd_data['choices'] as $key => $state)
					{
						if(stristr($user_data['Address'], ", ".$key." "))
						{
							$user_data['Address'] = substr_replace($user_data['Address'],$state, strpos($user_data['Address'], ', '.$key.' ')+2,2);
						}
					}
				}
			}

			$team_data = array();

			/** get team data */
			$query = "SELECT arrival_city as team_arrival_city, departure_city as team_departure_city,
                      arrival_country as team_arrival_country, departure_country as team_departure_country
				FROM ". $wpdb->prefix ."command_TEAM WHERE id= $this->team_id LIMIT 1";
			$cities = $wpdb->get_row($query,ARRAY_A);
			$team_data = array_merge($team_data,$cities);
			/** get VEC and Team Manager */
			$vec = "";
			$tm = "";
			$query = "SELECT um.user_id, um.meta_value, u.display_name, phone.meta_value as trip_coordinator_phone, u.user_email as trip_coordinator_email
				FROM ". $wpdb->prefix ."usermeta um
				inner join ". $wpdb->prefix ."users u on u.ID=um.user_id
				left join ". $wpdb->prefix ."usermeta phone on u.ID=phone.user_id
				WHERE um.meta_key = \"cmd_team". $this->team_id ."_staff_pos\"
				AND phone.meta_key = \"primary_phone\"
				ORDER BY display_name";
			$possibilities= $wpdb->get_results($query,ARRAY_A);
			foreach($possibilities as $data)
			{
				$staff[$data['meta_value']][$data['user_id']]=array(
					'name' => $data['display_name'],
					'email' => $data['trip_coordinator_email'],
					'phone' => $data['trip_coordinator_phone']);
			}


			if(!empty($staff['VEC']))
			{
				foreach($staff['VEC'] as $id =>$vec_data);
				{
					$vec = $vec_data['name'];
					$trip_coordinator = $vec_data['name'];
					$trip_coordinator_email = $vec_data['email'];
					$trip_coordinator_phone = $vec_data['phone'];

				}
			}
			
			/** finance stuff */
			if(!empty(self::get_rsvp_id()))
			{
				$rsvp = new RSVP();
				$rsvp->set_rsvp_id(self::get_rsvp_id());
				$reservation = $rsvp->fetch_rsvp();

				$finance = new FINANCE();
				$finance->Fetch_Items(array($reservation['order_item_id']), true);
				
				if(!empty($reservation['order_id']))
				{
					$user_data['Balance Due'] = '$' . number_format($finance->Calculate_ItemsBalance(),2);
					$user_data['Cost'] = '$' . number_format($finance->Calculate_ItemsCost(),2);
				}
				else
				{
					$user_data['Balance Due'] = "Unknown";
					$user_data['Cost'] = 'Unknown';
				}
			}

			if(!empty($staff['Team Manager']))
			{
				foreach($staff['Team Manager'] as $id =>$name);
				{
					$tm = $name ."<BR>";
				}
			}
			
			if(!isset($user_data['Cost']))
			{
				$team_data['Cost']=number_format(($team->trip_cost()),2);
			}
		
			$team_data['vec'] = $vec;
			$team_data['trip coordinator'] = $vec;

			$team_data['trip_coordinator_email'] = isset($trip_coordinator_email) ? $trip_coordinator_email : null;
			$team_data['trip_coordinator_phone'] = isset($trip_coordinator_phone) ? $trip_coordinator_phone : null;
			$team_data['team leader'] = $tm;

			/** Get Program, Country, Start, End */
			$progs = '';
			$cntry = '';
			if($team->get_schedule($team->value('id')) != "Unknown")
			{
				foreach($team->get_schedule($team->value('id')) as $sched_id=>$sched_data)
				{
					$start_date = '';
					$end_date = '';
					$docs_date = '';

					if(isset($sched_data['program']))
					{
						$program->set_program($sched_data['program']);
						$progs .= $program->get_name() . ", ";
					}
					if(strlen($progs)>0)
					{
						$progs = substr($progs, 0, -2);
					}

					if(isset($sched_data['country']))
					{
						$country->set_country($sched_data['country']);
						$cntry .= $country->get_name() . ", ";
					}
					if(strlen($cntry)>0)
					{
						$cntry = substr($cntry, 0, -2);
					}

					if($start_date == '')
					{
						$start_date = $sched_data['start_date'];
					}
					$end_date = $sched_data['end_date'];

					$end = date("m-d-Y", mktime(0,0,0,substr($end_date,0,2),substr($end_date,3,2),substr($end_date,6,4)));
					$start = date("m-d-Y", mktime(0,0,0,substr($start_date,0,2),substr($start_date,3,2),substr($start_date,6,4)));

					$options = get_option('cmd_center');
					$doc_interval = (!empty($options['docs_due_limit']) ? $options['docs_due_limit'] : 14);
					$docs_due = date("m-d-Y", mktime(0,0,0,substr($start_date,0,2),substr($start_date,3,2) - $doc_interval,substr($start_date,6,4)));
				}
			}
			else
			{
				$docs_due = "Unset";
				$start = "Unset";
				$end = "Unset";
			}
			$team_data['departure date'] = $start;
			$team_data['return date'] = $end;
			$team_data['country'] = $cntry;
			$team_data['program'] = $progs;
			$team_data['date'] = date("m/d/Y");
			$team_data['time'] = date("H:i A");
			$team_data['team'] = $team->value('hash');
			$team_data['myisl'] = '<a href="' . site_url('/my-isl/') . '">MyISL</a>'; 

			$sub_txt = $this->subject;
			$msg_txt = $this->message;

			/** User Data Replace */
			foreach($user_data as $u_key => $string)
			{
				$rep_txt = strtolower("{{".$u_key."}}");
				while(stristr($sub_txt,$rep_txt))
				{
					$sub_txt = substr_replace($sub_txt,$string,stripos($sub_txt,$rep_txt),strlen($rep_txt));
				}
				while(stristr($msg_txt,$rep_txt))
				{
					$msg_txt = substr_replace($msg_txt,$string,stripos($msg_txt,$rep_txt),strlen($rep_txt));
				}
			}

			/** Team Data Replace */
			foreach($team_data as $u_key => $string)
			{
				$rep_txt = strtolower("{{".$u_key."}}");
				
				while(stristr($sub_txt,$rep_txt))
				{
					$sub_txt = substr_replace($sub_txt,$string,stripos($sub_txt,$rep_txt),strlen($rep_txt));
				}

				while(stristr($msg_txt,$rep_txt))
				{
					$msg_txt = substr_replace($msg_txt,$string,stripos($msg_txt,$rep_txt),strlen($rep_txt));
				}
			}

			$msg_txt = preg_replace('/\{\{.+?\}\}/', ' (uh-oh) ', $msg_txt);
			
			$this->set_subject($sub_txt);
			$this->set_message_text($msg_txt);
			
			return $msg_txt;
		}
	}
}