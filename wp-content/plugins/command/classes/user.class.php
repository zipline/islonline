<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * USER class
 * Functionality for user objects
 */
CLASS USER
{
	/** @var integer $group ID of the group. */
	private $group = NULL;

	/** @var array $groups List of group ids. */
	private $groups = NULL;

	/** @var array $colleges List of college id and names. */
	private $colleges = NULL;

	/** @var integer $group_count Count of the number of groups for this user*/
	private $group_count = NULL;

	/** @var string $cap_group_table Database table for cap groups. */
	private $cap_group_table = NULL;

	/** @var array $caps List of capabilities */
	private $caps = NULL;

	/** @var array $cap_translate  List of capability translations */
	private $cap_translate = array();

	/** @var string $cap_table Database table for capabilities */
	private $cap_table = NULL;

	/** @var integer $user ID of the user */
	private $user = NULL;

	private $total_users = 0;
	
	/**
	 * Default Constructor
	 * Sets the current user id, builds the database table names and builds the groups and capabilities
	 */
	public function __construct()
	{
		global $wpdb;
		
		$current_user = wp_get_current_user();
		$this->user = $current_user->ID;

		$this->cap_group_table = $wpdb->prefix . "command_cap_groups";
		$this->cap_table = $wpdb->prefix . "command_caps";

		self::build_groups();
		self::build_caps();

	}

	public function Count_users()
	{
		return $this->total_users;
	}
	
	/**
	 * User Creation and Logging
	 *
	 * @param array $user_data Array of user data to create a user with
	 *
	 * @return integer Returns the user id for the user created
	 */
	public function create_user($user_data = array())
	{
		$this->user = wp_insert_user($user_data);
		return $this->user;
	}

	/**
	 * Rep Code
	 *
	 * @param integer $user_id
	 *
	 * @return string Rep Code
	 */
	public function rep_code($user_id = 0)
	{
		if(!empty($user_id))
		{
			if(get_user_meta($user_id, '_cmd_comm_ambassador_status', true) == 'true')
			{
				return get_user_meta($user_id, '_cmd_comm_ambassador_cap_code', true);
			}
		}

		return false;
	}

	/**
	 * Rep Code
	 *
	 * @param integer $user_id
	 *
	 * @return string Rep Code
	 */
	public function avatar($user_id = 0)
	{
		if($user_id)
		{
			$avi = get_user_meta($user_id,'local_avatar',true);
			if(!$avi)
			{
				$avi = 'https://www.islonline.org/wp-content/uploads/2016/03/avatar.png';		
			}
		}
		else
		{
			$avi = 'https://www.islonline.org/wp-content/uploads/2016/03/avatar.png';		
		}

		return $avi;
	}

	/**
	 * Rep Code
	 *
	 * @param integer $user_id
	 *
	 * @return string Rep Code
	 */
	public function banner($user_id = 0)
	{
		$user_banner = get_user_meta($user_id, 'banner', true);
		if(!$user_banner)
		{
			$user_banner = 'https://www.islonline.org/wp-content/uploads/2015/11/jungle-hero-1.jpg';
		}
		return $user_banner;
	}

	/**
	 * User By Rep Code
	 *
	 * @param string $rep_code
	 *
	 * @return integer $user_id
	 */
	public function user_by_rep_code($rep_code = '')
	{
		if(!empty($rep_code))
		{
			global $wpdb;
			
			$user = $wpdb->get_var("SELECT user_id 
									FROM " . $wpdb->prefix . "usermeta 
									WHERE meta_key = '_cmd_comm_ambassador_cap_code' AND 
										  meta_value = '" . $rep_code . "'");
			
			if(self::rep_code($user) == $rep_code)
			{
				$edate = date_create_from_format("m-d-Y", get_user_meta($user, '_cmd_comm_ambassador_edate', true));
				//echo var_dump($edate);
				$edate = strtotime($edate->format('Y-m-d h:i:s'));
				
				if(time() < $edate)
				{
					return $user;
				}
			}
		}
		return 0;
	}

	/**
	 * creates a temp user name based on the number of users in the db and returns it
	 * 
	 * @return string The temporary user name
	 */
	public function temp_user_name()
	{
		global $wpdb;
		
		$good = false;
		$index = $wpdb->get_var("SELECT Count(*) FROM $wpdb->users");
			
		while(!$good)
		{
			$name_to_try = 'User_' . $index;
			
			if(username_exists($name_to_try))
			{
				$index += 21;	
			}
			else
			{
				$good = true;
			}
		}
		
		return $name_to_try;
		
	}

	public function user_list($start_rec = 1, $limit = 100, $filters)
	{
		global $wpdb;
		$users = array();
		
		//
		//Basic query
		$query = "SELECT ID, user_login, user_registered, user_email FROM " . $wpdb->prefix . "users";
		
		//
		//Check if Enhanced permissions
		$usr = wp_get_current_user();
		if(self::user_group($usr->ID) == "enhanced")
		{
			$filters['poc'] = $usr->ID;
		}
	
		//
		//Find user id's if there's a filter
		if(!empty($filters))
		{
			$possible_users = self::users_ids($filters);	

			$query .=" WHERE ID in (" . implode(",", $possible_users) . ")";
		}

		//
		//Finalize query
		$query .= " ORDER BY ID";
		$total_users_query = "SELECT count(*) FROM (" . $query . ") cnt_tbl";
		$query .= " LIMIT " . $start_rec . ", " . $limit;

		//
		//Gather basic information and the user id's to other grabs...
		$possibles = $wpdb->get_results($query, ARRAY_A);
		$this->total_users = $wpdb->get_var($total_users_query);
		
		
		foreach($possibles as $data)
		{
			$users[$data['ID']] = array('email' => $data['user_email'],
										'login' => $data['user_login'],
										'registered' => $data['user_registered']);
		}

		if(!empty($users) && count($users) > 0)
		{
			//
			//Gather meta data...
			$query = "SELECT user_id, meta_key, meta_value
					  FROM " . $wpdb->prefix . "usermeta	
					  WHERE meta_key in ('first_name', 'last_name')
					  	AND user_id in (" . implode(",", array_keys($users)) . ")";
			$possibles = $wpdb->get_results($query, ARRAY_A);

			foreach($possibles as $data)
			{
				$users[$data['user_id']][$data['meta_key']] = $data['meta_value'];
			}

			//
			//Gather current Team
			return $users;
			exit;			
		}
		
		return array();
	}
	
	public function users_ids($filters)
	{
		//
		//2018-02-19 DJT changed all instances of $record['user_id'] to $record['ID'] where necessary
		//2018-02-19 DJT changed name of join and team start dates to reflect changes to user form
		//2018-02-20 DJT changed joinbefore and joinafter to reflect RSVP date, not registration date
	
		//
		//2018-03-11 DJT Aded ability to do split name search
		$split_name = false;
		if(!empty($filters['first_name']))
		{
			$tmp_name = trim($filters['first_name']);
			
			if(strlen($tmp_name) >= 3)
			{
				
				if(strpos($tmp_name, ' ') > 0)
				{
					list($filters['first_name'], $filters['last_name']) = explode(' ', $tmp_name);
					$split_name = true;
				}
			}
		}
		
		global $wpdb;

		$clauses = array();
		$possibles = array();
		$checks = 0;

		if(!empty($filters['ambassador']))
		{
			$list = array();
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "usermeta 
					  WHERE meta_key='_cmd_comm_ambassador_status' 
					  	AND meta_value='false'";
			$potentials = $wpdb->get_results($query, ARRAY_A);

			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['first_name']))
		{
			$list = array();
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "usermeta 
					  WHERE meta_key='first_name' 
					  	AND meta_value LIKE '%" . $filters['first_name'] . "%'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			//
			//2018-03-11 DJT Added split_name check delimiter so that if names werwe split, then terms are AND, not OR
			//if(empty($filters['last_name']))
			if(empty($filters['last_name']) || $split_name)
			{
				self::user_possible_check($possibles, $list, $checks);
			}
			else
			{
				$possibles = array_merge($possibles, $list);	
			}
		}

		if(!empty($filters['last_name']))
		{
			$list = array();
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "usermeta 
					  WHERE meta_key='last_name' 
					  	AND meta_value LIKE '%" . $filters['last_name'] . "%'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			//
			//2018-03-11 DJT Added split_name check delimiter so that if names werwe split, then terms are AND, not OR
			//if(empty($filters['first_name']))
			if(empty($filters['first_name']) || $split_name)
			{
				self::user_possible_check($possibles, $list, $checks);
			}
			else
			{
				$possibles = array_merge($possibles, $list);	
			}
		}

		if(!empty($filters['user_id']))
		{
			$list = array();
			$query = "SELECT ID
					  FROM " . $wpdb->prefix . "users 
					  WHERE ID = " . $filters['user_id'];
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				array_push($list, $record['ID']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['user_email']))
		{
			$list = array();
			$query = "SELECT ID 
					  FROM " . $wpdb->prefix . "users 
					  WHERE user_email = '" . $filters['user_email'] . "'";
			$potentials = $wpdb->get_results($query, ARRAY_A);

			foreach($potentials as $record)
			{
				array_push($list, $record['ID']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		//
		//2018-02-20 DJT Updated to reflect RSVP origination date instead of user registration date
		if(!empty($filters['joindateafter']))
		{
			$list = array();
			/*
			$query = "SELECT ID 
					  FROM " . $wpdb->prefix . "user 
					  WHERE DATE_FORMAT(user_registered, '%Y-%m-%d') >= '" . date_format(date_create_from_format('m-d-Y', $filters['joindateafter']), 'Y-m-d') . "'";
			*/
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "command_RSVP 
					  WHERE DATE_FORMAT(created, '%Y-%m-%d') >= '" . date_format(date_create_from_format('m-d-Y', $filters['joindateafter']), 'Y-m-d') . "'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				//array_push($list, $record['ID']);
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		//
		//2018-02-20 DJT Updated to reflect RSVP origination date instead of user registration date
		if(!empty($filters['joindatebefore']))
		{
			$list = array();
			/*
			$query = "SELECT ID 
					  FROM " . $wpdb->prefix . "user 
					  WHERE DATE_FORMAT(user_registered, '%Y-%m-%d') <= '" . date_format(date_create_from_format('m-d-Y', $filters['joindateafter']), 'Y-m-d') . "'";
			*/
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "command_RSVP 
					  WHERE DATE_FORMAT(created, '%Y-%m-%d') <= '" . date_format(date_create_from_format('m-d-Y', $filters['joindatebefore']), 'Y-m-d') . "'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				//array_push($list, $record['ID']);
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['country']))
		{
			$list = array();
			$query = "SELECT r.user_id 
					  FROM " . $wpdb->prefix . "command_RSVP r INNER JOIN
					  	" . $wpdb->prefix . "command_schedule s on r.team_id = s.team_id
					  WHERE s.country_id = " . $filters['country'];
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['program']))
		{
			$list = array();
			$query = "SELECT r.user_id 
					  FROM " . $wpdb->prefix . "command_RSVP r INNER JOIN
					  	" . $wpdb->prefix . "command_schedule s on r.team_id = s.team_id
					  WHERE s.program_id = " . $filters['program'];
			$potentials = $wpdb->get_results($query, ARRAY_A);

			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['teamstartafter']))
		{
			$list = array();
			$query = "SELECT DISTINCT r.user_id 
					  FROM " . $wpdb->prefix . "command_RSVP r INNER JOIN
					  	" . $wpdb->prefix . "command_schedule s on r.team_id = s.team_id
					  WHERE DATE_FORMAT(s.start_date, '%Y-%m-%d') >= '" . date_format(date_create_from_format('m-d-Y', $filters['teamstartafter']), 'Y-m-d') . "'";
			$potentials = $wpdb->get_results($query, ARRAY_A);

			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}
		
		if(!empty($filters['teamstartbefore']))
		{
			$list = array();
			$query = "SELECT DISTINCT r.user_id 
					  FROM " . $wpdb->prefix . "command_RSVP r INNER JOIN
					  	" . $wpdb->prefix . "command_schedule s on r.team_id = s.team_id
					  WHERE DATE_FORMAT(s.start_date, '%Y-%m-%d') <= '" . date_format(date_create_from_format('m-d-Y', $filters['teamstartbefore']), 'Y-m-d')  . "'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}
		
		if(!empty($filters['phone']))
		{
			$list = array();
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "usermeta
					  WHERE meta_key = 'primary_phone' AND meta_value LIKE '%" . $filters['phone'] . "%'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['address']))
		{
			$list = array();
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "usermeta
					  WHERE meta_key = 'mailing_address_line_1' AND meta_value LIKE '%" . $filters['address'] . "%'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
		
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['school']))
		{
			$list = array();
			
			//$school = $wpdb->get_var('SELECT INSTNM FROM ' . $wpdb->prefix . 'command_schools WHERE UNITID=' . $filters['school']);
			
			$query = "SELECT r.user_id 
					  FROM " . $wpdb->prefix . "postmeta p INNER JOIN 
					  	" . $wpdb->prefix . "command_RSVP r on p.post_id = r.post_id
					  WHERE p.meta_key = 'school' AND meta_value LIKE '%" . $filters['school'] . "%'";
			$potentials = $wpdb->get_results($query, ARRAY_A);
	
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['vec']))
		{
			$list = array();
			
			$school = $wpdb->get_col('SELECT meta_key FROM ' . $wpdb->prefix . 'usermeta WHERE user_id=' . $filters['vec'] . ' AND meta_key LIKE "cmd_team%_staff_pos" AND meta_value="VEC"');
			
			foreach($school as $meta_key)
			{
				$teams[] = preg_replace( '/[^0-9]/', '', $meta_key);
			}
			$teams[] = 0;
						
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "command_RSVP
					  WHERE team_id in(" . implode(',', $teams) . ")";
			$potentials = $wpdb->get_results($query, ARRAY_A);
	
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['team_lead']))
		{
			$list = array();
			
			$school = $wpdb->get_col('SELECT meta_key FROM ' . $wpdb->prefix . 'usermeta WHERE user_id=' . $filters['team_lead'] . ' AND meta_key LIKE "cmd_team%_staff_pos" AND meta_value="Team Leader"');
			
			foreach($school as $meta_key)
			{
				$teams[] = preg_replace( '/[^0-9]/', '', $meta_key);
			}
			$teams[] = 0;
			
			$query = "SELECT user_id 
					  FROM " . $wpdb->prefix . "command_RSVP
					  WHERE team_id in(" . implode(',', $teams) . ")";
			$potentials = $wpdb->get_results($query, ARRAY_A);
	
			foreach($potentials as $record)
			{
				array_push($list, $record['user_id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['role']))
		{
			$list = array();

			if(strpos($filters['role'], '-cap-') > 0)
			{
				$core_filter = substr($filters['role'], 0, strpos($filters['role'], '-cap-'));
				$cap_filter = substr($filters['role'], strpos($filters['role'], '-cap-') + 5);
			}
			else
			{
				$core_filter = $filters['role'];
				$cap_filter = '';
			}

			
			$cap_search = '';
			foreach($this->caps as $group_id => $cap_data)
			{
				if(isset($cap_data[$cap_filter]))
				{
					$cap_search = $cap_data[$cap_filter]['code'];
					break;
				}
			}
			
			$potentials = get_users(array('role' => $core_filter));

			foreach($potentials as $record)
			{
				if(empty($cap_filter))
				{
					array_push($list, $record->ID);
				}
				else
				{
					if(get_user_meta($record->ID, $cap_search))	
					{
						array_push($list, $record->ID);
					}
				}
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		
		if(!empty($filters['poc']))
		{
			$list = array();
			$usr = wp_get_current_user();
			
			if( get_user_meta( $usr->ID, 'cmd_custom_team_poc', true))
			{
				$list = array();
				$query = "SELECT ct.id, pm.meta_value
						  FROM " . $wpdb->prefix . "command_TEAM ct INNER JOIN
							  " . $wpdb->prefix . "command_schedule cs on ct.id = cs.team_id INNER JOIN
							  " . $wpdb->prefix . "postmeta pm on ct.post_id = pm.post_id
						  WHERE pm.meta_key = 'cmd_team_poc' AND 
								ct.team_status_id in (2, 3, 4, 6, 9) AND
								cs.start_date >= '" . date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y") -1)) . "'";
				$potentials = $wpdb->get_results($query, ARRAY_A);
			
				foreach($potentials as $record)
				{
					$arr = unserialize($record['meta_value']);
					
					foreach($arr as $email => $name_data)
					{
						if(!empty($email))
						{
							if($email == $usr->user_email)
							{
								//
								//Now that we know we are POC for this team, get all members of the team
								$query = "SELECT DISTINCT user_id
										  FROM " . $wpdb->prefix . "command_RSVP
										  WHERE team_id = " . $record['id'] . " AND user_id > 0";
								$potentials_users = $wpdb->get_results($query, ARRAY_A);
							
								foreach($potentials_users as $record_user)
								{
									array_push($list, $record_user['user_id']);
								}
							}
						}
					}
				}
			}
		
			array_push($list, $usr->ID);
			self::user_possible_check($possibles, $list, $checks);
		}
		
		if(!empty($filters['countryo']))
		{
			$list = array();

			switch($filters['countryo'])
			{
				case 100:
					$cntry_states = range(51, 60);
					break;
				case 400:
					$cntry_states = array(70);
					break;
				case 500:
					$cntry_states = range(1, 50);
					array_push($cntry_states, 61);
					break;
			}
			
			$query = "SELECT u.id 
					  FROM " . $wpdb->prefix . "users u INNER JOIN 
					  	" . $wpdb->prefix . "usermeta um on u.id = um.user_id
					  WHERE um.meta_key = 'state' AND meta_value IN (" . implode(',', $cntry_states) . ")";
			$potentials = $wpdb->get_results($query, ARRAY_A);
	
			foreach($potentials as $record)
			{
				array_push($list, $record['id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		if(!empty($filters['state']))
		{
			$list = array();
			
			$query = "SELECT u.id 
					  FROM " . $wpdb->prefix . "users u INNER JOIN 
					  	" . $wpdb->prefix . "usermeta um on u.id = um.user_id
					  WHERE um.meta_key = 'state' AND meta_value = " . $filters['state'];
			$potentials = $wpdb->get_results($query, ARRAY_A);
	
			foreach($potentials as $record)
			{
				array_push($list, $record['id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}
		
		if(!empty($filters['role_new']))
		{
			$list = array();
			
			if($filters['role_new'] == 100)
			{
				$query = "SELECT u.id 
						  FROM " . $wpdb->prefix . "users u
						  WHERE u.id NOT IN(
						  	SELECT user_id FROM " . $wpdb->prefix . "usermeta WHERE meta_key = 'v_role' and meta_value between 1 AND 99)";
				
			}
			else
			{
				$query = "SELECT u.id 
						  FROM " . $wpdb->prefix . "users u INNER JOIN 
							" . $wpdb->prefix . "usermeta um on u.id = um.user_id
						  WHERE um.meta_key = 'v_role' AND meta_value = " . $filters['role_new'];
			}
			$potentials = $wpdb->get_results($query, ARRAY_A);
	
			foreach($potentials as $record)
			{
				array_push($list, $record['id']);
			}

			self::user_possible_check($possibles, $list, $checks);
		}

		//
		//Sort the possibles and return
		sort($possibles, SORT_NUMERIC);
		$possibles = array_merge(array(0), $possibles);
		return $possibles;
	}
	
	/**
	 * computes the intersection of the current $possibles with the $list, accouting
	 *  for empty sets and if the $possibles have been iterated to 0 items
	 *  Since all values are passed by reference, there is no return
	 *
	 * @param array &$possible The current set of possible ids
	 * @param array &$list The current list to check against
	 * @param integer &$chekcs The current count of the check flag
	 *
	 */
	private function user_possible_check(& $possibles, & $list, & $checks)
	{
		if(count($list) > 0)
		{
			if(count($possibles) > 0)
			{
				$possibles = array_intersect($possibles, $list);	
			}
			else
			{
				if($checks == 0)
				{
					$possibles = $list;
				}
				else
				{
					$possibles = array();
				}
			}
		}
		else
		{
			$possibles = array();
		}

		$checks ++;	
	}

	/**
	 * Find all VEC in the database 
	 *
	 * @return array of users as user_id => display namearray(first_name, last_name)
	 */
	public function fetch_vec()
	{
		return self::fetch_staff('VEC');
	}

	/**
	 * Find all Team Managers in the database 
	 *
	 * @return array of users as user_id => display namearray(first_name, last_name)
	 */
	public function fetch_team_managers()
	{
		return self::fetch_staff('Team Leader');
	}

	private function fetch_staff($which = 'VEC')
	{
		global $wpdb;
		$staff= array();
		
		$query = "SELECT um.user_id, um.meta_value, u.display_name
					FROM " . $wpdb->prefix . "usermeta um INNER JOIN
						 " . $wpdb->prefix . "users u on u.ID = um.user_id 
				  WHERE meta_key like 'cmd_team%_staff_pos' AND um.meta_value = '" . $which . "'
				  ORDER BY display_name";
		$possibles = $wpdb->get_results($query, ARRAY_A);
		
		foreach($possibles as $data)
		{
			$staff[$data['user_id']] = $data['display_name'];
		}			
		
		return $staff;
		
	}


	/**
	 * Determine if POC exists
	 *
	 * @return boolean
	 */
	public function is_poc($post_id = 0, $email = '')
	{
		if(!empty($post_id) && !empty($email))
		{
			$possibles = self::fetch_poc($post_id);

			if(isset($possibles[$email]))
			{	
				return true;
			}
			
		}
		
		return false;
	}

	/**
	 * Find all POC in the database 
	 *
	 * @return array of users as user_id => array(email, first_name, last_name, verified user)
	 */
	public function fetch_poc($post_id = 0)
	{
		$staff = array();

		if(!empty($post_id))
		{
			$listing = get_post_meta($post_id, 'cmd_team_poc', false);
			if(!empty($listing))
			{
				foreach($listing[0] as $email => $data)
				{
					if(!empty($email) && !empty($data['first_name']) && !empty($data['last_name']))
					{
						$staff[$email] = array('first_name' => $data['first_name'],
											   'last_name' => $data['last_name'],
											   'verified' => '0'
											   );

						$user = get_user_by('email', $email);

						if(!empty($user))
						{
							$staff[$email]['verified'] = $user->ID;	
						}
					}
				}
			}
			
		}

		return $staff;
	}

	/**
	 * Set POC in the database 
	 *
	 * @string $post_id The post of the team
	 * @array $poc in form of (email[$] => array(first_name => $, last_name => $))
	 */
	public function set_poc($post_id = 0, $poc = array())
	{
		$staff = array();

		if(!empty($poc) && !empty($post_id))
		{
			foreach($poc as $email => $data)
			{
				$staff[$email] = $data;
			}

			update_post_meta($post_id, 'cmd_team_poc', $staff);

			//
			//Update actual permissions if necessary
			foreach($poc as $email => $data)
			{
				//
				//Find user by email
				$usr = get_user_by('email', $email);

				if($usr)
				{
					if(self::user_group($usr->ID) == "volunteer" || self::user_group($usr->ID) == "enhanced")
					{
						wp_update_user( array( 'ID' => $usr->ID, 'role' => 'enhanced') );
						update_user_meta($usr->ID, 'cmd_custom_team_poc', 'true');
					}
					
				}
			}
		}
		else if (!empty($post_id))
		{
			delete_post_meta($post_id, 'cmd_team_poc');
		}
	}

	/**
	 * Find all VEC in the database 
	 *
	 * @return array of users as user_id => display namearray(first_name, last_name)
	 */
	public function user_select($user_group = array(), $which = 0, $zero_nomenclature = '', $add_zero = false)
	{
		$option_list = '';
		
		if(!empty($user_group))
		{
			if($add_zero)
			{
				$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . ">" . $zero_nomenclature . "</OPTION>";
			}
	
			foreach($user_group as $id => $data)
			{
				$option_list .= "<OPTION VALUE=" . $id . (($which==$id) ? " SELECTED" : "") . ">" . $data . "</OPTION>";		
			}
		}

		return $option_list;
			
	}

	/**
	 * Group Methods
	 */

	/**
	 * Sets the group 
	 *
	 * @param integer @group_id id of the group
	 */
	public function set_group($group_id)
	{
		if(self::is_group($group_id))
		{
			$this->group = $group_id;
		}
	}

	/**
	 * Sets the current group by the code
	 *
	 * @param integer $code Code to set the group by
	 *
	 * @return boolean True if the group exists, and is set
	 */
	public function set_group_by_code($code)
	{
		foreach($this->groups as $id => $data)
		{
			if($data['code']==$code)
			{
				self::set_group($id);
				return true;
			}
		}
	}

	/**
	 * Gets the current group
	 *
	 * @return integer The id of the current grou
	 */
	public function get_group()
	{
		if(isset($this->group))
		{
			return $this->group;
		}
	}

	/**
	 * Gets the title of the group
	 *
	 * @return string Title of the current grou
	 */
	public function get_title()
	{
		if(isset($this->group))
		{
			return $this->groups[$this->group]['title'];
		}
	}

	/**
	 * Chekcs if the group identified exists
	 *
	 * @param integer $group_id ID of the group to check
	 *
	 * @return boolean True if group is in the list of groups, else returns false
	 */
	public function is_group($group_id)
	{
		return isset($this->groups[$group_id]);
	}
		

	/**
	 * Capability Methods
	 */

	/**
	 * Gets the capability detalis for the current group
	 *
	 * @return array List of capabilities for the group
	 */
	public function cap_details()
	{
		if(isset($this->caps) && self::is_group($this->group))
		{
			return !empty($this->caps[$this->group]) ? $this->caps[$this->group] : array();
		}
	}

	/**
	 * Gets the count of capabilities
	 *
	 * @param integer $group ID of the group
	 *
	 * @return integer Number of capabilities for the group OR 0 if not set
	 */
	public function cap_count($group)
	{
		if(isset($this->group_count[$group]))
		{
			return $this->group_count[$group];
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Gets the title for the capability
	 *
	 * @param integer $cap ID of the capability
	 *
	 * @return string Capability title OR empty string if not set
	 */
	public function cap_code_to_title($cap)
	{
		return isset($this->cap_translate[$cap]) ? $this->cap_translate[$cap] : "";
	}


	/**
	 * Methods to deal with Users
	 */

	/**
	 * Gets the user's name
	 *
	 * @param integer $user_id ID of the user
	 *
	 * @return string User's nickname
	 */
	public function user_name($user_id)
	{
		return get_user_meta($user_id, 'first_name', true) . ' ' . get_user_meta($user_id, 'last_name', true);
	}
	
	/**
	 * Builds an array of user data for users that have advanced permissions
	 *
	 * @return array The array of user data for users with advanced permissions
	 */
	public function users_with_adv_permissions()
	{
		global $wpdb;

		//
		//Gather possible data here
		$meta_codes = self::list_cap_codes();
		$query = "SELECT um.user_id, um.meta_key, u.display_name 
				  FROM " . $wpdb->prefix . "usermeta um INNER JOIN " . $wpdb->prefix . "users u
				  		ON u.ID = um.user_id  
				  WHERE meta_key in ('" . implode("','", $meta_codes) . "') OR
		  				(meta_key = 'wp_user_level' AND meta_value >=7)
				  ORDER BY display_name";
		$possibles = $wpdb->get_results($query, ARRAY_A);
		foreach($possibles as $data)
		{

			if($data['meta_key'] == 'wp_user_level')
			{
				$permission = "Admin";	
			}
			else
			{
				$permission = $data['meta_key'];
			}
			$users_with_permissions[$data['user_id']][] = $permission;		  
			$users_with_permissions[$data['user_id']]['name'] = $data['display_name'];	
		}

		 
		
		return $users_with_permissions;
	}

	/**
	 * Finds the highest permission level the user has.
	 *
	 * @return string The code of the permission level
	 */
	public function user_group($user_id = 0)
	{
		if(user_can($user_id, 'super'))
		{
			return 'super';
		}
		if(user_can($user_id, 'admin'))
		{
			return 'admin';
		}
		if(user_can($user_id, 'staff'))
		{
			return 'staff';
		}
		if(user_can($user_id, 'enhanced'))
		{
			return 'enhanced';
		}

		return 'volunteer';
	}

	public function user_title($user_id = 0)
	{

		$group = self::user_group($user_id);
		
		if($group == 'enhanced')
		{
			foreach($this->caps[1] as $id => $info)
			{
				if(self::has_cap($info['code'],$user_id) == 'true')
				{
					$group = $info['title'];
				}
			}
		}
		
		return $group;
	}

	
	/**
	 * Methods to deal with multiples
	 */

	/**
	 * Gets the list of groups
	 *
	 * @return array List of groups
	 */
	public function list_groups()
	{
		return $this->groups;
	}

	/**
	 * Gets list of capability codes
	 *
	 * @return array List of capability codes
	 */
	public function list_cap_codes()
	{
		$caps = array();
		
		foreach($this->caps as $group_id => $data)
		{
			foreach($data as $cap_id => $cap_data)
			{
				$caps[] = $cap_data['code'];
			}
		}

		return $caps;
	}

	/**
	 * Determines if a user has a specific capability or not
	 * 
	 * @param integer $user_id The id of the user
	 * 
	 * @return boolesn
	 */
	public function has_cap($cap, $user_id)
	{
		return get_user_meta( $user_id, $cap, true);
	}

	/**
	 * Builds a checkbox list of capabilities for a user
	 * 
	 * @param integer $user_id The id of the user
	 * 
	 * @return string The HTML string of checkbox form inputs for the user's caps
	 */
	public function cap_checks($user_id)
	{

		$checks = '';

		//
		// iterates through the caps for the user and builds the checkbox list
		// of caps
		foreach($this->caps[$this->group] as $cap)
		{
			$checks .= "<input id='" . $cap['code'] . "' 
						 type='checkbox' 
						 name='" . $cap['code'] . "' 
						 value='true' "; 
			$checks .= get_user_meta( $user_id, $cap['code'], true) == 'true' ? "checked = 'checked'" : "";
			$checks .= ">";
			$checks .= "<label for='" . $cap['code'] . "'>" . $cap['title'] . " &nbsp;&nbsp;</label>";
			$checks .= "<br>";
		}

		if(strlen($checks) > 4)
		{
			$checks = substr($checks, 0, -4);
		}
		return $checks;
	}
	
	/**
	 * Builds a HTML string of options for groups
	 * 
	 * @param integer $which The id of the current group for the user
	 * @param boolean $addzero If true, add the "none" option at the top of the options
	 * 
	 * @return string $option_list The HTML formatted string of group options
	 */
	public function group_selector($which = 0, $addzero = false, $expand = array())
	{
		$option_list = '';

		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . "> -- NONE --</OPTION>";
		}
		foreach($this->groups as $id => $data)
		{
			$option_list .= "<OPTION VALUE='" . $data['code'] . (($which===$data['code']) ? "' SELECTED" : "'") . ">"
						 . $data['title'] . "</OPTION>";		

			if(in_array(strtolower($data['title']), $expand))
			{
				foreach($this->caps[$id] as $cap_id => $cap_data)
				{
					$option_list .= "<OPTION VALUE='" . $data['code'] . '-cap-' . $cap_id . (($which===($data['code'] . '-cap-' . $cap_id)) ? "' SELECTED" : "'") . ">&nbsp;&nbsp;-->&nbsp; "
						 . $cap_data['title'] . "</OPTION>";
				}
			}
		
		}
			
		return $option_list;
	}

	/**
	 * Builds a HTML string of options for roles for a user
	 * 
	 * @param integer $which The id of the current role for the user
	 * @param boolean $addzero If true, add the "none" option at the top of the options
	 * 
	 * @return string $option_list The HTML formatted string of role options
	 */
	public function role_selector($which = 0, $addzero = false)
	{
		$option_list = '';
		
		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . "> -- NONE --</OPTION>";
		}
		foreach($this->caps[$this->group] as $id => $data)
		{
			$option_list .= "<OPTION VALUE='" . $data['code'] . (($which==$data['code']) ? "' SELECTED" : "'") . ">"
						 . $data['title'] . "</OPTION>";		
		}
		
		return $option_list;
	}

	/**
	 * Builds a HTML string of options for colleges
	 * 
	 * @param integer $which The id of the current role for the user
	 * @param boolean $addzero If true, add the "none" option at the top of the options
	 *
	 * @return string $option_list The HTML formatted string of colleges
	 */
	public function school_selector($which = 0, $addzero = false)
	{
		$option_list = '';
		
		if($addzero)
		{
			$option_list .= "<OPTION VALUE=0" . (($which==0) ? " SELECTED" : "") . ">School (ANY)</OPTION>";
		}
		foreach($this->colleges as $id => $data)
		{
			$option_list .= "<OPTION VALUE='" . $id . (($which==$id) ? "' SELECTED" : "'") . ">"
						 . $data . "</OPTION>";		
		}
		
		return $option_list;
	}

	/**
	 * Fetch the possible schools
	 *
	 * @param boolean $reset Resets the current list of schools, or just refreshes them
	 * @param string $clause The where clause for the query
	 *
	 * @return array $data The list of schools in id->name format
	 */
	 public function school_fetch($reset = true, $clause = "")
	 {
		global $wpdb;
		
		if($reset)
		{
			unset($this->colleges);
			$this->colleges = array();
		}
		
		$query = "SELECT UNITID, INSTNM from " . $wpdb->prefix . "command_schools ";
		$query = $query . ((!empty($clause)) ? $clause . " " : "");
		$query = $query . "ORDER BY INSTNM";
		
		$possibles = $wpdb->get_results($query, ARRAY_A);
		
		foreach($possibles as $record)
		{
			$this->colleges[$record['UNITID']] = $record['INSTNM'];
		}
	 }

	/**
	 * Private Methods
	 */

	/**
	 * Builds the groups to be used
	 */
	private function build_groups()
	{
		global $wpdb;

		$query = "SELECT id, title, kode, priority
					FROM " . $this->cap_group_table . "
					ORDER BY priority asc";
		$capabilities = $wpdb->get_results($query, ARRAY_A);

		foreach($capabilities as $data)
		{
			$this->groups[$data['id']] = array('title' => $data['title'],
  											   'code' => $data['kode'],
											   'priority' => $data['priority']
											  );
			$this->group_count[$data['id']] = 0;
		}
	}

	/**
	 * Builds the capabilities to be used
	 */
	private function build_caps()
	{
		global $wpdb;

		$query = "SELECT id, cap_group_id, title, kode
					FROM " . $this->cap_table . "
					ORDER BY title asc";
		$caps = $wpdb->get_results($query, ARRAY_A);

		foreach($caps as $data)
		{
			$this->caps[$data['cap_group_id']][$data['id']] = array('title' => $data['title'],
																	'code' => $data['kode'],
																	);
			$this->group_count[$data['cap_group_id']]++;
			$this->cap_translate[$data['kode']] = $data['title'];
		}
	}
}