<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/**
 * DOCS class
 * Contairn functionality for different documents that are used
 */
CLASS DOCS
{
	/** @var array $documents The list of documents */
	private $documents = array();
	
	/** @var array $document_keys The list of document keys */
	private $document_keys = array();

	/** @var string $post_type Type of post created by docs */
	private $post_type = 'cp_team_docs';

	/** @var string $meta_key Meta key for docs */
	private $meta_key = 'team_docs';

	/**
	 *Filters
	 */

	/** @var array $countries Countries array */
	private $countries = array();

	/** @var array $programs Array of programs for a doc */
	private $programs = array();

	/** @var array $teams Array of teams for a doc */
	private $teams = array();	

	/**
	 *Team Document Variables
	 */

	/** @var array $team_documents Array of documents for a team */
	private $team_documents = array();

	/** @var integer $team_id ID of the team */
	private $team_id = NULL;

	/** @var integer $post_id Post ID for the team */
	private $post_id = NULL;

	/**
	 *User variables
	 */

	/** @var integer $user User id */
	private $user = 0;

	/** @var integer $user User id */
	private $custome_user_docs = false;

	/** @var array $read_list list of read docs for the user */
	private $read_list = array();

	/**
	 * Default Constructor - sets the user to the current user id
	 */
	public function __construct($user_id = null)
	{
		if(!empty($user_id))
		{
			$this->user = $user_id;
		}
		else
		{
			$this->user = get_current_user_id();
		}
	}
	
	/**
	 * Sets the user incase it's different than expected
	 *
	 * @param array $user_id The id of the user
	 */
	public function set_user($user_id = 0)
	{
		$this->user = $user_id;
	}

	/**
	 * Sets the list of countries
	 *
	 * @param array $country_list List of countries to be set to this->countries
	 */
	public function set_countries($country_list = array())
	{
		$this->countries = $country_list;
	}

	/**
	 * Sets the list of programs
	 * 
	 * @param array $program_list List of programs to be set to this->progams
	 */
	public function set_programs($program_list = array())
	{
		$this->programs = $program_list;
	}

	/**
	 * Sets the teams
	 *
	 * @param array $team_list List of teams to be set to this->teams
	 */
	public function set_teams($team_list = array())
	{
		$this->teams = $team_list;
	}

	/**
	 * Sets if the custom user docs should be fetched or not
	 *
	 * @param boolean $find_custommers true if custom docs are to be added to the list.
	 */
	public function set_custom($find_customers = false)
	{
		$this->custom_user_docs = $find_customers;
	}

	/**
	 *Document Listings for given filters
	 */

	/**
	 * Runs self::fetch_ids()
	 */
	public function fetch($personal = false, $private = false)
	{
		$possibles = self::fetch_ids($personal, $private);
	}

	/**
	 * Returns the document list
	 *
	 * @return array List of documents
	 */
	public function document_list()
	{
		return $this->documents;
	}

	/**
	 * Fetch Read Documents and Mark Documents Read
	 *
	 * @return array List of read documents
	 */
	public function list_read()
	{
		return $this->read_list;
	}

	/**
	 * Check if document has been read
	 *
	 * @param integer $document_id ID of the document to be checked
	 *
	 * @return boolean true if document is in the read document list
	 */
	public function is_read($document_id)
	{
		return array_key_exists($document_id, $this->read_list);
	}

	/**
	 * Gets the read_list for the user
	 *
	 * @param integer $user_id The user id
	 *
	 * @return array The list of items read/signed
	 */
	public function fetch_read($user_id = 0)
	{
		$this->read_list = array();
		$which_user = ($user_id > 0 ? $user_id : $this->user);
		$possibles = get_post_meta($this->post_id, 'read_docs_' . $which_user);

		if(!empty($possibles))
		{
			foreach($possibles as $id=>$data)
			{
				foreach($data as $doc_id=>$signature)
				{
					$this->read_list[$doc_id] = $signature;
				}
			}
		}
		
		return $this->read_list;
	}

	/**
	 * Return the keys of read documents
	 *
	 * @return array List of keys of read documents
	 */
 	function fetch_read_keys()
	{
		return array_keys($this->read_list);
	}

	/**
	 * Updates the read list with the new read document
	 *
	 * @param integer $document_id ID of the document read
	 * @param string $signature Signature of the user
	 */
 	function update_read($document_id, $signature)
	{
		self::fetch_read();
		$this->read_list[$document_id] = $signature;
		update_post_meta($this->post_id, 'read_docs_' . $this->user, $this->read_list);
	}

	/**
	 * Removes the document from the read list
	 *
	 * @param integer $document_id: ID of the document to be removed
	 */
	function remove_read_document($document_id)
	{
		
		self::fetch_read();
		unset($this->read_list[$document_id]);
		update_post_meta($this->post_id, 'read_docs_' . $this->user, $this->read_list);
	}

	/**
	 * functions to deal with the document list associated with a team
	 */

	/**
	 * Sets the post id
	 * 
	 * @param integer $id The id of the post
	 */
	public function post_id($id = 0)
	{
		$this->post_id = $id;
	}

	/**
	 * Returns the list of team documents
	 *
	 * @return array The list of team documents
	 */
	public function get_team_list()
	{
		if(isset($this->post_id))
		{
			$doc_list = get_post_meta($this->post_id, $this->meta_key);
			if(isset($doc_list[0]))
			{
				$this->team_documents = explode(";", $doc_list[0]);
			}
			else
			{
				$this->team_documents = array();
			}
		}
		else
		{
			$this->team_documents = array();
		}
		

		if(!empty($this->custom_user_docs) && !empty($this->post_id))
		{
			$cur_list = get_user_meta($this->user, 'custom_docs_' . $this->post_id);

			if(isset($cur_list[0]))
			{
				$this->team_documents = array_merge($this->team_documents, explode(";",$cur_list[0]));
			}
		}

		return $this->team_documents;
	}

	/**
	 * Sets the team list depending on the document list
	 *
	 * @param array $doc_array List of documents
	 */
	public function set_user_list($doc_array)
	{
		if(!empty($this->post_id) && !empty($this->user))
		{
			$doc_list = implode(";", $doc_array);
			update_user_meta($this->user, 'custom_docs_' . $this->post_id, $doc_list);
		}
	}
	
	/**
	 * Sets the team list depending on the document list
	 *
	 * @param array $doc_array List of documents
	 */
	public function add_user_list($doc_array)
	{
		if(!empty($this->post_id) && !empty($this->user) && !empty($doc_array))
		{
			$arr_cur_docs = get_user_meta($this->user, 'custom_docs_' . $this->post_id);
			
			if(isset($arr_cur_docs[0]))
			{
				$arr_cur_doc_array = explode(";", $arr_cur_docs[0]);
			}
			else
			{
				$arr_cur_doc_array = array();
			}

			self::set_user_list(array_merge($arr_cur_doc_array, $doc_array));
		}
	}

	/**
	 * Removes items from a custome doc list
	 *
	 * @param array $doc_array List of documents to remove
	 */
	public function remove_user_list($doc_array)
	{
		if(!empty($this->post_id) && !empty($this->user) && !empty($doc_array))
		{
			$arr_cur_docs = get_user_meta($this->user, 'custom_docs_' . $this->post_id);
			
			if(isset($arr_cur_docs[0]))
			{
				$arr_cur_doc_array = explode(";", $arr_cur_docs[0]);
				self::set_user_list(array_diff($arr_cur_doc_array, $doc_array));
			}
		}
	}

	/**
	 * Returns the post id of the minor travel form
	 *
	 * @return number The ID of the post
	 */
		public function minor_travel_form()
	{
		$posts =  get_posts( array('posts_per_page' => 1, 'post_type' => 'cp_team_docs', 'post_status' => 'publish', 'title' => 'Minor Travel Forms') );
		return !empty($posts) ? $posts[0]->ID : 0;
	}

	/**
	 * Sets the team list depending on the document list
	 *
	 * @param array $doc_array List of documents
	 */
	public function set_team_list($doc_array)
	{
		if(isset($this->post_id) && $this->post_id >0)
		{
			$doc_list = implode(";", $doc_array);
			update_post_meta($this->post_id, $this->meta_key, $doc_list);
		}
	}

	/**
	 * Clears the team list
	 */
	public function clear_team_list()
	{
		if(isset($this->post_id) && $this->post_id > 0)
		{
			delete_post_meta($this->post_id, $this->meta_key);	
		}
	}

	/**
	 * Determines if all the docs for the team have been read.
	 *
	 * @return boolean true/false if all have been read
	 */
	public function documents_complete()
	{
		$team_docs = count(self::team_docs());
		$user_docs = count($this->read_list);
	
		return ($team_docs == $user_docs) ? true : false;
	}


	/**
	 *Mixed team/filtered lists
	 */

	/**
	 * Returns list of team documents
	 *
	 * @return array List of team documents
	 */
	public function team_docs()
	{
		self::get_team_list();
		return array_intersect_key($this->documents, array_flip($this->team_documents));	
	}

	/**
	 * Returs list of documents not found
	 *
	 * @return array List of documents not part of the team docs
	 */
	public function missing_docs()
	{
		return array_diff_key($this->documents, array_flip($this->team_documents));		
	}


	/**
	 * Private functions
	 */
	
	/**
	 * Fetch returns a list of ID's that indicate which documents conform to the filters
	 *
	 * @return array List of ID's of documents based on filters
	 */
	private function fetch_ids($personal = false, $private = false)
	{
		$found_docs = array();
		$need_count = 0;

		$country_filter = 'skip';
		if(isset($this->countries) && count($this->countries)>0)
		{
			$country_filter = 'array';
			$need_count++;
		}

		$program_filter = 'skip';
		if(isset($this->programs) && count($this->programs)>0)
		{
			$program_filter = 'array';
			$need_count++;
		}

		$team_filter = 'skip';
		if(isset($this->teams) && count($this->teams)>0)
		{
			$team_filter = 'array';	
		}

		//
		//Find all posts that contain documents
		$posts = get_posts(array('post_type' => 'cp_team_docs', 'posts_per_page' => 100000, 'orderby' => 'title', 'order' => 'ASC'));

		foreach($posts as $post)
		{
			$cur_count = 0;

			if($country_filter == 'array')
			{
				//$this->countries['temp'] = 17;
				$intersect = array();
				$country_list = get_post_meta($post->ID, 'countries')[0];

				if(is_array($country_list))
				{
					$intersect = array_intersect($this->countries, $country_list);
					if(count($intersect)>0)
					{
				//echo $post->ID . "<BR>";
						$cur_count++;
					}
				}
				//unset($this->countries['temp']);
			}

			if($program_filter == 'array')
			{
				$intersect = array();
				$program_list = get_post_meta($post->ID, 'associated_programs')[0];

				
				if(is_array($program_list))
				{
					$intersect = array_intersect($this->programs, $program_list);
					if(count($intersect)>0)
					{
						$cur_count++;
					}
					
				}
			}
			
			if($team_filter == 'array')
			{
				$intersect = array();
				$team_list = get_post_meta($post->ID, 'team_id')[0];

				if(is_array($team_list))
				{
					$intersect = array_intersect($this->teams, $team_list);
					if(count($intersect)>0)
					{
						$cur_count = $need_count;
					}
					
				}
			}

			if($cur_count == $need_count)
			{
				$this->documents[$post->ID] = $post->post_title;
				$this->document_keys[] = $post->ID;
			}
		}

		if($personal)
		{
			//
			//Find all posts that contain documents
			$posts = get_posts(array('post_type' => 'cp_special_doc', 
									 'posts_per_page' => 100000,
									 'post_status' => 'inherit',
									 'orderby' => 'title', 
									 'order' => 'ASC'));
			foreach($posts as $post)
			{
				$this->documents[$post->ID] = $post->post_title;
				$this->document_keys[] = $post->ID;
			}
			
		}

		if($private)
		{
			//
			//Find all posts that contain documents
			$posts = get_posts(array('post_type' => 'cp_user_doc', 
									 'posts_per_page' => 100000,
									 'post_status' => 'inherit',
									 'orderby' => 'title', 
									 'order' => 'ASC'));
			foreach($posts as $post)
			{
				$this->documents[$post->ID] = $post->post_title;
				$this->document_keys[] = $post->ID;
			}
			
		}

//		return $found_docs;
		
//		echo var_dump($this->documents);
	}

	/**
	 * Deconstructs the meta_string data passed in and returns the array of data
	 *
	 * @param string $meta_string The meta data to be deconstructed/converted into an arry
	 *
	 * @return array $return The array deconstruction of the $meta_string
	 */
	private function deconstruct_meta($meta_string = '')
	{
		$return = array();
		
		$wo_array = substr($meta_string, strpos($meta_string, '{') +1, -1);
		$item_array = explode(";", $wo_array);

		foreach($item_array as $element)
		{
			if(substr($element,0,1) == "s")
			{
				$string_array = explode(":", $element);
				$return[] = stripslashes($string_array[3]);
			}
		}
		
		return $return;
	}
}