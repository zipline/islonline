<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once CC_CLASSES_DIR . "cart.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "country.class.php";
require_once CC_CLASSES_DIR . "user.class.php";

//
//2018-02-21 DJT  Updated wp_command_order_transact table as follows to capture additional data
/*
ALTER TABLE  `wp_command_order_transact` ADD  `ip_address` VARCHAR( 64 ) NOT NULL AFTER  `transaction_id` ,
ADD  `user_agent` INT( 11 ) NOT NULL DEFAULT  '0' AFTER  `ip_address` ,
ADD  `pp_trans_id` VARCHAR( 64 ) NOT NULL AFTER  `user_agent` ,
ADD INDEX (  `user_agent` ) ;
*/

//
//2018-02-21 DJT Created new user agent table to track unique user agents for the order_transact table
/*
CREATE TABLE IF NOT EXISTS `wp_command_user_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) NOT NULL COMMENT 'md5 hash',
  `user_agent` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
*/

//
//2018-02-22 DJT alterd wp-command_order_transact for a default form if none specified.
/*
ALTER TABLE  `wp_command_order_transact` CHANGE  `form`  `form` VARCHAR( 25 ) 
CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  'Unknown';
*/


/**
 * FINANCE class
 * handles financial data (orders, payments, transactions)
 */
CLASS FINANCE
{

	/** @var string $order_table Name of the table in the db for orders */
	private $order_table = NULL;

	/** @var string $order_item_types Name of the table in the db for orders */
	private $order_item_types = NULL;

	/** @var string $item_table Name of the table in the db for items */
	private $item_type_table = NULL;

	/** @var string $item_table Name of the table in the db for items */
	private $item_table = NULL;

	/** @var string $payment_table Name of the table in the db for payments */
	private $payment_table = NULL;

	/** @var string $transact_table Name of the table in the db for transactions */
	private $transact_table = NULL;

	/** @var float $transaction_amount Dollar value of the transaction */
	private $transaction_amount = NULL;

	/** @var float $transaction_remaining Dollar value of the remaining balance */
	private $transaction_remaining = NULL;

	/** @var integer $transaction_id ID of the transaction */
	private $transaction_id = NULL;

	/** @var integer $order_id ID of the order */
	private $order_id = NULL;

	/** @var integer $order_id ID of the order */
	private $cur_funds = 0;

	/** @var integer $item ID of the item */
	private $item = NULL;

	/** @var array $payment_types List of payment types */
	private $payment_types = array();

	/** @var array $item_types List of payment types */
	private $item_types = array();

	/** @var  $temp */
	private $temp = NULL;

	/** @var integer $user ID of the user */
	private $user = NULL;

	/** @var list of items **/
	private $items = array();
	
	/***
	 * Default Constructor
	 * Sets up DB connections and defaults for the current finance object
	****/
	public function __construct($cookie = false)
	{
		global $wpdb;
		
		$this->order_table = $wpdb->prefix . "command_orders";
		$this->item_table = $wpdb->prefix . "command_order_items";
		$this->payment_table = $wpdb->prefix . "command_order_payment";
		$this->transact_table = $wpdb->prefix . "command_order_transact";
		$this->item_type_table = $wpdb->prefix . "command_order_item_types";

		$current_user = wp_get_current_user();
		$this->user = $current_user->ID;

		$this->transaction_id = 0;
		$this->order_id = 0;

		$this->items_table = array();
		
		self::build_payment_types();
		self::build_item_types();
	}

	
	//////////////////////////////////////////////////////////////////////////
	// Functions to deal with Order IDs
	/////////////////////////////////////////////////////////////////////////

		public function change_user($user_id)
		{
			$this->user = $user_id;
			
		}
	
	/**
	 * Returns the order id
	 *
	 * @return integer The id of the order
	 */
	public function get_order_id()
	{
		return self::OrderId();
	}

	/**
	 * Sets the order id
	 *
	 * @param integer $id The id of the order
	 */
	public function set_order_id($id = 0)
	{
		self::OrderId($id);	
	}

	/***
	 * Gets or Sets the Order ID
	 *
	 * @ param: optional numeric $id - the ID of the Order if setting
	 *
	 * @ return: optional numberic - the ID of the order if already set
	 *
	****/
	public function OrderId($Id = 0)
	{
		if(func_num_args() > 0)
		{
			$this->order_id = $Id;
		}
		else
		{
			return $this->order_id;
		}

	}

	public function CurrentFunds($Amt = 0)
	{
		if(func_num_args() > 0)
		{
			$this->cur_funds = $Amt;
		}
		else
		{
			return $this->cur_funds;	
		}
	
	}

	/***
	 * Finds the order_id when given the id from the item's table
	 * Since Reservations (RSVP) are matched vs order_items, this can be used to match an order to the RSVP
	 *
	 * @param - required string $field - the field used to test for the id
	 * @options (rsvp, order_item_id)
	 * @param - required numeric $value - the id of the field
	 * @param - optional boolean $set - if set to true, will call the OrderID function
	 *
	 * @return - numeric - the id of the order
	****/
	public function Fetch_OrderId($Field, $Value, $Set = false)
	{
		global $wpdb;
		
		//
		//If we are passed the RSVP, then find the order_item_id and set it
		if($Field == 'rsvp')
		{
			$rsvp = new RSVP();
			$rsvp->set_rsvp_id( $Value );
			$rsvp->fetch_rsvp();
	
			$reservation = $rsvp->get_rsvp();
			
			//
			//If we have an order_item_id then switch the function parameters to id
			if( isset( $reservation['order_item_id'] ))
			{
				$Value = $reservation['order_item_id'];
				$Field = 'order_item_id';
			}
		}
		
		//
		//If we have the order_item_id, then use it to find the OrderID
		if($Field == 'order_item_id')
		{
			$query = "SELECT order_id FROM " . $this->item_table . " WHERE id=" . $Value;
			$temp_var = $wpdb->get_var($query);
		}


		//
		//Set the return value
		$ret_val = max(0, $temp_var);
		
		//
		//Set the Order ID if we have it.
		if($Set)
		{
			self::OrderId($ret_val);
		}
		
		return $ret_val;
	}

	
	//////////////////////////////////////////////////////////////////////////
	// Functions to deal with Orders
	/////////////////////////////////////////////////////////////////////////

	/***
	 * creates a new order and sets the ID
	 *
	****/
	public function Create_Order()
	{
		global $wpdb;
		
		$wpdb->insert($this->order_table, 
					  array('created_by' => $this->user,
							'created_when' => NULL
							)
					  );

		self::OrderId($wpdb->insert_id);	
	}
	
	/*
	 * Creates new order in db and as an object
	 */
	public function new_order()
	{
		self::Create_Order();
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	// Functions to deal with Items within an Order
	/////////////////////////////////////////////////////////////////////////

	/***
	 *
	 * Returns an array of objects that contains all the current items
	 * 
	 * @param: optional array $ItemsArr - all the item data to set into the array
	 *
	 * @return: optional array - the elements of hte array of current items
	 *
	****/
	public function Items($ItemsArr = array())
	{
		if(func_num_args() > 0)
		{
			$this->items = $ItemsArr;
		}
		else
		{ 
			return $this->items;
		}
	}

	/**
	 * Inserts the new item into the database
	 *
	 * @param array $details The details/info for the new item
	 *
	 * @return integer The id of the item
	 */
	public function new_item($details = array())
	{
		global $wpdb;
		
		/** if details add_on is true/1 and details type is 0 */
		if($details['add_on']>0 && $details['type']==0 && 0)
		{
			$query = "SELECT originals.id 
					  FROM 
						(SELECT id 
						 FROM " . $this->item_table . " 
						 WHERE add_on = 0 AND
						 	   item_id = " . $details['item_id'] . " AND
							   order_id = " . $this->order_id . ") originals

					  LEFT JOIN

						(SELECT add_on 
						 FROM " . $this->item_table . " 
						 WHERE add_on > 0 AND
						 	   item_id = " . $details['item_id'] . " AND
							   order_id = " . $this->order_id . ") add_ons

					  ON originals.id = add_ons.add_on
					  WHERE add_on is null";

			$query = "SELECT id 
						 FROM " . $this->item_table . " 
						 WHERE add_on = 0 AND
						 	   item_id = " . $details['item_id'] . " AND
							   order_id = " . $this->order_id;

			/** get possible matches for item */
			$possibles = $wpdb->get_results($query, ARRAY_A);

			/** if there is an item returned */
			if(!empty($possibles[0]))
			{
				/** use it's id as the add_on for the new item */
				$wpdb->insert($this->item_table,
							  array('order_id' => $this->order_id,
									'item_type' => $details['type'],
									'item_id' => $details['item_id'],
									'add_on' => $possibles[0]['id'],
									'quantity' => $details['quantity'],
									'note' => $details['note'],
									'price' => $details['price'],
									'created_by' => $this->user,
									'created_when' => NULL
									));
			}
			else
			{
				/** else insert the new item with details add_on attribute */
				$wpdb->insert($this->item_table,
							  array('order_id' => $this->order_id,
									'item_type' => $details['type'],
									'item_id' => $details['item_id'],
									'add_on' => $details['add_on'],
									'quantity' => $details['quantity'],
									'note' => $details['note'],
									'price' => $details['price'],
									'created_by' => $this->user,
									'created_when' => NULL
									));
			}

		}
		else
		{
			//
			// else insert the new item with details attributes
			$wpdb->insert($this->item_table,
						  array('order_id' => isset($details['order_id']) ? $details['order_id'] : $this->order_id,
								'item_type' => $details['type'],
								'item_id' => $details['item_id'],
								'add_on' => $details['add_on'],
								'quantity' => $details['quantity'],
								'note' => $details['note'],
								'price' => $details['price'],
								'created_by' => $this->user,
								'created_when' => NULL
								));
		}

		//
		//2018-03-11 DJT Added do_action to let other functions see the cart as it's being added to an order, item by item
		$item_ins_id = $wpdb->insert_id;
		do_action('cmd_purchased_items', $details, $this->user, $item_ins_id);
		//return $wpdb->insert_id;
		return $item_ins_id;
	}
	
	/**
	 * Returns the requested item from the database
	 * @param integer $order_item_id Item id of requested item
	 * @return array The item info
	 */
	public function fetch_item($order_item_id)
	{
		/*
		global $wpdb;
		$query = "SELECT id, order_id, item_type, item_id, add_on, price, quantity 
				  FROM " . $this->item_table . " 
				  WHERE id = " . $order_item_id . " AND
				  		add_on = 0
				  ORDER BY item_type";
		$item = $wpdb->get_results($query, ARRAY_A);
			
		if(!empty($item[0]))
		{
			return $item[0];
		}
		else
		{
			return array();
		}
		*/
		
		$items = array();
		$single = self::Fetch_Items( array($order_item_id ));
		
		if( !empty( $single ))
		{
			foreach($single as $values)
			{
				foreach($values as $index=>$value)
				{
					$items[$index] = $value;
				}
			}
		}
		
		return $items;
	}
	
	/***
	 *
	 * Fetches one or more items from the database and returns their recordset
	 *
	 * @param: required array $ItemIds - the order item ids that are to be fetched.
	 * @param: optional boolean AddOns - Add all the add-ons for the given items too.
	 *
	 * @return: array of the item data 
	 * @fields: id, order_id, item_type, item_verbiage, item_id, add_on, notw, price, quantity, payid, balance, creaed_by, created_when
	 *
	****/
	public function Fetch_Items( $ItemIds = array(), $AddOns = false, $MultipleOrders = false )
	{
		global $wpdb;
	
		if( !empty( $ItemIds ))
		{
			$query = 'SELECT * FROM ' . $this->item_table . ' WHERE id in(' . implode( ',', $ItemIds ) . ')';
			$temp_items = $wpdb->get_results( $query, OBJECT_K );

			if( !empty( $temp_items ))
			{
				foreach( $temp_items as $single_item )
				{
					$return_items[$single_item->id] = $single_item;
					
					$query = 'SELECT sum(amt) FROM 
								(SELECT sum(amount) amt FROM ' . $this->payment_table . ' WHERE order_item_id = ' . $single_item->id . '
							  	 UNION
							  	 SELECT IFNULL(sum(price),0)*(-1) amt FROM ' . $this->item_table . ' WHERE add_on = ' . $single_item->id . ' AND item_type in (61,62)
								) tc ';
					$query = 'SELECT sum(amount) amt FROM ' . $this->payment_table . ' WHERE order_item_id = ' . $single_item->id;
					//echo $query;
					$temp_paid = $wpdb->get_var( $query );
					
					if( !empty ( $temp_paid ))
					{
						$return_items[$single_item->id]->paid = $temp_paid; 
						$return_items[$single_item->id]->balance = $temp_items[$single_item->id]->price - $temp_paid; 
					}
					else
					{
						$return_items[$single_item->id]->paid = 0; 
						$return_items[$single_item->id]->balance = $temp_items[$single_item->id]->price; 
					}

					$return_items[$single_item->id]->item_verbiage = self::Fetch_ItemVerbiage( $single_item->item_type, $single_item->item_id, true ); 
					
					if( $single_item->add_on > 0 )
					{
						$original_id = $wpdb->get_var( "SELECT item_id, item_type FROM " . $this->item_table . " WHERE id = " . $single_item->add_on );
						$original_type = $wpdb->get_var( NULL, 1 );

						$return_items[$single_item->id]->origin_verbiage = self::Fetch_ItemVerbiage( $original_type, $original_id, true );
					}
					
					//
					//Add Add-ons if they are needed
					if( $AddOns == true )
					{
						$query = 'SELECT id FROM ' . $this->item_table . ' WHERE add_on = ' . $single_item->id;
						$query .= $MultipleOrders ? '' : ' AND order_id = ' . $single_item->order_id;
						$temp_adds = $wpdb->get_results ( $query, ARRAY_A );

						unset( $indexes );
						if( !empty( $temp_adds ))
						{
							foreach( $temp_adds as $cur_index => $id)
							{
								$indexes[] = $id['id']	;
							}
							$return_items = array_merge( $return_items, self::Fetch_Items ( $indexes ));
						}
					}
				}
			}

			self::Items( $return_items );
			return $return_items;
		}
		
		return array();
	}
	
	/**
	 * Moves an order item to a new item
	 *
	 * @param integer $original_order_item_id The original ID of the item
	 * @param integer $new_team_id The ID of the new team
	 * @param boolean $use_old_cost If true, use the old cost of the item
	 * @param float $cost The cost of the item
	 *
	 * @return integer The new item object
	 */
	public function move_item($original_order_item_id, $new_team_id, $use_old_cost = false, $cost = 0, $MigratePayments = false)
	{
		/** Gather original item information */
		$orig = self::Fetch_Items(array($original_order_item_id));
		
		if(isset($orig))
		{
			$original = current($orig);
		}

		$orig_user = $this->user;
		
		if(isset($original) && isset($original->quantity) && $original->quantity > 0)
		{
			self::set_order_id($original->order_id);
	
			/** Insert new item */
			$this->user = $original->created_by;
			$item_data = array('type' => $original->item_type,
							   'item_id' => $new_team_id,
							   'note' => ('Transfer from item ' . $original->id),
							   'add_on' => $original->add_on,
							   'quantity' => $original->quantity,
							   'price' => ($use_old_cost ? $original->price : $cost)
							  );

			$new_item = self::new_item($item_data);
			$items[$original->id] = $new_item;

			if( $MigratePayments )
			{
				foreach( self::payments( array($original->id), false ) as $payment_data)
				{
					$this->transaction_id = $payment_data['ind_id'];
					//
					//2018-05-06 DJT Removed -1 multiplier
					//self::payment($payment_data['order_item_id'], 
					//			  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']) * (-1), 2,'.','')
					//			  );
					self::payment($payment_data['order_item_id'], 
								  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']), 2,'.','')
								  );

					//
					//2018-05-06 DJT Removed -1 multiplier
					//2018-05-06 DJT Removed -1 multiplier
					//self::payment($new_item,
					//			  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']) * (-1), 2,'.',''),
					//			  $this->transaction_id,
					//			  $payment_data['note'],
					//			  $payment_data['special'],
					//			  $payment_data['created_by']
					//			 );
					self::payment($new_item,
								  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']), 2,'.',''),
								  $this->transaction_id,
								  $payment_data['note'],
								  $payment_data['special'],
								  $payment_data['created_by']
								 );
				}
			}
			
			/** Insert add-ons */
			global $wpdb;
			
			$adds = $wpdb->get_results("SELECT id, order_id, item_type, item_id, add_on, quantity, price, created_by
										FROM " . $this->item_table . " WHERE add_on = " . $original_order_item_id, ARRAY_A);
			
			foreach($adds as $index=>$data)
			{					
				$item_data = array('type' => $data['item_type'],
								   'order_id' => $data['order_id'],
								   'item_id' => (($data['item_type'] == 0) ? $new_team_id : $data['item_id']),
								   'note' => ('Transfer from item ' . $data['id']),
								   'add_on' => $new_item,
								   'quantity' => $data['quantity'],
								   'price' => $data['price']
								  );
				$this->user = $data['created_by'];
				$item_ins_id = self::new_item($item_data);
			
				$items[$data['id']] = $item_ins_id;

				if( $MigratePayments )
				{
					foreach( self::payments( array($data['id']), false ) as $payment_data)
					{

						$this->transaction_id = $payment_data['ind_id'];
						//
						//2018-05-06 DJT Removed -1 multiplier
						//self::payment($payment_data['order_item_id'], 
						//			  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']) * (-1), 2,'.','')
						//			  );
						self::payment($payment_data['order_item_id'], 
									  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']), 2,'.','')
									  );

						//
						//2018-05-06 DJT Removed -1 multiplier
						//self::payment($item_ins_id,
						//			  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']) * (-1), 2,'.',''),
						//			  $this->transaction_id,
						//			  $payment_data['note'],
						//			  $payment_data['special'],
						//			  $payment_data['created_by']
						//			  );
						self::payment($item_ins_id,
									  number_format(preg_replace('/[^\-0-9.]*/', '', $payment_data['amount']), 2,'.',''),
									  $this->transaction_id,
									  $payment_data['note'],
									  $payment_data['special'],
									  $payment_data['created_by']
									  );

					}
				}

			}
			$this->user = $orig_user;
			
			/** Remove original item(s) */
			foreach($items as $index => $trans_to)
			{
				self::cancel_item($index, 'Transfer to item ' . $trans_to . (($use_old_cost) ? ', Keep Cost, ' : ''));
			}
			return $new_item;
		}
		else
		{
			return array();
		}
	}

	/**
	 * Cancel the current item
	 *
	 * @param integer $item_id The ID of the item to be cancelled
	 * @param string $note A note about why the item was cancelled
	 */
	public function cancel_item($item_id, $note)
	{
		global $wpdb;
 
			$query = "UPDATE " . $this->item_table . " 
					 SET quantity = 0, note = CONCAT(note, '" . $note . "')
					 WHERE id = " . $item_id;
			$wpdb->query($query); 	
	}
	
	/**
	 * Gets the add-ons for an order item
	 *
	 * @param integer $order_item_id The id of the order item
	 *
	 * @return array Order item add-ons for the item requested
	 */
	public function add_ons($order_item_id)
	{
		global $wpdb;
		$adds = $wpdb->get_results("SELECT id, order_id, item_type, item_id, price, quantity 
									FROM " . $this->item_table . " 
									WHERE add_on = " . $order_item_id, ARRAY_A);
		return $adds;
	}
	
	
	public function fetch_OrderItems($Order_Id = 0, $Item_Type = 'All', $Items = 'All', $Payments = false, $Deposits = false)
	{
		global $wpdb;

		$items = array();
		$query = "SELECT id, order_id, item_type, item_id, add_on, note, quantity, price, created_by, created_when,
						IF(add_on = 0, id, add_on) as seq
					FROM " . $this->item_table . " 
					WHERE order_id = " . $Order_Id . 
					(($Item_Type == 'All') ? "" : " AND item_type = " . $Item_Type) . 
					(($Items == 'All') ? "" : " AND id in(" . implode(',', self::Items()) . ")") . "
					ORDER BY seq, id";
		$core = $wpdb->get_results($query, ARRAY_A);
		
		if(!empty($core) && isset($core[0]))
		{
			foreach($core as $index => $data)
			{
				$items[$data['id']] = $data;
				$item_list[] = $data['id'];
			}

			if($Payments)
			{
				//
				//Default all payments to 0 and balances to price.
				foreach($items as $id => $data)
				{
					$items[$id]['payment'] = 0;
					$items[$id]['balance'] = (float)($items[$id]['price']);

					//
					//Reduce balance due of main item if we have a CAP
					if(!empty($items[$id]['item_type']) && ($items[$id]['item_type'] == 61 || $items[$id]['item_type'] == 62))
					{
						$tmp_item_id = $items[$id]['add_on'];
						$cap_price = (float)$items[$id]['price'];
						$items[$tmp_item_id]['balance'] += $cap_price;
					}
				}
				
				//
				//Adjust prices based upon found balances.
				$query = "SELECT order_id, order_item_id, special, sum(amount) amt
							FROM " . $this->payment_table . "
							WHERE order_id = " . $Order_Id . " AND order_item_id in (" . implode(",", $item_list) . ")
							GROUP BY order_id, order_item_id";
				$pays = $wpdb->get_results($query, ARRAY_A);
				
				if(!empty($pays))
				{
					foreach($pays as $index => $data)
					{
						if(!empty($items[$data['order_item_id']]))	
						{
							$items[$data['order_item_id']]['special'] = $data['special'];
							$items[$data['order_item_id']]['payment'] = (float)($data['amt']);
							$balance = $items[$data['order_item_id']]['balance'] - $data['amt'];

							$items[$data['order_item_id']]['balance'] = (float)($balance);
						}
					}
				}
			}	//End if($Payments)

			if($Deposits)
			{
				//
				//Find all reservation based items
				foreach($items as $id => $data)
				{
					if($data['item_type'] == 1)
					{
						$rsvps[] = $data['item_id'];
					}
				}
				
				if(!empty($rsvps) && count($rsvps) > 0)
				{
					$query = "SELECT id, appfee, deposit 
								FROM " . $wpdb->prefix . "command_TEAM
								WHERE id in(" . implode(',', $rsvps) . ")";
					$tmps = $wpdb->get_results($query, ARRAY_A);
					
					if(!empty($tmps))
					{

						$deps = array();
						foreach($tmps as $index=>$data)
						{
							$deps[$data['id']] = $data;	
						}
						
						foreach($items as $id => $data)
						{
							if($data['item_type'] == 1)
							{
								if(!empty($deps[$data['item_id']]))	
								{
									$items[$id]['appfee'] = (float)($deps[$data['item_id']]['appfee']);
									$items[$id]['deposit'] = (float)($deps[$data['item_id']]['deposit']);
								}
							}
						}
					}
				}
			}	// End if($Deposits)
		}	//End if(!empty($core))

		return $items;
	
	}
		
	
	/***
	 *
	 * Fetches the language associated with a specific item type
	 *
	 * @param: required array $ItemType - the item type
	 *
	 * @return: string - the verbiage associated with the item type
	 *
	****/
	public function Fetch_ItemVerbiage( $ItemType, $ItemId = 0, $DirectID = 0 )
	{

		//
		//The list of item types
		switch( $ItemType )
		{
			case 0:

				$value = 'Application Fee';
				break;

			case 1:

				if( !empty( $ItemId ))
				{
					$team = new TEAM();
					$team->set_team( $ItemId );
					$value = $team->value('hash');
				}
				else
				{
					$value = 'Reservation';
				}
				break;
				
			case 4:
				
				$value = 'Add-On:';
				
				if( !empty( $ItemId ))
				{
					global $wpdb;
					
					if($DirectID)
					{
						$check = $ItemId;
					}
					else
					{
						$check = $wpdb->get_var($query = "SELECT item_id FROM " . $this->item_table . " WHERE id=" . $ItemId);
					}
					
					$tmp = get_post( $check );
					$value .= ' ' . $tmp->post_title;
					unset($tmp); 
				}

				break;
				
			case 61:
				
				$value = 'CAP Rebate';
				break;
				
			case 62:
				
				$value = 'Coupon';
				break;
				

			default:
				
				$value = 'Unknown';
				break;
		}
	
		return $value;
	
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Functions to deal with fixed number: Costs, Payments, Balance Due
	/////////////////////////////////////////////////////////////////////////

	/***
	 * Determine how much an order costs
	 *
	 * @param: optional boolean $RSVPOnly - only find the cost of the main Reservation
	 * @param: optional numeric OrderId - The order id of the order, if not passed, use internal variable
	 *
	 * @return: double - the balance due on the order
	 *
	****/
	public function Fetch_Cost($RSVPOnly = false, $OrderId = 0)
	{
		global $wpdb;
		$cost = NULL;
		
		//
		//Determine which order id to use
		if(func_num_args() > 1)
		{
			$order_id = $OrderId;
		}
		else
		{
			$order_id = self::OrderId();	
		}

		//
		//If we have an order id
		if(!empty( $order_id ))
		{
			$query = "SELECT sum(price * quantity) FROM " . $this->item_table . " WHERE order_id = " . $order_id . 
						(( $RSVPOnly == true ) ? " AND item_type = 1 AND add_on = 0" : "" );
			$cost = $wpdb->get_var( $query );
		}

		return $cost;
	}

	/***
	 * Determine how much has bene made in payments
	 *
	 * @param: optional boolean $RSVPOnly - only find the amount paid for the main Reservation
	 * @param: optional numeric OrderId - The order id of the order, if not passed, use internal variable
	 *
	 * @return: double - the total amount of the payments
	 *
	****/
	public function Fetch_Paid($RSVPOnly = false, $OrderId = 0)
	{
		global $wpdb;
		$paid = NULL;
		
		//
		//Determine which order id to use
		if(func_num_args() > 1)
		{
			$order_id = $OrderId;
		}
		else
		{
			$order_id = self::OrderId();	
		}

		//
		//If we have an order id
		if(!empty( $order_id ))
		{
			$query = "SELECT sum(pay.amount) 
						FROM " . $this->payment_table . " pay
						INNER JOIN " . $this->item_table . " items ON pay.order_item_id = items.id
						WHERE items.order_id = " . $order_id . "
							AND items.quantity > 0 ";
			
			if( $RSVPOnly == true )
			{
				$query .= " AND pay.order_id = " . $order_id;
				$query .= " AND items.item_type = 1 AND items.add_on = 0";
			}

			$paid = $wpdb->get_var( $query );
		}

		return $paid;
	}

	/***
	 * Determine how much is left on an Order
	 *
	 * @param: optional boolean $RSVPOnly - only find the balance due for the main Reservation
	 * @param: optional numeric OrderId - The order id of the order, if not passed, use internal variable
	 *
	 * @return: double - the balance due on the order
	 *
	****/
	public function Fetch_Balance($RSVPOnly = false, $OrderId = 0)
	{
		global $wpdb;
		$balance = NULL;
		
		//
		//Determine which order id to use
		if(func_num_args() > 1)
		{
			$order_id = $OrderId;
		}
		else
		{
			$order_id = self::OrderId();	
		}

		//
		//If we have an order id
		if(!empty($order_id))
		{
			$base_cost = self::Fetch_Cost( $RSVPOnly, $order_id );
			$base_payment = self::Fetch_Paid( $RSVPOnly, $order_id );

			if(!empty( $base_cost ) && !empty( $base_payment ))
			{
				$balance = ( $base_cost - $base_payment );
			}
		}

		return $balance;
	}

	
	/***
	 * Determine how much the currently selected Items cost
	 *
	 * @return: number - the total cost of the items
	 *
	****/
	public function Calculate_ItemsCost()
	{
		$cost = 0;

		if( !empty( self::Items() ))
		{
			foreach( self::Items() as $id => $data )
			{
				$cost += ($data->quantity * $data->price);
			}
		}
	
		return $cost;
	}

	/***
	 * Determine how much has currently been paid toward the selected items
	 *
	 * @return: number - the total amount of the payments
	 *
	****/
	public function Calculate_ItemsPaid()
	{
		$paid = 0;
		
		if( !empty( self::Items() ))
		{
			foreach( self::Items() as $id => $data )
			{
				$paid += $data->paid;
			}
		}
	
		return $paid;
	}

	/***
	 * Determine the balance due for the selected items
	 *
	 * @return: number - the balance due on the items
	 *
	****/
	public function Calculate_ItemsBalance()
	{
		$balance = 0;
		
		if( !empty( self::Items() ))
		{
			foreach( self::Items() as $id => $data )
			{
				$balance += $data->balance;
			}
		}
	
		return $balance;
	}

	//////////////////////////////////////////////////////////////////////////
	// Functions to deal with Payments
	/////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the payments for identified order items
	 *
	 * @param array $order_item_ids The id's of requested order items
	 * @param boolean $collapse Changes return to sum amounts based upon original transaction id
	 *
	 * @return array Array of order payments for order_item_ids OR an empty array
	 */
	public function payments($order_item_ids = array(), $collapse = false, $special = NULL)
	{
		if(!empty($order_item_ids))
		{
	
			global $wpdb;
			if($collapse)
			{
				$query = "SELECT min(id) id, min(ind_id) ind_id, order_item_id, sum(amount) amount, max(created_when) created_when 
						  FROM " . $this->payment_table . " 
							WHERE order_item_id in (" . implode(",", $order_item_ids) . ")" . 
							((!empty($special)) ? " AND special='" . $special . "'" : "") . "
							GROUP BY order_item_id";
				//echo $query;
			}
			else
			{
				$query = "SELECT id, ind_id, order_id, order_item_id, transaction_id, special, 
							   amount, note, created_when, created_by 
						  FROM " . $this->payment_table . " 
						  WHERE order_item_id in (" . implode(",", $order_item_ids) . ")" . 
						  ((!empty($special)) ? " AND special='" . $special . "'" : "") . "
						  ORDER BY id";
			}

			$paym = $wpdb->get_results($query, ARRAY_A);
			return $paym;
		}
		else
		{
			return array();
		}
	}
	
	/**
	 * Returns the payments for identified id
	 *
	 * @param array $payment_ids The id's of requested order items
	 *
	 * @return array Array of payments for PRIMARY KEYS (id) OR an empty array
	 */
	public function payments_by_id($payment_ids = array(), $type = NULL)
	{
		if(!empty($payment_ids)) 
		{
			global $wpdb;
			
			$query = "SELECT id, ind_id, order_id, order_item_id, transaction_id, special, note, amount, created_when, created_by 
					  FROM " . $this->payment_table . " 
					  WHERE id in (" . implode(",", $payment_ids) . ")";
			$paym = $wpdb->get_results($query, ARRAY_A);
			return $paym[0];
		}
		else
		{
			return array();
		}
	}
	
	/**
	 * Moves a payment from one RSVP to another RSVP
	 *
	 * @param array $original_rsvp The original RSVP data, needed for order_item_id
	 * @param array $new_rsvp The new RSVP data
	 *
	 * @return null
	 */
	public function move_payments($original_rsvp, $new_rsvp)
	{
		
		$mod_payments = array();
		$oids = array($original_rsvp['order_item_id']);

		foreach(self::add_ons($original_rsvp['order_item_id']) as $data)
		{
			array_push($oids, $data['id']);
		}
		
		$original_payments = self::payments( $oids );

		foreach($original_payments as $index => $data)
		{
			if(empty($mod_payments[$data['ind_id']][$data['order_item_id']]))
			{
				$mod_payments[$data['ind_id']][$data['order_item_id']] = array('id' => $data['id'],
													   'order_id' => $data['order_id'], 
														'order_item_id' => $data['order_item_id'], 
														'transaction_id' => $data['transaction_id'],
														'amount' => $data['amount']);
			}
			else
			{
				$mod_payments[$data['ind_id']][$data['order_item_id']]['amount'] += $data['amount'];				
			}
		}

		//
		//Swap
		foreach($mod_payments as $ind_id => $oids)
		{
			foreach($oids as $oid => $payment_data)
			{
				$this->order_id = $payment_data['order_id'];
				$this->transaction_id = $payment_data['transaction_id'];
				
				self::payment($payment_data['order_item_id'], 
							  number_format($payment_data['amount'],2) * (-1),
							  $ind_id
							  );
	
				self::payment($new_rsvp['order_item_id'],
							  number_format($payment_data['amount'],2),
							  $ind_id
							  );
			}
		}
	}
	

	/**
	 * Returns data about requested order_item_ids
	 *
	 * array $order_item_ids - the ids of the requested items
	 * boolean $strip_ids - if true, remove the id's
	 * 
	 * array $paym - the info for the requested items OR
	 * array - empty array
	 */
	public function items_cost($order_item_ids = array(), $strip_ids = false)
	{
		if(!empty($order_item_ids))
		{
			global $wpdb;
			$query = "SELECT id, item_type, item_id, add_on, price, note, created_when 
										FROM " . $this->item_table . " 
										WHERE id in (" . implode(",", $order_item_ids) . ")";
			//echo $query;
			$paym = $wpdb->get_results($query, ARRAY_A);

			if($strip_ids)
			{
				foreach($paym as $index=>$data)
				{
					$tmp[$data['id']] = $data;
				}
				
				$paym = $tmp;
			}

			return $paym;
		}
		else
		{	
			return array();
		}
	}

		
	
	//////////////////////////////////////////////////////////////////////////
	// Functions to deal with Items within an Order
	/////////////////////////////////////////////////////////////////////////

	
	
	/**
	 *Functions to deal with Transactions
	 */

	/**
	 * Set the transaction id
	 * @param integer $id The id of the transaction
	 */
	public function set_trans_id($id = 0)
	{
		$this->transaction_id = $id;
	}
	
	/**
	 * Get the transaction id
	 */
	public function get_trans_id()
	{
		return $this->transaction_id;
	}
	
	/**
	 * Retrieves all the transaction data
	 *
	 * @param  array $transact_ids The ids the transactions
	 */
	public function get_transaction($transact_ids = array())
	{
		global $wpdb;
		$query = "SELECT id, ind, method, action, form, transaction_id, amount, response, raw_request, created_when, created_by 
										FROM " . $this->transact_table . " 
										WHERE id in (" . implode(",", $transact_ids) . ")";
		//echo $query;
		$paym = $wpdb->get_results($query, ARRAY_A);
		return $paym[0];

	}


	/**
	 * Inserts new transaction into the database, sets the transaction id, and
	 * sets the transaction info
	 * @param  array $details The details for the transaction
	 */
	public function new_transaction($details = array(), $Method = '', $Action = '')
	{
		global $wpdb;
		$raw_replace = array();

		if(isset($details['raw_request']))
		{
			$elements = explode('&', $details['raw_request']);
			
			$pwd_key = array_search('PWD[10]=PayPal4485', $elements);
			
			if(!empty($elements[$pwd_key]))
			{
				unset($elements[$pwd_key]);
			}
			
			foreach($elements as $element)
			{
				unset($index); unset($value);
				@list($index, $value) = @explode('=', $element);
				
				if(isset($index) && strlen($index) >= 4)
				{
					$pos_brace = strrpos($index, '[');
					
					if($pos_brace > 0)
					{
						$index = substr($index, 0, $pos_brace);
					}
					
					switch($index)
					{
						case 'PWD':
							break;
						case 'ACCT':
							$value = str_repeat('*', strlen($value) - 4) . substr($value, -4);
							$raw_replace[$index] = $value;
							break;
						default:
							$raw_replace[$index] = $value;
							
					}
				}
			}

			$details['raw_request'] = serialize($raw_replace);
		}



		//
		//2018-02-22 DJT Find user agent id
		$agent_lookup_query = "SELECT id FROM " . $wpdb->prefix . "command_user_agent WHERE hash='" . md5($details['user_agent']) . "'";
		$check_agent = $wpdb->get_var($agent_lookup_query);
		
		if(empty($check_agent))
		{
			$wpdb->insert($wpdb->prefix . "command_user_agent",
						  array('hash' => md5($details['user_agent']),
							    'user_agent' => $details['user_agent']),
						  array('%s', '%s')
						 );
			$user_agent_id = $wpdb->insert_id;
		}
		else
		{
			$user_agent_id = $check_agent;
		}
		
		//
		//2018-03-24 DJT Added Amount to make sure formatting is right
		$amount = number_format(preg_replace('/[^\-0-9.]*/', '', $details['amount']), 2, '.', '');
		//
		//2018-02-22 Added lines for ip_address, user_agent, and pp_trans_id
		$wpdb->insert($this->transact_table,
					  array('method' => $Method,
							'action' => $Action,
							'amount' => $amount,
					  		'form' => $details['form'],
							'response' => $details['response'],
							'transaction_id' => $details['transaction_id'],
							'ip_address' => $details['ip_address'],
							'user_agent' => $user_agent_id,
							'pp_trans_id' => $details['pp_trans_id'],
							'raw_request' => addslashes($details['raw_request']),
							'raw_response' => addslashes($details['raw_response']),
							'created_by' => $this->user,
							'created_when' => NULL
						    ),
					  array('%s', '%s', '%f', '%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s', '%d')
					   	);
		
		self::set_trans_id($wpdb->insert_id);
		self::funds_set($details['amount']);

		//
		//Set the indicator
		if(isset($details['ind']) && $details['ind'] > 0)
		{
			$wpdb->update($this->transact_table, array('ind' => $details['ind']), array('id' => self::get_trans_id()));
		} 
		else 
		{
			$wpdb->update($this->transact_table, array('ind' => self::get_trans_id()), array('id' => self::get_trans_id()));
		}

	}

	public function update_transaction($details = array())
	{
		global $wpdb;

		// TODO: update transact_table



	}

	/**
	 * Set the transaction funds
	 *
	 * @param float $amount The amount for the transaction
	 */
	private function funds_set($amount = 0)
	{
		$this->transaction_amount = $amount;
		$this->transaction_remaining = $amount;
	}

	/**
	 * Returns the amount remaining in the transaction
	 *
	 * @return float The amount remaining in the transaction
	 */
	private function funds_remaining()
	{
		return $this->transaction_remaining;
	}

	/**
	 * Subract the amount from the transaction_remaining amount
	 *
	 * @param float $amount The amount to subtract 
	 *
	 * @return float The amount to be subtracted OR he difference between the
	 * amount and the remaining balance of the transaction (if the $amount is >
	 * the funds remaining)
	 */
	private function funds_remove($amount = 0)
	{
		if($amount <= self::funds_remaining())
		{
			$this->transaction_remaining .- $amount;	
			return $amount;
		}
		else
		{
			$dif = $amount - $this->transaction_remaining;
			$this->transaction_remaining = 0;
			return $dif;
		}
	}

	/**
	 * Returns the type of payment
	 * 
	 * @param string Name of the payment type
	 *
	 * @return integer Number key of the payment type
	 */
	public function get_payment_type($payment_string)
	{
		return array_keys($this->payment_types, $payment_string);
	}

	/**
	 * Returns the invoice
	 * 
	 * @param integer team the team number
	 * @param array rsvp array of reservations to include on the invoice
	 *
	 * @return string HTML of the invoice
	 */
	public function team_block($team_id = 0)
	{

		$html = '';

		if($team_id > 0)
		{
			$team = new TEAM();
			$country = new COUNTRY();

			$team->set_team($team_id);
			$country_list = $country->country_list($team->schedule_countries());
 
			$html .= '    <b>Team: ' . $team->value('hash') . '</b><br />';
			$html .= '	  <b>Country:</b> ' . $country_list . '<br />';

			$html .= '    <div class="row collapse">';
			$html .= '      <div class="medium-6 columns">';
			$html .= '        <b>Arrival Date:</b> ' . $team->arrival_date() . '<br />';
			$html .= '		  <b>Departure Date:</b> ' . $team->departure_date() . '<br />';
			$html .= '		  <b>Duration:</b> ' . $team->trip_duration() . ' days<br />';
			$html .= '      </div>';

			$html .= '		<div class="medium-6 columns">';
			$html .= '		  <b>Arrival City:</b> ' . $team->value('arrival_city') . '<br />';
			$html .= '		  <b>Departure City:</b> ' . $team->value('departure_city') . '<br />';
			$html .= '		  <b>Cost:</b> $' . $team->trip_cost() . '<br />';
			$html .= '		</div>';
			$html .= '	  </div>';
		}

		return $html;

	}

	//
	//2018-03-23 DJT Added NoUserLink to make sure an RSVP is not assigned to a user
	//
	public function move_CartToOrder( $Cart , $Override_DateUses = false, $NoUserLink = false)
	{
		//
		//set necessary classes
		global $wpdb;

		$rsvp = new RSVP();
		$rsvp->change_user($this->user);
		//$cart = new CART();
		
		//
		//Validate cart, start a new order
		$Cart->validate($Override_DateUses);
		//
		//2018-03-22 DJT Changed from new_order() to Create_Order()
		//self::new_order();
		self::Create_Order();

		$work_cart = $Cart->Processed();
		
		//
		//Cycle through all the main items in the working cart...
		foreach(  $Cart->Processed() as $main_item_id => $item_types )
		{

			//
			//Cycle through each of the item types in the cart...			
			foreach( $item_types as $item_type => $items )
			{
				
				//
				//For each items
				foreach( $items as $item_id => $item_data )
				{
				
					//
					//Is this main item?  If so, we can write it, otherwise we move on...
					if($item_data['main'] == 1)
					{
					
						//
						//Everything is still one-by-one, so break out any quantity
						for($cnt = 1; $cnt <= (int)$item_data['quantity']; $cnt++)
						{
							//
							//If we still have one in our working cart, then we can move on...
							if( isset( $work_cart[$main_item_id][$item_type][$item_id]['quantity'] ) && 
							    $work_cart[$main_item_id][$item_type][$item_id]['quantity'] > 0
							  )
							{
								//
								//Build main item to write to db...
								$this_item_data = array('type' => $item_data['item_type_id'],
														'item_id' => $item_id,
														'note' => '',
														'add_on' => (( $main_item_id != $item_id ) ? $main_item_id : 0 ),
												   		'quantity' => 1,
														'price' => $item_data['cost']
												  	);
								$item_ins_id = self::new_item($this_item_data);

								
								//
								//RSVP
								if( $item_type == 'reservation' )
								{
									$rsvp_for = ( $cnt == 1 ? $this->user : 0 );
									
									//
									//2018-03-23 DJT Added NoUserLink to force the skipping of a RSVP assignment
									//
									//See an an RSVP already exists for this user and team then create new RSVP
									$cur_rsvp = $rsvp->order_id_by_team( $item_id, $rsvp_for );
									if( ( $rsvp_for > 0 && $cur_rsvp) || $NoUserLink )
									{
										$rsvp_for = 0;
									}
									$rsvp->new_rsvp( $item_id, $rsvp_for, $item_ins_id );
								}

								
								//
								//Set additional items that should be added to thie main item...
								$tmp_cart = $work_cart[$main_item_id];
								foreach( $tmp_cart as $a_item_type => $a_items )
								{
									//
									//Dont work on the item type we're working on...
									if( $a_item_type != $item_type )
									{
										//
										//Cycle through each item in the item type...
										foreach( $a_items as $a_item_id => $a_item_data )
										{
											//
											//Build main item to write to db...
											$this_item_data = array('type' => $a_item_data['item_type_id'],
																   'item_id' => $a_item_id,
																   'note' => '',
																   'add_on' => $item_ins_id,
																   'quantity' => 1,
																   'price' => $a_item_data['cost']
																   );
											$ins_id = self::new_item($this_item_data);

											//
											//Decrease count of the item...
											$work_cart[$main_item_id][$a_item_type][$a_item_id]['quantity']--;

											//
											//Check for a 0 quantity and remove if quantity is 0
											if( $work_cart[$main_item_id][$a_item_type][$a_item_id]['quantity'] <= 0)
											{
												unset( $work_cart[$main_item_id][$a_item_type][$a_item_id] );	
											}

										}	//End write of the individual item

									}	//End check to make sure we're not doing the same item

								}	//End the list of individual items that can be added to the RSVP


								//
								//Decrease count of the item...
								$work_cart[$main_item_id][$item_type][$item_id]['quantity']--;
								//
								//Check for a 0 quantity and remove if quantity is 0
								if( (int)$work_cart[$main_item_id][$item_type][$item_id]['quantity'] <= 0)
								{
									unset( $work_cart[$main_item_id][$item_type][$item_id] );	
								}

							}	//End check to see if there's still an item with quantity
							
						}	//End for-loop that cycles through each quantity of items
						
					}	//Enc check to see if this is a main item
					
				}	//End the individual item
				
			}	//End the item types
			
		}	//End the cart->Processed cycle
		
		//echo self::OrderId();
	}

	//
	//2018-03-23 DJT Added $AllowZeroPay to force sign-ups and blank users for CFL
	//
	public function apply_payments($Method, $Action, $PaymentAmount, $Items = 'All', $Message = '', $ForceDate = NULL, $AllowZeroPay = false)
	{
		//
		//Method - balance, deposit, defined 
		//
		//Set core variables

		$items = self::fetch_OrderItems(self::OrderId(), 'All', $Items, true, true);
		$checks = &$items;
		$passes = 3;
		$funds = (float) $PaymentAmount;

		$special = '';
		if($Action == 'donate')
		{
			$special = 'donate';	
		}
		if($Action == 'anonymous')
		{
			$special = 'donate anyonymous';	
		}
			

		//
		//Make 4 passes, checking each item to apply funds in proper order
		while ($passes > 0 && count($checks) > 0)
		{
			foreach($items as $item_id => $data)
			{
				//
				//Check tghe item to see if there's any further payment we can apply to it
				//$itm_check = self::Fetch_Items( array($item_id), false, true );
				//if($itm_check[$item_id]->balance > 0)
				//{
				//
				//2018-03-23 DJT Added $AllowZeroPay to each data check and self::payment
					//
					//Make payments to application fees first
					if($passes == 3 && $data['item_type'] == 0)
					{
						if((!empty($data['balance']) && $data['balance'] > 0) || $AllowZeroPay)
						{
							$payoff = min($funds, $data['balance']);
							self::payment($item_id, $payoff, 0, $Message, $special, NULL, $ForceDate, $AllowZeroPay);	
							$funds -= $payoff;
							unset($checks[$item_id]);
						}
					}

					//
					//Make payments to RSVPs second
					if($passes == 2 && $data['item_type'] == 1 && $data['add_on'] == 0)
					{
						if((!empty($data['balance']) && $data['balance'] > 0) || $AllowZeroPay)
						{
							if($Method == "deposit")
							{
								$payoff = $data['deposit'];
							}
							else
							{
								$payoff = min($funds, $data['balance']);
							}
							self::payment($item_id, $payoff, 0, $Message, $special, NULL, $ForceDate, $AllowZeroPay);	
							$funds -= $payoff;
							unset($checks[$item_id]);
						}

					}

					//
					// All non-RSVP items that are not add-ons
					if($passes == 1 && $data['item_type'] > 1 && $data['add_on'] == 0)
					{
						if((!empty($data['balance']) && $data['balance'] > 0) || $AllowZeroPay)
						{
							$payoff = min($funds, $data['balance']);
							self::payment($item_id, $payoff, 0, $Message, $special, NULL, $ForceDate, $AllowZeroPay);	
							$funds -= $payoff;
							unset($checks[$item_id]);
						}

					}

					//
					// All non-RSVP items add-ons
					if($passes == 1 && $data['item_type'] > 1 && $data['add_on'] > 0)
					{
						if((!empty($data['balance']) && $data['balance'] > 0) || $AllowZeroPay)
						{
							$payoff = min($funds, $data['balance']);
							//echo $payoff . "<BR><BR>";
							self::payment($item_id, $payoff, 0, $Message, $special, NULL, $ForceDate, $AllowZeroPay);
							$funds -= $payoff;
							unset($checks[$item_id]);
						}

						if($data['item_type'] == 61)
						{
							self::payment($item_id, 0, 0, $Message, $special, NULL, $ForceDate, $AllowZeroPay);
							unset($checks[$item_id]);
						}

					}
				//}
			}	//End foreach

			$passes --;

		}	//End while($passes)

		self::CurrentFunds($funds);
	}
	
	/**
	 * Returns the invoice
	 * 
	 * @param integer team the team number
	 * @param array rsvp array of reservations to include on the invoice
	 *
	 * @return string HTML of the invoice
	 */
	public function get_invoice($team_id = 0, $rsvps = array(), $Terms = 'Due upon Receipt', $InvoiceFor = '')
	{
		//
		//Set basic return and cost variables.
		$html = '';
		$rsvp_html = '';
		$pay_html = '';

		//
		//Set global amounts
		$grand_total = 0;
		$pays = 0;

		if(!empty($rsvps) && count($rsvps) > 0 && $team_id > 0)
		{
			$team = new TEAM();
			$country = new COUNTRY();

			$team->set_team($team_id);
			$country_list = $country->country_list($team->schedule_countries());
 			$arrival = DateTime::createFromFormat('m/d/Y', $team->arrival_date());
			$arrival->modify('-30 days');
			
			//
			//Start HTML
			//2018-03-29 GKR changed formatting
			$html .= '<div id="inner-content" class="row">';
			$html .= '  <main id="main" class="large-8 medium-8 columns" role="main">';
			$html .= '	<H1>';
			//
			//2018-03-23 DJT Changed Header
			$html .= '      <div style="display:inline-block; line-height:1; padding: 10px 0; background-color: #2B5990; min-width: 870px;">';
			$html .= '        <div style="float:left;margin-left:20px;color:white;">';
			$html .= '          INTERNATIONAL<BR>SERVICE<BR>LEARNING<BR><span style="font-size:70%">TRAVEL &middot; SERVE &middot; LEARN</span>';
			$html .= '        </div>';
			$html .= '        <div style="float:right;margin-right:20px;color:white; ">';
			$html .= '          INVOICE<BR><span style="font-size:70%; color: #fff;">PO Box 60925<BR>Corpus Christi, TX 78466<BR>361-814-4926</span>';
			$html .= '        </div>';
			$html .= '      </div>';
			//$html .= '    Unified Invoice for ' . $team->value('hash');
			$html .= '	</H1>';
			


			//
			//2018-03-23 New Team Block and Invoice options
			$html .= '      <div style="display:inline-block; line-height:1.4; min-width:850px;font-size:120%;">';
			$html .= '        <div style="float:left;">';
			$html .= '          Invoice For: &nbsp;';
			$html .= '        </div>';
			$html .= '        <div style="display:inline-block;">';
			$html .= '          ' . $InvoiceFor;
			$html .= '        </div>';
			$html .= '        <div style="float:right;">';
			$html .= '          ' . $team->value('hash') . '<BR>';
			$html .= '          ' . date('m/d/Y') . '<BR>';
			$html .= '          ' . $arrival->format('m/d/Y') . '<BR>';
			$html .= '			<b>';
			$html .= '          ' . $Terms;
			$html .= '			</b>';
			$html .= '        </div>';
			$html .= '        <div style="float:right">';
			$html .= '          Team ID: <BR>';
			$html .= '          Issue Date: <BR>';
			$html .= '          Due Date: <BR>';
			$html .= '          <b>Terms: </b><BR>';
			$html .= '        </div>';
			$html .= '      </div>';
			//$html .= self::team_block($team_id);
			
//			$html .= '	  <hr>';           
			$html .= '    <table style="width: 850px;border: hidden;border-top: solid;border-bottom:solid;margin-top:8px;margin-bottom:8px">';
			$html .= '		<TR>';
			$html .= '		  <TH style="width: 200px; text-align: left;">Item</TH>';
			$html .= '		  <TH style="width: 600px; text-align: right;">Cost</TH>';
			$html .= '		</TR>';
			
			//
			//Cycle through all the Resrvations
			foreach($rsvps as $rsvp_id)
			{
				$all_items = array();
				
				$rsvp = new RSVP();
				$rsvp->set_rsvp_id($rsvp_id);
				$rsvp->fetch_rsvp();
				
				$reservation = $rsvp->get_rsvp();
				$user = get_userdata($reservation['user_id']);
				$item = self::fetch_item($reservation['order_item_id']);
				$add_ons = self::add_ons($item['id']);
				$all_items[] = $reservation['order_item_id'];
	
				//
				//Get the user information if there is a real user
				if(!empty($reservation['user_id']))
				{
					$user = get_userdata($reservation['user_id']);
					$user_name = $user->last_name . ', ' . $user->first_name;
				}
				else
				{
					$user_name = "Unassigned";
				}

				//
				//Prep write out block data for the core RSVP
				$rsvp_html .= '<tr>';
            	$rsvp_html .= '  <td>RSVP (' . $rsvp_id . '): ' . $user_name . '</td>';
                $rsvp_html .= '  <td style="text-align: right;">$' . number_format($item['price'] * $item['quantity'],2) . '</td>';
                $rsvp_html .= '</tr>';
                
				$added_cost = 0;
				$app_fee = 0;
				foreach($add_ons as $index => $itm)
				{
					$all_items[] = $itm['id'];
					
					//
					//Pre write out block of non-core RSVP data
					$rsvp_html .= '<tr>';
					$rsvp_html .= '  <td>';
					
					/*
					switch($itm['item_type'])
						{
							case 0:
								$rsvp_html .= 'Application Fee';
								break;
							case 4:
								$tmp = get_post($itm['item_id'], ARRAY_A);
								//echo var_dump($tmp);
								
								$rsvp_html .= "Add-on: " . $tmp['post_title'];
								
								unset($tmp);
								break;
							case 61:
								$rsvp_html .= 'CAP Rebate';
								break;
							default:
								$rsvp_html .= 'Other';
								break;
						}
					*/
					$rsvp_html .= self::Fetch_ItemVerbiage($itm['item_type'], $itm['item_id']);
					
					$rsvp_html .= '  </td>';
					$rsvp_html .= '  <td align="right">$' . number_format($itm['price'] * $itm['quantity'],2) . '</td>';
					$rsvp_html .= '</tr>';
						
					$app_fee += (($itm['item_type'] ==0) ? ($itm['price'] * $itm['quantity']) : 0);
					$added_cost += ($itm['price'] * $itm['quantity']);
				}

				$grand_total += floatval(($item['price'] * $item['quantity']) + $added_cost);
				
				$costs = self::items_cost($all_items, true);
				foreach(self::payments($all_items) as $payments)
				{
					if(intval($payments['amount']) != 0)
					{
						$item = '';
						$override_item = false;

						if(!empty($payments['special']) && ($payments['special'] == 'donate refund' || $payments['special'] == 'donate' || $payments['special'] == 'donate anonymous'))
						{
							$item = "Donation";
							$override_item = true;
						}
						if(empty($payments['special']) && !empty($payments['note']))
						{
							$item = $payments['note'];
							$override_item = true;
						}
						if(!empty($payments['special']) && $payments['special'] == 'fee' && !empty($payments['note']))
						{
							$item = $payments['note'];
							$override_item = true;
						}
						
						
						if(!$override_item)
						{
							$item = self::Fetch_ItemVerbiage($costs[$payments['order_item_id']]['item_type']);
						}
						
						if(floatval($payments['amount']) < 0 && substr($payments['note'],0,4) != 'Spcl')
						{
							//$item = "Refund for " . $item;
							$item = $item;
						}

						
						$payed_by = get_user_by("ID", $payments['created_by']);
						$whom = $payed_by->first_name . " " . $payed_by->last_name;

						$pay_html .= "<tr><td>" . $payments['created_when'] . "</td>
								  <td>" . $item . "</td>
								  <td>" . $whom . "</td>	
								  <td align='right'>$" . number_format($payments['amount'] * (-1),2) . "</td></tr>";
						$pays += ($payments['amount'] * (-1));	
					}
				}

			}

			//
			//Add rsvp HTML to the main output
			$html .= $rsvp_html;
			
			$html .= '      <tr>';
			$html .= '		  <td><b>GRAND TOTAL</b></td>';
			$html .= '		  <td align="right"><b>$' . number_format($grand_total,2) . '</b></td>';
			$html .= '      </tr>';
			$html .= '	  </table>';
			
			//$html .= '    <hr>'; 
			$html .= '	  <br />';          

			$html .= '    <table style="border: hidden; width: 850px;">';                     
			$html .= '	    <TR>';
			$html .= ' 		  <TH style="width: 200px; text-align: left;">Payment Date</TH>';
			$html .= '  	  <TH style="width: 200px; text-align: left;">For</TH>';
			$html .= '		  <TH style="width: 200px; text-align: left;">By</TH>';
			$html .= '		  <TH style="width: 200px; text-align: right;">Amount</TH>';
			$html .= '		</TR>';


			//
			//Add payment HTML to the main output.
			$html .= $pay_html;
			
			$html .= '    <TR>';
			$html .= '      <TD><B>TOTAL PAYMENTS</B></TD>';
			$html .= '		<TD></TD><TD></TD>';
			$html .= '      <TD style="text-align: right"><B>$' . number_format($pays,2) . '</B></TD>';
			$html .= '    </TR>';
			 
			$html .= '	 <TR><TD colspan="4"><HR></TD>';          				
			$html .= '   <TR>';
			$html .= '     <TD></TD><TD></TD><TD></TD>';
			$html .= '	   <TD style="text-align: right;">';
			$html .= '       <b>Balance Due</b><BR> $' . number_format($grand_total + $pays,2);
			$html .= '	   </TD>';
			$html .= '    </TR>';
			$html .= '	 </table>';

			
		}

			//
			//2018-03-23 New Team Block and Invoice options
			$html .= '      <div style="display:inline-block; line-height:1.2; width:850px;margin-top:10px;">';
			$html .= '        <div style="float:left;margin-top: 30px;">';
			$html .= 'Payment must be received in U.S. dollars on or before the date indicated above. ';
			$html .= 'Late payments will be subject to a $50.00 fee. Please contact Peggy Neat at (361) 814-9296 or accounting@ISLonline.org with any questions.';
			$html .= '        </div>';
			$html .= '      </div>';
		$html .= '<P>';
		$html .= '</P>';
		$html .= '  </main>';
		$html .= '</div>';
		
		return $html;

	}

	public function transactions_by_user_new( $UserID = 0 )
	{
		
		global $wpdb;
		$item_list = array();
		
		//
		// Transactionss
		$query = "SELECT ot.id, ot.method, ot.action, ot.amount
				  FROM " . $this->transact_table . " ot
				  		INNER JOIN " . $this->payment_table . " pt on ot.id = pt.transaction_id
						INNER JOIN " . $this->item_table . " it on pt.order_item_id = it.id
				  WHERE ot.created_by = " . $UserID;
		$items_query = $wpdb->get_results($query, OBJECT_K);
		
		if( !empty ($items_query ) )
		{
				$item_list[$item_id] = array('type' => 'Order: ' . $item_id, 
											 'item' => $item_id,
											 'transact' => $transact, 
											 'total' => $item_data->total, 
											 'payment' => $payment,
											 'team' => $team,
											 'hash' => $hash);	
			
		}

		asort($item_list);
		return $item_list;
		
	}
	
	public function transactions_by_user($user_id = 0)
	{
		//
		// Orders are...
		// 1) Any item, paid for or not
		// 1.5) Payments
		// 2) A payment towards someone else's items
		// 3) Payment to a transferred RSVP
		global $wpdb;
		$item_list = array();
		
		//
		// Item Type 1
		$query = "SELECT ot.id, sum(price * quantity) total
					FROM " . $this->order_table . " ot 
						INNER JOIN " . $this->item_table . " it ON ot.id = it.order_id
					WHERE
						ot.created_by = " . $user_id . " AND
						it.quantity > 0
					GROUP BY 
						ot.id
					ORDER BY
						ot.id";
		$items_query = $wpdb->get_results($query, OBJECT_K);

		if(!empty($items_query))
		{
			foreach($items_query as $item_id => $item_data)
			{
				//
				//Get main item
				$item_query = "SELECT it.item_id, t.hash, t.no_finances 
								FROM " . $this->item_table . " it INNER JOIN
									" . $wpdb->prefix . "command_TEAM t on it.item_id = t.id
									WHERE item_type = 1 AND it.quantity > 0 AND order_id = " . $item_id;
				//echo $item_query;
				$main = $wpdb->get_results($item_query);
				$skip = 0;

				if(!empty($main))
				{
					$team = $main[0]->item_id;
					$hash = $main[0]->hash;
					$skip = $main[0]->no_finances;

				}
				else
				{
					//
					//Here, we are likely a follow=up payment...
					$add_query = "SELECT add_on FROM " . $this->item_table . " WHERE quantity > 0 AND order_id = " . $item_id;
					$add_id = $wpdb->get_var($add_query);
					
					if(!empty($add_id))
					{
						$item_query = "SELECT it.item_id, t.hash 
										FROM " . $this->item_table . " it INNER JOIN
											" . $wpdb->prefix . "command_TEAM t on it.item_id = t.id
											WHERE item_type = 1 AND quantity > 0 AND it.id = " . $add_id;
						$main = $wpdb->get_results($item_query);

						if(!empty($main))
						{
							$team = $main[0]->item_id;
							$hash = $main[0]->hash;
						}
						else
						{
							$team = ''; $hash = '';
						}

					}
					else
					{
						$team = ''; $hash = '';
					}
				}
				
				if($skip != 1)
				{
					$pay_query = "SELECT pt.ind_id, tt.method, tt.action, sum(pt.amount) payment 
								FROM " . $this->payment_table . " pt
									inner join " . $this->transact_table . " tt on pt.transaction_id = tt.id
								WHERE pt.order_id = " . $item_id . "
								GROUP BY ind_id, method, action";

					$pay_data = $wpdb->get_results($pay_query);

					foreach($pay_data as $trans_data)
					{
						$item_list[$trans_data->ind_id] = array('type' => 'Order', 
																'method' => $trans_data->method,
																'action' => $trans_data->action,
																'item' => $item_id,
																 'transact' => $trans_data->ind_id, 
																 'total' => $item_data->total, 
																 'payment' => $trans_data->payment,
																 'team' => $team,
																 'hash' => $hash);	
					}
				}
			}
		}

		//
		//Item Type 2
		$donate_query = "SELECT pt.ind_id, ot.id, ot.created_by, sum(price) total, sum(amount) payment 
					FROM " . $this->order_table . " ot 
						INNER JOIN " . $this->item_table . " it ON ot.id = it.order_id
						INNER JOIN " . $this->payment_table . " pt ON ot.id = pt.order_id AND it.id = pt.order_item_id
					WHERE
						ot.created_by != pt.created_by AND
						pt.created_by = " . $user_id . "
					GROUP BY 
						pt.ind_id, ot.id, ot.created_by
					ORDER BY
						ot.id";
		$donations = $wpdb->get_results($donate_query, OBJECT_K);

		if(!empty($donations))
		{
			foreach($donations as $item_id => $item_data)
			{
				$item_list[$item_data->ind_id] = array('type' => 'Donation to: ' . get_user_meta($item_data->created_by,'first_name',true) . ' ' . get_user_meta($item_data->created_by,'last_name',true), 
											 'transact' => $item_data->ind_id,
											 'total' => $item_data->total, 
											 'payment' => $item_data->payment,
										 	 'team' => '',
											 'hash' => '');	
			}
			
		}

		
		//
		//Item Type 3
		$query = "SELECT ot.id, pt.ind_id, sum(price) total, sum(amount) payment 
					FROM " . $this->order_table . " ot 
						INNER JOIN " . $this->item_table . " it ON ot.id = it.order_id
						INNER JOIN " . $this->payment_table . " pt ON ot.id = pt.order_id AND it.id = pt.order_item_id
						INNER JOIN " . $wpdb->prefix . "command_RSVP rsvp on rsvp.order_item_id = it.id
					WHERE
						rsvp.user_id = " . $user_id . " AND
						rsvp.user_id != rsvp.created_by
					GROUP BY 
						ot.id, pt.ind_id
					ORDER BY
						ot.id";
		$items_query = $wpdb->get_results($query, OBJECT_K);

		if(!empty($items_query))
		{
			foreach($items_query as $item_id => $item_data)
			{
				$item_list[$item_data->ind_id] = array('type' => 'Gift', 
											 'transact' => $item_data->ind_id,
											 'total' => $item_data->total, 
											 'payment' => $item_data->payment,
										 	 'team' => '',
											 'hash' => '');	
			}
			
		}

		asort($item_list);
		return $item_list;
		
	}

	public function Fetch_ItemsFromTransaction( $TransactionID = 0, $MainOnly = true )
	{
		$items = array();
		
		if( !empty( $TransactionID ) )
		{
			global $wpdb;
			
			//
			//Query to find Team purchases...
			$query = "SELECT DISTINCT it.id
						FROM " . $this->transact_table . " tt 
							INNER JOIN " . $this->payment_table . " pt ON tt.id = pt.transaction_id
							INNER JOIN " . $this->item_table . " it on pt.order_id = it.order_id 
								AND pt.order_item_id = it.id 
						WHERE
							tt.id = " . $TransactionID . "
							" . ( ( $MainOnly ) ? " AND it.add_on = 0" : '' ) . "
						ORDER BY
							it.id";
			$items_query = $wpdb->get_results( $query, OBJECT_K );

			//
			//If we have a Team purchase that may or may not include something else....
			if( !empty( $items_query ) )
			{
				foreach( $items_query as $item_id => $item_data )
				{
					$items[] = $item_data->id;
				}
			}
		}

		return $items;	
	}
	
	/**
	 * Returns the invoice
	 * 
	 * @param integer team the team number
	 * @param array rsvp array of reservations to include on the invoice
	 *
	 * @return string HTML of the invoice
	 */
	public function show_receipt( $TransactionID = 0, $Action = '', $Method = '')
	{
		//
		// action = checkout (initial rsvp puschase), donate (donation to an rsvp), payment (payment)
		// method = balance (cost and app fees), deposit (deposits and app fees), defined (defined amount)
		//
		
		if( $TransactionID > 0 )
		{
			global $wpdb;
			setlocale(LC_MONETARY, 'en_US.UTF-8');

			$html = '';
			$sub_total = 0;
			$discounts = 0;
			$this_main_item = 0;
			$itm_cnt = 0;

			$main_items = self::Fetch_ItemsFromTransaction( $TransactionID );

			if( count( $main_items ) > 0 )
			{
				$items = self::Fetch_Items( $main_items, true );	
			}
			else
			{
				$items = self::Fetch_Items( self::Fetch_ItemsFromTransaction( $TransactionID, false ), false );
			}
			
			//
			//Find Payments for this order/transaction
			if( count( $items ) > 0 )
			{
				$query = "SELECT sum(amount) FROM " . $this->payment_table . " WHERE transaction_id=" . $TransactionID;	
				$payment = $wpdb->get_var ($query);

				$query = "SELECT created_when FROM " . $this->payment_table . " WHERE transaction_id=" . $TransactionID;	
				$trans_date = date("M d, Y" , strtotime($wpdb->get_var ($query)));
				
				
				//Header -->
				$logo = 'logo.png';
				$html .= "<table class='body' data-made-with-foundation='' style='margin:0;background:#ffffff;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
					$html .= "<tbody>" . PHP_EOL;
						$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
							$html .= "<td class='float-center' align='center' valign='top' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0 auto;border-collapse:collapse!important;color:#0a0a0a;float:none;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0 auto;padding:0;text-align:center;vertical-align:top;word-wrap:break-word'>" . PHP_EOL;
								$html .= "<center data-parsed='' style='min-width:580px;width:100%'>" . PHP_EOL;
									$html .= "<table class='spacer float-center' style='Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%'>" . PHP_EOL;
										$html .= "<tbody>" . PHP_EOL;
											$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
												$html .= "<td height='16px' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>&nbsp;</td>" . PHP_EOL;
											$html .= "</tr>" . PHP_EOL;
										$html .= "</tbody>" . PHP_EOL;
									$html .= "</table>" . PHP_EOL;
									$html .= "<table align='center' class='container float-center' style='Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px'>" . PHP_EOL;
										$html .= "<tbody>" . PHP_EOL;
											$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
												//End Header -->
												$html .= "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . PHP_EOL;
													$html .= "<table class='spacer' style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
														$html .= "<tbody>" . PHP_EOL;
															$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																$html .= "<td height='16px' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>&nbsp;</td>" . PHP_EOL;
															$html .= "</tr>" . PHP_EOL;
														$html .= "</tbody>" . PHP_EOL;
													$html .= "</table>" . PHP_EOL;
													$html .= "<table class='row' style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
														$html .= "<tbody>" . PHP_EOL;
															$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																$html .= "<th class='small-12 large-12 columns first last' style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px'>" . PHP_EOL;
																	$html .= "<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																		$html .= "<tbody>" . PHP_EOL;
																			$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																				$html .= "<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
																					$html .= "<a href='" . site_url() . "' style='Margin:0;color:#2199e8;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none'>" . PHP_EOL;
																						$html .= " <img class='logo' src='" . site_url( "/wp-content/default-pics/" ) . $logo . "' style='-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;height:175px;margin:0 auto;max-width:100%;outline:0;text-decoration:none;width:auto'>" . PHP_EOL;
																					$html .= " </a>" . PHP_EOL;
																					$html .= "<h1 style='Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:34px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal'><strong>Your ISL Purchase</strong></h1>" . PHP_EOL;
																					$html .= "<p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>Thanks for shopping with us!</p>" . PHP_EOL;
																					$html .= "<table class='spacer' style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																						$html .= "<tbody>" . PHP_EOL;
																							$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																								$html .= "<td height='16px' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>&nbsp;</td>" . PHP_EOL;
																							$html .= "</tr>" . PHP_EOL;
																						$html .= "</tbody>" . PHP_EOL;
																					$html .= "</table>" . PHP_EOL;
																					$html .= "<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																						$html .= "<tbody>" . PHP_EOL;
																							$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																								$html .= "<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
																									$html .= "<h4 style='Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:24px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal'><strong>Order Details</strong></h4>" . PHP_EOL;
																								$html .= "</th>" . PHP_EOL;
																							$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																							$html .= "<td height='8px' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:8px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><span style='font-size:80%'>" . $trans_date . "</span></td>" . PHP_EOL;
																							$html .= "</tr>" . PHP_EOL;
																							$html .= "</tr>" . PHP_EOL;
				
																						$html .= "</tbody>" . PHP_EOL;
																					$html .= "</table>" . PHP_EOL;
																					$html .= "<hr>" . PHP_EOL;
																					// $html .= "<table class='spacer' style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																					// 	$html .= "<tbody>" . PHP_EOL;
																					// 		$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																					// 			$html .= "<td height='16px' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>&nbsp;</td>" . PHP_EOL;
																					// 		$html .= "</tr>" . PHP_EOL;
																					// 	$html .= "</tbody>" . PHP_EOL;
																					// $html .= "</table>" . PHP_EOL;
																					$html .= "<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																						$html .= "<tbody>" . PHP_EOL;

				//
				//Build HTML output
				$show_cost = true;
				foreach($items as $item_id => $item_data)
				{
					if( $item_data->add_on == 0 )
					{
						$this_main_item = $item_data->id;
						$order_id = $item_data->order_id;
						$itm_cnt ++;
						
						//
						//If it's a new main item, and not the first one, then add a divider line.
						if( $itm_cnt >= 2 )
						{
							$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "<th colspan=3 style='Margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><HR></th>" . PHP_EOL;
							$html .= "</tr>" . PHP_EOL;							
						}
						
					}
					
					
					//
					//Output block based on what it is
					$item_price = 0;
					switch($item_data->item_type)				
					{
						case 0:
							
							if($Action == "checkout")
							{
								$item_price = $item_data->price;
								
								$html .= "  <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "  <td colspan='2' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . PHP_EOL;
								$html .= "     <small style='color:#cacaca;font-size:80%'>&nbsp;&nbsp;+Non-refundable Application Fee</small>" . PHP_EOL;
								$html .= "  </td>" . PHP_EOL;

								$html .= "  <td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right'>" . PHP_EOL;
								//
								//2018-03-24 DJT Mmoved $ inside formatting
								//tml .= "     $" . ($show_cost ? number_format($item_data->price,2) : 'PAID') . "<br />" . PHP_EOL;
								$html .= "      " . ($show_cost ? '$' . number_format($item_data->price,2) : 'PAID') . "<br />" . PHP_EOL;
								$html .= "  </td>" . PHP_EOL;
								$html .= " </tr>" . PHP_EOL;
							}
							
							if($Action == "payment")
							{
								//Note: Do not show any data for payments as we'll enter "Prior amount owing" in lieu of actual items.
							}
							
							break;
						case 1:

							if($Action == "checkout" || $Action == "payment")
							{
								$team = new TEAM();
								$country = new COUNTRY();
								$program = new PROGRAM();

								$team->set_team($item_data->item_id);
								$team_ids[] = $item_data->item_id;
								$country_list = $country->country_list($team->schedule_countries());

								//
								//Build program listings with line breaks between each.
								$programs = '';
								foreach($team->get_schedule() as $id=>$schedule_data)
								{
									$program->set_program($schedule_data['program']);
									$programs .= $program->get_name() . "<BR>";		
								}
								if(strlen($programs) > 3)
								{
									$programs =substr($programs,0,-4);
								}

								if($team->value('no_finances') == 0)
								{
									$show_cost = true;
								}
								else
								{
									$show_cost = false;
								}
							}
							
							if($Action == "checkout")
							{
								$item_price = $item_data->price;
								
								$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'><strong>Program: " . $item_data->item_verbiage . "</strong></p>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   </tr>" . PHP_EOL;
								$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<strong>Program Details:</strong>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<strong>Quantity:</strong>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<strong>Trip Cost:</strong>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   </tr>" . PHP_EOL;
								$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . $programs . " in " . $country_list . "</td>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . $item_data->quantity . "</td>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right'>" . ($show_cost ? '$' . number_format($item_price,2) : 'PAID') . "</td>" . PHP_EOL;
								$html .= "   </tr>" . PHP_EOL;
							}

							if($Action == "payment")
							{
								$item_price = self::Fetch_Balance( false , $order_id ) + $payment;
								
								$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'><strong>&nbsp;</strong></p>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   </tr>" . PHP_EOL;
								
								$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<strong>Details:</strong>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<strong>" . str_repeat('&nbsp;', 8) . "</strong>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
								$html .= "   		<strong>Amount:</strong>" . PHP_EOL;
								$html .= "   	</th>" . PHP_EOL;
								$html .= "   </tr>" . PHP_EOL;
								
								$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>Prior Balance for: " . $item_data->item_verbiage . "</td>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . str_repeat('&nbsp;', 8) . "</td>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right'>" . ($show_cost ? '$' . number_format($item_price,2) : 'PAID') . "</td>" . PHP_EOL;
								$html .= "   </tr>" . PHP_EOL;
							}
							
							break;
						case 4:
							$item_price = $item_data->price;

							$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
							$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
							$html .= "   		<p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'><strong>Program: " . $item_data->origin_verbiage . "</strong></p>" . PHP_EOL;
							$html .= "   	</th>" . PHP_EOL;
							$html .= "   </tr>" . PHP_EOL;
							$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
							$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
							$html .= "   		<strong>Details:</strong>" . PHP_EOL;
							$html .= "   	</th>" . PHP_EOL;
							$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
							$html .= "   		<strong>Quantity:</strong>" . PHP_EOL;
							$html .= "   	</th>" . PHP_EOL;
							$html .= "   	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
							$html .= "   		<strong>Trip Cost:</strong>" . PHP_EOL;
							$html .= "   	</th>" . PHP_EOL;
							$html .= "   </tr>" . PHP_EOL;
							$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
							$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . $item_data->item_verbiage . "</td>" . PHP_EOL;
							$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . $item_data->quantity . "</td>" . PHP_EOL;
							$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right'>" . ($show_cost ? '$' . number_format($item_price,2) : 'PAID') . "</td>" . PHP_EOL;
							$html .= "   </tr>" . PHP_EOL;
							
							break;

						case 61:
						case 62:
							if($Action == "checkout")
							{
								$item_price = $item_data->price;
								
								$html .= "   <tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . $item_data->item_verbiage . "</td>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>" . $item_data->quantity . "</td>" . PHP_EOL;
								$html .= "   	<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right;'>-$" . number_format($item_price * (-1),2) . "</td>" . PHP_EOL;
								$html .= "   </tr>" . PHP_EOL;
							}

							if($Action == "payment")
							{
								//Note: Do not show any data for payments as we'll enter "Prior amount owing" in lieu of actual items.
							}
							
							break;
						default:
							$html .= "   <div class='details medium-6 columns'>" . PHP_EOL;
							$html .= "      <span class='small'>+ Shouldn't see this yet</span>" . PHP_EOL;
							$html .= "   </div>" . PHP_EOL;
							$html .= "   <div class='quantity medium-3 columns'>" . PHP_EOL;
							$html .= "      &nbsp;" . PHP_EOL;
							$html .= "   </div>" . PHP_EOL;
							$html .= "   <div class='team-totals medium-3 columns'>" . PHP_EOL;
							$html .= "      $" . $item_data['price'] . "<br />" . PHP_EOL;
							$html .= "   </div>" . PHP_EOL;
							
							break;
					}	//End switch

					//
					//Add to sub_total
					if($show_cost)
					{
						$sub_total += $item_price;
					}
					
					$last_item = $item_id;
					
				}	//End foreach

				//
				//If there's been more than one main item then add a divider line.
				if( $itm_cnt >= 2 )
				{
					$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
						$html .= "<th colspan=3 style='Margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><HR></th>" . PHP_EOL;
					$html .= "</tr>" . PHP_EOL;							
				}

				$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
				$html .= "	<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>&nbsp;</th>" . PHP_EOL;
				$html .= "</tr>" . PHP_EOL;
				
				if($Action=='checkout')
				{

					$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
					$html .= "<td colspan='2' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><b>Total:</b></td>" . PHP_EOL;
					//
					//2018-03-24 DJT updated to reflect paid for cfl's
					//$html .= "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right;'>$" . number_format($sub_total,2) . "</td>" . PHP_EOL;
					$html .= "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right;'>" . ($show_cost ? '$' . number_format($sub_total,2) : 'PAID') . "</td>" . PHP_EOL;
					$html .= "</tr>" . PHP_EOL;
				}

            	$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
				$html .= "<td colspan='2' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><b>Payment:</b></td>" . PHP_EOL;
				//
				//2018-03-24 DJT updated to reflect None for cfl's
				//$html .= "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right;'>" .  money_format('%+.2n', $payment * (-1)) . "</td>" . PHP_EOL;
				$html .= "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right;'>" .  ($show_cost ? money_format('%+.2n', $payment * (-1)) : 'NONE') . "</td>" . PHP_EOL;
				$html .= "</tr>" . PHP_EOL;

				//START added 02/04/2019 by Alexandr
                if (isset($payment) && (isset($TransactionID))){
                    $html .= '<div style="display: none" id="transaction_amount">' . $payment . '</div>';
                    $html .= '<div style="display: none" id="transaction_id">' . $TransactionID . '</div>';
                    $html .= '<div style="display: none" id="transaction_name">' . $item_data->item_verbiage . '</div>';
                    if (isset($order_id) && !empty($order_id)){
                        $base_cost = self::Fetch_Cost( false, $order_id );
                    }
                    else{
                        $base_cost = 0;
                    }
                    $html .= '<div style="display: none" id="transaction_total">' . $base_cost . '</div>';
                }
                //END added 02/04/2019

                if($Action=='checkout' || $Action == "payment")
				{
					$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
					$html .= "<td colspan='2' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><b>Balance Due:</b></td>" . PHP_EOL;
					//
					//2018-03-24 DJT updated to reflect None for cfl's
					//$html .= "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right;'>$" . number_format($sub_total - $payment + $discounts,2) . "</td>" . PHP_EOL;
					$html .= "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word;text-align:right;'>" . ($show_cost ? number_format($sub_total - $payment + $discounts,2) : 'NONE') . "</td>" . PHP_EOL;
					$html .= "</tr>" . PHP_EOL;
				}

																		$html .= "</tbody>" . PHP_EOL;
																			$html .= "</table>" . PHP_EOL;
																			$html .= "<table class='spacer' style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																				$html .= "<tbody>" . PHP_EOL;
																					$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																						$html .= "<td height='16px' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>&nbsp;</td>" . PHP_EOL;
																					$html .= "</tr>" . PHP_EOL;
																				$html .= "</tbody>" . PHP_EOL;
																			$html .= "</table>" . PHP_EOL;
																			$html .= "<hr>" . PHP_EOL;
																			$html .= "<table class='spacer' style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																				$html .= "<tbody>" . PHP_EOL;
																				$html .= "	<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																						$html .= "<td height='16px' style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'>&nbsp;</td>" . PHP_EOL;
																					$html .= "</tr>" . PHP_EOL;
																			$html .= "	</tbody>" . PHP_EOL;
																			$html .= "</table>" . PHP_EOL;
																		$html .= "</th>" . PHP_EOL;
																	$html .= "</tr>" . PHP_EOL;
																$html .= "</tbody>" . PHP_EOL;
															$html .= "</table>" . PHP_EOL;
														$html .= "</th>" . PHP_EOL;
													$html .= "</tr>" . PHP_EOL;
												$html .= "</tbody>" . PHP_EOL;
											$html .= "</table>" . PHP_EOL;
											//Footer -->
											$html .= "<table class='row footer text-center' style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:center;vertical-align:top;width:100%'>" . PHP_EOL;
												$html .= "<tbody>" . PHP_EOL;
													$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
														$html .= "<th class='small-12 large-6 columns first' style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:260px'>" . PHP_EOL;
															$html .= "<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																$html .= "<tbody>" . PHP_EOL;
																	$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																		$html .= "<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
																			$html .= "<p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>Call us at 877-779-8075<br>Email us at <a href='mailto:support@islonline.zendesk.com'>support@islonline.zendesk.com</a></p>" . PHP_EOL;
																		$html .= "</th>" . PHP_EOL;
																	$html .= "</tr>" . PHP_EOL;
																$html .= "</tbody>" . PHP_EOL;
															$html .= "</table>" . PHP_EOL;
														$html .= "</th>" . PHP_EOL;
														$html .= "<th class='small-12 large-6 columns last' style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:8px;padding-right:16px;text-align:left;width:260px'>" . PHP_EOL;
															$html .= "<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'>" . PHP_EOL;
																$html .= "<tbody>" . PHP_EOL;
																	$html .= "<tr style='padding:0;text-align:left;vertical-align:top'>" . PHP_EOL;
																		$html .= "<th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'>" . PHP_EOL;
																			$html .= "<p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>125 McKinley Ave<br>Kellogg, ID 83837</p>" . PHP_EOL;
																		$html .= "</th>" . PHP_EOL;
																	$html .= "</tr>" . PHP_EOL;
																$html .= "</tbody>" . PHP_EOL;
															$html .= "</table>" . PHP_EOL;
														$html .= "</th>" . PHP_EOL;
													$html .= "</tr>" . PHP_EOL;
												$html .= "</tbody>" . PHP_EOL;
											$html .= "</table>" . PHP_EOL;
											//End Footer -->
										$html .= "</td>" . PHP_EOL;
									$html .= "</tr>" . PHP_EOL;
								$html .= "</tbody>" . PHP_EOL;
							$html .= "</table>" . PHP_EOL;
						$html .= "</center>" . PHP_EOL;
					$html .= "</td>" . PHP_EOL;
				$html .= "</tr>" . PHP_EOL;
			$html .= "</tbody>" . PHP_EOL;
			$html .= "</table>" . PHP_EOL;
			
			} // End if(count(items))
		}	// End if(transaction) 

		return $html;
	}
				


	/**
	 *Functions to deal with payment
	 */

	//
	//2018-03-23 Added $AllowZeroPay to force purchase record for empty Reservations
	/**
	 * Inserts payment info into payment table
	 *
	 * @param integer $item_id Id of the item
	 * @param float $amt The amout of the payment made
	 */
	public function payment($item_id, $amt = 0, $indicator = 0, $note = '', $special = NULL, $user_override = NULL, $ForceDate = NULL, $AllowZeroPay = false)
	{
		//
		//2018-03-23 Added $AllowZeroPay to force purchase record for empty Reservations
		if($amt != 0 || $AllowZeroPay)
		{
			global $wpdb;

			$amount = self::funds_remove($amt);

			$wpdb->insert($this->payment_table,
						  array('order_id' => $this->order_id,
								'ind_id' => (($indicator > 0) ? $indicator : $this->transaction_id),
								'order_item_id' => $item_id,
								'transaction_id' => $this->transaction_id,
								'special' => $special,
								'amount' => $amt,
								'note' => $note,
								'created_by' => empty($user_override) ? $this->user : $user_override,
								'created_when' => empty($ForceDate) ? (new \DateTime())->format('Y-m-d H:i:s') : $ForceDate
								));
		}


	}

	/**
	 * Updates payment info in payment table
	 *
	 * @param        $adj_id The identity id of the payment
	 * @param int    $amt The payment amount
	 * @param string $note The adjustment reason
	 */
	public function update_payment($adj_id, $amt = 0, $note = '')
	{
		global $wpdb;

		$wpdb->update($this->payment_table, array(
			'amount' => $amt,
			'note'   => $note,
		), array(
			'id' => $adj_id,
		));
	}

	/**
	 * Deletes payment info in payment table
	 *
	 * @param        $adj_id The identity of the payment
	 */
	public function delete_payment($adj_id)
	{
		global $wpdb;

		$wpdb->delete($this->payment_table, array('id' => $adj_id));
	}

	/**
	 * Adds a new payment to the payment table
	 *
	 * @param float $amount The amount of the payment
	 */
	public function new_payment($amount = 0, $indicator = 0)
	{
		global $wpdb;
		
		$wpdb->insert($this->payment_table,
					  array('order_id' => $this->order_id,
							'ind_id' => (($indicator > 0) ? $indicator : $this->transaction_id),
					  		'transaction_id' => $this->transaction_id,
							'amount' => $amount,
							'created_by' => $this->user,
							'created_when' => NULL
						    ));
	}

	/**
	 * Private functions
	 */

	/**
	 * Builds the payment types array
	 */
	private function build_payment_types()
	{
		$this->payment_types[1] = 'VISA';
		$this->payment_types[2] = 'MACA';
		$this->payment_types[3] = 'AMEX';
		$this->payment_types[4] = 'DISC';

		$this->payment_types[31] = 'Cash';
		$this->payment_types[32] = 'Check';

		$this->payment_types[51] = 'PayPal Manual';

		$this->payment_types[61] = 'Rep Adjustment';
		$this->payment_types[62] = 'Manual Adjustment';
		
		$this->payment_types[99] = 'Other';
	}

	private function build_item_types()
	{
		global $wpdb;
		
		$query = "SELECT id, title, verbiage, function FROM " . $this->item_type_table;
		$results = $wpdb->get_results($query, OBJECT_K);
		
		foreach($results as $id=>$data)
		{
			$this->item_types[$id] = array( 'title' => $data->title,
									 		'verbiage' => $data->verbiage,
									 		'function' => $data->function);
		}
	}
}