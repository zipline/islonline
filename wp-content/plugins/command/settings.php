<?php

class COMMAND_CENTER_SETTINGS
{
	private $options;

	private $text_cols = 100;
	private $text_rows = 3;
	
	
	public function __construct()
	{
		add_action ('admin_menu', array ($this, 'add_settings_page'));
		add_action ('admin_init', array($this, 'page_init'));
	}
	
	public function add_settings_page()
	{
		add_options_page(
			'Settings Admin',
			'Command Center',
			'manage_options',
			'command-center-admin',
			array($this, 'create_admin_page')
		);
	}
	
	public function create_admin_page()
	{
		$this->options = get_option('cmd_center');
		?>
        <div class="wrap">
            <h2>Command Center</h2>
            
            <form method="post" action="options.php">
                <?php settings_fields('cmd_center_group'); ?>
            	<?php do_settings_sections('command-center-admin'); ?>
                <?php submit_button('Save Changes'); ?>
            </form>
        </div>
    	<?php
	}


	//
	// Build the options that will be on the page	
	public function page_init()
	{
		register_setting(
			'cmd_center_group', 
			'cmd_center',
			array($this, 'sanitize')
		);
		 
		//
		//Email settings
		add_settings_section(
			'email-setup',
			'Message Settings',
			array($this, 'message_section_heading'),
			'command-center-admin'
		);

		$messages = self::get_message_fields('command-center-admin', 'email-setup');
		foreach($messages as $fld_name => $fld_data)
		{
			add_settings_field(
				$fld_name,
				$fld_data['label'],
				array($this, $fld_data['callback']),
				$fld_data['page'],
				$fld_data['section'],
				array('name' => $fld_name)
			);
		}

		//
		//Twilio settings
		add_settings_section(
			'twilio-setup',
			'Twilio Settings',
			array($this, 'twilio_section_heading'),
			'command-center-admin'
		);

		$messages = self::get_twilio_fields('command-center-admin', 'twilio-setup');
		foreach($messages as $fld_name => $fld_data)
		{
			add_settings_field(
				$fld_name,
				$fld_data['label'],
				array($this, $fld_data['callback']),
				$fld_data['page'],
				$fld_data['section'],
				array('name' => $fld_name)
			);
		}

		//
		//Date section
		add_settings_section(
			'date-setup',
			'Date Settings',
			array($this, 'date_section_heading'),
			'command-center-admin'
		);

		$dates = self::get_date_fields('command-center-admin', 'date-setup');
		foreach($dates as $fld_name => $fld_data)
		{
			add_settings_field(
				$fld_name,
				$fld_data['label'],
				array($this, $fld_data['callback']),
				$fld_data['page'],
				$fld_data['section'],
				array('name' => $fld_name)
			);
		}

		//
		//Finance section
		add_settings_section(
			'finance-setup',
			'Financial Settings',
			array($this, 'finance_section_heading'),
			'command-center-admin'
		);

		$finances = self::get_finance_fields('command-center-admin', 'finance-setup');
		foreach($finances as $fld_name => $fld_data)
		{
			add_settings_field(
				$fld_name,
				$fld_data['label'],
				array($this, $fld_data['callback']),
				$fld_data['page'],
				$fld_data['section'],
				array('name' => $fld_name)
			);
		}

	}


	//
	// Core sanitation function
	public function sanitize($input)
	{
		$ret_val = array();
		
		foreach($input as $field => $value)
		{
			$type = substr($field, strrpos($field, '_') +1);	

			switch ($type)
			{
				case 'email':
					$email = '';
					
					foreach(explode(',', $value) as $possible_email)
					{
						if(is_email(trim($possible_email)))
						{
							$email .= ($possible_email) . ', ';
						}
					}
					
					$ret_val[$field] = substr($email, 0, -2);
					break;
				case 'limit':
				case 'credit':
				case 'fee':
					$ret_val[$field] = is_numeric($value) ? $value : '';
					break;
				case 'phone':
					$stripped = preg_replace("/[^0-9]/","",$value);
					$digits = strlen($stripped);
					$number = is_numeric($stripped);
					$ret_val[$field] = (($digits && $number) ? $stripped : "");
					break;
				case 'text':
					$ret_val[$field] = $value;
					break;
			}
		}
		
		return $ret_val;
	}


	//
	// Section Instructions
	public function message_section_heading()
	{
		print 'Enter e-mail addresses below, separated by commas. 
			   Each recipient will receive an email when the listed event occurs:';
	}
	
	//
	// Section Instructions
	public function date_section_heading()
	{
		print 'Enter default timelines for the listed options.';
	}

	//
	// Section Instructions
	public function finance_section_heading()
	{
		print 'Enter default Financial information for the listed options.';
	}

	//
	// Section Instructions
	public function twilio_section_heading()
	{
		print 'Enter Twilio information to enable text messaging.';
	}

	//
	// Build email input and place value in it if it exsits
	public function print_email_callback(array $args)
	{
		$name = $args['name'];

		printf(
			'<textarea rows="' . $this->text_rows . '" cols="' . $this->text_cols . '" 
			  id="' . $name . '" name="cmd_center[' . $name . ']">%s</textarea>',
			isset($this->options[$name]) ? esc_attr($this->options[$name]) : ''
		);

		/*
		printf(
			'<input type="text" id="create_a_team_email" name="cmd_center[title]" value="%s" />',
			isset($this->options['create_a_team_email']) ? esc_attr($this->options['create_a_team_email']) : ''
		);
		*/
	}

	//
	// Build email input and place value in it if it exsits
	public function print_number_callback(array $args)
	{
		$name = $args['name'];

		printf(
			'<input type="number"  
			  id="' . $name . '" name="cmd_center[' . $name . ']" min="0" value="%s">',
			isset($this->options[$name]) ? esc_attr($this->options[$name]) : ''
		);

	}

	//
	// Build phone input and place value in it if it exsits
	public function print_phone_callback(array $args)
	{
		$name = $args['name'];

		printf(
			'<input type="text"  
			  id="' . $name . '" name="cmd_center[' . $name . ']" value="%s">',
			isset($this->options[$name]) ? esc_attr($this->options[$name]) : ''
		);

	}
	
	//
	// Build text input and place value in it if it exsits
	public function print_text_callback(array $args)
	{
		$name = $args['name'];

		printf(
			'<input type="text"  
			  id="' . $name . '" name="cmd_center[' . $name . ']" maxsize="' . $this->text_cols . '" size="' . $this->text_cols . '" value="%s">',
			isset($this->options[$name]) ? esc_attr($this->options[$name]) : ''
		);

	}

	//
	// Fields used for email settings.
	private function get_message_fields($page, $section)
	{
		$ret_val = array();
		
		
		$ret_val['ambassador_submit_email'] = array('label' => 'Ambassador Submissions', 
													'callback' => 'print_email_callback', 
													'page' => $page, 
													'section' => $section);
		$ret_val['create_a_team_email'] = array('label' => 'Create a Team', 
												'callback' => 'print_email_callback', 
												'page' => $page, 
												'section' => $section);
		$ret_val['request_internship_email'] = array('label' => 'Internship Requests', 
												'callback' => 'print_email_callback', 
												'page' => $page, 
												'section' => $section);
		$ret_val['submit_budget_email'] = array('label' => 'Budget Submission', 
												'callback' => 'print_email_callback', 
												'page' => $page, 
												'section' => $section);
		$ret_val['submit_cancel_email'] = array('label' => 'Cancellations', 
												'callback' => 'print_email_callback', 
												'page' => $page, 
												'section' => $section);
		$ret_val['payment_failure_email'] = array('label' => 'Payment Failure', 
												'callback' => 'print_email_callback', 
												'page' => $page, 
												'section' => $section);

		return $ret_val;
	}

	//
	// Fields used for email settings.
	private function get_date_fields($page, $section)
	{
		$ret_val = array();
		
		$ret_val['docs_due_limit'] = array('label' => 'Days Prior to Team Start for Docs due', 
											'callback' => 'print_number_callback', 
											'page' => $page, 
											'section' => $section);
		return $ret_val;
	}

	//
	// Fields used for finance settings.
	private function get_finance_fields($page, $section)
	{
		$ret_val = array();
		
		$ret_val['campus_ambassador_credit'] = array('label' => 'Campus Ambassador Credit', 
													'callback' => 'print_number_callback', 
													'page' => $page, 
													'section' => $section);
		$ret_val['campus_ambassador_fee'] = array('label' => 'Campus Ambassador Volunteer Credit', 
												'callback' => 'print_number_callback', 
												'page' => $page, 
												'section' => $section);

		return $ret_val;
	}

	//
	// Fields used for Twilio settings.
	private function get_twilio_fields($page, $section)
	{
		$ret_val = array();
		
		$ret_val['twilio_sid_text'] = array('label' => 'Twilio SID', 
										   		'callback' => 'print_text_callback', 
										   		'page' => $page, 
										   		'section' => $section);
		$ret_val['twilio_token_text'] = array('label' => 'Twilio Token', 
											 	  'callback' => 'print_text_callback', 
											 	  'page' => $page, 
										 	 	  'section' => $section);
		$ret_val['twilio_phone'] = array('label' => 'Twilio Phone', 
											 'callback' => 'print_phone_callback', 
											 'page' => $page, 
										 	 'section' => $section);

		return $ret_val;
	}

}