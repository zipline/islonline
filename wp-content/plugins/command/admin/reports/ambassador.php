<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Enqueue Scripts
wp_enqueue_script('jquery-ui-datepicker');


//
//Load Classes
require_once (CC_CLASSES_DIR . "team.support.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");


//
//Initialize Classes
$support = new TEAM_SUPPORT();
$finance = new FINANCE();
$page = new TAB_MANAGE();

//
//Core variables
global $wpdb;
$whoami = wp_get_current_user();

//
//Filters
$filters = array();
!empty($_POST['start']) ? $filters['start'] = date_format(date_create_from_format('m/d/Y', $_POST['start']), 'Y-m-d') : '';
!empty($_POST['end']) ? $filters['end'] = date_format(date_create_from_format('m/d/Y', $_POST['end']), 'Y-m-d') : '';

?>

<div class='page-header wrap'>
	<h1>Ambassador CAP CODE Use Report</h1>
</div>

<div class="filter_box">
	<form id="filter" name="search" method="POST" enctype="multipart/form-data" action="<?php echo $page->GET_path()?>">
		<table id='search_table' class='standard-large-table wrap'>

			<TR>
				<td>
					<input 
						name="start" 
						id="start" 
						class="regular-text" 
						type="text"
						placeholder="After this Date"
						value="<?php _e(!empty($_POST['start']) ? $_POST['start'] : ''); ?>"
					/>
					<span class="date-icon"></span>
				</td>

				<td>
					<input 
						name="end" 
						id="end" 
						class="regular-text" 
						type="text" 
						<?php _e((!empty($_POST['end']) ? 'value="' . $_POST['end'] . '"' : 'placeholder="Before this Date"'))?>
					/>
					<span class="date-icon"></span>
				</td>

				<td><button type="submit" name="submit" id="submit" class="button" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button></td>

				<td><button type="" name="reset" id="reset" class="button" value="ALL" onclick="javascript:reset_form()">All</button></td>
			</tr>
		</table>
	</form>
</div

<div class="main-list-wrapper">
	<table id="report_info" class="report-list standard-large-table">
		<thead>
			<tr>
				<TH class="medium">
					AMBASSADOR
				</TH>
				<TH class="medium">
					USED BY
				</TH>
				<TH class="small">
					DATE
				</TH>
				<TH class="small">
					AMOUNT
				</TH>
			</tr>
		</thead>

		<TBODY>
		<?php
		
		//
		//Date filters are screwey, figure them out first.
		$date_filter = '';
		if(isset($filters['start']))
		{
			$date_filter .= ' AND coi.created_when > "' . $filters['start'] . '"';
		}
		if(isset($filters['end']))
		{
			$date_filter .= ' AND coi.created_when < "' . $filters['end'] . '"';
		}
	
						   
		$query = "SELECT u.ID, coi.price, coi.created_when ,
						um_fn.meta_value first_name, um_ln.meta_value last_name,
						um_p_fn.meta_value v_first_name, um_p_ln.meta_value v_last_name
					FROM " . $wpdb->prefix . "users u INNER JOIN
						 " . $wpdb->prefix . "usermeta um_fn on u.ID = um_fn.user_id INNER JOIN
						 " . $wpdb->prefix . "usermeta um_ln on u.ID = um_ln.user_id INNER JOIN
						 " . $wpdb->prefix . "command_order_items coi on coi.item_id = u.ID LEFT JOIN
						 " . $wpdb->prefix . "usermeta um_p_fn on coi.created_by = um_p_fn.user_id LEFT JOIN
						 " . $wpdb->prefix . "usermeta um_p_ln on coi.created_by = um_p_ln.user_id
					WHERE 
						um_fn.meta_key = 'first_name' AND 
						um_ln.meta_key = 'last_name' AND
						um_p_fn.meta_key = 'first_name' AND 
						um_p_ln.meta_key = 'last_name' AND
						(coi.quantity > 0 ) AND
						(coi.item_type = 61 ) AND 
						" . (isset($date_filter) && strlen($date_filter) > 0 ? substr($date_filter, 4) : '1=1') . "
					ORDER BY 
						um_ln.meta_value, um_fn.meta_value,
						um_p_ln.meta_value, um_p_fn.meta_value
						";
		$cap_codes = $wpdb->get_results($query);
		
		$cur_user = 0;
		foreach($cap_codes as $index => $row_data)
		{
			echo "<TR>";

			if($cur_user == $row_data->ID)
			{
				echo "  <TD>&nbsp;</TD>";
			}
			else
			{
				echo "  <TD>" . $row_data->last_name . ', ' . $row_data->first_name . "</a></TD>";
			}
			echo "  <TD>" . $row_data->v_last_name . ', ' . $row_data->v_first_name . "</a></TD>";
			echo "  <TD>" . $row_data->created_when . "</a></TD>";
			echo "  <TD>" . $row_data->price . "</a></TD>";
			echo "</TR>";
		}

		?>
		</TBODY>

	</table>
</div>

<script>

	function reset_form()
	{
		document.getElementById('start').value = '';
		document.getElementById('end').value = '';

		document.getElementById('submit').click();
	}

	jQuery(document).ready(function(){

		var datepickerOptions = {
			dateFormat : 'mm/dd/yy',
			defaultDate: 0,
			beforeShow: function(event, ui) {
			   jQuery('#ui-datepicker-div').addClass('open');
			},
			onClose: function(event, ui) {
				jQuery('#ui-datepicker-div').removeClass('open');
			}
		};

		jQuery('#start').datepicker(datepickerOptions);
		jQuery('#end').datepicker(datepickerOptions);
		jQuery('#start').change(function(){
			var endDate = jQuery('#start').val();
			jQuery.extend(datepickerOptions, {
				defaultDate: endDate,
				minDate: endDate
			});
			jQuery('#end').datepicker(datepickerOptions);
		});
	});

</script>
