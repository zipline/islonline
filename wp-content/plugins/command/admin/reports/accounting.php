<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Enqueue Scripts
wp_enqueue_script('jquery-ui-datepicker');


//
//Load Classes
require_once (CC_CLASSES_DIR . "team.support.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");


//
//Initialize Classes
$support = new TEAM_SUPPORT();
$finance = new FINANCE();
$page = new TAB_MANAGE();

//
//Core variables
global $wpdb;
$whoami = wp_get_current_user();
$total_cost = 0;
$total_paid = 0;
$total_balance = 0;

//
//Filters
$filters = array();
!empty($_POST['volunteers']) ? $filters['volunteers'] = $_POST['volunteers'] : $filters['volunteers'] = 'future';
!empty($_POST['type']) ? $filters['type'] = $_POST['type'] : '';
!empty($_POST['dates']) ? $filters['dates'] = $_POST['dates'] : $filters['dates'] = 'team';
!empty($_POST['start']) ? $filters['start'] = date_format(date_create_from_format('m/d/Y', $_POST['start']), 'Y-m-d') : $filters['start'] = date('Y-m-d', strtotime("+30 days"));
!empty($_POST['end']) ? $filters['end'] = date_format(date_create_from_format('m/d/Y', $_POST['end']), 'Y-m-d') : $filters['end'] = date('Y-m-d', strtotime("+60 days"));

?>

<div class='page-header wrap'>
	<h1>Reporting</h1>
</div>

<div class="filter_box">
	<form id="filter" name="search" method="POST" enctype="multipart/form-data" action="<?php echo $page->GET_path()?>">
		<table id='search_table' class='standard-large-table wrap'>
			<tr>
				<td>
					<input type="radio" id="volunteers_all" name="volunteers" value="all" <?php echo checked($filters['volunteers'], 'all')?>>All Volunteers
					<BR>
					<input type="radio" id="volunteers_future" name="volunteers" value="future" <?php echo checked($filters['volunteers'], 'future')?>>Future Volunteers
				</td>

				<td>
					<SELECT name='type' id='type'><?php echo $support->type_selector(isset($_POST['type'])? $_POST['type'] : 0,true) ?></SELECT>
				</td>

				<td>&nbsp</td>

				<td><button type="submit" name="submit" id="submit" class="button" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button></td>
			</TR>

			<TR>
				<td>
					<input type="radio" id="dates_applied" name="dates" value="applied" <?php echo checked($filters['dates'], 'applied')?>>Applied Date
					<BR>
					<input type="radio" id="dates_team" name="dates" value="team" <?php echo checked($filters['dates'], 'team')?>>Team Start Date
				</td>

				<td>
					<input 
						name="start" 
						id="start" 
						class="regular-text" 
						type="text"
						placeholder="After this Date"
						value="<?php _e($filters['start']); ?>"
					/>
					<span class="date-icon"></span>
				</td>

				<td>
					<input 
						name="end" 
						id="end" 
						class="regular-text" 
						type="text" 
						placeholder = "Before this Date"
						value = <?php _e($filters['end'])?>
					/>
					<span class="date-icon"></span>
				</td>

				<td><button type="" name="reset" id="reset" class="button" value="ALL" onclick="javascript:reset_form()">All</button></td>
			</tr>
		</table>
	</form>
</div

<div class="main-list-wrapper">
	<table id="report_info" class="report-list standard-large-table">
		<thead>
			<tr>
				<TH class="small">
					VOLUNTEER ID
				</TH>
				<TH class="small">
					TEAM NUMBER
				</TH>
				<TH class="medium">
					APPLIED DATE
				</TH>
				<TH class="medium">
					START DATE
				</TH>
				<TH class="small">
					ORDER TOTAL
				</TH>
				<TH class="small">
					PAID TO DATE
				</TH>
				<TH class="small">
					BALANCE
				</TH>
				<TH class="small">
					TEAM TYPE
				</TH>
			</tr>
		</thead>

		<TBODY>
		<?php
		
		//
		//Date filters are screwey, figure them out first.
		$date_filter = '';
		if(isset($filters['dates']))
		{
			if($filters['dates'] == 'applied')
			{
				if(isset($filters['start']))
				{
					$date_filter .= ' AND min(rsvp.created) > "' . $filters['start'] . '"';
				}
				if(isset($filters['end']))
				{
					$date_filter .= ' AND min(rsvp.created) < "' . $filters['end'] . '"';
				}
			}
			
			if($filters['dates'] == 'team')
			{
				if(isset($filters['start']))
				{
					$date_filter .= ' AND min(s.start_date) > "' . $filters['start'] . '"';
				}
				if(isset($filters['end']))
				{
					$date_filter .= ' AND min(s.start_date) < "' . $filters['end'] . '"';
				}
			}
			
		}
	
		if(isset($filters['volunteers']) && $filters['volunteers'] == 'future')
		{
			$date_filter .= ' AND min(s.start_date) > "' . date("Y-m-d") . '"';
		}
						   
		$query = "SELECT IF(rsvp.user_id > 0, rsvp.user_id, rsvp.created_by) who, 
						 min(rsvp.created) created,
						 min(s.start_date) arrival,
						 rsvp.team_id, 
						 team.hash,
						 team.team_type_id,
						 GROUP_CONCAT(DISTINCT rsvp.order_item_id) order_ids
					FROM " . $wpdb->prefix . "command_RSVP rsvp LEFT JOIN
						 " . $wpdb->prefix . "users u 
						 	ON rsvp.user_id = u.ID INNER JOIN
						 " . $wpdb->prefix . "command_TEAM team 
						 	ON team.id = rsvp.team_id INNER JOIN
						 " . $wpdb->prefix . "command_order_items oi 
						 	ON rsvp.order_item_id = oi.id INNER JOIN
						 " . $wpdb->prefix . "command_schedule s 
						 	ON team.id = s.team_id 
					WHERE 
						oi.quantity > 0 AND
						IF(rsvp.user_id > 0, rsvp.user_id, rsvp.created_by) > 0
						" . (isset($filters['type']) ? ' AND team.team_type_id = ' . $filters['type'] : '') . "
					GROUP BY
						who, rsvp.team_id, team.hash, team_type_id
						" . (isset($date_filter) && strlen($date_filter) > 0 ? ' HAVING ' . substr($date_filter, 4) : '') . "
					ORDER BY 
						rsvp.user_id";
		$users = $wpdb->get_results($query);
				  
		foreach($users as $index => $row_data)
		{
			
			$whoami = get_user_by('ID', $row_data->who);
			
			//
			//Find cost of the order_id and add_ons
			$finance->Fetch_Items(explode(',',$row_data->order_ids), true);
			$total_cost += $finance->Calculate_ItemsCost();
			$total_paid += $finance->Calculate_ItemsPaid();
			$total_balance += $finance->Calculate_ItemsBalance();
						   
			echo "<TR>";
			echo "  <TD><a href='/wp-admin/admin.php?page=cc-volunteers&user_id=" . $whoami->ID . "' target='_blank'>" . $whoami->display_name . "</a></TD>";
			echo "  <TD><a href='/wp-admin/admin.php?page=cc-teams&team_id=" . $row_data->team_id . "' target='_blank'>" . $row_data->hash . "</a></TD>";
			echo "  <TD>" . date_format(date_create_from_format('Y-m-d h:i:s', $row_data->created), 'm/d/Y') . "</TD>";
			echo "  <TD>" . date_format(date_create_from_format('Y-m-d h:i:s', $row_data->arrival), 'm/d/Y') . "</TD>";
			echo "  <TD align='right'>$" . number_format($finance->Calculate_ItemsCost(), 0) . "</TD>";
			echo "  <TD align='right'>$" . number_format($finance->Calculate_ItemsPaid(), 0) . "</TD>";
			echo "  <TD align='right'>$" . number_format($finance->Calculate_ItemsBalance(), 0) . "</TD>";
			echo "  <TD>" . $support->type_value($row_data->team_type_id, 'title') . "</TD>";
			
			echo "</TR>";
		}

		?>
		</TBODY>

		<TFOOT>
			<TR>
				<TD colspan = 4><B>Grand Total</B></TD>
				<TD align='right'><B>$<?php echo number_format($total_cost, 0) ?></B></TD>
				<TD align='right'><B>$<?php echo number_format($total_paid, 0) ?></B></TD>
				<TD align='right'><B>$<?php echo number_format($total_balance, 0) ?></B></TD>
			</TR>		
		</TFOOT>
	</table>
</div>

<script>

	function reset_form()
	{
		document.getElementById('volunteers_all').checked = false;
		document.getElementById('volunteers_future').checked = true;
		document.getElementById('type').value = '';
		document.getElementById('dates_applied').checked = false;
		document.getElementById('dates_team').checked = false;
		document.getElementById('start').value = '';
		document.getElementById('end').value = '';

		document.getElementById('submit').click();
	}

	jQuery(document).ready(function(){

		var datepickerOptions = {
			dateFormat : 'mm/dd/yy',
			defaultDate: 0,
			beforeShow: function(event, ui) {
			   jQuery('#ui-datepicker-div').addClass('open');
			},
			onClose: function(event, ui) {
				jQuery('#ui-datepicker-div').removeClass('open');
			}
		};

		jQuery('#start').datepicker(datepickerOptions);
		jQuery('#end').datepicker(datepickerOptions);
		jQuery('#start').change(function(){
			var endDate = jQuery('#start').val();
			jQuery.extend(datepickerOptions, {
				defaultDate: endDate,
				minDate: endDate
			});
			jQuery('#end').datepicker(datepickerOptions);
		});
	});

</script>
