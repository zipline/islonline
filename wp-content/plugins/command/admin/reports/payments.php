<?php
//
//2018-02-19 DJT added cardholder name to the report

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Enqueue Scripts
wp_enqueue_script('jquery-ui-datepicker');

//
//Load Classes
require_once (CC_CLASSES_DIR . "team.support.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "user.class.php");

//
//Initialize Classes
$support = new TEAM_SUPPORT();
$finance = new FINANCE();
$page = new TAB_MANAGE();
$user = new USER();

//
//Core variables
global $wpdb;

//
//Filters
$filters = array();
$filters['start'] = !empty($_POST['start']) ? $_POST['start'] : date('Y/m/d', strtotime("-7 days"));
$filters['end'] = !empty($_POST['end']) ? $_POST['end'] : date('Y/m/d');
?>

<div class='page-header wrap'>
	<h1>Reporting</h1>
</div>

<div class="filter_box">
	<form id="filter" name="search" method="POST" enctype="multipart/form-data" action="<?php echo $page->GET_path()?>">
		<table id='search_table' class='standard-large-table wrap'>
			<TR>
				<td>
					Payment Date Range
				</td>

				<td>
					<input 
						name="start" 
						id="start" 
						class="regular-text" 
						type="text"
						placeholder="After this Date"
						value="<?php _e($filters['start']); ?>"
					/>
					<span class="date-icon"></span>
				</td>

				<td>
					<input 
						name="end" 
						id="end" 
						class="regular-text" 
						type="text" 
						placeholder = "Before this Date"
						value = <?php _e($filters['end'])?>
					/>
					<span class="date-icon"></span>
				</td>

				<td>&nbsp</td>

				<td><button type="submit" name="submit" id="submit" class="button" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button></td>
			</tr>
		</table>
	</form>
</div
<div class="main-list-wrapper">
	<table id="report_info" class="report-list standard-large-table">
		<thead>
			<tr>
				<TH class="small">
					DATE
				</TH>
				<TH class="medium">
					PAID BY
				</TH>
				<TH class="medium">
					PAYED FOR
				</TH>
				<TH class="medium">
					CARDHOLDER
				</TH>
				<TH class="small">
					TYPE
				</TH>
				<TH class="small">
					STATE
				</TH>
				<TH class="small" style='text-align: right;'>
					AMOUNT
				</TH>
			</tr>
		</thead>

		<TBODY>
		<?php

		//
		//2018-03-10 DJT get state choices from DB
		$post = get_post(4872);
		$content = unserialize($post->post_content);
		$choices = $content['choices'];

		//
		//2018-02-19 DJT Added war request to query
	   //2018-04-25 DJT 
	   //Changed base query to add the Union to gather add-ons and moved Order By outside.
		$query = "(SELECT 
						DATE_FORMAT(cot.created_when, '%m/%d/%Y %h:%i:%s') purchase_date, DATE_FORMAT(cot.created_when, '%Y/%m/%d %h:%i:%s') formated_date, cot.raw_request, cot.created_by, r.user_id for_whom, cot.amount, cot.form
					FROM 
						" . $wpdb->prefix . "command_order_transact cot
							INNER JOIN " . $wpdb->prefix . "command_order_payment cop on cot.id = cop.transaction_id
							INNER JOIN " . $wpdb->prefix . "command_RSVP r on cop.order_item_id = r.order_item_id
					WHERE 
						cot.form in ('VISA', 'MACA', 'AMEX', 'DISC', 'Unknown') AND
						DATE_FORMAT(cot.created_when, '%Y/%m/%d') BETWEEN '" . $filters['start'] . "' AND '" . $filters['end'] . "')
						
					UNION
					
					(SELECT 
						DATE_FORMAT(cot.created_when, '%m/%d/%Y %h:%i:%s') purchase_date, DATE_FORMAT(cot.created_when, '%Y/%m/%d') formated_date, cot.raw_request, cot.created_by, r.user_id for_whom, cot.amount, cot.form
					FROM 
						" . $wpdb->prefix . "command_order_transact cot
							INNER JOIN " . $wpdb->prefix . "command_order_payment cop on cot.id = cop.transaction_id
							INNER JOIN " . $wpdb->prefix . "command_order_items coi on cop.order_item_id = coi.id
							INNER JOIN " . $wpdb->prefix . "command_RSVP r on coi.add_on = r.order_item_id
					WHERE 
						coi.item_type = 4 AND 
						cot.form in ('VISA', 'MACA', 'AMEX', 'DISC', 'Unknown') AND
						DATE_FORMAT(cot.created_when, '%Y/%m/%d') BETWEEN '" . $filters['start'] . "' AND '" . $filters['end'] . "')
						
					ORDER BY 
						formated_date DESC, created_by
					";
//		echo $query;
//		die;
		$users = $wpdb->get_results($query);

		foreach($users as $index => $row_data)
		{
			//
			//2018-02-19 DJT unserialize card data to add cardholder name to report.
			$card_data = unserialize(stripslashes($row_data->raw_request));
			$state = get_user_meta($row_data->for_whom, 'state', true);

			echo "<TR>";
			echo "  <TD>" . $row_data->purchase_date . "</TD>";
			echo "  <TD><a href='/wp-admin/admin.php?page=cc-volunteers&user_id=" . $row_data->created_by . "' target='_blank'>" . $user->user_name($row_data->created_by) . "</a></TD>";
			echo "  <TD><a href='/wp-admin/admin.php?page=cc-volunteers&user_id=" . $row_data->for_whom . "' target='_blank'>" . $user->user_name($row_data->for_whom) . "</a></TD>";
			echo "  <TD>" . $card_data['BILLTOFIRSTNAME'] . " " . $card_data['BILLTOLASTNAME'] . "</TD>";
			echo "  <TD>" . $row_data->form . "</TD>";
			//
			//2018-03-10 DJT Added state to report.
			echo "  <TD>" . $choices[$state] . "</TD>";
			echo "  <TD align='right'>$" . $row_data->amount . "</TD>";
			echo "</TR>";
		}

		?>
		</TBODY>

	</table>
</div>

<script>

	jQuery(document).ready(function(){

		var datepickerOptions = {
			dateFormat : 'yy/mm/dd',
			defaultDate: 0,
			beforeShow: function(event, ui) {
			   jQuery('#ui-datepicker-div').addClass('open');
			},
			onClose: function(event, ui) {
				jQuery('#ui-datepicker-div').removeClass('open');
			}
		};

		jQuery('#start').datepicker(datepickerOptions);
		jQuery('#end').datepicker(datepickerOptions);
		jQuery('#start').change(function(){
			var endDate = jQuery('#start').val();
			jQuery.extend(datepickerOptions, {
				defaultDate: endDate,
				minDate: endDate
			});
			jQuery('#end').datepicker(datepickerOptions);
		});
	});

</script>
