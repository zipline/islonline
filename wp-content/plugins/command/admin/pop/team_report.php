<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');


//
//Load Classes
require_once (CC_CLASSES_DIR . "country.class.php");
require_once (CC_CLASSES_DIR . "program.class.php");
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "team.support.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "user.class.php");
require_once (CC_CLASSES_DIR . "docs.class.php");


$team = new TEAM();
$support = new TEAM_SUPPORT();
$page = new TAB_MANAGE();
$program = new PROGRAM();
$country = new COUNTRY();
$rsvp = new RSVP();
$user = new USER();
$docs = new DOCS();

global $wpdb;
$team->set_team($_GET['team_id']);
$team_name = $team->value('code') . " / " . $team->value('hash');

$post_id = $team->value('post');
$printable = true;

//
//Team Information page
$points_of_contact = $team->get_poc();
	
//
//Schedule
if($team->get_schedule() == "Unknown")
{
	$team->blank_schedule(true);
}
$current = $team->get_schedule();

foreach($current as $segment_id => $segment_data)
{
	$sched[$segment_id]['country'] = $segment_data['country'];
	$sched[$segment_id]['region'] = $segment_data['region'];
	$sched[$segment_id]['program'] = $segment_data['program'];
	$sched[$segment_id]['start'] = $segment_data['start_date'];
	$sched[$segment_id]['end'] = $segment_data['end_date'];
	$sched[$segment_id]['duration'] = $segment_data['duration'];
}
?>
<style type="text/css" media="screen">
img.print {display: none; width: 0;}
</style>
<script>
    window.print();
</script>
<style type="text/css" media="print">
    @page { size: landscape; }
    #wpadminbar,
    #adminmenumain,
    #wpfooter,
    .page-title-action,
    .back-to-list-arrow,
    .update-nag {
        display: none !important;
        }
    #wpcontent {
        margin-left: 0 !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        }
    html.wp-toolbar {padding-top: 0 !important;}

    table.standard-large-table th.medium {width: 40px;}

    .team-info-wrapper.for-print,
    .form-table.team-info,
    .form-table.team-overview {
        padding: 20px;
        width: 100%;
        border: 0 !important;
        border-top: 3px solid black;
        line-height: 2rem;
    }

    .team-info-wrapper.for-print li,
    .form-table.team-overview tr {
        border-bottom: 1px solid #ccc;
    }

    {background-color: white !important; float: none !important;}

    .print-section {
        display: block;
        page-break-before: always !important;
        }

    h1 {font-weight: bold !important;}

    img.print {display: block; width: 100px;}
    div.avatar {display: none;}

</style>
<div class="page-header wrap">
    <a class="back-to-list-arrow" href='javascript: window.close();'>
        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
    </a>

    <h1><?php _e($team->value('hash'))?></h1>
    
    <a class='page-title-action' href="javascript:window.print();" target='_blank'>Print</a>
</div>

<table class="form-table team-info">
	<TR>
		<TH scope="row">Team Code (automatic)</TH>
		<TD><?php _e($team->value('code'))?></TD>
	</TR>
	<TR>
		<TH scope="row">Hash Tag</TH>
		<TD><?php _e($team->value('hash'))?></TD>
	</TR>

	<TR>
   		<TH scope="row">Schedule</TH>
		<TD>
		<table id="schedule_table">
            <TR><TH style='<?= $w?>'>Country</TH><TH>Region</TH><TH>Program</TH><TH>Start Date</TH><TH>End Date</TH><TH>Days</TH></TR>
    
            <?php
			foreach($sched as $segment_id => $segment_data)
            {
				$segment_save = true;
                $country->set_country($segment_data['country']);	
				$country->set_region($segment_data['region']);
                $program->set_program($segment_data['program']);
				
            	?>	
                <TR>
                    <TD>
                        <?php echo $country->get_name(); ?>
                    </TD>
                    <TD>
                        <?php echo $country->region_details()['title']; ?>
                    </TD>
                    <TD>
                        <?php echo $program->get_name()?>
                    </TD>
                    <TD>
                    	<?php echo isset($segment_data['start']) ? $segment_data['start'] : '' ?>
                    </TD>
                    <TD>
                    	<?php echo isset($segment_data['end']) ? $segment_data['end'] : '' ?>
                    </TD>
                    <TD>
                       <?php echo isset($segment_data['duration']) ? $segment_data['duration'] : '' ?>
                    </TD>
                </TR>
                <?php
            }
            ?>
        </table>
	
	</TD></TR>    

	<TR>
		<TH>Team Type</TH>
		<TD><?php echo $support->type_value($team->value('type')) ?></TD>
	</TR>
    <TR>    
		<TH>Status</TH>
		<TD><?php echo $support->status_value($team->value('status'))?></TD>
	</TR>
	<TR>
		<TH scope="row">Cost</TH>
		<TD>$<?php echo $team->value('cost'); ?></TD>
	</TR>
	<TR>
		<TH scope="row">Application Fee</TH>
		<TD>$<?php echo $team->value('appfee'); ?></TD>
	</TR>
	<TR>
		<TH scope="row">Arrival Country</label></TH>
		<TD><?php echo $team->value('arrival_country')?></TD>
	</TR>
	<TR>
		<TH scope="row">Arrival City</TH>
		<TD><?php echo $team->value('arrival_city')?></TD>
	</TR>
	<TR>
		<TH scope="row">Departure Country</TH>
		<TD><?php echo $team->value('departure_country')?></TD>
	</TR>
	<TR>
		<TH scope="row">Departure City</TH>
		<TD><?php echo $team->value('departure_city')?></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="poc_table">Point of Contact</label></TH>
		<TD>
            <table id="poc_table">    
            <?php
                $possibles = $user->fetch_poc($team->value('post'));
				$counter = 1;
				
				if(!empty($possibles))
				{
					foreach($possibles as $email=>$data)
					{
						echo "<TR>";
						echo "  <TD>" . $data['first_name'] . "</TD>";
						echo "  <TD>" . $data['last_name'] . "</TD>";
						echo "  <TD>" . $email . "</TD>";
						echo "</TR>";
					}
				}
            ?>
			</table>
		</TD>
	</TR>
</table>

<div class="print-section">
    <h3>Team Members</h3>
    <table class="standard-large-table form-table team-overview striped">
    	<thead>
    		<TR>
    			<TH class="medium">Picture</TH>
    	    	<TH class='large'>Name</TH>
    	        <TH class="small">Gender</TH>
    	        <TH class="small">School</TH>
    	        <TH class="small">App Date</TH>
    	    </TR>
    	</thead>
        
        <?php
    	foreach($rsvp->list_rsvp_by_team($team->get_team()) as $index=>$data)
    	{

    		$member_id = $data['user_id'];
    		$rsvp->set_rsvp_id($data['id']);
    		$rsvp_data = $rsvp->fetch_rsvp();
    		
    		if(!empty($rsvp_data))
    		{
    			
    			$post_id = $rsvp_data['post_id'];

    			//
    			//Name Information
				$user_info = get_userdata($member_id);
    			if(!empty($user_info))
    			{
    				$user_name = ($member_id > 0) ? $user_info->first_name . " " . $user_info->last_name : 'Unassigned';
    				$user_email = ($member_id > 0) ? $user_info->user_email : '';

    				//
    				//Gender
    				$gender = get_field('gender', 'user_' . $member_id);

    				//
    				//Profile
    				$the_school = isset($post_id) ? get_field('school', $post_id) : '';

    				echo "<TR>";
    				echo "	<TD><div class='avatar' style='background: url(" . $user->avatar($member_id) . "); background-size: cover;'></div></a><img class='print' src='". $user->avatar($member_id) ."' /></TD>";
    				echo "	<TD>" . $user_name . "</TD>";
    				echo "	<TD>" . $gender . "</TD>";
    				echo "	<TD>" . $the_school . "</TD>";
    				echo "	<TD>" . $rsvp_data['purchase_date'] . "</TD>";
    				echo "</TR>";

    			}
    		}
    	}
    	?>
    </table>
</div>
<?php
include(CC_ADMIN_DIR . "tabs/team.reports.php");

echo "<div class='print-section'>";
echo "<h3>Add-ons</h3>";
echo "<TABLE>";

//
//Add-ons
$query = "SELECT 
			coi.id, coi.order_id, main.user_id, p.post_title 
		  FROM 
		  	" . $wpdb->prefix . "command_order_items coi INNER JOIN
		  	" . $wpdb->prefix . "posts p on coi.item_id = p.ID INNER JOIN 
			" . $wpdb->prefix . "command_orders co on coi.order_id = co.id INNER JOIN
			(SELECT coi.id, rsvp.user_id 
				FROM " . $wpdb->prefix . "command_order_items coi INNER JOIN
				" . $wpdb->prefix . "command_RSVP rsvp on coi.id = rsvp.order_item_id
				WHERE item_id = " . $team->value('id') . " AND item_type = 1 and quantity > 0) main ON coi.add_on = main.id 
		  WHERE 
		  	coi.quantity > 0 AND
			coi.item_type = 4
		 ORDER BY
		 	main.user_id";
$results = $wpdb->get_results($query, OBJECT_K);

if(!empty($results))
{
	foreach($results as $id => $data)
	{
		$user_info = get_userdata($data->user_id);

		echo "<TR>";
		echo "<TD><B>" . $user_info->first_name . ' ' . $user_info->last_name . "</B>:</TD>";
		echo "<TD>" . $data->post_title . "</TD>";
		echo "<TR>";
		
	}
	
}

echo "</TABLE>";