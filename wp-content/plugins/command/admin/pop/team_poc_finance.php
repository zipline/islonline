<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Required Classes
if(isset($_POST['refund_button']) && $_POST['refund_button'] == "Process Refund")
{
	require_once CC_CLASSES_DIR . "paypal/config.php";
	require_once CC_CLASSES_DIR . "paypal/autoload.php";
	require_once CC_CLASSES_DIR . "paypal/PayPal/PayPal.php";
	require_once CC_CLASSES_DIR . "paypal/PayPal/PayFlow.php";
}

require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "team.support.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "finance.class.php";
require_once CC_CLASSES_DIR . "country.class.php";
require_once CC_CLASSES_DIR . "user.class.php";

$team = new TEAM();
$rsvp = new RSVP();
$finance = new FINANCE();
$country = new COUNTRY();
$user = new USER();
$support = new TEAM_SUPPORT();

//
//Check if Enhanced permissions
$team->set_team($_GET['team']);
$team_name = $team->value('code') . " / " . $team->value('hash');

$post_id = $team->value('post');

$usr = wp_get_current_user();
if($user->user_group($usr->ID) == "enhanced")
{
	$allow = false;
	if( get_user_meta( $usr->ID, 'cmd_custom_team_poc', true))
	{
		$poc_data = get_post_meta($post_id, 'cmd_team_poc', false);
		if($poc_data)
		{
			foreach($poc_data[0] as $email=>$name_info)
			{
				if($email == $usr->user_email)
				{
					$allow = true;
				}
			}
		}
	}

	if(!$allow)
	{
		echo "<script>";
		echo "window.location = 'admin.php?page=cc-teams'";
		echo "</script>";
		wp_die();
	}

}


if(!empty($_POST['res_list']))
{
	$reservations = $_POST['res_list'];
}
else
{
	if(isset($_REQUEST['rsvp']))
	{
		$reservations = array($_REQUEST['rsvp']);
	}
	else
	{
		$reservations = array();
	}
}

if(!empty($_REQUEST['emailto']) && !empty($_REQUEST['sendit']) && $_REQUEST['sendit']=='Send Invoice')
{
	$user = new USER();
	$current = wp_get_current_user();
	
	$team->set_team($_GET['team']);
	//
	//2018-03-23 Removed for new send to features
	/*
	$possibles = $user->fetch_poc($team->value('post'));

	if(!empty($possibles))
	{
		foreach($possibles as $e_mail=>$data)
		{
			$check = 'Email ' . $data['first_name'] . ' ' . $data['last_name'];
			
			if($check == $_REQUEST['email_who'])
			{
				$to = $data['first_name'] . ' ' . $data['last_name'] . ' <' . $e_mail . '>';
				
				$header[] = 'CC: ' . $current->display_name . ' <' . $current->user_email . '>';
				$header[] = 'From: ' . $current->display_name . ' <' . $current->user_email . '>';
				$header[] = 'Reply-to: ' . $current->display_name . ' <' . $current->user_email . '>';
				
				$subject = 'ISL Invoice';
				
				$message = '<!DOCTYPE html>';
				$message .= '  <HTML>';
				$message .= '    <HEAD>';
				$message .= '        <Title>ISL Invoice</Title>';
				$message .= '    </HEAD>';
				$message .= '    <BODY>';
				$message .=        $finance->get_invoice($_REQUEST['team'], $reservations);
				$message .= '    </BODY>';
				$message .= '  </HTML>';

				wp_mail($to, $subject, $message, $header);
			}
		}
	}
	*/

	//
	//2018-03-23 New Mesage

	//
	//Fix sendto
	$sendto = urlencode($_REQUEST['sendto']);
	$sendto = str_replace('%0D%0A', '<BR>', $sendto);
	$sendto = str_replace('++', '', $sendto);
	$sendto = urldecode($sendto);

	$to = $_REQUEST['emailto'];

	$header[] = 'CC: ' . $current->display_name . ' <' . $current->user_email . '>';
	$header[] = 'From: ' . $current->display_name . ' <' . $current->user_email . '>';
	$header[] = 'Reply-to: ' . $current->display_name . ' <' . $current->user_email . '>';

	$subject = 'ISL Invoice';

	$message = '<!DOCTYPE html>';
	$message .= '  <HTML>';
	$message .= '    <HEAD>';
	$message .= '        <Title>ISL Invoice</Title>';
	$message .= '    </HEAD>';
	$message .= '    <BODY>';
	$message .=        $finance->get_invoice($_REQUEST['team'], $reservations, $_REQUEST['terms'], $sendto);
	$message .= '    </BODY>';
	$message .= '  </HTML>';
	
	wp_mail($to, $subject, $message, $header);
}

?>
<?php //wp_head(); ?>

<h1>Invoice Builder</h1>
						
<div id="inner-content" class="row invoice-builder">
	<main id="main" class="large-8 medium-8 columns" role="main">
		<form name="f-data" method="POST" enctype="multipart/form-data" action="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>">
			<table>
				<tr>
                	<th>Check to Include</th>
                    <th colspan=2>Item</th>
                    <th>Origination</th>
                    <th class="text-right">Reservation Amount</th>
                    <th class="text-right">Balance Due</th>
                </tr>
                
				<?php

				if(!empty($_REQUEST['rsvp']))
				{
					$which_team = $rsvp->team($_REQUEST['rsvp']);
				}
				else
				{
					$which_team = $_REQUEST['team'];
				}
	
				foreach($rsvp->list_rsvp_by_team($which_team) as $cur_rsvp => $cur_rsvp_data)
                {
                    $this_rsvp = new RSVP();
                    
                    $this_rsvp->set_rsvp_id($cur_rsvp);
                    $this_rsvp->fetch_rsvp();
					$this_rsvp_data = $this_rsvp->get_rsvp();
					
					$user = get_userdata($this_rsvp_data['user_id']);
					$item = $finance->fetch_item($this_rsvp_data['order_item_id']);
					
					$purchaser = get_userdata($this_rsvp_data['purchaser']);
					$purchased_by = $purchaser->last_name . ', ' . $purchaser->first_name;	
					
					if(!empty($user))
					{
						$user_name = $user->last_name . ', ' . $user->first_name;	
					}
					else
					{
						$user_name = "Unassigned";
					}
					
					$payments = $finance->payments(array($this_rsvp_data['order_item_id']), true);
					$pays = !empty($payments[0]['amount']) ? $payments[0]['amount'] : 0;
					
                   	echo '<tr>';
            		echo '  <td><input type="checkbox" name="res_list[]" id="res_list_' . $cur_rsvp . '" value="' . $cur_rsvp . '"' . (!empty($_POST['res_list']) && in_array($cur_rsvp, $_POST['res_list']) ?  ' checked' : '') . '></td>';
            		echo '  <td colspan=2>RSVP (' . $cur_rsvp . '): ' . $user_name . '</td>';
            		echo '  <td>' . $purchased_by . '</td>';
                	echo '  <td class="text-right">$' . number_format($item['price'] * $item['quantity'],2) . '</td>';
                	echo '  <td class="text-right">$' . number_format($item['price'] * $item['quantity'] - $pays, 2) . '</td>';
                	echo '</tr>';

                }
                ?>
			</table>	

			<?php
			//
			//2018-03-23 DJT Added for new send block
			echo '<div style="padding: 10px; border: 1px solid #ccc; margin: 10px 0 20px 0;">';
			echo '    <div style="display:grid;grid-template-columns: 1fr 1fr 1fr;line-height:1.2;width:100%;margin: 10px 0;">';
			echo '      <div style="width:100%;">';
			echo '        Invoice For: <BR>';
			echo '        <textarea cols=50 rows=6 name="sendto" id="sendto" required>' . (isset($_REQUEST['sendto']) ? $_REQUEST['sendto'] : '') . '</textarea>';
			echo '      </div>';
			echo '      <div style="margin-left: 10px;">';
			echo '        Send Email To: <BR>';
			echo '        <input style="width: 97%;" type="text" size=50 name="emailto" id="emailto" required value="' . (isset($_REQUEST['emailto']) ? $_REQUEST['emailto'] : '') . '"></input>';
			echo '      </div>';
			echo '      <div style="margin-right:20px;width:100%;">';
			echo '        Terms: <BR>';
			echo '        <input style="width: 97%;" type="text" size=50 name="terms" id="terms" required value="' . (isset($_REQUEST['terms']) ? $_REQUEST['terms'] : '') . '"></input>';
			echo '      </div>';
			echo '    </div>';
			echo '</div>';
			
			?>
			
			<input class="button" type="submit" id="viewit" name="viewit" value="View Invoice">
			<input class="button" type="submit" id="sendit" name="sendit" value="Send Invoice">

            <?php
			
			//
			//2018-03-23 DJT Removed old POC email options, replaced with customizable options.
			//$user = new USER();
			//$team->set_team($_REQUEST['team']);
			//$possibles = $user->fetch_poc($team->value('post'));

			//if(!empty($possibles))
			//{
			//	foreach($possibles as $email=>$data)
			//	{
			//		echo '<input class="button" type="submit" name="email_who" value="Email ' . $data['first_name'] . ' ' . $data['last_name'] . '">';
			//	}
			//}

			if(isset($_REQUEST['sendto']))
			{
				//
				//Fix sendto
				$sendto = urlencode($_REQUEST['sendto']);
				$sendto = str_replace('%0D%0A', '<BR>', $sendto);
				$sendto = str_replace('++', '', $sendto);
				$sendto = urldecode($sendto);
				$_REQUEST['sendto'] = $sendto;
			}
            ?>

		</form>            
	</main>
</div>

<?php 
echo var_dump($_REQUEST);

if(isset($_REQUEST['sendto']) && isset($_REQUEST['team']))
{
	echo $finance->get_invoice($_REQUEST['team'], $reservations, $_REQUEST['terms'], $_REQUEST['sendto']);
}
?>

<?php wp_footer() ?>