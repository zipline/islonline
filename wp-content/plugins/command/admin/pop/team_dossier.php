<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');


//
//Load Classes
require_once (CC_CLASSES_DIR . "country.class.php");
require_once (CC_CLASSES_DIR . "program.class.php");
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "team.support.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "user.class.php");
require_once (CC_CLASSES_DIR . "docs.class.php");

//
//Setup classes
$team = new TEAM();
$support = new TEAM_SUPPORT();
$page = new TAB_MANAGE();
$program = new PROGRAM();
$country = new COUNTRY();
$rsvp = new RSVP();
$user = new USER();
$docs = new DOCS();

//
//Initial variables
global $wpdb;
$team->set_team($_GET['team_id']);
$team_name = $team->value('code') . " / " . $team->value('hash');
$printable = true;


//
//PAge breaks
echo "<style>";
echo "  @media print{ 
			#volunteer_page {page-break-after: always;}
			#adminmenumain {display: none;}
			#wpadminbar {display: none;}
			}";
echo "</style>";


//
//Cycle through all Team Members
foreach($rsvp->list_rsvp_by_team($team->get_team()) as $index=>$data)
{

	$member_id = $data['user_id'];
	$rsvp->set_rsvp_id($data['id']);
	$rsvp_data = $rsvp->fetch_rsvp();

	if(!empty($rsvp_data))
	{

		echo '<div class="volunteer_page">';

		$post_id = $rsvp_data['post_id'];

		//
		//Name Information
		$user_info = get_userdata($member_id);
		if(!empty($user_info))
		{
			//
			//Name and Email
			$user_name = ($member_id > 0) ? $user_info->first_name . " " . $user_info->last_name : 'Unassigned';
			$user_email = ($member_id > 0) ? $user_info->user_email : '';

			//
			//Passport
			$number = get_post_meta($post_id, '_cmd_passport_num', true);
			$country = get_post_meta($post_id, '_cmd_passport_country', true);
			$issue = get_post_meta($post_id, '_cmd_passport_issue', true);
			$expire = get_post_meta($post_id, '_cmd_passport_expire', true);

			//
			//Team Leader
			$team_leader_id = get_post_meta($post_id, '_cmd_team_leader', true);
			
			//
			//Notes
			$volunteer_note = get_user_meta($member_id, $team->get_team() ."_volunteer_note", true);
			$volunteer_note = !empty($volunteer_note)? $volunteer_note : '<span class="placeholder-text">Enter notes here</span>';
			$volunteer_note = '<span class="volunteer_note note_field" style="cursor: pointer;" onclick="editVolunteerNote('. $member_id .');">'. $volunteer_note .'</span>';


			echo "<TABLE>";
			echo "  <TR>";
			echo "    <TD rowspan=6 colspan=2 width=200 style='padding-right:20px'>
						<img src='" . $user->avatar($member_id) . "' height=200 width=200 style='border-radius: 25px;'>
					  </TD>";
			echo "    <TD colspan=4 width=400 style='font-size:400%'>". $user_name . "</H1></TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD><B>Address: </B></TD>";
			echo "    <TD colspan=3>";
			echo        get_field('mailing_address_line_1', 'user_' . $member_id) . "<BR>" . 
			        	get_field('city', 'user_' . $member_id) . ", " . 
						get_field('state', 'user_' . $member_id) . " " . 
						get_field('zip_code', 'user_' . $member_id);
			echo "    </TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD><B>School: </B></TD>";
			echo "    <TD colspan=3>" . get_field('school', 'post_' . $post_id) . "</TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD><B>Email: </B></TD>";
			echo "    <TD colspan=3>" . $user_email . "</TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD><B>Gender: </B></TD>";
			echo "    <TD colspan=3>" . get_field('gender', 'user_' . $member_id) . "</TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD><B>Birthdate: </B></TD>";
			echo "    <TD colspan=3>" . get_field('birthdate', 'user_' . $member_id) . "</TD>";
			echo "  </TR>";
			
			echo "  <TR><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>";

			echo "  <TR>";
			echo "    <TD colspan=6><B>Passport</B></TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD>Country: </TD>";
			echo "    <TD>" . $country . "</TD>";
			echo "    <TD>&nbsp;</TD>";
			echo "    <TD>Number: </TD>";
			echo "    <TD>" . $number . "</TD>";
			echo "    <TD>&nbsp;</TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD>Issued: </TD>";
			echo "    <TD>" . $issue . "</TD>";
			echo "    <TD>&nbsp;</TD>";
			echo "    <TD>Expires: </TD>";
			echo "    <TD>" . $expire . "</TD>";
			echo "    <TD>&nbsp;</TD>";
			echo "  </TR>";
			
			echo "  <TR><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>";

			echo "  <TR>";
			echo "    <TD colspan=3><B>Dietary Restrictions</B></TD>";
			echo "    <TD colspan=3><B>Allergies</B></TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD colspan=3>" . get_field('dietary_restrictions', 'post_' . $post_id) . "</TD>";
			echo "    <TD colspan=3>" . get_field('food_allergies', 'post_' . $post_id) . "</TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD colspan=3><B>Medications</B></TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD colspan=3>" . get_field('medications_taken', 'post_' . $post_id) . "</TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD colspan=3><B>Professional Experience</B></TD>";
			echo "    <TD colspan=3><B>Experience</B></TD>";
			echo "  </TR>";
			echo "  <TR>";
			echo "    <TD colspan=3>" . get_field('professional_experience', 'post_' . $post_id) . "</TD>";
			echo "    <TD colspan=3>" . get_field('academic_courses', 'post_' . $post_id) . "</TD>";
			echo "  </TR>";
			
			
			echo "  <TR><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>";

			//echo "  <TR><TD>1</TD><TD>2</TD><TD>3</TD><TD>4</TD><TD>5</TD><TD>6</TD></TR>";
			

			echo "</TABLE>";
			
		}
			
		echo "</div>";
		
	}	// End if(!empty($RSVP))

}	//End foreach(RSVP)

?>

<script>
	window.print();
</script>
