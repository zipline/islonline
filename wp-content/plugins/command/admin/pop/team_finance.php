<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Required Classes
if(isset($_POST['refund_button']) && $_POST['refund_button'] == "Process Refund")
{
	require_once CC_CLASSES_DIR . "paypal/config.php";
	require_once CC_CLASSES_DIR . "paypal/autoload.php";
	require_once CC_CLASSES_DIR . "paypal/PayPal/PayPal.php";
	require_once CC_CLASSES_DIR . "paypal/PayPal/PayFlow.php";
}

require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "finance.class.php";
require_once CC_CLASSES_DIR . "country.class.php";
require_once CC_CLASSES_DIR . "email.class.php";
require_once CC_CLASSES_DIR . "user.class.php";
require_once CC_CLASSES_DIR . "message.class.php";

$team = new TEAM();
$rsvp = new RSVP();
$finance = new FINANCE();
$country = new COUNTRY();
$email = new EMAIL();
$user = new USER();
$message = new MESSAGES();

//
//Check if Enhanced permissions
$team->set_team($_GET['team']);
$team_name = $team->value('code') . " / " . $team->value('hash');

$post_id = $team->value('post');

$usr = wp_get_current_user();
if($user->user_group($usr->ID) == "enhanced")
{
	$allow = false;
	if( get_user_meta( $usr->ID, 'cmd_custom_team_poc', true))
	{
		$poc_data = get_post_meta($post_id, 'cmd_team_poc', false);
		if($poc_data)
		{
			foreach($poc_data[0] as $email=>$name_info)
			{
				if($email == $usr->user_email)
				{
					$allow = true;
				}
			}
		}
	}

	if(!$allow)
	{
		echo "<script>";
		echo "window.location = 'admin.php?page=cc-teams'";
		echo "</script>";
		wp_die();
	}

}
$edit_finance = false;
if($user->user_group($usr->ID) == "staff" || $user->user_group($usr->ID) == "admin" || $user->user_group($usr->ID) == "super")
{
	$edit_finance = true;
}

?>
<h1>Edit Financials</h1>
<?php


if(isset($_POST['refund_button']) && $_POST['refund_button'] == "Process Refund" && $edit_finance)
{
	foreach($_POST as $id=>$value)
	{
		if(substr($id,0,10) == "payment_id" && (int)$value > 0)
		{
			$element = substr($id,11);

			$payment_details = $finance->payments_by_id(array($element));
			$transact_details = $finance->get_transaction(array($payment_details['transaction_id']));


			switch($transact_details['form'])
			{
				case 'No Payment':
					$write = false;
					break;

				case 'Manual':
				case 'Manual Adjustment':
					$write = true;
					$PayPalResult['RESPMSG'] = 'Approved';
					//
					//2018-02-27 DJT updated preg_replace with left bracket, changed from number to money
					//               Added new fields to trans_data
					//$trans_data = 	array('amount' => (number_format( preg_replace('/[0-9.]*/', '', $value),2) * (-1)),
					$trans_data = 	array('amount' => number_format(preg_replace('/[^\-0-9.]*/', '', $value) * (-1), 2,'.',''),
										  'form' => $transact_details['form'],
										  'response' => 'Approved',
										  'transaction_id' => $transact_details['transaction_id'],
										  'raw_request' => 'Refund: ' . $transact_details['raw_request'],
										  'raw_response' => 0,
				  						  'ip_address' => $_SERVER['REMOTE_ADDR'],
										  'user_agent' => $_SERVER['HTTP_USER_AGENT'],
										  'pp_trans_id' => "No Ref"

										  );
					break;

				default:
					$write = true;

					//
					//If we have a real transaction
					if($transact_details['response'] == 'Approved')
					{

						//
						//Setup PayPal if not setup already
						if(empty($payPalConfig))
						{
							$PayPalConfig = array(
												'Sandbox' => $sandbox,
												'APIUsername' => $payflow_username,
												'APIPassword' => $payflow_password,
												'APIVendor' => $payflow_vendor,
												'APIPartner' => $payflow_partner,
												'PrintHeaders' => $print_headers,
												'LogResults' => $log_results,
												'LogPath' => $log_path,
												);
						}

						//
						//Instantiate PayPal object
						$PayPal = new angelleye\PayPal\PayFlow($PayPalConfig);

						//
						//Set basic credit card details
						//
						//2018-02-22 DJT Changed formatting for AMT to standard
						$CCDetails = array(
											'TENDER' => 'C',			 										// Required
											'TRXTYPE' => 'C',			 										// Required
											'ORIGID' => $transact_details['transaction_id'], 					// Required
//											'AMT' => number_format(preg_replace('/0-9.]*/', '', $value),2)									// Required
											'AMT' => number_format(preg_replace('/[^\-0-9.]*/', '', $value), 2)
										);

						$PayPalRequestData = $CCDetails;
						$PayPalResult = $PayPal->ProcessTransaction($PayPalRequestData);

						//
						//2018-02-27 DJT updated preg_replace with left bracket, changed from number to money
						//               Added to transaction fields
						//$trans_data = 	array('amount' => (number_format( preg_replace('/[0-9.]*/', '', $value),2) * (-1)),
						$trans_data = 	array('amount' => number_format(preg_replace('/[^\-0-9.]*/', '', $value)  * (-1), 2,'.',''),
											  'form' => $transact_details['form'],
											  'response' => $PayPalResult['RESPMSG'],
											  'transaction_id' => $PayPalResult['PNREF'],
											  'raw_request' => $PayPalResult['RAWREQUEST'],
											  'raw_response' => $PayPalResult['RAWRESPONSE'],
					  						  'ip_address' => $_SERVER['REMOTE_ADDR'],
											  'user_agent' => $_SERVER['HTTP_USER_AGENT'],
											  'pp_trans_id' => empty($PayPalResult['PPREF']) ? "No Ref" : $PayPalResult['PPREF']

											  );
					}
					break;

			}	//End Case

 			if($write && $trans_data['response']=='Approved')
			{
				//
				//Write Transaction Record
				$finance->new_transaction($trans_data, 'refund', $transact_details['action'] );

				if(!empty($PayPalResult['RESPMSG']) && $PayPalResult['RESPMSG'] === 'Approved')
				{
					$finance->set_order_id($payment_details['order_id']);

					//
					//2018-02-27 Changed value to standard format
					$finance->payment($payment_details['order_item_id'],
//									  number_format(preg_replace('/0-9.]*/', '', $value),2) * (-1),
									  number_format(preg_replace('/[^\-0-9.]*/', '', $value) * (-1), 2,'.',''),
									  $transact_details['ind'],
									  ($payment_details['special'] == "donate" || $payment_details['special'] == "donate anyonymous" ? "donate refund" : '')
									 );

				}

			}
			else
			{
				$error = $trans_data['response'];
			}
		}
	}

	//
	//check to see if there's a balance due, that's not from fees. If balance due is 0, then cancel RSVP
	$check_rsvp = new RSVP();
	$check_rsvp->set_rsvp_id($_REQUEST['rsvp']);
	$check_rsvp->fetch_rsvp();

	$check_res = $check_rsvp->get_rsvp();

	$check_finances = new FINANCE();
	$check_finances->Fetch_Items(array($check_res['order_item_id']));

	//
	//If we've reduce the amount paid on the rsvp to below $1, then we should kill the order.
	if($check_finances->Calculate_ItemsPaid() < 1 || strtolower($_REQUEST['force_cancel'])=='yes')
	{
		//
		//Cancel RSVP
		$finance->cancel_item($check_res['order_item_id'], 'Cancelled due to refund.');

		/*
		$message->set_recipient($_REQUEST['member']);
		$message->set_team_id($_REQUEST['team']);
		$message->set_subject('Cancellation Processed');
		$message->set_message_text('Your Cancellation Request has been processed and a refund issued.');
		$cur_user = wp_get_current_user();
		$message->set_sender($cur_user->ID);
		$message->add_message('Workaround');
		*/

		//
		//2018-04-06 DJT Wrapped send with new send_notice check
		if(isset($_REQUEST['send_notice']) && $_REQUEST['send_notice'] == "on")
		{
			$user_info = get_user_by('ID', $_REQUEST['member']);
			$email->to($user_info->display_name, $user_info->user_email);
			$email->from('notice@islonline.org', 'notice@islonline.org');
			$email->subject('Cancellation from ISL');
			//$email->message('Your cancelation request for ' . $team_name . ' has been processed.
			//It may take a few days for your financial institution to register the refund.');
			$email->message(email_cancellation_confirmation($user_info->display_name, $team_name));

			if(!$email->send())
			{
				echo "Email failed";
			}
			else
			{
				echo "Email sent to: " . $user_info->user_email;
			}
		}

		$add_ons = $check_finances->add_ons($check_res['order_item_id']);

		foreach($add_ons as $index => $addon_data)
		{
			$check_finances->Fetch_Items(array($addon_data['id']));

			//
			//If we've reduce the amount paid on the rsvp to below $1, then we should kill the order.
			if($check_finances->Calculate_ItemsPaid() < 1 )
			{
				//
				//Cancel Add-on
				$finance->cancel_item($addon_data['id'], 'Cancelled due to refund.');
			}
		}

	}

}

if(!empty($_POST['cred_reason']) && !empty($_POST['cred_amt']) && isset($_POST['Pay_Style']) && $edit_finance)
{
	if($_POST['Pay_Style'] == "Apply Payment")
	{
		$_POST['cred_amt'] = $_POST['cred_amt'] * (-1);
	}

	//
	//Write Transaction Record
	//
	//2018-02-27 DJT Adjuste to standard forumcla
	//               Added new finance fields
//	$trans_data = 	array('amount' => preg_replace('/0-9.]*/', '', $_POST['cred_amt']),
	$trans_data = array('amount'=> number_format(preg_replace('/[^\-0-9.]*/', '', $_POST['cred_amt']) * (-1), 2,'.',''),
						'form' => 'Manual',
						  'response' => 'Manual',
						  'transaction_id' => 'Manual',
						  'raw_request' => 'Reason=>' . $_POST['cred_reason'] . ", Amount=>" . $_POST['cred_amt'],
						  'raw_response' => 'Accepted',
						  'ip_address' => $_SERVER['REMOTE_ADDR'],
						  'user_agent' => $_SERVER['HTTP_USER_AGENT'],
						  'pp_trans_id' => empty($PayPalResult['PPREF']) ? "No Ref" : $PayPalResult['PPREF']

						  );
		$finance->new_transaction($trans_data);

	$rsvp->set_rsvp_id($_REQUEST['rsvp']);
	$rsvp->fetch_rsvp();

	$reservation = $rsvp->get_rsvp();
	$finance->set_order_id($reservation['order_id']);

	//
	//2018-02-27 DJT changed to standard format
	$finance->payment($reservation['order_item_id'],
//					  preg_replace('/0-9.]*/', '', $_POST['cred_amt']) * (-1),
					  number_format(preg_replace('/[^\-0-9.]*/', '', $_POST['cred_amt'])* (-1), 2,'.',''),
					  NULL,
					  $_POST['cred_reason']
					  );
}

if(!empty($_POST['adj_reason']) && !empty($_POST['adj_amt']) && $edit_finance && empty($_POST['adj_id']))
{
	//
	//Write Transaction Record
	//
	//2018-02-17 DJT updated to starndard format
	//$trans_data = 	array('amount' => preg_replace('/0-9.]*/', '', $_POST['adj_amt']),
	$trans_data = array('amount' => number_format(preg_replace('/[^\-0-9.]*/', '', $_POST['adj_amt']),2,'.',''),
						  'form' => 'Manual Adjustment',
						  'response' => 'Manual',
						  'transaction_id' => 'Manual',
						  'raw_request' => 'Reason=>' . $_POST['adj_reason'] . ", Amount=>" . ($_POST['adj_amt'] * (-1)),
						  'raw_response' => 'Accepted',
						  'ip_address' => $_SERVER['REMOTE_ADDR'],
						  'user_agent' => $_SERVER['HTTP_USER_AGENT'],
						  'pp_trans_id' => "No Ref"

						  );
	$finance->new_transaction($trans_data);

	$rsvp->set_rsvp_id($_REQUEST['rsvp']);
	$rsvp->fetch_rsvp();

	$reservation = $rsvp->get_rsvp();
	$finance->set_order_id($reservation['order_id']);

	//
	//2018-02-27 djt updated to starndard format
	$finance->payment($reservation['order_item_id'],
		//preg_replace('/0-9.]*/', '', $_POST['adj_amt']) * (-1),
		number_format(preg_replace('/[^\-0-9.]*/', '', $_POST['adj_amt']) * (-1), 2,'.',''),
		NULL,
		$_POST['adj_reason']
	);
} else if(!empty($_POST['adj_id']) && !empty($_POST['adj_reason']) && !empty($_POST['adj_amt']) && $edit_finance) {
	
	if (isset($_POST['delete_adj'])) {
		$finance->delete_payment($_POST['adj_id']);
	} else {
		$finance->update_payment(
			$_POST['adj_id'],
			number_format(preg_replace('/[^\-0-9.]*/', '', $_POST['adj_amt']) * (-1), 2,'.',''),
			$_POST['adj_reason']
		);
	}
	
	//
	//Edit Transaction Record
	//

//	$trans_data = array(
//		'id' => $_POST['adj_id'],
//		'amount' => number_format(preg_replace('/[^\-0-9.]*/', '', $_POST['adj_amt']),2,'.',''),
//		'reason' => $_POST['adj_reason'],
//		'form' => 'Manual Adjustment',
//		'response' => 'Manual',
//		'transaction_id' => 'Manual',
//		'raw_request' => array(
//			'Reason' => $_POST['adj_reason'],
//			'Amount' => ($_POST['adj_amt'] * (-1)),
//			),
//		'raw_response' => 'Accepted',
//		'ip_address' => $_SERVER['REMOTE_ADDR'],
//		'user_agent' => $_SERVER['HTTP_USER_AGENT'],
//		'pp_trans_id' => "No Ref"
//	);

//	$finance->update_transaction($trans_data); ?
//
//	$rsvp->set_rsvp_id($_REQUEST['rsvp']);
//	$rsvp->fetch_rsvp();
//
//	$reservation = $rsvp->get_rsvp();
//	$finance->set_order_id($reservation['order_id']);

}


if(isset($_REQUEST['display']) && $_REQUEST['display'] == 'invoice')
{
	$user_info = get_user_by('ID', $_REQUEST['member']);

	$email->to($user_info->display_name, $user_info->user_email);
	$email->from('notice@islonline.org', 'notice@islonline.org');
	$email->subject('Invoice from ISL');
	$email->message($finance->get_invoice($_REQUEST['team'], array($_REQUEST['rsvp'])));
	
	if(!$email->send())
	{
		echo "Email failed";
	}
	else
	{
		echo "Email sent to: " . $user_info->user_email;
	}

}

wp_enqueue_style('Modal_Styling', CC_CSS_PATH . 'modal.css');
wp_enqueue_script('Modal_Scripting', CC_JS_PATH . 'modal.js', array(), '', true);

$team->set_team($_REQUEST['team']);
$country_list = $country->country_list($team->schedule_countries());

$rsvp->set_rsvp_id($_REQUEST['rsvp']);
$rsvp->fetch_rsvp();

$reservation = $rsvp->get_rsvp();
$item = $finance->fetch_item($reservation['order_item_id']);
$add_ons = $finance->add_ons($item['id']);
$all_items[] = $reservation['order_item_id'];

//
//2013-03-27 DJT Aded to allow for only the items on this RSVP to be calculated in cost
//echo var_dump($add_ons);
//echo var_dump($all_items);
//echo explode(',',implode(',',$all_items) . (!empty($add_ons) ? ',' . implode(',',array_column($add_ons, 'id')) : ''));

$finance->Fetch_Items( explode(',',implode(',',$all_items) . (!empty($add_ons) ? ',' . implode(',',array_column($add_ons, 'id')) : '')));

//
// build urls
$url = '/wp-admin/admin.php?page=cc-pops&display=finance&team=' . $team->get_team() . "&member=" . $_REQUEST['member'] . "&rsvp=" . $rsvp->get_rsvp_id();
$ref_url = '/wp-admin/admin.php?page=cc-pops&display=refund&team=' . $team->get_team() . "&member=" . $_REQUEST['member'] . "&rsvp=" . $rsvp->get_rsvp_id();
$cred_url = '/wp-admin/admin.php?page=cc-pops&display=credit&team=' . $team->get_team() . "&member=" . $_REQUEST['member'] . "&rsvp=" . $rsvp->get_rsvp_id();
$mig_url = '/wp-admin/admin.php?page=cc-pops&display=migrate&team=' . $team->get_team() . "&member=" . $_REQUEST['member'] . "&rsvp=" . $rsvp->get_rsvp_id();
$inv_url = '/wp-admin/admin.php?page=cc-pops&display=invoice&team=' . $team->get_team() . "&member=" . $_REQUEST['member'] . "&rsvp=" . $rsvp->get_rsvp_id();


//
// HTML output below
?>
<?php //wp_head()?>

<br>
	<div id="inner-content" class="row">
		<?php
		if(isset($error))
		{
			echo $error;
		}
		?>

   	    	<main id="main" class="large-8 medium-8 columns" role="main">
        	<b>Volunteer: <?php _e($user->user_name($_GET['member']))?></b><br />
        	<b>Team: <?php _e($team->value('hash'))?></b><br />
			<b>Country:</b> <?php _e($country_list)?><br />
        	<div class="row collapse">
        		<div class="medium-6 columns">
            		<b>Arrival Date:</b> <?php _e($team->arrival_date())?><br />
            		<b>Departure Date:</b> <?php _e($team->departure_date())?><br />
            		<b>Duration:</b> <?php _e($team->trip_duration())?> days<br />
        		</div>
        		<div class="medium-6 columns">
            		<b>Arrival City:</b> <?php _e($team->value('arrival_city'))?><br />
            		<b>Departure City:</b> <?php _e($team->value('departure_city'))?><br />
            		<b>Cost:</b> $<?php echo number_format($finance->Calculate_ItemsCost(),2) //2018-03-27 DJT changed out. _e($finance->Fetch_Cost(true, $reservation['order_id']))?><br />
        		</div>
			</div>

			<form name="data" method="POST" enctype="multipart/form-data" action="<?=$url?>">

			<hr>
            <table style="border: hidden;">
				<TR>
                	<TH style="width: 200px; text-align: left;">Item</TH>
                    <TH style="width: 600px; text-align: right;">Cost</TH>
                </TR>
            	<tr>
            		<td>RSVP: <?php _e($team->value('hash'))?></td>
                    <td style="text-align: right;">$<?php _e(number_format($item['price'] * $item['quantity'],2))?></td>
                </tr>

                <?php

				$added_cost = 0;
				$app_fee = 0;
				foreach($add_ons as $index => $itm)
				{
					$all_items[] = $itm['id'];
					echo "<tr>";
						echo "<td>";
							echo $finance->Fetch_ItemVerbiage($itm['item_type'], $itm['id']);
						echo "</td>";
						echo "<td align='right'>$" . number_format($itm['price'] * $itm['quantity'],2) . "</td>";
					echo "</tr>";	
					$app_fee += (($itm['item_type'] ==0) ? ($itm['price'] * $itm['quantity']) : 0);
					$added_cost += ($itm['price'] * $itm['quantity']);
				}

				echo "<tr>";
					echo "<td><b>GRAND TOTAL</b></td>";
					echo "<td align='right'><b>$" . number_format((($item['price'] * $item['quantity']) + $added_cost),2) . "</b></td>";
					$grand_total = number_format((($item['price'] * $item['quantity']) + $added_cost),2);
				echo "</tr>";
				?>
           </table>

			<hr>
            <br />
           <table style="border: hidden;">
				<TR>
                	<TH style="width: 200px; text-align: left;">Payment Date</TH>
                	<TH style="width: 200px; text-align: left;">For</TH>
                	<TH style="width: 200px; text-align: left;">By</TH>
                    <TH style="width: 200px; text-align: right;">Amount</TH>
                </TR>

				<?php

				//
				//$cost['id'] = $payments['order_item_id']
				$pays = 0;
				$costs = $finance->items_cost($all_items, true);
				//echo var_dump($all_items);
					
				foreach($finance->payments($all_items) as $payments)
				{
					if($payments['amount'] != 0)
					{
						$item = '';
						$override_item = false;

						if(!empty($payments['special']) && ($payments['special'] == 'donate refund' || $payments['special'] == 'donate' || $payments['special'] == 'donate anonymous'))
						{
							$item = "Donation";
							$override_item = true;
//							echo '<pre>';print_r('Donation');die;
						}
						if(empty($payments['special']) && !empty($payments['note']))
						{
							$item = $payments['note'];
							$override_item = true;
//							echo '<pre>';print_r($payments['note']);die;
//							echo '<pre>';print_r($payments);die;
						}
						if(!empty($payments['special']) && $payments['special'] == 'fee' && !empty($payments['note']))
						{
							$item = $payments['note'];
							$override_item = true;
//							echo '<pre>';print_r('note');die;
						}


						if(!$override_item)
						{
							$item = $finance->Fetch_ItemVerbiage($costs[$payments['order_item_id']]['item_type']);
						} else {
//							echo '<pre>';print_r($payments);die;
						}


						if(floatval($payments['amount']) < 0 && substr($payments['note'],0,4) != 'Spcl')
						{
							//$item = "Refund for " . $item;
							$item = $item;
						}

						$payed_by = get_user_by("ID", $payments['created_by']);
						$whom = $payed_by->first_name . " " . $payed_by->last_name;

						echo "<tr data-payment='" . $payments['id'] . "' onclick='javascript:set_edit_model(\"edit_credit\", this)'><td>" . $payments['created_when'] . "</td>
								  <td data-adj-reason='" . $item  . "' class='adj-reason' style='cursor:pointer'>" . $item . "</td>
								  <td>" . $whom . "</td>	
								  <td data-adj-amt='". ($payments['amount'] * (-1)) . "' class='adj-amt' style='cursor:pointer' align='right'>$" . number_format($payments['amount'] * (-1),2) . "</td></tr>";
						$pays += $payments['amount'];

					}
				}
				echo "<TR>";
				echo "   <TD><B>TOTAL PAYMENTS</B></TD>";
				echo "   <TD></TD><TD></TD>";
				echo "   <TD style='text-align: right'><B>$" . number_format($pays,2) . "</B></TD>";
				echo "</TR>";
				?>
           </table>

 			<HR>
           <table style="border: hidden;">
				<TR>
                	<TH style="width: 200px; text-align: center;"></TH>
                	<TH style="width: 200px; text-align: center;"></TH>
                	<TH style="width: 200px; text-align: center;">Options</TH>
                	<TH style="width: 200px; text-align: left;"></TH>
                </TR>

				<?php
				echo "<TR><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>";
				if($edit_finance)
				{
					if(($team->trip_cost() + $added_cost - $pays) > 0)
					{
						echo "<a href='/payment/?team_id=" . $_REQUEST['team'] . "&member_id=" . $_REQUEST['member'] . "&rsvp=" . $_REQUEST['rsvp'] . "' target='_blank' class='button button-primary bounce' style='width: 200px; text-align: center'>Credit Card Payment</a><HR>";

						echo "<input type='button' class='button button-primary bounce open-model' style='width: 200px; text-align: center' value='Manual Payment' onclick='javascript:set_modal(\"manualpay\")'><HR>";
					}

					echo "<input type='button' class='button button-primary bounce open-model' style='width: 200px; text-align: center' value='Manual Adjustment' onclick='javascript:set_modal(\"credit\")'><HR>";

					echo "<input type='button' class='button button-primary bounce open-model' style='width: 200px; text-align: center' value='Refund' onclick='javascript:set_modal(\"refund\")'><HR>";

					echo "<a href='" . $mig_url . "' class='button button-primary bounce' style='width: 200px; text-align: center'>Migrate</a><HR>";															
				}


				echo "<a href='" . $inv_url . "' class='button button-primary bounce' style='width: 200px; text-align: center'>Invoice</a>";

				echo "</TD>";
			   	$finance->Fetch_Items(array($reservation['order_item_id']), true);
			   echo "<TD style='text-align: right;'><b>Balance Due</b><BR>
							$" . number_format($finance->Calculate_ItemsBalance(),2) . "
						</TD>";
				echo "</TR>";
				?>
            </table>
			</form>
        </main>

        <?php
		if($edit_finance)
		{
			?>
		<div id="manualpay_modal" class="modal">
        	<div class="modal-content">
				<form name="credit" method="POST" enctype="multipart/form-data" action="<?php echo $url?>">
                    <div class="modal-header">
                        Apply a Manual Payment
                    </div>

                    <div class="modal-body">
						<table>
							<TR>
								<TD>Amount</TD>
								<TD>Notes</TD>
							</TR>
							<TR>
								<TD><input type="number" name="cred_amt" id="cred_amt" min="1" step="0.01" required="true"></TD>
								<TD><input type="text" name="cred_reason" id="cred_reason" size="40" requried="true"></TD>
							</TR>
						</table>
                    </div>

                    <div class="modal-footer">
                        <input class='button button-primary' type="submit" name="Pay_Style" value="Apply Payment">
                        <input class='button button-primary' type="submit" name="Pay_Style" value="Apply Refund">
                    </div>
				</form>
			</div>
		</div>

        <div id="credit_modal" class="modal">
        	<div class="modal-content">
				<form name="credit" method="POST" enctype="multipart/form-data" action="<?php echo $url?>">
                    <div class="modal-header">
                        Apply Adjustment
                    </div>

                    <div class="modal-body">
						<P><B>Negative amounts will reduce the amount owed. Positive amounts will increase the amount owed.</B></p>
						<table>
							<TR>
								<TD>Adjustment Amount</TD>
								<TD>Adjustment Reason</TD>
							</TR>
							<TR>
								<TD><input type="number" name="adj_amt" id="adj_amt" step="0.01" required="true"></TD>
								<TD><input type="text" name="adj_reason" id="adj_reason" size="40" required="true"></TD>
							</TR>
						</table>
                    </div>
                    <div class="modal-footer">
                    	<input class="button button-primary" type="submit" value="Make Adjustment">
                	</div>
				</form>
			</div>
		</div>

        <div id="edit_credit_modal" class="modal">
        	<div class="modal-content">
				<form name="credit" method="POST" enctype="multipart/form-data" action="<?php echo $url?>">
                    <div class="modal-header">
                        Edit Adjustment
                    </div>

                    <div class="modal-body">
						<P><B>Negative amounts will reduce the amount owed. Positive amounts will increase the amount owed.</B></p>
						<table>
							<TR>
								<TD>Adjustment Amount</TD>
								<TD>Adjustment Reason</TD>
							</TR>
							<TR>
								<TD><input type="number" name="adj_amt" value="" id="adj_amt" step="0.01" required="true"></TD>
								<TD><input type="text" name="adj_reason" value="" id="adj_reason" size="40" required="true"></TD>
							</TR>
						</table>
                    </div>
                    <div class="modal-footer">
	                    <input type="hidden" name="adj_id" value="">
                    	<input class="button button-primary" type="submit" value="Edit Adjustment">
                    	<input class="button button-primary" type="submit" name="delete_adj" value="Delete Adjustment">
                	</div>
				</form>
			</div>
		</div>

        <div id="refund_modal" class="modal">
        	<div class="modal-content">
				<form name="data" method="POST" enctype="multipart/form-data" action="<?php _e($url); ?>">

				<div class="modal-header">
                	Process Refund
                </div>

                <div class="modal-body">
                   <table style="border: hidden;">
                        <TR>
                            <TH style="width: 200px; text-align: left;">Payment Date</TH>
                            <TH style="width: 200px; text-align: left;">Payment For</TH>
                            <TH style="width: 200px; text-align: left;">Payed By</TH>
                            <TH style="width: 200px; text-align: right;">Amount</TH>
                            <TH style="width: 200px; text-align: right;">Refund</TH>
                        </TR>

                        <?php

                        //
                        //$cost['id'] = $payments['order_item_id']
                        $pays = 0;
                        $costs = $finance->items_cost($all_items, true);
                        foreach($finance->payments($all_items, false) as $payments)
                        {
                            switch($costs[$payments['order_item_id']]['item_type'])
                            {
                                case 1:
                                    $item = "RSVP";
                                    break;
                                case 0:
                                    $item = "Application Fee";
                                    break;
                            }

                            if($payments['amount'] > 0)
							{
								if(isset($payments['created_by']))
								{
									$payed_by = get_user_by("ID", $payments['created_by']);
									$whom = $payed_by->first_name . " " . $payed_by->last_name;
								}
								else
								{
									$whom = "Varies";
								}

								echo "<tr><td>" . $payments['created_when'] . "</td>
										  <td>" . $item . "</td>
										  <td>" . $whom . "</td>	
										  <td align='right'>$" . number_format($payments['amount'],2) . "</td>
										  <td align='right'>
											<input 
												name=\"payment_id_" . $payments['id'] . "\" 
												id=\"payment_id_" . $payments['id'] . "\"	
												type=\"number\" 
												min=0 
												max=" . $payments['amount'] . "
												step=\"0.01\"
												value=" . (($costs[$payments['order_item_id']]['item_type'] == 0) ?  "0.00" : $payments['amount']) . "
												style=\"width:120px; text-align: right;\"	
											></td></tr>";
								$pays += $payments['amount'];	
							}
                        }
                        echo "<TR>";
                        echo "   <TD><B>TOTAL PAYMENTS</B></TD>";
                        echo "   <TD></TD><TD></TD>";
                        echo "   <TD style='text-align: right'><B>$" . number_format($pays,2) . "</B></TD>";
                        echo "</TR>";
                        ?>
                   </table>
                   <br />
	                 Force Cancel: <input type="text" size=10 placeholder='Yes or No' name="force_cancel" id="force_cancel">
	               	<!--     -->
	               	<!--2018-04-06 DJT Added new send_notice checkbox to determine if cancellation confirmation email is sent -->
	               	<br />
	               	<br />
	               	Send Notice: <input type="checkbox" name="send_notice" id="send_notice" checked>
    			</div>
				<!-- 2018-04-16: GKR moved block above (line 722) to get modal body styling and padding -->
               	<!--Force Cancel: <input type="text" size=10 placeholder='Yes or No' name="force_cancel" id="force_cancel">

               	<!--2018-04-06 DJT Added new send_notice checkbox to determine if cancellation confirmation email is sent -->
               	<!--<BR>
               	Send Notice: <input type="checkbox" name="send_notice" id="send_notice" checked> -->
                <div class="modal-footer">
                	<input class="button button-primary" id="refund_button" name="refund_button" type="submit" value="Process Refund">
                </div>

				</form>
    		</div>
        </div>

   	<?php
	}
	?>
    </div> <!-- end #content -->

<?php wp_footer() ?>