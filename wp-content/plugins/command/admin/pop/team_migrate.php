<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Required Classes
require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "rsvp.class.php";
require_once CC_CLASSES_DIR . "finance.class.php";
require_once CC_CLASSES_DIR . "country.class.php";
require_once CC_CLASSES_DIR . "program.class.php";

$team = new TEAM();
$rsvp = new RSVP();
$finance = new FINANCE();
$country = new COUNTRY();
$program = new PROGRAM();

//
//Migrate from one rsvp to another
if(!empty($_REQUEST['submit']) && !empty($_REQUEST['migrate_id']))
{
	if($_REQUEST['submit']=="Migrate" && $_REQUEST['migrate_id'] > 0)
	{
		//
		//Fetch order item id from rsvp
		$rsvp->set_rsvp_id($_REQUEST['rsvp']);
		$cur_rsvp = $rsvp->fetch_rsvp();

		//
		//Migrate the old item to the new item
		$new_item = $finance->move_item($cur_rsvp['order_item_id'], 
							   		  	$_REQUEST['migrate_id'], 
						  			  	!empty($_REQUEST['adjust']) ? true : false, 
						  			  	!empty($_REQUEST['new_cost']) ? $_REQUEST['new_cost'] : 0,
										true
						  			 	);

		if(!empty($new_item))
		{
			//
			//Migrate the reservation
			$rsvp->migrate($_REQUEST['rsvp'], $_REQUEST['migrate_id'], $new_item);		

			//
			//fix $_REQUEST for new team and rsvp
			$transfer = true;
			$_REQUEST['team'] = $_REQUEST['migrate_id'];
			$_REQUEST['rsvp'] = $rsvp->get_rsvp_id();
		}
		

	}
}

wp_enqueue_script('cmd_team_fetch', plugins_url() . '/command/js/team.fetch.js');	


//
// load/set the data to be used
$team->set_team($_REQUEST['team']);
$country_list = $country->country_list($team->schedule_countries());

$rsvp->set_rsvp_id($_REQUEST['rsvp']);
$rsvp->fetch_rsvp();

$reservation = $rsvp->get_rsvp();
$user_reservations = $rsvp->for_user($_REQUEST['member']);

if(empty($_REQUEST['rsvp']) || (!empty($_REQUEST['rsvp'] && !isset($user_reservations[$_REQUEST['rsvp']]))))
{

	echo "<H1>Migration</H1>";
	echo "No Reservation exists.";
	die();
}

$item = $finance->fetch_item($reservation['order_item_id']);
$add_ons = $finance->add_ons($reservation['order_item_id']);

$all_items[] = $reservation['order_item_id'];


$surl = '/wp-admin/admin.php';
$url = $surl . '?page=cc-pops&display=migrate&team=' . $team->get_team() . "&member=" . $_REQUEST['member'] . "&rsvp=" . $rsvp->get_rsvp_id();


//
//Make lsit of upcoming teams
$filters = array();
$filters['start'] = date("m-d-Y");
$filters['status'] = array(9, 4);
$filters['program'] = ((isset($_REQUEST['program_select']) ? $_REQUEST['program_select'] : $team->schedule_programs()));
$filters['country'] = ((isset($_REQUEST['country_select']) ? $_REQUEST['country_select'] : $team->schedule_countries()));

$team->create_list($filters, 0, 100);
$teams = $team->get_list();

$possible_teams = 0;
$selector = "<SELECT name='migrate_id' id='migrate_id'>";
$selector .= "<OPTION value=0> -- Choose a Team -- </OPTION>";	
foreach($teams as $id => $data)
{
	if($data['code'] != $team->value('code'))
	{
		$selector .= "<OPTION value=" . $id . ">" . $data['code'] . "</OPTION>";	
		$possible_teams++;
	}
}
$selector .= "</SELECT>";

?>
<? wp_head()?>

<br>
<h1>
    <A href='/wp-admin/admin.php?page=cc-teams&team_id=<?php _e($_REQUEST['team']);?>'>
        <img height='20' src='http://new.islonline.org/wp-content/uploads/2015/09/left_arrow.png'>
        Return to Team: <?=$team->value('hash')?>
    </A>
</H1>
<br>

<FORM name="filters" id="filters" method="GET" enctype="multipart/form-data" action="<?php echo $surl?>">
	<Table>
		<TR>
			<TD>Filters:</TD>
			<TD>
				<SELECT name="country_select" id="country_select" onchange="javascript:document.getElementById('filters').submit()">
					<?php echo $country->selector(isset($_GET['country_select']) ? $_GET['country_select'] : '',true) ?>
				</SELECT>
			</TD>
			<TD>
				<SELECT name="program_select" id="program_select" onchange="javascript:document.getElementById('filters').submit()">
					<?php echo $program->selector(isset($_GET['program_select']) ? $_GET['program_select'] : '', true)?>
				</SELECT>
			</TD>

		</TR>
	</Table>

	<input type="hidden" name="team" id="team" value="<?php echo $_GET['team']?>">
	<input type="hidden" name="member" id="member" value="<?php echo $_GET['member']?>">
	<input type="hidden" name="rsvp" id="rsvp" value="<?php echo $_GET['rsvp']?>">
	<input type="hidden" name="page" id="page" value="cc-pops">
	<input type="hidden" name="display" id="display" value="migrate">
</FORM>


<BR />

<form name="data" method="POST" enctype="multipart/form-data" action="<?=$url?>">

	<table class="widefat striped" width="100%">
		<TR>
			<TH width="10%"></TH>
			<TH width="10%" align="left"><?php _e($team->value('hash'))?></TH>
			<TH width="5%" align="center"> => </TH>
			<TH width="20%" align="left"><?php _e($possible_teams > 0 ? $selector : '<b>No available teams</b>')?></TH>
			<TH width="35%">&nbsp;</TH>
		</TR>

		<TR>
			<TD><B>Country</B></TD>
			<TD><?php _e($country_list)?></TD>
			<TD>&nbsp;</TD>
			<TD><span id="country"></span></TD>	
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD><B>Arrival Date:</B></TD>
			<TD><?php _e($team->arrival_date())?></TD>
			<TD>&nbsp;</TD>
			<TD><span id="arr_date"></span></TD>	
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD><B>Departure Date:</B></TD>
			<TD><?php _e($team->departure_date())?></TD>
			<TD>&nbsp;</TD>
			<TD><span id="dep_date"></span></TD>	
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD><B>Duration:</B></TD>
			<TD><?php _e($team->trip_duration())?> days</TD>
			<TD>&nbsp;</TD>
			<TD><span id="duration"></span></TD>	
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD><B>Arrival City:</B></TD>
			<TD><?php _e($team->value('arrival_city'))?></TD>
			<TD>&nbsp;</TD>
			<TD><span id="arrive"></span></TD>	
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD><B>Departure City:</B></TD>
			<TD><?php _e($team->value('departure_city'))?></TD>
			<TD>&nbsp;</TD>
			<TD><span id="depart"></span></TD>	
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD><B>Cost:</B></TD>
			<TD>$<span id="orig_cost"><?php _e($finance->Fetch_Cost(true, $reservation['order_id']))?></span></TD>
			<TD>&nbsp;</TD>
			<TD><span id="cost"></span></TD>	
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD><div id="keep" style="display: none;">
					Keep Original Cost? &nbsp;&nbsp;
					<input type="checkbox" id="adjust" name="adjust" value="true">
				</div>
				<input type="hidden" name="new_cost" id="new_cost" value="0">
			</TD>	
			<TD>&nbsp;</TD>		
		</TR>

		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>	
		</TR>

		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD><input class="button button-primary" type="submit" value="Migrate" id="submit" name="submit" disabled=true></TD>	
			<TD>&nbsp;</TD>			
		</TR>

	</table>

</form>
 
 <?php wp_footer() ?>