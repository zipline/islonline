<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Checking all values are set
if (isset($_GET['team_id']) && isset($_GET['user_id']) && isset($_GET['private'])) 
{
	do_shortcode('[chatterbox_chat team_id=' . $_GET['team_id'] . ' user_id=' . $_GET['user_id'] . ' private=' . $_GET['private'] . ' page=' . get_the_title() . ']');
} 
else 
{
	exit;
}

