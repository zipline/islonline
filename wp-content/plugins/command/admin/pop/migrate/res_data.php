<?php

function migrate_res_data($Range = "ALL", $wpdb, $cur_wp, $cur_db)
{

	global $wpdb;
	
	$query = "SELECT 
				r.id, r.post_id, r.user_id, m.meta_value old_key, u.meta_value member_id 
			  FROM 
			  	wp_command_RSVP r
					INNER JOIN
					 (SELECT post_id, meta_value FROM wp_postmeta WHERE meta_key = 'migrate_special_value') m ON r.post_id = m.post_id
					INNER JOIN
					 (SELECT user_id, meta_value FROM wp_usermeta WHERE meta_key = 'member_id') u ON r.user_id = u.user_id
			  WHERE
			  	" . (($Range == "ALL") ? '' : '(r.created >= NOW() - INTERVAL ' . $Range . ' DAY OR r.modified >= NOW() - INTERVAL ' . $Range . ' DAY) AND ' ) . "
			  	1=1 AND r.id in (12187, 12188, 12189, 12190, 12191, 12192, 12193, 12194)  
			  ORDER BY
			  	r.id DESC";
	
	$results = $wpdb->get_results($query);
	
	if(!empty($results))
	{
		foreach($results as $record)
		{
			echo "Building RSVP " . $record->id . "<BR>";
			flush();

			//
			//Clear variables
			unset($passport_record);
			unset($field);
			unset($flight_info);
			
			//
			//Gather passport data
			$passport_query = "SELECT passport_number, passport_country, passport_date_of_issue, passport_expire_date
							   FROM islonlin_app2015.members
							   WHERE id=" . $record->member_id;
			$passport_results = @$cur_db->query($passport_query);
			
			if(!empty($passport_results))
			{
				$passport_record = @$passport_results->fetch_object();
			}

			//
			//Academic Profile
			$field['profile']['name'][1] = 'field_5720a337c8314';
			$field['profile']['name'][2] = 'field_5720a359c8315';
			$field['profile']['name'][3] = 'field_5720a38dc8316';
			$field['profile']['name'][4] = 'field_5720a402c8317';
			$field['profile']['name'][5] = 'field_5720a0c8c830f';
			$field['profile']['label'][1] = 'school';
			$field['profile']['label'][2] = 'academic_major';
			$field['profile']['label'][3] = 'academic_courses';
			$field['profile']['label'][4] = 'professional_experience';
			$field['profile']['label'][5] = 'languages_spoken';

			$aca_query = "SELECT school, major, related_courses, languages_spoken, prof_training
						  FROM islonlin_app2015.members
						  WHERE id=" . $record->member_id;
			$aca_results = @$cur_db->query($aca_query);

			if(!empty($aca_results))
			{
				$aca_record = @$aca_results->fetch_object();

				if(!empty($aca_record->school))
				{
					$field['profile']['value'][1] = $aca_record->school;
				}

				if(!empty($aca_record->major))
				{
					$field['profile']['value'][2] = $aca_record->major;
				}

				if(!empty($aca_record->related_courses))
				{
					$field['profile']['value'][3] = $aca_record->related_courses;
				}

				if(!empty($aca_record->languages_spoken))
				{
					$field['profile']['value'][5] = $aca_record->languages_spoken;
				}

				if(!empty($aca_record->prof_training))
				{
					$field['profile']['value'][4] = $aca_record->prof_training;
				}
			}
			
			
			//
			//Health Profile
			$field['profile']['name'][10] = 'field_582dc5d62fb59';
			$field['profile']['name'][11] = 'field_57ed7a1946fbb';
			$field['profile']['name'][12] = 'field_582dc6092fb5a';
			$field['profile']['name'][13] = 'field_57ed7a7c46fbc';
			$field['profile']['name'][14] = 'field_57ed7abe46fbd';
			$field['profile']['name'][15] = 'field_582dc6842fb5b';
			$field['profile']['name'][16] = 'field_57ed7b0046fbe';
			$field['profile']['label'][10] = 'are_you_taking_any_medications';
			$field['profile']['label'][11] = 'medications_taken';
			$field['profile']['label'][12] = 'do_you_have_any_special_dietary_restrictions';
			$field['profile']['label'][13] = 'dietary_restrictions';
			$field['profile']['label'][14] = 'other_diet';
			$field['profile']['label'][15] = 'do_you_have_any_food_allergies';
			$field['profile']['label'][16] = 'food_allergies';
			
			$health_query = "SELECT medications, allergies, diet_restrictions 
							FROM islonlin_app2015.members
						  	WHERE id=" . $record->member_id;
			$health_results = @$cur_db->query($health_query);
			if(!empty($health_results))
			{
				$health_record = @$health_results->fetch_object();

				if(!empty($health_record->medications) && $health_record->medications != 'N/A')
				{
					$field['profile']['value'][10] = 'yes';
					$field['profile']['value'][11] = $health_record->medications;
				}
				else
				{
					$field['profile']['value'][10] = 'no';
				}

				if(!empty($health_record->allergies))
				{
					$field['profile']['value'][15] = 'yes';
					$field['profile']['value'][16] = $health_record->allergies;
				}
				else
				{
					$field['profile']['value'][15] = 'no';
				}

				if(!empty($health_record->diet_restrictions))
				{
					$field['profile']['value'][12] = 'yes';
					$field['profile']['value'][13] = 'Other (Please specify)';
					$field['profile']['value'][14] = $health_record->diet_restrictions;
				}
				else
				{
					$field['profile']['value'][12] = 'no';
					$field['profile']['value'][13] = 'No restrictions';
				}
				
			}
			
			
			//
			//Emergency Contact
			$field['profile']['name'][20] = 'field_57ed7b80e2abc';
			$field['profile']['name'][21] = 'field_57ed7b94e2abd';
			$field['profile']['name'][22] = 'field_57ed7ba7e2abe';
			$field['profile']['name'][23] = 'field_57ed7a7c46fbc';
			$field['profile']['name'][24] = 'field_57ed7bb3e2abf';
			$field['profile']['label'][20] = 'emergency_contact';
			$field['profile']['label'][21] = 'emergency_contact_relationship';
			$field['profile']['label'][22] = 'emergency_contact_email';
			$field['profile']['label'][23] = 'emergency_contact_primary_phone';
			$field['profile']['label'][24] = 'emergency_contact_secondary_phone';
			
			$emergency_query = "SELECT emerg_name, emerg_relationship, emerg_email, emerg_phone_1, emerg_phone_2 
							FROM islonlin_app2015.members
						  	WHERE id=" . $record->member_id;
			$emergency_results = @$cur_db->query($emergency_query);
			
			if(!empty($emergency_results))
			{
				$emergency_record = @$emergency_results->fetch_object();

				if(!empty($emergency_record->emerg_name))
				{
					$field['profile']['value'][20] = $emergency_record->emerg_name;
				}
				if(!empty($emergency_record->emerg_relationship))
				{
					$field['profile']['value'][21] = $emergency_record->emerg_relationship;
				}
				if(!empty($emergency_record->emerg_email))
				{
					$field['profile']['value'][22] = $emergency_record->emerg_email;
				}
				if(!empty($emergency_record->emerg_phone_1))
				{
					$field['profile']['value'][23] = $emergency_record->emerg_phone_1;
				}
				if(!empty($emergency_record->emerg_phone_2))
				{
					$field['profile']['value'][24] = $emergency_record->emerg_phone_2;
				}
				
			}
			
			
			//
			//Flight information
			$flight_query = "SELECT departure_airline_flightnum, departure_departs_datetime, departure_arrives_datetime, departure_arrives_airport,
									return_airline_flightnum, return_departs_datetime, return_arrives_datetime, return_departs_airport,
									early_arrival, late_departure
							 FROM
							 	islonlin_app2015.reservations
							 WHERE
							 	id = " . $record->old_key;
			$flight_results = @$cur_db->query($flight_query);

			if(!empty($flight_results))
			{
				$flight_record = @$flight_results->fetch_object();
				
				if(!empty($flight_record) &&
				   !empty($flight_record->departure_arrives_datetime) && $flight_record->departure_arrives_datetime != "0000-00-00 00:00:00" && 
				   !empty($flight_record->return_departs_datetime) && $flight_record->return_departs_datetime != "0000-00-00 00:00:00")
				{
					//
					//Find flights
					preg_match('/^\D*(?=\d)/', $flight_record->departure_airline_flightnum, $m);
					$depart_break_point = isset($m[0]) ? strlen($m[0]) -1 : strlen($flight_record->departure_airline_flightnum);
					$depart_airline = trim(substr($flight_record->departure_airline_flightnum, 0, $depart_break_point));
					$depart_flightnum = preg_replace("/[^a-zA-Z0-9]/", "", trim(substr($flight_record->departure_airline_flightnum, $depart_break_point)));

					preg_match('/^\D*(?=\d)/', $flight_record->return_airline_flightnum, $m);
					$return_break_point = isset($m[0]) ? strlen($m[0]) -1 : strlen($flight_record->return_airline_flightnum);
					$return_airline = trim(substr($flight_record->return_airline_flightnum, 0, $return_break_point));
					$return_flightnum = preg_replace("/[^a-zA-Z0-9]/", "", trim(substr($flight_record->return_airline_flightnum, $return_break_point)));

					$return_date = DateTime::createFromFormat('Y-m-d H:i:s', $flight_record->return_departs_datetime);
					$depart_date = DateTime::createFromFormat('Y-m-d H:i:s', $flight_record->departure_arrives_datetime);

					//echo var_dump($depart_date);
					//echo var_dump($return_date);
					
					$flight_info[] = array('direction' => 0,
										   'airline' => $depart_airline, 'flight' => $depart_flightnum, 'seat' => 0,
										   'depart' => "Unknown", 'ddate' => $depart_date->format('m/d/Y'), 'dtime' => $depart_date->format('H:i:s'),
										   'arrive' => $flight_record->departure_arrives_airport, 'adate' => $depart_date->format('m/d/Y'), 'atime' => $depart_date->format('H:i:s')); 		
					$flight_info[] = array('direction' => 1,
										   'airline' => $return_airline, 'flight' => $return_flightnum, 'seat' => 0,
										   'depart' => $flight_record->return_departs_airport, 'ddate' => $return_date->format('m/d/Y'), 'dtime' => $return_date->format('H:i:s'),
										   'arrive' => "Unknown", 'adate' => $return_date->format('m/d/Y'), 'atime' => $return_date->format('H:i:s')); 		

					$flight_fees = array('arrive' => !empty($flight_record->early_arrival) ? "checked" : '',
										 'depart' => !empty($light_record->late_departure) ? "checked" : '');
				}
			}

			echo "&nbsp;&nbsp;  Writing RSVP " . $record->id . "<BR>";
			flush();
			//
			//Write all data
			if(!empty($passport_record->passport_number))
			{
				update_post_meta($record->post_id, '_cmd_passport_num', $passport_record->passport_number);	
			}
			if(!empty($passport_record->passport_country))
			{
				update_post_meta($record->post_id, '_cmd_passport_country', $passport_record->passport_country);	
			}
			if(!empty($passport_record->passport_date_of_issue))
			{
				update_post_meta($record->post_id, '_cmd_passport_issue', $passport_record->passport_date_of_issue);	
			}
			if(!empty($passport_record->passport_expire_date))
			{
				update_post_meta($record->post_id, '_cmd_passport_expire', $passport_record->passport_expire_date);	
			}

			foreach($field['profile']['name'] as $index => $fld_name)
			{
				update_post_meta($record->post_id, '_' . $field['profile']['label'][$index], $fld_name);
				
				if(!empty($field['profile']['value'][$index]))
				{
					update_post_meta($record->post_id, $field['profile']['label'][$index], $field['profile']['value'][$index]);	
				}
			}
			
			if(!empty($flight_info))
			{
				echo "&nbsp;&nbsp; Flight Info " . $record->id . "<BR>";
				flush();

				$rsvp = new RSVP;
				$rsvp->set_rsvp_id($record->id);
				$rsvp->fetch_rsvp();
				$rsvp->change_user($record->user_id);
				$rsvp->set_flight_info($flight_info);
				$rsvp->set_flight_fees($flight_fees);

				unset($rsvp);
			}
			//echo var_dump($field);
		}
	}
}