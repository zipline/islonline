<?php

function migrate_reservations($Range = "ALL", $wpdb, $cur_wp, $cur_db)
{
	$mpa_labels = array('med add',
						'med prof add on',
						'mediacal professional add-on',
						'medical profesional add-on',
						'medical professioinal add-on',
						'medical professional add-on',
						'medical professional medical add-on',
						'medical professional medical-add on',
						'mpa',
						'mpa - reference letter',
						'mpa fee',
						'mpa letter',
						'mpa letter of reference',
						'mpa reference letter',
						'professional medical add-on',
					    'reference letter');
	
	$late_fees = array('late arrival fee',
					   'late fee',
					   'late fee arrival',
					   'late arrival');
						
	$arrival_fees = array('arrival fee',
						  'early arrival',
						  'early arrival and late departure fee',
						  'early fee',
						  'early/ate arrival fee');
	
	$departure_fees = array('early departure fee');
	
	$extended_fees = array('extended stay fee',
						   'extension fee',
						   'extra day fee',
						   'extra fee',
						   '7 day extension fee',
						   'extra night fee');
	
	$cont_ed_fees = array('ce fee',
						  'ceu',
						  'ceu add-on',
						  'ceu fee',
						  'cont ed credits');						 
	
	$rec_fees = array('rec fee',
					  'recreation fee',
					  'recreational fee',
					  'extra curr activity');
	
	$transportation_fees = array('airport transportation',
								 'transportation',
								 'transportation fee',
								 'pick up fee');
	
	$group_fees = array('group cost',
					    'group fee',
					    'group invoice fee',
					    'group payment fee',
					    'group team cost',
					    'group team fee',
					    'group trip cost',
					    'group trip fee',
					    'groupteam cost',
					    'team fee');
	
	$trip_fees = array('trip cost',
					   'trip cost (group)',
					   'trip cost - group',
					   'trip cost group',
					   'trip delay fee',
					   'trip fee',
					   'trip fee cost',
					   'trip team fee');
	
	
	$query = "SELECT 
					reserves.id, reserves.email, reserves.team_id, reserves.number, reserves.post_id, reserves.created,
					financials.order_item_id, financials.order_item_name, IFNULL(financials.order_item_type, '_missing') order_item_type, 
					financials.order_id, financials.user
			  FROM 
				
				  (
				  SELECT 
						r.id, m.email, s.team_id, s.number, pm.post_id, r.created
				  FROM
						islonlin_app2015.reservations r
							INNER JOIN islonlin_app2015.members m on r.member_id = m.id
							LEFT JOIN islonlin_app2015.subteams s on r.subteam_id = s.id
							LEFT JOIN
								(SELECT post_id, meta_value FROM islonlin_wp2015.wp_postmeta WHERE meta_key='reservation_id') pm on pm.meta_value = r.id
				  WHERE
				  		" . (($Range == "ALL") ? '' : '(r.created >= NOW() - INTERVAL ' . $Range . ' DAY OR r.modified >= NOW() - INTERVAL ' . $Range . ' DAY) AND ' ) . "
						r.subteam_id IS NOT NULL
				  ) 
				  reserves

			  LEFT JOIN
			  
				  (
				  SELECT 
					   p.id, woi.*, pm.meta_value user
				  FROM 
					   islonlin_wp2015.wp_posts p
							INNER JOIN islonlin_wp2015.wp_woocommerce_order_items woi on p.ID = woi.order_id
							INNER JOIN islonlin_wp2015.wp_postmeta pm on p.ID = pm.post_id
				  WHERE 
					   p.post_type = 'shop_order' AND
					   pm.meta_key = '_customer_user' AND
					   woi.order_id <= 200000
				  ORDER BY
					   p.id DESC, woi.order_item_id
				  )
				  financials
			  
			  	  ON reserves.post_id = financials.id
			  
			  WHERE reserves.id in (13713, 13659, 13607, 13695, 13420, 13715, 13443, 13623  )

				UNION
			  
			  SELECT 2000000, 'x', 0, 0, 0, 0, 0, 0, 0, 0, 0
			  
  			  ORDER BY
			  	id, post_id";

	//echo $query;
	
	$results = $cur_db->query($query);
	$last_id = 0;
	$last_user_id = 0;
	$last_team_id = 0;
	$last_order_id = 0;
	$fail = true;
	
	//
	//Cycle thorugh all users
	while($record = $results->fetch_object())
	{

		//
		//If we have some data and we are now moving onto a new record, write the old transaction...
		if($last_id > 0 && $last_id != $record->id && $last_user_id > 0 && $last_team_id > 0 && !$fail)
		{
			//echo "&nbsp;&nbsp;&nbsp; Writing " . $last_id . ".";
			//flush();
			$passes = 0;
			$total_amt = 0;
			unset($paid_record);
			if(!empty($last_order_id) && $last_order_id != 'NULL')
			{
				$paid_query = "SELECT o.meta_value order_key, p.meta_value pay_method, d.meta_value deposit
								FROM 
									islonlin_wp2015.wp_postmeta o
									LEFT JOIN 
										(SELECT pm.meta_value, pm.post_id FROM islonlin_wp2015.wp_postmeta pm
										 WHERE pm.post_id = " . $last_order_id . " AND pm.meta_key='_payment_method_title') p 
										 ON o.post_id = p.post_id
									LEFT JOIN 
										(SELECT pm.meta_value, pm.post_id FROM islonlin_wp2015.wp_postmeta pm
										 WHERE pm.post_id = " . $last_order_id . " AND pm.meta_key='_deposit_paid') d 
										 ON o.post_id = d.post_id
								WHERE 
									o.post_id = " . $last_order_id . " AND o.meta_key='_order_key'";

				$paid_query = "SELECT wpp.order_id, wpp.sum, wpp.payment_method pay_method, wpp.payment_date, o.meta_value order_key
							   FROM islonlin_wp2015.wp_wpp_deposit_records wpp
							   LEFT JOIN 
										(SELECT pm.meta_value, pm.post_id FROM islonlin_wp2015.wp_postmeta pm
										 WHERE pm.post_id = " . $last_order_id . " AND pm.meta_key='_order_key') o 
										 ON o.post_id = wpp.order_id
							   WHERE wpp.order_id = " . $last_order_id . "
							   ORDER BY payment_date";
				$paid_record = $cur_wp->query($paid_query, MYSQLI_STORE_RESULT);
			}
			
			if(!empty($paid_record) && $paid_record->num_rows > 0)
			{
				$num_rows = $paid_record->num_rows;
			}
			else
			{
				$num_rows = 1;
			}
			
			for($cur_row = 0; $cur_row < $num_rows; $cur_row++)
			{
				$paid_amt = 0;
				if(!empty($paid_record) && $paid_record->num_rows > 0)
				{
					$paid_record->data_seek($cur_row);
					
					$paid_data = $paid_record->fetch_object();
					$paid_amt = floatval(preg_replace("/[^0-9.]/", "", $paid_data->sum));

					$trans_data = array('amount' => $paid_amt,
										'form' => ((empty($paid_data->pay_method)) ? "IDK" : $paid_data->pay_method),
										'response' => 'Approved',
										'transaction_id' => ((empty($paid_data->order_key)) ? date("Ymdhis") : $paid_data->order_key),
										'raw_request' => 'Legacy Transition',
										'raw_response' => 'Legacy Transition'
										);

				}
				else
				{
					//
					//If we have no financial data then it's an old record
					//If the team is Completed, then the people paid so we can mark them as such.
					if($team->value('status') == 3)
					{
						$paid_amt = floatval(preg_replace("/[^0-9.]/", "", $cart->Total()));

						$trans_data = array('amount' => $paid_amt,
											'form' => 'Unknown',
											'response' => 'Approved',
											'transaction_id' => date('Ymdhis'),
											'raw_request' => 'Legacy Transaction - no record',
											'raw_response' => 'Legacy Transaction - no record'
											);


					}
					else
					{
						$paid_amt = 0;

						$trans_data = array('amount' => 0,
											'form' => 'Unknown',
											'response' => 'Approved',
											'transaction_id' => date('Ymdhis'),
											'raw_request' => 'Legacy Transition - cannot locate record',
											'raw_response' => 'Legacy Transition - cannot locate record'
											);
					}
				}
				$total_amt += $paid_amt;

				//
				//Determine method and action
				if($paid_amt < $cart->Total() && $cur_row == 0)
				{
					$method = "deposit";
				}
				else
				{
					$method = "balance";
				}
				$action = "checkout";


				//
				//Find the reservation and flag it as special
				$rsvp = new RSVP;
				$rsvp->set_id_by_team_user($last_team_id, $last_user_id);
				$reservation = $rsvp->fetch_rsvp();

				//
				//Dont duplicate transaction or rsvp if we already have one.
				if(!$reservation)
				{
					$finance = new FINANCE();
					$finance->change_user($last_user_id);
					$finance->new_transaction($trans_data, $method, $action);	

					$finance->move_CartToOrder( $cart , true );
					$finance->apply_payments($method, $action, $paid_amt, 'All', '', (!empty($paid_data->payment_date) ? $paid_data->payment_date : NULL));

					if(!empty($special_trans) && count($special_trans) > 0)
					{
						foreach($special_trans as $trans_info => $trans_cost)
						{
							$trans_data = 	array('amount' => ($trans_cost * (-1)),
												  'form' => 'Manual Adjustment',
												  'response' => 'Manual',
												  'transaction_id' => 'Manual',
												  'raw_request' => 'Reason=>' . $trans_info . ", Amount=>" . ($trans_cost ),
												  'raw_response' => 'Accepted'
												  );
							$finance->new_transaction($trans_data);	

							//
							//Find the reservation and flag it as special
							$rsvp = new RSVP;
							$rsvp->set_id_by_team_user($last_team_id, $last_user_id);
							$reservation = $rsvp->fetch_rsvp();

							$finance->set_order_id($reservation['order_id']);
							$finance->payment($reservation['order_item_id'], 
											 ($trans_cost * (-1)),
											  '',
											  'Spcl: ' . $trans_info,
											  "fee",
											  NULL,
											  NULL
											  );

						}
						
						unset($special_trans);
					}
					
					//
					//If we just did a deposit and there's money left over, then make a payment
					if($method == 'deposit' && $finance->CurrentFunds() > 0)
					{
						$finance->apply_payments('defined', 'payment', $paid_amt, 'All', '', (!empty($paid_data->payment_date) ? $paid_data->payment_date : NULL));
					}
					//$cart->Clear();	
					//do_action('cmd_checkout', $last_team_id, $last_user_id);

					//
					//Find the reservation and flag it as special
					$rsvp = new RSVP;
					$rsvp->set_id_by_team_user($last_team_id, $last_user_id);
					$reservation = $rsvp->fetch_rsvp();
					
					if(!empty($discount))
					{
						//
						//Write Transaction Record
						$trans_data = 	array('amount' => $discount,
											  'form' => 'Manual Adjustment',
											  'response' => 'Manual',
											  'transaction_id' => 'Manual',
											  'raw_request' => 'Reason=>Discount, Amount=>' . ($discount * (-1)),
											  'raw_response' => 'Accepted'
											  );
						$finance->new_transaction($trans_data);	

						$finance->payment($reservation['order_item_id'], 
										 $discount * (-1),
										  NULL,
										  'Manual Adjustment',
										  NULL,
										  1,
										  !empty($paid_data->payment_date) ? $paid_data->payment_date : NULL
										  );
					}

				}
				else
				{
					//
					//Clear cart
					//$cart->Clear();	

					//
					//Check to see if there has been an updated payment...
					$finance = new FINANCE();
					$finance->Fetch_OrderId( 'rsvp' , $reservation['id'] , true);
					$paid = $finance->Fetch_Paid();

					if(($paid + $discount) < $total_amt)
					{
						$finance->new_transaction($trans_data, $method, $action);	
						$finance->apply_payments('defined', 'payment', $paid_amt, 'All', '', !empty($paid_data->payment_date) ? $paid_data->payment_date : NULL);
					}

				}

			}

			if($reservation)
			{
				echo "	Reservation: " . $reservation['id'] . "<BR>";
				update_post_meta($reservation['post_id'], 'migrate_special', 'true');
				update_post_meta($reservation['post_id'], 'migrate_special_value', $last_id);

			//	echo "RSVP: " . $reservation['id'] . "<BR>";
			}
			else
			{
			//	echo "No Write <BR>";
			}

			@mysqli_free_result($paid_record);
			//$cart->Clear();

		}

		//
		//Grab Team and User stuff for the new one
		if($last_id != $record->id)
		{
			unset($actual_team_number);
			unset($actual_user);
			
			$actual_team_query = "SELECT 
										t.id 
				  				  FROM
				  						wp_command_TEAM t INNER JOIN wp_postmeta pm on t.post_id = pm.post_id
				  				  WHERE
				  						pm.meta_key = '_original_team_id' AND pm.meta_value = " . $record->team_id;
			$actual_team_number = $wpdb->get_var($actual_team_query);
			
			if($actual_team_number)
			{
				$team = new TEAM();
				$team->set_team($actual_team_number);
			}
			
			//
			//Initial user find
			$actual_user = get_user_by( 'email' , trim($record->email));
			
			//
			//Initial user set attempt
			if($actual_user)
			{
				wp_set_current_user($actual_user->ID);
			}
			else
			{
				//
				//Secondary user find...
				$temp_id_query = "SELECT 
										um.user_id 
								  FROM 
								  		islonlin_wp2015.wp_usermeta um
										INNER JOIN islonlin_app2015.members m on um.meta_value = m.id
								  WHERE 
								  		um.meta_key='member_id' AND
										m.email = '" . trim($record->email) . "'";
				$temp_id = $cur_wp->query($temp_id_query);
				
				if($temp_id)
				{
					$temp_record = $temp_id->fetch_object();
					
					if(!empty($temp_record))
					{
						$actual_user = get_user_by( 'ID' , $temp_record->user_id);
					}
				}
				@mysqli_free_result($temp_id);
				
				//
				//Secondary user set attempt
				if($actual_user)
				{
					wp_set_current_user($actual_user->ID);
				}
				else
				{
					//
					//Tertiary user find...
					$temp_id_query = "SELECT 
											m.id + 20000 user_id 
									  FROM 
											islonlin_app2015.members m
									  WHERE 
											m.email = '" . trim($record->email) . "'";
					$temp_id = $cur_wp->query($temp_id_query);

					if($temp_id)
					{
						$temp_record = $temp_id->fetch_object();

						if(!empty($temp_record))
						{
							$actual_user = get_user_by( 'ID' , $temp_record->user_id);
						}
					}
					@mysqli_free_result($temp_id);

					//
					//Tertiary user set attempt
					if($actual_user)
					{
						wp_set_current_user($actual_user->ID);
					}

				}

			}

			
			//
			//Got good data, so start things up
			if($actual_team_number && $actual_user)
			{
				echo "Starting " . $record->id . " for team " . $team->value('hash') . " for " . $actual_user->first_name . " " . $actual_user->last_name . "<BR>";
			}
			else
			{				
				echo "Bad Record: " . $record->id . " ";
				
				if(!$actual_team_number)
				{
					echo "(Team: " . $record->team_id . ") ";
				}
				
				if(!$actual_user)
				{
					echo "(User: " . trim($record->email) . ") ";
				}

				echo "<BR>";
			}
			
			//
			//Clear cart table
			$wpdb->query('DELETE FROM wp_command_cart');

			flush();
		}
		
		//
		//Update "lasts" so we can use them when we write
		$last_id = $record->id;
		$last_user_id = $actual_user->ID;
		$last_team_id = $team->value('id');
		$last_order_id = $record->order_id;
		

		//
		//If we have a user and team
		if($actual_team_number && $actual_user)
		{

			//
			//Gather transaction data based on item type
			switch ($record->order_item_type)
			{
				case "_missing":
					$fail = true;
					
					wp_set_current_user($actual_user->ID);
					$cart = new CART( false, $actual_user->ID);
					$cart->add_RSVP( $actual_team_number, floatval(preg_replace("/[^0-9.]/", "", $team->value('cost'))), 1 );
					$deposit = 0;

					$fail = false;
					break;

				case "line_item":
					$fail = true;
					$add_on = false;
					$special_trans = array();
					$mod = 0;
					$discount = 0;
					
					if($team->value('id'))
					{

						$main_item_query = "SELECT p.meta_value product, q.meta_value qty, st.meta_value sub_total, d.meta_value deposit, t.meta_value total
											FROM 
												islonlin_wp2015.wp_woocommerce_order_itemmeta p
												LEFT JOIN 
													(SELECT order_item_id, meta_value FROM islonlin_wp2015.wp_woocommerce_order_itemmeta w
													 WHERE w.order_item_id = " . $record->order_item_id . " AND w.meta_key='_line_subtotal'
													) st ON p.order_item_id = st.order_item_id
												LEFT JOIN 
													(SELECT order_item_id, meta_value FROM islonlin_wp2015.wp_woocommerce_order_itemmeta w
													 WHERE w.order_item_id = " . $record->order_item_id . " AND w.meta_key='_line_total'
													) t ON p.order_item_id = t.order_item_id
												LEFT JOIN 
													(SELECT order_item_id, meta_value FROM islonlin_wp2015.wp_woocommerce_order_itemmeta w
													 WHERE w.order_item_id = " . $record->order_item_id . " AND w.meta_key='_deposit_value'
													) d ON p.order_item_id = d.order_item_id
												LEFT JOIN 
													(SELECT order_item_id, meta_value FROM islonlin_wp2015.wp_woocommerce_order_itemmeta w
													 WHERE w.order_item_id = " . $record->order_item_id . " AND w.meta_key='_qty'
													) q ON p.order_item_id = q.order_item_id
											WHERE 
												(p.order_item_id = " . $record->order_item_id . " AND p.meta_key='_product_id')
												";
						$main_item_record = $cur_wp->query($main_item_query);

						if($main_item_record)
						{
							$main_item = $main_item_record->fetch_object();

							if(!empty($main_item))
							{
								wp_set_current_user($actual_user->ID);
								$cart = new CART( false, $actual_user->ID);
								$cart->add_RSVP( $actual_team_number, floatval(preg_replace("/[^0-9.]/", "", $main_item->sub_total)), $main_item->qty );
								//$deposit = floatval(preg_replace("/[^0-9.]/", "", $main_item->deposit));

								if($main_item->total < $main_item->sub_total)
								{
									$discount = $main_item->total - $main_item->sub_total;
								}
								
								$fail = false;
							}
							else
							{
								//echo "Failed Main Item for " . $record->order_item_id . "<BR>";
								//echo $main_item_query;
							}

						}
						else
						{
							//echo "Failed Main Record for " . $record->order_item_id . "<BR>";
							//echo $main_item_query;
						}
						
						@mysqli_free_result($main_item_record);

					}

					if($fail)
					{
						echo "Order: (" . $record->order_item_id . "), cannot find main item " . $record->order_item_name . "<BR>";
					}

					break;

				case "coupon":
					//
					//No sense if this record already fails
					if(!$fail)
					{
						//
						//Determine if we have a coupon or cap code
						$possible = $record->order_item_name;

						$amb_query = "SELECT user_id FROM wp_usermeta 
									  WHERE meta_key='_cmd_comm_ambassador_cap_code' and meta_value = '" . $possible . "'";
						$amb = $wpdb->get_var($amb_query);

						if($amb)
						{
							$cart->add_CAP($amb);
							$mod = $cart->Total('CAP');
						}
						else
						{
							$cart->add_Coupon($possible, true, true);
							$mod = $cart->Total('Coupon');
						}

						if(isset($discount) && $discount != 0 && isset($mod) && $mod != 0)
						{
							$discount -= $mod;	
						}
					}

					break;

				case "fee":
					//
					//No sense if this record already fails
					if(!$fail && $record->order_item_name != 'Application Fee' && $record->order_item_name != 'Appliccation Fee')
					{
						//
						//Shouldn't have to do anything since the fee is already covered in the team setup now
						//Grab MPA add-ons
						if(in_array(strtolower($record->order_item_name), $mpa_labels))
						{
							if($team->value('id'))
							{

								$main_item_query = "SELECT p.meta_value
													FROM 
														islonlin_wp2015.wp_woocommerce_order_itemmeta p
													WHERE 
														p.order_item_id = " . $record->order_item_id . " AND p.meta_key='_line_subtotal'
														";
								$main_item_record = $cur_wp->query($main_item_query);

								if($main_item_record)
								{
									$main_item = $main_item_record->fetch_object();
									
									if(!empty($main_item))
									{
										$cart->add_AddOn( $actual_team_number, 36402, floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value)), 1 );
										$add_on = true;
									}
								}
							}
						}

						//
						//Add other fees
						if($team->value('id'))
						{
							$main_item_query = "SELECT p.meta_value
												FROM 
													islonlin_wp2015.wp_woocommerce_order_itemmeta p
												WHERE 
													p.order_item_id = " . $record->order_item_id . " AND p.meta_key='_line_subtotal'
													";
							$main_item_record = $cur_wp->query($main_item_query);

							if($main_item_record)
							{
								$main_item = $main_item_record->fetch_object();

								if(!empty($main_item))
								{
									if(in_array(strtolower($record->order_item_name), $late_fees))
									{
										$special_trans['Late Arrival'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $arrival_fees))
									{
										$special_trans['Early Arrival'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $departure_fees))
									{
										$special_trans['Early Departure'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $extended_fees))
									{
										$special_trans['Extended Stay'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $cont_ed_fees))
									{
										$special_trans['Continuing Education'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $rec_fees))
									{
										$special_trans['Recreation Fees'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $transportation_fees))
									{
										$special_trans['Transportation Fees'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $group_fees))
									{
										$special_trans['Group Fees'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(in_array(strtolower($record->order_item_name), $trip_fees))
									{
										$special_trans['Trip Fees'] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
									else if(!in_array(strtolower($record->order_item_name), $mpa_labels))
									{
										$special_trans[$record->order_item_name] = floatval(preg_replace("/[^0-9.]/", "", $main_item->meta_value));
									}
								}
							}
						}
						//echo var_dump($special_trans);
						//exit;
					}

					break;

				default:
					break;

			}
		}
	}

}