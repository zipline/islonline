<?php

function migrate_teams($Range = "ALL", $wpdb, $cur_wp, $cur_db)
{

	$team = new TEAM();
	
	$query = "SELECT t.id, t.number, t.number_legacy, t.arrival_city, t.arrival_country_id, t.departure_city, t.departure_country_id,
					 t.start_date, t.end_date, t.team_status_id, t.public, t.private, t.is_ip, t.status_chart_note, t.cost,
					 t.high_school_team, t.general_note, t.teaser, t.public_team_note, IFNULL(t.minimum,1) minimum, IFNULL(t.goal, 1) goal,
					 t.start_date, t.end_date, t.school_paid,
					 GROUP_CONCAT(DISTINCT p.id ORDER BY p.id DESC SEPARATOR ',') program_id, 
					 GROUP_CONCAT(DISTINCT p.name ORDER BY p.name DESC SEPARATOR ',') program_name, 
					 GROUP_CONCAT(DISTINCT c.id ORDER BY c.id DESC SEPARATOR ',') country_id, 
					 GROUP_CONCAT(DISTINCT c.name ORDER BY c.name DESC SEPARATOR ',') country_name,
					 ac.name arrival_country_name,
					 dpc.name departure_country_name
			  FROM 
			  	islonlin_app2015.teams t 
					LEFT JOIN islonlin_app2015.countries_teams ct on t.id = ct.team_id
					LEFT JOIN islonlin_app2015.countries c ON ct.country_id = c.id
					LEFT JOIN islonlin_app2015.programs_teams pt on t.id = pt.team_id
					LEFT JOIN islonlin_app2015.programs p on pt.program_id = p.id
					LEFT JOIN islonlin_app2015.countries ac on t.arrival_country_id = ac.id
					LEFT JOIN islonlin_app2015.countries dpc on t.departure_country_id = dpc.id

			 " . (($Range == "ALL") ? '' : 'WHERE t.created >= NOW() - INTERVAL ' . $Range . ' DAY OR t.modified >= NOW() - INTERVAL ' . $Range . ' DAY' ) 
			   .	
			 "
			 WHERE t.id = 3799
			 GROUP BY
			 	t.id";
	$results = $cur_db->query($query);
	
	//
	//Cycle thorugh all teams
	while($record = $results->fetch_object())
	{
		//
		//Try to figure out team type - default it in case none of the filters work
		$team_type = 1;
		if(!empty($record->public) && $record->public == 1)
		{
			$team_type = 1;
		}
		if(!empty($record->high_school_team) && $record->high_school_team == 1)
		{
			$team_type = 2;
		}
		if(!empty($record->private) && $record->private == 1)
		{
			$team_type = 3;
		}
		if(!empty($record->is_ip) && $record->is_ip == 1)
		{
			$team_type = 4;
		}
		if(!empty($record->high_school_team) && $record->high_school_team == 1 && !empty($record->private) && $record->private == 1)
		{
			$team_type = 5;
		}
		if(!empty($record->cost) && floatval(preg_replace("/[^0-9.]/", "", $record->cost)) > 3000)
		{
			$team_type = 27;
		}
		
		//
		//Try to figure out team hash
		unset($hash);
		if(substr($record->number,0,1) == '#')
		{
			$hash = $record->number;
		}
		else
		{
			$hash = '#' . $record->number;
		}
		
		//
		//Try to figure out arrival city and country
		if(!empty($record->arrival_country_id))
		{
			$arrival_city = $record->arrival_city;
			$arrival_country = $record->arrival_country_name;
		}
		else
		{
			switch($record->arrival_city)
			{
				case 'Belize City':
				case 'Belize City (BZE)':
					$arrival_city = 'Belize City';
					$arrival_country = 'Belize';
					break;
				case 'Cancun':
				case 'Cancun International Airport':
					$arrival_city = 'Cancun';
					$arrival_country = 'Mexico';
					break;
				case 'Havana':
					$arrival_city = 'Havana';
					$arrival_country = 'Cuba';
					break;
				case 'Kilimanjaro':
				case 'Kilimanjaro International Airport':
				case 'Kilimanjaro International Airport (JRO)':
					$arrival_city = 'Kilimanjaro';
					$arrival_country = 'Tanzania';
					break;
				case 'Kingston':
					$arrival_city = 'Kingston';
					$arrival_country = 'Jamaica';
					break;
				case 'Lima':
					$arrival_city = 'Lima';
					$arrival_country = 'Peru';
					break;
				case 'Managua':
				case 'Managua, Nicaragua':
				case 'Managua, Nicaragua (MGA)':
					$arrival_city = 'Managaua';
					$arrival_country = 'Nicaragua';
					break;
				case 'Panama City':
				case 'Panama City Airport':
				case 'Panama City Airport (PTY)':
					$arrival_city = 'Panama City';
					$arrival_country = 'Panama';
					break;
				case 'Pereira':
					$arrival_city = 'Pereira';
					$arrival_country = 'Columbia';
					break;
				case 'Phoenix':
				case 'Phoenix, AZ':
					$arrival_city = 'Phoenix, AZ';
					$arrival_country = 'United States';
					break;
				case 'San Jose':
				case 'San Jose, Costa Rica':
				case 'San Jose, Costa Rica (SJO)':
					$arrival_city = 'San Jose';
					$arrival_country = 'Costa Rica';
					break;
				case 'Santo Domingo':
				case 'Santo Domingo - Los Americas Airport':
				case 'Santo Domingo - Los Americas Airport (SDQ)':
					$arrival_city = 'Santa Domingo';
					$arrival_country = 'Dominican Republic';
					break;	
			}
		}
		
		//
		//Try to figure out departure city and country
		if(!empty($record->departure_country_id))
		{
			$departure_city = $record->departure_city;
			$departure_country = $record->departure_country_name;
		}
		else
		{
			switch($record->arrival_city)
			{
				case 'Belize City':
				case 'Belize City (BZE)':
					$departure_city = 'Belize City';
					$departure_country = 'Belize';
					break;
				case 'Cancun':
				case 'Cancun International Airport':
					$departure_city = 'Cancun';
					$departure_country = 'Mexico';
					break;
				case 'Havana':
					$departure_city = 'Havana';
					$departure_country = 'Cuba';
					break;
				case 'Kilimanjaro':
				case 'Kilimanjaro International Airport':
				case 'Kilimanjaro International Airport (JRO)':
					$departure_city = 'Kilimanjaro';
					$departure_country = 'Tanzania';
					break;
				case 'Kingston':
					$departure_city = 'Kingston';
					$departure_country = 'Jamaica';
					break;
				case 'Lima':
					$departure_city = 'Lima';
					$departure_country = 'Peru';
					break;
				case 'Managua':
				case 'Managua, Nicaragua':
				case 'Managua, Nicaragua (MGA)':
					$departure_city = 'Managaua';
					$departure_country = 'Nicaragua';
					break;
				case 'Panama City':
				case 'Panama City Airport':
				case 'Panama City Airport (PTY)':
					$departure_city = 'Panama City';
					$departure_country = 'Panama';
					break;
				case 'Pereira':
					$departure_city = 'Pereira';
					$departure_country = 'Columbia';
					break;
				case 'Phoenix':
				case 'Phoenix, AZ':
					$departure_city = 'Phoenix, AZ';
					$departure_country = 'United States';
					break;
				case 'San Jose':
				case 'San Jose, Costa Rica':
				case 'San Jose, Costa Rica (SJO)':
					$departure_city = 'San Jose';
					$departure_country = 'Costa Rica';
					break;
				case 'Santo Domingo':
				case 'Santo Domingo - Los Americas Airport':
				case 'Santo Domingo - Los Americas Airport (SDQ)':
					$departure_city = 'Santa Domingo';
					$departure_country = 'Dominican Republic';
					break;	
			}
		}
		
		//
		//Determine if we need to update or build new...
		$query = "SELECT 
						t.id 
				  FROM
				  		wp_command_TEAM t INNER JOIN wp_postmeta pm on t.post_id = pm.post_id
				  WHERE
				  		pm.meta_key = '_original_team_id' AND pm.meta_value = " . $record->id;
		$team_number = $wpdb->get_var($query);
		
		if(!$team_number)
		{
			$team->build(array('hash' => $hash,
							   'type' => $team_type,
							   'status' => $record->team_status_id,
							   'arrival_city' => $arrival_city,
							   'arrival_country' => $arrival_country,
							   'departure_city' => $departure_city,
							   'departure_country' => $departure_country,
							   'min' => floatval(preg_replace("/[^0-9]/", "", $record->minimum)),
							   'max' => floatval(preg_replace("/[^0-9]/", "", $record->goal)),
							   'cost' => floatval(preg_replace("/[^0-9]/", "", $record->cost)),
							   'team_chat' => 1,
							   'appfee' => 85,
							   'deposit' => 200,
							   'messaging' => 0
						));
			
			update_post_meta($team->value('post'), '_original_team_id', $record->id);

			echo "Building Team: " . $hash;
		}
		else
		{
			$team->set_team($team_number);
			echo "Updating Team: " . $team->value('hash') . " / " . $team->value('code') . "<BR>";
			
			if($team->value('type') != $team_type)
			{
				$team->update('type', $team_type);
			}
			if($team->value('status') != $record->team_status_id)
			{
				$team->update('status', $record->team_status_id);
			}
			if($team->value('arrival_city') != $arrival_city)
			{
				$team->update('arrival_city', $arrival_city);
			}
			if($team->value('arrival_country') != $arrival_country)
			{
				$team->update('arrival_country', $arrival_country);
			}
			if($team->value('departure_city') != $departure_city)
			{
				$team->update('departure_city', $departure_city);
			}
			if($team->value('departure_country') != $departure_country)
			{
				$team->update('departure_country', $departure_country);
			}
			if($team->value('min') != floatval(preg_replace("/[^0-9]/", "", $record->minimum)))
			{
				$team->update('min', floatval(preg_replace("/[^0-9]/", "", $record->minimum)));
			}
			if($team->value('max') != floatval(preg_replace("/[^0-9]/", "", $record->goal)))
			{
				$team->update('max', floatval(preg_replace("/[^0-9]/", "", $record->goal)));
			}
			if($team->value('cost') != floatval(preg_replace("/[^0-9]/", "", $record->cost)))
			{
				$team->update('cost', floatval(preg_replace("/[^0-9]/", "", $record->cost)));
			}
		}
		
		
		//
		//Insert Schedule if this is new since the old system doesn't allow for those changes
		if(!$team_number)
		{
			foreach(explode(',', $record->program_id) as $p_element => $prog_id)
			{
				foreach(explode(',', $record->country_id) as $c_element => $country_id)
				{
					$split_country = 0;
					$split_region = 0;

					switch($country_id)
					{
						case 2:
							$actual_country = $country_id;
							$actual_region = 1;
							break;
						case 3:	
							$actual_country = $country_id;
							$actual_region = 2;
							break;
						case 4:	
							$actual_country = $country_id;
							$actual_region = 3;
							break;
						case 5:	
							$actual_country = $country_id;
							$actual_region = 4;
							break;
						case 6:	
							$actual_country = $country_id;
							$actual_region = 5;
							break;
						case 8:	
							$actual_country = $country_id;
							$actual_region = 6;
							break;
						case 9:	
							$actual_country = $country_id;
							$actual_region = 7;
							break;
						case 17:	
							$actual_country = $country_id;
							$actual_region = 8;
							break;
						case 18:	
							$actual_country = 5;
							$actual_region = 4;

							$split_country = 37;
							$split_region = 544;
							break;
						case 19:	
							$actual_country = $country_id;
							$actual_region = 9;
							break;
						case 20:	
							$actual_country = $country_id;
							$actual_region = 10;
							break;
						case 21:	
							$actual_country = 17;
							$actual_region = 8;
							break;
						case 22:	
							$actual_country = 33;
							$actual_region = 356;
							break;
						case 23:	
							$actual_country = 30;
							$actual_region = 14;
							break;
						case 24:	
							$actual_country = 30;
							$actual_region = 15;
							break;
						case 25:	
							$actual_country = 30;
							$actual_region = 16;
							break;
						case 26:	
						case 27:	
							$actual_country = 2;
							$actual_region = 0;

							$split_country = 9;
							$split_region = 0;
							break;
						case 28:	
						case 29:	
							$actual_country = 2;
							$actual_region = 1;

							$split_country = 6;
							$split_region = 5;
							break;
						case 30:	
							$actual_country = $country_id;
							$actual_region = 11;
							break;
						case 31:	
							$actual_country = $country_id;
							$actual_region = 12;
							break;
						case 32:	
							$actual_country = $country_id;
							$actual_region = 13;
							break;
						case 34:	
							$actual_country = 8;
							$actual_region = 17;
							break;
						case 35:	
							$actual_country = 8;
							$actual_region = 18;
							break;
						case 37:	
						case 40:	
						case 41:	
							$actual_country = 41;
							$actual_region = 655;
							break;
						case 43:	
							$actual_country = 43;
							$actual_region = 656;
							break;
					}

					$write_id = 0;
					$cntry = $actual_country;	
					$regn = $actual_region;
					$prgm = $prog_id;	
					$cost = 0;	
					$start = substr($record->start_date,5,2) . "/" . substr($record->start_date,8,2) . "/" .substr($record->start_date,0,4);	
					$end = substr($record->end_date,5,2) . "/" . substr($record->end_date,8,2) . "/" .substr($record->end_date,0,4);	

					if($prgm != 91)
					{
						$team->save_segment($write_id, $cntry, $regn, $prgm, $cost, $start, $end);

						if(!empty($split_country))
						{
							$team->save_segment($write_id, $split_country, $split_region, $prgm, $cost, $start, $end);
						}
					}
				}
			}

			echo " / " . $team->value('code') . " from (" . $record->number_legacy . ")<BR>";
		}
		flush();
		
		//
		//Update charts and notes		
		$team_note = '';
		if(isset($record->general_note) || isset($record->status_chart_note))
		{
			if(!empty($record->status_chart_note))
			{
				$team_note .= $record->status_chart_note;
			}
			if(!empty($record->general_note))
			{
				$team_note .= $record->general_note;
			}
			
			if(!empty($team_note) && strlen($team_note) > 1)
			{
				update_post_meta($team->value('post'), '_chart_note', strip_tags($team_note));
			}
		}
		
		if(isset($record->public_team_note))
		{
			update_post_meta($team->value('post'), '_public_team_note', strip_tags($record->public_team_note));
		}

		if(isset($record->teaser))
		{
			$team->save_blurb('teaser', $record->teaser);
		}
		
		if(!empty($record->school_paid) && $record->school_paid == 1)
		{
			$team->update("no_finances", 1);
		}
		
		//
		//Add MPA
		update_post_meta($team->value('post'), '_cmd_add_ons', array(36402 => 75));

		//
		//Update Status
		$team->update('status', $record->team_status_id);
	}	// End Record Loop
}