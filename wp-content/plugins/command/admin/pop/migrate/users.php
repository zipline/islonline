<?php

function migrate_users($Range = "ALL", $wpdb, $cur_wp, $cur_db)
{

	if($Range == "ALL")
	{

		$query = "SELECT * FROM 
				 (
					 (
					  SELECT 
							'WP' source, wpu.ID, wpu.user_login, wpu.user_pass, wpu.user_nicename, wpu.user_email, 
							wpu.user_url, wpu.user_registered, wpu.user_status, wpu.display_name,
							wpm.meta_value member_id 
					  FROM 
							islonlin_wp2015.wp_users wpu LEFT JOIN 
								(SELECT user_id, meta_value FROM islonlin_wp2015.wp_usermeta WHERE meta_key = 'member_id') wpm
							ON wpu.ID = wpm.user_id
					  WHERE
							wpm.meta_value >= 2900
					  ORDER BY 
							member_id
					  ) 

					  UNION 

					 (
					 SELECT 
							'DB' source, m.id + 20000 ID, IFNULL(m.login, m.email) user_login, MD5(CONCAT('ILostMyPassword', m.email)) user_pass, 
							CONCAT(m.first_name, ' ' , m.last_name) user_nicename, m.email user_email, '' user_url, '2010-01-01' user_registered, 
							0 user_status, m.first_name display_name, m.id member_id	
					 FROM
							islonlin_app2015.members m LEFT JOIN 
								(SELECT 
										wpu.ID, wpu.user_login, wpu.user_pass, wpu.user_nicename, wpu.user_email, wpu.user_url, 
										wpu.user_registered, wpu.user_status, wpu.display_name, wpm.meta_value member_id 
								 FROM 
										islonlin_wp2015.wp_users wpu LEFT JOIN 
											(SELECT user_id, meta_value FROM islonlin_wp2015.wp_usermeta WHERE meta_key = 'member_id') wpm
										ON wpu.ID = wpm.user_id
								 WHERE
										wpm.meta_value >= 2900
								 ORDER BY 
										member_id
								) wp_users

							ON m.id = wp_users.member_id

					 WHERE 
							wp_users.member_id is null
					) 
				) result

				WHERE 
					ID >= 0

				ORDER BY 
					ID asc";

	}
	else
	{
		$query = "SELECT 'WP' source, wp_users.*, 0 member_id FROM islonlin_wp2015.wp_users WHERE ID > 2000 AND user_registered >= NOW() - INTERVAL " . $Range . " DAY";
	}
	$results = $cur_wp->query($query);
	
	//
	//Cycle thorugh all users
	while($record = $results->fetch_object())
	{
		//
		//Check to see if the record/email already exists
		$updating = false;
		$this_user = get_user_by( 'email' , $record->user_email );
		
		if($this_user)
		{
			//
			//Already found this user....so...check to see if the member id is already in use
			$meta_query = "SELECT meta_value FROM wp_usermeta WHERE meta_key = 'member_id' AND user_id = " . $this_user->ID;
			$meta_result = $wpdb->get_var ($meta_query);

			//
			//Add the member_id to the current meta value
			if($meta_result)
			{
				//
				//Check to see if they match
				if($meta_result != $record->member_id)
				{
					update_user_meta ($this_user->ID, 'member_id', $meta_result . ',' . $record->member_id);
					$updating = true;
				}
			}
		}
		else
		{
			$user_query = "REPLACE INTO " . $wpdb->prefix . "users 
								(ID, 
								 user_login, 
								 user_pass, 
								 user_nicename, 
								 user_email, 
								 user_url, 
								 user_registered, 
								 user_status, 
								 display_name
								)
						   VALUES
								(" . $record->ID . ",
								 '" . trim($record->user_login) . "',
								 '" . addslashes($record->user_pass) . "',
								 '" . addslashes($record->user_nicename) . "',
								 '" . trim($record->user_email) . "',
								 '" . $record->user_url . "',
								 '" . $record->user_registered . "',
								 '" . $record->user_status . "',
								 '" . addslashes($record->display_name) . "'
								);";
			$wpdb->query($user_query);
		}
		
		//
		//Get user_meta and convert it
		if(!$this_user || 1)
		{
			echo "Writing " . $record->ID . ": " . $record->user_email;
			flush();
			
			switch($record->source)
			{
				case "WP":
					$user_query = "(SELECT * FROM islonlin_wp2015.wp_usermeta WHERE user_id = " . $record->ID . ")
									UNION
								   (SELECT 'x', " . $record->ID . ", 'billing_state' meta_key, states.id meta_value 
									FROM islonlin_wp2015.wp_usermeta INNER JOIN islonlin_app2015.states on wp_usermeta.meta_value = states.abbreviation
									WHERE wp_usermeta.user_id=" . $record->ID . " AND wp_usermeta.meta_key = 'billing_state')
									UNION
									(SELECT 'x', " . $record->ID . ", 'email_2' meta_key, email_2 meta_value
									FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")";
					break;

				case "DB":
					$user_query = "(SELECT 'first_name' meta_key, first_name meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'last_name' meta_key, last_name meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'email_2' meta_key, email_2 meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'nickname' meta_key, first_name meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'member_id' meta_key, id meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'wp_user_level' meta_key, '0' meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'rich_editing' meta_key, 'true' meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'billing_phone' meta_key, phone_1 meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'billing_address_1' meta_key, mailing_address_street meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'billing_address_2' meta_key, mailing_address_street_2 meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'billing_city' meta_key, mailing_address_city meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'billing_state' meta_key, states.id meta_value 
									FROM islonlin_app2015.members INNER JOIN islonlin_app2015.states on members.state_id = states.id
									WHERE members.id=" . $record->member_id . ")
									UNION
								   (SELECT 'billing_postcode' meta_key, mailing_address_zip meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'gender' meta_key, IF(gender_id=1,'Male','Female') meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'birth_date' meta_key, birth_date meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'admin_color' meta_key, 'midnight' meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")
									UNION
								   (SELECT 'wp_capabilities' meta_key, 'customer' meta_value FROM islonlin_app2015.members WHERE id=" . $record->member_id . ")";
					break;

			}
			$user_results = $cur_wp->query($user_query);

			$pic = false;
			if($user_results)
			{
				while($user_row = $user_results->fetch_object())
				{
					//echo $user_row->meta_key . ":" . $user_row->meta_value . "<BR>";
					switch($user_row->meta_key)
					{
						case 'first_name':
						case 'last_name':
						case 'nickname':
						case 'description':
						case 'member_id':
						case 'wp_user_level':
						case 'rich_editing':

							update_user_meta ($record->ID, $user_row->meta_key, $user_row->meta_value);
							break;

						case 'billing_phone':
							update_user_meta ($record->ID, 'primary_phone', $user_row->meta_value);
							update_user_meta ($record->ID, '_primary_phone', 'field_5721d1c98b81e');
							break;

						case 'billing_address_1':
							update_user_meta ($record->ID, 'mailing_address_line_1', $user_row->meta_value);
							update_user_meta ($record->ID, '_mailing_address_line_1', 'field_5721d2078b820');
							break;

						case 'billing_address_2':
							update_user_meta ($record->ID, 'mailing_address_line_2', $user_row->meta_value);
							update_user_meta ($record->ID, '_mailing_address_line_2', 'field_5721d2078b821');
							break;

						case 'billing_city':
							update_user_meta ($record->ID, 'city', $user_row->meta_value);
							update_user_meta ($record->ID, '_city', 'field_5721d2338b822');
							break;

						case 'billing_state':
							update_user_meta ($record->ID, 'state', $user_row->meta_value);
							update_user_meta ($record->ID, '_state', 'field_5721d2418b823');
							break;

						case 'billing_postcode':
							update_user_meta ($record->ID, 'zip_code', $user_row->meta_value);
							update_user_meta ($record->ID, '_zip_code', 'field_5721d3488b824');
							break;

						case 'billing_phone_2':
							update_user_meta ($record->ID, 'secondary_phone', $user_row->meta_value);
							update_user_meta ($record->ID, '_secondary_phone', 'field_5721d1f18b81f');
							break;

						case 'email_2':
							if($user_row->meta_value != $record->user_email)
							{
								update_user_meta ($record->ID, 'secondary_email', $user_row->meta_value);
								update_user_meta ($record->ID, '_secondary_email', 'field_5721d1b78b81d');
							}
							break;

						case 'gender':
							update_user_meta ($record->ID, $user_row->meta_key, $user_row->meta_value);
							update_user_meta ($record->ID, '_gender', 'field_56fc9743543d0');
							break;

						case 'birth_date':
							if($user_row->meta_value != '0000-00-00')
							{
								$temp_bd = DateTime::createFromFormat('Y-m-d', $user_row->meta_value);

								if(!empty($temp_bd))
								{
									update_user_meta ($record->ID, 'birthdate', $temp_bd->format('m/d/Y'));
									update_user_meta ($record->ID, '_birthdate', 'field_57249f7a0c63c');
								}
							}
							break;

						case 'admin_color':
							update_user_meta ($record->ID, $user_row->meta_key, 'midnight');
							break;

						case 'wp_capabilities':
							unset($permissions);

							if(strpos($user_row->meta_value, 'customer'))
							{
								$permissions[] = 'volunteer';
							}
							if(strpos($user_row->meta_value, 'administrator'))
							{
								$permissions[] = 'administrator';
							}

							if(empty($permissions))
							{
								$permissions[] = 'volunteer';
							}
							
							update_user_meta ($record->ID, $user_row->meta_key, $permissions);

						default:
							break;
					}
				}

				//
				//Non-converted USer Meta
				update_user_meta ($record->ID, 'contact_preference', array(0=>'Email'));
				update_user_meta ($record->ID, '_contact_preference', 'field_57ff5711447f7');

				//
				//Get profile pic
				if($record->source == "WP" && !$this_user)
				{
					$pic = false;
					//$profile_pic = 'http://www.islonline.org/wp-content/uploads/profilepics/' . $record->ID . '_profile_picture.jpg';

					$pic_query = "SELECT meta_value FROM islonlin_wp2015.wp_usermeta WHERE user_id = " . $record->ID . " AND meta_key = 'profile_image'";
					$pic_results = $cur_wp->query($pic_query);

					if($pic_results)
					{
						$profile_row = $pic_results->fetch_object();

						if($profile_row)
						{
							$profile_pic = $profile_row->meta_value;
							$pic_handle = @fopen($profile_pic, 'rb');
							$filename = substr($profile_pic, strrpos($profile_pic, "/") + 1);
							
							if($pic_handle)
							{
								$contents = '';
								while(!feof($pic_handle))
								{
									$contents .= @fread($pic_handle, 8192);
								}
								
								if(file_put_contents(WP_CONTENT_DIR . '/user-pics/' . $filename, $contents))
								{
									update_user_meta ($record->ID, 'local_avatar', 'https://www.islonline.org/wp-content/user-pics/' . $filename);
									update_user_meta ($record->ID, '_local_avatar', 'field_56f882d3ccb2a');
									$pic = true;
								}
								
								@fclose($pic_handle);
							}
						}
					}
				}	//End Profile pic for WP

			}
			
			if(!$pic)
			{
				echo "  (No Pic)<BR>";
			}
			else
			{
				echo  "<BR>";
			}
		}
		else
		{
			if($updating)
			{
				echo "Updating " . $record->ID . ": " . $record->user_email . "<BR>";
			}
			else
			{
				echo "Duplicate " . $record->ID . ": " . $record->user_email . "<BR>";
			}
		}
		flush();
	}
	
	mysqli_free_result($results);
}
	
