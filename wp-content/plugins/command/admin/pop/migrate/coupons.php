<?php

function migrate_coupons($Range = "ALL", $wpdb, $cur_wp, $cur_db)
{
	$query = "SELECT 
				   p.id, p.post_date, IF(p.post_excerpt = '', p.post_name, p.post_excerpt) post_excerpt, p.post_name, p.post_author, p.post_title, 
				   pm_amt.meta_value amount, pm_exp.meta_value expiration, pm_type.meta_value type

			  FROM 
			  	   islonlin_wp2015.wp_posts p
				   		LEFT JOIN islonlin_wp2015.wp_postmeta pm_amt on p.id = pm_amt.post_id
				   		LEFT JOIN islonlin_wp2015.wp_postmeta pm_exp on p.id = pm_exp.post_id
				   		LEFT JOIN islonlin_wp2015.wp_postmeta pm_type on p.id = pm_type.post_id
				        
			  WHERE 
			       p.post_type = 'shop_coupon' AND
				   pm_amt.meta_key = 'coupon_amount' AND
				   pm_exp.meta_key = 'expiry_date' AND
				   pm_type.meta_key = 'discount_type'
			  ORDER BY
			  	   p.id";
	$results = $cur_wp->query($query);
echo $query;	
	//
	//Cycle thorugh all users
	while($record = $results->fetch_object())
	{

		if($record->post_excerpt == "New Ambassador (QB)" || 
		   $record->post_excerpt == "New Rep (QB)" || 
		   $record->post_excerpt == "new rep" || 
		   $record->post_excerpt == ""
		  )
		{
			$missed_codes[] = $record->post_name;
		}
		else
		{
			//
			//Check to see if the exceprt is a name...this is a two word, one space item that is not null
			$cap_code = false;
			if(!empty($record->post_excerpt) && substr_count($record->post_excerpt, ' ') == 1)
			{

				list($fname, $lname) = explode(' ', $record->post_excerpt);

				if(!empty($fname) && !empty($lname))
				{
					$u_query = array(
									'meta_query' => array(
														'relation' => 'AND',
														array(
															'key' => 'first_name',
															'value' => $fname,
															'compare' => '='
														),
														array(
															'key' => 'last_name',
															'value' => $lname,
															'compare' => '='
														),
													)
									);

					$user_check = new WP_USER_QUERY( $u_query );
					if(!empty($user_check->results))
					{
						$cap_code = true;
					}
				}
			}


			if($cap_code)
			{
				$user_record = @current($user_check->results);
				
				if(!empty($user_record))
				{
	
					//
					//Upgrade user to enhanced
					$user_updates = array('ID' => $user_record->ID,
										  'role' => 'enhanced');
					wp_update_user($user_updates);

					//
					//Fix values
					$end_date = substr($record->expiration, 5, 2) . '-' . substr($record->expiration, 8, 2) . '-' . substr($record->expiration, 0, 4);
					$req_date = substr($record->post_date, 5, 2) . '-' . substr($record->post_date, 8, 2) . '-' . substr($record->post_date, 0, 4);

					//
					//Write in CAP CODE fields
					update_user_meta($user_record->ID, 'cmd_isl_rep', 'true');
					update_user_meta($user_record->ID, '_cmd_comm_ambassador_status', 'true');
					update_user_meta($user_record->ID, '_cmd_comm_ambassador_post', '0');
					update_user_meta($user_record->ID, '_cmd_comm_ambassador_request_date', $req_date);
					update_user_meta($user_record->ID, '_cmd_comm_ambassador_cap_code', $record->post_name);
					update_user_meta($user_record->ID, '_cmd_comm_ambassador_edate', $end_date);
					update_user_meta($user_record->ID, '_cmd_comm_ambassador_original_amt', $record->amount);
				}
			}
			else
			{
				
				$post_id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . addslashes($record->post_excerpt) . "'" );

				if(!$post_id)
				{
					//
					//What we found must be a coupon...
					$field_data=array(	'post_author' => $record->post_author,
										'post_date' => $record->post_date,
										//'post_date_gmt' => get_gmt_from_date(strval(strtotime($record->post_date))),
										'post_content' => ' ',
										'post_title' => ((empty($record->post_excerpt)) ? $record->post_title : $record->post_excerpt),
										'post_status' => 'publish',
										'comment_status' => 'closed',
										'ping_status' => 'closed',
										'post_name' => $record->post_name,
										'post_modified' => '2017-12-01 12:01:01',
										'post_modified_gmt' => '2017-12-01 12:01:01',
										'post_parent' => 0,
										'menu_order' => 0,
										'post_type' => 'isl_coupon',
										'page_template' => '',
										'comment_count' => 0
								);
					$post_id = wp_insert_post($field_data, true);				
				}
				
				update_post_meta($post_id, '_code', 'field_597c69281b656');
				update_post_meta($post_id, '_discount', 'field_597c69581b657');
				update_post_meta($post_id, '_uses', 'field_597c69ba1b658');
				update_post_meta($post_id, '_team', 'field_597c69d81b659');
				update_post_meta($post_id, '_expiration', 'field_597c6a021b65a');
				update_post_meta($post_id, '_notes', 'field_597c6a201b65b');

				$end_date = substr($record->expiration, 0, 4) . substr($record->expiration, 5, 2) . substr($record->expiration, 8, 2);

				switch($record->type)
				{
					case "fixed_cart":
					case "fixed_product":
						$discount = '$' . number_format(preg_replace("/[^0-9,.]/", '', $record->amount),2);
						break;

					case "percent":
					case "percent_product":
						$discount = number_format(preg_replace("/[^0-9,.]/", '', $record->amount),2) . '%';
						break;

				}

				update_post_meta($post_id, 'code', $record->post_name);
				update_post_meta($post_id, 'discount', $discount);
				update_post_meta($post_id, 'uses', '0');
				update_post_meta($post_id, 'team', '');
				update_post_meta($post_id, 'expiration', $end_date);
				update_post_meta($post_id, 'notes', '');

				update_post_meta($post_id, '_edit_lock', '1513162270:1');
				update_post_meta($post_id, '_edit_last', '1');

				/*
				update_field('field_597c69281b656', $record->post_name, $post_id);
				update_field('field_597c69281b657', $discount, $post_id);
				update_field('field_597c69281b658', 0, $post_id );
				update_field('field_597c69281b659' ,'' , $post_id);
				update_field('field_597c69281b65a', $end_date, $post_id);
				update_field('field_597c69281b65b', '', $post_id);
				*/
			}

		}
		/*
						   GROUP_CONCAT(IF(ISNULL(pm_used.meta_value), 0, pm_used.meta_value) DESC SEPARATOR ',')
				   		LEFT JOIN islonlin_wp2015.wp_postmeta pm_used on p.id = pm_used.post_id
				   pm_used.meta_key = '_used_by'
		*/
		
		
	}
	
	if(!empty($missed_codes))
	{
		print_r($missed_codes);
	}
}