<?php

function migrate_res_link($Range = "ALL", $wpdb, $cur_wp, $cur_db)
{
	
	$query = "SELECT 
					reserves.id, reserves.email, reserves.team_id, reserves.number, reserves.post_id, reserves.created,
					financials.order_item_id, financials.order_item_name, IFNULL(financials.order_item_type, '_missing') order_item_type, 
					financials.order_id, financials.user
			  FROM 
				
				  (
				  SELECT 
						r.id, m.email, s.team_id, s.number, pm.post_id, r.created
				  FROM
						islonlin_app2015.reservations r
							INNER JOIN islonlin_app2015.members m on r.member_id = m.id
							LEFT JOIN islonlin_app2015.subteams s on r.subteam_id = s.id
							LEFT JOIN
								(SELECT post_id, meta_value FROM islonlin_wp2015.wp_postmeta WHERE meta_key='reservation_id') pm on pm.meta_value = r.id
				  WHERE
				  		" . (($Range == "ALL") ? '' : '(r.created >= NOW() - INTERVAL ' . $Range . ' DAY OR r.modified >= NOW() - INTERVAL ' . $Range . ' DAY) AND ' ) . "
						r.subteam_id IS NOT NULL
				  ) 
				  reserves

			  LEFT JOIN
			  
				  (
				  SELECT 
					   p.id, woi.*, pm.meta_value user
				  FROM 
					   islonlin_wp2015.wp_posts p
							INNER JOIN islonlin_wp2015.wp_woocommerce_order_items woi on p.ID = woi.order_id
							INNER JOIN islonlin_wp2015.wp_postmeta pm on p.ID = pm.post_id
				  WHERE 
					   p.post_type = 'shop_order' AND
					   pm.meta_key = '_customer_user' AND
					   woi.order_id <= 200000
				  ORDER BY
					   p.id DESC, woi.order_item_id
				  )
				  financials
			  
			  	  ON reserves.post_id = financials.id
			  
			  WHERE reserves.id <= 13000 AND reserves.id >=11406

				UNION
			  
			  SELECT 2000000, 'x', 0, 0, 0, 0, 0, 0, 0, 0, 0
			  
  			  ORDER BY
			  	id, post_id";

	$results = $cur_db->query($query);
	$last_id = 0;
	$last_user_id = 0;
	$last_team_id = 0;
	$last_order_id = 0;
	$fail = false;
	
	//
	//Cycle thorugh all users
	while($record = $results->fetch_object())
	{
		//
		//If we have some data and we are now moving onto a new record, write the old transaction...
		if($last_id > 0 && $last_id != $record->id && $last_user_id > 0 && $last_team_id > 0 && !$fail)
		{
			//
			//Find the reservation and flag it as special
			$rsvp = new RSVP;
			$rsvp->set_id_by_team_user($last_team_id, $last_user_id);
			$reservation = $rsvp->fetch_rsvp();

			if($reservation)
			{
				update_post_meta($reservation['post_id'], 'migrate_special', 'true');
				update_post_meta($reservation['post_id'], 'migrate_special_db_value', $last_id);
				update_post_meta($reservation['post_id'], 'migrate_special_wp_value', $last_order_id);
			}
		}

		//
		//Grab Team and User stuff for the new one
		if($last_id != $record->id)
		{
			unset($actual_team_number);
			unset($actual_user);
			
			$actual_team_query = "SELECT 
										t.id 
				  				  FROM
				  						wp_command_TEAM t INNER JOIN wp_postmeta pm on t.post_id = pm.post_id
				  				  WHERE
				  						pm.meta_key = '_original_team_id' AND pm.meta_value = " . $record->team_id;
			$actual_team_number = $wpdb->get_var($actual_team_query);
			
			if($actual_team_number)
			{
				$team = new TEAM();
				$team->set_team($actual_team_number);
			}
			
			//
			//Initial user find
			$actual_user = get_user_by( 'email' , trim($record->email));
			
			//
			//Initial user set attempt
			if($actual_user)
			{
				wp_set_current_user($actual_user->ID);
			}
			else
			{
				//
				//Secondary user find...
				$temp_id_query = "SELECT 
										um.user_id 
								  FROM 
								  		islonlin_wp2015.wp_usermeta um
										INNER JOIN islonlin_app2015.members m on um.meta_value = m.id
								  WHERE 
								  		um.meta_key='member_id' AND
										m.email = '" . trim($record->email) . "'";
				$temp_id = $cur_wp->query($temp_id_query);
				
				if($temp_id)
				{
					$temp_record = $temp_id->fetch_object();
					
					if(!empty($temp_record))
					{
						$actual_user = get_user_by( 'ID' , $temp_record->user_id);
					}
				}
				@mysqli_free_result($temp_id);
				
				//
				//Secondary user set attempt
				if($actual_user)
				{
					wp_set_current_user($actual_user->ID);
				}
				else
				{
					//
					//Tertiary user find...
					$temp_id_query = "SELECT 
											m.id + 20000 user_id 
									  FROM 
											islonlin_app2015.members m
									  WHERE 
											m.email = '" . trim($record->email) . "'";
					$temp_id = $cur_wp->query($temp_id_query);

					if($temp_id)
					{
						$temp_record = $temp_id->fetch_object();

						if(!empty($temp_record))
						{
							$actual_user = get_user_by( 'ID' , $temp_record->user_id);
						}
					}
					@mysqli_free_result($temp_id);

					//
					//Tertiary user set attempt
					if($actual_user)
					{
						wp_set_current_user($actual_user->ID);
					}

				}

			}

			
			//
			//Got good data, so start things up
			if($actual_team_number && $actual_user)
			{
				echo "Starting " . $record->id . " for team " . $team->value('hash') . " for " . $actual_user->first_name . " " . $actual_user->last_name . "<BR>";
			}
			else
			{				
				echo "Bad Record: " . $record->id . " ";
				
				if(!$actual_team_number)
				{
					echo "(Team: " . $record->team_id . ") ";
				}
				
				if(!$actual_user)
				{
					echo "(User: " . trim($record->email) . ") ";
				}

				echo "<BR>";
			}
			
			//
			//Clear cart table
			$wpdb->query('DELETE FROM wp_command_cart');

			flush();
		}
		
		//
		//Update "lasts" so we can use them when we write
		$last_id = $record->id;
		$last_user_id = $actual_user->ID;
		$last_team_id = $team->value('id');
		$last_order_id = $record->order_id;
		

	}
}