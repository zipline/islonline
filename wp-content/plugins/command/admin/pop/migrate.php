<?php

set_time_limit(0);
$general_date_range = "ALL";				//Typical number of days to run processes on

//
//Migration controls
$migrate_users = false;								//Good for updates
$migrate_users_range = $general_date_range;			//ALL for everyone, or number for day count

$migrate_teams = false;								//Good for updates
$migrate_teams_range = $general_date_range;			//ALL for everyone, or number for day count

$migrate_coupons_caps = false;						//Good for updates
$migrate_coupons_range = $general_date_range;		//Not required

$migrate_reservations = false;						//Good for updates
$migrate_reservations_range = $general_date_range;	//All for everyone, or number for day count (system adds 60)

$migrate_res_data = true;
$migrate_res_link = false;
$migrate_cap_update = false;

//exit;

//
//Current WP setup
$cur_wp_address = '50.28.57.34';
$cur_wp_login = 'islonlin_wp3s';
$cur_wp_pwd = 'w8T(IHOS$.^[';


//
//Current DB setup
$cur_db_address = '50.28.57.34';
$cur_db_login = 'islonlin_app2015';
$cur_db_pwd = 'q52Jz7k0423D1zy';


//
//other variables
global $wpdb;


//
//Necessary Classes
require_once (CC_CLASSES_DIR . 'cart.class.php');
require_once (CC_CLASSES_DIR . 'finance.class.php');
require_once (CC_CLASSES_DIR . 'rsvp.class.php');
require_once (CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . "itinerary.class.php");


//
//Current Connections
$cur_wp = new mysqli($cur_wp_address, $cur_wp_login, $cur_wp_pwd);
$cur_db = new mysqli($cur_db_address, $cur_db_login, $cur_db_pwd);



if($migrate_users)
{
	require_once(CC_ADMIN_DIR . 'pop/migrate/users.php');
	migrate_users($migrate_users_range, $wpdb, $cur_wp, $cur_db);
}


if($migrate_teams)
{
	require_once(CC_ADMIN_DIR . 'pop/migrate/teams.php');
	migrate_teams($migrate_teams_range, $wpdb, $cur_wp, $cur_db);
}


if($migrate_coupons_caps)
{
	require_once(CC_ADMIN_DIR . 'pop/migrate/coupons.php');
	migrate_coupons($migrate_coupons_range, $wpdb, $cur_wp, $cur_db);
}



if($migrate_reservations)
{
	require_once(CC_ADMIN_DIR . 'pop/migrate/reservations.php');
	migrate_reservations($migrate_reservations_range, $wpdb, $cur_wp, $cur_db);
}


if($migrate_res_data)
{
	require_once(CC_ADMIN_DIR . 'pop/migrate/res_data.php');
	migrate_res_data($migrate_reservations_range, $wpdb, $cur_wp, $cur_db);
}


if($migrate_res_link)
{
	require_once(CC_ADMIN_DIR . 'pop/migrate/res_link.php');
	migrate_res_link($migrate_reservations_range, $wpdb, $cur_wp, $cur_db);
}


if($migrate_cap_update)
{
	require_once(CC_ADMIN_DIR . 'pop/migrate/cap_update.php');
	migrate_cap_update($migrate_reservations_range, $wpdb, $cur_wp, $cur_db);
}


$cur_wp->close();
$cur_db->close();
