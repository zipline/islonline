<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "country.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");

$country = new COUNTRY();
$page = new TAB_MANAGE();

//
//Deactivate any Country that does not have a published POST
foreach($country->list_countries(true) as $country_id=>$country_data)
{
	if(empty($country_data['post_id']) || get_post_status($country_data['post_id']) != "publish")
	{
		$country->set_country($country_id);
		$country->save_info('active', 0);
	}
}

//
//Divide the page into two options, Detail and Listing
$country = new COUNTRY();
if(isset($_GET['country_id']) && ($country->is_country($_GET['country_id']) || $_GET['country_id']=='new'))
{
	$country->set_country($_GET['country_id']);

	//
	//Country Detail section
	if($_GET['country_id']=='new')
	{
		$page->SetTabs(array('cinfo' => 'Country Info'));
	}
	else
	{
		$page->SetTabs(array('cinfo' => 'Country Info', 'creg' => 'Regions', 'cstaff' => 'Staff', 'ccont' => 'Public Website Page'));
	}
	$page->SetTabName('cur_tab');
	$page->SetCurrent(isset($_GET[$page->GetTabName()]) ? $_GET[$page->GetTabName()] : 'cinfo');

	//
	//Write in Select field
	?>
	<br>
	<h1>
   	<A href='<?=$page->GET_path(array('country_id','save'))?>'><img height='20' src=site_url() . '/wp-content/uploads/2015/09/left_arrow.png'></A>

	Country: <SELECT name="country" id="country" onChange="javascript:change_country()">
				<?php echo $country->selector($_GET['country_id']) ?>
                <?php if($_GET['country_id']=='new') { echo "<OPTION value='new' selected> --> Add New </OPTION>"; }?>
             </SELECT></H1>
	<br>
	<?php

	$page->ShowTabs(array('save'));

	//
	//Deal with tabs
	switch ($page->GetCurrent())
	{
		case 'cinfo':
			include (CC_ADMIN_DIR . "tabs/country.info.php");
			break;	
		case 'ccont':
			include (CC_ADMIN_DIR . "tabs/country.cont.php");
			break;
		case 'creg':
			include (CC_ADMIN_DIR . "tabs/country.reg.php");
			break;
		case 'cstaff':
			include (CC_ADMIN_DIR . "tabs/country.staff.php");
			break;
		default:
			break;	
	}

	?>
		<script>
        function change_country()
        {
            var url;
            url = '<?=$page->GET_path(array('country_id','save'))?>' + '&country_id=' + document.getElementById('country').value;
            window.location.href = url;
        }
        </script>
	<?php
}
else
{

	$country = new COUNTRY();
	$active_countries = array();
	
	echo "<div class='header-wrapper wrap'><h1>Countries" . str_repeat('&nbsp;',6) . 
			"<a href='" . $page->GET_path(array('country_id','save'),array('country_id'=>'new')) . "' class='page-title-action'>Add New</a></h1></div>";	
	echo "<table class='wp-list-table widefat fixed'>";
	echo "<thead>";
	echo "	<tr>";
	echo "		<th scope='col' class='manage-column'>Code</th>";
	echo "		<th scope='col' class='manage-column'>Country</th>";
	echo "  </tr>";
	echo "</thead>";
	
	$base_path = $page->GET_path(array('country_id','save'));
	foreach($country->list_countries() as $country_id=>$country_data)
	{
		$active_countries[] = $country_id;
		
		echo "<TR>";
		echo "  <TD>" . $country_data['code'] . "</TD>";
		echo "  <TD><a href='" . $base_path . "&country_id=" . $country_id . "'>" . $country_data['title'] . "</a></TD>";
	}
	
	echo "</table>";

	echo "<HR />";
	echo "<BR />";

	$country = new COUNTRY();
	
	echo "<table class='wp-list-table widefat fixed'>";
	echo "<thead>";
	echo "	<tr>";
	echo "		<th scope='col' class='manage-column' colspan=2><B>Inactive Countries</B></th>";
	echo "  </tr>";
	echo "	<tr>";
	echo "		<th scope='col' class='manage-column'>Code</th>";
	echo "		<th scope='col' class='manage-column'>Country</th>";
	echo "  </tr>";
	echo "</thead>";
	
	$base_path = $page->GET_path(array('country_id','save'));

	foreach($country->list_countries(true) as $country_id=>$country_data)
	{
		if(!in_array($country_id, $active_countries))
		{
			echo "<TR>";
			echo "  <TD>" . $country_data['code'] . "</TD>";
			echo "  <TD><a href='" . $base_path . "&country_id=" . $country_id . "'>" . $country_data['title'] . "</a></TD>";
		}
	}
	
	echo "</table>";

}

?>