<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

global $wpdb;
include_once(CC_CLASSES_DIR . 'user.class.php');
$command_user = new USER();
$meta_staff_key = 'cmd_' . str_replace(' ', '_', strtolower($country->get_name())) . '_staff_pos';
$w = 250;


//
//Save changes here
if(isset($_GET['save']) && $_GET['save']=='true')
{
	if(isset($_POST['del_id']) && $_POST['del_id'] > 0 && $_GET['delete'] == 'true')
	{
		delete_user_meta($_POST['del_id'], $meta_staff_key);		
	}
	else
	{
		if(!empty($_POST['staff_pos']) && !empty($_POST['title']))
		{
			add_user_meta($_POST['staff_pos'], $meta_staff_key, $_POST['title']);		
		}
	}
}


//
//Gather Defined Data here
$query = "SELECT um.user_id, um.meta_value, u.display_name
			FROM " . $wpdb->prefix . "usermeta um INNER JOIN
				 " . $wpdb->prefix . "users u on u.ID = um.user_id 
		  WHERE meta_key = '" . $meta_staff_key . "'
		  ORDER BY display_name";
$possibles = $wpdb->get_results($query, ARRAY_A);

if($possibles)
{
	foreach($possibles as $data)
	{
		$current_users[$data['user_id']]['title'] = $data['meta_value'];
		$current_users[$data['user_id']]['name'] = $data['display_name'];
	}
}
else
{
		$current_users[] = array('name' => '', 'title' => '');
}

?>	

<div class="country-info-wrapper">
<form name="data" method="POST" enctype="multipart/form-data" 
      action="<?=$page->GET_path(array('save','delete')) . '&save=true'?>">
	<h1>Staff</h1>
	<table class="form-table">
		<TR><TH style='<?= $w?>'>User</TH><TH style='<?= $w?>'>Title</TH><TH></TH></TR>
        
		<?php
        foreach($current_users as $id => $data)
        {
        	if(!empty($id))
			{
				echo "<TR><TD>" . $data['name'] . "</TD><TD>" . $data['title'] . "</TD>
						  <TD><button type='button' onclick='javascript:delrec(" . $id . ")'>Remove</button></TD></TR>";	
			}
        }
        ?>

		<TR>
			<TD>
                <?php
                $users_with_permissions = $command_user->users_with_adv_permissions();
				foreach($users_with_permissions as $user_id => $keys)
				{
					if(isset($current_users[$user_id]))
					{
						unset($users_with_permissions[$user_id]);
					}
				}

				if($users_with_permissions)
				{
					?>				
                    <SELECT name='staff_pos' id='staff_pos' required>

                    <?php
                    $users_with_permissions = $command_user->users_with_adv_permissions();
                    foreach($users_with_permissions as $user_id => $keys)
                    {
  
						$titles = '';
                        foreach($keys as $id => $code)
                        { 
                            if($id === "name") { } else
                            {
                        		if($code == 'Admin')
								{
									$titles .= (strlen($titles) > 0 ? ", " : "") . 'Admin';
								}
								else
								{
									$titles .= (strlen($titles) > 0 ? ", " : "") . $command_user->cap_code_to_title($code); 
								}
                            }
                        }
                        
                        if(!isset($current_users[$user_id]))
                        {
                            echo "<OPTION VALUE	= $user_id>" . $users_with_permissions[$user_id]['name'] . " " . 
                                        str_repeat('&nbsp;', 10) .
                                        "(" . $titles . ")</OPTION>";
                        }
					}
					?>

                    </SELECT>
				<?php
				}
			?>
			</TD>
			<TD><INPUT TYPE="text" name="title", id="title" required="true"></TD><TD><button type="submit">Add</button></TD>
		</TR>
	</table>
</form>
</div>
<form name="del_form" id="del_form" method="POST" enctype="multipart/form-data" 
      action="<?=$page->GET_path(array('save','delete')) . '&save=true&delete=true'?>">
      
	<input type="hidden" name="del_id" id="del_id" value="0">		      
</form>

<script>
	function delrec(record_id)
	{
		var id = document.getElementById('del_id');
		id.value = record_id;
		document.getElementById("del_form").submit();				
	}
</script>