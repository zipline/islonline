<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//require_once (CC_CLASSES_DIR . "itinerary.class.php");
//require_once (CC_CLASSES_DIR . "program.class.php");
//require_once (CC_CLASSES_DIR . "country.class.php");
//require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "docs.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "user.class.php");

//$page = new TAB_MANAGE();
//$itinerary = new ITINERARY();
//$country = new COUNTRY();
//$program = new PROGRAM();
$docs = new DOCS();
$finance = new FINANCE();
$user = new USER();

//
//Initialize jQuery
wp_enqueue_script('cmd_tl_change', plugins_url() . '/command/js/tl.change.js');
//wp_localize_script('cmd_tl_change', 'svars', $script_vars);

wp_enqueue_script('cmd_vol_selector', plugins_url() . '/command/js/vol_selector.js', array('jquery', 'jquery-ui-autocomplete'), '0.1', true );
wp_localize_script('cmd_vol_selector', 'svars', array('ajax_url' => admin_url('admin-ajax.php')));

$itin_rows = 8;
$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, wp_help',
					   "toolbar2" => '');
			
$text_settings = array("media_buttons" => false,
					   "textarea_rows" => $itin_rows,
					   "teeny" => true,
					   "tinymce" => $tiny_settings,
					   "quicktags" => false);

if(isset($_GET['save']) && $_GET['save']=='true')
{
	//
	//Save all the blurbs
	$team->save_blurb('public', isset($_POST['public']) ? $_POST['public'] : NULL);
}

if(isset($_REQUEST['edit_volunteer_note']) || isset($_REQUEST['volunteer_note']))
{
	update_user_meta($_REQUEST['volunteer_id'],$_REQUEST['team_id'] ."_volunteer_note",$_REQUEST['volunteer_note']);
}

//
//Count Required Docs
$docs->post_id($team->value('post'));
$doc_list = $docs->get_team_list();
$docs_required = count($doc_list);

//
//Gather Team Managers Here
$tl_users = array();
$meta_team_staff_key = 'cmd_team' . $_GET['team_id'] . '_staff_pos';
$query = "SELECT um.user_id, um.meta_value, u.display_name
			FROM " . $wpdb->prefix . "usermeta um INNER JOIN
				 " . $wpdb->prefix . "users u on u.ID = um.user_id 
		  WHERE meta_key = '" . $meta_team_staff_key . "' AND
		        meta_value = 'Team Leader'
		  ORDER BY display_name";
$possibles = $wpdb->get_results($query, ARRAY_A);

foreach($possibles as $data)
{
	$tl_users[$data['user_id']]['title'] = $data['meta_value'];
	$tl_users[$data['user_id']]['name'] = $data['display_name'];
}

?>
<?php //echo var_dump(acf_get_fields(4849));?>
<table class="standard-large-table form-table team-overview striped">
	<thead>
		<TR>
			<TH class="medium">&nbsp;</TH>
	    	<TH class='large'>Name</TH>
	        <TH class="medium">Role</TH>
	        <!-- <TH>ACCOUNT</TH> -->
	        <TH class="small">Profile</TH>
	        <TH class="small">Passport</TH>
	        <TH class="small">Flight</TH>
	        <TH class="small">Docs</TH>
	        <TH>Program Lead</TH>
	        <TH class="medium">Balance</TH>
	        <TH colspan="2">Notes</TH>
	    </TR>
	</thead>
    
    <?php
	foreach($rsvp->list_rsvp_by_team($team->get_team()) as $index=>$data)
	{

		$member_id = $data['user_id'];
		$rsvp->set_rsvp_id($data['id']);
		$rsvp_data = $rsvp->fetch_rsvp();
		
		if(empty($rsvp_data))
		{
			$user_info = get_userdata($member_id);
			if(!empty($user_info))
			{
				$user_name = ($member_id > 0) ? $user_info->first_name . " " . $user_info->last_name : 'Unassigned';
				$user_email = ($member_id > 0) ? $user_info->user_email : '';
				if($member_id > 0)
				{
					$user_name = '<a href="/wp-admin/admin.php?page=cc-volunteers&user_id=' . $member_id . '" target="_blank">' . $user_name . (($user->is_poc($team->value('post'), $user_email)) ? " (POC)" : '') . "</a>";
				}

				echo "<TR>";
				echo "	<TD >" . (($member_id > 0) ? "<a href='/wp-admin/admin.php?page=cc-volunteers&user_id=" . $member_id . "' target='_blank'>" : '') . "<div class='avatar' style='background: url(" . $user->avatar($member_id) . "); background-size: cover;'></div></a></TD>";
				echo "	<TD>" . $user_name . "</TD>";
				echo "	<TD>" . ucfirst($user->user_group($member_id)) . "</TD>";
				echo "	<TD colspan = 8><B>RSVP Missing. Contact Technical Support</B></TD>";
				echo "</TR>";

			}
			else
			{
				echo "<TR>";
				echo "  <TD>&nbsp</TD>";
				echo "	<TD colspan = 2><B>Invalid Member Found: ID: " . $member_id . "</B></TD>";
				echo "	<TD colspan = 8><B>RSVP Missing. Contact Technical Support</B></TD>";
				echo "</TR>";
			}
		}
		else
		{
			
			$post_id = $rsvp_data['post_id'];

			$points = 0;

			//
			//Name Information
			$user_info = get_userdata($member_id);
			if(!empty($user_info))
			{
				$user_name = ($member_id > 0) ? $user_info->first_name . " " . $user_info->last_name : 'Unassigned';
				$user_email = ($member_id > 0) ? $user_info->user_email : '';
				if($member_id > 0)
				{
					$user_name = '<a href="/wp-admin/admin.php?page=cc-volunteers&user_id=' . $member_id . '&rsvp=' . $rsvp_data['id'] . '" target="_blank">' . $user_name . (($user->is_poc($team->value('post'), $user_email)) ? " (POC)" : '') . "</a>";
					
					$wp_link = get_post_meta($rsvp_data['post_id'], 'migrate_special_wp_value', true);
					$db_link = get_user_meta($member_id, 'member_id', true);
				
					if(isset($wp_link) && is_numeric($wp_link))
					{
						$user_name .= '<a href="https://old.islonline.org/wp-admin/post.php?post=' . $wp_link . '&action=edit" target="_blank"> (Woo)</a>';
					}
					if(isset($db_link) && is_numeric($db_link))
					{
						$user_name .= '<a href="http://database.islonline.org/admin/members/view/' . $db_link . '" target="_blank"> (DB)</a>';
					}
					$points++;
				}


				//
				//Avatar information
				//$avatar = get_user_meta($member_id,'local_avatar',true);
				//$img_url = "/wp-content/uploads/2016/03/avatar.png";
				//if($avatar)
				//{
				//	$avatar_url = get_post_field('guid',$avatar);
				//	if(!empty($avatar_url))
				//	{
				//		$points++;
				//		$img_url = $avatar_url;
				//	}
				//}

				//
				//Gender
				$gender = get_field('gender', 'user_' . $member_id);
				if(!empty($gender))
				{
					$points++;
				}


				//
				//Profile
				$required = 0;
				$found = 0;
				$profile = '';
				foreach(acf_get_fields(4849) as $index => $data)
				{		
					if(substr($data['name'],0,10) != 'Emergency')
					{
						if($data['required'] == true)
						{
							$required++;
						}

						if(!empty(get_field($data['name'], $post_id)))
						{
							$found++;
						}
					}
				}
				if($required == $found)
				{
					$profile = '<span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span>';
					$points++;		
				}
				else
				{
					$profile = '<span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span>';
				}


				//
				//Passport
					
				$passport = '';
				if($rsvp->has_passport($rsvp_data['id']))
				{
					$passport = '<span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span>';
					$points++;
				}
				else
				{
					$passport = '<span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span>';
				}

				//
				//Flight
				$flight = '';
				if($rsvp->has_travel($rsvp_data['id']))
				{
					$flight = '<span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span>';
					$points++;
				}
				else
				{
					$flight = '<span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span>';
				}


				//
				//Emergency
				$required = 0;
				$found = 0;
				$emergency = '';
				foreach(acf_get_fields(21041) as $index => $data)
				{		
					if(substr($data['name'],0,9) == 'Emergency')
					{
						if($data['required'] == true)
						{
							$required++;
						}

						if(!empty(get_field($data['name'], $post_id)))
						{
							$found++;
						}
					}
				}
				if($required == $found)
				{
					$emergency = '<span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span>';
					$points++;		
				}


				//
				//Documents
				$read = $member_id ? count($docs->fetch_read($member_id)) : 0;
//				print_r($docs->fetch_read($member_id));die;
				$doc_cnt = $read . "/" . $docs_required;
				if($read >= $docs_required)
				{
					$doc_cnt = '<span class="completed"><i class="fa fa-check" aria-hidden="true"></i></span>';
					$points++;
				}
				else
				{
					$doc_cnt = '<span class="incomplete"><i class="fa fa-times" aria-hidden="true"></i></span>';
				}

				//
				//Team Leader
				$number = get_post_meta($post_id, '_cmd_team_leader', true);
				if(count($tl_users) == 0)
				{
					$tl = 'None';
				}
				elseif (count($tl_users) == 1)
				{
					$tl = current($tl_users)['name'];
				}
				else
				{
					$tl = '<SELECT name="team_lead_' . $post_id . '" id="team_lead_' . $post_id . '" class="team_lead">';
					$tl .= '<OPTION value="0"> -- Choose --</OPTION>';	
					foreach($tl_users as $tl_id => $tl_data)
					{
						$tl .= '<OPTION value="' . $tl_id . '"' . ($number == $tl_id ? ' SELECTED' : '') . '>' . $tl_data['name'] . '</OPTION>';	
					}
					$tl .= '</SELECT>';			
				}

				//
				//Payments
				unset($all_items);
				//$cost = $team->trip_cost();
				$item = $finance->fetch_item($rsvp_data['order_item_id']);
				$cost = $item['price'] * $item['quantity'];
				$add_ons = $finance->add_ons($item['id']);
				$all_items[] = $rsvp_data['order_item_id'];

				foreach($add_ons as $index => $itm)
				{
					$all_items[] = $itm['id'];
					$cost += (float)($itm['price'] * $itm['quantity']);
				}		

				$pays = 0;
				foreach($finance->payments($all_items) as $payments)
				{
					$pays += (float)$payments['amount'];	
				}

				$payment = '$' . (round($cost,2) - round($pays,2));
				if($payment ==0)
				{
					$points ++;
				}

				$volunteer_note = get_user_meta($member_id, $team->get_team() ."_volunteer_note", true);
				$volunteer_note = !empty($volunteer_note)? $volunteer_note : '<span class="placeholder-text">Enter notes here</span>';
				$volunteer_note = '<span class="volunteer_note note_field" style="cursor: pointer;" onclick="editVolunteerNote('. $member_id .');">'. $volunteer_note .'</span>';

				$url ='/wp-admin/admin.php?page=cc-pops&display=finance&team=' . $team->get_team() . "&member=" . $member_id . "&rsvp=" . $rsvp->get_rsvp_id();

				if($user->is_poc($team->value('post'), $user_email))
				{
					$inv_url = '&nbsp;&nbsp; <A href="/wp-admin/admin.php?page=cc-pops&display=poc_invoice&team=' . $team->get_team() . '&member=' . $member_id . '&rsvp=' . $rsvp->get_rsvp_id() . '" target="_blank">(I)</A>';
					
				}
				else
				{
					$inv_url = '';
				}
				$inv_url .= '&nbsp;&nbsp; <a href="/payment/?team_id=' . $team->get_team() . '&member_id=' . $member_id . '&rsvp=' . $rsvp->get_rsvp_id() . '&max=0" target="_blank">(Pay)</A>';

				//
				//2018-03-11 DJT Changed to new User "Role" label
				$new_user_role = get_user_meta($member_id, 'v_role', true);
				
				//
				//2018-02-21 DJT added switch to breakout code to title
				switch($new_user_role)
				{
					case 1:
						$new_role_title = "Faculty";
						break;
					case 2 : 
						$new_role_title = "Undergrad";
						break;
					case 3 : 
						$new_role_title = "Graduate Student";
						break;
					case 4 : 
						$new_role_title = "Medical Professional";
						break;
					case 5 : 
						$new_role_title = "High School Student";
						break;
					case 6 : 
						$new_role_title = "School Staff";
						break;
					case 99 : 
						$new_role_title = "Other";
						break;
					default : 
						$new_role_title = "Undefined";
						break;
				}
				
				echo "<TR>";
				echo "	<TD >" . (($member_id > 0) ? "<a href='/wp-admin/admin.php?page=cc-volunteers&user_id=" . $member_id . "' target='_blank'>" : '') . "<div class='avatar' style='background: url(" . $user->avatar($member_id) . "); background-size: cover;'></div></a></TD>";
				echo "	<TD>" . $user_name . "</TD>";
				
				//
				//2018-03-11 DJT Changed to new User "Role" label
				//echo "	<TD>" . ucfirst($user->user_group($member_id)) . "</TD>";
				//2018-03-21 DJT Changed from user role to user title due ot breakout above.
				//echo "	<TD>" . (empty($new_user_role) ? "Undefined" : $new_user_role) . "</TD>";
				echo "	<TD>" . $new_role_title . "</TD>";
				// echo "	<TD>ACCOUNT</TD>";
				echo "	<TD>" . $profile . "</TD>";
				echo "	<TD>" . $passport . "</TD>";
				echo "	<TD>" . $flight . "</TD>";
				echo "	<TD>" . $doc_cnt . "</TD>";
				echo "	<TD>" . $tl . "</TD>";
				echo "	<TD><A href='" . $url . "' target='_blank'>" . $payment . "</A>" . $inv_url . "</TD>";
				echo "	<TD colspan='2' id=\"volunteer_note_". $member_id ."\">" . $volunteer_note . "</TD>";
				echo "</TR>";

			}
			else
			{
				if($member_id == 0)
				{
					//
					//Payments
					unset($all_items);
					//$cost = $team->trip_cost();
					$item = $finance->fetch_item($rsvp_data['order_item_id']);
					$cost = $item['price'] * $item['quantity'];
					$add_ons = $finance->add_ons($item['id']);
					$all_items[] = $rsvp_data['order_item_id'];

					foreach($add_ons as $index => $itm)
					{
						$all_items[] = $itm['id'];
						$cost += ($itm['price'] * $itm['quantity']);
					}		

					$pays = 0;
					foreach($finance->payments($all_items) as $payments)
					{
						$pays += $payments['amount'];	
					}

					$payment = '$' . ($cost - $pays);
					if($payment ==0)
					{
						$points ++;
					}

					$volunteer_note = get_user_meta($member_id, $team->get_team() ."_volunteer_note", true);
					$volunteer_note = !empty($volunteer_note)? $volunteer_note : '<span class="placeholder-text">Enter notes here</span>';
					$volunteer_note = '<span class="volunteer_note note_field" style="cursor: pointer;" onclick="editVolunteerNote('. $member_id .');">'. $volunteer_note .'</span>';

					$url ='/wp-admin/admin.php?page=cc-pops&display=finance&team=' . $team->get_team() . "&member=" . $member_id . "&rsvp=" . $rsvp->get_rsvp_id();

					//if($user->is_poc($team->value('post'), $user_email))
					//{
					//	$inv_url = '&nbsp;&nbsp; <A href="/wp-admin/admin.php?page=cc-pops&display=poc_invoice&team=' . $team->get_team() . '&member=' . $member_id . '&rsvp=' . $rsvp->get_rsvp_id() . '" target="_blank">(I)</A>';
					//}
					//else
					//{
						$inv_url = '';
					//}

					$purchaser = get_user_by('ID', $rsvp_data['purchaser']);
					echo "<TR>";
					echo "  <TD>&nbsp</TD>";
					echo "	<TD colspan = 2>Unassigned Reservation</TD>";
					echo "  <TD colspan = 5>Purchased by: <a href='/wp-admin/admin.php?page=cc-volunteers&user_id=" . $purchaser->ID. "' target='_blank'>" . $purchaser->display_name . "</a></TD>";
					echo "	<TD colspan=3><A href='" . $url . "' target='_blank'>" . $payment . "</A>" . $inv_url . "</TD>";
					echo "</TR>";
				}
				else
				{
					echo "<TR>";
					echo "  <TD>&nbsp</TD>";
					echo "	<TD colspan = 2>Invalid Member Found: ID: " . $member_id . "</TD>";
					echo "  <TD colspan = 8>Contact Technical Support</TD>";
					echo "</TR>";
				}
			}
		}
	}
	?>
</table>

<div id="add_container" name="add_container">

	<input type="button" name="show_hide" id="show_hide" class="button button-primary" value="Add Volunteer" onclick="javascript:open_close_add_vol()">
	<div name="add_vol" id="add_vol" style="display: none">

		<input type="text" name="vadd_user_name" id="vadd_user_name" size=50 placeholder="Enter Volunteer Name">
		<input type="text" name="vadd_user_email" id="vadd_user_email" disabled=true value=''>
		<input type="text" name="vadd_user_id" id="vadd_user_id" style="display: none" value=''>
		<input type="text" name="vadd_team_id" id="vadd_team_id" style="display: none" value='<?php echo $_GET['team_id']?>'>

		<input type="button" name="submit_vol" id="submit_vol" class="button button-primary" disabled="true" value="Add Volunteer to Team">

	</div>
</div>

<?php

//
//2018-03-21 DJT Added new container to add empty reservations for CFL
if($support->type_value($team->value('type'), 'rsvp') == 1)
{
	wp_enqueue_script('cmd_add_rsvp', plugins_url() . '/command/js/vol_rsvp_add.js', array('jquery'), NULL, true );
	wp_localize_script('cmd_add_rsvp', 'svars', array('ajax_url' => admin_url('admin-ajax.php')));

	?>
	<div id="add_rsvp_container" name="add_rsvp_container">

		<input type="button" name="show_hide_rsvp" id="show_hide_rsvp" class="button button-primary" value="Add Reservations" onclick="javascript:open_close_add_rsvp()">
		<div name="add_vol_rsvp" id="add_vol_rsvp" style="display: none">

			<input type="text" name="vadd_user_count" id="vadd_user_count" size=50 placeholder="Enter Number of RSVPs">
			<input type="text" name="vadd_team_rsvp_id" id="vadd_team_rsvp_id" style="display: none" value='<?php echo $_GET['team_id']?>'>

			<input type="button" name="submit_add_rsvp" id="submit_add_rsvp" class="button button-primary" disabled="true" value="Add Reservations">
		</div>
	</div>

	<script>

		function open_close_add_rsvp()
		{
			var show_hide = document.getElementById('add_vol_rsvp');

			if(show_hide.style.display == "none")
			{
				show_hide.style.display	= "block";	
			}
			else
			{
				show_hide.style.display = "none";	
			}
		}
	</script>
	<?php
}
?>
	<script>

		function open_close_add_vol()
		{
			var show_hide = document.getElementById('add_vol');

			if(show_hide.style.display == "none")
			{
				show_hide.style.display	= "block";	
			}
			else
			{
				show_hide.style.display = "none";	
			}
		}
		
		
		function editVolunteerNote(volunteer_id)
		{
			var form = jQuery('<form class="edit_note_form edit_volunteer_note_form" method="POST" action="<?php echo $page->GET_path(); ?>">');
			var container = jQuery("#volunteer_note_"+ volunteer_id);
			var note_text = container.children('.volunteer_note').html();

			if (note_text == '<span class="placeholder-text">Enter notes here</span>') {
					note_text = 'Enter notes here';
				}
			
			form.append('<input type="hidden" name="volunteer_id" value="'+volunteer_id+'" />');
			form.append('<input type="text" class="note_field" placeholder="Enter notes here" name="volunteer_note" value="'+note_text+'" />');
			form.append('<input type="submit" class="save-note button button-primary" name="edit_volunteer_note" value="Save Note" /> ');
			form.append(' <button type="button" class="cancel-save-note button button-primary" onclick="resetVolunteerNote('+ volunteer_id +',\''+ note_text.replace(/' /g, "\\'") +'\');">X</button>');
			container.html(form);
			container.find('input.note_field').select();
		}

		function resetVolunteerNote(volunteer_id,note_text)
		{
			var container = jQuery("#volunteer_note_"+ volunteer_id);
			var note_text = note_text;

				if (note_text == '' || note_text == 'Enter notes here') {
					note_text = '<span class="placeholder-text">Enter notes here</span>';
				} else {
					note_text = note_text;
				}
				
			container.html('<span class="volunteer_note note_field" style="cursor: pointer;" onclick="editVolunteerNote('+ volunteer_id +');">'+ note_text +'</span>');
		}

		jQuery(document).on("submit",".edit_volunteer_note_form",function(event)
		{
			event.preventDefault();
			var v_id = jQuery("input[name='volunteer_id']",this).val();
			var v_note = jQuery("input[name='volunteer_note']",this).val();

			jQuery.post("<?php echo $page->GET_path(); ?>",jQuery(this).serialize()).done(function()
			{
				resetVolunteerNote(v_id,v_note);
			});
		});
	</script>

<!-- <form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<table class="form-table">
	<TR>
		<TD><label for="public_notice"><strong>Public Team Notice</strong></label><BR>
			<?php wp_editor($team->get_blurb('public'), "public", $text_settings)?>
	</TR>
</table>

<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>
</form> -->
