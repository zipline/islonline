<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

if(isset($_FILES) && isset($_FILES['image']))
{
	
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}
	

	$uploadedfile = $_FILES['image'];
	
	$upload_overrides = array( 'test_form' => false );
	
	$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
	
	if ( $movefile && !isset( $movefile['error'] ) ) {
		$team->save_image($movefile['url']);
		$team_id = $team->get_team();
		$team = new TEAM();
		$team->set_team($team_id);

		//var_dump( $movefile);
	} else {
		/**
		 * Error generated by _wp_handle_upload()
		 * @see _wp_handle_upload() in wp-admin/includes/file.php
		 */
		echo $movefile['error'];
	}
}
?>

<div name="pimg" id="pimg">

<form name="picupload" id="picupload" method="POST" enctype = "multipart/form-data" action="<?php echo $page->GET_path(array('save'));?>">
<table class="form-table">
	<TR>
    	<TD style="verticle-align: top">Image --></TD>
        <?php
		$image = $team->get_image();
		
		if (isset($image) && strlen($image)>0)
		{
			echo "<td rowspan='2'><img src='" . $team->get_image() . "' height=200 /></td>";	
		}
		?>
    </TR>
    <TR>    
        <TD style="verticle-align: top"><input name="image" id="image" type="file" title=" " style="color: transparent"/></TD>
    </TR>
</table>
</form>


<script>
	jQuery("#image").on('change', function()
		{
 		   	jQuery("form#picupload").submit();
		}
	)
</script>
