<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');
$error = array();
$err = 0;
$w = 'width:125px';
$sw = 'width:95px';
$xsw = 'width:45px';

wp_enqueue_script('jquery-ui-datepicker');

wp_enqueue_media();
wp_enqueue_script('cmd_media_uploader', plugins_url() . '/command/js/media.uploader.js');

$script_vars = array('style' => $w,
					 'short_style' => $sw,
				   	 'country_select' => $country->selector(0, true),
				 	 'program_select' => $program->selector(0, true)
				 	);
wp_enqueue_script('cmd_schedule_adder', plugins_url() . '/command/js/schedule.adder.js', array(), '0.4');
wp_localize_script('cmd_schedule_adder', 'svars', $script_vars);

wp_enqueue_script('cmd_poc_adder', plugins_url() . '/command/js/poc.adder.js');


$itin_rows = 3;
$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, wp_help',
					   "toolbar2" => '');
$inty_settings = array();
			
$text_settings = array("media_buttons" => false,
					   "textarea_rows" => $itin_rows,
					   "teeny" => true,
					   "tinymce" => array(),
					   "quicktags" => false);


if(isset($_POST) && isset($_POST['delete']) && $_POST['delete']=='Delete Image')
{
	if(isset($_POST['image_id']))
	{
		delete_post_meta($team->value('post'), '_thumbnail_id');
	}
}


/** Save a new team */
if ( isset( $_POST['submit'] ) && $_POST['submit'] == 'Save Changes' )
{
	if($_GET['team_id']=='new')
	{
		//
		//Check schedule, make sure we have at least one item
		foreach($_POST as $id=>$value)
		{
			if(substr($id,0,8) == 'country_')
			{
				$actual_id = substr($id,8);
				$write_id = ($actual_id > 999999 ? 0 : $actual_id);
				$ctry = $_POST['country_' . $actual_id];	
				$regn = ((isset($_POST['region_' . $actual_id])) ? $_POST['region_' . $actual_id] : 0);
				$cost = 0;	
				
				if(!isset($_POST['program_' . $actual_id]) || $_POST['program_' . $actual_id] <1)
				{
					$error[] = "Must have a program associated with Schedule.";
				}
				if(!isset($_POST['start_' . $actual_id]) || empty($_POST['start_' . $actual_id]))
				{	
					$error[] = "Start Date must be set for Schedule";
				}
				if(!isset($_POST['end_' . $actual_id]) || empty($_POST['end_' . $actual_id]))
				{
					$error[] = "End Date must be set for Schedule";
				}

				break;
			}
		}


		//
		//hash and type are required, default if we don't have them
		$type = ((!empty($_POST['type'])) ? $_POST['type'] : 1);

		if(!empty($_POST['hash']))
		{
			$hash = $_POST['hash'];
			$codehash = false;

			//
			//Generate some error messages if we don't have a unique hash
			if(!$team->unique_code($hash))
			{
				$error[] = "Hash Tag already exists, please choose another";	
			}
		}
		else
		{
			$hash = rand(1000000000, 1000000000000);
			$codehash = true;
		}
		
		if(isset($_POST['min']) && isset($_POST['max']))
		{
			if($_POST['max'] < $_POST['min'])
			{
				$error[] = "Maximum volunteers must be greater than or equal to minimum volunteers.";
			}
		}

		//
		//No errors, so check other data elements, and write them in if good
		if(empty($error))
		{
			$team->build(array('hash' => $hash,
							   'type' => $type,
							   'status' => 7,
							   'arrival_city' => $_POST['arrival_city'],
							   'arrival_country' => $_POST['arrival_country'],
							   'departure_city' => $_POST['departure_city'],
							   'departure_country' => $_POST['departure_country'],
							   'min' => $_POST['min'],
							   'max' => $_POST['max'],
							   'cost' => $_POST['cost'],
							   'team_chat' => (isset($_POST['team_chat']) ? 1 : 0),
							   'appfee' => ((!empty($_POST['appfee'])) ? $_POST['appfee'] : 0),
							   'deposit' => ((!empty($_POST['deposit'])) ? $_POST['deposit'] : 0),
							   'messaging' => ((!empty($_POST['messaging'])) ? $_POST['messaging'] : 0)
							));

			$team_id = $team->get_team();
			do_action('cmd_team_create', $team_id, $hash);
			
			$new_path = $page->GET_path(array('team_id','save'),array('team_id'=>$team_id));

			$team->save_blurb('teaser', isset($_POST['teaser']) ? $_POST['teaser'] : NULL);

			//
			//Insert Schedule
			foreach($_POST as $id=>$value)
			{
				if(substr($id,0,8) == 'country_')
				{
					$actual_id = substr($id,8);
					$write_id = ($actual_id > 999999 ? 0 : $actual_id);
					$cntry = $_POST['country_' . $actual_id];	
					$regn = ((isset($_POST['region_' . $actual_id])) ? $_POST['region_' . $actual_id] : 0);
					$prgm = $_POST['program_' . $actual_id];	
					$cost = 0;	
					$start = $_POST['start_' . $actual_id];	
					$end = $_POST['end_' . $actual_id];	

					$team->save_segment($write_id, $cntry, $regn, $prgm, $cost, $start, $end);
				}
			}

			if($codehash)
			{
				$team->update('hash', "#" . $team->value('code'));
			}
			do_action('cmd_team_hash_change', $team_id, $hash);
			
			$cur_mail = array();
			if(!empty($_POST['email']))
			{
				$em = $_POST['email'];
				$fn = $_POST['fn'];
				$ln = $_POST['ln'];

				foreach($em as $mail_data)
				{
					$cur_mail[$mail_data] = array('first_name' => current($fn), 'last_name' => current($ln));
					next($fn);
					next($ln);
				}
			}
			$user->set_poc($team->value('post'), $cur_mail);


			if(isset($_POST['image_id']))
			{
				update_post_meta($team->value('post'), '_thumbnail_id', $_POST['image_id']);
			}

			/** process notes */
			if(isset($_POST['public_team_note']))
			{
				update_post_meta($team->value('post'), '_public_team_note', $_POST['public_team_note']);
			}

			if(isset($_POST['chart_note']))
			{
				update_post_meta($team->value('post'), '_chart_note', $_POST['chart_note']);
			}

			//
			//Set all team docs on here
			$sel_docs = array();
			$docs->post_id($team->value('post'));
			$docs->set_countries($team->schedule_countries());
			$docs->set_programs($team->schedule_programs());
			$docs->fetch();

			foreach($docs->missing_docs() as $doc_id => $doc_title)
			{
				$sel_docs[] = $doc_id;
			}
			$docs->set_team_list($sel_docs);
			
			//
			//Add MPA
			//update_post_meta($team->value('post'), "_cmd_add_ons", array(35667 => 100.00)); 
			
			//End good data write and so we can move on and reload page with actual team id
			echo "<script>";
			echo "  window.location = '$new_path';";
			echo "</script>";

		}
		else
		{
			//Process Errors
			$err = 1;
		}
	}
	else
	{
		//
		//Fix team_chat variable since it being "off" means it disappears
		if(empty($_POST['team_chat']))
		{
			$_POST['team_chat'] = 0;
		}
		
		//
		//Fix team_chat variable since it being "off" means it disappears
		if(empty($_POST['messaging']))
		{
			$_POST['messaging'] = 0;
		}

		//
		//Fix no_finance variable since it being "off" means it disappears
		if(empty($_POST['no_finances']))
		{
			$_POST['no_finances'] = 0;
		}

		//printX($_POST);

		$team->save_blurb('teaser', isset($_POST['teaser']) ? $_POST['teaser'] : NULL);

		$save_fields = array ('hash', 'type', 'status', 'arrival_city', 'arrival_country', 'departure_city', 'departure_country', 'min', 'max', 'cost', 'appfee', 'deposit', 'team_chat', 'messaging', 'no_finances');
		
		foreach($save_fields as $field_name)
		{
			if(isset($_POST[$field_name]) && $_POST[$field_name] != $team->value($field_name))
			{
				if($field_name == 'hash')
				{
					if (strlen($_POST[$field_name]) <= 3 && strlen($_POST[$field_name]) > 0) {
						$error[] = 'Invalid hash.';
					} else {
						// Validate unique hash
						$hash = $_POST[$field_name];
						$valid = $team->unique_hash($hash);
						if($valid == false)
						{
							$error[] = "Hash Tag already exists, please choose another";
						}
					}
				}
				
				if(empty($error))
				{
					$team->update($field_name, $_POST[$field_name]);
				}
			}
		}

		if(empty($error))
		{
			do_action('cmd_team_save', $_GET['team_id'], $_POST['hash']);
			
			//
			//Insert Schedule
			$segs[] = array();
			foreach($_POST as $id=>$value)
			{
				if(substr($id,0,8) == 'country_')
				{
					$actual_id = substr($id,8);
					$write_id = ($actual_id > 999999 ? 0 : $actual_id);
					$cntry = $_POST['country_' . $actual_id];	
					$regn = ((isset($_POST['region_' . $actual_id])) ? $_POST['region_' . $actual_id] : 0);	
					$prgm = $_POST['program_' . $actual_id];	
					$cost = 0;	
					$start = $_POST['start_' . $actual_id];	
					$end = $_POST['end_' . $actual_id];	

					if($cntry > 0 && $prgm > 0 && strlen($start)>=8 && strlen($end)>=8)
					{
						$segs[] = $team->save_segment($write_id, $cntry, $regn, $prgm, $cost, $start, $end);
					}
				}
			}
			unset($segs[0]);
			
			//
			//Remove old segments
			$current = $team->get_schedule();
			if(!empty($current) && count($segs)>0)
			{
				error_log(print_r($current,true));
				foreach($current as $segment_id => $segment_data)
				{
					if(!in_array($segment_id, $segs))
					{
						$team->delete_segment($segment_id);
					}
				}
			}

			$cur_mail = array();
			if(!empty($_POST['email']))
			{
				$em = $_POST['email'];
				$fn = $_POST['fn'];
				$ln = $_POST['ln'];

				foreach($em as $mail_data)
				{
					$cur_mail[$mail_data] = array('first_name' => current($fn), 'last_name' => current($ln));
					next($fn);
					next($ln);
				}
			}
			$user->set_poc($team->value('post'), $cur_mail);

			if(isset($_POST['image_id']))
			{
				update_post_meta($team->value('post'), '_thumbnail_id', $_POST['image_id']);
			}

			//
			//Reload the Team data now that new data is saved
			$team = new TEAM();
			$team->set_team($_GET['team_id']);	
			$country = NEW COUNTRY();
			$program = NEW PROGRAM();	
		
		}
		else
		{
			//Process Errors
			$err = 1;
		}
	}
}

//
//Process errors for page or gather real data...
if(!empty($error) && $err==1)
{
	$errors = implode("<BR>",$error);
	$code = '';	
	$hash = ((isset($_POST['hash'])) ? $_POST['hash'] : '');		
	$type = ((isset($_POST['type'])) ? $_POST['type'] : 0);
	$status = ((isset($_POST['status'])) ? $_POST['status'] : 7);
	
	$arrival_city = ((isset($_POST['arrival_city'])) ? $_POST['arrival_city'] : '');
	$arrival_country = ((isset($_POST['arrival_country'])) ? $_POST['arrival_country'] : '');
	$departure_city = ((isset($_POST['departure_city'])) ? $_POST['departure_city'] : '');
	$departure_country = ((isset($_POST['departure_country'])) ? $_POST['departure_country'] : '');
	$min = ((isset($_POST['min'])) ? $_POST['min'] : '');
	$max = ((isset($_POST['max'])) ? $_POST['max'] : '');
	$cost = ((isset($_POST['cost'])) ? $_POST['cost'] : '');
	$appfee = ((isset($_POST['appfee'])) ? $_POST['appfee'] : '85');
	$deposit = ((isset($_POST['deposit'])) ? $_POST['deposit'] : '200');
	$teaser = ((isset($_POST['teaser'])) ? $_POST['teaser'] : '');
	$team_chat = isset( $_POST['team_chat'] ) ? 'checked' : '';
	$messaging = isset( $_POST['messaging'] ) ? 'checked' : '';
	$no_finances = isset( $_POST['no_finances'] ) ? 'checked' : '';

	//
	//Schedule

	$sched = array();
	foreach($_POST as $id=>$value)
	{
		if(substr($id,0,8) == 'country_')
		{
			$actual_id = substr($id,8);
			$write_id = ($actual_id > 999999 ? 0 : $actual_id);
			$cntry = $_POST['country_' . $actual_id];
			$regn = ((isset($_POST['region_' . $actual_id])) ? $_POST['region_' . $actual_id] : 0);
			$prgm = $_POST['program_' . $actual_id];
			$start = $_POST['start_' . $actual_id];
			$end = $_POST['end_' . $actual_id];
			$days = $_POST['days_' . $actual_id];
			$sched[$actual_id] = array(
					'country'    => $cntry,
					'region'     => $regn,
					'program'    => $prgm,
					'start'      => $start,
					'end'        => $end,
					'duration'   => $days,
				);
		}
	}

	//
	//Image
	$image_id = ((isset($_POST['image_id'])) ? $_POST['image_id'] : '');
	$image_url = get_post_field ('guid', $image_id, 'raw') ;

}
else
{
	$code = $team->value('code');
	$hash = $team->value('hash');
	$type = $team->value('type');
	$status = $team->value('status');
	$team_name = ($hash ? $code . " / " . $hash : "New Program");

	$arrival_city = $team->value('arrival_city');
	$arrival_country = $team->value('arrival_country');
	$departure_city = $team->value('departure_city');
	$departure_country = $team->value('departure_country');
	$min = $team->value('min');
	$max = $team->value('max');
	$cost = $team->value('cost');
	$appfee = $team->value('appfee');
	$deposit = $team->value('deposit');	
	$team_chat = ($team->value('team_chat') ? 'checked = true' : '');
	$teaser = $team->get_blurb('teaser');
	$messaging = ($team->value('messaging') ? 'checked = true' : '');
	$no_finances = ($team->value('no_finances') ? 'checked = true' : '');
	
	$points_of_contact = $team->get_poc();
		
	//
	//Schedule
	if($team->get_schedule() == "Unknown")
	{
		$team->blank_schedule(true);
	}
	$current = $team->get_schedule();

	foreach($current as $segment_id => $segment_data)
	{
		$sched[$segment_id]['country'] = $segment_data['country'];
		$sched[$segment_id]['region'] = $segment_data['region'];
		$sched[$segment_id]['program'] = $segment_data['program'];
		$sched[$segment_id]['start'] = $segment_data['start_date'];
		$sched[$segment_id]['end'] = $segment_data['end_date'];
		$sched[$segment_id]['duration'] = $segment_data['duration'];
	}
	
	//
	//Image
	$image_id = get_post_meta($team->value('post'), '_thumbnail_id', true);
	$image_url = get_post_field ('guid', $image_id, 'raw') ;

	echo "<script>";
	echo "   document.getElementById('team_name_main').innerHTML='" . $team_name . "'";
	echo "</script>";	
}


?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save'))?>">


<table class="form-table team-info">
	<?php
	if(!empty($error))
	{
		echo "<TR>";
		foreach($error as $message)
		{
			echo "<TD colspan=2>&nbsp;&nbsp;&nbsp;<span style='color:red'>$message</span><BR></TD>";		
		}
		echo "<HR>";
		echo "</TR>";	
	}
	?>

	<TR>
		<TH scope="row"><label for="code">Program Code (automatic)</label></TH>
		<TD><input name="code" id="code" class="regular-text" type="text" value='<?php _e($code)?>' size='50' maxlength='50' disabled='true'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="hash">Hash Tag</label></TH>
		<TD><input name="hash" id="hash" class="regular-text" type="text" value='<?php _e($hash)?>' size='20' maxlength='20' placeholder='Leave blank to default to Program Code', pattern="^$|#{1}[\w!@&:._-]{3,49}" title="Hashtag must start with # and be followed by 3-49 letters and/or numbers and/or the following special characters !@&:._-" /></TD>
	</TR>

	<TR>
   		<TH scope="row"><label for="hash">Schedule</label></TH>
		<TD>
		<table id="schedule_table">
            <TR><TH style='<?= $w?>'>Country</TH><TH>Region</TH><TH>Program</TH><TH>Start Date</TH><TH>End Date</TH><TH>Days</TH></TR>
    
            <?php
	            foreach ( $sched as $segment_id => $segment_data ) {
		            if ( ! empty( $segment_data['country'] ) ) {
			            $segment_save = true;
			            $country->set_country( $segment_data['country'] );
		            }

		            ?>
		            <TR>
			            <TD>
				            <input type='hidden' class='cnthold' name='key_<?php _e( $segment_id ) ?>' id='key_<?php _e( $segment_id ) ?>' value='<?php _e( $segment_id ) ?>'>
				            <SELECT style='<?= $w ?>' name="country_<?php _e( $segment_id ) ?>" id="country_<?php _e( $segment_id ) ?>" class="country">
					            <?php echo $country->selector( $segment_data['country'], true ) ?>
				            </SELECT>
			            </TD>
			            <TD>
				            <SELECT style='<?= $w ?>' name="region_<?php _e( $segment_id ) ?>" id="region_<?php _e( $segment_id ) ?>" class="region">
					            <?php if ( $country->get_country() ) {
						            echo $country->region_selector( $segment_data['region'] );
					            } ?>
				            </SELECT>
			            </TD>
			            <TD>
				            <SELECT style='<?= $w ?>' name="program_<?php _e( $segment_id ) ?>" id="program_<?php _e( $segment_id ) ?>" class="program">
					            <?php echo $program->selector( $segment_data['program'], true ) ?>
				            </SELECT>
			            </TD>
			            <TD>
				            <input data-segment-id="<?= $segment_id; ?>" style='<?= $sw ?>' name="start_<?php _e( $segment_id ) ?>" id="start_<?php _e( $segment_id ) ?>"
					            class="regular-text datepicker startDate" type="text"
					            value='<?= isset( $segment_data['start'] ) ? $segment_data['start'] : '' ?>'>
			            </TD>
			            <TD>
				            <input data-segment-id="<?= $segment_id; ?>" style='<?= $sw ?>' name="end_<?php _e( $segment_id ) ?>" id="end_<?php _e( $segment_id ) ?>"
					            class="regular-text datepicker endDate" type="text"
					            value='<?= isset( $segment_data['end'] ) ? $segment_data['end'] : '' ?>'>
			            </TD>
			            <TD>
				            <input data-segment-id="<?= $segment_id; ?>" style='<?= $xsw ?>' name="days_<?php _e( $segment_id ) ?>" id="days_<?php _e( $segment_id ) ?>"
					            class="regular-text" type="text"
					            value='<?= isset( $segment_data['duration'] ) ? $segment_data['duration'] : '' ?>'>
			            </TD>
			            <TD>
				            <input type="button" name="delete_<?php _e( $segment_id ) ?>" id="delete_<?php _e( $segment_id ) ?>"
					            class="button delete delete_sched secondary" value="Delete">
				            <input type="button" name="add_<?php _e( $segment_id ) ?>" id="add_<?php _e( $segment_id ) ?>"
					            class="button add_sched" value="Add">
			            </TD>
		            </TR>

		            <?php
	            }
            ?>
        </table>
	
	</TD></TR>    

	<TR>
		<TH><label for="type">Program Type</label></TH>
		<TD><SELECT name='type' id='type'><?php echo $support->type_selector($type, true) ?></SELECT></TD>
	</TR>
    <TR>    
		<TH><label for="status">Status</label></TH>
		<TD><SELECT name="status" id="status" <?php echo ($_GET['team_id']=='new' ? " disabled = 'true' " : "")?>>
			<?php echo $support->status_selector(
				($_GET['team_id']=='new' ? 7 : $status),false)
			 ?></SELECT>
		</TD>
	</TR>
	<TR>
		<TH scope="row"><label for="cost">Cost</label></TH>
		<TD><input name="cost" id="cost" class="regular-text" type="text" value='<?php echo $cost?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="appfee">Application Fee</label></TH>
		<TD><input name="appfee" id="appfee" class="regular-text" type="text" value='<?php echo $appfee?>' size='40' maxlength='40' placeholder='85'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="deposit">Deposit</label></TH>
		<TD><input name="deposit" id="deposit" class="regular-text" type="text" value='<?php echo $deposit?>' size='40' maxlength='40' placeholder='200'/></TD>
	</TR>
	
	<?php
	if($support->type_value($type, 'public') == 0 && $_GET['team_id']!='new')
	{
		?>
		<tr>
			<th scope="row"><label for="hide_financials">Hide Financials?</label></th>
			<td><input name="no_finances" id="no_finances" type="checkbox" <?php echo $no_finances; ?> value="1"></td>
		</tr>
		<?php
	}
	?>		
		<TR>
		<TH scope="row"><label for="arrival_country">Arrival Country</label></TH>
		<TD><input name="arrival_country" id="arrival_country" class="regular-text" type="text" value='<?php echo $arrival_country?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="arrival_city">Arrival City</label></TH>
		<TD><input name="arrival_city" id="arrival_city" class="regular-text" type="text" value='<?php echo $arrival_city?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="departure_country">Departure Country</label></TH>
		<TD><input name="departure_country" id="departure_country" class="regular-text" type="text" value='<?php echo $departure_country?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="departure_city">Departure City</label></TH>
		<TD><input name="departure_city" id="departure_city" class="regular-text" type="text" value='<?php echo $departure_city?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="min">Minimum Volunteers</label></TH>
		<TD><input name="min" id="min" class="regular-text" type="text" value='<?php echo $min?>' size='5' maxlength='5'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="max">Maximum Volunteers</label></TH>
		<TD><input name="max" id="max" class="regular-text" type="text" value='<?php echo $max?>' size='5' maxlength='5'/></TD>
	</TR>
	<tr>
		<th scope="row"><label for="team_chat">Program Chat Enabled?</label></th>
		<td><input name="team_chat" id="team_chat" type="checkbox" <?php echo $team_chat; ?> value="1"></td>
	</tr>
	<tr>
		<th scope="row"><label for="team_chat">Automatic Messages Enabled?</label></th>
		<td><input name="messaging" id="messaging" type="checkbox" <?php echo $messaging; ?> value="1"></td>
	</tr>
	<TR>
		<TH scope="row"><label for="poc_table">Point of Contact</label></TH>
		<TD>
            <table id="poc_table">    
            <?php
	            if ( isset( $_POST['email'] ) && isset( $_POST['fn'] ) && isset( $_POST['ln'] ) ) {
	            	foreach ( $_POST['email'] as $index => $email ) : ?>
			            <tr>
				           <td>
					           <input type="text" size="30" maxlength="50" placeholder="Email" name="email[<?php echo $index ?>]" value="<?php echo $email ?>" id="email[<?php echo $index ?>]">
				           </td>
				           <td>
					           <input type="text" size="30" maxlength="50" placeholder="First Name" name="fn[<?php echo $index ?>]" value="<?php echo $_POST['fn'][$index] ?>" id="fn[<?php echo $index ?>]">
				           </td>
				           <td>
					           <input type="text" size="30" maxlength="50" placeholder="Last Name" name="ln[<?php echo $index ?>]" value="<?php echo $_POST['ln'][$index] ?>" id="ln[<?php echo $index ?>]">
				           </td>
				            <td>
					            <input type="button" name="del_poc_<?php echo $index; ?>" class="button del_poc" value="Delete">
					            <input type="button" name="add_poc_<?php echo $index; ?>" class="button add_poc" value="Add">
				            </td>
			            </tr>
		            <?php endforeach;
	            } else {

		            $possibles = $user->fetch_poc( $team->value( 'post' ) );
		            $counter   = 1;

		            if ( ! empty( $possibles ) ) {
			            foreach ( $possibles as $email => $data ) {
				            echo "<TR>";
				            echo "  <TD>";
				            echo "    <input type='text' size='30' maxlength='50' placeholder='Email' name='email[" . $counter . "]' id='email[" . $counter . "]' value='" . $email . "'>";
				            echo "  </TD>";
				            echo "  <TD>";
				            echo "    <input type='text' size='30' maxlength='50' placeholder='First Name' name='fn[" . $counter . "]' id='fn[" . $counter . "]' value='" . $data['first_name'] . "'>";
				            echo "  </TD>";
				            echo "  <TD>";
				            echo "    <input type='text' size='30' maxlength='50' placeholder='Last Name' name='ln[" . $counter . "]' id='ln[" . $counter . "]' value='" . $data['last_name'] . "'>";
				            echo "  </TD>";
				            echo "  <TD>";
				            echo "      <input type='button' name='del_poc_" . $counter . "' id='del_poc_" . $counter . "' class='button del_poc secondary' value='Delete'>";
				            echo "      <input type='button' name='add_poc_" . $counter . "' id='add_poc_" . $counter . "' class='button add_poc' value='Add'>";
				            echo "  </TD>";
				            echo "</TR>";

				            $counter ++;
			            }
		            } else {
			            echo "<TR>";
			            echo "  <TD>";
			            echo "    <input type='text' size='30' maxlength='50' name='email[" . $counter . "]' id='email[" . $counter . "]' placeholder='Email'>";
			            echo "  </TD>";
			            echo "  <TD>";
			            echo "    <input type='text' size='30' maxlength='50' name='fn[" . $counter . "]' id='fn[" . $counter . "]' placeholder='First Name'>";
			            echo "  </TD>";
			            echo "  <TD>";
			            echo "    <input type='text' size='30' maxlength='50' name='ln[" . $counter . "]' id='ln[" . $counter . "]' placeholder='Last Name''>";
			            echo "  </TD>";
			            echo "  <TD>";
			            echo "      <input type='button' name='del_poc_" . $counter . "' id='del_poc_" . $counter . "' class='button del_poc secondary' value='Delete'>";
			            echo "      <input type='button' name='add_poc_" . $counter . "' id='add_poc_" . $counter . "' class='button add_poc' value='Add'>";
			            echo "  </TD>";
			            echo "</TR>";
		            }

		            echo "<input type='hidden' name='poc_counter' id='poc_counter' value='" . ( $counter + 1 ) . "'>";

	            }
            ?>
			</table>
		</TD>
	</TR>
	<tr>
		<th scope="row"><label for="invoice_builder">Invoice Builder</label></th>
		<td>
			<A class="button" href='/wp-admin/admin.php?page=cc-pops&display=poc_invoice&team=<?= $team->get_team(); ?>' target='_blank'>Open Invoice Builder</A>
		</td>
	</tr>
	<TR>
		<TH scope="row"><label for="teaser">Program Goal</label></TH>
		<TD><?php wp_editor($teaser, "teaser", $text_settings)?></TD>
	</TR>
	<TR>
    	<TH scope="row"><label for="image_src"><a href="#" id="insert-my-media" class="button">Add Featured Image</a></label></TH>
        <TD><input type="hidden" id="image_url" name="image_url" value='<?php echo $image_url; ?>'>
			<input type="hidden" id="image_id" name="image_id" value='<?php echo $image_id; ?>'>
			<img class='team-image' name='image_src' id='image_src' src='<?php echo $image_url; ?>' alt='No Image Selected'>
		</TD>
    </TR>

</table>

<p class="submit actions-container">
	<input type="submit" name="delete" id="delete" class="button secondary float right" value="Delete Image"  />
	<button type="reset" name="reset" id="reset" class="button secondary float right" value="RESET">Reset</button>
	<input type="submit" name="submit" id="submit" class="button button-primary float left" value="Save Changes"  />
</p>

</form>

<form name="del_form" id="del_form" method="POST" enctype="multipart/form-data" 
      action="<?=$page->GET_path(array('save','delete')) . '&delete=true'?>">
      
	<input type="hidden" name="del_id" id="del_id" value="0">
</form>

<script>
	function delrec(record_id)
	{
		var id = document.getElementById('del_id');
		id.value = record_id;
		document.getElementById("del_form").submit();
	}
</script>
