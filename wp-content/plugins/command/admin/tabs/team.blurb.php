<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');


$itin_rows = 8;
$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, wp_help',
					   "toolbar2" => '');
			
$text_settings = array("media_buttons" => false,
					   "textarea_rows" => $itin_rows,
					   "teeny" => true,
					   "tinymce" => $tiny_settings,
					   "quicktags" => false);

if(isset($_GET['save']) && $_GET['save']=='true')
{
	//
	//Save all the blurbs
	$team->save_blurb('teaser', isset($_POST['teaser']) ? $_POST['teaser'] : NULL);
	$team->save_blurb('public', isset($_POST['public']) ? $_POST['public'] : NULL);
	$team->save_blurb('status', isset($_POST['status']) ? $_POST['status'] : NULL);
}

?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<table class="form-table">
	<TR>
		<TD><label for="teaser"><strong>Teaser</strong></label><BR>
			<?php wp_editor($team->get_blurb('teaser'), "teaser", $text_settings)?>
	</TR>
	<TR>
		<TD><label for="public_notice"><strong>Public Team Notice</strong></label><BR>
			<?php wp_editor($team->get_blurb('public'), "public", $text_settings)?>
	</TR>
	<TR>
		<TD><label for="status_chart"><strong>Status Chart Note</strong></label><BR>
			<?php wp_editor($team->get_blurb('status'), "status", $text_settings)?>
	</TR>
</table>

<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>
</form>
