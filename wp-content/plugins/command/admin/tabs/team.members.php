<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

global $wpdb;
include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');

$command_user = new USER();
		$team = new TEAM();
$meta_team_staff_key = 'cmd_team' . $_GET['team_id'] . '_staff_pos';
$w = 250;

//
//Save changes here
if(isset($_GET['save']) && $_GET['save']=='true')
{
	if(isset($_POST['del_id']) && $_POST['del_id'] > 0 && $_GET['delete'] == 'true')
	{
		delete_user_meta($_POST['del_id'], $meta_team_staff_key);
		do_action('cmd_staff_remove', $_GET['team_id'], $_POST['del_id']);
	}
	else
	{
		add_user_meta($_POST['staff_pos'], $meta_team_staff_key, $_POST['title']);
		do_action('cmd_staff_add', $_GET['team_id'], $_POST['staff_pos']);
	}
	
	//do_action('cmd_team_save', $_GET['team_id'], $_POST['hash']);

}


//
//Gather Defined Data here
$current_users = array();
$query = "SELECT um.user_id, um.meta_value, u.display_name
			FROM " . $wpdb->prefix . "usermeta um INNER JOIN
				 " . $wpdb->prefix . "users u on u.ID = um.user_id 
		  WHERE meta_key = '" . $meta_team_staff_key . "'
		  ORDER BY display_name";
$possibles = $wpdb->get_results($query, ARRAY_A);
foreach($possibles as $data)
{
	$current_users[$data['user_id']]['title'] = $data['meta_value'];
	$current_users[$data['user_id']]['name'] = $data['display_name'];
}

//
//Find possible Staff members for this team
//

//
//Find the countries involved in this team
$query = "SELECT c.id, c.title 
		  FROM " . $wpdb->prefix . "command_schedule s INNER JOIN
			   " . $wpdb->prefix . "command_country c on s.country_id = c.id 
		  WHERE s.team_id = " . $_GET['team_id'];
$possibles = $wpdb->get_results($query, ARRAY_A);

foreach($possibles as $data)
{
	$countries[$data['id']]=$data['title'];	
	$meta_staff_key[] = 'cmd_' . str_replace(' ', '_', strtolower($data['title'])) . '_staff_pos';
}

//
//Find all staff assigned to those countries
foreach($countries as $cname)
{
	
	$query = "SELECT um.user_id, um.meta_value, u.display_name 
			  FROM " . $wpdb->prefix . "usermeta um INNER JOIN " . $wpdb->prefix . "users u
					ON u.ID = um.user_id  
			  WHERE meta_key in ('" . implode("','", $meta_staff_key) . "')
			  ORDER BY display_name";

	$possibles = $wpdb->get_results($query, ARRAY_A);
	//echo var_dump($possibles);
	
	foreach($possibles as $data)
	{
		if(!isset($current_users[$data['user_id']]))
		{
			$staffers[$data['user_id']]['title'] = $data['meta_value'];		  
			$staffers[$data['user_id']]['name'] = $data['display_name'];	
		}
	}
}

	
//
//Snag all VEC
$filters = array('role' => 'staff-cap-101');
$vecs1 = $user->user_list(0, 10000, $filters);

$filters = array('role' => 'admin');
$vecs2 = $user->user_list(0, 10000, $filters);

$filters = array('role' => 'super');
$vecs3 = $user->user_list(0, 10000, $filters);

$vecs = $vecs1 + $vecs2 + $vecs3;
foreach($vecs as $v_user_id => $v_user_data)
{
	$staffers[$v_user_id]['title'] = 'VEC';
	$staffers[$v_user_id]['name'] = $v_user_data['first_name'] . ' ' . $v_user_data['last_name'];
}

?>	

<form name="data" method="POST" enctype="multipart/form-data" 
      action="<?=$page->GET_path(array('save','delete')) . '&save=true'?>">

	<table class="form-table team-info">
		<TR><TH style='<?= $w?>'>User</TH><TH style='<?= $w?>'>Title</TH><TH></TH></TR>
        
		<?php
        foreach($current_users as $id => $data)
        {
            echo "<TR><TD>" . $data['name'] . "</TD><TD>" . $data['title'] . "</TD>
                      <TD><button type='button' onclick='javascript:delrec(" . $id . ")'>Remove</button></TD></TR>";	
        }
        ?>

		<TR>
			<TD>
				<?php
				if(!empty($staffers) && count($staffers))
				{
					?>
                    <SELECT name='staff_pos' id='staff_pos'>
                    <?php
                    foreach($staffers as $user_id => $data)
                    {
                        echo "<OPTION VALUE	= $user_id>" . $data['name'] . 
                                    str_repeat('&nbsp;', 10) .
                                    "(" . $data['title'] . ")</OPTION>";
                    }
                    ?>
					</SELECT>
                    </TD>
                    <TD><SELECT name="title", id="title">
                    		<OPTION VALUE="VEC">Volunteer Experience Coordinator</OPTION>
                    		<OPTION VALUE="Team Leader">Team Leader</OPTION>
						</SELECT></TD>
					<TD><button type="submit">Add</button></TD>
				<?php
				}
				else
				{ 
					echo "<EM><B>No other qualified staff members.<B></EM></TD>";
				}
				?>
		</TR>
	</table>
	<input type="hidden" name="team_id" id="team_id" value="<?php echo $_GET['team_id'];?>">
</form>

<form name="del_form" id="del_form" method="POST" enctype="multipart/form-data" 
      action="<?=$page->GET_path(array('save','delete')) . '&save=true&delete=true'?>">
      
	<input type="hidden" name="del_id" id="del_id" value="0">		      
</form>

<script>
	function delrec(record_id)
	{
		var id = document.getElementById('del_id');
		id.value = record_id;
		document.getElementById("del_form").submit();				
	}
</script>
