<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "message.admin.class.php");
require_once (CC_CLASSES_DIR . "trigger.class.php");

$page = new TAB_MANAGE();
$admin_msg = new ADMIN_MESSAGES();
$triggers = new TRIGGERS();

if(isset($_REQUEST['create_message']))
{
	/** create trigger */
	$continue = true;
echo "<BR>";
echo $_REQUEST['subject'] . "<BR>";
echo $_REQUEST['message_text'] . "<BR>";
	
	foreach($_REQUEST['trigger'] as $t_key => $value)
	{
echo $t_key . " -> " . $value . "<BR>";
		if(empty($value))
		{
			$continue = false;
		}
	}
echo $continue . "<BR>";
	if($continue)
	{
		$triggers->set_meta_data($_REQUEST['trigger']);
		$triggers->set_notify(isset($_REQUEST['notify']) && $_REQUEST['notify'] == "on" ? 1 : 0);
		$triggers->set_name_from_meta();
		$triggers->set_type_from_meta();
		$triggers->set_run_time();
		$trigger_id = $triggers->save();

		$admin_msg->set_event($trigger_id);
		$admin_msg->set_subject($_REQUEST['subject']);
		$admin_msg->set_message($_REQUEST['message_text']);
		$admin_msg->set_notify(isset($_REQUEST['notify']) && $_REQUEST['notify'] == "on" ? 1 : 0);
		//
		//2018-04-16 DJT Added finance optino
		$admin_msg->set_finance(isset($_REQUEST['finance']) && $_REQUEST['finance'] == "on" ? 1 : 0);
		$admin_msg->add();
	}
	else
	{
		$error = "You need to complete the trigger";
	}
}

$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, removeformat, wp_help',"toolbar2" => '');

$text_settings = array("media_buttons" => false,
					   "textarea_rows" => 10,
					   "teeny" => false,
					   "tinymce" => $tiny_settings,
					   "quicktags" => false);


?>
<div class="create-message-view">
	<div class="page-header wrap">
		<h1>Create Message</h1>
		<?php if(isset($error)) { ?>
		<h1><?php echo $error; ?></h1>
		<?php } ?>
		<?php //echo var_dump($triggers->get_joined_meta()) ?>
	</div>

	<form method="POST" action="<?php echo $page->GET_path()?>">

		<p class="helper-text">First, define the criteria for when this message will be sent.<br><em>Think: "this message will be sent when ________ happens."</em></p>

		<div id="message_start"></div>

		<p class="helper-text">Next, set up the message itself:</p>

		<div class="message-wrapper">
			<div class="message-inner-wrapper">
				<input type="text" name="subject" placeholder="Message Subject" class="message-subject-input"/>
				<?php wp_editor("", "message_text", $text_settings)?>
				<Input type="checkbox" name="notify" id="notify"></Input>Notify Trip Coordinator<br />
				<!-- -->
				<!--2016-04-16 DJT Addded topinton for finance-->
				<Input type="checkbox" name="finance" id="finance"></Input>Finance<br />
				<input type="submit" class="button button-primary" name="create_message" value="Create Message" />
			</div>

			
			<div class="shortcode-legend">
				<h6>The following shortcodes are available within your message:</h6>
				<ul>
					<li><span>{{First Name}}</span></li>
					<li><span>{{Last Name}}</span></li>
					<li><span>{{Display Name}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{Address}}</span></li>
					<li><span>{{Phone}}</span></li>
					<li><span>{{Email}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{Program}}</span></li>
					<li><span>{{Country}}</span></li>
					<li><span>{{Departure Date}}</span></li>
					<li><span>{{Return Date}}</span><br><small><em>(Departure + Itinerary Length)</em></small></li>
				</ul>
				<ul>
<!--					<li><span>{{VEC}}</span></li>-->
					<li><span>{{Trip Coordinator}}</span></li>
					<li><span>{{Team Leader}}</span></li>
					<li><span>{{Balance Due}}</span></li>
					<li><span>{{Cost}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{DATE}}</span></li>
					<li><span>{{team_arrival_city}}</span></li>
					<li><span>{{team_arrival_country}}</span></li>
					<li><span>{{team_departure_city}}</span></li>
					<li><span>{{team_departure_country}}</span></li>
					<li><span>{{trip_coordinator_email}}</span></li>
					<li><span>{{trip_coordinator_phone}}</span></li>
				</ul>
			</div>

		</div>
	</form>

</div>

<script>
	var msg_obj = <?php echo json_encode( (array)$triggers->get_joined_meta()); ?>;
	console.log('msg_obj', msg_obj);
	var level = 1;

	var get_next_obj = function(obj_json,level)
	{
		var return_obj;
		if (level == null)
		{
			return_obj = msg_obj;
		}
		else if (level == 1)
		{
			 return_obj = obj_json;
		}
		else if(level > 1)
		{
			var count = jQuery("#message_start").children().last().data("level");
			var cur_level = ((count+0)-(level+0))+2;
			var cur_select = jQuery("select.level_"+cur_level);
			var cur_name = cur_select.attr("name");
			cur_name = cur_name.substring(8,cur_name.length - 1);
			var cur_choice = cur_select.children(":selected").val();
			
			var obj2_json = obj_json[cur_name];
			obj2_json = obj2_json[cur_choice];
			
			return_obj = get_next_obj(obj2_json,level-1);
		}
		return return_obj;
	}

	function make_selection(msg_object,level)
	{
	console.log('msg_object', msg_object);
		if(level == null)
		{
			level = 1
		}

		var output = "";

		for (key in msg_object)
		{
			if(jQuery.inArray(key,['value']))
			{
				
				if(jQuery.type(msg_object[key]) == "object")
				{
					console.log('key', key);
					// if(key == "interval" || key == "filter_value")
					if(key == "interval")
					{
						output +='<input type="text" class="level_'+ level +'"  data-level="'+ level +'" name="trigger['+ key +']" placeholder="'+ (key == "interval" ? key+" (a number)" : "Filter value") + '"/>';
					}
					else 
					{
						output +='<select class="level_'+ level +'" data-level="'+ level +'" name="trigger['+ key +']">';
						msg_obj_data = msg_object[key];

						// This object's key...
						var keyString = '';
						switch (key) {
							case 'type':
								keyString = 'Trigger on...';	
								break;
							case 'unit':
								keyString = 'Interval unit...';
								break;
							case 'name':
								keyString = 'Choose option...';
								break;
							case 'filter':
								keyString = 'Filter by...';
								break;
							case 'filter_value':
								keyString = 'Choose option...';
								break;
						}
						// output+='<option class="selection" value="" disabled>'+ key.charAt(0).toUpperCase()+key.slice(1) +':</option>';
						output+='<option class="selection" value="">'+ keyString +'</option>';

						// Child object....
						for(keyb in msg_obj_data)
						{
							output+='<option class="selection" data-next="'+keyb+'" value="'+keyb+'" >'+msg_obj_data[keyb].value.charAt(0).toUpperCase()+msg_obj_data[keyb].value.slice(1) +'</option>';
						}
						output+="</select>";
					}
				}
			}
		}

		jQuery("#message_start").append(output);
	}

	function clear_elements(event_obj)
	{
		level = jQuery(event_obj).parent('select').data('level');
		var selectElements = jQuery(event_obj).parent("select").parent("#message_start").children();
		var count = jQuery("#message_start").children().last().data("level");
		for (var i = count; i > level; i--)
		{
				var select = jQuery(event_obj).parent("select").parent("#message_start").children(".level_"+i);
				select.remove();
		}

		return level+1;
	}

	jQuery(document).ready(function()
	{
		make_selection(msg_obj,null);
	});

	jQuery(document).on('change','select', function() {
		var opt = jQuery("option:selected", this);
		var level = clear_elements(opt);
		var new_obj = get_next_obj(msg_obj,level);
		console.log('get_next_obj', msg_obj, level);
		// console.log(level);
		make_selection(new_obj,level);
	});
</script>
