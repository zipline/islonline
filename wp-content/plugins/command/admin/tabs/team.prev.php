<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');

$post_data = get_post($team->value('post'));
$team_url = '/team/' . $post_data->post_name;

?>
<div class="team-info-wrapper">
<H1 align='center'>
    Preview &nbsp;&nbsp; or &nbsp;&nbsp;
    <button class="button" name="pressme" onclick="javascript:window.open('<?php echo $team_url?>', '_blank', '', false)">Live View</button>
</H1>

<br />
<iframe name="prev_window" 
		id="prev_window" 
        src='<?php echo $team_url?>' 
        width="100%" 
        height="200px" 
        scrolling="auto" 
        onLoad="autoResize('prev_window');">
</iframe> 
                    
</div>
<script language="JavaScript">
<!--
function autoResize(id){
    var newheight;
    var newwidth;

    if(document.getElementById){
        newheight = document.getElementById(id).contentWindow.document .body.scrollHeight;
        newwidth = document.getElementById(id).contentWindow.document .body.scrollWidth;
    }

    document.getElementById(id).height = (newheight *.9) + "px";
    document.getElementById(id).width = (newwidth) + "px";
}
-->
</script>