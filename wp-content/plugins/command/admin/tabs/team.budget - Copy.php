<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');

$core_cats = array("Lodging", "Transportation", "Professional Staff", "ISL Staff", "Food & Water", "Other");
$status = array("New", "Working", "Submitted", "Rejected", "Approved", "Reconciliation", "Closed");
$data = array();

//
//Ok, doing something like saving here
if(!empty($_POST['switch']))
{
	$cur_status = $_POST['switch'];

	//
	//Build array of cats and data
	foreach($_POST as $id=>$value)
	{

		if (substr($id, -4) == '_cat')
		{
			$cat = substr($id, 0, -4);
			$data[$id] = array('cat' => $_POST[$cat . '_cat'],
							   'bud' => $_POST[$cat . '_bud'],
					   		   'act' => $_POST[$cat . '_act'],
					   		   'com' => $_POST[$cat . '_com']
				 			   );
		}
	}

	$success = update_post_meta($team->value('post'), '_isl_budget', array($cur_status => $data));
}

//
//Gather post meta here if available
$post_meta = get_post_meta($team->value('post'), '_isl_budget', false);

if(!empty($post_meta))
{
	//
	//Deconstruct post_meta back into $data
	foreach($post_meta as $dummy => $holding)
	{
		foreach($holding as $cur_status => $temp_data)
		{
			//$cur_status = $status[$status_index];
			foreach($temp_data as $index => $short_data)
			{
				$prefix = substr($index, 0, -4);
				foreach($short_data as $suffix => $value)
				{
					$data[$prefix . '_' . $suffix] = $value;				
				}
			}
		}
	}
}
else
{
	foreach($core_cats as $category)
	{
		$short = preg_replace('/[^A-Za-z0-9\-]/', '', strtolower(str_replace(' ', '', $category)));
		$data[$short . '_cat'] = $category;
	}

	if(empty($cur_status))
	{
		$cur_status = $status[0];
	}
}

?>

<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('switch'))?>">

    <div class="Table">
        <div class="Header">
            <div class="Cell">Category</div>
            <div class="Cell">Budget</div>
            <div class="Cell">Actual</div>
            <div class="Cell">Difference</div>
            <div class="Cell">Comments</div>
        </div>        
    
        <?php
        foreach($data as $id=>$value)
        {
			if(substr($id, -4) == '_cat')
			{
				$short = preg_replace('/[^A-Za-z0-9\-]/', '', strtolower(str_replace(' ', '', $data[substr($id, 0, -4) . '_cat'])));
				
				echo "<div class='Row' name='" . $short . "' id='" . $short . "'>";
				echo "<div class='Cell'>";
				echo "<input type='text' name='" . $short . "_cat' id='" . $short . "_cat' value='" . (!empty($data[$short . '_cat']) ? $data[$short . '_cat'] : '') . "' readonly='true'></div>";
						
				echo "<div class='Cell'>$
						<input type='number' name='" . $short . "_bud' id='" . $short . "_bud' value='" . (!empty($data[$short . '_bud']) ? $data[$short . '_bud'] : '') . "'" .  ($short == 'other' ? " onfocus='javascript:check_prompt()'" : '') . "></div>";
				echo "<div class='Cell'>$
						<input type='number' name='" . $short . "_act' id='" . $short . "_act' value='" . (!empty($data[$short . '_act']) ? $data[$short . '_act'] : '') . "'" . ($short == 'other' ? "onfocus='javascript:check_prompt()'" : '') . "></div>";
				echo "<div class='Cell'>$
						<input type='number' name=''" . $short . "_dif' id='" . $short . "_dif' value='" . (!empty($data[$short . '_bud']) ? (!empty($data[$short . '_act']) ? $data[$short . '_bud'] - $data[$short . '_act'] : '') : '') . "' readonly='true'></div>";
				echo "<div class='Cell'>
						<input type='text' name='" . $short . "_com' id='" . $short . "_com' value='" . (!empty($data[$short . '_com']) ? $data[$short . '_com'] : '') . "'></div>";
				echo "</div>";
			}
        }

		echo "Status: " . $status[$cur_status] . '<BR>';
        
        switch ($cur_status)
        {
            case '0':
            case '1':
                echo "<input type='button' value='Save' onClick='javascript:switch_status(1)'>";
                echo "<input type='button' value='Submit' onClick='javascript:switch_status(2)'>";
                break;			
            case '2':
                echo "<input type='button' value='Reject' onClick='javascript:switch_status(3)'>";
                echo "<input type='button' value='Approve' onClick='javascript:switch_status(4)'>";
                break;
            case '4':
                echo "<input type='button' value='Save' onClick='javascript:switch_status(5)'>";
                echo "<input type='button' value='Close' onClick='javascript:switch_status(6)'>";
                break;
            case '6':
                break;		
        }
        ?>
        
    </div>
    
    <input type="hidden" name="switch" id="switch" value="<?php _e($cur_status)?>">

</form>

<script>
	
	function switch_status(switch_to)
	{
		element = document.getElementById('switch');
		element.value = switch_to;
		document.data.submit();			
	}

	function check_prompt()
	{
		budget = document.getElementById('other_bud');
		
		if(budget > 0)
		{
			var x = 0;
		}
		else
		{
			var other = confirm("Are you sure about using the 'Other' Category?");
			if(other == false)
			{
				var new_cat = confirm("Would you like a new Category added?");
				if(new_cat == true)
				{
					var new_element = prompt("Please enter new Category title");
				}
			}
		}
	}

</script>

<style type="text/css">
    .Table
    {
        display: table;
		margin-top:30px;
    }
    .Title
    {
        display: table-caption;
        text-align: center;
        font-weight: bold;
        font-size: larger;
    }
    .Header
    {
        display: table-row;
        font-weight: bold;
        text-align: center;
    }
    .Row
    {
        display: table-row;
    }
    .Cell
    {
        display: table-cell;
        border: solid;
        border-width: thin;
        padding-left: 5px;
        padding-right: 5px;
    }
	input[type=number]
	{
		text-align: right;	
	}
	input[type=button]
	{
		margin: 25px 5px 5px 5px;
		font-weight: bold;
		text-align: center;
		width: 100px;	
	}
</style>