<?php

require_once (CC_CLASSES_DIR . "acf.link.class.php");
$adv_custom_flds = new ACF_LINK();
$custom_post = get_post_meta($team->value('post'), '_cmd_custom_team_build', true);


if(!empty($_POST['action']) && $_POST['action']=="update")
{
	$fld_list = $_POST;

	foreach($_POST as $temp=>$val)
	{
		if($temp == "acf")
		{
			foreach($val as $index=>$value)
			{
				//
				//ACF field so handle differently
				if(substr($index,0,6) == "field_" && !empty($fld_list['acf'][$index]))
				{
					$adv_custom_flds->save_field_group($index, $fld_list['acf'], $_GET['user_id'], $post_id, true);
				}
			}
		}
	}
}
?>

<form class="u_info" name="u_info" method = "POST" action="<?php $page->GET_path(array('save'))?>">
 
	<table>
	<tr><td><h2>Custom Request</h2></td></tr>

		<?php
			$content = $adv_custom_flds->get_fields(20716);
			
			foreach($content as $fld_id => $fld_data)
			{
				echo "<TR>";
				echo "<th><label for='" . $fld_id . "'>" . $fld_data['title'] . "</label></th>";
	        	echo "<td>" . $adv_custom_flds->build_field($fld_id, 0, $custom_post, $fld_data) . "</td>";
				echo "</TR>";				
			}
		?>

	</table>

	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $_GET['user_id']?>" />

	<!--<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Update Data"  /></p>-->

</form>
