<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
// updates the region data
$country->set_country($_GET['country_id']);
$country->set_region((isset($_GET['region_id']) ? $_GET['region_id'] : -1));
$region = $country->region_details();

if(isset($_GET['save']) && $_GET['save']=='true')
{

	if(!empty($_GET['region_id']))
	{
		if($_POST['title'] != $region['title']) { $country->save_region_info('title',$_POST['title']); }
		if($_POST['code'] != $region['code']) { $country->save_region_info('kode',$_POST['code']); }
		if($_POST['desc'] != $region['desc']) { $country->save_region_info('description',urlencode($_POST['desc'])); }
	
		if(isset($_POST['active']) && $_POST['active']=='on')
		{
			if($region['active']!=1)
			{
				$country->save_region_info('active',1);
			}
		}
		else
		{
			if($region['active']!=0)
			{
				$country->save_region_info('active',0);
			}
		}
	}
	else
	{
		if(!empty($_POST['title']) &&
		   !empty($_POST['code']) &&
		   !empty($_POST['desc'])
		   )
		 {
			$country->build_region($_GET['country_id'],
								   $_POST['title'],
								   $_POST['code'],
								   $_POST['desc'],
								   isset($_POST['active']) && $_POST['active']=='on' ? 1 : 0
								  ); 
			$tmp_region = $country->get_region();			 
			 
		 }
	}
	
	$country_id = $country->get_country();

	$country = new country();
	$country->set_country($country_id);
	$country->set_region(!empty($tmp_region) ? $tmp_region : (isset($_GET['region_id']) ? $_GET['region_id'] : -1));
	$region = $country->region_details();
}

//
//Write in Select field
?>
<div class="country-info-wrapper">
<H2>Region: <SELECT name="region" id="region" onChange="javascript:change_region()"><?php echo $country->region_selector($country->get_region(), true, "New") ?></SELECT></H2>


<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<table class="form-table">
	<TR>
    	<TH scope="row"><label for="title">Title</label></TH>
        <TD><input name="title" id="title" type="text" class="regular-text" value="<?=$region['title']?>" /></TD>
    </TR>
	<TR>
    	<TH scope="row"><label for="code">Code</label></TH>
        <TD><input name="code" id="code" type="text" class="regular-text" value="<?=$region['code']?>" /></TD>
    </TR>
	<TR>
    	<TH scope="row"><label for="active">Active</label></TH>
        <TD><input name="active" id="active" type="checkbox" <?php echo ($region['active'] == 1 ? "checked" : "")?> /></TD>
    </TR>
</table>

<?php
$settings = array('textarea_name' => 'desc', 'textarea_rows' => 6, 'media_buttons' => FALSE);
wp_editor(urldecode($region['description']),'desc', $settings);
echo "<h2><B>Description</B></h2>";
?>

<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>
</form>
</div>
		<script>
        function change_region()
        {
            var url;
            url = '<?=$page->GET_path(array('region_id','save'))?>' + '&region_id=' + document.getElementById('region').value;
            window.location.href = url;
        }
        </script>

