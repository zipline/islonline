<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');
include_once(CC_CLASSES_DIR . 'message.class.php');
include_once(CC_CLASSES_DIR . 'user.class.php');

$user = new USER();

if(isset($_REQUEST['send_message']))
{
	$cur_user = wp_get_current_user();

	$message = new MESSAGES();
	$message->set_sender($cur_user->ID);
	$message->set_message_text($_REQUEST['team_message']);
	$message->set_subject($_REQUEST['message_subject']);
	$message->set_team_id($_REQUEST['team_id']);
	$message->add_team_message($_REQUEST['team_id'], 'Workaround');

	//do_action('cmd_queue_trigger',"Team Message Sent",array(),array('team_id' => $_REQUEST['team_id']));
}

$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, removeformat, wp_help',"toolbar2" => '');

$text_settings = array("media_buttons" => false,
					   "textarea_rows" => 12,
					   "teeny" => false,
					   "tinymce" => $tiny_settings,
					   "quicktags" => false);



?>
<div id="main" class="team-msg">
<h2>Send team a message</h2>
<form id="volunteer_message_form"method="POST" enctype="multipart/form-data" action="<?=$page->GET_path()?>">
<div class="shortcode-legend">
				<h6>The following shortcodes are available within your message:</h6>
				<ul>
					<li><span>{{First Name}}</span></li>
					<li><span>{{Last Name}}</span></li>
					<li><span>{{Display Name}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{Address}}</span></li>
					<li><span>{{Phone}}</span></li>
					<li><span>{{Email}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{Program}}</span></li>
					<li><span>{{Country}}</span></li>
					<li><span>{{Departure Date}}</span></li>
					<li><span>{{Return Date}}</span><br><small><em>(Departure + Itinerary Length)</em></small></li>
				</ul>
				<ul>
					<li><span>{{VEC}}</span></li>
                    <li><span>{{Trip Coordinator}}</span></li>
					<li><span>{{Team Leader}}</span></li>
					<li><span>{{Balance Due}}</span></li>
					<li><span>{{Cost}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{DATE}}</span></li>
                    <li><span>{{team_arrival_city}}</span></li>
                    <li><span>{{team_arrival_country}}</span></li>
                    <li><span>{{team_departure_city}}</span></li>
                    <li><span>{{team_departure_country}}</span></li>
					<li><span>{{trip_coordinator_email}}</span></li>
					<li><span>{{trip_coordinator_phone}}</span></li>
				</ul>
			</div>
					<input type="text" name="message_subject" placeholder="Message Subject"/>
		<?php wp_editor('', "team_message", $text_settings)?>
		<input class="button button-primary" type="submit" name="send_message" value="Send Message" />
	</form>
</div>


<div class='message-archive'>
<h3>Message Archive</h3>
<?php

$message = new MESSAGES();
$all_messages = $message->get_team_messages($_REQUEST['team_id']);
		  
if(count($all_messages) > 0)
{
	echo "<TABLE class='standard-large-table form-table striped'>";
	echo "<TR>
			<TH class='large'>Created</TH>
			<TH>Subject</TH>
			<TH>Message</TH>
			<TH class='large'>Read</TH>
			<TH class='large'>User</TH>
		  </TR>";
	
	foreach($all_messages as $ind=>$data)
	{
		echo "<TR><TD>" . str_replace(' ', '<BR>', $data->created_at) . "</TD>
				  <TD> " . $data->subject . "</TD>
				  <TD> " . html_entity_decode($data->msg_text, ENT_QUOTES | ENT_XML1, 'UTF-8') . "</TD>
				  <TD> " . str_replace(' ', '<BR>', $data->read_at) . "</TD>
				  <TD> " . $user->user_name($data->recipient) . "</TD>
			  </TR>";
		
	}
	
	echo "</TABLE>";
}

else 
if(count($all_messages) == 0) {
	echo "<p><em>No messages sent.</em></p>";
}

		  ?>