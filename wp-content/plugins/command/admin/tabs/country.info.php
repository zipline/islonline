<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

$all_countries = $country->list_countries(true);
$cur_country = $country->details();

//
// parses and saves country info
if(isset($_GET['save']) && $_GET['save']=='true')
{
	if(isset($_POST['passport']) && $_POST['passport'] == "on")
	{
		$passport = 1;		
	}
	else
	{
		$passport = 0;
	}
	
	if($_GET['country_id']=='new')
	{
		if(isset($_POST['title']) && isset($_POST['code']))
		{
			if(strlen($_POST['title'])>3 && strlen($_POST['code'])>1)
			{
				$country->build($_POST['title'],$_POST['code'], $passport);	
				$new_path = $page->GET_path(array('country_id','save'),array('country_id'=>$country->get_country()));
				echo "<script>";
				echo "  window.location = '$new_path';";
				echo "</script>";
			}
		}

	}
	else
	{
	
		if($_POST['title'] != $cur_country['title']) { $country->save_info('title',$_POST['title']); }
		if($_POST['code'] != $cur_country['code']) { $country->save_info('kode',$_POST['code']); }
	
		if(isset($_POST['active']) && $_POST['active']=='on')
		{
			if($cur_country['active']!=1)
			{
				$country->save_info('active',1);
			}
		}
		else
		{
			if($cur_country['active']!=0)
			{
				$country->save_info('active',0);
			}
		}

		if(isset($_POST['passport']) && $_POST['passport']=='on')
		{
			if($cur_country['passport']!=1)
			{
				$country->save_info('passport',1);
			}
		}
		else
		{
			if($cur_country['passport']!=0)
			{
				$country->save_info('passport',0);
			}
		}

		$country_id = $country->get_country();
		$country = new country();
		$country->set_country($country_id);
	
		$all_countries = $country->list_countries();
		$cur_country = $country->details();
	}

}

?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<table class="form-table country-table">
	<TR>
    	<TH scope="row"><label for="title">Title</label></TH>
        <TD><input name="title" id="title" type="text" class="regular-text" value="<?=$cur_country['title']?>" /></TD>
    </TR>
	<TR>
    	<TH scope="row"><label for="code">Code</label></TH>
        <TD><input name="code" id="code" type="text" class="regular-text" value="<?=$cur_country['code']?>" /></TD>
    </TR>
	<TR>
    	<TH scope="row"><label for="code">Passport Required</label></TH>
        <TD><input name="passport" id="passport" type="checkbox"  <?php echo ($cur_country['passport'] == 1 ? "checked" : "")?> /></TD>
    </TR>
	<TR>
    	<TH scope="row"><label for="active">Active <span>note: country will only activate if there is content entered for the page</span></label></TH>
        <TD><input name="active" id="active" type="checkbox" <?php echo ($cur_country['active'] == 1 ? "checked" : "")?> /></TD>
    </TR>
</table>

<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>
</form>

