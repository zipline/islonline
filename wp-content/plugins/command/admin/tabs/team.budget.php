<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');

unset($possibles);
unset($data);

$core_cats = array(1 => "Lodging", 2 => "Transportation", 3 => "Professional Staff", 
				   4 => "ISL Staff", 5 => "Food & Water", 6 => "Other");
$status = array("New", "Working", "Submitted", "Rejected", "Approved", "Reconciliation", "Closed");

function cat_select($id = '', $cat_list = array(), $selected = 0)
{

	$ret = "";
	$ret .= "<SELECT name='" . $id . "' id='" . $id . "'>";
	$ret .= "<OPTION value=0" . (($selected == 0) ? " SELECTED" : "") . "> -- SELECT --</OPTION>";	

	foreach($cat_list as $val=>$title)
	{
		$ret .= "<OPTION value=" . $val . (($selected == $val) ? " SELECTED" : "") . ">" . $title . "</OPTION>";	
	}
	
	$ret .= "</SELECT>";
	
	return $ret;		
}

//
//Ok, doing something like saving here
if(!empty($_POST['switch']))
{
	$cur_status = $_POST['switch'];

	//
	//Build array of cats and data
	foreach($_POST as $id=>$value)
	{

		if (substr($id, 0, 4) == 'itm_')
		{
			$row = substr($id, 4);
			$index = $_POST['cat_' . $row];
			
			if($index > 0 && !empty($_POST['itm_' . $row]))
			{
				$line = empty($data[$index]) ? 1 : count($data[$index])+1;
	
				$data[$index][$line] = array('itm' => $_POST['itm_' . $row],
											 'bud' => $_POST['bud_' . $row],
											 'act' => $_POST['act_' . $row],
											 'com' => $_POST['com_' . $row]
											);
			}
		}
	}

	ksort($data);
	$success = update_post_meta($team->value('post'), '_isl_budget', array($cur_status => $data));
}


//
//Gather post meta here if available
$budget_data = get_post_meta($team->value('post'), '_isl_budget', false);

if(!empty($budget_data))
{
	//
	//Deconstruct post_meta back into $data
	foreach($budget_data as $dummy => $holding)
	{
		foreach($holding as $cur_status => $cats)
		{
			foreach($cats as $cat_id => $row)
			{
				foreach($row as $row_id => $row_data)
				{
					foreach($row_data as $item => $value)
					{
						$data[$cat_id][$row_id][$item] = $value;				
					}
				}
			}
		}
	}
}
else
{
	$cur_status = 0;
}
$data[0][999999]['itm']='';
$data[0][999999]['bud']='';
$data[0][999999]['act']='';
$data[0][999999]['com']='';

?>
<div class="team-info-wrapper">
<?php
echo "<B>Status: " . $status[$cur_status] . '</b><BR>';
?>

<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('switch'))?>">

    <div class="table">
        <div class="header">
            <div class="cell">Item</div>
            <div class="cell">Category</div>
            <div class="cell">Budget</div>
            <div class="cell">Actual</div>
            <div class="cell">Difference</div>
            <div class="cell">Comments</div>
        </div>        
    
        <?php

		$line=1;
		foreach($data as $cat => $rows)
		{
			foreach($rows as $row => $items)
			{
				echo "<div class='row' name='row_" . $line . "' id='row_" . $line . "'>";
		
				echo "<div class='cell'>";
					echo "<input type='text' name='itm_" . $line . "' id='itm_" . $line . "' value='" . stripslashes($items['itm']) . "' size='30' maxlength='30'>";
				echo "</div>";
		
				echo "<div class='cell'>";
					echo cat_select('cat_' . $line, $core_cats, $cat);
				echo "</div>";
		
				echo "<div class='cell'>";
					echo "<span>$</span><input type='text' name='bud_" . $line . "' id='bud_" . $line . "' value='" . $items['bud'] . "' size='12' maxlength='12'>";
				echo "</div>";
		
				echo "<div class='cell'>";
					echo "<span>$</span><input type='text' name='act_" . $line . "' id='act_" . $line . "' value='" . $items['act'] . "' size='12' maxlength='12'>";
				echo "</div>";
		
				echo "<div class='cell'>";
					echo "<span>$</span><input type='text' name='dif_" . $line . "' id='dif_" . $line . "' value='" . ($items['act'] - $items['bud']) . "' readonly='true' size='12' maxlength='12'>";
				echo "</div>";
		
				echo "<div class='cell'>";
					echo "<input type='text' name='com_" . $line . "' id='com_" . $line . "' value='" . stripslashes($items['com']) . "' size='30' maxlength='30'>";
				echo "</div>";
		
				echo "</div>";
				$line ++;
			}
		}

        switch ($cur_status)
        {
            case '0':
            case '1':
                echo "<input type='button' class='button' value='Save' onClick='javascript:switch_status(1)'>";
                echo "<input type='button' class='button' value='Submit' onClick='javascript:switch_status(2)'>";
                break;			
            case '2':
                echo "<input type='button' class='button' value='Reject' onClick='javascript:switch_status(3)'>";
                echo "<input type='button' class='button' value='Approve' onClick='javascript:switch_status(4)'>";
                break;
            case '3':
                echo "<input type='button' class='button' value='Save' onClick='javascript:switch_status(1)'>";
                echo "<input type='button' class='button' value='Submit' onClick='javascript:switch_status(2)'>";
                break;			
            case '4':
                echo "<input type='button' class='button' value='Save' onClick='javascript:switch_status(5)'>";
                echo "<input type='button' class='button' value='Close' onClick='javascript:switch_status(6)'>";
                break;
            case '6':
                break;		
        }
        ?>
        
    </div>
    
    <input type="hidden" name="switch" id="switch" value="<?php _e($cur_status)?>">

</form>
</div>
<script>
	
	function switch_status(switch_to)
	{
		element = document.getElementById('switch');
		element.value = switch_to;
		document.data.submit();			
	}

	function check_prompt()
	{
		budget = document.getElementById('other_bud');
		
		if(budget > 0)
		{
			var x = 0;
		}
		else
		{
			var other = confirm("Are you sure about using the 'Other' Category?");
			if(other == false)
			{
				var new_cat = confirm("Would you like a new Category added?");
				if(new_cat == true)
				{
					var new_element = prompt("Please enter new Category title");
				}
			}
		}
	}

</script>

<style type="text/css">
    .Table
    {
        display: table;
		margin-top:30px;
    }
    .Title
    {
        display: table-caption;
        text-align: center;
        font-weight: bold;
        font-size: larger;
    }
    .Header
    {
        display: table-row;
        font-weight: bold;
        text-align: center;
    }
    .Row
    {
        display: table-row;
    }
    .Cell
    {
        display: table-cell;
        border: solid;
        border-width: thin;
        padding-left: 5px;
        padding-right: 5px;
    }
	input[type=number]
	{
		text-align: right;	
	}
	input[type=button]
	{
		margin: 25px 5px 5px 5px;
		font-weight: bold;
		text-align: center;
		width: 100px;	
	}
</style>