<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "message.admin.class.php");
require_once (CC_CLASSES_DIR . "trigger.class.php");

$page = new TAB_MANAGE();
$admin_msg = new ADMIN_MESSAGES();
$trigger = new TRIGGERS();

/** update the message */
if(isset($_REQUEST['update_message']))
{
	$admin_msg->set_event($_REQUEST['msg_id']);
	$admin_msg->set_subject($_REQUEST['subject']);
	$admin_msg->set_message($_REQUEST['message_text']);
	//
	//2018-04-16 DJT Added finance optino
	$admin_msg->set_finance(isset($_REQUEST['finance']) && $_REQUEST['finance'] == "on" ? 1 : 0);
	$admin_msg->set_notify(isset($_REQUEST['notify']) && $_REQUEST['notify'] == "on" ? 1 : 0);
	$admin_msg->edit();
}

/** or delete the message from the db */
else if(isset($_REQUEST['remove_message']))
{
	$admin_msg->set_event($_REQUEST['msg_id']);
	$admin_msg->destroy();
}

$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, removeformat, wp_help',"toolbar2" => '');

$text_settings = array("media_buttons" => false,
					   "textarea_rows" => 10,
					   //"teeny" => false,
					   //"tinymce" => $tiny_settings,
					   //"quicktags" => false
					  );
?>

<!-- Load the current message -->
<div class="edit-message-view">
	<?php
	if(isset($_REQUEST['msg_id'])) {
		if($trigger->check_exists($_REQUEST['msg_id']))
		{
		$trigger->read($_REQUEST['msg_id']);
		$trigger_name = $trigger->build_full_name();
		$admin_msg->set_event($_REQUEST['msg_id']);
		$admin_msg->load(); ?>
	<div class="page-header wrap">
		<h1>Edit Message</h1>
	</div>

	<p class="helper-text"><strong>Message Trigger:</strong> <?php echo $trigger_name; ?></p>

	<form method="POST" action="<?php echo $page->GET_path(); ?>">
		<div class="message-wrapper">
			<div class="message-inner-wrapper">
				<input class="message-subject-input" type="text" name="subject" placeholder="Message Subject" value="<?php echo $admin_msg->get_subject(); ?>" />
				<?php wp_editor(stripslashes($admin_msg->get_message()), "message_text", $text_settings); ?>
				<Input type="checkbox" name="notify" id="notify" <?php echo ($admin_msg->get_notify()==1 ? 'checked' : '')?>></Input>Notify Trip Coordinator<br />
				<Input type="checkbox" name="finance" id="finance" <?php echo ($admin_msg->get_finance()==1 ? 'checked' : '')?>></Input>Finance<br />
				<input type="hidden" name="event" value="<?php echo $admin_msg->get_event(); ?>" />
				<input type="submit" class="button button-primary right" name="remove_message" value="Delete Message" />
				<input type="submit" class="button button-primary left" name="update_message" value="Save Message" />
			</div>

			<div class="shortcode-legend">
				<h6>The following shortcodes are available within your message:</h6>
				<ul>
					<li><span>{{First Name}}</span></li>
					<li><span>{{Last Name}}</span></li>
					<li><span>{{Display Name}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{Address}}</span></li>
					<li><span>{{Phone}}</span></li>
					<li><span>{{Email}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{Program}}</span></li>
					<li><span>{{Country}}</span></li>
					<li><span>{{Departure Date}}</span></li>
					<li><span>{{Return Date}}</span><br><small><em>(Departure + Itinerary Length)</em></small></li>
				</ul>
				<ul>
<!--					<li><span>{{VEC}}</span></li>-->
                    <li><span>{{Trip Coordinator}}</span></li>
					<li><span>{{Team Leader}}</span></li>
					<li><span>{{Balance Due}}</span></li>
					<li><span>{{Cost}}</span></li>
					<li>&nbsp;</li>
					<li><span>{{DATE}}</span></li>
                    <li><span>{{team_arrival_city}}</span></li>
                    <li><span>{{team_arrival_country}}</span></li>
                    <li><span>{{team_departure_city}}</span></li>
                    <li><span>{{team_departure_country}}</span></li>
					<li><span>{{trip_coordinator_email}}</span></li>
					<li><span>{{trip_coordinator_phone}}</span></li>
				</ul>
			</div>
		</div>
	</form>

	<form method="POST" action="<?php echo $page->GET_path()?>">
	</form>

	<?php } else { ?>
	<div class="page-header wrap">
		<h1>Message has been deleted</h1>
		<script type="text/javascript">
			window.location = "<?php echo $page->GET_path(array('msg_id')); ?>";
		</script>
	</div>
	<?php } ?>
</div>
<?php } ?>