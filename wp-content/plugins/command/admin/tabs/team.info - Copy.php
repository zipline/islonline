<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');
$error = array();
$w = 'width:125px';

wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
wp_enqueue_script('cmd_schedule_adder', plugins_url() . '/command/js/schedule.adder.js');


$script_vars = array('style' => $w,
				 'country_select' => $country->selector(0, true),
				 'program_select' => $program->selector(0, true)
				 );
wp_localize_script('cmd_schedule_adder', 'svars', $script_vars);

$itin_rows = 3;
$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, wp_help',
					   "toolbar2" => '');
$inty_settings = array();
			
$text_settings = array("media_buttons" => false,
					   "textarea_rows" => $itin_rows,
					   "teeny" => true,
					   "tinymce" => array(),
					   "quicktags" => false);


if(isset($_GET['save']) && $_GET['save']=='true')
{
	if($_GET['team_id']=='new')
	{
		//
		//Check schedule, make sure we have at least one item
		foreach($_POST as $id=>$value)
		{
			if(substr($id,0,8) == 'country_')
			{
				$actual_id = substr($id,8);
				$write_id = ($actual_id > 999999 ? 0 : $actual_id);
				$ctry = $_POST['country_' . $actual_id];	
				$regn = ((isset($_POST['region_' . $actual_id])) ? $_POST['region_' . $actual_id] : 0);
				$cost = 0;	
				
				if(!isset($_POST['program_' . $actual_id]) || $_POST['program_' . $actual_id] <1)
				{
					$error[] = "Must have a program associated with Schedule.";
				}
				if(!isset($_POST['start_' . $actual_id]) || empty($_POST['start_' . $actual_id]))
				{	
					$error[] = "Start Date must be set for Schedule";
				}
				if(!isset($_POST['end_' . $actual_id]) || empty($_POST['end_' . $actual_id]))
				{
					$error[] = "End Date must be set for Schedule";
				}

				break;
			}
		}


		//
		//hash and type are required, default if we don't have them
		$type = ((!empty($_POST['type'])) ? $_POST['type'] : 1);

		if(!empty($_POST['hash']))
		{
			$hash = $_POST['hash'];
			$codehash = false;

			//
			//Generate some error messages if we don't have a unique hash
			if(!$team->unique_code($hash))
			{
				$error[] = "Hash Tag already exists, please choose another";	
			}
		}
		else
		{
			$hash = rand(1000000000, 1000000000000);
			$codehash = true;
		}
		

		//
		//No errors, so check other data elements, and write them in if good
		if(empty($error))
		{
			$team->build(array('hash' => $hash,
							   'type' => $type,
							   'status' => 7,
							   'arrival_city' => $_POST['arrival_city'],
							   'arrival_country' => $_POST['arrival_country'],
							   'departure_city' => $_POST['departure_city'],
							   'departure_country' => $_POST['departure_country'],
							   'min' => $_POST['min'],
							   'max' => $_POST['max'],
							   'cost' => $_POST['cost']
							));

			$team_id = $team->get_team();
			$new_path = $page->GET_path(array('team_id','save'),array('team_id'=>$team_id));

			$team->save_blurb('teaser', isset($_POST['teaser']) ? $_POST['teaser'] : NULL);

			//
			//Insert Schedule
			foreach($_POST as $id=>$value)
			{
				if(substr($id,0,8) == 'country_')
				{
					$actual_id = substr($id,8);
					$write_id = ($actual_id > 999999 ? 0 : $actual_id);
					$cntry = $_POST['country_' . $actual_id];	
					$regn = ((isset($_POST['region_' . $actual_id])) ? $_POST['region_' . $actual_id] : 0);
					$prgm = $_POST['program_' . $actual_id];	
					$cost = 0;	
					$start = $_POST['start_' . $actual_id];	
					$end = $_POST['end_' . $actual_id];	

					$team->save_segment($write_id, $cntry, $regn, $prgm, $cost, $start, $end);
				}
			}

			if($codehash)
			{
				$team->update('hash', $team->value('code'));
			}

			// ***************
			//
			// Enter Chatterbox information
			global $wpdb;
			$cbox_fields = array('id' => $team_id, 'title' => $team->value('code'));
			$wpdb->insert($wpdb->prefix . 'chatterbox_teams', $cbox_fields);
			//
			//
			// ***************

			//End good data write and so we can move on and reload page with actual team id
			echo "<script>";
			echo "  window.location = '$new_path';";
			echo "</script>";

		}
		else
		{
			//Process Errors
			$err = 1;
		}
	}
	else
	{

		$team->save_blurb('teaser', isset($_POST['teaser']) ? $_POST['teaser'] : NULL);

		$save_fields = array ('hash', 'type', 'status', 'arrival_city', 'arrival_country', 'departure_city', 'departure_country', 'min', 'max', 'cost');
		
		foreach($save_fields as $field_name)
		{
			if(isset($_POST[$field_name]) && $_POST[$field_name] != $team->value($field_name))
			{
				$team->update($field_name, $_POST[$field_name]);
			}
		}

		//
		//Insert Schedule
		foreach($_POST as $id=>$value)
		{
			if(substr($id,0,8) == 'country_')
			{
				$actual_id = substr($id,8);
				$write_id = ($actual_id > 999999 ? 0 : $actual_id);
				$cntry = $_POST['country_' . $actual_id];	
				$regn = ((isset($_POST['region_' . $actual_id])) ? $_POST['region_' . $actual_id] : 0);	
				$prgm = $_POST['program_' . $actual_id];	
				$cost = 0;	
				$start = $_POST['start_' . $actual_id];	
				$end = $_POST['end_' . $actual_id];	

				if($cntry > 0 && $prgm > 0)
				{
					$team->save_segment($write_id, $cntry, $regn, $prgm, $cost, $start, $end);
				}
			}
		}

		//
		//Reload the Team data now that new data is saved
		$team = new TEAM();
		$team->set_team($_GET['team_id']);	
		$country = NEW COUNTRY();
		$program = NEW PROGRAM();	
	}
}

//
//Process errors for page or gather real data...
if(!empty($error) && $err==1)
{
	$errors = implode("<BR>",$error);
	$code = '';	
	$hash = ((isset($_POST['hash'])) ? $_POST['hash'] : '');		
	$type = ((isset($_POST['type'])) ? $_POST['type'] : 0);
	$status = ((isset($_POST['status'])) ? $_POST['status'] : 7);
	
	$arrival_city = ((isset($_POST['arrival_city'])) ? $_POST['arrival_city'] : '');
	$arrival_country = ((isset($_POST['arrival_country'])) ? $_POST['arrival_country'] : '');
	$departure_city = ((isset($_POST['departure_city'])) ? $_POST['departure_city'] : '');
	$departure_country = ((isset($_POST['departure_country'])) ? $_POST['departure_country'] : '');
	$min = ((isset($_POST['min'])) ? $_POST['min'] : '');
	$max = ((isset($_POST['max'])) ? $_POST['max'] : '');
	$cost = ((isset($_POST['cost'])) ? $_POST['cost'] : '');
	$teaser = ((isset($_POST['teaser'])) ? $_POST['teaser'] : '');

	//
	//Schedule
	$sched[0]['country'] = ((!empty($_POST['country_0'])) ? $_POST['country_0'] : 0);
	$sched[0]['region'] = ((!empty($_POST['region_0'])) ? $_POST['region_0'] : 0);
	$sched[0]['program'] = ((!empty($_POST['program_0'])) ? $_POST['program_0'] : 0);
	$sched[0]['start'] = ((!empty($_POST['start_0'])) ? $_POST['start_0'] : NULL);
	$sched[0]['end'] = ((!empty($_POST['end_0'])) ? $_POST['end_0'] : NULL);
}
else
{
	$code = $team->value('code');
	$hash = $team->value('hash');
	$type = $team->value('type');
	$status = $team->value('status');

	$arrival_city = $team->value('arrival_city');
	$arrival_country = $team->value('arrival_country');
	$departure_city = $team->value('departure_city');
	$departure_country = $team->value('departure_country');
	$min = $team->value('min');
	$max = $team->value('max');
	$cost = $team->value('cost');
	$teaser = $team->get_blurb('teaser');
	
	//
	//Schedule
	$current = $team->get_schedule();

	foreach($current as $segment_id => $segment_data)
	{
		$sched[$segment_id]['country'] = $segment_data['country'];
		$sched[$segment_id]['region'] = $segment_data['region'];
		$sched[$segment_id]['program'] = $segment_data['program'];
		$sched[$segment_id]['start'] = $segment_data['start_date'];
		$sched[$segment_id]['end'] = $segment_data['end_date'];
	}

}

?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">

<?php
if(!empty($error))
{
	echo "<div>";
	foreach($error as $message)
	{
		echo "&nbsp;&nbsp;&nbsp;<span style='color:red'>$message</span><BR>";		
	}
	echo "<HR>";
	echo "</div>";	
}
?>

<table >
	<TR>
		<TH scope="row"><label for="code">Team Code (automatic)</label></TH>
		<TD><input name="code" id="code" class="regular-text" type="text" value='<?php _e($code)?>' size='50' maxlength='50' disabled='true'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="hash">Hash Tag</label></TH>
		<TD><input name="hash" id="hash" class="regular-text" type="text" value='<?php _e($hash)?>' size='20' maxlength='20' placeholder='Leave blank to default to Team Code'/></TD>
	</TR>

	<TR>
   		<TH scope="row"><label for="hash">Schedule</label></TH>
		<TD>
		<table id="schedule_table">
            <TR><TH style='<?= $w?>'>Country</TH><TH>Region</TH><TH>Program</TH><TH>Start Date</TH><TH>End Date</TH></TR>
    
            <?php
			foreach($sched as $segment_id => $segment_data)
            {
                if(!empty($segment_data['country']))
                {
					$segment_save = true;
                    $country->set_country($segment_data['country']);	
                }
            ?>	
                <TR>
                    <TD>
						<input type='hidden' class='cnthold' name='key_<?php _e($segment_id)?>' id='key_<?php _e($segment_id)?>' value='<?php _e($segment_id)?>'>
                        <SELECT style='<?=$w?>' name="country_<?php _e($segment_id)?>" id="country_<?php _e($segment_id)?>" class="country">
                            <?php echo $country->selector($segment_data['country'],true) ?>
                        </SELECT>
                    </TD>
                    <TD>
                        <SELECT style='<?=$w?>' name="region_<?php _e($segment_id)?>" id="region_<?php _e($segment_id)?>" class="region">
                            <?php if($country->get_country()) { echo $country->region_selector($segment_data['region']); }?>
                        </SELECT>
                    </TD>
    
                    <TD>
                        <SELECT style='<?=$w?>' name="program_<?php _e($segment_id)?>" id="program_<?php _e($segment_id)?>" class="program">
                            <?php echo $program->selector($segment_data['program'], true)?>
                        </SELECT>
                    </TD>
    
                    <TD>
                        <input style='<?=$w?>' name="start_<?php _e($segment_id)?>" id="start_<?php _e($segment_id)?>" 
                               class="regular-text datepick" type="text" 
                               value='<?= isset($segment_data['start']) ? $segment_data['start'] : '' ?>'>
                    </TD>
                    
                    <TD><input style='<?=$w?>' name="end_<?php _e($segment_id)?>" id="end_<?php _e($segment_id)?>" 
                               class="regular-text datepick" type="text" 
                               value='<?= isset($segment_data['end']) ? $segment_data['end'] : '' ?>'>
                    </TD>
                    <TD>
                        <input type="button" name="delete_<?php _e($segment_id)?>" id="delete_<?php _e($segment_id)?>" 
                               class="button delete" value="DELETE">
                        <input type="button" name="add_<?php _e($segment_id)?>" id="add_<?php _e($segment_id)?>" 
                               class="button add_sched" value="ADD">
                    </TD>
                </TR>
                
                <?php
            }
            ?>
        </table>
	
	</TD></TR>    
	<TR><TD>&nbsp;</TD></TR>

	<TR>
		<TH><label for="type">Team Type</label></TH>
		<TD><SELECT name='type' id='type'><?php echo $support->type_selector($type, true) ?></SELECT></TD>
	</TR>
    <TR>    
		<TH><label for="status">Status</label></TH>
		<TD><SELECT name="status" id="status" <?php echo ($_GET['team_id']=='new' ? " disabled = 'true' " : "")?>>
			<?php echo $support->status_selector(
				($_GET['team_id']=='new' ? 7 : $status),false)
			 ?></SELECT></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="cost">Cost</label></TH>
		<TD><input name="cost" id="cost" class="regular-text" type="text" value='<?php echo $cost?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="arrival_country">Arrival Country</label></TH>
		<TD><input name="arrival_country" id="arrival_country" class="regular-text" type="text" value='<?php echo $arrival_country?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="arrival_city">Arrival City</label></TH>
		<TD><input name="arrival_city" id="arrival_city" class="regular-text" type="text" value='<?php echo $arrival_city?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="hash">Departure Country</label></TH>
		<TD><input name="departure_country" id="departure_country" class="regular-text" type="text" value='<?php echo $departure_country?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="departure_city">Departure City</label></TH>
		<TD><input name="departure_city" id="departure_city" class="regular-text" type="text" value='<?php echo $departure_city?>' size='40' maxlength='40'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="hash">Minimum Volunteers</label></TH>
		<TD><input name="min" id="min" class="regular-text" type="text" value='<?php echo $min?>' size='5' maxlength='5'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="max">Maximum Volunteers</label></TH>
		<TD><input name="max" id="max" class="regular-text" type="text" value='<?php echo $max?>' size='5' maxlength='5'/></TD>
	</TR>
	<TR>
		<TH scope="row"><label for="teaser">Team Goal</label></TH>
		<TD><?php wp_editor($teaser, "teaser", $text_settings)?></TD>
	</TR>
</table>

	<p class="submit">
		<button type="reset" name="reset" id="reset" class="button button-primary" value="RESET">RESET</button>
    	<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  />
    </p>

</form>

<form name="del_form" id="del_form" method="POST" enctype="multipart/form-data" 
      action="<?=$page->GET_path(array('save','delete')) . '&delete=true'?>">
      
	<input type="hidden" name="del_id" id="del_id" value="0">		      
</form>

<script>
	function delrec(record_id)
	{
		var id = document.getElementById('del_id');
		id.value = record_id;
		document.getElementById("del_form").submit();				
	}
</script>
