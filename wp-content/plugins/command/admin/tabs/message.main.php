<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "message.admin.class.php");
require_once (CC_CLASSES_DIR . "trigger.class.php");

$page = new TAB_MANAGE();
$admin_msg = new ADMIN_MESSAGES();

$messages = $admin_msg->get_all();

?>
<div class="page-header wrap">
	<h1>Messages</h1>
	<a class="page-title-action" href="<?php echo $page->GET_path(array(),array('msg_id' =>'new')); ?>">Add New</a>
</div>

<table id="messages_info" class="widefat fixed striped">
	<tr>
		<th>Message Action</th>
		<th>Message Subject</th>
		<th>Message Text</th>
		<TH>Notify</TH>
		<th></th>
	</tr>
	<?php 
	foreach($messages as $message) :
		$trigger = new TRIGGERS();
		$trigger->read($message->event);
		$event_title = $trigger->build_full_name(); ?>
	<tr>
		<td><?php echo $event_title; ?></td>
		<td><?php echo $message->subject; ?></td>
		<td><?php echo substr($message->message,0,200); ?></td>
		<td><?php echo ($message->notify == 0 ? "No" : "Yes"); ?></td>
		<td>
			<a href="<?php echo $page->GET_path(array(),array('msg_id' => $message->event)); ?>" >Edit Message</a>
		</td>
	</tr>
	<?php 
	endforeach; ?>
</table>
