<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

wp_enqueue_media();
wp_enqueue_script('cmd_media_uploader', plugins_url() . '/command/js/media.uploader.js');

if(isset($_POST) && isset($_POST['submit']) && $_POST['submit']=='Save Changes')
{
	if(isset($_POST['image_id']))
	{
		update_post_meta($team->value('post'), '_thumbnail_id', $_POST['image_id']);
	}
}

if(isset($_POST) && isset($_POST['delete']) && $_POST['delete']=='Delete Image')
{
	if(isset($_POST['image_id']))
	{
		delete_post_meta($team->value('post'), '_thumbnail_id');
	}
}

$image_id = get_post_meta($team->value('post'), '_thumbnail_id', true);
$image_url = get_post_field ('guid', $image_id, 'raw') 

?>

<p> 
<a href="#" id="insert-my-media" class="button">Add Featured Image</a>
</p>

<form name="picupload" id="picupload" method="POST" enctype = "multipart/form-data" action="<?php echo $page->GET_path(array('save'));?>">

	<input type="hidden" id="image_url" name="image_url" value="">
	<input type="hidden" id="image_id" name="image_id" value="">
	<img name="image_src" id='image_src' src='<?php _e(isset($image_url) ? $image_url : '')?>' alt='No Image Selected' height='400' border=1>

	<p class="submit">
    	<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	<input type="submit" name="delete" id="delete" class="button button-primary" value="Delete Image"  />
    </p>

</form>
