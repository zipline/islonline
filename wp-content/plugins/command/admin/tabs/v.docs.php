<?php
//
//Required classes
$plugin_path = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/command/classes/";
require_once ($plugin_path . "docs.class.php");
require_once ($plugin_path . "team.class.php");
require_once ($plugin_path . "rsvp.class.php");
require_once ($plugin_path . "message.admin.class.php");

wp_enqueue_script('cmd_document_signature', plugins_url() . '/command/js/doc.sig.js', array('jquery'));	
wp_localize_script('cmd_document_signature', 'svars', array('ajax' => admin_url('admin-ajax.php')));

$user_id = isset($_REQUEST['user_id'])? $_REQUEST['user_id'] : null;

$docs = new DOCS($user_id);
$team = new TEAM();
$rsvp = new RSVP();

$team->set_team($rsvp->team($_GET['rsvp']));
$docs->post_id($team->value('post'));
$docs->fetch();
$docs->fetch_read();

if(isset($_REQUEST['uncheck_docs']))
{
	$read_docs = $docs->list_read();
	foreach($read_docs as $d_key => $sig)
	{
		if(!isset($_REQUEST['doc_check_'.$d_key]) && !empty($d_key))
		{
			$event ="volunteer_document_unchecked";
			$admin_msg = new ADMIN_MESSAGES();
			$admin_msg->set_event($event);
			$admin_msg->load();
			$docs->remove_read_document($d_key);
			$message = new MESSAGES();
			$message->set_message_text($admin_msg->get_message());
			$message->set_recipient($user_id);
			$message->set_team_id($team->get_team());
			$message->set_sender(1);
			$message->add_message($event);
		}
	}
}

$docs->fetch();
$docs->fetch_read();
$docs->set_custom(true);

?>
    <div id="content">

		<form method="POST" action="<?php echo $page->GET_path(); ?>" >
		<?php
        foreach($docs->team_docs() as $doc_id => $doc_title)
        {
            $doc_type = get_post_meta($doc_id, 'resource_type', true);
			$signature = get_post_meta($doc_id, 'signature_required', true);
			$classname = '';
			if(empty($signature) || $signature == false)
			{
				$classname = "class='no_req_check' name='" . $doc_id . "'";
			}
			
            echo "<div class='document' id='" . $doc_id . "'>";
			echo "<input type='checkbox' name='doc_check_" . $doc_id . "' id='doc_check_" . $doc_id . "' " . ($docs->is_read($doc_id)  ? 'checked' : '') . ">&nbsp;&nbsp;";
			
			switch($doc_type)
			{
				case 'HTML Page':
					echo "<a href='' onclick=\"window.open('read-info-docs?doc_id=" . $doc_id . "&rsvp=" . $_GET['rsvp'] . "&post_id=" . $team->value('post') . "', 'newwindow', 'width=800', 'height=800')\" " . $classname . ">" . $doc_title . "</a>";
					break;
				case 'URL':
					$url = get_post_meta($doc_id, 'url', true);
					echo "<a href='" . $url . "' " . $classname . " id='doc_check_" . $doc_id . "' target='_blank'>" . $doc_title . "</a>";
					break;
				case 'PDF Document':
					$pid = get_post_meta($doc_id, 'pdf_document', true);
					echo "<a href='" . get_the_guid($pid) . "'  " . $classname . " target='_blank'>" . $doc_title . "</a>";
					break;
			}
			
            echo "</div><BR>\r\n";
    
        }
		echo "<input type='hidden' name='post_id' id='post_id' value='" . $team->value('post') . "'>";

        ?>
				<input type="submit" name="uncheck_docs" value="Update Volunteer's Doc List" class="button button-primary" />
			</form>
    </div> <!-- end #content --> 
 
 <?php wp_footer() ?>