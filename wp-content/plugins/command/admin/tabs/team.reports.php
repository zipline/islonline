<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');

$data['meds'] = array();
$data['diet'] = array();
$data['allergies'] = array();
$data['train'] = array();
$data['flight'] = array();
$data['border'] = array();

//
// Cycle through all team members and write up their stuff
foreach($rsvp->list_rsvp($team->get_team()) as $member_id)
{

	$rsvp->set_id_by_team_user($team->get_team(), $member_id);
	$rsvp_data = $rsvp->fetch_rsvp();
	$flight_info = $rsvp->get_flight_info();
	$flight_fees = $rsvp->get_flight_fees();

	$post_id = $rsvp_data['post_id'];
	$user_info = get_userdata($member_id);

	$fields = get_fields($post_id);
//	if ($member_id == 33642){
//	    echo '_'.$post_id;
//        echo '<pre>';
//        print_r($fields);
//        die;
//    }


	//
	//Fix up medications_taken field
	if(!empty($fields['medications_taken']))
	{
		$data['meds'][$member_id] = str_replace(array("<p>", "</p>"), '', $fields['medications_taken']);
	}
	else
	{
		$data['meds'][$member_id] = 'N/A';
	}
	
	//
	//Fix up dieteary_restrictions field
	if(!empty($fields['dietary_restrictions']))
	{
		$data['diet'][$member_id] = str_replace(array("<p>", "</p>"), '', $fields['dietary_restrictions']);
	}
	else
	{
		$data['diet'][$member_id] = 'N/A';
	}

	//
	//Fix up allergies field
	if(!empty($fields['food_allergies']))
	{
		$data['allergies'][$member_id] = str_replace(array("<p>", "</p>"), '', $fields['food_allergies']);
	}
	else
	{
		$data['allergies'][$member_id] = 'N/A';
	}

	//
	//Fix up professional_experience field
	if(!empty($fields['professional_experience']))
	{
		$data['train'][$member_id] = str_replace(array("<p>", "</p>"), '', $fields['professional_experience']);
	}
	else
	{
		$data['train'][$member_id] = 'N/A';
	}

	if(!empty($flight_info))
	{
//ho var_dump($flight_info);
		$incoming = '';
		$departing = '';
		
		foreach($flight_info as $segment => $flight_data)
		{
			if(isset($flight_data['direction']) && $flight_data['direction'] == 0)
			{
				if(isset($flight_data['airline']) && isset($flight_data['flight']) && !empty($flight_data['airline']))
				{
					$incoming .= 'Arriving in <B>' . $flight_data['arrive'] . '</B>, on <B>' . $flight_data['airline'] . '</B> flight: ' . $flight_data['flight'] . ' at ' . $flight_data['atime'] . ' on ' . $flight_data['adate'] . "<BR>";
				}
			}

			if(isset($flight_data['direction']) && $flight_data['direction'] ==1)
			{
				if(isset($flight_data['airline']) && isset($flight_data['flight']) && !empty($flight_data['airline']))
				{
					$departing .= 'Departing <B>' . $flight_data['depart'] . '</B> on <B>' . $flight_data['airline'] . '</B> flight: ' . $flight_data['flight'] . ' at ' . $flight_data['dtime'] . ' ' . $flight_data['ddate'] . '<BR>';
				}
			}
		
		}
		
		if(!empty($incoming) || !empty($departing))
		{
			$data['flight'][$member_id] = '<div style="display: inline-block; vertical-align: top; margin-left:10px;">' . $incoming . '<BR>' . $departing . '</div>';
		}
		else
		{
			$data['flight'][$member_id] = 'Unanswered';
		}

	}
	else
	{
		$data['flight'][$member_id] = 'Unanswered';
	}
	
	if(isset($flight_fees['arrive']) && $flight_fees['arrive'] == "checked")
	{
		$data['flight'][$member_id] .= '<BR><div style="display: inline-block; vertical-align: top; margin-left:10px;">Arriving Late or Early</div>';
	}
	if(isset($flight_fees['depart']) && $flight_fees['depart'] == "checked")
	{
		$data['flight'][$member_id] .= '<BR><div style="display: inline-block; vertical-align: top; margin-left:10px;">Departing Late or Early</div>';
	}
		
	$number = get_post_meta($post_id, '_cmd_passport_num', true);
	$country = get_post_meta($post_id, '_cmd_passport_country', true);
	$issue = get_post_meta($post_id, '_cmd_passport_issue', true);
	$expire = get_post_meta($post_id, '_cmd_passport_expire', true);

	if(!empty($number) && !empty($country))
	{
		$data['border'][$member_id] = '<div style="display: inline-block; vertical-align: top; margin-left:10px">' . $country . ': ' . $number . '.<BR>Issued: ' . $issue . '<BR>Expires: ' . $expire . '</div>';
	}
	else
	{
		$data['border'][$member_id] = '';
	}
	
	$data[$member_id] = ($member_id > 0) ? $user_info->first_name . " " . $user_info->last_name : 'Unassigned';

	empty($data['border'][$member_id]) ? $data['border'][$member_id] = 'Unanswered' : '';

}
?>

<div class="print-section">
    <h3>Medications</h3>
    <ul class="team-info-wrapper for-print">
        <?php
        foreach($data['meds'] as $index=>$value) {
            echo "<li><strong>" . $data[$index] . "</strong>: " . $value . "</li>";
        }
        ?>
    </ul>
</div>

<div class="print-section">
    <h3>Dietary Requirements</h3>
    <ul class="team-info-wrapper for-print">
        <?php
        foreach($data['diet'] as $index=>$value) {
            echo "<li><strong>" . $data[$index] . "</strong>: " . $value . "</li>";
        }
        ?>
    </ul>
</div>

<div class="print-section">
    <h3>Food Allergies</h3>
    <ul class="team-info-wrapper for-print">
        <?php
        foreach($data['allergies'] as $index=>$value) {
            echo "<li><strong>" . $data[$index] . "</strong>: " . $value . "</li>";
        }
        ?>
    </ul>
</div>

<div class="print-section">
    <h3>Specialized Training</h3>
    <ul class="team-info-wrapper for-print">
        <?php
        foreach($data['train'] as $index=>$value) {
            echo "<li><strong>" . $data[$index] . "</strong>: " . $value . "</li>";
        }
        ?>
    </ul>
</div>

<div class="print-section">
    <h3>Flight Information</h3>
    <ul class="team-info-wrapper for-print">
        <?php
        foreach($data['flight'] as $index=>$value) {
            echo "<li><strong>" . $data[$index] . "</strong>: " . $value . "</li>";
        }
        ?>
    </ul>
</div>

<div class="print-section">
    <h3>Passport Information</h3>
    <ul class="team-info-wrapper for-print">
        <?php
        foreach($data['border'] as $index=>$value) {
            echo "<li><strong>" . $data[$index] . "</strong>: " . $value . "</li>";
        }
        ?>
    </ul>
</div>