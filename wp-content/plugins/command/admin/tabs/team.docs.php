<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "docs.class.php");
$docs = new DOCS();
$docs->post_id($team->value('post'));

//
//Save data if there's data to save
if(isset($_GET['save']) && $_GET['save']=='true')
{
	$sel_docs = array();
	foreach($_POST as $id=>$value)
	{
		if(substr($id,0,4) == "doc_")
		{
			$sel_docs[] = substr($id,4);
		}
	}

	$curr_docs = $docs->get_team_list();

	$notify = false;
	foreach ( $sel_docs as $doc ) {
		if ( ! in_array( $doc, $curr_docs ) ) {
			$notify = true;
		}
	}

	// Check if adding a document instead of removing
	// Add new documents

	if(isset($sel_docs) && count($sel_docs)>0)
	{
		$docs->set_team_list($sel_docs);
	}
	else
	{
		$docs->clear_team_list();
	}

	if($team->value('messaging')==1 && $notify)
	{
		do_action('cmd_queue_trigger',"Team Document Added",array(),array('team_id' => $_GET['team_id']));
	}

}

$docs->set_teams(array($team->get_team()));
$docs->set_countries($team->schedule_countries());
$docs->set_programs($team->schedule_programs());
$docs->fetch();

?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
    <table class="form-table team-info documents">
        <tr>
            <td class="label">
                <label for="current">Current Documents</label>
            </td>
			<td>
            	<?php
				foreach($docs->team_docs() as $doc_id => $doc_title)
				{
                    echo "<p>";
					echo "<input type='checkbox' class='cur' name='doc_" . $doc_id . "' id='doc_" . $doc_id . "' checked>" . $doc_title . "<BR>";
                    echo "</p>";
				}
				?>
			</td>
        </tr>
        <tr>
            <td class="label">
                <label for="available">Available Documents</label>
            </td>
            <td>
            	<?php
				foreach($docs->missing_docs() as $doc_id => $doc_title)
				{
                    echo "<p>";
					echo "<input type='checkbox' class='avail' name='doc_" . $doc_id . "' id='doc_" . $doc_id . "'>" . $doc_title . "<BR>";
                    echo "</p>";
				}
				?>
			</td>
        </tr>
    </table>
    
	<p class="submit">
		<button type="reset" name="reset" id="reset" class="button button-primary" value="RESET">RESET</button>
    	<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  />
    </p>
</form>
