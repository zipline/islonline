<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');
$who = wp_get_current_user();

echo "<div id='content' class='team-chat'>";
	do_shortcode('[chatterbox_chat team_id=' . $_GET['team_id'] . ' user_id=' . $who->ID . ' private=false]');
echo "</div>";