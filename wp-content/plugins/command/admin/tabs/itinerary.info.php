<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

$countries = $country->list_countries();
$programs = $program->list_programs();

$itemcols = 15;
$itin_rows = 5;
$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, wp_help',
					   "toolbar2" => '');
			
$text_settings = array("media_buttons" => false,
					   "textarea_rows" => $itin_rows,
					   "teeny" => true,
					   "tinymce" => $tiny_settings,
					   "quicktags" => false);


function enqueue_itinerary_mods()
{
	wp_enqueue_script('cmd_itinerary_mods', plugins_url() . '/command/js/itinerary.build.js');	
}
add_action ('admin_footer', 'enqueue_itinerary_mods');

//
//Saving NOW!
if(isset($_GET['save']) && $_GET['save']=='true')
{
	$itin_id = $_GET['itin_id'];

	//
	//Required to have an Itinerary
	if(isset($_POST['itin']))
	{
		//
		//Collect all countries and programs
		$cnt_list = array();
		$pgm_list = array();
		foreach($_POST as $label=>$value)
		{
			if(substr($label,0,4) == "cty_")
			{
				array_push($cnt_list, substr($label, 4));		
			}
			if(substr($label,0,4) == "pgm_")
			{
				array_push($pgm_list, substr($label, 4));		
			}
		}
		
		//
		//Header and Footer
		$header = (isset($_POST['header']) && $_POST != "Header") ? $_POST['header'] : NULL;
		$footer = (isset($_POST['footer']) && $_POST != "Footer") ? $_POST['footer'] : NULL;

		if(!$itinerary->build($_POST['itin'], $header, $footer, implode(",",$cnt_list), implode(",",$pgm_list)))
		{
			$error = "Failed to save";
		}
		
		$itin_id = $itinerary->get_itinerary();
		
		//
		//If we still have a proper itinerary, then save the days
		if(isset($itin_id) && $itin_id>0)
		{
			$l_items = array();
			foreach($_POST as $label=>$value)
			{
				//
				//Gather all the temporary id's from the build
				if(strlen($label) >=6 && substr($label,0,5) == "desc_")
				{
					array_push($l_items, substr($label,5));		
				}
				
				//
				//Get all the id's in order
				asort($l_items);
				
				//
				//Build the array of information
				$itinerary->clear_details();
				$day = 1;
				foreach($l_items as $post_id)
				{
					$title = ((isset($_POST['titl_' . $post_id])) ? $_POST['titl_' . $post_id] : "Day: " . $day);
					$desc = ((isset($_POST['desc_' . $post_id])) ? $_POST['desc_' . $post_id] : '');
			
					$itinerary->add_detail($day, $title,$desc);
					$day++;		
				}
				
				//
				//Save the data
				$itinerary->build_details();				
			}
		}
		
		//
		//Saved, so reload with proper id now
		$new_path = $page->GET_path(array('itin_id','save'),array('itin_id'=>$itin_id));
		echo "<script>";
		echo "  window.location = '$new_path';";
		echo "</script>";
	}
}


?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<input type="hidden" name="itin" id="itin" value="<?php _e($name)?>">
<table class="form-table">
	<TR>
    	<TD><strong>Header</strong><BR>
			<?php wp_editor($itinerary->header(), "header", $text_settings)?>
        </TD>
    </TR>
	<TR>
    	<TD><strong>Countries</strong>
        	<table>
            	<?php
					$cnt=1;
					foreach($countries as $id=>$data)
					{
						echo $cnt == 1 ? "<TR>" : '';
						echo "<TD>
								<input type='checkbox' 
									   name='cty_" . $id . "' 
									   id='cty_" . $id . "'
									   " . ($itinerary->in_countries($id) ? " checked" : '') . " 
									   >" . $data['code'] . 
							 "</TD>";						
						
						$cnt == $itemcols ? "</TR>" : '';
						$cnt == $itemcols ? $cnt=1 : $cnt++;	
					}
				?>
        	</table>
        </TD>
    </TR>
	<TR>
        <TD><strong>Programs</strong>
        	<table>
            	<?php
					$cnt=1;
					foreach($programs as $id=>$data)
					{
						echo $cnt == 1 ? "<TR>" : '';
						echo "<TD>
								<input type='checkbox' 
									   name='pgm_" . $id . "' 
									   id='pgm_" . $id . "'
									   " . ($itinerary->in_programs($id) ? " checked" : '') . " 
									   >" . $data['code'] . 
							 "</TD>";						
						
						$cnt == $itemcols ? "</TR>" : '';
						$cnt == $itemcols ? $cnt=1 : $cnt++;	
					}
				?>
        	</table>
        </TD>
    </TR>
	<TR>
        <TD><strong>Itinerary</strong>
			<table id="itinerary_table">	
				<?php
					$details = $itinerary->details();
					
					if(count($details)>0)
					{
						foreach($details as $id=>$data)
						{
							$counter = $data['day'] * 10000;
						?>
                            <TR>
                                <TD align="center">
                                    <input type="button" class="button addprior" value="Add Prior">
                                    <BR><BR>
                                    <input type="button" class="button delete" value="Delete">
                                    <BR><BR>
                                    <input type="button" class="button addafter" value="Add After">
                                </TD>
                                <TD><strong>Day: </strong>
                                		<span class="dayholder" name="<?php _e($counter)?>">
											<?php _e($data['day'])?>
                                        </span>
                                </TD>
                                <TD><textarea cols=30 rows=<?php _e($itin_rows+3)?> placeholder="title"
                                            name="titl_<?php _e($counter)?>" id="titl_<?php _e($counter)?>"><?php _e($data['title'])?></textarea>
                                </TD>
                                <TD><?php wp_editor(stripslashes($data['description']), "desc_" . $counter, $text_settings)?>
                                </TD>
                            </TR>    
						<?php							
						}
					}
					else 
					{
					?>	
                <TR>
                    <TD align="center">
                    	<input type="button" class="button addprior" value="Add Prior">
                        <BR><BR>
                        <input type="button" class="button delete" value="Delete">
                        <BR><BR>
                        <input type="button" class="button addafter" value="Add After">
                    </TD>
                	<TD><strong>Day: </strong><span class="dayholder" name="10000">1</span></TD>
                	<TD><textarea cols=30 rows=<?php _e($itin_rows+3)?> placeholder="title"
                    			name="titl_10000" id="titl_10000"></textarea></TD>
	                <TD><?php wp_editor("Description", "desc_10000", $text_settings)?>
                    </TD>
                </TR>    
					<?php
					}
					?>
            </table>
        </TD>
    </TR>
	<TR>
    	<TD><strong>Footer</strong><BR>
			<?php wp_editor($itinerary->footer(), "footer", $text_settings)?>
        </TD>
    </TR>
</table>

<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>
</form>

