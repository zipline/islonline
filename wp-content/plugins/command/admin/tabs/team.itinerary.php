<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "itinerary.class.php");
$itinerary = new ITINERARY();


//
//Building from template
if(isset($_POST['build']) && $_POST['build']>0)
{
	$itinerary->set_team($_GET['team_id']);
	$itinerary->set_itinerary($_POST['build']);
	$itinerary->apply_template();
	
	unset($_GET['save']);
}


//
//Put the class into production mode and set basic data
$itinerary->production(true);
$itinerary->set_team($_GET['team_id']);


//
//Saving NOW!
if(isset($_GET['save']) && $_GET['save']=='true')
{
	//
	//Header and Footer
	$header = (isset($_POST['header']) && $_POST['header'] != "Header") ? $_POST['header'] : NULL;
	$footer = (isset($_POST['footer']) && $_POST['footer'] != "Footer") ? $_POST['footer'] : NULL;

	$l_items = array();
	foreach($_POST as $label=>$value)
	{
		//
		//Gather all the temporary id's from the build
		if(strlen($label) >=6 && substr($label,0,5) == "desc_")
		{
			$l_items[] = substr($label,5);		
		}
	}
	
	//
	//Get all the id's in order
	asort($l_items);
	
	//
	//Build the array of information
	$itinerary->clear_details();
	$day = 1;
	foreach($l_items as $post_id)
	{
		$title = ((isset($_POST['titl_' . $post_id])) ? $_POST['titl_' . $post_id] : "Day: " . $day);
		$desc = ((isset($_POST['desc_' . $post_id])) ? $_POST['desc_' . $post_id] : '');

		$itinerary->add_detail($day, $title,$desc);
		$day++;		
	}
	
	//
	//Save the data
	$itinerary->add_detail(10000, 'header', $header);
	$itinerary->add_detail(0, 'footer', $footer);
	$itinerary->build_details();				

	if(isset($_POST['is_public']) && $_POST['is_public'] == 'on')
	{
		$itinerary->show_public("true");
	}
	else
	{
		$itinerary->show_public("false");
	}
	
	//
	//Saved, so reload with proper id now
	$new_path = $page->GET_path(array('team_id','save'),array('team_id'=>$_GET['team_id']));
	echo "<script>";
	echo "  window.location = '$new_path';";
	echo "</script>";
}

$itin_rows = 5;
$tiny_settings = array("toolbar1" => 'alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, bullist, numlist, link, unlink, removeformat, wp_help',
					   "toolbar2" => '');
			
$text_settings = array("media_buttons" => false,
					   "textarea_rows" => $itin_rows,
					   "teeny" => true,
					   "tinymce" => $tiny_settings,
					   "quicktags" => false);

function enqueue_itinerary_mods()
{
	wp_enqueue_script('cmd_itinerary_mods', plugins_url() . '/command/js/itinerary.build.js');	
}
add_action ('admin_footer', 'enqueue_itinerary_mods');

?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<table class="form-table team-info">
	<TR>
    	<TD><strong>Reset Itinerary to:</strong>
        <SELECT name="build_select" id="build_select"><?php echo $itinerary->selector(0, true)?></SELECT>
        <input type="button" name="bld" id="bld" onclick="javascript:rebuild()" 
        	   class="button button-primary" value="Go!"  />
        </TD>
		<!-- 2018-03-11 DJT Added print function -->
  		<TD>&nbsp;</TD>
   		<TD>
	    	<a class='page-title-action button' href='javascript:window.open("/itinerary?team_id=<?php echo $_GET['team_id']?>&admino=admin_over_print&rsvp=0", "Itinerary", "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");'>Print</a>
		</TD>
    </TR>
    <TR>
		<TD>Show public view: 
		<input type="checkbox" name="is_public" id="is_public" <?php echo $itinerary->show_public() ? "checked" : "";?>>
		</TD>
	<TR>
    	<TD><strong>Header</strong><BR>
			<?php wp_editor(stripslashes($itinerary->header()), "header", $text_settings)?>
        </TD>
    </TR>
	<TR>
        <TD><strong>Itinerary</strong>
			<table id="itinerary_table">	
				<?php
					$details = $itinerary->details();
					
					if(count($details)>0)
					{
						foreach($details as $id=>$data)
						{
							$counter = $data['day'] * 10000;
						?>
                            <TR>
                                <TD align="center">
                                    <input type="button" class="button addprior" value="Add Prior">
                                    <BR><BR>
                                    <input type="button" class="button delete" value="Delete">
                                    <BR><BR>
                                    <input type="button" class="button addafter" value="Add After">
                                </TD>
                                <TD><strong>Day: </strong>
                                		<span class="dayholder" name="<?php _e($counter)?>">
											<?php _e($data['day'])?>
                                        </span>
                                
                                		<BR>
                                        
                                    <strong></strong>    
                                </TD>
                                <TD><textarea cols=100 rows=1 placeholder="Title for this item (optional)"
                                            name="titl_<?php _e($counter)?>" id="titl_<?php _e($counter)?>" style="resize: none;"><?php echo isset($data['title']) ? $data['title'] : '';?></textarea>
                                	<BR>
                                	<?php wp_editor(stripslashes($data['description']), "desc_" . $counter, $text_settings)?>
                                </TD>
                            </TR>    
						<?php							
						}
					}
					else 
					{
					?>	
                <TR>
                    <TD align="center">
                    	<input type="button" class="button addprior" value="Add Prior">
                        <BR><BR>
                        <input type="button" class="button delete" value="Delete">
                        <BR><BR>
                        <input type="button" class="button addafter" value="Add After">
                    </TD>
                	<TD><strong>Day: </strong><span class="dayholder" name="10000">1</span></TD>
                	<TD><textarea cols=100 rows=1 placeholder="title"
                    			name="titl_10000" id="titl_10000"></textarea>
                    	<BR>
	                	<?php wp_editor("Description", "desc_10000", $text_settings)?>
                    </TD>
                </TR>    
					<?php
					}
					?>
            </table>
        </TD>
    </TR>
	<TR>
    	<TD><strong>Footer</strong><BR>
			<?php wp_editor(stripslashes($itinerary->footer()), "footer", $text_settings)?>
        </TD>
    </TR>
</table>

<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>
</form>

<form name="bld_new" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('build'))?>">
<input type="hidden" name="build" id="build" value="0">
</form>

<script>

	function rebuild()
	{
		var bld_id = document.getElementById('build_select');
		var build = document.getElementById('build');
		build.value = bld_id.value;
		document.forms.bld_new.submit();
	}
	
	
</script>

