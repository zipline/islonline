<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//$program = new PROGRAM();
$all_programs = $program->list_programs();
$cur_program = $program->details();


if(isset($_GET['save']) && $_GET['save']=='true')
{

	if($_GET['program_id']=='new')
	{
		if(isset($_POST['title']) && isset($_POST['code']))
		{
			if(strlen($_POST['title'])>3 && strlen($_POST['code'])>1)
			{
				$program->build($_POST['title'],$_POST['code']);	
				$new_path = $page->GET_path(array('program_id','save'),array('program_id'=>$program->get_program()));
				echo "<script>";
				echo "  window.location = '$new_path';";
				echo "</script>";
			}
		}

	}
	else
	{
		if($_POST['title'] != $cur_program['title']) { $program->save_info('title',$_POST['title']); }
		if($_POST['code'] != $cur_program['code']) { $program->save_info('kode',$_POST['code']); }
		//if($_POST['sequence'] != $cur_program['sequence']) { $program->save_info('sequence',$_POST['sequence']); }
	
		if(isset($_POST['exprel']) && $_POST['exprel']=='on')
		{
			if($cur_program['exp_related']!=1)
			{
				$program->save_info('exp_related',1);
			}
		}
		else
		{
			if($cur_program['exp_related']!=0)
			{
				$program->save_info('exp_related',0);
			}
		}
	
		if(isset($_POST['active']) && $_POST['active']=='on')
		{
			if($cur_program['active']!=1)
			{
				$program->save_info('active',1);
			}
		}
		else
		{
			if($cur_program['active']!=0)
			{
				$program->save_info('active',0);
			}
		}
	
		$program_id = $program->get_program();
		$program = new PROGRAM();
		$program->set_program($program_id);
	
		$all_programs = $program->list_programs();
		$cur_program = $program->details();
	}
}

?>	
<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<table class="form-table program-table">
	<TR>
    	<TH scopt="row"><label for="title">Title</label></TH>
        <TD><input name="title" id="title" type="text" class="regular-text" value="<?=$cur_program['title']?>" /></TD>
    </TR>
	<TR>
    	<TH scopt="row"><label for="code">Code</label></TH>
        <TD><input name="code" id="code" type="text" class="regular-text" value="<?=$cur_program['code']?>" /></TD>
    </TR>
	<TR>
    	<TH scopt="row"><label for="active">Active <span>note: program will only activate if there is content entered for the page</span></label></TH>
        <TD><input name="active" id="active" type="checkbox" <?php echo ($cur_program['active'] == 1 ? "checked" : "")?> /></TD>
    </TR>
</table>

<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>
</form>

