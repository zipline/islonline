<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');
$post_id = $country->post_id();

//
//Check to see if the page exists...
if(!isset($post_id) || !$post_id)
{
	$post_title = "Country " . $country->get_name();
	$post_name = strtolower(str_replace(' ', '_', $post_title));

	$field_data=array(	'post_author' => 1,
						'post_date' => date('Y-m-d H:i:s'),
						'post_date_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
						'post_content' => 'Never to be seen',
						'post_title' => $post_title,
						'post_status' => 'draft',
						'comment_status' => 'closed',
						'ping_status' => 'closed',
						'post_name' => $post_name,
						'post_modified' => date('Y-m-d H:i:s'),
						'post_modified_gmt' => get_gmt_from_date(date('Y-m-d H:i:s')),
						'post_parent' => 4015,
						'menu_order' => 0,
						'post_type' => 'page',
						'page_template' => 'country.php',
						'comment_count' => 0
					);
	$post_id = wp_insert_post($field_data);
	
	//
	//Update country record
	$country->post_id($post_id, true);	
}

$page_path = admin_url() . 'post.php?post=' . $post_id . '&action=edit';

echo "<script>";
echo "  window.location = '$page_path';";
echo "</script>";

?>