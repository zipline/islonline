<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');


if(isset($_GET['save']) && $_GET['save']=='true')
{
	$team->save_blurb('planning', isset($_POST['planning']) ? $_POST['planning'] : NULL);
}

$itin_rows = 20;
$tiny_settings = array("toolbar1" => 'formatselect, alignleft, aligncenter, alignright, indent, outdent, bold, italic, underline, strikethrough, superscript, subscript, bullist, numlist, link, unlink, wp_help',
					   "toolbar2" => '');
$inty_settings = array();
			
$text_settings = array("media_buttons" => false,
					   "textarea_rows" => $itin_rows,
					   "teeny" => true,
					   "tinymce" => array(),
					   "quicktags" => false);

$planning = $team->get_blurb('planning');


?>

<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
<input type="text" name="subject" placeholder="Message Subject" />

<div style="margin-right: 15px; margin-top: 15px;">
	<?php wp_editor($planning, "planning", $text_settings)?>
    
    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  />
    </p>
</div>
</form>



