<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

include_once(CC_CLASSES_DIR . 'user.class.php');
include_once(CC_CLASSES_DIR . 'team.class.php');

//
//Save the data, or reset the fields if necessary.
if(isset($_GET['save']) && $_GET['save']=='true')
{

	if(!empty($_REQUEST['add_ons']))
	{
		$adds = array();
		foreach($_REQUEST['add_ons'] as $add_id)
		{
			if(!empty($_REQUEST['cost'][$add_id]))
			{
				$adds[$add_id] = $_REQUEST['cost'][$add_id];	
			}
		}
		if(!empty($adds))
		{
			update_post_meta($team->value('post'), "_cmd_add_ons", $adds); 		
		}
		else
		{
			delete_post_meta($team->value('post'), "_cmd_add_ons");
		}
	}
	else
	{
		delete_post_meta($team->value('post'), "_cmd_add_ons");
	}
}

$set_adds = get_post_meta($team->value('post'), "_cmd_add_ons");

?>

<form name="data" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path(array('save')) . '&save=true'?>">
    <table class="form-table team-info">
		<tr>
        	<th>Active</th>
            <th colspan="2">Add-on</th>
            <th colspan="4">Actual Cost</th>
		</tr>
        
		<?php 
        $loop = new WP_Query( array( 'post_type' => 'team_add_ons', 'posts_per_page' => 100 ) );
        
        while ( $loop->have_posts() )
        {
            $loop->the_post();
			$base_cost = get_field('cost', false, false);
        
			echo "<tr>";
            echo '<td><input type="checkbox" name="add_ons[]" id="add_on_' . get_the_ID() . '" value="' . get_the_ID() . '"' . (!empty($set_adds[0][get_the_ID()]) ? ' checked' : '') . '></TD>';
            echo '<td colspan="2"><a href="/wp-admin/post.php?post=' . get_the_ID() . '&action=edit" rel="bookmark" target="_blank">' . the_title_attribute( 'echo=0' ) . '</a></td>';
            echo '<td colspan="4">$<input type="text" name="cost[' . get_the_ID() . ']" id="cost_' . get_the_ID() . '" length="9" value="' . (!empty($set_adds[0][get_the_ID()]) ? number_format($set_adds[0][get_the_ID()],2) : '') . '" placeholder="' . $base_cost . '"></td>';
			echo "</tr>";
        }
        ?>
	    
        <tr>
        	<td colspan='3'><input type="submit" name="save" value="Update Team Add-ons" class="button button-primary"></td>
        </tr>    
	</table>
</form>



