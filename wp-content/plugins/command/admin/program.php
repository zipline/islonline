<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "program.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");

$program = new PROGRAM();
$page = new TAB_MANAGE();

//
//Deactivate any Program that does not have a published POST
foreach($program->list_programs(true) as $program_id=>$program_data)
{
	if(empty($program_data['post']) || get_post_status($program_data['post']) != "publish")
	{
		$program->set_program($program_id);
		$program->save_info('active', 0);
	}
}


//
//Divide the page into two options, Detail and Listing
if(isset($_GET['program_id']) && ($program->is_program($_GET['program_id']) || $_GET['program_id']=='new'))
{
	//
	//Program Detail section
	if($_GET['program_id']=='new')
	{
		$page->SetTabs(array('pinfo' => 'Program Info'));
	}
	else 
	{
		$program->set_program($_GET['program_id']);
		$page->SetTabs(array('pinfo' => 'Program Info', 'pcont' => 'Public Website Page'));
	}
	$page->SetTabName('cur_tab');
	$page->SetCurrent(isset($_GET[$page->GetTabName()]) ? $_GET[$page->GetTabName()] : 'pinfo');

	//
	//Write in Select field
	?>
	<br>
    <h1>
     	<A href='<?=$page->GET_path(array('program_id','save'))?>'><img height='20' src=site_url() . '/wp-content/uploads/2015/09/left_arrow.png'></A>
		Program: <SELECT name="program" id="program" onChange="javascript:change_program()">
				<?php echo $program->selector($_GET['program_id']) ?>
                <?php if($_GET['program_id']=='new') { echo "<OPTION value='new' selected> --> Add New </OPTION>"; }?>
				</SELECT>
    </H1>
	<br>
	<?php

	$page->ShowTabs(array('save'));

	//
	//Deal with tabs
	switch ($page->GetCurrent())
	{
		case 'pinfo':
			include (CC_ADMIN_DIR . "tabs/program.info.php");
			break;	
		case 'pcont':
			include (CC_ADMIN_DIR . "tabs/program.cont.php");
			break;
		default:
			break;	
	}

	?>
		<script>
        function change_program()
        {
            var url;
            url = '<?=$page->GET_path(array('program_id','save'))?>' + '&program_id=' + document.getElementById('program').value;
            window.location.href = url;
        }
        </script>
	<?php
}
else
{
	$program = new PROGRAM();

	echo "<div class='header-wrapper wrap'><h1>Programs" . str_repeat('&nbsp;',6) . 
		"<a href='" . $page->GET_path(array('program_id','save'),array('program_id'=>'new')) . "' class='page-title-action'>Add New</a></h1></div>";
	echo "<table class='wp-list-table widefat fixed striped'>";
	echo "<thead>";
	echo "	<tr>";
	echo "		<th scope='col' class='manage-column'>Code</th>";
	echo "		<th scope='col' class='manage-column'>Program</th>";
	echo "		<th scope='col' class='manage-column'>Type</th>";
	echo "  </tr>";
	echo "</thead>";
	
	$base_path = $_SERVER['REQUEST_URI'];

	foreach($program->list_programs() as $program_id=>$program_data)
	{
		$active_progs[] = $program_id;
		
		echo "<TR>";
		echo "  <TD>" . $program_data['code'] . "</TD>";
		echo "  <TD><a href='" . $base_path . "&program_id=" . $program_id . "'>" . $program_data['title'] . "</a></TD>";
		echo "  <TD>" . $program_data['type_title'] .  "</TD>";
	}
	echo "</table>";
	
	echo "<HR />";
	echo "<BR>";
	

	$program = new PROGRAM();

	echo "<table class='wp-list-table widefat fixed striped'>";
	echo "<thead>";
	echo "	<tr>";
	echo "		<th colspan=3 class='manage-column'><B>Inactive Programs</B></th>";
	echo "  </tr>";
	echo "	<tr>";
	echo "		<th scope='col' class='manage-column'>Code</th>";
	echo "		<th scope='col' class='manage-column'>Program</th>";
	echo "		<th scope='col' class='manage-column'>Type</th>";
	echo "  </tr>";
	echo "</thead>";

	foreach($program->list_programs(true) as $program_id=>$program_data)
	{
		if(!in_array($program_id, $active_progs))
		{
			echo "<TR>";
			echo "  <TD>" . $program_data['code'] . "</TD>";
			echo "  <TD><a href='" . $base_path . "&program_id=" . $program_id . "'>" . $program_data['title'] . "</a></TD>";
			echo "  <TD>" . $program_data['type_title'] .  "</TD>";
		}
	}


}

?>