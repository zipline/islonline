<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "message.admin.class.php");

$page = new TAB_MANAGE();
$admin_msg = new ADMIN_MESSAGES();

if(!isset($_REQUEST['msg_id']))
{
	$messages = $admin_msg->get_all();
	include (CC_ADMIN_DIR . "tabs/message.main.php");
}
else if(isset($_REQUEST['msg_id']) && $_REQUEST['msg_id'] == "new")
{
	include (CC_ADMIN_DIR . "tabs/message.create.php");
}
else if(isset($_REQUEST['msg_id']))
{
	include (CC_ADMIN_DIR . "tabs/message.edit.php");
}
?>