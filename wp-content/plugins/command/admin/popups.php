<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

if(isset($_REQUEST['display']))
{
	switch ($_REQUEST['display'])
	{
		case 'finance':
			require_once CC_ADMIN_DIR . "/pop/team_finance.php";
			break;
		case 'invoice':
			require_once CC_ADMIN_DIR . "/pop/team_finance.php";
			break;
		case 'poc_invoice':
			require_once CC_ADMIN_DIR . "/pop/team_poc_finance.php";
			break;
		case 'refund':
			require_once CC_ADMIN_DIR . "/pop/team_refund.php";
			break;
		case 'migrate':
			require_once CC_ADMIN_DIR . "/pop/team_migrate.php";
			break;
		case 'mtest':
			require_once CC_ADMIN_DIR . "/pop/mail_test.php";
			break;
		case 'chat':
			require_once CC_ADMIN_DIR . "/pop/admin_chat.php";
			break;
		case 'dossier':
			require_once CC_ADMIN_DIR . "/pop/team_dossier.php";
			break;
		case 'report':
			require_once CC_ADMIN_DIR . "/pop/team_report.php";
			break;
		case 'system_migrate':
			require_once CC_ADMIN_DIR . "/pop/migrate.php";
			break;
		default:
			break;
	}

}
