<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Load Classes
require_once (CC_CLASSES_DIR . "country.class.php");
require_once (CC_CLASSES_DIR . "program.class.php");
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "team.support.class.php");
require_once (CC_CLASSES_DIR . "tabs.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "user.class.php");
require_once (CC_CLASSES_DIR . "docs.class.php");
require_once (CC_CLASSES_DIR . "trigger.class.php");


$team = new TEAM();
$support = new TEAM_SUPPORT();
$page = new TAB_MANAGE();
$program = new PROGRAM();
$country = new COUNTRY();
$rsvp = new RSVP();
$user = new USER();
$docs = new DOCS();

if(isset($_GET['team_id']) && $_GET['team_id'] == 1800)
{
	$trigger = new TRIGGERS();
	//$trigger->pull_trigger(4424);
}

global $wpdb;

/** Process chart note */
if(isset($_REQUEST['edit_team_note']) || isset($_REQUEST['team_note']))
{
		$team->set_team($_REQUEST['team_id']);
		$post_id = $team->value('post');
		update_post_meta($post_id,"_chart_note",$_POST['team_note']);
}

$haveTeam = isset($_GET['team_id']) && ($_GET['team_id']=="new" || $team->is_team($_GET['team_id']));
/////////////////////////////////////////
// IF WE HAVE A TEAM...
/////////////////////////////////////////
if ($haveTeam) :

	wp_enqueue_script('cmd_clone_to_dev', plugins_url() . '/command/js/clone.team.js', array('jquery', 'jquery-ui-autocomplete'), NULL, true );
	wp_localize_script('cmd_clone_to_dev', 'svars', array('ajax_url' => admin_url('admin-ajax.php')));

	//
	//Get Team Name
	if($_GET['team_id']=='new')
	{
		$team_name = "NEW TEAM";
		$docs_due = 'Unknown';
		$progs = 'Unknown';
		$cntry = 'Unknown';
		$start = 'Unknown';
		$end = 'Unknown';
		$vec = 'Unknown';
		$tm = 'Unknown';
		$public_team_note = '';
		$chart_note = '';
		
	}
	else 
	{
		$team->set_team($_GET['team_id']);
		$team_name = $team->value('code') . " / " . $team->value('hash');

		$post_id = $team->value('post');

		//
		//Check if Enhanced permissions
		$usr = wp_get_current_user();
		if($user->user_group($usr->ID) == "enhanced")
		{
			$allow = false;
			if( get_user_meta( $usr->ID, 'cmd_custom_team_poc', true))
			{
				$poc_data = get_post_meta($post_id, 'cmd_team_poc', false);
				if($poc_data)
				{
					foreach($poc_data[0] as $email=>$name_info)
					{
						if($email == $usr->user_email)
						{
							$allow = true;
						}
					}
				}
			}

			if(!$allow)
			{
				echo "<script>";
				echo "window.location = 'admin.php?page=cc-teams'";
				echo "</script>";
				wp_die();
			}

		}

		/** Public Team Note Processing */
		if(isset($_POST['submit_public_team_note']) || isset($_POST['public_team_notes']))
		{
			/** set the public team note meta for this post/team */
			update_post_meta($post_id,"_public_team_note",$_POST['public_team_notes']);
		}

		/** Chart Note Processing */
		if(isset($_POST['submit_chart_note']) || isset($_POST['chart_notes']))
		{
			/** set the chart note meta for this post/team */
			update_post_meta($post_id,"_chart_note",$_POST['chart_notes']);
		}

		$progs = '';
		$cntry = '';
		if($team->get_schedule($_GET['team_id']) != "Unknown")
		{
			foreach($team->get_schedule($_GET['team_id']) as $sched_id=>$sched_data)
			{
				$start_date = '';
				$end_date = '';
				$docs_date = '';

				if(isset($sched_data['program']))
				{
					$program->set_program($sched_data['program'], true);
					
					if(strpos($progs, $program->get_abbv()) === false)
					{
						$progs .= $program->get_abbv() . ", ";
					}
				}
	
				if(isset($sched_data['country']))
				{
					$country->set_country($sched_data['country']);
					
					if(strpos($cntry, $country->get_abbv()) === false )
					{
						$cntry .= $country->get_abbv() . ", ";
					}

				}
				
				if($start_date == '')
				{
					$start_date = $sched_data['start_date'];
				}
				$end_date = $sched_data['end_date'];
	
				$end = date("m-d-Y", mktime(0,0,0,substr($end_date,0,2),substr($end_date,3,2),substr($end_date,6,4)));
				$start = date("m-d-Y", mktime(0,0,0,substr($start_date,0,2),substr($start_date,3,2),substr($start_date,6,4)));
				
				$options = get_option('cmd_center');
				$doc_interval = (!empty($options['docs_due_limit']) ? $options['docs_due_limit'] : 14);
				$docs_due = date("m-d-Y", mktime(0,0,0,substr($start_date,0,2),substr($start_date,3,2) - $doc_interval,substr($start_date,6,4)));
			}

			if(strlen($progs)>0)
			{
				$progs = substr($progs, 0, -2);
			}
			if(strlen($cntry)>0)
			{
				$cntry = substr($cntry, 0, -2);
			}

		}
		else
		{
			$docs_due = "Unset";
			$start = "Unset";
			$end = "Unset";
		}
		
		//
		//Gather Defined Data here
		$vec = '';
		$tm = '';
		$query = "SELECT um.user_id, um.meta_value, u.display_name
					FROM " . $wpdb->prefix . "usermeta um INNER JOIN
						 " . $wpdb->prefix . "users u on u.ID = um.user_id 
				  WHERE meta_key = '" . 'cmd_team' . $_GET['team_id'] . '_staff_pos' . "'
				  ORDER BY display_name";
		$possibles = $wpdb->get_results($query, ARRAY_A);
		foreach($possibles as $data)
		{
			$staff[$data['meta_value']][$data['user_id']] = $data['display_name'];
		}

		if(!empty($staff['VEC']))
		{
			foreach($staff['VEC'] as $id=>$name)
			{
				$vec .= $name . "<BR>";	
			}
		}
		else
		{
			$vec .= 'Unknown';	
		}

		if(!empty($staff['Team Leader']))
		{
			foreach($staff['Team Leader'] as $id=>$name)
			{
				$tm .= $name . "<BR>";	
			}
		}
		else
		{
			$tm .= 'Unknown';	
		}

		/** Public Team Note */
		$public_team_note = get_post_meta($team->value('post'),'_public_team_note',true);
		$public_team_note = empty($public_team_note)? "Note" : $public_team_note;

		/** Chart Note */
		$chart_note = get_post_meta($team->value('post'),'_chart_note',true);
		$chart_note = empty($chart_note)? "Note" : $chart_note;
	}

	//
	//Have a Team, let's do stuff with it
	if($_GET['team_id']=='new')
	{
		$page->SetTabs(array('tinfo' => 'Program Basics'));
	} 
	else
	{
		$page_tabs = array();
		
		if(!empty($progs) && !empty($cntry))
		{		
			$page_tabs['tovr'] = 'Program Overview';
		}
		
		$page_tabs['tinfo'] = 'Program Setup';
		
		if($team->is_team($_GET['team_id']) && $team->is_custom_team($_GET['team_id']))
		{								
			$page_tabs['tcust'] = "Custom";
		}

		if($team->is_team($_GET['team_id']) && $team->is_internship($_GET['team_id']))
		{								
			$page_tabs['tintern'] = "Internship";
		}

		if(!empty($progs) && !empty($cntry))
		{		
			$page_tabs['tadd'] = 'Add-ons';
			$page_tabs['titin'] = 'Program Itinerary';
			$page_tabs['tdoc'] = "Documents";
			$page_tabs['treps'] = "Reporting"; 
			$page_tabs['tmem'] = 'Staffing';
		}

		$page_tabs['tplan'] = 'Planning'; 

		if(!empty($progs) && !empty($cntry))
		{		
			$page_tabs['tbud'] = 'Budget';
			$page_tabs['tprev'] = 'Preview';
			$team_chat_enabled = get_post_meta($team->value('post'),'team_chat_enabled',true);
			if(empty($team_chat_enabled) || $team_chat_enabled == "true")
			{
				$page_tabs['tchat'] = 'Chat';
			}
			$page_tabs['tmesg'] = 'Messages';
		}

		if($user->user_group(get_current_user_id()) == "enhanced" || $user->user_group(get_current_user_id()) == "volunteer")
		{
			unset($page_tabs['tinfo']);
			unset($page_tabs['tcust']);
			unset($page_tabs['tintern']);
			unset($page_tabs['tadd']);
			unset($page_tabs['titin']);
			unset($page_tabs['tdoc']);
			unset($page_tabs['tmem']);
			unset($page_tabs['tplan']);
			unset($page_tabs['tbud']);
			unset($page_tabs['tprev']);
		}
		
		$page->SetTabs($page_tabs);
	}
	$page->SetTabName('cur_tab');
	if(isset($_GET[$page->GetTabName()]) && $page->IsTab($_GET[$page->GetTabName()]))
	{
		$page_name = $_GET[$page->GetTabName()];
	}
	else
	{
		if(isset($_GET['team_id']) && $_GET['team_id']=="new")
		{
			$page_name = "tinfo";
		}
		else
		{
			if(!empty($progs) && !empty($cntry))
			{		
				$page_name = "tovr";
			}
			else
			{
				$page_name = "tinfo";
			}
		}
	}
	$page->SetCurrent($page_name);
	
    /////////////////////////////////////
    // BEGIN HTML
    /////////////////////////////////////
	?>
	<div class="page-header wrap">
	   	<a class="back-to-list-arrow" href='<?=$page->GET_path(array('team_id','save'))?>'>
	   		<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
	   	</a>

        <?php
        if ($team_name == "NEW TEAM"){
            ?>
            <h1 id="team_name_main">New Program</h1>
            <?php
        }
        else{
            ?>
            <h1 id="team_name_main"><?=$team_name?></h1>
            <?php
        }
        ?>

    	<a class='page-title-action' href='javascript:window.open("/wp-admin/admin.php?page=cc-pops&display=report&team_id=<?php echo $_GET['team_id']?>", "<?=$team_name?> Report", "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");'>Report</a>
	  	
	  	<?php
		if(get_current_user_id() == 1)
		{
			?>
			<a class='page-title-action' name="clone_to_dev" id="clone_to_dev">Clone to Dev</a>
			<?php
		}
		?>
		
	</div>

	<div class="header-table-wrap">
		<div class="team-details">
			<dl>
				<dt>Countries</dt>
				<dd><?php echo $cntry; ?></dd>
				<dt>Programs</dt>
				<dd><?php echo $progs; ?></dd>
				<dt>Status</dt>
				<dd><?php echo $support->status_value($team->value('status')); ?></dd>
				<dt>Docs Due</dt>
				<dd><?php echo $docs_due; ?></dd>
			</dl>

			<dl>
				<dt>Start Date</dt>
				<dd><?php echo $start; ?></dd>
				<dt>End Date</dt>
				<dd><?php echo $end; ?></dd>
<!--				<dt>VEC</dt>-->
				<dt>Trip Coordinator</dt>
				<dd><?php echo $vec; ?></dd>
				<dt>Program Leader</dt>
				<dd><?php echo $tm; ?></dd>
			</dl>
		</div>

	    <div id="team_notes">
			<?php
			if ($_REQUEST['team_id'] != "new") : ?>

	        
	        	<div class="note-left">
	        		<h3>Public Program Note (external)</h3>
					<form id="public_team_note_form" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path()?>">
						<textarea name="public_team_notes" id="public_team_notes"><?php echo $public_team_note; ?></textarea>
						<input class="button button-primary" type="submit" name="submit_public_team_note" value="Update Public Program Note" id="sbmt_pblc_tm_nt"/>
					</form>
					<script>
						jQuery("#public_team_note_form").submit( function( event ) {
							event.preventDefault();
							jQuery("#sbmt_pblc_tm_nt").val("Saving...");
							var form_data = jQuery("#public_team_note_form").serialize();
							jQuery.post("<?php echo $page->GET_path(); ?>",form_data).done(function(data)
							{
								jQuery("#sbmt_pblc_tm_nt").val("Update Public Program Note");
							});
						});
					</script>
				</div>
				<div class="note-right">
					<h3>Chart Note (internal)</h3>
					<form id="chart_note_form"method="POST" enctype="multipart/form-data" action="<?=$page->GET_path()?>">
						<textarea name="chart_notes" id="chart_notes"><?php echo $chart_note; ?></textarea>
						<input class="button button-primary" type="submit" name="submit_chart_note" value="Update Chart Note" id="sbmt_cht_nt"/>
					</form>
					<script>
						/** jQuery submits for Note forms */
						jQuery("#chart_note_form").submit( function( event )
						{

							event.preventDefault();
							jQuery("#sbmt_cht_nt").val("Saving...");
							var form_data = jQuery("#chart_note_form").serialize();
							jQuery.post("<?php echo $page->GET_path(); ?>",form_data).done(function( data )
							{
								jQuery("#sbmt_cht_nt").val("Update Chart Note");
							});
						});
					</script>
				</div>
	
			<?php endif; ?>
	    </div>
	</div>

	<?php
	$page->ShowTabs(array('save'));

	//
	//Deal with tabs
	switch ($page->GetCurrent())
	{
		case 'tovr':
			include (CC_ADMIN_DIR . "tabs/team.view.php");
			break;	
		case 'tinfo':
			include (CC_ADMIN_DIR . "tabs/team.info.php");
			break;	
		case 'titin':
			include (CC_ADMIN_DIR . "tabs/team.itinerary.php");
			break;
		case 'tadd':
			include (CC_ADMIN_DIR . "tabs/team.add-ons.php");
			break;
		case 'tdoc':
			include (CC_ADMIN_DIR . "tabs/team.docs.php");
			break;
		case 'treps':
			include (CC_ADMIN_DIR . "tabs/team.reports.php");
			break;
		case 'tmeds':
			include (CC_ADMIN_DIR . "tabs/team.meds.php");
			break;
		case 'ttrain':
			include (CC_ADMIN_DIR . "tabs/team.train.php");
			break;
		case 'tmem':
			include (CC_ADMIN_DIR . "tabs/team.members.php");
			break;
		case 'tbud':
			include (CC_ADMIN_DIR . "tabs/team.budget.php");
			break;
		case 'tplan':
			include (CC_ADMIN_DIR . "tabs/team.plan.php");
			break;
		case 'timg':
			include (CC_ADMIN_DIR . "tabs/team.img.php");
			break;
		case 'tprev':
			include (CC_ADMIN_DIR . "tabs/team.prev.php");
			break;
		case 'tchat':
			include (CC_ADMIN_DIR . "tabs/team.chat.php");
			break;
		case 'tmesg':
			include (CC_ADMIN_DIR . "tabs/team.msg.php");
			break;
		case 'tcust':
			include (CC_ADMIN_DIR . "tabs/team.custom.php");
			break;
		case 'tintern':
			include (CC_ADMIN_DIR . "tabs/team.intern.php");
			break;
		default:
			break;
	}
endif;

/////////////////////////////////////////
// IF WE *DO NOT* HAVE A TEAM...
/////////////////////////////////////////
if (!$haveTeam) :

	wp_enqueue_script('jquery-ui-datepicker');

	$whoami = wp_get_current_user();

	//
	//Filters
	$filters = array();
	!empty($_POST['code']) ? $filters['code'] = $_POST['code'] : '';
	!empty($_POST['status']) ? $filters['status'] = $_POST['status'] : '';
	!empty($_POST['country']) ? $filters['country'] = $_POST['country'] : '';
	!empty($_POST['start']) ? $filters['start'] = $_POST['start'] : '';
	!empty($_POST['teamlead']) ? $filters['teamlead'] = $_POST['teamlead'] : '';
	!empty($_POST['type']) ? $filters['type'] = $_POST['type'] : '';
	!empty($_POST['program']) ? $filters['program'] = $_POST['program'] : '';
	!empty($_POST['end']) ? $filters['end'] = $_POST['end'] : '';
	!empty($_POST['tm']) ? $filters['tm'] = $_POST['tm'] : '';
	!empty($_POST['vec']) ? $filters['vec'] = $_POST['vec'] : '';
	!empty($_POST['custom_teams']) ? $filters['custom_teams'] = $_POST['custom_teams'] : '';
	!empty($_POST['internship']) ? $filters['internship'] = $_POST['internship'] : '';

	if(!empty($_POST['my_teams']) && $_POST['my_teams'] == 1)
	{
		//$filters = array();
		$filters['vec'] = $whoami->ID;
		$filters['tm'] = $whoami->ID;
	}

	$currec = ((isset($_POST['cur_rec'])) ? $_POST['cur_rec'] : 0);
	$team->create_list($filters, $currec, 100);
	$teams = $team->get_list();
	?>

	<div class='page-header wrap'>
    	<h1>Programs</h1>

       	<?php
		if($user->user_group($whoami) == "staff" || $user->user_group($whoami) == "admin" || $user->user_group($whoami) == "super")
		{
			?>
			<a href='<?=$page->GET_path(array(),array('team_id'=>'new'))?>' class='page-title-action'>Add New</a>
			<?php
		}
		?>

		<span>
			<form id="team_search_2" name="search_2" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path()?>">
            <input name="code" id="code" class="regular-text" type="text" size='20' maxlength='20' 
				<?php echo "value='" . (!empty($_POST['code']) ? $_POST['code']  : '') . "'"?> placeholder="Program ID Search"/>
            <button type="submit" name="submit_tc" id="submit_tc" class="button" value="Search"><i class="fa fa-caret-right" aria-hidden="true"></i></button>
			</form>
		</span>
            
        <a id="advanced_search_toggle" class="button" onClick="javascript:toggle_search()">Advanced Search
        	<i class="arrow-down fa fa-caret-down" aria-hidden="true"></i>
        	<i class="arrow-up fa fa-caret-up" aria-hidden="true"></i>
        </a>

       	<?php
		if($user->user_group($whoami) == "staff" || $user->user_group($whoami) == "admin" || $user->user_group($whoami) == "super")
		{
			?>
			<a class='page-title-action' onclick="javascript:my_team_search()">My Programs</a>
			<a class='page-title-action' onclick="javascript:custom_team_search()">Custom Programs</a>
			<a class='page-title-action' onclick="javascript:internship_search()">Internships</a>
			<?php
		}
		?>
    </div>

	<form id="team_search" name="search" method="POST" enctype="multipart/form-data" action="<?=$page->GET_path()?>">
		<table id='search_table' class='standard-large-table wrap'>
			<tr>
				<td>
	            	<SELECT name="status" id="status"><?php echo $support->status_selector(isset($_POST['status'])? $_POST['status'] : 0,true) ?></SELECT>
	            </td>
				<td>
	            	<SELECT name="country" id="country"><?php echo $country->selector(isset($_POST['country'])? $_POST['country'] : 0,true) ?></SELECT>
	            </td>
				<td>
	            	<input 
	            		style='<?=$w?>' 
	            		name="start" 
	            		id="start" 
	            		class="regular-text" 
	            		type="text"
	            		placeholder="Start Date"
	            		value="<?php _e(!empty($_POST['start']) ? $_POST['start'] : ''); ?>"
					/>
	            	<span class="date-icon"></span>
	            </td>
				<td>
	            	<input name="end" id="end" class="regular-text" type="text" <?php _e((!empty($_POST['end']) ? 'value="' . $_POST['end'] . '"' : 'placeholder="End Date"'))?>/>
	            	<span class="date-icon"></span>
	            </td>
				<td>
					<button type="submit" name="submit" id="submit" class="button" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
					<!--<input type="checkbox" name="vols_only" id="vols_only" <?php echo ((isset($_POST['vols_only'])) ? "checked" : "") ?> value="1">-->
				</td>
			</tr>
			<tr>
				<td>
	            	<SELECT name='type' id='type'><?php echo $support->type_selector(isset($_POST['type'])? $_POST['type'] : 0,true) ?></SELECT>
	            </td>
				<td>
	            	<SELECT name="program" id="program"><?php echo $program->selector(isset($_POST['program'])? $_POST['program'] : 0,true) ?></SELECT>
	            </td>
				<td>
	            	<SELECT name="vec" id="vec"><?php echo $user->user_select($user->fetch_vec(),isset($_POST['vec'])? $_POST['vec'] : 0, 'VEC (ANY)', true) ?></SELECT>
	            </td>
				<td>
	            	<SELECT name="tm" id="tm"><?php echo $user->user_select($user->fetch_team_managers(),isset($_POST['tm']) ? $_POST['tm'] : 0, 'PROGRAMS LEADER (ANY)', true) ?></SELECT>
	            </td>
				<td><button type="" name="reset" id="reset" class="button" value="ALL" onclick="javascript:reset_form()">All</button></td>
			</tr>
		</table> <!-- /#search_table -->

		<input type="hidden" name="my_teams" id="my_teams" value="0">
		<input type="hidden" name="custom_teams" id="custom_teams" value="0">
		<input type="hidden" name="internship" id="internship" value="0">
        <input type="hidden" name="adv_search" id="adv_search" value="<?php _e(!empty($_POST['adv_search']) ? $_POST['adv_search'] : 0)?>">
        <input type="hidden" name="qnum" id="qnum" value="100">
        <input type="hidden" name="cur_rec" id="cur_rec" value="<?php _e($currec)?>">
	</form>
    
	<script>

		<?php
		//if(!empty($_POST['adv_search']) && $_POST['adv_search'] == "1")
		//{
			// echo "toggle_search()";		
		//}
		?>
		
		function my_team_search()
		{
			document.getElementById('my_teams').value = 1;
			document.getElementById('custom_teams').value = 0;
			document.getElementById('internship').value = 0;
			document.getElementById('status').value = '';
			document.getElementById('country').value = '';
			document.getElementById('start').value = '';
			document.getElementById('end').value = '';
			document.getElementById('type').value = '';
			document.getElementById('program').value = '';
			document.getElementById('vec').value = '';
			document.getElementById('tm').value = '';
			document.getElementById('code').value = '';
			document.getElementById('cur_rec').value = '0';

			document.getElementById('submit').click();
		}

		function custom_team_search()
		{
			document.getElementById('my_teams').value = 0;
			document.getElementById('custom_teams').value = 1;
			document.getElementById('internship').value = 0;
			document.getElementById('status').value = '';
			document.getElementById('country').value = '';
			document.getElementById('start').value = '';
			document.getElementById('end').value = '';
			document.getElementById('type').value = '';
			document.getElementById('program').value = '';
			document.getElementById('vec').value = '';
			document.getElementById('tm').value = '';
			document.getElementById('code').value = '';
			document.getElementById('cur_rec').value = '0';

			document.getElementById('submit').click();
		}

		function internship_search()
		{
			document.getElementById('my_teams').value = 0;
			document.getElementById('custom_teams').value = 0;
			document.getElementById('internship').value = 1;
			document.getElementById('status').value = '';
			document.getElementById('country').value = '';
			document.getElementById('start').value = '';
			document.getElementById('end').value = '';
			document.getElementById('type').value = '';
			document.getElementById('program').value = '';
			document.getElementById('vec').value = '';
			document.getElementById('tm').value = '';
			document.getElementById('code').value = '';
			document.getElementById('cur_rec').value = '0';

			document.getElementById('submit').click();
		}

		function reset_form()
		{
			document.getElementById('my_teams').value = 0;
			document.getElementById('status').value = '';
			document.getElementById('country').value = '';
			document.getElementById('start').value = '';
			document.getElementById('end').value = '';
			document.getElementById('type').value = '';
			document.getElementById('program').value = '';
			document.getElementById('vec').value = '';
			document.getElementById('tm').value = '';
			document.getElementById('code').value = '';
			document.getElementById('cur_rec').value = '0';
			
			document.getElementById('submit').click();
		}

		function toggle_search()
		{
			var table = jQuery('#search_table');
			var toggle = jQuery('#advanced_search_toggle');
			var adv = document.getElementById('adv_search');

			if (table.hasClass('open'))
			{
				table.removeClass('open');
				toggle.removeClass('open');
				adv.value = "0";
			}
			else
			{
				table.addClass('open');
				toggle.addClass('open');
				adv.value = "1";
			}
		}

		jQuery(document).ready(function(){
			toggle_search();

			var datepickerOptions = {
			    dateFormat : 'mm-dd-yy',
			    defaultDate: 0,
			    //minDate: 0,
			    beforeShow: function(event, ui) {
			       jQuery('#ui-datepicker-div').addClass('open');
			    },
			    onClose: function(event, ui) {
			        jQuery('#ui-datepicker-div').removeClass('open');
			    }
			};

			jQuery('#start').datepicker(datepickerOptions);
			jQuery('#end').datepicker(datepickerOptions);
			jQuery('#start').change(function(){
				var endDate = jQuery('#start').val();
				jQuery.extend(datepickerOptions, {
					defaultDate: endDate,
					minDate: endDate
				});
				jQuery('#end').datepicker(datepickerOptions);
			});
		});
	</script>
	
	<div class="main-list-wrapper">
	<table id="team_info" class="teams-list standard-large-table">
		<thead>
			<tr>
				<th class="primary-data-cell">
                    PROGRAMS ID
					<div class="tooltip-wrapper">
						<i class="fa fa-info-circle" aria-hidden="true"></i>
						<div class="tooltip">
							There are two Programs ID's for every program. <br /><br />Custom "hashtags" are editable in the system and show upon the website. <br /><br />"Legacy numbers" include information about the program. Country/Start Date/Program/Program Type.
						</div>
					</div>
				</th>
	        	<th class="medium">STATUS
	        		<div class="tooltip-wrapper">
	        			<i class="fa fa-info-circle" aria-hidden="true"></i>
	    				<div class="tooltip">
	                    	<h5>Statuses include:</h5>
							
							<dl>
								<dt>Open</dt>
									<dd><?php _e($support->status_tip($support->status_by_name('Open')))?></dd>
								<dt>Go</dt>
									<dd>A trip that has reached its minimum volunteer threshold and will travel.</dd>
								<dt>Closed</dt>
									<dd><?php _e($support->status_tip($support->status_by_name('Closed')))?></dd>
								<dt>In Progress</dt>
									<dd><?php _e($support->status_tip($support->status_by_name('In Progress')))?></dd>
								<dt>Complete</dt>
									<dd><?php _e($support->status_tip($support->status_by_name('Complete')))?></dd>
								<dt>Canceled</dt>
									<dd><?php _e($support->status_tip($support->status_by_name('Canceled')))?></dd>
							</dl>
	                    </div>
	                </div>
	            </th>
	            <th class="small">TYPE
	            	<div class="tooltip-wrapper">
	            		<i class="fa fa-info-circle" aria-hidden="true"></i>
	    				<div class="tooltip">
	                    	<h5>Program Types include:</h5>

							<dl>
								<dt>Open</dt>
									<dd>A program that is visible on to anyone</dd>
								<dt>HS</dt>
									<dd><?php _e($support->type_value($support->type_by_fld('HS', 'abbv'), 'title'))?></dd>
								<dt>Custom</dt>
									<dd>A normal custom program (not open, not faculty-led)</dd>
								<dt>CFL</dt>
										<dd><?php _e($support->type_value($support->type_by_fld('CFL', 'abbv'), 'title'))?></dd>
								<dt>CHS</dt>
									<dd><?php _e($support->type_value($support->type_by_fld('CHS', 'abbv'), 'title'))?></dd>
								<dt>IT</dt>
									<dd>Internship Template</dd>
								<dt>INT</dt>
									<dd>An internship</dd>							
							</dl>
	                    </div>
	                </div>
	            </th>
	            <th class="small">CTRY</th>
	            <th class="small">PRG</th>
	            <th class="small">VOL</th>
	            <th class="medium">START DATE</th>
	            <th class="medium">END DATE</th>
	            <th class="small">LENGTH</th>
	            <th class="large">PROGRAM</th>
	            <th>CHART NOTES</th>
	        </tr>
		</thead>

		<?php
		$lastmo = 0;
		$cur_month = "NONE";
		$cur_year = '';
		$count = 0;
		$volunteers = 0;

		/////////////////////////
		// TEAMS LIST LOOP
		/////////////////////////
		foreach($teams as $id=>$data) {
			$chart_note = get_post_meta($data['post'],'_chart_note',true);
			$chart_note = !empty($chart_note) ? $chart_note : '<span class="placeholder-text">Enter notes here</span>';
			$chart_note = '<span class="team_note note_field" onclick="editTeamNote('.$id .');">'. $chart_note .'</span>';
			$progs = '';
			$cntry = '';

			if($team->get_schedule($id) != "Unknown")
			{
				foreach($team->get_schedule($id) as $sched_id=>$sched_data)
				{
					$start_date = '';
					$end_date = '';
					
					if(isset($sched_data['program']))
					{
						$program->set_program($sched_data['program']);
						$progs .= $program->get_abbv() . "<BR>";
					}
					if(isset($sched_data['country']))
					{
						$country->set_country($sched_data['country']);
						$cntry .= $country->get_abbv() . "<BR>";
					}
					
					if($start_date == '')
					{
						$start_date = $sched_data['start_date'];
					}
					$end_date = $sched_data['end_date'];
	
					$end = mktime(0,0,0,substr($end_date,0,2),substr($end_date,3,2),substr($end_date,6,4));
					$start = mktime(0,0,0,substr($start_date,0,2),substr($start_date,3,2),substr($start_date,6,4));
					$length = (($end-$start) / 86400) + 1;
				}
			}
			else
			{
				$start_date = "Unknown";
				$end_date = "Unknown";
				$length = "??";	
				$start = "Undefined";
			}
			
			if(strlen($progs) >=4)
			{
				$progs = substr($progs,0,-4);
			}
			global $wpdb;
			$team_manager = '';
			$team_leader = '';
			$meta_team_staff_key = 'cmd_team' . $id . '_staff_pos';
			$query = "SELECT um.user_id, um.meta_value, u.display_name
						FROM " . $wpdb->prefix . "usermeta um INNER JOIN
							 " . $wpdb->prefix . "users u on u.ID = um.user_id 
					  WHERE meta_key = '" . $meta_team_staff_key . "'
					  ORDER BY display_name";
			$possibles = $wpdb->get_results($query, ARRAY_A);
			
			foreach($possibles as $team_data)
			{
				if($team_data['meta_value'] == 'Team Leader')
				{
					$team_leader .= $team_data['display_name'] . ", ";	
				}
				if($team_data['meta_value'] == 'VEC')
				{
					$team_manager .= $team_data['display_name'] . "<BR>";	
				}
			}
			if(strlen($team_manager) > 4)
			{
				$team_manager = substr($team_manager,0,-4);
			}
			if(strlen($team_leader) > 4)
			{
				$team_leader = substr($team_leader,0,-2);
			}
			
			if(empty($_POST['custom_teams']) && empty($_POST['internship']))
			{
				if(isset($start) && is_numeric($start))
				{
					$test_date = date("Ym",$start);
				}
				else
				{
					$test_date = "Undefined";
				}
				
				if($lastmo != $test_date)
				{
					if($test_date > 0 && $count > 0)
					{
						echo "<tr class='totals'>";
						echo "    <td colspan='11'><b>TOTALS for " . $cur_month . " " . $cur_year . " - Programs: " . $count . ", Volunteers: " . $volunteers . "</b></td>";
						echo "</tr>";
					}
					
					$count = 0;
					$volunteers = 0;
					$lastmo = is_numeric($start) ? date("Ym", $start) : 0;	
					$cur_month = is_numeric($start) ? date("M", $start) : 'NONE';
					$cur_year = is_numeric($start) ? date("Y", $start): '';
				}
			}

			if( (isset($_POST['vols_only']) && $_POST['vols_only'] ==1 && $rsvp->count_rsvp($id) > 0) || !isset($_POST['vols_only']) )
			{
				echo "<tr>";
				echo "<td><a class='team-link' href='" . $page->GET_path(array('save','display','team_id'),array('team_id'=>$id)) . "'>" . $data['code'] . "<span class='team-hash'>" . $data['hash'] . "</span></a></td>";
				echo "<td>" . $support->status_value($data['status']) . "</td>";
				echo "<td>" . $support->type_value($data['type'], 'abbv')   . "</td>";
				echo "<td>" . $cntry . "</td>";
				echo "<td>" . $progs . "</td>";
				echo "<td>" . $rsvp->count_rsvp($id) . "</td>"; //Removed min/max from listing " of (" . $data['min'] . "/" . $data['max'] . ")</td>";
				echo "<td>" . $start_date . "</td>";
				echo "<td>" . $end_date . "</td>";
				echo "<td>" . $length . " days</td>";
				echo "<td class='large'>VEC: " . $team_manager . "<br>TL: " . $team_leader . "</td>";
				echo "<td id=\"team_note_". $id ."\" >". $chart_note ."</td>";
				echo "</tr>";

				 $count++;
				 $volunteers += $rsvp->count_rsvp($id);

			}
		}
		// end - TEAM LIST LOOP
		////////////////////////////

		echo "<tr class='totals'>";
		echo "    <td colspan='11'><b> TOTALS for " . $cur_month . " " . $cur_year . " - Programs: " . $count . ", Volunteers: " . $volunteers . "</b></td>";
		echo "</tr>"; ?>

		<script>
			function editTeamNote(team_id)
			{
				var form = jQuery('<form class="edit_note_form edit_team_note_form" method="POST" action="<?php echo $page->GET_path(); ?>">');
				var container = jQuery("#team_note_"+ team_id);
				var note_text = container.children('.team_note').html();

				if (note_text == '<span class="placeholder-text">Enter notes here</span>') {
					note_text = 'Enter notes here';
				}
				form.append('<input type="hidden" name="team_id" value="'+team_id+'" />');
				form.append('<input type="text" class="note_field" placeholder="Enter notes here" name="team_note" value="'+note_text+'" />');
				form.append('<input type="submit" class="save-note button button-primary" name="edit_team_note" value="Save Note" /> ');
				form.append('<button type="button" class="cancel-save-note button button-primary" onclick="resetTeamNote('+team_id+',\''+ note_text.replace(/'/g, "\\'") +'\');">X</button>');
				container.html(form);
				container.find('input.note_field').select();
			}

			function resetTeamNote(team_id,note_text)
			{
				var container = jQuery("#team_note_"+ team_id);
				var note_text = note_text;

				if (note_text == '' || note_text == 'Enter notes here') {
					note_text = '<span class="placeholder-text">Enter notes here</span>';
				} else {
					note_text = note_text;
				}

				container.html('<span class="team_note note_field" onclick="editTeamNote('+team_id +');">'+ note_text +'</span>');
			}

			jQuery(document).on("submit",".edit_team_note_form",function(event)
			{
				event.preventDefault();
				var t_id = jQuery("input[name='team_id']",this).val();
				var t_note = jQuery("input[name='team_note']",this).val();

				jQuery.post("<?php echo $page->GET_path(); ?>",jQuery(this).serialize()).done(function()
				{
					resetTeamNote(t_id,t_note);
				});
			});
		</script>
	</table> <!-- /#team_info -->
	</div>

	<hr>

    <div class="pagination-wrapper">
        <a class="pagination-arrow first-page" onclick="javascript:change_start(0)"><i class="fa fa-fast-backward" aria-hidden="true"></i></a>
        <a class="pagination-arrow prev-page" onclick="javascript:change_start(<?php _e(max(0,$currec-100))?>)"><i class="fa fa-step-backward" aria-hidden="true"></i></a>

    	<input type="text" name="start_cnt" id="start_cnt" disabled="true" size="6" value="<?php _e($currec)?>">
        to
    	<input type="text" name="end_cnt" id="end_cnt" disabled="true" size="6" value="<?php _e($currec+99)?>">

        <a class="pagination-arrow next-page" onclick="javascript:change_start(<?php _e(max(0,$currec+100))?>)"><i class="fa fa-step-forward" aria-hidden="true"></i></a>
        <a class="pagination-arrow last-page" onclick="javascript:change_start(<?php _e($team->possible_teams()-100)?>)"><i class="fa fa-fast-forward" aria-hidden="true"></i></a>
        
        of

       	<input type="text" name="ttl_cnt" id="ttl_cnt" disabled="true" size="6" value="<?php _e($team->possible_teams())?>">
        
    </div>

	<script>
		function change_start(start_num)
		{
			document.getElementById('cur_rec').value = start_num;
			document.getElementById('submit').click();
		}
	</script>
<?php 
endif;
?>