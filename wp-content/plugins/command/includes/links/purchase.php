<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/** Required Classes */
require_once (CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . 'rsvp.class.php');
require_once (CC_CLASSES_DIR . 'country.class.php');
require_once (CC_CLASSES_DIR . 'email.class.php');

/** Process team status change triggers */
add_action( 'cmd_purchased_items', 'purchased_item', 10, 3 );


function purchased_item($details, $user_id, $order_item_id)
{
	if($details['type'] == 4 && $details['item_id'] == 36402 && $details['quantity'] > 0)
	{
		global $wpdb;

		//
		//Find Team
		$query = "SELECT item_id FROM " . $wpdb->prefix . "command_order_items WHERE id = " . $details['add_on'];
		$team_id = $wpdb->get_var($query);
		
		if($team_id)
		{
			$team = new TEAM();
			$team->set_team($team_id);
			$user = get_user_by('ID', $user_id);
			
			$meta_team_staff_key = 'cmd_team' . $team_id . '_staff_pos';

			$query = "SELECT um.user_id, um.meta_value, u.display_name, u.user_email
						FROM " . $wpdb->prefix . "usermeta um INNER JOIN
							 " . $wpdb->prefix . "users u on u.ID = um.user_id 
					  WHERE meta_key = '" . $meta_team_staff_key . "' AND
							meta_value = 'VEC'
					  ORDER BY display_name";
			$possibles = $wpdb->get_results($query, ARRAY_A);

			foreach($possibles as $data)
			{
				$e_mail = new EMAIL();
				$e_mail->from( 'TC NOTIFY', 'donotreply@islonline.org' );
				$e_mail->to( $data['display_name'], $data['user_email'] );
				$e_mail->subject('TC NOTICE: MPA Purchase');
				$e_mail->message('<html>User: ' . $user->first_name . ' ' . $user->last_name . '<BR>Team: ' . $team->value('code') . '</html>');
				$e_mail->send();
			}

			//
			//2018-03-22 DJT Added email to Reference and Diane
			$e_mail = new EMAIL();
			$e_mail->from( 'TC NOTIFY', 'donotreply@islonline.org' );
			$e_mail->to( 'Reference', 'reference@islonline.org' );
			$e_mail->to( 'Diane', 'diane@islonline.org' );
			$e_mail->subject('TC NOTICE: MPA Purchase');
			$e_mail->message('<html>User: ' . $user->first_name . ' ' . $user->last_name . '<BR>Team: ' . $team->value('code') . '</html>');
			$e_mail->send();

		}
	}
}