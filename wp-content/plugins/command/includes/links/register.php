<?php

//
//Required classes
require_once (CC_CLASSES_DIR . 'email.class.php');
require_once (CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . 'user.class.php');

//
//Actions
add_action('cmd_custom_register', 'cmd_custom_register', 10, 5);
add_action('cmd_checkout', 'cmd_poc_email_check', 10, 2);

//
//2018-02-19 DJT Added action
add_action('user_register', 'cmd_auto_contact_check', 10, 2);



function cmd_custom_register($action, $login, $first_name, $last_name, $address)
{
	//
	//Send custom email for registrant
	$email = new EMAIL();

	//$message_obj = get_post(35196, 'OBJECT');
	//$message = $message_obj->post_content;
	
	//
	//Fix email, name, time/date pieces
	//$message = str_replace('{{first_name}}', $first_name, $message);
	//$message = str_replace('{{last_name}}', $last_name, $message);
	//$message = str_replace('{{login}}', $login, $message);
	//$message = str_replace('{{email}}', $address, $message);
	//$message = str_replace('{{site_url}}', site_url(), $message);
	//$message = str_replace('{{time}}', date("H:n:s"), $message);
	//$message = str_replace('{{date}}', date("F j, Y"), $message);
	

	//
	//Make the email and send it
	$email->to($first_name . ' ' . $last_name, $address);
	$email->from('ISL Registration', 'donotreply@islonline.org');
	$email->subject('ISL Registration');
	
	switch($action)
	{
		case 'checkout':
		case 'payment':
			$email->message(email_auto_register_team($first_name, $login, $last_name));
			break;
		case 'donate':
			$email->message(email_auto_register_donation($first_name, $login, $last_name));
			break;
	}

	$email->send();
	
}


function cmd_poc_email_check($team_id, $user_id)
{
	$team = new TEAM();
	$user = new USER();
	$email = new EMAIL();
	
	$team->set_team($team_id);
	$possibles = $user->fetch_poc($team->value('post'));
				
	if(!empty($possibles))
	{
		foreach($possibles as $email_address=>$data)
		{
			$email->to($data['first_name'] . ' ' . $data['last_name'], $email_address);
		}

		$email->from('ISL Registration', 'donotreply@islonline.org');
		$email->subject('ISL Registration');
		$email->message('A new user has registered for team ' . $team->value('hash'));
		$email->send();
	}

	
}

//
//2018-02-19 DJT Added functino to make sure all new registrants have email set for contact preference.
function cmd_auto_contact_check( $UserID )
{
	update_user_meta( $UserID , 'contact_preference' , array( 0 => 'Email' ) );
	update_user_meta( $UserID , '_contact_preference' , 'field_57ff5711447f7' );
	
	echo '<script>';
	echo 'fbq(\'track\', \'CompleteRegistration\');';
	echo '</script>';
}