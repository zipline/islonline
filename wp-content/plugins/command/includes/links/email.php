<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
/** Required Classes */
require_once (CC_CLASSES_DIR . "email.class.php");

$cmd_email_override_sent = false;

//
/** Process team status change triggers */
add_filter('wp_mail', 'cmd_hijack_email', 99, 1);
add_action( 'wp_mail_failed', 'wppb_override', 1 );

//
/** Trigger for team change events */
function cmd_hijack_email( $args )
{
	$e_mail = new EMAIL();
	global $cmd_email_override_sent;
	
	//
	//Build to by deconstructin $args['t0']
	if( is_array( $args['to'] ))
	{
		foreach( $args['to'] as $address )
		{
			$recipient_name = '';
 
            if ( preg_match( '/(.*)<(.+)>/', $address, $matches ) ) 
			{
            	if ( count( $matches ) == 3 ) 
				{
                	$recipient_name = $matches[1];
					$address        = $matches[2];
                    
				}
			}
 
			$e_mail->to( $recipient_name, $address );
		}
	}
	else
	{
		$recipient_name = '';
		$address = $args['to'];
		foreach(explode(',', $address) as $temp_addr)
		{
			if ( preg_match( '/(.*)<(.+)>/', $temp_addr, $matches ) ) 
			{
				if ( count( $matches ) == 3 ) 
				{
					$recipient_name = $matches[1];
					$email_address  = $matches[2];

					$e_mail->to( $recipient_name, $email_address );
				}
			}
			else
			{
				$e_mail->to( $temp_addr, $temp_addr );
			}
		}
	}

	$args['to'] = $e_mail->get_to();

	//
	//Set Subject
	$e_mail->subject( $args['subject'] );

	//
	//Set Message Body
	$e_mail->message( $args['message'] );
	
	//
	//Deconstruct headers and set their values
	if( !empty( $args['headers'] ) )
	{
		foreach( $args['headers'] as $header )
		{
			list( $name, $content ) = explode( ':', trim ($header), 2 );
			
			$name = trim ( $name );
			$content = trim( $content );
			
			switch( strtolower( $name ))
			{
				case 'from':
					$recipient_name = 'ISL Automated';
					$address = 'no_reply@islonline.org';

					if ( preg_match( '/(.*)<(.+)>/', $content, $matches ) ) 
					{
						if ( count( $matches ) == 3 ) 
						{
							$recipient_name = $matches[1];
							$address        = $matches[2];

						}
					}
 
					$e_mail->from( $recipient_name, $address );
					break;

				case 'content-type':
					if ( strpos( $content, ';' ) !== false ) 
					{
						list( $type, $charset_content ) = explode( ';', $content );
						$content_type = trim( $type );
					
						if ( false !== stripos( $charset_content, 'charset=' ) ) 
						{
							$charset = trim( str_replace( array( 'charset=', '"' ), '', $charset_content ) );
						} 
						elseif ( false !== stripos( $charset_content, 'boundary=' ) ) 
						{
							$boundary = trim( str_replace( array( 'BOUNDARY=', 'boundary=', '"' ), '', $charset_content ) );
							$charset = '';
						}

					// Avoid setting an empty $content_type.
					} 
					elseif ( '' !== trim( $content ) ) 
					{
						$content_type = trim( $content );
					}

					break;

				case 'cc':
					$recipient_name = '';
					$address = $content;

					if( is_array( $content ))
					{
						foreach( $content as $address )
						{
							$recipient_name = '';

							if ( preg_match( '/(.*)<(.+)>/', $address, $matches ) ) 
							{
								if ( count( $matches ) == 3 ) 
								{
									$recipient_name = $matches[1];
									$address        = $matches[2];

								}
							}

							$e_mail->cc( $recipient_name, $address );
						}
					}
					else
					{
						$recipient_name = '';
						$address = $content;
						foreach(explode(',', $address) as $temp_addr)
						{
							if ( preg_match( '/(.*)<(.+)>/', $temp_addr, $matches ) ) 
							{
								if ( count( $matches ) == 3 ) 
								{
									$recipient_name = $matches[1];
									$email_address  = $matches[2];

									$e_mail->cc( $recipient_name, $email_address );
								}
							}
							else
							{
								$e_mail->cc( $temp_addr, $temp_addr );
							}
						}
					}
 
					$args['cc'] = $e_mail->get_cc();
					break;

				case 'bcc':
					$recipient_name = '';
					$address = $content;

					if( is_array( $content ))
					{
						foreach( $content as $address )
						{
							$recipient_name = '';

							if ( preg_match( '/(.*)<(.+)>/', $address, $matches ) ) 
							{
								if ( count( $matches ) == 3 ) 
								{
									$recipient_name = $matches[1];
									$address        = $matches[2];

								}
							}

							$e_mail->bcc( $recipient_name, $address );
						}
					}
					else
					{
						$recipient_name = '';
						$address = $content;
						foreach(explode(',', $address) as $temp_addr)
						{
							if ( preg_match( '/(.*)<(.+)>/', $temp_addr, $matches ) ) 
							{
								if ( count( $matches ) == 3 ) 
								{
									$recipient_name = $matches[1];
									$email_address  = $matches[2];

									$e_mail->bcc( $recipient_name, $email_address );
								}
							}
							else
							{
								$e_mail->bcc( $temp_addr, $temp_addr );
							}
						}
					}
					
					$args['bcc'] = $e_mail->get_bcc();
					break;

				case 'reply-to':
					$recipient_name = '';
					$address = $content;

					if ( preg_match( '/(.*)<(.+)>/', $address, $matches ) ) 
					{
						if ( count( $matches ) == 3 ) 
						{
							$recipient_name = $matches[1];
							$address        = $matches[2];

						}
					}
 
					$e_mail->reply_to( $recipient_name, $address );
					break;

				default:
					// Add it to our grand headers array
					$headers[trim( $name )] = trim( $content );
					break;
			}
				
		}

	}

	//
	//Hack for passworrd reset
	if( $args['subject'] == "[International Service Learning Online] Password Reset" )
	{
		$e_mail->from( 'ISL Password Reset', 'donotreply@islonline.org' );	
	}
	//
	//Default mail options
	$e_mail->clean_language( true );
	$e_mail->group( false );
	
	//
	//Finalize the message and send it
	$try = $e_mail->send();

	if( $try )
	{
		$args['to'] = array();
		$args['subject'] = array();
		$args['message'] = array();
		$args['headers'] = array();
		$args['attachments'] = array();

		$cmd_email_override_sent = true;
	} 
	else
	{
		//echo var_dump($args['to']);
//		$args['to'] = array_merge($args['to'], array('david@illumatech.net'));
		return false;
	}

	return $args;
	
}


function wppb_override ( $wp_error )
{
	global $cmd_email_override_sent;
	
	if(!$cmd_email_override_sent)
	{
		?>
		<script>
			window.location.replace('\');
		</script>
		<?php
		
		wp_die();
	}

	//
	//If a password reset was reqesuted, then shunt back to login.
	if(isset($_REQUEST['_wp_http_referer']) && $_REQUEST['_wp_http_referer'] == '/recover-password/' && 
	   isset($_REQUEST['action']) && $_REQUEST['action'] == 'recover_password' &&
	   $cmd_email_override_sent)
	{
		?>
		<script>
			window.location.replace('/login');
		</script>
		<?php
		
		wp_die();
	}
	
}

/**
 * Gets the page id/object identifier for access control
 */
function get_page_id()
{
	$req_uri = $_SERVER['SCRIPT_NAME'];
	/* Get the page id */
	if($req_uri == "/wp-admin/admin.php")
	{
		$page_id = $_REQUEST['page'];
	}
	/* get the post type */
	elseif($req_uri == "/wp-admin/edit.php")
	{

		if(isset($_REQUEST['post_type']))
		{
			$page_id = $_REQUEST['post_type'];
		}
		else
		{
			$page_id = "edit.php";
		}
	}
	/* get the post type */
	elseif($req_uri == "/wp-admin/post.php")
	{
		if(isset($_REQUEST['post']))
		{
			$page_id = get_post_type($_REQUEST['post']);
		}
		else
		{
			$page_id = $_REQUEST['post_type'];
		}
	}
	else
	{
		$uri_array = explode('/',$req_uri);
		$page_id = end($uri_array);
	}

	return $page_id;
}

// Redefine user notification function
if ( !function_exists('wp_new_user_notification') ) {
    function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
        $user = new WP_User($user_id);

        $user_login = stripslashes($user->user_login);
        $user_email = stripslashes($user->user_email);

        $message  = sprintf(__('New user registration on your blog %s:'), get_option('blogname')) . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
        $message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";

        @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);

        if ( empty($plaintext_pass) )
            return;

        $message  = __('Hi there,') . "\r\n\r\n";
        $message .= sprintf(__("Welcome to %s! Here's how to log in:"), get_option('blogname')) . "\r\n\r\n";
        $message .= wp_login_url() . "\r\n";
        $message .= sprintf(__('Username: %s'), $user_login) . "\r\n";
        $message .= sprintf(__('Password: %s'), $plaintext_pass) . "\r\n\r\n";
        $message .= sprintf(__('If you have any problems, please contact me at %s.'), get_option('admin_email')) . "\r\n\r\n";
        $message .= __('Adios!');

        wp_mail($user_email, sprintf(__('[%s] Your username and password'), get_option('blogname')), $message);

    }
}