<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/** Required Classes */
require_once (CC_CLASSES_DIR . 'trigger.class.php');
require_once (CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . 'rsvp.class.php');
require_once (CC_CLASSES_DIR . 'country.class.php');
require_once (CC_CLASSES_DIR . 'email.class.php');

/** Process team status change triggers */
add_action( 'cmd_team_update_after', 'team_change_triggers', 10, 4 );
add_action( 'cmd_rsvp_create_after', 'new_rsvp_trigger', 10, 2 );
add_action( 'cmd_queue_trigger', 'queue_trigger', 10, 3 );
add_action( 'save_post_cp_team_docs', 'send_document_notification');


/** Trigger for team change events */
function team_change_triggers($team_id, $element, $old, $new)
{
	if($element == 'status')
	{
		global $wpdb;
		$team = new TEAM();
		$team->set_team($team_id);
		
		/** queue status change trigger if existing */
		$trigger = new TRIGGERS();
		$old_v=strtolower($wpdb->get_var("SELECT title from wp_command_team_status WHERE id = $old"));
		$v=strtolower($wpdb->get_var("SELECT title from wp_command_team_status WHERE id = $new"));

		if($team->value('messaging')==1)
		{
			//
			//Schedule
			if($team->get_schedule() == "Unknown")
			{
				$team->blank_schedule(true);
			}
			$current = $team->get_schedule();

			foreach($current as $segment_id => $segment_data)
			{
				$countries[$segment_data['country']] = $segment_data['country'];
			}

			$country = new COUNTRY();
			foreach($countries as $cntry_id)
			{
				$country->set_country($cntry_id);	
				do_action('cmd_queue_trigger', 
						  'Team Status Changed', 
						  array('filter_value' => $country->get_name(), 'old_value' => $old_v, 'new_value' => $v), 
						  array('team_id' => $team_id)
						 );
			}
			/*
			do_action('cmd_queue_trigger', 
					  'Team Status Changed', 
					  array('filter' => $team_id, 'old_value' => $old_v, 'new_value' => $v), 
					  array('team_id' => $team_id)
					 );
			*/
		}

		//
		//Send VEC a notice if the team becomes a Go
		if($new == 9)
		{
			$meta_team_staff_key = 'cmd_team' . $team_id . '_staff_pos';

			$query = "SELECT um.user_id, um.meta_value, u.display_name, u.user_email
						FROM " . $wpdb->prefix . "usermeta um INNER JOIN
							 " . $wpdb->prefix . "users u on u.ID = um.user_id 
					  WHERE meta_key = '" . $meta_team_staff_key . "' AND
							meta_value = 'VEC'
					  ORDER BY display_name";
			$possibles = $wpdb->get_results($query, ARRAY_A);

			foreach($possibles as $data)
			{
				$e_mail = new EMAIL();
				$e_mail->from( 'TC NOTIFY', 'donotreply@islonline.org' );
				$e_mail->to( $data['display_name'], $data['user_email'] );
				$e_mail->subject('TC NOTICE: Team ' . $team->value('hash') . ' is GO!');
				$e_mail->message('<html>Team ' . $team->value('code') . ' is GO!</html>');
				$e_mail->send();
			}
		}

		//$trigger->queue_trigger("Team Status Changed", array('old_value' => $old_v, 'new_value' => $v),array('team_id' => $team_id));
			//Last elemtns was $this, butyg since removed from class, doesn't function - altered to array()
	}
	if($element == '')
	{

	}
}

/** Trigger fon new RSVP's being added to a team */
function new_rsvp_trigger($team_id, $user_id)
{
	$skip_new_sign_up = false;
	
	global $wpdb;
	$team = new TEAM();
	/** get team data */
	$team->set_team($team_id);
	/** compare current number of RSVP's to min and max */
	$rsvp = new RSVP();
	$rsvp_count = $rsvp->count_rsvp($team_id);
	$team_max_volunteers = $team->value('max');
	$team_min_volunteers = $team->value('min');
	$team_status_id = $team->value('status');
	$team_status = strtolower($wpdb->get_var("SELECT title from wp_command_team_status WHERE id = $team_status_id"));
	/** if current number of rsvp's > min volunteers and status = open, change status to go */
	if($rsvp_count >= $team_min_volunteers && $team_status == "open")
	{
		$team->update("status",9);
		$skip_new_sign_up = true;
	}
	/** if current number of rsvp's = max volunteers and status = go, change status to close */
	if($rsvp_count >= $team_max_volunteers && $team_status == "go")
	{
		$team->update("status",2);
	}
	

	
	if(!$skip_new_sign_up && $team->value('messaging')==1)
	{
		//
		//Schedule
		if($team->get_schedule() == "Unknown")
		{
			$team->blank_schedule(true);
		}
		$current = $team->get_schedule();

		foreach($current as $segment_id => $segment_data)
		{
			$countries[$segment_data['country']] = $segment_data['country'];
		}

		$country = new COUNTRY();
		foreach($countries as $cntry_id)
		{
			$country->set_country($cntry_id);	
			do_action('cmd_queue_trigger', 'New Sign-up', array('filter_value' => $country->get_name()), array('team_id' => $team_id, 'user_id' => $user_id));
		}
		do_action('cmd_queue_trigger', 'New Sign-up', array('filter' => 146), array('team_id' => $team_id, 'user_id' => $user_id));
	}

	//
	//2018-03--11 DJT Added VEC Notify to new RSVP
	$user = get_user_by('ID', $user_id);
	$meta_team_staff_key = 'cmd_team' . $team_id . '_staff_pos';

	$query = "SELECT um.user_id, um.meta_value, u.display_name, u.user_email
				FROM " . $wpdb->prefix . "usermeta um INNER JOIN
					 " . $wpdb->prefix . "users u on u.ID = um.user_id 
			  WHERE meta_key = '" . $meta_team_staff_key . "' AND
					meta_value = 'VEC'
			  ORDER BY display_name";
	$possibles = $wpdb->get_results($query, ARRAY_A);

	foreach($possibles as $data)
	{
		$e_mail = new EMAIL();
		$e_mail->from( 'TC NOTIFY', 'donotreply@islonline.org' );
		$e_mail->to( $data['display_name'], $data['user_email'] );
		$e_mail->subject('TC NOTICE: User ' . $user->first_name . ' ' . $user->last_name . ' has joined Team ' . $team->value('hash'));
		//
		//2018-03-22 DJT Changed message to mimic subject
		//$e_mail->message('<html>Team ' . $team->value('code') . ' is GO!</html>');
		$e_mail->message('<html>User ' . $user->first_name . ' ' . $user->last_name . ' has joined Team ' . $team->value('hash') . '</html>');
		$e_mail->send();
	}

}

function queue_trigger($trigger_name, $trigger_args = array(), $unserialized_trigger_object = array())
{
	$trigger = new TRIGGERS();
	$trigger->queue_trigger($trigger_name, $trigger_args, $unserialized_trigger_object);
}

/**
 * Checks if documents are on active teams, resets them, then sends a message
 */
function send_document_notification()
{
	if(isset($_REQUEST['post_id']))
	{
		do_action('cmd_queue_trigger',"Team Document Changed",array(),array('doc_id' => $_REQUEST['post_id']));
	}
}