<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
// Required Classes
require_once (WP_PLUGIN_DIR . '/chatterbox/classes/chatter.class.php');
require_once (WP_PLUGIN_DIR . '/command/classes/rsvp.class.php');
require_once (WP_PLUGIN_DIR . '/command/classes/user.class.php');

//
// Process team status change triggers
add_action('cmd_team_create', 'chatter_team_setup', 10, 2);

add_action('cmd_team_save','chatter_team_setup', 10, 2);
add_action('cmd_team_save','chatter_user_setup', 15, 2);
add_action('cmd_team_save','chatter_staff_setup', 20, 2);

add_action('cmd_chat_check','chatter_user_setup', 15, 2);
add_action('cmd_chat_check','chatter_staff_setup', 20, 2);

add_action('cmd_team_hash_change', 'chatter_team_setup', 20, 2);

add_action('cmd_staff_remove', 'chatter_staff_remove', 18, 2);
add_action('cmd_staff_add', 'chatter_staff_add', 18, 2);

add_action('cmd_checkout', 'chatter_user_add', 18, 2);
add_action('cmd_intern_checkout', 'chatter_user_add', 18, 2);

add_filter('chatterbox_avatar', 'chatter_avatar_override', 10, 1);

add_filter('cmd_unread_count_all', 'chatter_unread_count_all', 10, 1);

add_filter('chatterbox_validate', 'chatter_permission_override', 10, 1);

add_filter('chatterbox_last_read_user', 'chatter_last_read_user', 10, 1);

add_filter('chatterbox_private_chat', 'chatter_priv_chat', 10, 2);

add_filter('chatterbox_admin_check', 'chatter_admin_check', 10, 1);


//
// Trigger for team change events
function chatter_team_setup($team_id, $hash)
{
	$chatter = new CHATTERBOX();
	$chatter->team($team_id);
	$chatter->team_title($hash);
	$chatter->privacy(false);

	//
	//Update if exists, otherwise add
	if($chatter->team_exists($team_id))
	{
		$chatter->update_team();
	}
	else
	{
		$chatter->insert_team();
	}

}

//
// Trigger for team change events
function chatter_user_setup($team_id, $hash)
{
	$chatter = new CHATTERBOX();
	$chatter->team($team_id);
	$chatter->team_title($hash);
	
	//
	//Make sure each team member is set as users and has private chats setup
	$rsvp = new RSVP();
	foreach($rsvp->list_rsvp($team_id) as $member_id)
	{
		if($member_id > 0)
		{
			$chatter->privacy(false);
			$chatter->insert_user($member_id);

			$chatter->set_user($member_id);
			$chatter->privacy(true);

			if($chatter->team_exists($team_id))
			{
				$chatter->update_team();
			}
			else
			{
				$chatter->insert_team();	
			}

			$chatter->insert_user($member_id);

		}
	}
}

//
// Trigger for team change events
function chatter_staff_setup($team_id, $hash)
{
	$chatter = new CHATTERBOX();
	$chatter->team($team_id);
	
	//
	//Gather team stff here
	global $wpdb;
	$query = "SELECT um.user_id, um.meta_value, u.display_name
				FROM " . $wpdb->prefix . "usermeta um INNER JOIN
					 " . $wpdb->prefix . "users u on u.ID = um.user_id 
			  WHERE meta_key = '" . 'cmd_team' . $team_id . '_staff_pos' . "'
			  ORDER BY display_name";
	$possibles = $wpdb->get_results($query, ARRAY_A);

	foreach($possibles as $data)
	{
		$chatter->privacy(false);
		$chatter->insert_user($data['user_id']);
		$chatter->insert_user(0);
		
		//
		//Add staff members to each private chat
		$chatter->privacy(true);

		$rsvp = new RSVP();
		foreach($rsvp->list_rsvp($team_id) as $member_id)
		{
			if($member_id > 0)
			{
				$chatter->set_user($member_id);
				$chatter->insert_user($data['user_id']);
				$chatter->insert_user(0);

			}
		}
	}

}

//
// Trigger for team change events
function chatter_staff_remove($team_id, $user_id)
{
	$chatter = new CHATTERBOX();
	$chatter->team($team_id);
	
	$chatter->privacy(false);
	$chatter->remove_user($user_id);
		
	//
	//Remove staff members to each private chat
	$chatter->privacy(true);

	$rsvp = new RSVP();
	foreach($rsvp->list_rsvp($team_id) as $member_id)
	{
		$chatter->set_user($member_id);
		$chatter->remove_user($user_id);
		$chatter->insert_user(0);
	}

}

//
// Trigger for team change events
function chatter_staff_add($team_id, $user_id)
{
	chatter_staff_setup($team_id, "X");
}


//
// Trigger for team change events
function chatter_user_remove($team_id, $member_id)
{
	$chatter = new CHATTERBOX();
	
	$chatter->team($team_id);
	$chatter->set_user($member_id);
	$chatter->privacy(false);
	
	$chatter->remove_user($member_id);

	$chatter->privacy(true);
	$chatter->remove_user($member_id);
}

//
// Trigger for team change events
function chatter_user_add($team_id, $member_id)
{
	$chatter = new CHATTERBOX();

	$chatter->team($team_id);
	$chatter->set_user($member_id);

	$chatter->privacy(false);
	$chatter->insert_user($member_id);

	$chatter->privacy(true);
	$chatter->insert_user($member_id);

	chatter_staff_setup($team_id, 'X');
}

function chatter_avatar_override($member_id)
{
	$user = new USER();
	return $user->avatar($member_id);
}

function chatter_unread_count_all($member_id)
{
	$chatter = new CHATTERBOX();
	$chatter->set_user($member_id);
	
	return $chatter->unread_by_user();
}

function chatter_permission_override($valid)
{
	$user = new USER();

	if($user->user_group(get_current_user_id())=="super" || $user->user_group(get_current_user_id())=="admin")
	{
		$valid = true;
	}
	
	return $valid;
}

function chatter_last_read_user($user_id)
{
	$user = new USER();

	if($user->user_group(get_current_user_id())=="super" || $user->user_group(get_current_user_id())=="admin")
	{
		$user_id = 0;
	}
	
	return $user_id;
}

function chatter_priv_chat($member_id = 0, $team_id = 0)
{
	$passfail = false;
	
	if(!empty($team_id) && !empty($member_id))
	{
		$chatter = new CHATTERBOX();
		
		$chatter->team($team_id);
		$chatter->set_user($member_id);
		$chatter->privacy(true);
		
		$team_members = $chatter->count_users();
		$passfail = $team_members > 1 ? true : false;
	}
	
	return $passfail;
	
}

function chatter_admin_check($uid)
{
	$user = new USER();

	if($user->user_group($uid)=="super" || $user->user_group($uid)=="admin")
	{
		return 0;
	}
	else
	{
		return NULL;
	}
}