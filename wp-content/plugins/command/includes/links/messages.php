<?php
//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

/** Required Classes */
require_once (CC_CLASSES_DIR . 'trigger.class.php');
require_once (CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . 'rsvp.class.php');

//
/** Process team status change triggers */
add_action('cmd_team_update_after', 'team_change_messages', 10, 4);

//
//
function team_change_messages($team_id, $element, $old, $new)
{
	if($element == 'status')
	{
		/** If current status is open and new value is closed */
		if($old == 4 && $new == 2)
		{
			$message = new MESSAGES();
			$admin_msg = new ADMIN_MESSAGES();
			$message->set_sender(1);
			//$message->set_sender($this->value('poc'));
			$message->set_message_text($admin_msg::$event_types["status_change_open_to_closed"]);
			$message->set_team_id($team_id);
			$message->add_team_message($team_id,"status_change_open_to_closed");
		}
		/** If current status is open and new value is go */
		else if($old == 4 && $new == 9)
		{
			//$message = new MESSAGES();
			//$admin_msg = new ADMIN_MESSAGES();
			//$message->set_sender(1);
			//$message->set_sender($this->value('poc'));
			//$message->set_message_text($admin_msg::$event_types["status_change_open_to_go"]);
			//$message->set_team_id($team_id);
			//$message->add_team_message($team_id,"status_change_open_to_go");
		}
	}

}