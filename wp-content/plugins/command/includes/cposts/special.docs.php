<?php

//
//Make sure there cannot be direct access

defined ('ABSPATH') or die ('No direct access to plugins');

function cc_custom_post_special_docs() {

	//
	//Set parameters to register custome post type
	$labels = array(
    	'name'               => _x( 'Special Docs', 'post type general name' ),
	    'singular_name'      => _x( 'Special Docs', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Team Doc' ),
	    'edit_item'          => __( 'Edit Team Doc' ),
    	'new_item'           => __( 'New Team Doc' ),
    	'all_items'          => __( 'All Team Docs' ),
    	'view_item'          => __( 'View Team Doc' ),
    	'search_items'       => __( 'Search Team Docs' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Team Docs'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Team Document specific data',
    	'public'        => true,
		'show_ui'		=> false,
		'show_in_menu'	=> false,
    	'has_archive'   => true,
  		);
  
  //
  //Register customer post type
  register_post_type( 'cp_special_doc', $args ); 

}

add_action( 'init', 'cc_custom_post_special_docs' );