<?php

//
//Make sure there cannot be direct access

defined ('ABSPATH') or die ('No direct access to plugins');

function cc_custom_post_coupon() {

	//
	//Set parameters to register custom post type
	$labels = array(
    	'name'               => _x( 'Coupons', 'post type general name' ),
	    'singular_name'      => _x( 'Coupon', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Coupon' ),
	    'edit_item'          => __( 'Edit Coupon' ),
    	'new_item'           => __( 'New Coupon' ),
    	'all_items'          => __( 'All Coupons' ),
    	'view_item'          => __( 'View Coupons' ),
    	'search_items'       => __( 'Search Coupons' ),
    	'not_found'          => __( 'No coupons found' ),
    	'not_found_in_trash' => __( 'No coupons found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Coupons'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Coupon specific data',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> true,		//should be false
    	'has_archive'   => true,
		'rewrite' 		=> array('slug' => 'coupon', 'with_front' => false)
  		);
  
  //
  //Register customer post type
  register_post_type( 'isl_coupon', $args ); 

}

add_action( 'init', 'cc_custom_post_coupon' );