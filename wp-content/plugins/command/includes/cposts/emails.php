<?php

//
//Make sure there cannot be direct access

defined ('ABSPATH') or die ('No direct access to plugins');

function cc_custom_post_email() {

	//
	//Set parameters to register custome post type
	$labels = array(
    	'name'               => _x( 'Emails', 'post type general name' ),
	    'singular_name'      => _x( 'Email', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Email' ),
	    'edit_item'          => __( 'Edit Email' ),
    	'new_item'           => __( 'New Email' ),
    	'all_items'          => __( 'All Email' ),
    	'view_item'          => __( 'View Email' ),
    	'search_items'       => __( 'Search Email' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Email'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Team Document specific data',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> false,
    	'has_archive'   => true,
  		);
  
  //
  //Register customer post type
  register_post_type( 'cp_email', $args ); 

}

add_action( 'init', 'cc_custom_post_email' );