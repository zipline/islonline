<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Load Classes
//require_once (CC_CLASSES_DIR . 'team.class.php');
//require_once (CC_CLASSES_DIR . 'team.support.class.php');
require_once (CC_CLASSES_DIR . 'email.class.php');
//require_once (CC_CLASSES_DIR . 'cart.class.php');
//require_once (CC_CLASSES_DIR . 'finance.class.php');

//
//Actions and Filters
add_action('init', 'cc_custom_post_ambassador' );
add_action('save_post_isl_ambassador', 'cc_custom_ambassador_check', 99, 3);

function cc_custom_post_ambassador() {

	//
	//Set parameters to register custom post type
	$labels = array(
    	'name'               => _x( 'Ambassadorships', 'post type general name' ),
	    'singular_name'      => _x( 'Ambassadorship', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'Ambassadors' ),
	    'add_new_item'       => __( 'Add New Ambassador' ),
	    'edit_item'          => __( 'Edit Ambassador' ),
    	'new_item'           => __( 'New Ambassador' ),
    	'all_items'          => __( 'All Ambassador' ),
    	'view_item'          => __( 'View Ambassador' ),
    	'search_items'       => __( 'Search Ambassador' ),
    	'not_found'          => __( 'No Ambassadors found' ),
    	'not_found_in_trash' => __( 'No Ambassadors found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Ambassador'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Ambassador specific data',
    	'public'        => false,
		'show_ui'		=> true,
		'show_in_menu'	=> false,		//should be false
    	'has_archive'   => false,
		'rewrite' 		=> array('slug' => 'ambassador', 'with_front' => false),
  		);
  
  //
  //Register customer post type
  register_post_type( 'isl_ambassador', $args ); 

}


function cc_custom_ambassador_check($post_id, $post, $update)
{
	//
	//Set variables
	$slug = 'ambassador';	
	$mail = new EMAIL();

	//
	//Remove self just in case
	remove_action('save_post_ambassador', 'cc_custom_ambassador_check', 99);
	
	//
	//Check to make sure this is new
	if(wp_is_post_revision($post_id))
	{
		return;
	}

	update_post_meta($post_id, '_cmd_ambassador_request', true);

	//
	//Check to see if the user is logged in, and grab data if so...
	if(is_user_logged_in())
	{
		$user = wp_get_current_user();	
	}
	else
	{
		$user = get_user_by('email', $_POST['acf']['_validate_email']);

		//
		//If there still isn't a user, then we can't find one, so we need to make one...
		if(empty($user))
		{
			//
			//Create a new user
			$user_info['user_login'] = $_POST['acf']['_validate_email'];
			$user_info['user_pass'] = $_POST['acf']['field_58781bc4cde52'];
			$user_info['first_name'] = $_POST['acf']['field_58781baecde51'];
			$user_info['last_name'] = $_POST['acf']['field_58781bc4cde52'];
			$user_info['user_email'] = $_POST['acf']['_validate_email'];

			$new_user = $user->create_user($user_info);
			do_action('cmd_custom_register', $user_info['user_login'], $user_info['first_name'], $user_info['last_name'], $user_info['user_email']);

			//
			//Log in the user
			if($new_user)
			{
				wp_set_current_user($new_user, $user_info['user_login']);
				wp_set_auth_cookie($new_user);
				do_action('wp_login', $user_info['user_login']);		
				$user = wp_get_current_user();
			}
			else
			{
				//
				//Error, cannot find nor create a user
				echo "Really bad error as we should never be here!";
				add_action('save_post_isl_ambassador', 'cc_custom_ambassador_check', 99, 3);
				
				return;
				
			}
		}
	}

	//
	//Write user data post_id link
	update_user_meta($user->ID, '_cmd_comm_ambassador_status', "false");
	update_user_meta($user->ID, '_cmd_comm_ambassador_post', $post_id);
	update_user_meta($user->ID, '_cmd_comm_ambassador_request_date', date("m-d-Y"));
	
	//
	//Gather mail information
	$admin_addresses = explode(",", get_option('cmd_center')['ambassador_submit_email']);
	$headers[] = 'From: ISL Automation <do_not_reply@islonline.org>';
	$headers[] = 'Content-Type: text/html; charset=UTF-8';

	$message = '<p>A new Ambassador Request has come in.</p>';
	$message .= '<p><a href="' . get_option('siteurl') . '/wp-admin/admin.php?page=cc-volunteers&user_id=' . $user->ID . '&cur_tab=vambassador">Click Here</a> to view this User</p>';

	//
	//Send the email
	wp_mail($admin_addresses, 'New Ambassador Request has been submitted Request', $message, $headers);

	add_action('save_post_isl_ambassador', 'cc_custom_ambassador_check', 99, 3);
}