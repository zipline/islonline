<?php

//
//Make sure there cannot be direct access

defined ('ABSPATH') or die ('No direct access to plugins');

function cc_custom_post_faculty_docs() {

	//
	//Set parameters to register custome post type
	$labels = array(
    	'name'               => _x( 'Faculty Docs', 'post type general name' ),
	    'singular_name'      => _x( 'Faculty Doc', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Doc' ),
	    'edit_item'          => __( 'Edit Doc' ),
    	'new_item'           => __( 'New Doc' ),
    	'all_items'          => __( 'All Docs' ),
    	'view_item'          => __( 'View Doc' ),
    	'search_items'       => __( 'Search Docs' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Faculty Docs'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Team Document specific data',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> false,
    	'has_archive'   => true,
  		);
  
  //
  //Register customer post type
  register_post_type( 'cp_faculty_docs', $args ); 

}