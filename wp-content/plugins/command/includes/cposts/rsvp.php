<?php

//
//Make sure there cannot be direct access

defined ('ABSPATH') or die ('No direct access to plugins');

function cc_custom_post_rsvp() {

  //
  //Set parameters to register custom post type
  $labels = array(
      'name'               => _x( 'RSVPs', 'post type general name' ),
      'singular_name'      => _x( 'RSVP', 'post type singular name' ),
      'add_new'            => _x( 'Add New', 'book' ),
      'add_new_item'       => __( 'Add New Team' ),
      'edit_item'          => __( 'Edit Team' ),
      'new_item'           => __( 'New Team' ),
      'all_items'          => __( 'All Teams' ),
      'view_item'          => __( 'View Teams' ),
      'search_items'       => __( 'Search Teams' ),
      'not_found'          => __( 'No docs found' ),
      'not_found_in_trash' => __( 'No docs found in the Trash' ), 
      'parent_item_colon'  => '',
      'menu_name'          => 'RSVP'
      );
    
  $args = array(
      'labels'        => $labels,
      'description'   => 'Holds our Team specific data',
      'public'        => true,
    'show_ui'   => true,
    'show_in_menu'  => false,   //should be false
      'has_archive'   => true,
    'rewrite'     => array('slug' => 'isl_rsvp', 'with_front' => false)
      );
  
  //
  //Register customer post type
  register_post_type( 'isl_rsvp', $args ); 

}

add_action( 'init', 'cc_custom_post_rsvp' );