<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Load Classes
require_once (CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . 'team.support.class.php');
require_once (CC_CLASSES_DIR . 'email.class.php');
require_once (CC_CLASSES_DIR . 'cart.class.php');
require_once (CC_CLASSES_DIR . 'finance.class.php');

//
//Actions and Filters
add_action('init', 'cc_custom_post_intern' );
add_action('init', 'cc_custom_post_intern_app' );
add_action('save_post_isl_intern_app', 'cc_custom_intern_check', 99, 3);

function cc_custom_post_intern() {

	//
	//Set parameters to register custom post type
	$labels = array(
    	'name'               => _x( 'Internship', 'post type general name' ),
	    'singular_name'      => _x( 'Internship', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Internship' ),
	    'edit_item'          => __( 'Edit Internship' ),
    	'new_item'           => __( 'New Internship' ),
    	'all_items'          => __( 'All Internships' ),
    	'view_item'          => __( 'View Internships' ),
    	'search_items'       => __( 'Search Internships' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Internships'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Internship specific data',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> true,		//should be false
    	'has_archive'   => false,
		//'capability_type' => 'page',
		//'hierarchical' 	=> true,
		'query_var'		=> true,
		//'supports'		=> array('page-attributes'),
		'rewrite' 		=> array('slug' => 'internships', 'with_front' => false),
  		);
  
  //
  //Register customer post type
  register_post_type( 'isl_intern', $args ); 

}


function cc_custom_post_intern_app() {

	//
	//Set parameters to register custom post type
	$labels = array(
    	'name'               => _x( 'Internship', 'post type general name' ),
	    'singular_name'      => _x( 'Internship', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Internship' ),
	    'edit_item'          => __( 'Edit Internship' ),
    	'new_item'           => __( 'New Internship' ),
    	'all_items'          => __( 'All Internships' ),
    	'view_item'          => __( 'View Internships' ),
    	'search_items'       => __( 'Search Internships' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Internships'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Internship specific data',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> false,		//should be false
    	'has_archive'   => false,
		'rewrite' 		=> array('slug' => 'intern_app', 'with_front' => true),
  		);
  
  //
  //Register customer post type
  register_post_type( 'isl_intern_app', $args ); 

}

function cc_custom_intern_check($post_id, $post, $update)
{
	//
	//Set variables
	$slug = 'isl_intern_app';	
	$team = new TEAM();
	$mail = new EMAIL();

	
	remove_action('save_post_isl_intern_app', 'cc_custom_intern_check', 99);

	
	//
	//Check to make sure this is new
	if(wp_is_post_revision($post_id))
	{
		return;
	}

	
	//
	//Get a random, unique hash/code
	do
	{
		$hash = rand(1000000000, 1000000000000);	
	} while (!$team->unique_code($hash));


	//
	//Grab the Template data
	if(isset($_REQUEST['id']))
	{
		$cost = preg_replace("/[^0-9]/", "", get_post_meta($_REQUEST['id'], 'starting_cost', true));
		$country_id = get_post_meta($_REQUEST['id'], 'country', true);
		
		//
		//Build an actual Team with some defaults set...
		$team->build(array('hash' => $hash,
						   'type' => 27,
						   'status' => 7,
						   'arrival_city' => '',
						   'arrival_country' => $_REQUEST['country'],
						   'departure_city' => '',
						   'departure_country' => $_REQUEST['country'],
						   'min' => 1,
						   'max' => 1,
						   'cost' => $cost,
						   'appfee' => 0,
						   'deposit' => 0
						));
		$team_id = $team->get_team();

		//
		//Write in Template Schedule
		$begin_date = date_parse_from_format('Ymd', $_REQUEST['acf']['field_585e9e8d07edb']);
		$start_date = mktime(0, 0, 0, $begin_date['month'], $begin_date['day'], $begin_date['year']);
		$end_date = $start_date + (31 * 86400);
		$team->save_segment(0, $country_id, 0, 83, 0, date("m/d/Y", $start_date), date("m/d/Y", $end_date));

		//
		//Update hash to new code after schedule
		$team->update('hash', "#" . $team->value('code'));
		update_post_meta($team->value('post'), '_cmd_internship_request', $post_id);

		//
		//Check to see if the user is logged in, and grab data if so...
		if(is_user_logged_in())
		{
			$user = wp_get_current_user();	
		}
		else
		{
			$user = get_user_by('email', $_POST['acf']['field_585e9e8d07edb']);
			
			//
			//If there still isn't a user, then we can't find one, so we need to make one...
			if(empty($user))
			{
				//
				//Create a new user
				$user_info['user_login'] = $_POST['acf']['field_585e9e8d07edb'];
				$user_info['user_pass'] = $_POST['acf']['field_585e9e8d07edb'];
				$user_info['first_name'] = 'First Name';
				$user_info['last_name'] = 'Last Name';
				$user_info['user_email'] = $_POST['acf']['field_585e9e8d07edb'];

				$new_user = $user->create_user($user_info);
				do_action('cmd_custom_register', $user_info['user_login'], $user_info['first_name'], $user_info['last_name'], $user_info['user_email']);

				//
				//Log in the user
				if($new_user)
				{
					wp_set_current_user($new_user, $user_info['user_login']);
					wp_set_auth_cookie($new_user);
					do_action('wp_login', $user_info['user_login']);		
					$user = $new_user;
				}
				else
				{
					//
					//Error, cannot find nor create a user
				}
			}
		}
		
		//
		//Add user to team
		if(!empty($user) && 1==1)
		{
			//
			//Start the new team
			$cart = new CART();
			//$cart->add_item(1, $team->value('id'), 0, $team->value('cost'), 1);
			$cart->add_RSVP($team->value('id'), $team->value('cost'), 1);
			$trans_data = array('amount' => 0,
					'form' => 'No Payment',
					'response' => 'Approved',
					'transaction_id' => date('Ymdhis'),
					'raw_request' => 'No Request Data,  No Payment Due',
					'raw_response' => 'Approved with 0 down'
					);

			$finance = new FINANCE();
			$finance->new_transaction($trans_data);	

			$finance->move_CartToOrder($cart);
			$items = 'All';
			$cart->Clear();	

			do_action('cmd_intern_checkout', $team->value('id'), $user->ID);
		}

		//
		//Gather mail information
		$cmd_options = get_option('cmd_center');
		$to_list = $cmd_options['request_internship_email'];

		//
		//Construct email to staff
		$to_array = explode(',', $to_list);
		foreach($to_array as $to) 
		{
			$mail->to(trim($to), trim($to));
		}
		$mail->from('ISL Automation', 'do_not_reply@islonline.org');
		$mail->reply_to('ISL Automation', 'do_not_reply@islonline.org');
		$mail->subject('New Internship Request has been submitted');

		$message = '';
		$message .= '<p>A new Internship Request has come in.</p>';
		$message .= '<p><a href="' . get_option('siteurl') . '/wp-admin/admin.php?page=cc-teams&team_id=' . $team->value('id') . '">Click Here</a> to view this team</p>';

		$options = array("MIME-Version: 1.0\r\n", "Content-Type: text/html; charset=ISO-8859-1\r\n");

		$mail->options($options);
		$mail->message($message);
		$mail->send();
	}

	add_action('save_post_isl_intern_app', 'cc_custom_intern_check', 99, 3);

}