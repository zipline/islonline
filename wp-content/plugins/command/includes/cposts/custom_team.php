<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Load Classes
require_once (CC_CLASSES_DIR . 'team.class.php');
require_once (CC_CLASSES_DIR . 'team.support.class.php');
require_once (CC_CLASSES_DIR . 'email.class.php');

//
//Add actions for the post type
add_action( 'init', 'cc_custom_team' );
add_action('save_post_custom_team', 'cc_custom_team_check');

function cc_custom_team() {

	//
	//Set parameters to register custome post type
	$labels = array(
    	'name'               => _x( 'Custom Teams', 'post type general name' ),
	    'singular_name'      => _x( 'Custom Team', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New' ),
	    'edit_item'          => __( 'Edit Team' ),
    	'new_item'           => __( 'New Team' ),
    	'all_items'          => __( 'All Teams' ),
    	'view_item'          => __( 'View Team' ),
    	'search_items'       => __( 'Search Teams' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => ''
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Custom Team Requests',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> false,
    	'has_archive'   => true,
  		);
  
  //
  //Register customer post type
  register_post_type( 'custom_team', $args ); 

}

function cc_custom_team_check($post_id)
{
	//
	//Set variables
	$slug = 'custom_team';	
	$team = new TEAM();
	$mail = new EMAIL();

	//
	//Log in the special user
	$user = get_user_by( 'ID' , 2 );

	if($user)
	{
		wp_set_current_user($user, $user->user_login);
		wp_set_auth_cookie($user);
		do_action('wp_login', $user->user_login);		
	
		try
		{
			//
			//Check to make sure this is new
			//if(wp_is_post_revision($post_id))
			//{
			//	return;
			//}

			//
			//Get a random, unique hash/code
			do
			{
				$hash = rand(1000000000, 1000000000000);	
			} while (!$team->unique_code($hash));

			//
			//Build an actual Team with some defaults set...
			$team->build(array('hash' => $hash,
							   'type' => 3,
							   'status' => 7,
							   'arrival_city' => 'Unknown',
							   'arrival_country' => 'Unknown',
							   'departure_city' => 'Unknown',
							   'departure_country' => 'Unknown',
							   'min' => (!empty($_POST['acf']['field_57bd418415e87']) ? $_POST['acf']['field_57bd418415e87'] : 10),
							   'max' => (!empty($_POST['acf']['field_57bd418415e87']) ? $_POST['acf']['field_57bd418415e87'] : 10),
							   'cost' => 0,
							   'appfee' => 85,
							   'deposit' => 200
							));

			$team_id = $team->get_team();
			$team->update('hash', "#" . $team->value('code'));
			update_post_meta($team->value('post'), '_cmd_custom_team_build', $post_id);
			update_post_meta($team->value('post'),"_chart_note",$_POST['acf']['field_57bd4038a7cbd']);

			$cmd_options = get_option('cmd_center');
			$to_list = $cmd_options['create_a_team_email'];

			$to_array = explode(',', $to_list);
			foreach($to_array as $to) 
			{
				$mail->to(trim($to), trim($to));
			}
			$mail->from('ISL Automation', 'do_not_reply@islonline.org');
			$mail->reply_to('ISL Automation', 'do_not_reply@islonline.org');
			$mail->subject('New Custom Team Request has been submitted');


			$message = '';
			$message .= '<p>A new Custom Team Request has come in from ' . $_POST['acf']['field_57bd4038a7cbd'] . ".</p>";

			if($_POST['acf']['field_5aac2c0a630a3'] == 'yes')
			{
				$message .= '<p>' . $_POST['acf']['field_57bd4038a7cbd'] . " is an ISL Ambassador.</p>";
			}
			
			$message .= '<p>' . $_POST['acf']['field_57bd4038a7cbd'] . " is ";
			if(in_array('Affiliated with a school?', $_POST['acf']['field_57bd412fa7cc2']) || in_array('Affiliated with a group/organization?', $_POST['acf']['field_57bd412fa7cc2']))
			{
				if(in_array('Affiliated with a school?', $_POST['acf']['field_57bd412fa7cc2']))
				{
					//
					//2018-04-16 DJT added clarification of faculty or instrucotr
					if(isset($_POST['acf']['field_5aab31a865201']) && $_POST['acf']['field_5aab31a865201'] == 'yes')
					{
						$message .= ' a school instructor OR faculty member of ' . $_POST['acf']['field_592f50d3e0c7e'];
					}
					else
					{
						$message .= ' affiliated with ' . $_POST['acf']['field_592f50d3e0c7e'];
					}
				}
				if(in_array('Affiliated with a group/organization?', $_POST['acf']['field_57bd412fa7cc2']))
				{
					$message .= ' affiliated with ' . $_POST['acf']['field_592f50fae0c7f'];
				}
			}
			else
			{
				$message .= ' not affiliated with a school or organization';
			}
			$message .= '</p>';
			
			$message .= '<p>They are interested in: </p>';
			
			$message .= "<p>Programs: ";
			foreach($_POST['acf']['field_57bd42fc15e88'] as $index => $program)
			{
				if($program == 79)
					$message .= 'Athletic Training, ';
				if($program == 18)
					$message .= 'Community Enrichment, ';
				if($program == 5)
					$message .= 'Dentistry, ';
				if($program == 80)
					$message .= 'Gap Year, ';
				if($program == 63)
					$message .= 'Global Health, ';
				if($program == 30)
					$message .= 'Hike for Humanity, ';
				if($program == 83)
					$message .= 'Internships, ';
				if($program == 59)
					$message .= 'Medical Teams, ';
				if($program == 9)
					$message .= 'Nursing, ';
				if($program == 11)
					$message .= 'Optometry, ';
				if($program == 65)
					$message .= 'Pharmacy, ';
				if($program == 8)
					$message .= 'Physical Therapy, ';
				if($program == 84)
					$message .= 'Sports Without Borders, ';
				if($program == 1)
					$message .= 'Veterinary, ';
				if($program == 2)
					$message .= 'Well Child Init, ';
			}
			$message = substr($message, 0, -2) . '</p>';

			$message .= '<p>Countries: ';
			foreach($_POST['acf']['field_57bd432115e89'] as $index => $country)
			{
				if($country == 4)
					$message .= 'Belize, ';
				if($country == 32)
					$message .= 'Colombia, ';
				if($country == 2)
					$message .= 'Costa Rica, ';
				if($country == 5)
					$message .= 'Dominican Republic, ';
				if($country == 37)
					$message .= 'Haiti, ';
				if($country == 41)
					$message .= 'India, ';
				if($country == 33)
					$message .= 'Jamaica, ';
				if($country == 8)
					$message .= 'Mexico, ';
				if($country == 9)
					$message .= 'Nicaragua, ';
				if($country == 6)
					$message .= 'Panama, ';
				if($country == 30)
					$message .= 'Peru, ';
				if($country == 43)
					$message .= 'Spain, ';
				if($country == 3)
					$message .= 'Tanzania, ';
			}
			$message = substr($message, 0, -2) . '</p>';

			$message .= '<p>They are looking to travel on or around ' . $_POST['acf']['field_57bd435915e8a'] . ' for ' . $_POST['acf']['field_57bd438215e8b'] . ' days.</p>';

			$message .= '<p>They are estimating ' . $_POST['acf']['field_57bd418415e87'] . ' volunteers will be travelling.</p>';

			$message .= '<p>' . $_POST['acf']['field_57bd4038a7cbd'] . ' would like to be contacted by: ';
			if($_POST['acf']['field_57bd40e9a7cc1'] == 'Email')
			{
				$message .= 'Email</p><p>Email: ' . $_POST['acf']['field_57bd405da7cbe'] . '</p>';
			}
			if($_POST['acf']['field_57bd40e9a7cc1'] == 'Phone')
			{
				$message .= 'Phone</p><p>Phone: ' . $_POST['acf']['field_57bd4076a7cbf'] . '</p>'; 
			}

			$message .= '<p><a href="' . get_option('siteurl') . '/wp-admin/admin.php?page=cc-teams&team_id=' . $team_id . '">Click Here</a> to view this team</p>';

			$options = array("MIME-Version: 1.0\r\n", "Content-Type: text/html; charset=ISO-8859-1\r\n");

			$mail->options($options);
			$mail->message($message);
			$mail->send();

			wp_logout();

		}
		catch (Exception $e)
		{
			wp_logout();
		}
		
	}
}