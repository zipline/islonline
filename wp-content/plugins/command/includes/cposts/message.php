<?php

//
//Make sure there cannot be direct access

defined ('ABSPATH') or die ('No direct access to plugins');

function cc_custom_post_message() {

	//
	//Set parameters to register custome post type
	$labels = array(
    	'name'               => _x( 'Messages', 'post type general name' ),
	    'singular_name'      => _x( 'MEssage', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Message' ),
	    'edit_item'          => __( 'Edit Message' ),
    	'new_item'           => __( 'New Message' ),
    	'all_items'          => __( 'All Messages' ),
    	'view_item'          => __( 'View Messages' ),
    	'search_items'       => __( 'Search Messages' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Message'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Message from Jonathan',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> false,
    	'has_archive'   => true,
  		);
  
  //
  //Register customer post type
  register_post_type( 'message', $args ); 

}

add_action( 'init', 'cc_custom_post_message' );