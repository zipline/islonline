<?php

//
//Make sure there cannot be direct access

defined ('ABSPATH') or die ('No direct access to plugins');

add_action( 'init', 'cc_custom_post_team_docs' );

function cc_custom_post_team_docs() {

	//
	//Set parameters to register custome post type
	$labels = array(
    	'name'               => _x( 'Team Docs', 'post type general name' ),
	    'singular_name'      => _x( 'Team Docs', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
	    'add_new_item'       => __( 'Add New Team Doc' ),
	    'edit_item'          => __( 'Edit Team Doc' ),
    	'new_item'           => __( 'New Team Doc' ),
    	'all_items'          => __( 'All Team Docs' ),
    	'view_item'          => __( 'View Team Doc' ),
    	'search_items'       => __( 'Search Team Docs' ),
    	'not_found'          => __( 'No docs found' ),
    	'not_found_in_trash' => __( 'No docs found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Team Docs'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Holds our Team Document specific data',
        'hierarchical'  => false,
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> false,
    	'has_archive'   => true,
  		);
  
  //
  //Register customer post type
  register_post_type( 'cp_team_docs', $args ); 

}

add_filter( "manage_cp_team_docs_posts_columns", "set_custom_cp_team_docs_columns", 20, 1);
add_filter( "post_row_actions", "custom_cp_team_docs_row_mods", 20, 2);
add_action( "manage_cp_team_docs_posts_custom_column", "custom_cp_team_docs_columns", 10, 2);

function set_custom_cp_team_docs_columns($columns)
{
    unset($columns);

	$columns = array(
					'cb'        	=> '<input type="checkbox" />',
					'document' 		=> 'Document',
					'document_type'	=> 'Document Type',
					'author'		=> 'Author',
					'date'			=> 'Date'
					);

	return $columns;
}

function custom_cp_team_docs_row_mods($actions, $post)
{
	if($post->post_type == "cp_team_docs")
	{
		unset($actions['view']);	

		$doc_type = get_post_meta($post->ID, 'resource_type', true);
		$signature = get_post_meta($post->ID, 'signature_required', true);
		$classname = '';

		if(empty($signature) || $signature == false || $doc_type=='PDF Document')
		{
			$classname = "class='no_req_check' name='" . $post->ID . "'";
		}
		
		switch($doc_type)
		{
			case 'HTML Page':
				$actions['view'] = "<a href='' onclick=\"window.open('read-info-docs?doc_id=" . $post->ID . "&rsvp=0&post_id=0', 'newwindow', 'width=800, height=800, menubar=yes')\" " . $classname . ">View</a>";
				break;
			case 'URL':
				$url = get_post_meta($post->ID, 'url', true);
				$actions['view'] = "<a href='" . $url . "' " . $classname . " id='doc_check_" . $post->ID . "' target='_blank'>View</a>";
				break;
			case 'PDF Document':
				$pid = get_post_meta($post->ID, 'pdf_document', true);
				$actions['view'] = "<a href='" . get_the_guid($pid) . "' " . $classname . " id='doc_check_" . $post->ID . "' target='_blank'>View</a>";
				break;
			default:
				break;
		}
	}
	
	return $actions;
}

function custom_cp_team_docs_columns($column, $post_id)
{
    
	switch($column)
	{
		case 'document':
			echo "<a href='" . site_url() . "/wp-admin/post.php?post=" . $post_id . "&action=edit'>" . get_the_title($post_id) . "</a>";
			break;
		case 'document_type':
			echo the_field('resource_type', $post_id);
			break;
		default:
			break;
	}
	
}