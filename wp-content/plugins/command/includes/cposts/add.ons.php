<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
//Add actions for the post type
add_action( 'init', 'cc_add_ons' );

function cc_add_ons() {

	//
	//Set parameters to register custome post type
	$labels = array(
    	'name'               => _x( 'Add-ons', 'post type general name' ),
	    'singular_name'      => _x( 'Add-ons', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'add-on' ),
	    'add_new_item'       => __( 'Add New' ),
	    'edit_item'          => __( 'Edit Add-ons' ),
    	'new_item'           => __( 'New Add-on' ),
    	'all_items'          => __( 'All Add-ons' ),
    	'view_item'          => __( 'View Add-ons' ),
    	'search_items'       => __( 'Search Add-ons' ),
    	'not_found'          => __( 'No add-ons found' ),
    	'not_found_in_trash' => __( 'No add-ons found in the Trash' ), 
    	'parent_item_colon'  => '',
    	'menu_name'          => 'Add-ons'
  		);
  	
	$args = array(
    	'labels'        => $labels,
    	'description'   => 'Add-ons',
    	'public'        => true,
		'show_ui'		=> true,
		'show_in_menu'	=> true,
    	'has_archive'   => true,
		'supports'		=> array(
								'title',
								'thumbnail',
								'excerpt',
								'revisions',
								'custom-fields'
								),
		'rewrite' 		=> array('slug' => 'add-on', 'with_front' => false)
  		);
  
  //
  //Register customer post type
  register_post_type( 'team_add_ons', $args ); 

}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_57ea02a002da0',
	'title' => 'Team Add On',
	'fields' => array (
		array (
			'key' => 'field_59747638e962a',
			'label' => 'Delivery',
			'name' => 'delivery',
			'type' => 'radio',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Direct Ship' => 'Direct Ship',
				'Program Related' => 'Program Related',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array (
			'key' => 'field_597475bbe9629',
			'label' => 'Pay Type',
			'name' => 'pay_type',
			'type' => 'radio',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'POS' => 'POS',
				'Team' => 'Team',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array (
			'key' => 'field_57ea034df9e3d',
			'label' => 'Cost',
			'name' => 'cost',
			'type' => 'number',
			'instructions' => 'Enter the dollar amount for the add-on',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => 0,
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_597475aae9628',
			'label' => 'Excerpt',
			'name' => 'excerpt',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_57ea037af9e3e',
			'label' => 'Description',
			'name' => 'description',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 0,
		),
		array (
			'key' => 'field_57ea04f1f9e3f',
			'label' => 'Documents',
			'name' => 'documents',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => 'Add Row',
			'sub_fields' => array (
				array (
					'key' => 'field_57ea05a8f9e40',
					'label' => 'Doc',
					'name' => 'doc',
					'type' => 'file',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'url',
					'library' => 'all',
					'min_size' => '',
					'max_size' => '',
					'mime_types' => '',
				),
			),
		),
		array (
			'key' => 'field_5974485eac65a',
			'label' => 'Featured Image',
			'name' => 'featured_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'team_add_ons',
			),
		),
	),
	'menu_order' => 4,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'custom_fields',
		3 => 'discussion',
		4 => 'comments',
		5 => 'revisions',
		6 => 'author',
		7 => 'format',
		8 => 'page_attributes',
		9 => 'featured_image',
		10 => 'categories',
		11 => 'tags',
		12 => 'send-trackbacks',
	),
	'active' => 1,
	'description' => '',
));

endif;