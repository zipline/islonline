jQuery(document).ready(function()
{
	var text_select = jQuery('#school_select').find('input[type="text"]')[0].id;

	jQuery('#' + text_select).autocomplete(
	{
		minLength: 3,
		delay: 750,
		source: function (request, response)
		{
			var term = request.term;

			var data = 
			{	
				'action': 'cmd_school_select',
				'search_term': term,
			};
					
			jQuery.ajax(
			{
				type: "POST",
				url: svars.ajax_url,
				data: data,
				
				success:function(data)
				{
					response(JSON.parse(data));
				},
				error:function(xml, status, error)
				{
					response(data);
					//alert('Error: ' + error);
				}
			});
		}
		
	});
});

/*
	var timer;
	
	$('.school_select').on('keyup', function()
	{
		clearTimeout(timer);
		timer = setTimeout( function()
		{
			var fld_name = $('.school_select').attr('data-key');
			var pass_thru = $('#acf-' + fld_name).val();
			alert(pass_thru);
			
			var data = 
			{
				'action': 'cmd_school_select',
				'text': pass_thru,
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: svars.ajax,
				data: data,
				success:function(data)
				{
					alert(data);
				},
				error:function(xml, status,error)
				{
					//alert('Cart Count Error: ' + error);
				}
			});
			
		}, 750);
	});
});

*/