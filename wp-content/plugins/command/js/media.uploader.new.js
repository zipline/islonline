jQuery(function()
{
  jQuery(document).ready(function()
	{
    jQuery('.image_add_btn').click(function()
		{
			var obj = this;
      		open_media_window(this);
		});
  });

	function open_media_window(obj)
	{
		var fld_id = jQuery(obj).attr('id').substring(16);
    	image_src = 'image_src_' + fld_id;
		image_url = 'image_url_' + fld_id;
		image_id = 'image_id_' + fld_id;

		if (obj.window === undefined) {
			obj.window = wp.media({
				title: 'Choose an Image',
				library: {type: 'image'},
				multiple: false,
				button: {text: 'Select'}
			});

			var self = obj; // Needed to retrieve our variable in the anonymous function below

			obj.window.on('select', function() {
				var first = self.window.state().get('selection').first().toJSON();
				
				jQuery('#' + fld_id).attr('value',first.id);
				jQuery('#' + image_src).attr('src',first.url);
				jQuery('#' + image_url).attr('value',first.url);
				jQuery('#' + image_id).attr('value',first.id);
				
			});
		}
		obj.window.open();
		return false;
	}

});
