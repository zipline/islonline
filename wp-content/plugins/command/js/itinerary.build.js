
jQuery(document).ready(function($)
{

	var replace = 0;

	jQuery('#itin_name').click(function()
	{
		$(this).attr('hidden', true);
		$('#itin_box').attr('hidden', false);	
	});

	jQuery('#itin_box').keydown(function(e){
		if(e.keyCode == 13)
		{
			var label = $('#itin_box').val();
			$('#itin_name').html(label);
			$('#itin').val(label);
			$(this).attr('hidden', true);
			$('#itin_name').attr('hidden', false);	
		}
	});

	jQuery('#itinerary_table').on('click', ".delete", function()
	{
		$(this).parent().parent().remove();

		$('span.dayholder').each(function(i) {
			var $this = $(this);
			$this.text(i+1);
		});

	});
	jQuery('#itinerary_table').on('click', ".addafter", function()
	{
		var currow = $(this).parent().parent();
		var curcnt = currow.find(".dayholder").attr('name');
		var orow = currow.next();
		if (!orow.length)
		{
			replace = parseInt(curcnt) + 10000;	
		}
		else
		{
			var tval = orow.find(".dayholder").attr('name');
			replace = Math.ceil(((parseInt(tval) - parseInt(curcnt)) / 2) + parseInt(curcnt)); 			
		}
		var nextarea = "desc_" + replace;
		var nexttitl = "titl_" + replace;
		
		var RowToAdd = "";
		RowToAdd += "<TR>";
		RowToAdd += "	<TD align=\"center\">";
		RowToAdd += "		<input type=\"button\" class=\"button addprior\" value=\"Add Prior\">";
		RowToAdd += "			<BR><BR>";
		RowToAdd += "		<input type=\"button\" class=\"button delete\" value=\"Delete\">";
		RowToAdd += "			<BR><BR>";
		RowToAdd += "		<input type=\"button\" class=\"button addafter\" value=\"Add After\">";
		RowToAdd += "	</TD>";
		RowToAdd += "	<TD><strong>Day: </strong><span class=\"dayholder\" name=\"" + replace + "\">1</span></TD>";
		RowToAdd += "	<TD><textarea cols=100 rows=1 autocomplete=\"off\"";
		RowToAdd += "				  name=\"" + nexttitl + "\" id=\"" + nexttitl + "\"";
		RowToAdd += "				  placeholder=\"title\"></textarea>";
		RowToAdd += "	<BR>";
		RowToAdd += "	<textarea cols=40 rows=5 autocomplete=\"off\"";
		RowToAdd += "				  name=\"" + nextarea + "\" id=\"" + nextarea + "\">";
		RowToAdd += "		 	Description";
		RowToAdd += "		</textarea>";
		RowToAdd += "	</TD>";
		RowToAdd += "</TR>";    

		currow.after(RowToAdd);

		$(this).parent().find(nextarea).ready(function()
		{
			tinyMCE.execCommand('mceAddEditor', false, nextarea);
		});
		
		$('span.dayholder').each(function(i) {
			var $this = $(this);
			$this.text(i+1);
		});
		
	});
	jQuery('#itinerary_table').on('click', '.addprior', function()
	{
		var nextarea = "desc_" + replace;
		var currow = $(this).parent().parent();
		var curcnt = currow.find(".dayholder").attr('name');
		var orow = currow.prev();
		if (!orow.length)
		{
			replace = parseInt(curcnt) - 1000;	
		}
		else
		{
			var tval = orow.find(".dayholder").attr('name');
			replace = Math.floor(((parseInt(curcnt) - parseInt(tval)) / 2) + parseInt(tval)); 			
		}
		var nextarea = "desc_" + replace;
		var nexttitl = "titl_" + replace;

		var RowToAdd = "";
		RowToAdd += "<TR>";
		RowToAdd += "	<TD align=\"center\">";
		RowToAdd += "		<input type=\"button\" class=\"button addprior\" value=\"Add Prior\">";
		RowToAdd += "			<BR><BR>";
		RowToAdd += "		<input type=\"button\" class=\"button delete\" value=\"Delete\">";
		RowToAdd += "			<BR><BR>";
		RowToAdd += "		<input type=\"button\" class=\"button addafter\" value=\"Add After\">";
		RowToAdd += "	</TD>";
		RowToAdd += "	<TD><strong>Day: </strong><span class=\"dayholder\" name=\"" + replace + "\">1</span></TD>";
		RowToAdd += "	<TD><textarea cols=100 rows=1 autocomplete=\"off\"";
		RowToAdd += "				  name=\"" + nexttitl + "\" id=\"" + nexttitl + "\"";
		RowToAdd += "				  placeholder=\"title\"></textarea>";
		RowToAdd += "	  <BR>";
		RowToAdd += "	<textarea cols=40 rows=5 autocomplete=\"off\"";
		RowToAdd += "				  name=\"" + nextarea + "\" id=\"" + nextarea + "\">";
		RowToAdd += "		 	Description";
		RowToAdd += "	</textarea>";
		RowToAdd += "	</TD>";
		RowToAdd += "</TR>";    

		$(this).parent().parent().before(RowToAdd);

		$(this).parent().find(nextarea).ready(function()
		{
			tinyMCE.execCommand('mceAddEditor', false, nextarea);
		});

		$('span.dayholder').each(function(i) {
			var $this = $(this);
			$this.text(i+1);
		});

	});

});