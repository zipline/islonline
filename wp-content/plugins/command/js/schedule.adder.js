// OLD:
// $(document).ready(function($) {
// 	var datepickerOptions = {
// 		dateFormat : 'mm/dd/yy'
// 		,changeMonth : true
// 		,changeYear : true
// 		,defaultDate: 0
// 		,minDate: 0
// 		// ,onSelect: function(theDate, ui) {
// 		// 	console.log(theDate, ui);
// 		// }
// 		,beforeShow: function() {
// 			$('#ui-datepicker-div').addClass('open');
// 		}
// 		,onClose: function() {
// 			$('#ui-datepicker-div').removeClass('open');
// 		}
// 	};

// 	$('#schedule_table').on('focusin', '.datepicker', function() {
// 		$(this).datepicker(datepickerOptions);
// 	});
// });

jQuery(document).ready(function($) {
	$('#schedule_table').on('change', '.country', function() {
		var cntry = $(this).val();
		var full_cntry = $(this).attr('id');
		var element_id = full_cntry.substr(full_cntry.indexOf("_")+1);
		var region = '#region_' + element_id;

		var data = {
			'action': 'cmd_region_load',
			'country': cntry
		};

		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data,
			success:function(data)
			{
				$(region).html(data);
			},
			error:function(xml, status,error)
			{
				alert('Error: ' + error);
			}
		});
	});

	// Dates:
	var datepickerOptions = {
		dateFormat : 'mm/dd/yy'
		,changeMonth : true
		,changeYear : true
		,defaultDate: 0
		,minDate: 0
		,beforeShow: function() {
			$('#ui-datepicker-div').addClass('open');
		}
		,onClose: function() {
			$('#ui-datepicker-div').removeClass('open');
		}
	};
	$('#schedule_table').on('focusin', '.datepicker', function() {
		var segmentId = $(this).attr('id').substring(4);

		// If this is an "Ending date" we need to set the "minDate" to the date of the Starting Date
		if ($(this).hasClass('endDate')) {
			var startInput = $('#start_' + segmentId);
			var startDate = new Date(startInput.val());

			// If the startDate is a valid date, then we add it's value to the
			// datepickers options as the "minDate" (or "starting date")
			if (startDate.getTime() === startDate.getTime()) {
				$.extend(datepickerOptions, {
					minDate: startDate
				});
			}
		} 

		// Either way, we're going to fire up the datepicker now...
		$(this).datepicker(datepickerOptions);
	}); 
		
	$('#schedule_table').on('change', '.datepicker', function() {
		var segmentId = $(this).data('segment-id');
		var end = $('#end_' + segmentId);
		var start = $('#start_' + segmentId);
		var dur = $('#days_' + segmentId);

		if (end.val() && end.val().length==10) {
			end_date = new Date(end.val());
		} else {
			end_date = 0;
		}
		
		if (start.val() && start.val().length==10) {
			start_date = new Date(start.val());
		} else {
			start_date = 1;
		}

		if (end_date > start_date) {
			duration = Math.round((( end_date - start_date ) / 1000 / 60 / 60 / 24 ) + 1);
			dur.val(duration);
		} else {
			dur.val('');
		}
	}); 

	$('#schedule_table').on('click', ".delete", function() {
		$(this).closest('tr').remove();
	});

	var newIDKey = 0;
	$('#schedule_table').on('click', ".add_sched", function() {
		var currentRow = $(this).closest('tr');
		var currentCount = currentRow.find(".cnthold").val();
		newIDKey = parseFloat(currentCount) + 1000000;	

		var newRow = "";
		newRow += "<tr>";
		newRow += "    <td>";
		newRow += "        <input type='hidden' class='cnthold' name='key_" + newIDKey + "' id='key_" + newIDKey + "' value='" + newIDKey + "'>";
        newRow += "        <select style='" + svars.style + "' name='country_" + newIDKey + "' id='country_" + newIDKey + "' class='country'>" + svars.country_select + "</select>";
		newRow += "    </td>";
		newRow += "    <td>";
        newRow += "        <select style='" + svars.style + "' name='region_" + newIDKey + "' id='region_" + newIDKey + "' class='region'></select>";
		newRow += "    </td>";
		newRow += "    <td>";
        newRow += "        <select style='" + svars.style + "' name='program_" + newIDKey + "' id='program_" + newIDKey + "' class='program'>" + svars.program_select + "</select>";
		newRow += "    </td>";
		newRow += "    <td>";
		newRow += "        <input data-segment-id='" + newIDKey + "' style='" + svars.style + "' name='start_" + newIDKey + "' id='start_" + newIDKey + "' class='regular-text datepicker startDate' type='text' value=''>";
		newRow += "    </td>";
		newRow += "    <td>";
		newRow += "        <input data-segment-id='" + newIDKey + "' style='" + svars.style + "' name='end_" + newIDKey + "' id='end_" + newIDKey + "' class='regular-text datepicker endDate' type='text' value=''>";
		newRow += "    </td>";
        newRow += "    <TD>";
        newRow += "    		<input data-segment-id='" + newIDKey + "' style='" + svars.short_style + "' name='days_" + newIDKey + "' id='days_" + newIDKey + "' class='regular-text' type='text' value=''>";
        newRow += "    </TD>";
		newRow += "    <td>";
		newRow += "        <input type='button' name='delete_" + newIDKey + "' id='delete_" + newIDKey + "' class='button delete secondary' value='Delete'>";
		newRow += "        <input type='button' name='add_" + newIDKey + "' id='add_" + newIDKey + "' class='button add_sched' value='Add'>";
		newRow += "    </td>";
		newRow += "</tr>";

		currentRow.after(newRow);
	});
});


