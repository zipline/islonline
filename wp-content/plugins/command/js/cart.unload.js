jQuery(document).ready(function($)
{
	jQuery('.unload').on('click', function()
	{
		var index = jQuery(this).attr('id').substr(jQuery(this).attr('id').lastIndexOf("_")+1);

		var item_id = jQuery('#item_id_' + index).val();
		var item_quan = jQuery('#item_quan_' + index).val();
		var item_type = jQuery('#item_type_' + index).val();

		var data = 
		{
			'action': 'cmd_cart_remove',
			'item_id': item_id,
			'item_quan': item_quan,
			'item_type': item_type,
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: ajax_path.ajax_url,
			data: data,
			success:function(data)
			{
				var url = '/cart';
				var form = $('<form action="' + url + '" method="post">' +
				  '<input type="text" name="cart_refer" value="' + window.location + '" />' +
				  '</form>');
				$('body').append(form);
				form.submit();
				//window.location = "/cart?refer=" + window.location;
				//alert('Data: ' + data);
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
	});
});

jQuery(document).ready(function($)
{
	jQuery('.empty').on('click', function()
	{
		var index = jQuery(this).attr('id').substr(jQuery(this).attr('id').lastIndexOf("_")+1);

		var main_item = jQuery('#item_id_' + index).val();
		var item_quan = jQuery('#item_quan_' + index).val();
		var item_type = jQuery('#item_type_' + index).val();
		var item_id = jQuery('#add_on_' + index).val();

		var data = 
		{
			'action': 'cmd_cart_remove',
			'item_id': item_id,
			'main_item': main_item,
			'item_quan': item_quan,
			'item_type': item_type,
		};
		
		jQuery.ajax(
		{
			type: 'POST',
			url: ajax_path.ajax_url,
			data: data,
			success:function(data)
			{
console.log(data);
				var url = '/cart';
				var form = $('<form action="' + url + '" method="post">' +
				  '<input type="text" name="cart_refer" value="' + window.location + '" />' +
				  '</form>');
				$('body').append(form);
				form.submit();
				//window.location = "/cart?refer=" + window.location;
				//alert('Data: ' + data);
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
	});
});