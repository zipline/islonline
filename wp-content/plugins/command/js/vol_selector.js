jQuery(document).ready(function()
{

	jQuery(jQuery('#vadd_user_name')).autocomplete(
	{
		minLength: 2,
		delay: 750,
		source: function (request, response)
		{
			var term = request.term;

			var data = 
			{	
				'action': 'cmd_volunteer_select',
				'search_term': term,
			};
					
			jQuery.ajax(
			{
				type: "POST",
				url: svars.ajax_url,
				data: data,
				
				success:function(return_data)
				{
					//console.log(return_data);
					response(JSON.parse(return_data));
				},
				error:function(xml, status, error)
				{
					response(JSON.parse(return_data));
					//alert('Error: ' + error);
				}
			});
		},
		
		select: function (event, ui)
		{
			//console.log(ui.item['label']);
			event.preventDefault();
			var whole = ui.item['label'];

			var email = ( whole.match(/\((.*)\)/) );
			var user = ( whole.match(/\[(.*)\]/) );
			
			var act_name = jQuery.trim( whole.replace(email[0], '').replace(user[0], '') );
			
			jQuery('#vadd_user_name').val(act_name);
			jQuery('#vadd_user_email').val(email[1]);
			jQuery('#vadd_user_id').val(user[1]);
		
			jQuery('#submit_vol').prop('disabled', false);
		}
		
	});

	jQuery('#submit_vol').on('click', function()
	{
		
		var user_id = jQuery('#vadd_user_id').val();
		var team_id = jQuery('#vadd_team_id').val();

		var data = 
		{
			'action': 'cmd_volunteer_add',
			'user_id': user_id,
			'team_id': team_id,
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: svars.ajax_url,
			data: data,
			success:function(data)
			{
				//console.log(data);
				//console.log('Fin');
				location.reload();
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
		
	});
	
});

/*
	var timer;
	
	$('.school_select').on('keyup', function()
	{
		clearTimeout(timer);
		timer = setTimeout( function()
		{
			var fld_name = $('.school_select').attr('data-key');
			var pass_thru = $('#acf-' + fld_name).val();
			alert(pass_thru);
			
			var data = 
			{
				'action': 'cmd_school_select',
				'text': pass_thru,
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: svars.ajax,
				data: data,
				success:function(data)
				{
					alert(data);
				},
				error:function(xml, status,error)
				{
					//alert('Cart Count Error: ' + error);
				}
			});
			
		}, 750);
	});
});

*/