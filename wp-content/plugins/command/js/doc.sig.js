jQuery(document).ready(function()
{
	jQuery('#log_read').click(function()
	{
		var doc_id = jQuery('#doc_id').val();
		var signature = jQuery('#signature').val();
		var post_id = jQuery('#post_id').val();
		
		var data = 
		{
			'action': 'cmd_doc_signature',
			'doc_id': doc_id,
			'signature': signature,
			'post_id': post_id
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: svars.ajax,
			data: data,
			success:function(data)
			{
				var ck_box = window.opener.document.getElementById('doc_check_' + doc_id);
				ck_box.checked=true;
				alert('This document is now marked as "Read". If the window does not close automatically, please close it to continue reading documents.');
				window.close();
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
	});

	jQuery('.no_req_check').on('click', function()
	{
		var doc_id = this.name;
		var signature = 'Not Required';
		var post_id = jQuery('#post_id').val();

		var data = 
		{
			'action': 'cmd_doc_signature',
			'doc_id': doc_id,
			'signature': signature,
			'post_id': post_id
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: svars.ajax,
			data: data,
			success:function(data)
			{
				var ck_box = document.getElementById('doc_check_' + doc_id);
				ck_box.checked=true;
				window.close();
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
	});
});