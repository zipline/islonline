
jQuery(document).ready(function()
{
	var online = navigator.onLine;

	if(online)
	{

		jQuery('#cancel_button').on('click', function()
		{
			if(jQuery('#cancel_button').text() == 'Request Cancellation')
			{
				jQuery('#cancel_button').text('Processing');
				
				var which = jQuery('#cancel_rsvp').val();
				var reason = jQuery('#cancel_reason').val();

				var data = 
				{
					'action': 'cmd_rsvp_cancel',
					'rsvp_id': which,
					'reason': reason
				};

				jQuery.ajax(
				{
					type: 'POST',
					url: svars.ajax,
					data: data,
					success:function(data)
					{
						jQuery('#cancel_button').text('Cancellation has been requested');
						jQuery('#cancel_form').text('Cancellation has been requested');
					},
					error:function(xml, status, error)
					{
						jQuery('#cancel_button').text('Request Cancellation');
					}
				});
			}
		});
	}
});