jQuery(document).ready(function($)
{
	setInterval(function()
	{
		var online = navigator.onLine;
		
		if(online)
		{
			var data = 
			{
				'action': 'cmd_cart_count',
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: ajax_path.ajax_url,
				data: data,
				success:function(data)
				{
					jQuery("#cart_items").html(data);
				},
				error:function(xml, status,error)
				{
					jQuery("#cart_items").html("E");
					//alert('Cart Count Error: ' + error);
				}
			});
		}
		else
		{
			jQuery("#cart_items").html('0');
		}
	},60000);
});