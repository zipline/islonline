jQuery(document).ready(function($)
{
	jQuery('.country').each(function()
	{
		jQuery(this).change(function()
		{
			var cntry = jQuery(this).val();
			var full_cntry = jQuery(this).attr('id');
			var element_id = full_cntry.substr(full_cntry.indexOf("_")+1);
			var region = '#region_' + element_id;
			var data = 
			{
				'action': 'cmd_region_load',
				'country': cntry
			};
	
			jQuery.ajax(
			{
				type: 'POST',
				url: ajaxurl,
				data: data,
				success:function(data)
				{
					jQuery(region).html(data);
				},
				error:function(xml, status,error)
				{
					alert('Error: ' + error);
				}
			});
		});
	});
});