jQuery(document).ready(function()
{

	setInterval(function()
	{
		var online = navigator.onLine;
		
		if(online)
		{
			if(jQuery('#vadd_user_count').val() > 0)
			{
				jQuery('#submit_add_rsvp').prop('disabled', false);	
			}
			else
			{
				jQuery('#submit_add_rsvp').prop('disabled', true);	
			}
		}
	}, 400);
	
	jQuery('#submit_add_rsvp').on('click', function()
	{
		
		var res_count = jQuery('#vadd_user_count').val();
		var team_id = jQuery('#vadd_team_id').val();

		var data = 
		{
			'action': 'cmd_volunteer_add_rsvp',
			'res_count': res_count,
			'team_id': team_id,
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: svars.ajax_url,
			data: data,
			success:function(data)
			{
				//console.log(data);
				//console.log('Fin');
				location.reload();
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
		
	});
	
});