
var modal = '';
var modal_btn = '';
var modal_span = '';

function set_modal(base)
{
	// Get the modal
	modal = document.getElementById(base + '_modal');
	modal.style.display = "block";
	
	// Get the button that opens the modal
	modal_btn = document.getElementById(base + "_open_model");
	
	// Get the <span> element that closes the modal
	modal_span = document.getElementById(base + "_close_model");
}

function set_edit_model(base, element)
{
	// Get the modal
	modal = document.getElementById(base + '_modal');

	// Populate modal
	var adj_id = jQuery(element).data('payment');
	var adj_amt = jQuery(element).children('.adj-amt').data('adj-amt');
	var adj_reason = jQuery(element).children('.adj-reason').data('adj-reason');

	jQuery(modal).find('input[name="adj_id"]').val(adj_id);
	jQuery(modal).find('input[name="adj_amt"]').val(adj_amt);
	jQuery(modal).find('input[name="adj_reason"]').val(adj_reason);

	// Add confirmation to submittion
	jQuery(modal).find('form').submit(function() {
		if (!confirm('Are you sure?')) {
			event.preventDefault();
		}
	});

	// Show modal
	modal.style.display = "block";

	// Get the button that opens the modal
	modal_btn = document.getElementById(base + "_open_model");

	// Get the <span> element that closes the modal
	modal_span = document.getElementById(base + "_close_model");
}

jQuery(document).ready(function($)
{
	jQuery(modal_span).click(function()
	{
		modal.style.display = "none";
	})

	//
	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
})