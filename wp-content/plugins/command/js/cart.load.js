jQuery(document).ready(function($)
{
	jQuery('.joiner').on('click', function()
	{
		var index = jQuery(this).attr('id').substr(jQuery(this).attr('id').lastIndexOf("_")+1);

		var item_id = jQuery('#item_id_' + index).val();
		var item_quan = jQuery('#item_quan_' + index).val();
		var item_type = jQuery('#item_type_' + index).val();
		var item_cost = jQuery('#item_cost_' + index).val();
		var add_on = jQuery('#add_on_' + index).val();

		var data = 
		{
			'action': 'cmd_cart_loading',
			'item_id': item_id,
			'item_quan': item_quan,
			'item_type': item_type,
			'item_cost': item_cost,
			'add_on': add_on,
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: ajax_path.ajax_url,
			data: data,
			success:function(data)
			{
//console.log(data);
				var url = '/cart';
				var form = $('<form action="' + url + '" method="post">' +
				  '<input type="text" name="cart_refer" value="' + window.location + '" />' +
				  '</form>');
				$('body').append(form);
				form.submit();
				//window.location = "/cart?refer=" + window.location;
				//alert('Data: ' + data);
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
	});
});

jQuery(document).ready(function($)
{
	var wto;
	
	jQuery('.set_join').on('keyup change', function()
	{
		var field = this;

		clearTimeout(wto);
		wto = setTimeout(function()
		{
	
			var index = jQuery(field).attr('id').substr(jQuery(field).attr('id').lastIndexOf("_")+1);

			var item_id = jQuery('#item_id_' + index).val();
			var item_quan = jQuery(field).last().val();
			var item_min = jQuery(field).attr('min');
			var item_max = jQuery(field).attr('max');
			var add_on = jQuery('#add_on_' + index).val();
			var item_type = jQuery('#item_type_' + index).val();
			var item_cost = jQuery('#item_cost_' + index).val();

			if(Number(item_quan) > Number(item_max))
			{
				item_quan = item_max;
			}
	
			if(Number(item_quan) < Number(item_min))
			{
				item_quan = item_min;
			}
						 
			var data = 
			{
				'action': 'cmd_cart_set',
				'item_id': item_id,
				'item_quan': item_quan,
				'add_on': add_on,
				'item_type': item_type,
				'item_cost': item_cost,
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: ajax_path.ajax_url,
				data: data,
				success:function(results)
				{
					var url = '/cart';
					var form = $('<form action="' + url + '" method="post">' +
					  '<input type="text" name="cart_refer" value="' + window.location + '" />' +
					  '</form>');
					$('body').append(form);
					form.submit();
					//window.location = "/cart?refer=" + window.location;
				},
				error:function(xml, status, error)
				{
					alert('Error: ' + error);
				}
			});
		}, 1000);
	});
});