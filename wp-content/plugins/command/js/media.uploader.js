jQuery(function($) {
    $(document).ready(function(){
            $('#insert-my-media').on('click',open_media_window);
    });

    function open_media_window() {
			console.log(this.window);
    	if (this.window === undefined) {
        	this.window = wp.media({
                title: 'Choose a featured Image',
                library: {type: 'image'},
                multiple: false,
                button: {text: 'Select'}
            });

        	var self = this; // Needed to retrieve our variable in the anonymous function below
        	this.window.on('select', function() {
                var first = self.window.state().get('selection').first().toJSON();
                
				$('#image_src').attr('src',first.url);
				$('#image_url').attr('value',first.url);
				$('#image_id').attr('value',first.id);
				
				//wp.media.editor.insert('[myshortcode id="' + first.id + '"]');
            });
    	}

    	this.window.open();
    	return false;
	}
});
