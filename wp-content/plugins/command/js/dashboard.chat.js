jQuery(document).ready(function($) {

    $('#dashboard-chat-inbox').on('click', '.chat-popout-trigger', function(event) {
        event.preventDefault();
        var $chatPopoutTrigger = $(this);
        var $chatConvo = $(this).parent('.chat-convo');
        var user_id = $chatConvo.data('userId');
        var team_id = $chatConvo.data('teamId');
        var admin = $chatConvo.data('admin');
        window.open('chat?user_id='+user_id+'&team_id='+team_id+'&admin='+admin+'&private=true', target='blank', 'fullscreen=1, location=0, menubar=0, status=0, toolbar=0, titlebar=0, height=640, width=480, left=100');
        //window.open('admin.php?page=cc-pops&display=chat&user_id='+user_id+'&team_id='+team_id+'&private=true', target='blank', 'fullscreen=1, location=0, menubar=0, status=0, toolbar=0, titlebar=0, height=640, width=480, left=100');
    });

    $('#dashboard-chat-inbox').on('click', '.flag-chat-trigger', function(event) {
        event.preventDefault();
        var $flagTrigger = $(this);
        var $chatConvo = $(this).parent('.chat-convo');
        var user_id = $chatConvo.data('userId');
        var team_id = $chatConvo.data('teamId');

        var action = '';
        if ($flagTrigger.hasClass('flagged')) {
            // Remove the flag
            action = 'cmd_chat_team_delete';
            $flagTrigger.removeClass('flagged');
            $flagTrigger.attr('title', 'Flag this chat');
            console.log('flag removed');
        } else {
            // Add the flag
            $flagTrigger.addClass('flagged')
            $flagTrigger.attr('title', 'Remove the flag from this chat');
            action = 'cmd_chat_team_save';
            console.log('flag added');
        }
        // cmd_chat_team_save
        // cmd_chat_team_delete
        // cmd_chat_team_list

        jQuery.ajax({
            type: 'POST'
            ,url: ajaxurl
            ,data: {
                action: action
                ,team_id: team_id
                ,user_id: user_id
            }
            ,success:function(data) {
                console.log('SUCCESS!', data);
            }
            ,error:function(xml, status, error) {
                console.log('NOPE!', xml, status, error);
            }
        });

    });


});