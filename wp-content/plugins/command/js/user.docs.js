jQuery(document).ready(function()
{
	var resume = jQuery('#wp_cust_att');
	
	//
	//Hide the actual ACF fields
	resume.hide();

	//
	//Add presume and upload button
	if(svars.user == 1)
	{
			
		var resume_new_html = '<div class="profile_switch" id="profile_switch">';

		if(typeof(resume.val()) != "undefined" && resume.val().length > 2)
		{
			resume_new_html += '<a href="' + resume.val() + '" id="resume_file_actual" target="_blank"><img id="resume_file_image" src="/wp-content/uploads/2017/08/download-3.jpg" width="150"></a>';	
		}
		else
		{
			resume_new_html += '<a href="" id="resume_file_actual" target="_blank"><img id="resume_file_image" class="preview_file" src="/wp-content/uploads/2017/08/download-2.jpg" height="150" width="150"></a>';	
		}
		resume_new_html += '<div class="upload-input-container button secondary">Add to Volunteer\'s Required Documents<BR><input name="async-upload" class="document_file" id="resume_id" accept=".csv, .doc, .docx, .pdf, .txt, .xls, .xlsx" type="file" style="color: transparent"/></div>';
		resume_new_html += '<div class="upload-feedback profile">Processing upload...</div>';

		jQuery(resume_new_html).insertAfter(resume);
	}
	else
	{
		jQuery('label[for=' + resume.attr('id') + ']').hide();	
	}
	

	//
	//Watch to see the profile image upload
	jQuery("#resume_id").on('change', function(e)
	{
		e.preventDefault();

		jQuery('.upload-feedback.profile').addClass('show');

		var uploaded = false;
		
		var form_data = new FormData();
		form_data.append('action' , 'upload-attachment');
		form_data.append('async-upload', jQuery('#resume_id')[0].files[0]);
		form_data.append('name', jQuery('#resume_id')[0].files[0].name);
		form_data.append('_wpnonce', svars.nonce);
 
		var file_up = jQuery.ajax(
		{
			type: 'POST',
			url: svars.upload_url,
			data: form_data,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json'
		});
		
		file_up.success(function(data_return)
		{
			 //console.log('SUCCESSFUL UPLOAD ðŸ‘!!');
			 //console.log(data_return);
		});
		
		file_up.done(function(data_return)
		{
			jQuery('.upload-feedback.profile').removeClass('show');
			var url = data_return.data.url;
			var post_id = data_return.data.id;
			var move_data = 
			{
				'action': 'cmd_settings_user_file_upload',
				'url': url,
				'id': post_id
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: svars.ajax_url,
				data: move_data,
				dataType: 'json',
				success:function(move_return)
				{
					jQuery('#wp_custom_attachment').val(post_id);
					jQuery('#docs_form').submit();

					console.log("Success: ");
				},
				error:function(xml, status, error)
				{
					jQuery('#wp_custom_attachment').val('');
					console.log('Error: ' + error);
					console.log(xml)
				}
			});

		});

		file_up.error(function(xml, status, error)
		{
		});

	});
	
});

