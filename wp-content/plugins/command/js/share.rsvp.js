
jQuery(document).ready(function()
{
	var online = navigator.onLine;

	if(online)
	{

		jQuery('.give_button').each(function()
		{
			jQuery(this).on('click',function()
			{
				var which = jQuery(this).attr('id');
				var values = which.split('_');
				var post = values[1];
				var user = values[2];
				var rsvp = values[3];
				var email = jQuery('#give_' + post + '_' + user + '_' + rsvp);
				
				var data = 
				{
					'action': 'cmd_rsvp_share',
					'post_id': post,
					'user_id': user,
					'rsvp_id': rsvp,
					'email': email.val()
				};

				jQuery.ajax(
				{
					type: 'POST',
					url: svars.ajax,
					data: data,
					dataType: 'json',
					success:function(data)
					{
						if(data['response'] == 'Email sent')
						{
							//
							//Disable Give button, Enable Revoke button
							var give = jQuery('#give_' + post + '_' + user + '_' + rsvp + '_g_button');
							var revk = jQuery('#give_' + post + '_' + user + '_' + rsvp + '_r_button');

							give.prop('disabled', true);
							revk.prop('disabled', false);
							email.prop('disabled', true);
						}
						else
						{
							alert(data['response']);
							email.val('');
						}
					},
					error:function(xml, status, error)
					{
						//alert('Error: ' + xml.responseText);
					}
				});
			});
		});

		jQuery('.revoke_button').each(function()
		{
			jQuery(this).on('click',function()
			{
				var which = jQuery(this).attr('id');
				var values = which.split('_');
				var post = values[1];
				var user = values[2];
				var rsvp = values[3];
				var email = jQuery('#give_' + post + '_' + user + '_' + rsvp);
				
				var data = 
				{
					'action': 'cmd_rsvp_unshare',
					'post_id': post,
					'user_id': user,
					'rsvp_id': rsvp,
					'email': email.val()
				};

				jQuery.ajax(
				{
					type: 'POST',
					url: svars.ajax,
					data: data,
					success:function(data)
					{
						//
						//Disable Give button, Enable Revoke button
						var give = jQuery('#give_' + post + '_' + user + '_' + rsvp + '_g_button');
						var revk = jQuery('#give_' + post + '_' + user + '_' + rsvp + '_r_button');

						give.prop('disabled', false);
						revk.prop('disabled', true);
						email.prop('disabled', false);
						email.val('');
						
						//
						//alert('Success: ' + data);
					},
					error:function(xml, status, error)
					{
						//alert('Error: ' + error);
					}
				});
			});
		});

	}
});