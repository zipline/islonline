jQuery(document).ready(function() {
	var profile_image = jQuery('#acf-field_56f882d3ccb2a');
	var banner_image = jQuery('#acf-field_56fc1b73dff8c');

	//var haveProfileImage = (profile_image.val().length > 2);	
	//
	//Hide the actual ACF fields
	profile_image.hide();
	banner_image.hide();

	
	//
	//Add profile image and upload button
	//var prof_new_html = '<form name="prof_upload" id="prof_upload" method="POST" enctype="multipart/form-data">';
	if(svars.user == 1)
	{
			
		var prof_new_html = '<div class="profile_switch" id="profile_switch">';

		if(profile_image.val().length > 2)
		{
			prof_new_html += '<img id="profile_image_actual"  src="' + profile_image.val() + '" width="150">';	
		}
		else
		{
			prof_new_html += '<img id="profile_image_actual" class="preview_image" src="/wp-content/uploads/2016/03/avatar.png" height="150" width="150">';	
		}
		prof_new_html += '<div class="upload-input-container button secondary">Choose file<input name="async-upload" class="image-file" id="prof_image" accept="image/*" type="file" style="color: transparent"/></div>';
		prof_new_html += '<div class="upload-feedback profile">Processing upload...</div>';
		//prof_new_html += '</form>';

		jQuery(prof_new_html).insertAfter(profile_image);
	}
	else
	{
		jQuery('label[for=' + profile_image.attr('id') + ']').hide();	
	}
	

	//
	//Add banner image and upload button
	if(svars.banner == 1)
	{
		var bann_new_html = '<div class="banner_switch" id="banner_switch">';

		if(banner_image.val().length > 2)
		{
			bann_new_html += '<img id="banner_image_actual" class="preview_image" src="' + banner_image.val() + '" height="150" width="150">';	
		}
		else
		{
			bann_new_html += '<img id="banner_image_actual" class="preview_image" src="/wp-content/uploads/2016/03/avatar.png" height="150" width="150">';	
		}
		bann_new_html += '<div class="upload-input-container button secondary">Choose file<input name="async-upload" class="image-file" id="bann_image" accept="image/*" type="file" style="color: transparent"/></div>';
		bann_new_html += '<div class="upload-feedback banner">Processing upload...</div>';

		jQuery(bann_new_html).insertAfter(banner_image);
	}
	else
	{
		jQuery('label[for=' + banner_image.attr('id') + ']').hide();	
	}
	
	//
	//Watch to see the profile image upload
	jQuery("#prof_image").on('change', function(e)
	{
		e.preventDefault();

		jQuery('.upload-feedback.profile').addClass('show');

		var uploaded = false;
		
		var form_data = new FormData();
		form_data.append('action' , 'upload-attachment');
		form_data.append('async-upload', jQuery('#prof_image')[0].files[0]);
		form_data.append('name', jQuery('#prof_image')[0].files[0].name);
		form_data.append('_wpnonce', svars.nonce);
 
		console.log(svars.upload_url);		
		var file_up = jQuery.ajax(
		{
			type: 'POST',
			url: svars.upload_url,
			data: form_data,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json'
		});
		
		file_up.success(function(data_return)
		{
			// console.log('SUCCESSFUL UPLOAD 👏!!');
			// console.log(data_return);
		});
		
		file_up.done(function(data_return)
		{

			jQuery('.upload-feedback.profile').removeClass('show');
			var url = data_return.data.url;
			var post_id = data_return.data.id;
			var move_data = 
			{
				'action': 'cmd_settings_img_upload',
				'url': url,
				'id': post_id
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: svars.ajax_url,
				data: move_data,
				dataType: 'json',
				success:function(move_return)
				{
					jQuery('#profile_image_actual').attr('src', move_return.new_url);
					profile_image.val(move_return.new_url);
					
					jQuery('.avatar', window.parent.document).css("background-image", "url(" + move_return.new_url + ")" );
					console.log("Success: " + move_return.new_url);
				},
				error:function(xml, status,error)
				{
					jQuery('#profile_image_actual').attr('src', '');
					jQuery('#profile_image_actual').attr('alt', 'Image Failed to Copy');
					console.log('Error: ' + error);
				}
			});

		});

		file_up.error(function(json, status, error)
		{
				jQuery('#profile_image_actual').attr('src', '');
				jQuery('#profile_image_actual').attr('alt', 'Image Failed to Upload');
				console.log('Error: ' + status + ', ' + error);
				console.log(json);
				//console.log(jQuery.parseXML(xml));
				//var xmlit = jQuery.parseXML(xml);
				//jQuery(xmlit).each(function()
				//{
				//	console.log(jQuery(this));
				//});
				// + error
		});

	});
	
	//
	//Watch to see the profile image upload
	jQuery("#bann_image").on('change', function(e)
	{
		e.preventDefault();

		jQuery('.upload-feedback.banner').addClass('show');

		var uploaded = false;
		
		var form_data = new FormData();
		form_data.append('action' , 'upload-attachment');
		form_data.append('async-upload', jQuery('#bann_image')[0].files[0]);
		form_data.append('name', jQuery('#bann_image')[0].files[0].name);
		form_data.append('_wpnonce', svars.nonce);
 		
		var file_up = jQuery.ajax(
		{
			type: 'POST',
			url: svars.upload_url,
			data: form_data,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
		});
		
		file_up.success(function(data_return)
		{
			 //console.log(data_return);
		});
		
		file_up.done(function(data_return)
		{
			jQuery('.upload-feedback.banner').removeClass('show');
 
			var url = data_return.data.url;
			var post_id = data_return.data.id;
			var move_data = 
			{
				'action': 'cmd_settings_img_upload',
				'url': url,
				'id': post_id
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: svars.ajax_url,
				data: move_data,
				dataType: 'json',
				success:function(move_return)
				{
					jQuery('#banner_image_actual').attr('src', move_return.new_url);
					banner_image.val(move_return.new_url);

					jQuery('.banner', window.parent.document).css("background-image", "url(" + move_return.new_url + ")" );
					window.parent.autoResize('myisl_canvas')
					// console.log("Success: " + move_return.new_url);
				},
				error:function(json, status, error)
				{
					//console.log('1');
					//console.log(json);
					jQuery('.upload-feedback.banner').removeClass('show');
					jQuery('#banner_image_actual').attr('src', '');
					jQuery('#banner_image_actual').attr('alt', 'Image Failed to Copy');
					//console.log('Error: ' + error);
				}
			});

		});

		file_up.error(function(json, status, error)
		{
				//console.log('2');
				//console.log(json);
				jQuery('.upload-feedback.banner').removeClass('show');
				jQuery('#banner_image_actual').attr('src', '');
				jQuery('#banner_image_actual').attr('alt', 'Image Failed to Upload');
				//console.log('Error: ' + error);  
		});

	});
		
});

