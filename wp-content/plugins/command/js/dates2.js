jQuery(document).ready(function(){
	jQuery('.datepick').each(function(){
		
		jQuery(this).datepicker('destroy');
		
		jQuery(this).datepicker({
				changeMonth : true,
				changeYear : true,
				dateFormat : 'mm-dd-yy'
		});
	});
});