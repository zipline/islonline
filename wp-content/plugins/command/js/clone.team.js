jQuery(document).ready(function()
{

	jQuery('#clone_to_dev').on('click', function()
	{
		var team_name = jQuery('#team_name_main').html();
		
		var data = 
		{
			'action': 'cmd_clone_team',
			'team_name': team_name,
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: svars.ajax_url,
			data: data,
			success:function(data)
			{
				console.log(data);
				
				//location.reload();
			},
			error:function(xml, status, error)
			{
				alert('Error: ' + error);
			}
		});
		
	});
	
});

