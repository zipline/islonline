
jQuery(document).ready(function(){	

	//
	// svars.container should be the id of the container div
	// svars.sdate should be the id of the start date
	// svars.edate should be the id of the end date
	//
	// console.log('svars', svars);
	var container = svars.container; 
	var sdate = svars.sdate;
	var edate = svars.edate;
	
	jQuery('#' + container).on('focusin', '.datepick', function()
	{
		if(jQuery(this).attr('id') == edate)
		{
			if(jQuery('#' + sdate).val().length==10)
			{
				//
				//If there is a valid start date, then 
				//     set the minimum end date to the start date (one day of data)
				//     set the maximum end date to today
				jQuery(jQuery('#' + edate)).datepicker('destroy');
				
				var m = jQuery('#' + sdate).val().substring(0,2);
				var d = jQuery('#' + sdate).val().substring(3,5);
				var y = jQuery('#' + sdate).val().substring(6);
				start_date = new Date(y, parseInt(m) - 1, d);

				jQuery('#' + edate).datepicker(
				{
					changeMonth : true,
					changeYear : true,
					minDate: start_date,
					maxDate: new Date(),
					dateFormat : 'mm/dd/yy'
				});
			}
			else
			{
				//
				//If there is not a valid start date, then 
				//     set the minimum end date to 1/1/2017
				//     set the maximum end date to today
				jQuery(this).datepicker('destroy');

				jQuery(this).datepicker(
				{
					changeMonth : true,
					changeYear : true,
					minDate: new Date('2017', '00', '01'),
					maxDate: new Date(),
					dateFormat : 'mm/dd/yy'
				});
			}
		}
		else
		{
			//
			// Set the paramaters of the start date
			//     The minimum start date is set to 1/1/2017
			//     The maximum start date is set to today
			jQuery(jQuery('#' + sdate)).datepicker(
			{
				changeMonth : true,
				changeYear : true,
				minDate: new Date('2017', '00', '01'),
				maxDate: new Date(),
				dateFormat : 'mm/dd/yy'
			});
		}
	}); 
	
	jQuery('#' + container).on('change', jQuery('#' + sdate), function()
	{
		//
		// If a value has already been set for the start date
		//     then check deeper, 
		//     otherwise set a default end date
		if(jQuery('#' + sdate).val().length==10)
		{
			//
			// If a value has already been set for the start date
			//     then if the end date is less than start date
			//          then set end date to the day after start date
			//          update the end date field to reflect the minimum
			element = jQuery('#' + edate);
			if(element.val().length==10)
			{
				var m = element.val().substring(0,2);
				var d = element.val().substring(3,5);
				var y = element.val().substring(6);
				end_date = new Date(y, m, d);
				
				var m = jQuery('#' + sdate).val().substring(0,2);
				var d = jQuery('#' + sdate).val().substring(3,5);
				var y = jQuery('#' + sdate).val().substring(6);
				start_date = new Date(y, parseInt(m) - 1, d);

				jQuery('#' + edate).datepicker('destroy');
				jQuery('#' + edate).datepicker(
				{
					changeMonth : true,
					changeYear : true,
					minDate: start_date,
					maxDate: new Date(),
					dateFormat : 'mm/dd/yy'
				});

				if(start_date > end_date)
				{
					element.val(m +  '/' + d + '/' + y);
				}
			} 
			else
			{
				//
				// Since the end date is greater than the start date
				//      adjust the minimum date to the new start date
				//      make sure the max date s still ser to today
				var m = jQuery('#' + sdate).val().substring(0,2);
				var d = jQuery('#' + sdate).val().substring(3,5);
				var y = jQuery('#' + sdate).val().substring(6);
				start_date = new Date(y, m, d);

				jQuery('#' + edate).datepicker('destroy');
				jQuery('#' + edate).datepicker(
				{
					changeMonth : true,
					changeYear : true,
					minDate: start_date,
					maxDate: new Date(),
					dateFormat : 'mm/dd/yy'
				});
			}
		}
	});

});