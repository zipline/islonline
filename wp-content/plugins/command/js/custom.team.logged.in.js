jQuery(document).ready(function($)
{
	setInterval(function()
	{
		var online = navigator.onLine;
		
		if(online)
		{
			var data = 
			{
				'action': 'cmd_user_logged_in',
			};

			var apply = jQuery(".acf-form-submit");
			var login = jQuery("#intern_not_logged_in");
			
			jQuery.ajax(
			{
				type: 'POST',
				url: ajax_path.ajax_url,
				data: data,
				dataType: "json",
				success:function(json)
				{

					if(json.loggedIn)
					{
						apply.css('display', 'inline');
						login.css('display', 'none');
					}
					else
					{
						apply.css('display', 'none');
						login.css('display', 'inline');
					}
						
				},
				error:function(xml, status,error)
				{
					apply.css('display', 'none');
					login.css('display', 'none');
				}
			});
		}
		else
		{
			jQuery("#cart_items").html('0');
		}
	},1500);
});