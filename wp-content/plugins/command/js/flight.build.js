jQuery(document).ready(function($) {

	$('#flight_information').on('focus', '.datepicker', function() {

		if($(this).attr('id').substring(0,5) == "adate") {

			element = $('#ddate' + $(this).attr('id').substring(5));
            element_date = new Date(element.val().substring(6), element.val().substring(0,2) -1, element.val().substring(3,5));

			console.log(element_date);
			jQuery(this).datepicker('destroy');
			jQuery(this).datepicker({
					changeMonth : true,
					changeYear : true,
					minDate: element_date,
					dateFormat : 'mm/dd/yy',
					beforeShow: function(event, ui) {
				   jQuery('#ui-datepicker-div').addClass('open');
				},
				onClose: function(event, ui) {
					jQuery('#ui-datepicker-div').removeClass('open');
				}
			});
        } else {
			jQuery(this).datepicker({
					changeMonth : true,
					changeYear : true,
					minDate: 0,
					dateFormat : 'mm/dd/yy',
					beforeShow: function(event, ui) {
				   jQuery('#ui-datepicker-div').addClass('open');
				},
				onClose: function(event, ui) {
					jQuery('#ui-datepicker-div').removeClass('open');
				}
			});

            element = $('#ddate' + $(this).attr('id').substring(5));
            start_date = new Date(element.val().substring(6), element.val().substring(0,2) -1, element.val().substring(3,5));
            
            element = $('#adate' + $(this).attr('id').substring(5));
            end_date = new Date(element.val().substring(6), element.val().substring(0,2) -1, element.val().substring(3,5));
            
            if(end_date > start_date) {
                element.val('');
            }
        }
    });

    $('#flight_information').on('click', ".delete-segment", function() {
        var segment = $(this).closest('.flight_block');
        segment.remove();
        parent.canvasLoaded();
    });
                                     
    $('#flight_information').on('click', ".add-segment", function() {
        var counter = $('#count_fld');
        var cnt = parseInt(counter.val()) + 1;
        counter.val(cnt);

        var RowToAdd = "";

        RowToAdd += "<tr class='flight_block'>";
        RowToAdd += "    <td>";
        RowToAdd += "        <table name='seg_" + cnt + "' id='seg_" + cnt + "'>"; 
        RowToAdd += "            <tr>";
        RowToAdd += "                <th><label>Direction:</label></th>";
        RowToAdd += "                <td>";
        RowToAdd += "                   <select name='direction_" + cnt + "' id='direction_" + cnt + "' required>";
        RowToAdd += "                       <option value='0'>Travel To " + vars.to + "</option>";
        RowToAdd += "                       <option value='1'>Travel From " + vars.from + "</option>";
        RowToAdd += "                   </select>";
        RowToAdd += "                </td>";
        RowToAdd += "            </tr>";
        RowToAdd += "            <tr>";
        RowToAdd += "                <th><label>Airline:</label></th>";
        RowToAdd += "                <td><input placeholder='Your airline' type='text' name='airline_" + cnt + "' id='airline_" + cnt + "' value='' required></td>";
        RowToAdd += "            </tr>";
        RowToAdd += "            <tr>";
        RowToAdd += "                <th><label>Flight:</label></th>";
        RowToAdd += "                <td><input placeholder='Your flight number' type='text' name='flight_" + cnt + "' id='flight_" + cnt + "' value='' required></td>";
        RowToAdd += "            </tr>";
        RowToAdd += "            <tr>";
        RowToAdd += "                <th><label>Departing:</label></th>";
        RowToAdd += "                <td><input placeholder='Departure city' type='text' name='depart_" + cnt + "' id='depart_" + cnt + "' placeholder='Which City / State / Country' value='' required></td>";
        RowToAdd += "            </tr>";
        RowToAdd += "            <tr>";
        RowToAdd += "               <th><label>&nbsp;</label></th>";
        RowToAdd += "               <td>";
        RowToAdd += "                   <div class='input-group'>";
        RowToAdd += "                       <label>Date:</label>";
        RowToAdd += "                       <input placeholder='Departure date' type='text' class='datepicker' name='ddate_" + cnt + "' id='ddate_" + cnt + "' value='' required>";
        RowToAdd += "                   </div>";
        RowToAdd += "                   <div class='input-group'>";
        RowToAdd += "                       <label>Time:</label>";
        RowToAdd += "                       <input placeholder='Departure time' type='text' name='dtime_" + cnt + "' id='dtime_" + cnt + "' value='' required>";
        RowToAdd += "                   </div>";
        RowToAdd += "               </td>";
        RowToAdd += "            </tr>";
        RowToAdd += "            <tr>";
        RowToAdd += "                <th><label>Arriving:</label></th>";
        RowToAdd += "                <td><input placeholder='Arrival city' type='text' name='arrive_" + cnt + "' id='arrive_" + cnt + "' placeholder='Which City / State / Country' value='' required></td>";
        RowToAdd += "            </tr>";
        RowToAdd += "            <tr>";
        RowToAdd += "                <th><label>&nbsp;</label></th>";
        RowToAdd += "                <td>";
        RowToAdd += "                    <div class='input-group'>";
        RowToAdd += "                        <label>Date:</label>";
        RowToAdd += "                        <input placeholder='Arrival date' type='text' class='datepicker' name='adate_" + cnt + "' id='adate_" + cnt + "' value='' required>";
        RowToAdd += "                    </div>";
        RowToAdd += "                    <div class='input-group'>";
        RowToAdd += "                        <label>Time:</label>";
        RowToAdd += "                        <input placeholder='Arrival time' type='text' name='atime_" + cnt + "' id='atime_" + cnt + "' value='' required>";
        RowToAdd += "                    </div>";
        RowToAdd += "               </td>";
        RowToAdd += "            </tr>";
        RowToAdd += "            <tr>";
        RowToAdd += "                <th><label>&nbsp;</label></th>";
        RowToAdd += "                <td>";
        RowToAdd += "                    <input type='hidden' name='seat_" + cnt + "' id='seat_" + cnt + "' value='0'>";
        RowToAdd += "                    <input type='submit' class='button small delete-segment' value='Delete this trip segment' name='delete_" + cnt + "' id='delete_" + cnt + "'>";
        RowToAdd += "                    <input type='button' class='button small add-segment' value='Add another trip segment' />";
        RowToAdd += "                </td>";
        RowToAdd += "            </tr>";
        RowToAdd += "        </table>";
        RowToAdd += "    </td>";
        RowToAdd += "</tr>";

        $('#flight_information > tbody').append(RowToAdd);

        // Tell the parent iframe to resize itself...
        parent.canvasLoaded();
    });
});