
jQuery(document).ready(function($)
{
	jQuery('#poc_table').on('click', ".del_poc", function()
	{
		$(this).parent().parent().remove();
	});

	jQuery('#poc_table').on('click', ".add_poc", function()
	{
		var currow = $(this).parent().parent();
		var curpoc = jQuery('#poc_counter');
		var curcnt = curpoc.val();

		var RowToAdd = "";
		RowToAdd += "<TR>";
		RowToAdd += "  <TD>";
		RowToAdd += "    <input type='text' size='30' maxlength='50' name='email[" + curcnt + "]' id='email[" + curcnt + "]' placeholder='Email'>";
		RowToAdd += "  </TD>";
		RowToAdd += "  <TD>";
		RowToAdd += "    <input type='text' size='30' maxlength='50' name='fn[" + curcnt + "]' id='fn[" + curcnt + "]' placeholder='First Name'>";
		RowToAdd += "  </TD>";
		RowToAdd += "  <TD>";
		RowToAdd += "    <input type='text' size='30' maxlength='50' name='ln[" + curcnt + "]' id='ln[" + curcnt + "]' placeholder='Last Name'>";
		RowToAdd += "  </TD>";
		RowToAdd += "   <TD>";
		RowToAdd += "      <input type='button' name='del_poc_" + curcnt + "' id='del_poc_" + curcnt + "' class='button del_poc' value='DELETE'>";
		RowToAdd += "      <input type='button' name='add_poc_" + curcnt + "' id='add_poc_" + curcnt + "' class='button add_poc' value='ADD'>";
		RowToAdd += "   </TD>";
		RowToAdd += "</TR>";

		currow.after(RowToAdd);
		var newval = parseInt(curcnt) + 1;
		curpoc.val(newval);
	});
});