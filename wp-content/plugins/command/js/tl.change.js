jQuery(document).ready(function($)
{
	jQuery('.team_lead').each(function()
	{
		jQuery(this).change(function()
		{
			var tl = jQuery(this).val();
			var id = jQuery(this).attr('id');
			var rsvp = id.substr(id.lastIndexOf("_")+1);
			var data = 
			{
				'action': 'cmd_tl_change',
				'tl': tl,
				'rsvp': rsvp
			};

			jQuery.ajax(
			{
				type: 'POST',
				url: ajaxurl,
				data: data,
				success:function(data)
				{
					//;
				},
				error:function(xml, status,error)
				{
					alert('Error: ' + error);
				}
			});
		});
	});
});