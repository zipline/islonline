
jQuery(document).ready(function($)
{
	jQuery('.featured').each(function()
	{
		jQuery(this).change(function()
		{
			var values = '';

			for(orders = 1; orders <=8; orders++)
			{
				var temp = '#spot_' + orders;
				var temp_val = jQuery(temp).val();
				
				if(temp_val > 0)
				{
					values = values.concat(orders + ":" + temp_val + ";");
				}
			}
			
			var data = 
			{
				'action': 'cmd_featured_teams',
				'data': values,
				'post_id': svars.post_id
			};
	
			jQuery.ajax(
			{
				type: 'POST',
				url: ajaxurl,
				data: data,
				success:function(data)
				{
					//alert(data);
				},
				error:function(xml, status,error)
				{
					alert('Error: ' + error);
				}
			});
		});
	});
});