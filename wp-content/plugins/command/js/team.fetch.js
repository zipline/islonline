jQuery(document).ready(function()
{
	jQuery('#migrate_id').change(function()
	{
		var team = jQuery('#migrate_id').val();
		var data = 
		{
			'action': 'cmd_team_data_fetch',
			'team': team
		};

		jQuery.ajax(
		{
			type: 'POST',
			url: ajaxurl,
			data: data,
			success:function(data)
			{
				var json_data = jQuery.parseJSON(data);

				jQuery('#country').html(json_data.country_list);
				jQuery('#arr_date').html(json_data.arrival_date);
				jQuery('#dep_date').html(json_data.departure_date);
				jQuery('#duration').html(json_data.trip_duration + ' days');
				jQuery('#arrive').html(json_data.arrival_city);
				jQuery('#depart').html(json_data.departure_city);
				jQuery('#cost').html('$' + json_data.trip_cost);
				jQuery('#new_cost').attr('value',json_data.trip_cost);
				
				var orig_cost = jQuery('#orig_cost').val();
				
				if(orig_cost != json_data.trip_cost && team > 0)
				{
					jQuery('#keep').show();	
				}
				else
				{
					jQuery('#keep').hide();
				}
				
				if(team > 0)
				{
					jQuery('#submit').prop('disabled', false);
				}
				else
				{
					jQuery('#submit').prop('disabled', true);
				}

			},
			error:function(xml, status,error)
			{
				alert('Error: ' + error);
			}
		});
	});
});