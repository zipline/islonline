<?php

//
//Enhanced error reporting
error_reporting(E_ALL);
ini_set("display_errors",1);

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

Class COMMAND_CENTER
{

	private static $initialized = null;

	private $menu_levels = array('super' => 401, 'admin' => 301, 'staff' => 201, 'enhanced' => 101, 'default' => 1, 'volunteer' => 0);
	private $menu_order = array();

	public $menu = null;
	
	//
	//Function used to determine if self is actualized, then start the remainder of the classes
	public static function init()
	{
		if (!self::$initialized)
		{
			self::$initialized = true;
			$class = __CLASS__;
			new $class;
		}
	}
	
	//
	//Launch the menu items
	function __construct()
	{
		add_action ('admin_init', array($this, 'start_session'));
		//
		//2018-02-21 DJT Kill session data on logout
		add_action ('wp_logout', array($this, 'end_session'));
		add_action ('admin_init', array($this, 'admin_authorize'));
		add_action ('admin_menu', array( $this, 'command_page'), 11);
		add_action ('admin_menu', array( $this, 'clear_menu'), 999);
		
		add_filter('custom_menu_order', '__return_true');
		add_filter('menu_order', array($this, 'cc_final_menu_order'));
	}

	function start_session()
	{

		if(!session_id())
		{
			session_start();
		}
	}

	//
	//2018-02-21 DJT kill session data on logou
	function end_session()
	{
		if(session_id())
		{
			session_destroy();
		}
	}

	public function user_role()
	{
		$roles = get_user_meta(get_current_user_id(), 'wp_capabilities', true);
		$roles = array_keys($roles);
		reset ($roles);

		foreach($roles as $role)
		{
			if(in_array($role, $this->menu_levels))
			{
				return $role;
			}
	   }

		return "no permissions";
	}

	/**
	 * Gets the page id/object identifier for access control
	 */
	private function get_admin_page_id()
	{
		$req_uri = $_SERVER['SCRIPT_NAME'];
		/* Get the page id */
		if($req_uri == "/wp-admin/admin.php")
		{
			$page_id = $_REQUEST['page'];
		}
		/* get the post type */
		elseif($req_uri == "/wp-admin/edit.php")
		{

			if(isset($_REQUEST['post_type']))
			{
				$page_id = $_REQUEST['post_type'];
			}
			else
			{
				$page_id = "edit.php";
			}
		}
		/* get the post type */
		elseif($req_uri == "/wp-admin/post.php")
		{
			if(isset($_REQUEST['post']))
			{
				$page_id = get_post_type($_REQUEST['post']);
			}
			else
			{
				$page_id = $_REQUEST['post_type'];
			}
		}
		else
		{
			$uri_array = explode('/',$req_uri);
			$page_id = end($uri_array);
		}

		return $page_id;
	}
	
	public function cc_final_menu_order()
	{
		return $this->menu_order;
	}
	
	public function command_page()
	{
		$menu_order = array();

		switch (self::user_role())
		{
			case "super":
				$start_pos = $this->menu_levels['super'];
								
				$this->menu_order[$start_pos] = 'plugins.php';
				$start_pos++;

				$this->menu_order[$start_pos] = 'themes.php';
				$start_pos++;

				$this->menu_order[$start_pos] = 'options-general.php';
				$start_pos++;

				$this->menu_order[$start_pos] = 'upload.php';
				$start_pos++;

				$this->menu_order[$start_pos] = 'edit.php?post_type=acf-field-group';
				$start_pos++;
				
				$this->menu_order[$start_pos] = 'edit-comments.php';
				$start_pos++;

				$this->menu_order[$start_pos] = 'profile-builder';
				$start_pos++;

				$this->menu_order[$start_pos] = 'tools.php';
				$start_pos++;

				$this->menu_order[$start_pos] = 'wpseo_dashboard';
				$start_pos++;

				if ( get_role('wpseo_manager') ) {
					// Add Yoast SEO capabilities
					$role = get_role( 'super' );
					$role->add_cap( 'wpseo_bulk_edit' );
					$role->add_cap( 'wpseo_edit_advanced_metadata' );
					$role->add_cap( 'wpseo_manage_options' );
				}

			case "admin":
				$start_pos = $this->menu_levels['admin'];

				add_menu_page ('Accounting', 'Accounting', 'manage_options', 'cc-accounting', 
							   array($this,'cc_accounting_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-accounting';
				$start_pos++;
				
				add_menu_page ('Payments', 'Payments', 'manage_options', 'cc-payments', 
							   array($this,'cc_payment_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-payments';
				$start_pos++;
				
				add_menu_page ('Coupons', 'Coupons', 'manage_options', 'cc-coupon', 
							   array($this,'cc_coupon_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-coupon';
				$start_pos++;

				add_menu_page ('Ambassador', 'Ambassador Data', 'manage_options', 'cc-ambassador', 
							   array($this,'cc_ambassador_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-ambassador';
				$start_pos++;

				if ( get_role('wpseo_manager') ) {
					// Add Yoast SEO capabilities
					$role = get_role( 'admin' );
					$role->add_cap( 'wpseo_bulk_edit' );
					$role->add_cap( 'wpseo_edit_advanced_metadata' );
					$role->add_cap( 'wpseo_manage_options' );
				}

			case "staff":

				$start_pos = $this->menu_levels['staff'];

				add_menu_page ('Messages','Messages', 'edit_posts', 'team-messages', 
							   array($this,'cc_message_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'team-messages';
				$start_pos++;
				
				add_menu_page ('Documents','Documents', 'edit_pages', 'docs', 
							   NULL, NULL, $start_pos);
				add_submenu_page ('docs', 'Team Docs', 'Team Docs', 'edit_pages', 
								  'edit.php?post_type=cp_team_docs', NULL);
				add_submenu_page ('docs', 'Ambassador Info', 'Ambassador Info Docs', 'edit_pages', 
								  'edit.php?post_type=cp_rep_docs', NULL);
				remove_submenu_page('docs','docs');
				$this->menu_order[$start_pos] = 'docs';
				$start_pos++;
				
				add_menu_page ('Itineraries','Itineraries', 'edit_pages', 'cc-itinerary', 
							   array($this,'cc_itin_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-itinerary';
				$start_pos++;

				$this->menu_order[$start_pos] = 'edit.php?post_type=team_add_ons';
				$start_pos++;

				$this->menu_order[$start_pos] = 'edit.php?post_type=isl_intern';
				$start_pos++;

				$this->menu_order[$start_pos] = 'edit.php?post_type=isl_coupon';
				$start_pos++;

				add_menu_page ('Countries','Countries', 'edit_pages', 'cc-country', 
							   array($this,'cc_country_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-country';
				$start_pos++;

				add_menu_page ('Programs','Programs', 'edit_pages', 'cc-program', 
							   array($this,'cc_program_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-program';
				$start_pos++;

				//add_menu_page ('Emails','Emails', 'edit_pages', 'edit.php?post_type=cp_email', 
				//			   NULL, NULL, $start_pos);
				//$this->menu_order[$start_pos] = 'edit.php?post_type=cp_email';
				//$start_pos++;

				$this->menu_order[$start_pos] = 'edit.php?post_type=testimonial';
				$start_pos++;

				$this->menu_order[$start_pos] = 'edit.php?post_type=page';
				$start_pos++;

				$this->menu_order[$start_pos] = 'edit.php';
				$start_pos++;

				$role = get_role('staff');
				$role->add_cap('gform_full_access');
				$role = get_role('administrator');
				$role->add_cap('gform_full_access');
				$role = get_role('super');
				$role->add_cap('gform_full_access');
				$this->menu_order[$start_pos] = 'gf_edit_forms';
				$start_pos++;


				global $menu;
				global $submenu;
				global $wp_post_types;

				$menu[5][0] = 'Blog';
				$submenu['edit.php'][5][0] = 'Blog';
				$submenu['edit.php'][10][0] = 'Add Blog';
				
				$labels = &$wp_post_types['post']->labels;
			    $labels->name = 'Blog';
				$labels->add_new = 'Add Blog Item';
				$labels->add_new_item = 'Add Blog Item';
					
				//add_menu_page ('Staff','Staff', 'edit_pages', 'team-manage', 
				//			   array($this,'cc_message_page'), NULL, $start_pos);
				//$this->menu_order[$start_pos] = 'team-manage';
				//$start_pos++;

			case "enhanced":
				$start_pos = $this->menu_levels['enhanced'];

/*				add_menu_page ('Messages','Messages', 'edit_posts', 'team-messages', 
							   array($this,'cc_message_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'team-messages';
				$start_pos++;*/
							   
				add_menu_page ('Teams','Teams', 'edit_posts', 'cc-teams', 
							   array($this,'cc_team_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-teams';
				$start_pos++;
		
				add_menu_page ('Volunteers','Volunteers', 'edit_posts', 'cc-volunteers', 
							   array($this,'cc_volunteer_page'), NULL, $start_pos);
				$this->menu_order[$start_pos] = 'cc-volunteers';
				$start_pos++;
				
				if(self::user_role() == 'enhanced')
				{
					add_menu_page ('MyISL','MyISL', 'edit_posts', 'cc-myisl', 
								   array($this,'cc_myisl_redirector'), NULL, $start_pos);
					$this->menu_order[$start_pos] = 'cc-myisl';
					$start_pos++;
				}

				//
				//Hidden popups
				add_submenu_page ('options.php', 'Pops', 'Pops', 'edit_posts', 'cc-pops', array($this,'cc_pops'));
				remove_submenu_page('command','cc-pops');

			default:
				$start_pos = $this->menu_levels['default'];

				remove_menu_page('index.php');
				add_menu_page ('Dashboard','Dashboard', 'edit_posts', 'index.php', 
							   NULL, NULL, $start_pos);
				$this->menu_order[$start_pos] = 'index.php';
				$start_pos++;
		};

	}

	/**
	 * Checks if user should be able to access to admin pages based on role and page identifier
	 */
	public function admin_authorize()
	{
		
		$role = self::user_role();

		$page_id = self::get_admin_page_id();

		$auths = array(
			'super' => array(
				'plugins.php',
				'themes.php',
				'customize.php',
				'widgets.php',
				'theme-editor.php',
				'nav-menus.php',
				'options-general.php',
				'options-writing.php',
				'options-reading.php',
				'options-discussion.php',
				'options-media.php',
				'options-permalink.php',
				'acf-field-group',
				'edit-comments.php',
				'profile-builder',
				'wpseo_dashboard',
				'wpseo_titles',
				'wpseo_social',
				'wpseo_xml',
				'wpseo_advanced',
				'wpseo_tools',
				'wpseo_search_console',
				'wpseo_licenses',
				'tools.php',
				'users.php',
				'user-edit.php',
				'media-new.php',
				'attachment',
				'upload.php',
				'plugin-install.php',
				'plugin-editor.php',
			),
			'admin' => array(
				'cc-accounting',
				'cc-payments',
				'cc-coupon',
				'cc-ambassador',
				'post',
				'options.php',
			),
			'staff' => array(
				'team-messages',
				'docs',
				'manage-options',
				'cc-itinerary',
				'cc-country',
				'cc-program',
				'isl_intern',
				'isl_coupon',
				'team_add_ons',
				'cp_email',
				'testimonial',
				'page',
				'post-new.php',
				'edit-tags.php',
				'team-manage',
				'cp_team_docs',
				'cp_rep_docs',
				'upload-new',
				'gf_edit_forms',
				'gf_new_form',
				'gf_entries',
				'gf_settings',
				'gf_export',
				'gf_update',
				'gf_addons',
				'gf_help',
			),
			'enhanced' => array(
				'cc-pops',
				'cc-teams',
				'cc-volunteers',
				'cc-myisl',
			),
			'base' => array(
				'edit.php',
				'index.php',
				'profile.php',
				'team-messages',
			),
			'volunteer' => array(
				'async-upload.php',
			),
		);
		$accessible = array();

		$combine = false;
		foreach($auths as $a_key => $pages)
		{
			if($a_key == $role)
			{
				if($role != 'enhanced' || ($role == "enhanced" && self::poc_not_ambassador()))
				{
					$combine = true;
				}
			}

			if($combine)
			{
				$accessible = array_merge($pages,$accessible);
			}
		}

		if(!in_array($page_id,$accessible) && $role!='super')
		{
			wp_safe_redirect( wp_get_referer() );
			wp_die( "You don't have access to this page. " . self::user_role(), __( 'WordPress &rsaquo; Error' ),403 );
		}
		

	}

	public function clear_menu()
	{
		
		switch(self::user_role())
		{
			case "volunteer":
				remove_menu_page('tools.php');
			case "enhanced":
				remove_menu_page('gf_edit_forms');
				remove_menu_page('edit.php?post_type=testimonial');
				remove_menu_page('edit.php?post_type=page');
				remove_menu_page('edit.php?post_type=team_add_ons');
				remove_menu_page('edit.php?post_type=isl_coupon');
				remove_menu_page('edit.php?post_type=isl_intern');
				remove_menu_page('edit.php');

			case "staff":

			case "admin":
				remove_menu_page('upload.php');
				remove_menu_page('edit-comments.php');
				remove_menu_page('themes.php');
				remove_menu_page('plugins.php');
				remove_menu_page('options-general.php');	

				remove_menu_page('profile-builder');
				remove_menu_page('tools.php');
				remove_menu_page('edit.php?post_type=acf-field-group');
				remove_menu_page('oa_social_login_setup');
				
				remove_menu_page('wpseo_dashboard');
				remove_submenu_page('index.php', 'update-core.php');

			case "super":
				remove_menu_page('users.php');
				remove_menu_page('profile.php');
		}

	}

	//
	//Command pages
	public function cc_message_page()
	{
		// commented out because it is causing issues on staff page
		require_once(CC_ADMIN_DIR . "message.php");
	}
	public function cc_team_page()
	{
		require_once(CC_ADMIN_DIR . "team.php");
	}
	public function cc_accounting_page()
	{
		require_once(CC_ADMIN_DIR . "reports/accounting.php");
	}
	public function cc_payment_page()
	{
		require_once(CC_ADMIN_DIR . "reports/payments.php");
	}
	public function cc_coupon_page()
	{
		require_once(CC_ADMIN_DIR . "reports/coupons.php");
	}
	public function cc_ambassador_page()
	{
		require_once(CC_ADMIN_DIR . "reports/ambassador.php");
	}
	public function cc_pops()
	{
		require_once(CC_ADMIN_DIR . "popups.php");
	}

	//
	//Control pages
	public function cc_country_page()
	{
		require_once(CC_ADMIN_DIR . "country.php");
	}
	public function cc_itin_page()
	{
		require_once(CC_ADMIN_DIR . "itinerary.php");
	}
	public function cc_program_page()
	{
		require_once(CC_ADMIN_DIR . "program.php");
	}

	//
	//Other pages
	public function cc_staff_page()
	{
		require_once(CC_USER_DIR . "staff.php");
	}
	public function cc_volunteer_page()
	{
		require_once(CC_USER_DIR . "user.php");
	}

	public function cc_myisl_redirector()
	{
		echo "<script>";
		echo "window.location = '" . site_url("/my-isl/") . "';";
		echo "</script>";
		exit;
	}
	
	function poc_not_ambassador()
	{
		if(get_user_meta(get_current_user_id(), 'cmd_isl_rep', true))
		{
			if(get_user_meta(get_current_user_id(), 'cmd_custom_team_poc', true))
			{
				return true;
			}

			return false;
		}

		return true;
	}
}