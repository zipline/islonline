<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
// install plugin
Class COMMAND_RESET
{

	function __construct($perform_all = false)
	{
		if($perform_all)
		{
			self::reset();
		}
	}


	function reset($teams = true, $internships = true, $orders = true, $itineraries = true, $messages = true, $user_info = true)
	{

		if($teams)
		{
			self::reset_teams();
		}

		if($internships)
		{
			self::reset_internships();
		}
		
		if($orders)
		{
			self::reset_orders();
		}
		
		if($itineraries)
		{
			self::reset_itineraries();
		}
		
		if($messages)
		{
			self::reset_messaging();
		}
		
		if($user_info)
		{
			self::reset_user_information();
		}

	}	
	
	//
	// Delete data from Post, PostMeta, TEAM and ITINERARY Tables for Teams
	function reset_teams()
	{
		global $wpdb;
		
		//
		//Remove Teams from Post and PostMeta
		$query = "DELETE wp, wpm 
					FROM `" . $wpdb->prefix . "posts` wp LEFT JOIN `" . $wpdb->prefix . "postmeta` wpm on wp.id = wpm.post_id 
					WHERE post_type = 'isl_team'";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Team Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_TEAM`";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Team Itinerary Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_TEAM_ITINERARY`";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Team Schedule Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_schedule`";
		self::execute_query($wpdb, $query);

		//
		//Delete from Team Notes Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_team_note`";
		self::execute_query($wpdb, $query);
		
		//
		//Remove Teams Add-ons from Post and PostMeta
		$query = "DELETE wp, wpm 
					FROM `" . $wpdb->prefix . "posts` wp LEFT JOIN `" . $wpdb->prefix . "postmeta` wpm on wp.id = wpm.post_id 
					WHERE post_type = 'team_add_ons'";
		self::execute_query($wpdb, $query);
	}

	//
	// Delete data from Post and PostMeta for Internships
	function reset_internships()
	{
		global $wpdb;
		
		//
		//Remove Internships from Post and PostMeta
		$query = "DELETE wp, wpm 
					FROM `" . $wpdb->prefix . "posts` wp LEFT JOIN `" . $wpdb->prefix . "postmeta` wpm on wp.id = wpm.post_id 
					WHERE post_type = 'isl_intern'";
		self::execute_query($wpdb, $query);

	}

	//
	// Delete data from Post, PostMeta, orders, order_items, order_payment, order_transact, and RSVP Tables
	function reset_orders()
	{
		global $wpdb;
		
		//
		//Remove RSVP from Post and PostMeta
		$query = "DELETE wp, wpm 
					FROM `" . $wpdb->prefix . "posts` wp LEFT JOIN `" . $wpdb->prefix . "postmeta` wpm on wp.id = wpm.post_id 
					WHERE post_type = 'isl_rsvp'";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from RSVP Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_RSVP`";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Orders Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_orders`";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Order_Items Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_order_items`";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Order_Payments Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_order_payment`";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Order_Transact Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_order_transact`";
		self::execute_query($wpdb, $query);

			//
		//Delete from Order_Transact Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_cart`";
		self::execute_query($wpdb, $query);
	}

	//
	//Delete data from Itinerary tables
	function reset_itineraries()
	{
		global $wpdb;
		
		//
		//Delete from Order_Payments Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_itinerary`";
		self::execute_query($wpdb, $query);
		
		//
		//Delete from Order_Transact Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_itin_days`";
		self::execute_query($wpdb, $query);
	}

	//
	//Delete data from Post and PostMeta for user data
	function reset_user_information()
	{
		global $wpdb;
		
		//
		//Remove Ambassador Information from Post and PostMeta
		$query = "DELETE wp, wpm 
					FROM `" . $wpdb->prefix . "posts` wp LEFT JOIN `" . $wpdb->prefix . "postmeta` wpm on wp.id = wpm.post_id 
					WHERE post_type = 'isl_ambassador'";
		self::execute_query($wpdb, $query);

	}

	//
	//Delete data from Messaging tables
	function reset_messaging()
	{
		global $wpdb;

		//
		//Delete from Trigger Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_trigger`";
		//self::execute_query($wpdb, $query);

		//
		//Delete from Trigger Event Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_trigger_event`";
		self::execute_query($wpdb, $query);

		//
		//Delete from Message Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_message`";
		self::execute_query($wpdb, $query);

		//
		//Delete from Admin Message Table
		$query = "DELETE FROM `" . $wpdb->prefix . "command_admin_message`";
		self::execute_query($wpdb, $query);


		//
		//Delete from Chattterbox Tables
		$query = "DELETE IGNORE FROM `" . $wpdb->prefix . "chatterbox_index`";
		self::execute_query($wpdb, $query);

		$query = "DELETE IGNORE FROM `" . $wpdb->prefix . "chatterbox_messages`";
		self::execute_query($wpdb, $query);

		$query = "DELETE IGNORE FROM `" . $wpdb->prefix . "chatterbox_teams`";
		self::execute_query($wpdb, $query);

		$query = "DELETE IGNORE FROM `" . $wpdb->prefix . "chatterbox_users`";
		self::execute_query($wpdb, $query);

	}
	
	
	//
	// execute_query
	// reads in db connection and query and executes the query on the db
	// Paramaters:
	//  database connection $db - the connection to be used
	//  string $query - the query to be executed
	function execute_query($db, $query)
	{
		if(substr($query,6) == "CREATE")
		{
			$db->query($query) || die (trigger_error($query . ':' . $db->last_error, E_USER_ERROR));			 
		} 
		else 
		{
			$affect = $db->query($query);
			
			if(!$affect && isset($db->last_error) && $db->last_error != '')
			{
				trigger_error("((" . $query . ")) : " . $db->last_error, E_USER_ERROR);
			}
		}
	}

}

?>