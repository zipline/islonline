<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

//
// install plugin
Class COMMAND_INSTALL
{

	function __construct()
	{
		self::permission_activate();
		self::caps_create();
		
		self::country_activate();
		self::region_activate();
		self::program_type_activate();
		self::program_activate();
		self::gender_activate();
		self::team_status_activate();
		self::team_type_activate();
		
		self::team_create();
		self::cart_create();
		self::rsvp_create();
		self::schedule_create();
		self::itinerary_create();
		self::messages_create();
		self::admin_messages_create();
		
	}


	//
	// activate permissions for different roles
	function permission_activate()
	{
		//
		//Gather existing role data
		$role['editor'] = get_role('editor');
		$role['author'] = get_role('author');
		$role['contributor'] = get_role('contributor');
		$role['subscriber'] = get_role('subscriber');
		$role['admin'] = get_role('administrator');
		$role['super'] = get_role('administrator');

		//
		//Write in new roles
		isset($role['super']->name) ? add_role('super', 'Super', $role['super']->capabilities) : '';
		isset($role['admin']->name) ? add_role('admin', 'Admin', $role['admin']->capabilities) : '';
		isset($role['editor']->name) ? add_role('staff', 'Staff', $role['editor']->capabilities) : '';
		isset($role['author']->name) ? add_role('enhanced', 'Enhanced', $role['author']->capabilities) : '';
		isset($role['contributor']->name) ? add_role('volunteer', 'Volunteer', $role['contributor']->capabilities) : '';

		//
		//Remove existing roles if they exist
		isset($role['editor']->name) ? remove_role('editor') : '';
		isset($role['author']->name) ? remove_role('author') : '';
		isset($role['contributor']->name) ? remove_role('contributor') : '';
		isset($role['subscriber']->name) ? remove_role('subscriber') : '';
		
		remove_role('super_admin');
		remove_role('shop_manager');
		remove_role('customer');

		// Remove Yoast `SEO Manager` role
		if ( get_role('wpseo_manager') ) {
			remove_role( 'wpseo_manager' );
		}
		// Remove Yoast `SEO Editor` role
		if ( get_role('wpseo_editor') ) {
			remove_role( 'wpseo_editor' );
		}

	}

	//
	// create the capabilities table and populate it
	function caps_create()
	{

		global $wpdb;
		
		//
		//Create capabilities group table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_cap_groups
				(
				 id int(11) NOT NULL auto_increment,
				 kode varchar(100) NOT NULL,
				 title varchar(100) NOT NULL,
				 priority int(11) NOT NULL,
				 active tinyint(1) DEFAULT 1,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 KEY kode (kode)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core Capability Group Informatino
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_cap_groups
						(id, kode, title, priority, active, created, created_by)
					  VALUES
					   	(1, 'enhanced', 'Enhanced', 20, 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_cap_groups
						(id, kode, title, priority, active, created, created_by)
					  VALUES
					   	(2, 'staff', 'Staff', 30, 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_cap_groups
						(id, kode, title, priority, active, created, created_by)
					  VALUES
					   	(3, 'volunteer', 'Volunteer', 10, 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_cap_groups
						(id, kode, title, priority, active, created, created_by)
					  VALUES
					   	(4, 'admin', 'Administrator', 40, 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_cap_groups
						(id, kode, title, priority, active, created, created_by)
					  VALUES
					   	(5, 'super', 'Super Admin', 50, 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		//
		//Create capabilities group table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_caps
				(
				 id int(11) NOT NULL auto_increment,
				 cap_group_id int(11) NOT NULL,	
				 kode varchar(100) NOT NULL,
				 title varchar(100) NOT NULL,
				 active tinyint(1) DEFAULT 1,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 KEY cap_group_id (cap_group_id),	
				 KEY kode (kode)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert Core Capabilities
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_caps
						(id, cap_group_id, kode, title, active, created, created_by)
					  VALUES
					   	(1, 1, 'cmd_custom_team_poc', 'Custom Team POC', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_caps
						(id, cap_group_id, kode, title, active, created, created_by)
					  VALUES
					   	(2, 1, 'cmd_isl_rep', 'Ambassador', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_caps
						(id, cap_group_id, kode, title, active, created, created_by)
					  VALUES
					   	(101, 2, 'cmd_team_manager', 'Team Manager', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_caps
						(id, cap_group_id, kode, title, active, created, created_by)
					  VALUES
					   	(102, 2, 'cmd_country_coordinator', 'Country Coordinator', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_caps
						(id, cap_group_id, kode, title, active, created, created_by)
					  VALUES
					   	(103, 2, 'cmd_team_leader', 'Team Leader', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_caps
						(id, cap_group_id, kode, title, active, created, created_by)
					  VALUES
					   	(104, 2, 'cmd_non_web_staff', 'Non-Website Staff', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

	}

	// 
	// create and populate country data
	function country_activate()
	{
		global $wpdb;
		
		//
		//Create country table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_country
				(
				 id int(11) NOT NULL auto_increment,
				 post_id int(11) DEFAULT NULL,
				 kode varchar(15) NOT NULL,
				 title varchar(100) NOT NULL,
				 description varchar(255) DEFAULT NULL,
				 images varchar(255) DEFAULT NULL,
				 active tinyint(1) DEFAULT 1,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 KEY kode (kode)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core Country Data
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(2, 'CR', 'Costa Rica', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(3, 'TANZ', 'Tanzania', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(4, 'BZE', 'Belize', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(5, 'DR', 'Dominican Republic', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(6, 'PAN', 'Panama', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(8, 'MEX', 'Mexico', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(9, 'NIC', 'Nicaragua', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(17, 'NA', 'Non-Specific', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(19, 'US', 'United States', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(20, 'CA', 'Canada', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(30, 'PERU', 'Peru', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(31, 'CUB', 'Cuba', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_country
						(id, kode, title, active, created, created_by)
					  VALUES
					   	(32, 'COL', 'Colombia', 1, NOW(), 0)";
		self::execute_query($wpdb, $query);
	}

	//
	// create and populate region data
	function region_activate()
	{
		global $wpdb;

		//
		//Create region content table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_regions
				(
				 id int(11) NOT NULL auto_increment,
				 country_id int(11) NOT NULL,
				 kode varchar(15) NOT NULL,
				 title varchar(100) NOT NULL,
				 description varchar(255) DEFAULT NULL,
				 images varchar(255) DEFAULT NULL,
				 active tinyint(1) DEFAULT 1,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 KEY country_id (country_id),
				 KEY kode (kode),
				 UNIQUE uniqueness (kode, country_id)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core Region Data
		$country=$wpdb->get_col("SELECT c.id FROM " . $wpdb->prefix . "command_country c ORDER BY id", 0);
		
		foreach($country as $country_id)
		{
			$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_regions
						(id, country_id, kode, title, created, created_by)
					VALUES
						(NULL, " . $country_id . ", 'ALL', 'ALL', NOW(), 0)";
			self::execute_query($wpdb, $query);
		}

		//
		//Insert specific regions
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_regions
					(country_id, kode, title, created, created_by)
					  	SELECT c.id, 'AMA', 'Amazon', NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_country c WHERE c.title='Peru'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_regions
					(country_id, kode, title, created, created_by)
					  	SELECT c.id, 'COA', 'Coastal', NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_country c WHERE c.title='Peru'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_regions
					(country_id, kode, title, created, created_by)
					  	SELECT c.id, 'MP', 'Machu Picchu', NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_country c WHERE c.title='Peru'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_regions
					(country_id, kode, title, created, created_by)
					  	SELECT c.id, 'CC', 'Cancun', NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_country c WHERE c.title='Mexico'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_regions
					(country_id, kode, title, created, created_by)
					  	SELECT c.id, 'B', 'Baja', NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_country c WHERE c.title='Mexico'";
		self::execute_query($wpdb, $query);

	}

	//
	// create and populate program type data
	function program_type_activate()
	{
		global $wpdb;
		
		//
		//Create program_type table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_program_type
				(
				 id int(11) NOT NULL auto_increment,
				 title varchar(50) NOT NULL,
				 images varchar(255) DEFAULT NULL,
				 active tinyint(1) DEFAULT 1,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 UNIQUE (title)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core Program_type Data
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program_type
					(title, created, created_by)
				  VALUES
				   	('Adventure', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program_type
					(title, created, created_by)
				  VALUES
				   	('Enrichment', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program_type
					(title, created, created_by)
				  VALUES
				   	('Medical', NOW(), 0)";
		self::execute_query($wpdb, $query);

	}


	//
	// create and populate program data
	function program_activate()
	{
		global $wpdb;
		
		//
		//Create program table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_program
				(
				 id int(11) NOT NULL auto_increment,
				 post_id int(11) DEFAULT NULL,
				 program_type_id int(11) NOT NULL,
				 title varchar(50) NOT NULL,
				 kode varchar(15) NOT NULL,
				 exp_related tinyint(1) DEFAULT 0,
				 sequence int(11),
				 images VARCHAR(500) NULL,
				 active tinyint(1) DEFAULT 1,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) NOT NULL,
				 PRIMARY KEY (id),
				 UNIQUE (title)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core Program Data
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 1, cpt.id, 'Veterinary', 'VET', 1, 18, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 2, cpt.id, 'Well Child Intl', 'WCI', 1, 13, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 5, cpt.id, 'Dental', 'DENT', 1, 21, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 8, cpt.id, 'Physical Therapy', 'PT', 1, 19, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 9, cpt.id, 'Nursing', 'NUR', 1, 12, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 11, cpt.id, 'Optometry', 'OPT', 1, 14, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 15, cpt.id, 'Public Health', 'PH', 1, 11, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 18, cpt.id, 'Community Enrichment', 'SERV', 1, 22, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 30, cpt.id, 'Hike for Humanity', 'H4H', 1, 15, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Adventure'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 37, cpt.id, 'Education', 'EDU', 1, 23, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 51, cpt.id, 'PA Students', 'PA', 1, 16, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 57, cpt.id, 'Chiropractic', 'CHIRO', 1, 20, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 59, cpt.id, 'Med Students', 'MD', 1, 17, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 63, cpt.id, 'Global Health', 'GH', 0, 0, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 64, cpt.id, 'Global Medicine', 'GMD', 1, 13, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 65, cpt.id, 'Pharmacy', 'PHARM', 1, 2, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 66, cpt.id, 'Nutrition', 'NUTRI', 1, 7, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 67, cpt.id, 'Health Care Management', 'HCM', 1, 7, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 69, cpt.id, 'Continuing Education', 'CE', 0, 5, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 70, cpt.id, 'Photojournalism', 'PHOTO', 0, 6, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 71, cpt.id, 'Sportsmen Without Borders', 'SWB', 0, 8, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Adventure'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 72, cpt.id, 'Sports Medicine Program', 'SPM', 0, 9, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 73, cpt.id, 'Internship Programs', 'INTR', 0, 10, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 74, cpt.id, 'Rotation Programs', 'ROTA', 0, 1, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 75, cpt.id, 'Sustainability', 'SUST', 0, 0, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 76, cpt.id, 'Creative Arts', 'CRART', 0, 0, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 77, cpt.id, 'Ecology', 'ECO', 0, 0, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 78, cpt.id, 'Global Health Pathways', 'GHP', 0, 0, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Medical'";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_program
					(id, program_type_id, title, kode, exp_related, sequence, created, created_by)
					  	SELECT 79, cpt.id, 'Athletic Trainer', 'ATR', 0, 0, NOW(), 0 
					  	FROM " . $wpdb->prefix . "command_program_type cpt WHERE cpt.title='Enrichment'";
		self::execute_query($wpdb, $query);

	}

	//
	// create and populate gender data
	function gender_activate()
	{
		global $wpdb;
		
		//
		//Create Gender table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_gender
				(
				 id int(11) NOT NULL auto_increment,
				 title varchar(15) NOT NULL,
				 kode varchar(5) NOT NULL,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 UNIQUE (title)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core Gender Data
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_gender
					(id, title, kode, created, created_by)
				  VALUES
				   	(1, 'Male', 'M', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_gender
					(id, title, kode, created, created_by)
				  VALUES
				   	(2, 'Female', 'F', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_gender
					(id, title, kode, created, created_by)
				  VALUES
				   	(3, 'Unspecified', 'U', NOW(), 0)";
		self::execute_query($wpdb, $query);

	}

	//
	// create and populate team status data
	function team_status_activate()
	{
		global $wpdb;
		
		//
		//Create team status table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_team_status
				(
				 id int(11) NOT NULL auto_increment,
				 title varchar(20) NOT NULL,
				 kode varchar(5) NOT NULL,
				 tip varchar(250) NOT NULL,
				 pre tinyint(1) NOT NULL,
				 avail tinyint(1) NOT NULL,
				 prep tinyint(1) NOT NULL,
				 trav tinyint(1) NOT NULL,
				 cancel tinyint(1) NOT NULL,
				 sequence INT(11) NOT NULL,
				 user_sequence INT(11) NOT NULL,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 UNIQUE (title)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core team status Data
		$tip = 'These teams are about to travel and are no longer accepting sign-ups.';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(2, 'CLOSED', 'CLO', '" . $tip . "', 0, 0, 1, 0, 0, 5, 2, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$tip = 'These teams have already traveled.';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(3, 'COMPLETE', 'COM', '" . $tip . "', 0, 0, 0, 1, 0, 7, 4, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$tip = 'These teams are available for volunteers to join.';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(4, 'OPEN', 'OPE', '" . $tip . "', 0, 1, 0, 0, 0, 3, 3, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$tip = 'These teams were discontinued without traveling.';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(5, 'CANCELLED', 'CAN', '" . $tip . "', 0, 0, 0, 0, 1, 8, 5, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$tip = 'These teams are currently travelling';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(6, 'IN PROGRESS', 'ONG', '" . $tip . "', 0, 0, 0, 1, 0, 6, 1, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$tip = '';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(7, 'DRAFT', 'DFT', '" . $tip . "', 1, 0, 0, 0, 0, 1, 0, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$tip = 'This is a Custom Team awaiting Administrator approval.';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(8, 'PENDING APPROVAL', 'PEND', '" . $tip . "', 1, 0, 0, 0, 0, 2, 0, NOW(), 0)";
		self::execute_query($wpdb, $query);

		$tip = 'This trip is on and is still accepting volunteers!';
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_status
					(id, title, kode, tip, pre, avail, prep, trav, cancel, sequence, user_sequence, created, created_by)
				  VALUES
				   	(9, 'Go', 'GO', '" . $tip . "', 0, 1, 1, 0, 0, 4, 2, NOW(), 0)";
		self::execute_query($wpdb, $query);
	}

	//
	// create and populate team type data
	function team_type_activate()
	{
		global $wpdb;
		
		//
		//Create team type table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_team_type
				(
				 id int(11) NOT NULL auto_increment,
				 title varchar(20) NOT NULL,
				 public tinyint(1) NOT NULL DEFAULT 1,
				 page varchar(10) NOT NULL,	
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 UNIQUE (title)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Insert core team type Data
		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_type
					(title, public, page, created, created_by)
				  VALUES
				   	('Standard', 1, 'standard', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_type
					(title, public, page, created, created_by)
				  VALUES
				   	('High School', 1, 'minors', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_type
					(title, public, page, created, created_by)
				  VALUES
				   	('Reserved', 0, 'standard', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_type
					(title, public, page, created, created_by)
				  VALUES
				   	('IP Team', 0, 'standard', NOW(), 0)";
		self::execute_query($wpdb, $query);

		$query="INSERT IGNORE INTO " . $wpdb->prefix . "command_team_type
					(title, public, page, created, created_by)
				  VALUES
				   	('Custom High School', 0, 'minors', NOW(), 0)";
		self::execute_query($wpdb, $query);

	}

	//
	// create and populate team data
	function team_create()
	{
		global $wpdb;
		
		//
		//Create TEAM table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_TEAM
				(
				 id int(11) NOT NULL auto_increment,
				 post_id int(11) NOT NULL,
				 team_status_id int(11) NOT NULL,
				 team_type_id int(11) NOT NULL,
				 page_id int(11) DEFAULT NULL,
				 kode varchar(100) NOT NULL,	
				 hash varchar(25) NOT NULL,
				 cost double(11,2) NOT NULL DEFAULT 0,
				 appfee double(11,2) NOT NULL DEFAULT 0,
				 deposit double(11,2) NOT NULL DEFAULT 0,
				 arrival_city varchar(100) DEFAULT NULL,
				 arrival_country varchar(100) DEFAULT NULL,
				 departure_city varchar(100) DEFAULT NULL,
				 departure_country varchar(100) DEFAULT NULL,
				 minimum_volunteers int(11) NOT NULL,
				 maximum_volunteers int(11) NOT NULL,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 UNIQUE (post_id),
				 UNIQUE (kode),
				 UNIQUE (hash),
				 KEY (team_status_id),
				 KEY (team_type_id)
				)"; 
		self::execute_query($wpdb, $query);
	}

	//
	// create shopping cart table
	function cart_create()
	{
		global $wpdb;
		
		//
		//Create Cart
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_cart
				(
				 kode varchar(32) NOT NULL,
				 md5_or_id tinyint(1) NOT NULL,
				 items varchar(1000) NOT NULL,	
				 expires datetime NOT NULL,
				 PRIMARY KEY (kode)
				)"; 
		self::execute_query($wpdb, $query);
	}

	//
	// create rspv table
	function rsvp_create()
	{
		global $wpdb;
		
		//
		//Create RSVP table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_RSVP
				(
				 id int(11) NOT NULL auto_increment,
				 team_id int(11) NOT NULL,
				 user_id int(11) NOT NULL,	
				 title varchar(15) NOT NULL,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 KEY (team_id),
				 KEY (user_id)
				)"; 
		self::execute_query($wpdb, $query);
	}

	//
	// create schedule table
	function schedule_create()
	{
		global $wpdb;
		
		//
		//Create schedule table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_schedule
				(
				 id int(11) NOT NULL auto_increment,
				 team_id int(11) NOT NULL,
				 country_id int(11) NOT NULL,
				 region_id int(11) NOT NULL,
				 program_id int(11) NOT NULL,
				 cost double(10,2) NOT NULL DEFAULT '0.00',
				 start_date date NULL,
				 end_date date NULL,	
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id),
				 UNIQUE (team_id, country_id, region_id, program_id, start_date, end_date),
				 KEY (country_id),
				 KEY (region_id),
				 KEY (program_id),
				 KEY (start_date),
				 KEY (end_date)
				)"; 
		self::execute_query($wpdb, $query);
	}

	//
	// create itinerary table
	function itinerary_create()
	{
		global $wpdb;
		
		//
		//Create ITINERARY table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_itinerary
				(
				 id int(11) NOT NULL auto_increment,
				 title varchar(200) NOT NULL,
				 header TEXT,
				 footer TEXT,
				 countries varchar(200) NOT NULL DEFAULT 0,
				 programs varchar(200) NOT NULL DEFAULT 0,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Create ITINERARY_DAYS table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_itin_days
				(
				 id int(11) NOT NULL auto_increment,
				 itinerary_id int(11) NOT NULL,
				 day int(11) NOT NULL,
				 title VARCHAR(200) NOT NULL,				 	
				 description TEXT,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (id)
				)"; 
		self::execute_query($wpdb, $query);

		//
		//Create ITINERARY_TEAM table
		$query="CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "command_TEAM_ITINERARY
				(
				 team_id int(11) NOT NULL,
				 day int(11) NOT NULL,
				 title VARCHAR(200) NOT NULL,
				 description TEXT,
				 created datetime NOT NULL,
				 created_by int(11) NOT NULL,
				 modified timestamp NULL ON UPDATE CURRENT_TIMESTAMP,
				 modified_by int(11) DEFAULT NULL,
				 PRIMARY KEY (team_id, day)
				)"; 
		self::execute_query($wpdb, $query);

	}

	/**
	 * Create the table for team notes
	 */
	function messages_create()
	{
		global $wpdb;

		$query="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix ."command_message
			(
				id int(11) NOT NULL AUTO_INCREMENT,
				sender int(11) NOT NULL,
				recipient int(11) NOT NULL,
				msg_text text NOT NULL,
				created_at timestamp NOT NULL DEFAULT now(),
				read_at timestamp NULL,
				team int(11) NOT NULL,
				hide_at timestamp NULL,
				subject text NOT NULL,
				PRIMARY KEY (id)
			)
		";
		self::execute_query($wpdb, $query);
	}

	/**
	 * Create table for admin message management
	 */
	public function admin_messages_create()
	{
		global $wpdb;

		$query ="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix ."command_admin_message
			(
				event varchar(64) NOT NULL,
				meta_info text,
				message_text text,
				created_at timestamp NOT NULL DEFAULT now(),
				subject text,
				PRIMARY KEY(event)
			)";

		self::execute_query($wpdb,$query);
	}

	/**
	 * Create table for trigger metadata
	 */
	public function triggermeta_create()
	{
		global $wpdb;

		$query="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix ."command_triggermeta
			(
				id int(11) NOT NULL AUTO_INCREMENT,
				meta_id int(11),
				meta_key varchar(255),
				meta_value varchar(255),
				PRIMARY KEY(id)
			)";
		self::execute_query($wpdb,$query);
	}

	/**
	 * Create table for trigger objects
	 */
	public function trigger_create()
	{
		global $wpdb;

		$query="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix ."command_trigger
			(
				id int(11) NOT NULL AUTO_INCREMENT,
				trigger_type varchar(16) NOT NULL,
				meta text,
				run_time int DEFAULT NULL,
				name varchar(255) NOT NULL,
				PRIMARY KEY(id)
		)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (1, NULL, 'type', 'event')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (2, NULL, 'type', 'schedule')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (3, 1, 'name', 'Team Status Changed')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (4, 3, 'old_value', 'open')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (5, 4, 'new_value', 'go')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (6, 4, 'new_value', 'closed')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (7, 4, 'new_value', 'canceled')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (8, 1, 'name', 'Team Document Changed')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (9, 1, 'name', 'Team Document Added')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (10, 1, 'name', 'Personal Message Sent')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (11, 1, 'name', 'Team Message Sent')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (12, 1, 'name', 'New Sign-up')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (31, 2, 'interval', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (32, 2, 'unit', 'days')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (33, 2, 'unit', 'hours')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (34, 32, 'name', 'Before Trip Departure')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (35, 32, 'name', 'After Trip Departure')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (36, 32, 'name', 'Before Trip Return')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (37, 32, 'name', 'After Trip Return')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (38, 33, 'name', 'Before Trip Departure')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (39, 33, 'name', 'After Trip Departure')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (40, 33, 'name', 'Before Trip Return')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (41, 33, 'name', 'After Trip Return')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (42, 36, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (43, 36, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (44, 36, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (45, 37, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (46, 37, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (47, 37, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (48, 40, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (49, 40, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (50, 40, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (51, 41, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (52, 41, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (53, 41, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (58, 34, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (59, 34, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (60, 34, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (61, 35, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (62, 35, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (63, 35, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (64, 38, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (65, 38, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (66, 38, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    67, 39, 'filter', 'country')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (68, 39, 'filter', 'region')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (69, 39, 'filter', 'program')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (70, 42, 'filter_value', 'Choose')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (71, 43, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (72, 44, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (73, 45, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (74, 46, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (75, 47, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (76, 48, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (77, 49, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (78, 50, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (79, 51, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (80, 52, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (81, 53, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (82, 58, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (83, 59, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (84, 60, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (85, 61, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (86, 62, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (87, 63, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (88, 64, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (89, 65, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (90, 66, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (91, 67, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (92, 68, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (93, 69, 'filter_value', NULL)";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (94, 34, 'filter', 'less than 1/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (95, 34, 'filter', 'less than 2/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (96, 34, 'filter', 'less than 100% paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (97, 34, 'filter', 'unread team documents')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (98, 34, 'filter', 'trip information')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (99, 34, 'filter', 'Missing passport')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (100, 34, 'filter', 'Missing emergency contact')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (101, 35, 'filter', 'less than 1/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (102, 35, 'filter', 'less than 2/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (103, 35, 'filter', 'less than 100% paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (104, 35, 'filter', 'unread team documents')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (105, 35, 'filter', 'trip information')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (106, 35, 'filter', 'Missing passport')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (107, 35, 'filter', 'Missing emergency contact')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (108, 38, 'filter', 'less than 1/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (109, 38, 'filter', 'less than 2/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (110, 38, 'filter', 'less than 100% paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (111, 38, 'filter', 'unread team documents')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (112, 38, 'filter', 'trip information')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (113, 38, 'filter', 'Missing passport')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (114, 38, 'filter', 'Missing emergency contact')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (115, 39, 'filter', 'less than 1/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (116, 39, 'filter', 'less than 2/3 paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (117, 39, 'filter', 'less than 100% paid')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (118, 39, 'filter', 'unread team documents')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (119, 39, 'filter', 'trip information')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (120, 39, 'filter', 'Missing passport')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (121, 39, 'filter', 'Missing emergency contact')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (122, 34, 'filter', 'Team Status is Go')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (123, 38, 'filter', 'Team Status is Go')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (124, 122, 'action', 'Close Team')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (125, 38, 'action', 'Close Team')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (126, 37, 'filter', 'Team Status is Closed')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (127, 41, 'filter', 'Team Status is Closed')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (128, 126, 'action', 'Mark Team Completed')";
		self::execute_query($wpdb,$query);
		
		$query = "INSERT INTO `wp_command_triggermeta` 
					(`id`, `meta_id`, `meta_key`, `meta_value`) 
				  VALUES
				    (129, 127, 'action', 'Mark Team Completed')";
		self::execute_query($wpdb,$query);
		
	}
				
	/**
	 * Create table for trigger events (already triggered objects, to track the next trigger time
	 */
	public function trigger_event_create()
	{
		global $wpdb;

		$query="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix ."command_trigger_event
			(
				id int(11) NOT NULL AUTO_INCREMENT,
				trigger_id int(11) NOT NULL,
				triggered_at datetime DEFAULT now(),
				executed_at datetime DEFAULT NULL,
				trigger_obj text,
				PRIMARY KEY(id)
		)";
		self::execute_query($wpdb,$query);
	}

	//
	// execute_query
	// reads in db connection and query and executes the query on the db
	// Paramaters:
	//  database connection $db - the connection to be used
	//  string $query - the query to be executed
	function execute_query($db, $query)
	{
		if(substr($query,6) == "CREATE")
		{
			$db->query($query) || die (trigger_error($query . ':' . $db->last_error, E_USER_ERROR));			 
		} 
		else 
		{
			$affect = $db->query($query);
			
			if(!$affect && isset($db->last_error) && $db->last_error != '')
			{
				trigger_error("((" . $query . ")) : " . $db->last_error, E_USER_ERROR);
			}
		}
	}

}

?>