<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

Class PERMISSIONS
{

	private static $initialized = null;
	

	public function add_team_poc( $user_id, $team )
	{
		$user = new WP_User ( $user_id );
		$user->add_cap ('team_poc');
			
	}

	public function add_rep( $user_id )
	{
		$user = new WP_User ( $user_id );
		$user->add_cap ('isl_rep');
			
	}

	public function add_team_manager( $user_id, $team )
	{
		$user = new WP_User ( $user_id );
		$user->add_cap ('team_manager');
			
	}

	public function add_country_coordinator( $user_id, $country )
	{
		$user = new WP_User ( $user_id );
		$user->add_cap ('country_coordinator');
			
	}

	public function add_team_leader( $user_id, $team )
	{
		$user = new WP_User ( $user_id );
		$user->add_cap ('team_leader');
			
	}
	

	public function remove_team_poc( $user_id, $team )
	{
		//
		//Make sure only the noted team is removed
		$user = new WP_User ( $user_id );
		$user->remove_cap ('team_poc');
			
	}

	public function remove_rep( $user_id )
	{
		//
		//Make sure only the noted team is removed
		$user = new WP_User ( $user_id );
		$user->remove_cap ('isl_rep');
			
	}

	public function remove_team_manager( $user_id, $team )
	{
		//
		//Make sure only the noted team is removed
		$user = new WP_User ( $user_id );
		$user->remove_cap ('team_manager');
			
	}

	public function remove_country_coordinator( $user_id, $country )
	{
		//
		//Make sure only the noted country is removed
		$user = new WP_User ( $user_id );
		$user->remove_cap ('country_coordinator');
			
	}

	public function remove_team_leader( $user_id, $team )
	{
		//
		//Make sure only the noted team is removed
		$user = new WP_User ( $user_id );
		$user->remove_cap ('team_leader');
			
	}

}

?>