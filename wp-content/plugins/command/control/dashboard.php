<?php

//
//Make sure there cannot be direct access
defined ('ABSPATH') or die ('No direct access to plugins');

require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "message.admin.class.php");
require_once (CC_CLASSES_DIR . "message.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "docs.class.php");
require_once (CC_CLASSES_DIR . "user.class.php");

//
//Add The widgets we want, kill those we don't
add_action( 'admin_init', 'remove_dashboard_meta');
add_action( 'wp_dashboard_setup', 'add_dashboard_message');
//add_action( 'wp_dashboard_setup', 'add_dashboard_calendar');
add_action( 'wp_dashboard_setup', 'add_dashboard_chat');
add_action( 'wp_dashboard_setup', 'add_dashboard_featured');
add_filter('screen_options_show_screen', '__return_false');


if(user_role() == 'enhanced')
{
	add_filter('contextual_help', 'cc_remove_help', 999, 3);
	add_action('admin_bar_menu', 'cc_remove_bar_nodes', 999);
}


function cc_remove_bar_nodes( $wp_admin_bar )
{
	$wp_admin_bar->remove_node('wp-logo');
	$wp_admin_bar->remove_node('new-content');
	$wp_admin_bar->remove_node('comments');
	$wp_admin_bar->remove_node('wpseo-menu');
}

//
// add message to dashboard
function add_dashboard_message()
{
	$cc = new COMMAND_CENTER();
	
	if($cc->user_role() != 'enhanced')
	{
		wp_add_dashboard_widget(
			'dashboard_message',
			'Message from Jonathan',
			'display_dashboard_message'
		);

		global $wp_meta_boxes;

		$basic_dash = $wp_meta_boxes['dashboard']['normal']['core'];
		$arc_message_board = array( 'dashboard_message' => $basic_dash['dashboard_message']);
		$sort_dash = array_merge ($arc_message_board, $basic_dash);
		$wp_meta_boxes['dashboard']['normal']['core'] = $sort_dash;

	}
}

//
// add calendar to dashboard
function add_dashboard_calendar()
{
	$cc = new COMMAND_CENTER();
	
	if($cc->user_role() != 'enhanced')
	{
		wp_add_dashboard_widget(
			'dashboard_calendar',
			'Dashboard Calendar',
			'display_dashboard_calendar'
		);
	}
}

//
// add chat to dashboard
function add_dashboard_chat()
{
	$cc = new COMMAND_CENTER();
	
	if($cc->user_role() != 'enhanced')
	{
		wp_add_dashboard_widget(
			'dashboard_chat',
			'New Direct Messages (Staff Chat)',
			'display_dashboard_chat'
		);
	}
}

//
// add featured teams to dash
function add_dashboard_featured()
{
	$cc = new COMMAND_CENTER();
	
	if($cc->user_role() != 'enhanced')
	{
		wp_add_dashboard_widget(
			'dashboard_featured',
			'Featured Teams',
			'display_dashboard_featured'
		);
	}
}

//
// display message on dashboard
function display_dashboard_message()
{
	$post_id = 4071;
	$message = get_post($post_id);
	
	echo apply_filters('the_content', @$message->post_content);
	
	echo "<a href=\"" . esc_url(get_edit_post_link($post_id)) . "\">Edit</a>";
}

//
// display calendar on dash
function display_dashboard_calendar()
{
global $wp_meta_boxes;
	echo "Calendar goes here";
	//echo var_dump($wp_meta_boxes['dashboard']);
}

//
// Dashboard Chat
function display_dashboard_chat() {
	wp_enqueue_script('dashboard_chat', plugins_url() . '/command/js/dashboard.chat.js', array( 'jquery' ), '0.04');

	$user = new USER();
	$actual = 0;
	
	//
	//This user id is for the overrides for admin access to other chats.
	$the_user_id = (($user->user_group(get_current_user_id())=="super" || $user->user_group(get_current_user_id())=="admin") ? 0 : get_current_user_id());

	// Get all the unread messages
	$unreadMessages = apply_filters('cmd_unread_count_all', $the_user_id);
	//echo var_dump($unreadMessages);

	//$flaggedMessages = apply_filters('cmd_chat_team_list_callback', $user_id);
	$flaggedMessages = cmd_chat_team_list(get_current_user_id());

	$html = '';
	$html .= '<div id="dashboard-chat-inbox">';
	
	if (count($unreadMessages) > 0 || count($flaggedMessages) > 0) {
		if(!empty($flaggedMessages) && count($flaggedMessages) > 0)
		{
			foreach($flaggedMessages as $team_data => $index) {
				$team_id = str_replace(',','',substr($team_data, 0, strpos($team_data, '.')));
				$user_id = floatval(substr($team_data, strpos($team_data, '.') +1));
				
				$team = new TEAM();
				$team->set_team($team_id);
				
				do_action('cmd_chat_check', $team_id, $team->value('hash'));
				//echo $user_id;
				
				if(isset($user_id) && $user_id > 0)
				{
					$usr = get_userdata($user_id);

					$html .= '<div class="chat-convo" data-user-id="'.$user_id.'" data-team-id="'.$team_id.'" data-admin="'.$the_user_id.'">';
					$html .= '	<a class="chat-popout-trigger" title="View this chat">';
					$html .= '		<span class="team-title">' . $usr->first_name . ' ' . $usr->last_name . '</span>
									<span class="popout-trigger-text">
										View <span class="unread-count">Follow up</span>
										<i class="fa fa-external-link" aria-hidden="true"></i>
									</span>
								</a>';
					$html .= '  <a class="flag-chat-trigger flagged" title="Remove the flag from this chat"><i class="fa fa-flag" aria-hidden="true"></i></a>';
					$html .= '</div>';

					$actual ++;
				}
			}
		}
		else
		{
			$flaggedMessages = array();
		}
		
		if(count($unreadMessages) > 0)
		{
			foreach($unreadMessages as $team_id => $data) {
				if(isset($data['check_one']) && $data['check_one'])
				{
					$private = (empty($data['private']) ? 'false' : 'true');

					$check_team = $data['team_id'] . '.' . $data['check_one_val'];

					$team_id = str_replace(',','',$data["team_id"]);
					$user_id = str_replace(',','',$data['check_one_val']);

					$team = new TEAM();
					$team->set_team($team_id);

					do_action('cmd_chat_check', $team_id, $team->value('hash'));

					if(!in_array($check_team, array_keys($flaggedMessages)))
					{
						if ($private == 'true') 
						{ 
							//$html .= '<div class="chat-convo" data-user-id="'.get_current_user_id().'" data-team-id="'.$data["team_id"].'">';
							$html .= '<div class="chat-convo" data-user-id="'.$user_id.'" data-team-id="'.$team_id.'" data-admin="'.$the_user_id.'">';
							$html .= '	<a class="chat-popout-trigger" title="View this chat">';
							$html .= '		<span class="team-title">'.$data['title'].'</span>
											<span class="popout-trigger-text">
												View <span class="unread-count">'.$data['unread'].' unread messages</span>
												<i class="fa fa-external-link" aria-hidden="true"></i>
											</span>
										</a>';
							$html .= '  <a class="flag-chat-trigger" title="Flag this chat"><i class="fa fa-flag" aria-hidden="true"></i></a>';
							$html .= '</div>';

							$actual ++;

						}
					}
				}
			}
		}
	} else {
		$actual ++;
		$html .= '<p class="no-new-chats">No new chats to review!</p>';
	}

	if($actual == 0 )
	{
		$html .= '<p class="no-new-chats">No new chats to review!</p>';
	}
	
	$html .= '</div>';
	echo $html;
}

//
// display featured teams on dash
function display_dashboard_featured()
{
	$team = new TEAM();

	$max_teams = 8;
	$post_id = 4633;
	$cur_teams = explode(",", get_post_meta($post_id, 'featured_teams', true));
	$upcoming_teams = $team->upcoming(); 
	$found = array();

	wp_enqueue_script('cmd_featured_teams', plugins_url() . '/command/js/featured.teams.js');
	wp_localize_script('cmd_featured_teams', 'svars', array('post_id' => $post_id));


	//
	//Check each currently listed team is still available
	foreach($cur_teams as $id)
	{
		if(isset($upcoming_teams[$id]))
		{
			$found[]=$id;
		}
	}
	
	//
	//Reset Teams list now
	$cur_teams = array_unique($found);
	update_post_meta($post_id, 'featured_teams', implode(",",$cur_teams));

	//
	//List boxes now	
	for($ordinal = 1; $ordinal<=$max_teams; $ordinal++)
	{
		echo "<div class='team-row'>";

		echo "<h5>Team " . $ordinal . "</h5> <SELECT class ='featured' name='spot_" . $ordinal . "' id='spot_" . $ordinal . "'>"; 
		echo "<OPTION value=0> -- SELECT --</OPTION>";
			
		foreach($upcoming_teams as $team_id => $data)
		{
			//$team_display = date("Y-m-d", strtotime($data['start_date'])) . " : " . $data['team_name'];
			$team_display = $data['team_name'] . " : " . $data['code'];
			echo "<OPTION value='" . $team_id . "'" . ((isset($cur_teams[0]) && ($cur_teams[0] == $team_id)) ? " SELECTED" : "") . ">" . $team_display . "</OPTION>";
		}
		echo "</SELECT>";
		
		if(isset($cur_teams[0]))
		{
			if(isset($upcoming_teams[$cur_teams[0]]))
			{
				array_shift($cur_teams);	
			}
		}
		
		echo "</div>";
	}
}

//
// remove meta data from dash
function remove_dashboard_meta()
{
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
	remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal');
	remove_meta_box( 'joints_rss_dashboard_widget', 'dashboard', 'normal');
	
	remove_action('welcome_panel', 'wp_welcome_panel');
}

function cc_remove_help($old_help, $screen_id, $screen)
{
	$screen->remove_help_tabs();
	return $old_help;
}

function user_role()
{
	$roles = get_user_meta(get_current_user_id(), 'wp_capabilities', true);
	if ($roles) {
		$roles = array_keys($roles);
		reset($roles);
		return current($roles);
	}
}