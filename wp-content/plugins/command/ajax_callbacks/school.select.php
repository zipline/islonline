<?php


//
//Callback for school.select.js script 
add_action('wp_ajax_cmd_school_select','cmd_school_select_callback');
function cmd_school_select_callback()
{
	global $wpdb;
	$ret_val = '';
	
	$query = "SELECT INSTNM FROM " . $wpdb->prefix . "command_schools WHERE INSTNM LIKE '%" . $_POST['search_term'] . "%'";
	$results = $wpdb->get_col($query);
	 
	if($results && count($results)<25)
	{
		if(count($results) < 1)
		{
			$suggenstions[] = "No Schools found, please refine criteria.";
		}
		else
		{	
			foreach($results as $name)
			{
				$suggestions[] = $name;
			}
		}
	}
	else
	{
		$suggestions[] = count($results) . " Results, please refine criteria.";
	}

	echo json_encode($suggestions);
	wp_die();
}


?>