<?php

//
//Callback for region.load.js script to provide options values for select box
add_action('wp_ajax_cmd_tl_change','cmd_tl_change_callback');
function cmd_tl_change_callback()
{
	if(!empty($_POST['tl']) && !empty($_POST['rsvp']))
	{
		update_post_meta($_POST['rsvp'], '_cmd_team_leader', $_POST['tl']);
	}
	wp_die();
}
?>