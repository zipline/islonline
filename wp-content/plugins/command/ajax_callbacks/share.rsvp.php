<?php
require_once (CC_CLASSES_DIR . "email.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");

//
//Callback for rsvp.share.js script
add_action('wp_ajax_cmd_rsvp_share','cmd_rsvp_share_callback');
add_action('wp_ajax_nopriv_cmd_rsvp_share','cmd_rsvp_share_callback');

function cmd_rsvp_share_callback()
{

	if(!empty($_POST['post_id']) &&
	   !empty($_POST['rsvp_id']) &&
	   !empty($_POST['user_id']) &&
	   !empty($_POST['email'])
	)
	{
		$mail = new EMAIL();

		//
		//Check to see if we already know the subject
		$check_who = get_user_by('email', $_POST['email']);
		$write_user_data = false;
		
 		//
		//Set appropriate information if we know, or prep to create account if we don't
		if(!empty($check_who))
		{
			$res = $mail->to($check_who->first_name . " " . $check_who->last_name, $_POST['email']);

			//
			//Check RSVP to see if one is already active
			$rsvp = new RSVP();
			$rsvp->set_rsvp_id($_POST['rsvp_id']);
			$rsvp->fetch_rsvp();
			$reservation = $rsvp->get_rsvp();

			//
			//Check to see if the user already has an RSVP for the team
			if( $rsvp->get_rsvp_id ( $rsvp->set_id_by_team_user ( $reservation['team_id'] , $check_who->ID) ) )
			{
				echo json_encode(array('response' => "RSVP Already exists for user"));
				wp_die();
			}
		
		}
		else
		{
			$res = $mail->to('',$_POST['email']);
			$password = md5($_POST['email']);
			$user_id = wp_insert_user(array('user_email' => $_POST['email'],
											'user_login' => $_POST['email'],
											'user_pass' => $password,
											'user_registered' => date('Y-m-d H:i:s')
											)
									  );	
			$check_who = get_user_by('email', $_POST['email']);
			$write_user_data = true;
			
			if(empty($user_id))
			{
				trigger_error('User cannot be created', E_USER_ERROR);
			}
		}

		//
		//Set remainder of core email pieces
		$mail->from('International Service Learning','noreply@islonline.org');
		$mail->subject("You've been invited");
		$mail->group(false);

		//
		//No errors thus far....
		if(empty($mail->error_level()))
		{
			//
			//Find sender user
			$user = get_user_by('id', $_POST['user_id']);
			
			//
			//Write in post meta data
			update_post_meta($_POST['post_id'], '_cmd_rsvp_shared_to', $_POST['email']);
			update_post_meta($_POST['post_id'], '_cmd_rsvp_shared_by', $_POST['user_id']);

			$transfer_key = urlencode($_POST['email'] . "___" . $_POST['user_id'] . "___" . $_POST['post_id'] . "___" . $_POST['rsvp_id']);

			//
			///Construct the rest of the email
			$message = "Welcome to ISL!<BR>" . PHP_EOL;
			$message .= "<BR>" . PHP_EOL;
			$message .= "You have been invited by " . $user->first_name . " " . $user->last_name . " to join them on a trip with International Service Learning<BR>" . PHP_EOL;			

			if($write_user_data)
			{
				$message .= '<BR>' . PHP_EOL;
				$message .= 'An account has been created for you to speed up your acceptance!<BR>' . PHP_EOL;
				$message .= '<B>User Name:</B> ' . $check_who->user_login . '<BR>' . PHP_EOL;
				$message .= '<B>Password:</B> ' . $password . '<BR>' . PHP_EOL;
				$message .= '<BR>' . PHP_EOL;
				$message .= '<a href="' . site_url() . '/my-isl/">CLICK HERE</a> to log into your account!<BR>' . PHP_EOL;
			}
			else
			{
				$message .= '<BR>' . PHP_EOL;
				$message .= 'You must first log into your account before accepting this invitation';
				$message .= '<BR>' . PHP_EOL;
				$message .= '<a href="' . site_url() . '/my-isl/">CLICK HERE</a> to log into your account!<BR>' . PHP_EOL;
			}

			$message .= '<BR>' . PHP_EOL;
			$message .= '<a href="' . site_url() . '/my-isl/?transfer=' . $transfer_key . '">CLICK HERE!</a> to accept this invitation.<BR>' . PHP_EOL;

			if($write_user_data)
			{
				$message .= 'After you\'ve logged into your account' . PHP_EOL;
			}

			//
			//Finalize the message and send it
			$mail->message($message);
			$results = $mail->send();
			echo json_encode(array('response' => "Email sent"));
			wp_die();
		}
		else
		{
			trigger_error($mail->error_list_text(), E_USER_ERROR);
		}
	}
	else
	{
		trigger_error('Information Missing', E_USER_ERROR);
	}
}

//
//Callback for rsvp.share.js script
add_action('wp_ajax_cmd_rsvp_unshare','cmd_rsvp_unshare_callback');
add_action('wp_ajax_nopriv_cmd_rsvp_unshare','cmd_rsvp_unshare_callback');

function cmd_rsvp_unshare_callback()
{

	if(!empty($_POST['post_id']) &&
	   !empty($_POST['rsvp_id']) &&
	   !empty($_POST['user_id'])
	)
	{
		//
		//Gather pertinent information
		$shared_to = get_post_meta($_POST['post_id'], '_cmd_rsvp_shared_to', true);
		$shared_by = get_post_meta($_POST['post_id'], '_cmd_rsvp_shared_by', true);

		//
		//Make sure the right user is revoking
		if($_POST['user_id'] == $shared_by)
		{
			$mail = new EMAIL();
	
			//
			//Check to see if we already know the subject
			$check_who = get_user_by('email', $shared_to);
			
			//
			//Set appropriate information if we know, or prep to create account if we don't
			if(!empty($check_who))
			{
				$res = $mail->to($check_who->first_name . " " . $check_who->last_name, $shared_to);
				$user_id = $check_who->ID;		
			}
			else
			{
				$res = $mail->to('',$shared_to);
				$user_id = 0;	
			}
	
			//
			//Set remainder of core email pieces
			$mail->from('No Reply','noreply@islonline.org');
			$mail->subject("Your invitation has been cancelled");
			$mail->group(false);
	
			//
			//No errors thus far....
			if(empty($mail->error_level()))
			{
				//
				//Find sender user
				$user = get_user_by('id', $_POST['user_id']);
				
				//
				//Write in post meta data
				delete_post_meta($_POST['post_id'], '_cmd_rsvp_shared_to');
				delete_post_meta($_POST['post_id'], '_cmd_rsvp_shared_by');
	
				//
				///Construct the rest of the email
				$message = "Sorry," . PHP_EOL;
				$message .= PHP_EOL;
				$message .= "Your invitation has been cancelled by " . $user->first_name . " " . $user->last_name . " to join them on a trip with International Service Learning";			
	
				//
				//Finalize the message and send it
				$mail->message($message);
				$results = $mail->send();
				wp_die();
			}
			else
			{
				trigger_error('Revokation by wrong individual', E_USER_ERROR);
			}
		}
		else
		{
			trigger_error($mail->error_list_text(), E_USER_ERROR);
		}
	}
	else
	{
		trigger_error('Information Missing', E_USER_ERROR);
	}
}

?>