<?php

require_once CC_CLASSES_DIR . 'docs.class.php';


//
//Callback for docs.sig.js script to enter information into database
add_action('wp_ajax_cmd_doc_signature','cmd_doc_signature_callback');
add_action('wp_ajax_nopriv_cmd_doc_signature','cmd_doc_signature_callback');
function cmd_doc_signature_callback()
{

	$docs = new DOCS();
	$docs->fetch_read();
	
	if(!$docs->is_read($_POST['doc_id']))
	{
		$docs->post_id($_POST['post_id']);
		$docs->update_read($_POST['doc_id'], $_POST['signature']);
	}
	
	echo "php";
	wp_die();
}

?>