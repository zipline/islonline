<?php

require_once CC_CLASSES_DIR . 'cart.class.php';

//
//Callback for cart.add.js script to enter information into cart.
add_action('wp_ajax_cmd_cart_loading','cmd_cart_loading_callback');
add_action('wp_ajax_nopriv_cmd_cart_loading','cmd_cart_loading_callback');

function cmd_cart_loading_callback()
{
	$cart = new CART(true);
//echo var_dump($_POST);
	switch( $_POST['item_type'] )
	{
		case 'rsvp':
			$cart->add_RSVP( $_POST['item_id'], $_POST['item_cost'], $_POST['item_quan'] );
			break;
			
		case 'add-on':
			$cart->add_AddOn( $_POST['item_id'], $_POST['add_on'], $_POST['item_cost'], $_POST['item_quan'] );
			break;
			
		case 'cap':
			$cart->add_CAP( $_POST['item_id'] );
			break;
			
		case 'coupon':
			$cart->add_Coupon( $_POST['item_id'] );
			break;
			
	}

	wp_die();
}

//
//Callback for cart.add.js script to set information into cart.
add_action('wp_ajax_cmd_cart_set','cmd_cart_set_callback');
add_action('wp_ajax_nopriv_cmd_cart_set','cmd_cart_set_callback');
function cmd_cart_set_callback()
{
	$cart = new CART(true);

	switch( $_POST['item_type'] )
	{
		case 'rsvp':
			$cart->set_RSVP( $_POST['item_id'], $_POST['item_cost'], $_POST['item_quan'] );
			break;
			
		case 'add-on':
			$cart->add_AddOn( $_POST['team_id'], $_POST['item_id'], $_POST['item_cost'], $_POST['item_quan'] );
			break;
			
		case 'cap':
			$cart->add_CAP( $_POST['item_id'] );
			break;
			
		case 'coupon':
			$cart->add_Coupon( $_POST['item_id'] );
			break;
			
	}
	
	wp_die();
}

//
//Callback for cart.add.js script to enter information into cart.
add_action('wp_ajax_cmd_cart_remove','cmd_cart_remove_callback');
add_action('wp_ajax_nopriv_cmd_cart_remove','cmd_cart_remove_callback');
function cmd_cart_remove_callback()
{
echo var_dump($_POST);
	$cart = new CART(true);
	$cart->remove_Item($_POST['main_item'] , 'All', $_POST['item_id'], $_POST['item_quan']);
	echo $cart->message();
	
	wp_die();
}


add_action('wp_ajax_cmd_cart_count','cmd_cart_count_callback');
add_action('wp_ajax_nopriv_cmd_cart_count','cmd_cart_count_callback');
function cmd_cart_count_callback()
{
	$cart = new CART( false );
	echo $cart->Count( 'Reservation' ) + $cart->Count( 'Service' ) + $cart->Count( 'Merchandise' ) + $cart->Count( 'Add-on' );
	wp_die();
}