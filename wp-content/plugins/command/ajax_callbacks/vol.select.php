<?php

//
//Possible necessary classes
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "cart.class.php");


//
//Callback for school.vol.js script 
add_action('wp_ajax_cmd_volunteer_select','cmd_vol_select_callback');
add_action('wp_ajax_cmd_volunteer_add','cmd_vol_add_callback');

function cmd_vol_select_callback()
{
	global $wpdb;
	$meta_query = array();
	$whole_name = explode(" ", $_POST['search_term']);

	if(isset($whole_name[0]))
	{
		$first_name = $whole_name[0];
	}
	if(isset($whole_name[1]))
	{
		$last_name = $whole_name[1];
	}

	if(!empty($first_name))
	{
		$f_name_arg = array('key' => 'first_name',
						    'value' => $first_name,
						   	'compare' => 'LIKE');
	}
	if(!empty($last_name))
	{
		$l_name_arg = array('key' => 'last_name',
						    'value' => $last_name,
						   	'compare' => 'LIKE');
	}

	if(isset($f_name_arg) && isset($l_name_arg))
	{
		$meta_query['relation'] = 'AND';
	}
	if(isset($f_name_arg))
	{
		$meta_query[] = $f_name_arg;
	}
	if(isset($l_name_arg))
	{
		$meta_query[] = $l_name_arg;
	}
	
	if(count($meta_query) >= 1)
	{
		$args = array('meta_query' => $meta_query);
	}
	
	//
	//run query
	$suggestions = array();
	$user_query = new WP_User_query( $args );
	if( !empty( $user_query->results ) )
	{
		foreach($user_query->results as $user)
		{
			$suggestions[] = "[" . $user->ID . "] " . $user->first_name . " " . $user->last_name . " (" . $user->user_email . ")";
		}
	}
	else
	{
		$suggestions[] = "No Volunteers Found, please refine criteria.";
	}
	
	echo json_encode($suggestions);
	wp_die();
}


function cmd_vol_add_callback()
{

	if(!empty($_POST['team_id']) && !empty($_POST['user_id']))
	{
		$team = new TEAM();
		$finance = new FINANCE();
		$cart = new CART( false, $_POST['user_id'] );

		$team->set_team($_POST['team_id']);
		$cart->add_RSVP( $team->value('id'), $team->value('cost'), 1 );

		$trans_data = array('amount' => 0,
							'form' => 'No Payment',
							'response' => 'Approved',
							'transaction_id' => date('Ymdhis'),
							'raw_request' => 'No Request Data,  No Payment Due',
							'raw_response' => 'Approved with 0 down',
							'ip_address' => $_SERVER['REMOTE_ADDR'],
							'user_agent' => $_SERVER['HTTP_USER_AGENT'],
							'pp_trans_id' => date('Ymdhis')
							);		
		
		$finance->change_user($_POST['user_id']);
		$finance->new_transaction($trans_data, 'defined', 'checkout');	
		$finance->move_CartToOrder( $cart, false );
		//
		//2018-03-23 DJT Aded payment to link trans in reports
		$finance->apply_payments('defined', 'checkout', $trans_data['amount'], 'All', 'Zero payment link', NULL, true);
		$cart->Clear();	

		do_action('cmd_checkout', $team->value('id'), $_POST['user_id']);

	}

}
?>