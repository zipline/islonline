<?php

//
//Callback for featured.teams.js script to enter information into featured teams listing.
add_action('wp_ajax_cmd_featured_teams','cmd_featured_teams_callback');
function cmd_featured_teams_callback()
{

	$pairs = explode(";", $_POST['data']);
	
	foreach($pairs as $values)
	{
		$pairs = array();
		$pairs = explode(":", $values);
		
		if(isset($pairs[0]) && isset($pairs[1]))
		{
			$teams[$pairs[0]] = $pairs[1];	
		}
	}

	ksort($teams);

	update_post_meta($_POST['post_id'], 'featured_teams', implode(",",$teams));
	
	wp_die();
}


?>