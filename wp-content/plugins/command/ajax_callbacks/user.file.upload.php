<?php

//
//Callback for docs.sig.js script to enter information into database
add_action('wp_ajax_cmd_settings_user_file_upload','cmd_settings_user_file_upload_callback');
add_action('wp_ajax_nopriv_cmd_settings_user_file_upload','cmd_settings_user_file_upload_callback');
function cmd_settings_user_file_upload_callback()
{

	$url = $_POST['url'];
	$post_id = $_POST['id'];

	wp_update_post( array( 'ID' => $_POST['id'], 'post_type' => 'cp_special_doc') );
	
	echo json_encode(array('success' => true));
	wp_die();
}

?>