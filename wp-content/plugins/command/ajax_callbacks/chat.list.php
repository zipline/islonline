<?php


//
//Callback for cart.add.js script to enter information into cart.
add_action('wp_ajax_cmd_chat_team_save','cmd_chat_team_save_callback');
add_action('wp_ajax_cmd_chat_team_delete','cmd_chat_team_delete_callback');
add_action('wp_ajax_cmd_chat_team_list','cmd_chat_team_list_callback');

function cmd_chat_team_save_callback()
{
	$user = get_current_user_id();
	
	if(!empty($_POST['team_id']) && $user)
	{
		$all_chats = cmd_chat_team_list($user);
		unset($all_chats[0]);
		$all_chats[$_POST['team_id'] . '.' . $_POST['user_id']] = 1;
		
		update_user_meta($user, '_chatter_team_list', $all_chats);
	}
	wp_die();
}

function cmd_chat_team_delete_callback()
{
	$user = get_current_user_id();
	
	if(!empty($_POST['team_id']) && !empty($_POST['user_id']) && $user)
	{
		$all_chats = cmd_chat_team_list($user);
		unset($all_chats[$_POST['team_id'] . '.' . $_POST['user_id']]);
		
		update_user_meta($user, '_chatter_team_list', $all_chats);
	}
	wp_die();
}

function cmd_chat_team_list_callback()
{
	$user = get_current_user_id();
	
	return cmd_chat_team_list($user);
	wp_die();
}

function cmd_chat_team_list($user)
{
	$core_list = get_user_meta($user, '_chatter_team_list', false);

	if(!empty($core_list[0]))
	{
		return $core_list[0];
	}
	else
	{
		return false;
	}
	
}
?>