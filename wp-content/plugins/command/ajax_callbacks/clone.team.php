<?php

//
//Possible necessary classes
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "cart.class.php");


//
//Callback for school.vol.js script 
add_action('wp_ajax_cmd_clone_team','cmd_clone_team_callback');

function cmd_clone_team_callback()
{
	//
	//Parse name data
	if(isset($_POST['team_name']) && strpos($_POST['team_name'], '/') > 1)
	{
		//
		//Find Team
		$team = new TEAM();

		list($team_code, $team_hash) = explode("/", $_POST['team_name']);
		$team_code = trim($team_code); 
		$team_hash = trim($team_hash);

		$team_code_check = $team->find_team_by_name($team_code);
		$team_hash_check = $team->find_team_by_name($team_hash);

		//
		//If all of our checks are good, then we have a real team
		if($team_code_check == $team_hash_check)
		{
			$team->set_team($team_code_check);

			//
			//Dev DB setup
			$cur_db_address = '10.210.64.80';
			$cur_db_login = 'wordpress';
			$cur_db_pwd = '0verL0rdF!dd!ch';
			$cur_db_pre = "dev.wp_";

			$cur_db = new mysqli($cur_db_address, $cur_db_login, $cur_db_pwd);

			
			//
			//Copy Team to Dev or get ID if Team already exists
			$query = "SELECT id, post_id FROM " . $cur_db_pre . "command_TEAM WHERE kode = '" . $team_code . "' AND hash = '" . $team_hash . "'";
			$results = $cur_db->query($query);

			if($results->num_rows > 0)
			{
				$result = $results->fetch_object();
				$dev_team_id = $result->id;	
				$dev_post_id = $result->post_id;
			}
			else
			{
				$old_post = get_post($team->value('post'));
				
				$ins_query = 'INSERT INTO ' . $cur_db_pre . 'posts ';
				$ins_query .= ' (';
				$ins_query .= '  post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, ';
				$ins_query .= '  comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, ';
				$ins_query .= '  post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, ';
				$ins_query .= '  comment_count ';
				$ins_query .= ' )';
				$ins_query .= ' VALUES ';
				$ins_query .= ' (';
				$ins_query .= "  " . $old_post->post_author . ", ";
				$ins_query .= "  '" . $old_post->post_date . "', ";
				$ins_query .= "  '" . $old_post->post_date_gmt . "', ";
				$ins_query .= "  '" . $old_post->post_content . "', ";
				$ins_query .= "  '" . $old_post->post_title . "', ";
				$ins_query .= "  '" . $old_post->post_excerpt . "', ";
				$ins_query .= "  '" . $old_post->post_status . "', ";
				$ins_query .= "  '" . $old_post->comment_status . "', ";
				$ins_query .= "  '" . $old_post->ping_status . "', ";
				$ins_query .= "  '" . $old_post->post_password . "', ";
				$ins_query .= "  '" . $old_post->post_name . "', ";
				$ins_query .= "  '" . $old_post->to_ping . "', ";
				$ins_query .= "  '" . $old_post->pinged . "', ";
				$ins_query .= "  '" . $old_post->post_modified . "', ";
				$ins_query .= "  '" . $old_post->post_modified_gmt . "', ";
				$ins_query .= "  '" . $old_post->post_content_filtered . "', ";
				$ins_query .= "   " . $old_post->post_parent . ", ";
				$ins_query .= "  '" . str_replace('new.islonline.org', 'dev.islonline.org', $old_post->post_modified_gmt) . "', ";
				$ins_query .= "   " . $old_post->menu_order . ", ";
				$ins_query .= "  '" . $old_post->post_type . "', ";
				$ins_query .= "  '" . $old_post->post_mime_type . "', ";
				$ins_query .= "  '" . $old_post->comment_count . "' ";
				$ins_query .= ' )';

				$cur_db->query($ins_query);
				$dev_post_id = $cur_db->insert_id;
				
				$ins_query = 'INSERT INTO ' . $cur_db_pre . 'command_TEAM ';
				$ins_query .= ' (';
				$ins_query .= '  post_id, team_status_id, team_type_id, kode, hash, cost, appfee, deposit, ';
				$ins_query .= '  arrival_city, arrival_country, departure_city, departure_country, minimum_volunteers, maximum_volunteers, ';
				$ins_query .= '  team_chat, messaging, no_finances ';
				$ins_query .= ' )';
				$ins_query .= ' VALUES ';
				$ins_query .= ' (';
				$ins_query .= "  " . $dev_post_id . ", ";
				$ins_query .= "  " . $team->value('status') . ", ";
				$ins_query .= "  " . $team->value('type') . ", ";
				$ins_query .= "  '" . $team->value('code') . "', ";
				$ins_query .= "  '" . $team->value('hash') . "', ";
				$ins_query .= "  " . $team->value('cost') . ", ";
				$ins_query .= "  " . $team->value('appfee') . ", ";
				$ins_query .= "  " . $team->value('deposit') . ", ";
				$ins_query .= "  '" . $team->value('arrival_city') . "', ";
				$ins_query .= "  '" . $team->value('arrival_country') . "', ";
				$ins_query .= "  '" . $team->value('departure_city') . "', ";
				$ins_query .= "  '" . $team->value('departure_country') . "', ";
				$ins_query .= "  " . $team->value('min') . ", ";
				$ins_query .= "  " . $team->value('max') . ", ";
				$ins_query .= "  " . $team->value('team_chat') . ", ";
				$ins_query .= "  " . $team->value('messaging') . ", ";
				$ins_query .= "  " . $team->value('no_finances');
				$ins_query .= ' )';

				$cur_db->query($ins_query);
				$dev_team_id = $cur_db->insert_id;
			}
			
			if(!empty($dev_team_id) && !empty($dev_post_id))
			{
				global $wpdb;
				
				//
				//Reset VEC info
				$vec_del_query = "DELETE from " . $cur_db_pre . "usermeta WHERE meta_value like 'VEC' AND meta_key = 'cmd_team" . $dev_team_id . "_staff_pos'";
				$cur_db->query($vec_del_query);
				
				$vec_ins_query = "INSERT INTO " . $cur_db_pre . "usermeta 
									(user_id, meta_key, meta_value)
									VALUES
									(1, 'cmd_team" . $dev_team_id . "_staff_pos', 'VEC')";
				$cur_db->query($vec_ins_query);

				
				//
				//Copy Team Itinerary to Dev
				$itin_query = "SELECT * FROM " . $wpdb->prefix . "command_TEAM_ITINERARY WHERE team_id = " . $team->value('id');
				$itin_data = $wpdb->get_results( $itin_query );

				foreach($itin_data as $itinerary)
				{
					$itin_ins_query = '';
					$itin_ins_query .= "REPLACE INTO " . $cur_db_pre . "command_TEAM_ITINERARY ";
					$itin_ins_query .= ' (';
					$itin_ins_query .= '  team_id, day, title, description, created, created_by, modified, modified_by ';
					$itin_ins_query .= ' )';
					$itin_ins_query .= ' VALUES ';
					$itin_ins_query .= ' (';
					$itin_ins_query .= "  " . $dev_team_id . ", ";
					$itin_ins_query .= "  " . $itinerary->day . ", ";
					$itin_ins_query .= "  '" . $itinerary->title . "', ";
					$itin_ins_query .= "  '" . $itinerary->description . "', ";
					$itin_ins_query .= "  '" . $itinerary->created . "', ";
					$itin_ins_query .= "  " . $itinerary->created_by . ", ";
					$itin_ins_query .= "  '" . $itinerary->modified . "', ";
					$itin_ins_query .= "  " . $itinerary->modified_by . " ";
					$itin_ins_query .= ' )';

					$cur_db->query($itin_ins_query);
				}

				//
				//Copy Team Schedule to Dev
				$sched_query = "SELECT * FROM " . $wpdb->prefix . "command_schedule WHERE team_id = " . $team->value('id');
				$sched_data = $wpdb->get_results( $sched_query );

				foreach($sched_data as $schedule)
				{
					$sched_ins_query = '';
					$sched_ins_query .= "REPLACE INTO " . $cur_db_pre . "command_schedule ";
					$sched_ins_query .= ' (';
					$sched_ins_query .= '  team_id, country_id, region_id, program_id, cost, start_date, end_date, ';
					$sched_ins_query .= '  created, created_by, modified, modified_by ';
					$sched_ins_query .= ' )';
					$sched_ins_query .= ' VALUES ';
					$sched_ins_query .= ' (';
					$sched_ins_query .= "  " . $dev_team_id . ", ";
					$sched_ins_query .= "  " . $schedule->country_id . ", ";
					$sched_ins_query .= "  " . $schedule->region_id . ", ";
					$sched_ins_query .= "  " . $schedule->program_id . ", ";
					$sched_ins_query .= "  " . $schedule->cost . ", ";
					$sched_ins_query .= "  '" . $schedule->start_date . "', ";
					$sched_ins_query .= "  '" . $schedule->end_date . "', ";
					$sched_ins_query .= "  '" . $schedule->created . "', ";
					$sched_ins_query .= "  " . $schedule->created_by . ", ";
					$sched_ins_query .= "  '" . $schedule->modified . "', ";
					$sched_ins_query .= "  " . $schedule->modified_by . " ";
					$sched_ins_query .= ' )';

					$cur_db->query($sched_ins_query);
				}
				
				//
				//Copy Team Postmeta to Dev
				$pm_query = "SELECT * FROM " . $wpdb->prefix . "postmeta WHERE post_id = " . $team->value('post');
				$pm_data = $wpdb->get_results( $pm_query );

				foreach($pm_data as $post_meta)
				{
					$pm_ins_query = '';
					$pm_ins_query .= "REPLACE INTO " . $cur_db_pre . "postmeta ";
					$pm_ins_query .= ' (';
					$pm_ins_query .= '  post_id, meta_key, meta_value ';
					$pm_ins_query .= ' )';
					$pm_ins_query .= ' VALUES ';
					$pm_ins_query .= ' (';
					$pm_ins_query .= "  " . $dev_post_id . ", ";
					$pm_ins_query .= "  '" . $post_meta->meta_key . "', ";
					$pm_ins_query .= "  '" . $post_meta->meta_value . "' ";
					$pm_ins_query .= ' )';

					$pm_del_query = '';
					$pm_del_query .= 'DELETE FROM ' . $cur_db_pre . 'postmeta ';
					$pm_del_query .= 'WHERE post_id = ' . $dev_post_id . ' AND meta_key = "' . $post_meta->meta_key . '"';

					$cur_db->query($pm_del_query);
					$cur_db->query($pm_ins_query);
				}

				//
				//Copy Reservations to Dev
				$rsvp_query = "SELECT * FROM " . $wpdb->prefix . "command_RSVP WHERE team_id = " . $team->value('id');
				$rsvp_data = $wpdb->get_results( $rsvp_query );

				foreach($rsvp_data as $reservation)
				{

					//
					//Build Post for RSVP
					$old_post = get_post($reservation->post_id);

					$ins_query = 'INSERT INTO ' . $cur_db_pre . 'posts ';
					$ins_query .= ' (';
					$ins_query .= '  post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, ';
					$ins_query .= '  comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, ';
					$ins_query .= '  post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, ';
					$ins_query .= '  comment_count ';
					$ins_query .= ' )';
					$ins_query .= ' VALUES ';
					$ins_query .= ' (';
					$ins_query .= "  " . $old_post->post_author . ", ";
					$ins_query .= "  '" . $old_post->post_date . "', ";
					$ins_query .= "  '" . $old_post->post_date_gmt . "', ";
					$ins_query .= "  '" . $old_post->post_content . "', ";
					$ins_query .= "  '" . $old_post->post_title . "', ";
					$ins_query .= "  '" . $old_post->post_excerpt . "', ";
					$ins_query .= "  '" . $old_post->post_status . "', ";
					$ins_query .= "  '" . $old_post->comment_status . "', ";
					$ins_query .= "  '" . $old_post->ping_status . "', ";
					$ins_query .= "  '" . $old_post->post_password . "', ";
					$ins_query .= "  '" . $old_post->post_name . "', ";
					$ins_query .= "  '" . $old_post->to_ping . "', ";
					$ins_query .= "  '" . $old_post->pinged . "', ";
					$ins_query .= "  '" . $old_post->post_modified . "', ";
					$ins_query .= "  '" . $old_post->post_modified_gmt . "', ";
					$ins_query .= "  '" . $old_post->post_content_filtered . "', ";
					$ins_query .= "   " . $old_post->post_parent . ", ";
					$ins_query .= "  '" . str_replace('new.islonline.org', 'dev.islonline.org', $old_post->post_modified_gmt) . "', ";
					$ins_query .= "   " . $old_post->menu_order . ", ";
					$ins_query .= "  '" . $old_post->post_type . "', ";
					$ins_query .= "  '" . $old_post->post_mime_type . "', ";
					$ins_query .= "  '" . $old_post->comment_count . "' ";
					$ins_query .= ' )';

					$cur_db->query($ins_query);
					$rsvp_post_id = $cur_db->insert_id;

					//
					//Build postmeta for RSV
					$pm_query = "SELECT * FROM " . $wpdb->prefix . "postmeta WHERE post_id = " . $reservation->post_id;
					$pm_data = $wpdb->get_results( $pm_query );

					foreach($pm_data as $post_meta)
					{
						$pm_ins_query = '';
						$pm_ins_query .= "REPLACE INTO " . $cur_db_pre . "postmeta ";
						$pm_ins_query .= ' (';
						$pm_ins_query .= '  post_id, meta_key, meta_value ';
						$pm_ins_query .= ' )';
						$pm_ins_query .= ' VALUES ';
						$pm_ins_query .= ' (';
						$pm_ins_query .= "  " . $rsvp_post_id . ", ";
						$pm_ins_query .= "  '" . $post_meta->meta_key . "', ";
						$pm_ins_query .= "  '" . $post_meta->meta_value . "' ";
						$pm_ins_query .= ' )';

						$pm_del_query = '';
						$pm_del_query .= 'DELETE FROM ' . $cur_db_pre . 'postmeta ';
						$pm_del_query .= 'WHERE post_id = ' . $dev_post_id . ' AND meta_key = "' . $post_meta->meta_key . '"';

						$cur_db->query($pm_del_query);
						$cur_db->query($pm_ins_query);
					}
					
					//
					//Build User for RSVP
					$old_user = get_userdata($reservation->user_id);

					$ins_query = 'INSERT INTO ' . $cur_db_pre . 'users ';
					$ins_query .= ' (';
					$ins_query .= '  ID, user_login, user_pass, user_nicename, user_email, user_url, user_registered, ';
					$ins_query .= '  user_activation_key, user_status, display_name ';
					$ins_query .= ' )';
					$ins_query .= ' VALUES ';
					$ins_query .= ' (';
					$ins_query .= "  " . $old_user->ID . ", ";
					$ins_query .= "  '" . $old_user->user_login . "', ";
					$ins_query .= "  '" . $old_user->user_pass . "', ";
					$ins_query .= "  '" . $old_user->user_nicename . "', ";
					$ins_query .= "  'david+TestTeam@illumatech.net', ";
					$ins_query .= "  '" . $old_user->user_url . "', ";
					$ins_query .= "  '" . $old_user->user_registered . "', ";
					$ins_query .= "  '" . $old_user->user_activation_key . "', ";
					$ins_query .= "  '" . $old_user->user_status . "', ";
					$ins_query .= "  '" . $old_user->display_name . "' ";
					$ins_query .= ' )';

					$cur_db->query($ins_query);

					//
					//Build usermeta for RSVP
					$um_query = "SELECT * FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $reservation->user_id;
					$um_data = $wpdb->get_results( $um_query );

					foreach($um_data as $user_meta)
					{
						$um_ins_query = '';
						$um_ins_query .= "REPLACE INTO " . $cur_db_pre . "usermeta ";
						$um_ins_query .= ' (';
						$um_ins_query .= '  user_id, meta_key, meta_value ';
						$um_ins_query .= ' )';
						$um_ins_query .= ' VALUES ';
						$um_ins_query .= ' (';
						$um_ins_query .= "  " . $reservation->user_id . ", ";
						$um_ins_query .= "  '" . $user_meta->meta_key . "', ";
						$um_ins_query .= "  '" . $user_meta->meta_value . "' ";
						$um_ins_query .= ' )';

						$um_del_query = '';
						$um_del_query .= 'DELETE FROM ' . $cur_db_pre . 'usermeta ';
						$um_del_query .= 'WHERE user_id = ' . $reservation->user_id . ' AND meta_key = "' . $post_meta->meta_key . '"';

						$cur_db->query($um_del_query);
						$cur_db->query($um_ins_query);
					}
					
					
					//
					//Create Order
					$o_query = "INSERT INTO " . $cur_db_pre . "command_orders (created_by) VALUES (" . $reservation->user_id . ")";
					$cur_db->query($o_query);
					$order_id = $cur_db->insert_id;
					
					//
					//Copy Order Items to Dev
					$items_query = "SELECT * FROM " . $wpdb->prefix . "command_order_items 
									WHERE 
										(id = " . $reservation->order_item_id . " AND add_on = 0 AND created_by = " . $reservation->user_id . ") OR
										(add_on = " . $reservation->order_item_id . " AND created_by = " . $reservation->user_id . ")
									ORDER BY add_on";
					$item_data = $wpdb->get_results( $items_query );
					$main_item_id = 0;
					$item_trans = array();
					
					foreach($item_data as $items)
					{
						$item_ins_query = '';
						$item_ins_query .= "REPLACE INTO " . $cur_db_pre . "command_order_items ";
						$item_ins_query .= ' (';
						$item_ins_query .= '  order_id, item_type, item_id, add_on, note, quantity, price, created_by, created_when ';
						$item_ins_query .= ' )';
						$item_ins_query .= ' VALUES ';
						$item_ins_query .= ' (';
						$item_ins_query .= "  " . $items->order_id . ", ";
						$item_ins_query .= "  " . $items->item_type . ", ";
						$item_ins_query .= "  " . $dev_team_id . ", ";
						$item_ins_query .= "  " . $main_item_id . ", ";
						$item_ins_query .= "  '" . $items->note . "', ";
						$item_ins_query .= "  " . $items->quantity . ", ";
						$item_ins_query .= "  " . $items->price . ", ";
						$item_ins_query .= "  " . $items->created_by . ", ";
						$item_ins_query .= "  '" . $items->created_when . "' ";
						$item_ins_query .= ' )';

						$cur_db->query($item_ins_query);
					
						if($items->item_type == 1)
						{
							$main_item_id = $cur_db->insert_id;
						}
						
						$item_trans[$items->id] = $cur_db->insert_id;
					}
					
					//
					//Copy Payments and Transactions to Dev
					$trans_id_query = 'SELECT GROUP_CONCAT( transaction_id ) 
									FROM  `wp_command_order_payment` 
									WHERE order_item_id IN ( ' . implode(",",array_keys($item_trans)) . ' )'; 
					$trans_id_data = $wpdb->get_var( $trans_id_query );
					
					//
					//Copy Transactions to Dev
					$trans_query = "SELECT * FROM " . $wpdb->prefix . "command_order_transact 
									WHERE id in (" . $trans_id_data . ")";
					$trans_data = $wpdb->get_results( $trans_query );

					foreach($trans_data as $transaction)
					{
						$trans_ins_query = '';
						$trans_ins_query .= "REPLACE INTO " . $cur_db_pre . "command_order_transact ";
						$trans_ins_query .= ' (';
						$trans_ins_query .= '  method, action, amount, response, form, transaction_id, raw_request, raw_response, created_by, created_when ';
						$trans_ins_query .= ' )';
						$trans_ins_query .= ' VALUES ';
						$trans_ins_query .= ' (';
						$trans_ins_query .= "  '" . $transaction->method . "', ";
						$trans_ins_query .= "  '" . $transaction->action . "', ";
						$trans_ins_query .= "  " . $transaction->amount . ", ";
						$trans_ins_query .= "  '" . $transaction->response . "', ";
						$trans_ins_query .= "  '" . $transaction->form . "', ";
						$trans_ins_query .= "  '" . $transaction->transaction_id . "', ";
						$trans_ins_query .= "  '" . $transaction->raw_request . "', ";
						$trans_ins_query .= "  '" . $transaction->raw_response . "', ";
						$trans_ins_query .= "  " . $transaction->created_by . ", ";
						$trans_ins_query .= "  '" . $items->created_when . "' ";
						$trans_ins_query .= ' )';

						$cur_db->query($trans_ins_query);
						$trans_link[$transaction->id] = $cur_db->insert_id;
					}
					
					//
					//Copy payments
					$pay_query = "SELECT * FROM " . $wpdb->prefix . "command_order_payment 
									WHERE transaction_id in (" . $trans_id_data . ")";
					$pay_data = $wpdb->get_results( $pay_query );

					foreach($pay_data as $payments)
					{
						$pay_ins_query = '';
						$pay_ins_query .= "REPLACE INTO " . $cur_db_pre . "command_order_payment ";
						$pay_ins_query .= ' (';
						$pay_ins_query .= '  ind_id, order_id, order_item_id, transaction_id, special, amount, note, created_by, created_when ';
						$pay_ins_query .= ' )';
						$pay_ins_query .= ' VALUES ';
						$pay_ins_query .= ' (';
						$pay_ins_query .= "  " . $trans_link[$payments->transaction_id] . ", ";
						$pay_ins_query .= "  " . $order_id . ", ";
						$pay_ins_query .= "  " . $item_trans[$payments->order_item_id] . ", ";
						$pay_ins_query .= "  " . $trans_link[$payments->transaction_id] . ", ";
						$pay_ins_query .= "  '" . $payments->special . "', ";
						$pay_ins_query .= "  " . $payments->amount . ", ";
						$pay_ins_query .= "  '" . $payments->note . "', ";
						$pay_ins_query .= "  " . (($payments->created_by == $reservation->user_id) ? $reservation->user_id : 2) . ", ";
						$pay_ins_query .= "  '" . $payments->created_when . "' ";
						$pay_ins_query .= ' )';

						$cur_db->query($pay_ins_query);
					}
					
					//
					//Write the actual RSVP now that we have EVERYTHING else
					$rsvp_ins_query = '';
					$rsvp_ins_query .= "REPLACE INTO " . $cur_db_pre . "command_RSVP ";
					$rsvp_ins_query .= ' (';
					$rsvp_ins_query .= '  team_id, user_id, order_item_id, post_id, title, passport, created_by, created ';
					$rsvp_ins_query .= ' )';
					$rsvp_ins_query .= ' VALUES ';
					$rsvp_ins_query .= ' (';
					$rsvp_ins_query .= "  " . $dev_team_id . ", ";
					$rsvp_ins_query .= "  " . $reservation->user_id . ", ";
					$rsvp_ins_query .= "  " . $main_item_id . ", ";
					$rsvp_ins_query .= "  " . $rsvp_post_id . ", ";
					$rsvp_ins_query .= "  '" . $reservation->title . "', ";
					$rsvp_ins_query .= "  '" . $reservation->passport . "', ";
					$rsvp_ins_query .= "  " . $reservation->user_id . ", ";
					$rsvp_ins_query .= "  '" . $reservation->created . "' ";
					$rsvp_ins_query .= ' )';

					$cur_db->query($rsvp_ins_query);

				}	//Foreach reservation
			} 		//Have Team and Post on Dev
		}			//Found team - code and hash match
	}				//Have Team Name and Slash
}