<?php

//
//Callback for docs.sig.js script to enter information into database
add_action('wp_ajax_cmd_settings_file_upload','cmd_settings_file_upload_callback');
add_action('wp_ajax_nopriv_cmd_settings_file_upload','cmd_settings_file_upload_callback');
function cmd_settings_file_upload_callback()
{

	$url = $_POST['url'];
	$post_id = $_POST['id'];

	//
	//Parse and build paths
	//http://new.islonline.org/wp-content/uploads/2016/10/motumcgmoviestory_large-2.jpg
	$location = parse_url($url);
	$scheme = $location['scheme'];
	$host = $location['host'];
	
	$filename = substr($location['path'], strrpos($location['path'], '/'));
	$filetype = substr($filename, strrpos($filename, '.'));
	$path = str_replace($filename, '', $location['path']);
	$path = str_replace('wp-content/', '', $path);
	
	$new_filename = get_current_user_id() . '-' . date('Ymdhis') . $filetype;
	$new_path = '/user-app-files/';

	//
	//Move file to new path
	$tries = 0;
	do
	{
		$uploaded = file_exists(WP_CONTENT_DIR . $path . $filename);

		if($uploaded)
		{
			copy(WP_CONTENT_DIR . $path . $filename, WP_CONTENT_DIR . $new_path . $new_filename);
		}
		else
		{
			sleep (1);
			$tries++;
		}
	} while (!$uploaded && $tries<10);


	//
	//Clear the entry into the post_table
	wp_delete_post($post_id);

	$rt = array('url' => $url,
			   'old' => WP_CONTENT_DIR . $path . $filename,
			   'new' => WP_CONTENT_DIR . $new_path . $new_filename,
			   'new_url' => $scheme . '://' . $host . '/wp-content' . $new_path . $new_filename,
			   'file_exists' => $uploaded,
			   'tries' => $tries);

	echo json_encode($rt);
	wp_die();
}

?>