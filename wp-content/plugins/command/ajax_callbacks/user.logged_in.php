<?php


//
//Callback for user.check.js script to check if username exists
add_action('wp_ajax_cmd_user_logged_in','cmd_user_logged_in_callback');
add_action('wp_ajax_nopriv_cmd_user_logged_in','cmd_user_logged_in_callback');

function cmd_user_logged_in_callback()
{
	$ret['loggedIn'] = false;
	
	if( is_user_logged_in() )
	{
		$ret['loggedIn'] = true;
	}
	
	echo json_encode($ret);
	die();

}