<?php


//
//Callback for user.check.js script to check if username exists
add_action('wp_ajax_cmd_user_name_check','cmd_user_name_check_callback');
add_action('wp_ajax_nopriv_cmd_user_name_check','cmd_user_name_check_callback');

function cmd_user_name_check_callback()
{
	$username = $_POST["data"]["usernameToCheck"];
		
	if (username_exists($username))
	{
		$available = 0;
	} 
	else 
	{
		$available = 1;
	} 
	
	echo $available;
	die();

}