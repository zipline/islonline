<?php

require_once CC_CLASSES_DIR . "team.class.php";
require_once CC_CLASSES_DIR . "country.class.php";

//
//Callback for featured.teams.js script to enter information into featured teams listing.
add_action('wp_ajax_cmd_team_data_fetch','cmd_team_data_fetch_callback');
function cmd_team_data_fetch_callback()
{
	$team = new TEAM();
	$country = new COUNTRY();

	$team->set_team($_POST['team']);
	$country_list = $country->country_list($team->schedule_countries());

	$ret_array['hash'] = $team->value('hash');
	$ret_array['arrival_date'] = $team->arrival_date();
	$ret_array['departure_date'] = $team->departure_date();
	$ret_array['trip_duration'] = $team->trip_duration();
	$ret_array['arrival_city'] = $team->value('arrival_city');
	$ret_array['departure_city'] = $team->value('departure_city');
	$ret_array['trip_cost'] = $team->trip_cost();
	$ret_array['country_list'] = $country_list;
	
	echo json_encode($ret_array);
	wp_die();
}


?>