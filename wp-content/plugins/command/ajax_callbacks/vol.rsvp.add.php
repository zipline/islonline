<?php

//
//Possible necessary classes
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "finance.class.php");
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "cart.class.php");


//
//Callback for school.vol.js script 
add_action('wp_ajax_cmd_volunteer_add_rsvp','cmd_volunteer_add_rsvp_callback');

function cmd_volunteer_add_rsvp_callback()
{
	if(!empty($_POST['team_id']) && !empty($_POST['res_count']))
	{
		$team = new TEAM();
		$finance = new FINANCE();
		$cart = new CART( false, 0 );
		
		//
		//2018-03-22 DJT Added a clear cart just in case the user had something in their cart to begin with.
		$cart->Clear();

		$team->set_team($_POST['team_id']);
		$cart->add_RSVP( $team->value('id'), $team->value('cost'), $_POST['res_count'] );

		$trans_data = array('amount' => 0,
							'form' => 'No Payment',
							'response' => 'Approved',
							'transaction_id' => date('Ymdhis'),
							'raw_request' => 'CFL Blank Volunteer',
							'raw_response' => 'Approved with 0 down',
							'ip_address' => $_SERVER['REMOTE_ADDR'],
							'user_agent' => $_SERVER['HTTP_USER_AGENT'],
							'pp_trans_id' => date('Ymdhis')
							);		
		
		$finance->change_user(get_current_user_id());
		$finance->new_transaction($trans_data, 'defined', 'checkout');	
		$finance->move_CartToOrder( $cart, false, true );
		$finance->apply_payments('defined', 'checkout', $trans_data['amount'], 'All', 'Zero payment link', NULL, true);
		$cart->Clear();	
	}
}