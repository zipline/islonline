<?php
require_once (CC_CLASSES_DIR . "rsvp.class.php");
require_once (CC_CLASSES_DIR . "team.class.php");
require_once (CC_CLASSES_DIR . "message.class.php");

//
//Callback for rsvp.share.js script
add_action('wp_ajax_cmd_rsvp_cancel','cmd_rsvp_cancel_callback');
add_action('wp_ajax_nopriv_cmd_rsvp_cancel','cmd_rsvp_cancel_callback');

function cmd_rsvp_cancel_callback()
{

	if(!empty($_POST['rsvp_id']))
	{
		//
		//Find options
		$options = get_option('cmd_center');
		
		//
		//No errors thus far....
		if(!empty($options))
		{
			$rsvp = new RSVP();
			$team = new TEAM();
			
			//
			//Who is requesting the refund.
			$who = wp_get_current_user();
			
			//
			//Find the reservation and team
			$rsvp->set_rsvp_id($_POST['rsvp_id']);
			$rsvp_info = $rsvp->fetch_rsvp();
			$team->set_team($rsvp_info['team_id']);
			
			$finance_page = "wp-admin/admin.php?page=cc-pops&display=finance&team=" . $rsvp_info['team_id'] . "&member=" . $who->ID . "&rsvp=" . $_POST['rsvp_id'];
			$team_page = "wp-admin/admin.php?page=cc-teams&team_id=" . $rsvp_info['team_id'];
			
			//
			//Set cancel flag in meta
			update_post_meta($rsvp_info['post_id'], '_cmd_cancel_request', 'Cancel Requested: ' . date('m/d/Y h:i:s'));
				
			//
			//Set core email pieces
			$admin_addresses = explode(",", get_option('cmd_center')['submit_cancel_email']);
			$headers[] = 'From: ' . $who->display_name . ' <' . $who->user_email . '>';
			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			//
			///Construct the rest of the email
			$body = "User: " . $who->display_name . PHP_EOL;
			$body .= "<BR>" . PHP_EOL;
			$body .= "Wishes to cancel their reservation for team: " . $team->value('hash') . "<BR>" . PHP_EOL;	
			
			if(!empty($_POST['reason']))
			{
				$body .= "<BR>" . $_POST['reason'] . "<BR>" . PHP_EOL;	
			}
			
			$body .= "<BR>" . PHP_EOL;
			$body .= "Click <a href='" . site_url($team_page, 'admin') . "'>HERE</a> to view the Team Page<BR>" . PHP_EOL;
			$body .= "Click <a href='" . site_url($finance_page, 'admin') . "'>HERE</a> to view the RSVP Finance Page<BR>" . PHP_EOL;
			
			$message = new MESSAGES();
			$message->set_sender(1);
			$message->set_recipient($who->ID);
			$message->set_subject('Cancellation Request');
			$message->set_message_text('Your Cancellation Request has been delivered. We should be in contact shortly.');
			$message->set_team_id($rsvp_info['team_id']);
			$message->add_message();

			//
			//Send the email
			wp_mail($admin_addresses, 'Cancellation Request', $body, $headers);
			
			
			//
			//Send mail to user
			$vol_headers[] = 'From: ISL Cancellations <donotreply@islonline.org>';
			$vol_headers[] = 'Content-Type: text/html; charset=UTF-8';
			wp_mail($who->user_email, 'Cancellation Request', email_cancellation_request($who->user_firstname, 'Cancel ' . $team->value('hash')), $vol_headers);
			wp_die();
		}
		else
		{
			trigger_error($mail->error_list_text(), E_USER_ERROR);
		}
	}
	else
	{
		trigger_error('Information Missing', E_USER_ERROR);
	}
}
