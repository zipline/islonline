<?php

//
//Callback for region.load.js script to provide options values for select box
add_action('wp_ajax_cmd_region_load','cmd_region_load_callback');
function cmd_region_load_callback()
{
	global $wpdb;

	$country = isset($_POST['country']) ? $_POST['country'] : 30;

	$query = "SELECT id, title FROM " . $wpdb->prefix . "command_regions WHERE country_id=$country";
	$results = $wpdb->get_results($query, ARRAY_A);

	$regions = '';
	foreach($results as $record)
	{
		$regions .= "<option value='" . $record['id'] . "'>" . $record['title'] . "</option>";
	}

	echo $regions;
	wp_die();
}
?>